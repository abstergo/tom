package com.cat.common.constant;

/**
 * ali发送短信验证静态属性
 * Created by lxw on 2016/9/28.
 */
public class AliSmsConstants {

    public static final String bigFishUrl = "http://gw.api.taobao.com/router/rest";
    public static final String bigFishAppKey = "23478635";
    public static final String bigFishSecret = "01842b4b7bdcecec0d474b8fc1775486";

    public static final String productName = "盒饭LIVE";

    //登录确认验证码 1
    public static final String bigFishSmsFreeSignNameLogin = "盒饭LIVE";
    public static final String bigFishSmsTemplateCodeLogin = "SMS_16775070";

    //身份验证验证码 2
    public static final String bigFishSmsFreeSignIdentity = "盒饭LIVE";
    public static final String bigFishSmsTemplateCodeIdentity = "SMS_16775072";

    //信息变更验证码 3
    public static final String bigFishSmsFreeSignNameChangeMobile = "盒饭LIVE";
    public static final String bigFishSmsTemplateCodeChangeMobile = "SMS_16775065";

    //登录确认验证码  ? 绑定手机号短信配置信息  4
    public static final String bigFishSmsFreeSignBindMobile = "盒饭LIVE";
    public static final String bigFishSmsTemplateCodeBindMobile = "SMS_16775070";

    //修改密码验证码 5
    public static final String bigFishSmsFreeSignStarModifyPasswor = "盒饭LIVE";
    public static final String bigFishSmsTemplateCodeStarModifyPasswor = "SMS_16775066";

    /**
     * IOS马甲包短信模版 -S
     */
    public static final String productNameChannel = "直播LIVE";

    //登录确认验证码 100
    public static final String bigFishSmsFreeSignNameLoginChannel = "直播LIVE";
    public static final String bigFishSmsTemplateCodeLoginChannel = "SMS_16775070";

    //身份验证验证码 200
    public static final String bigFishSmsFreeSignIdentityChannel = "直播LIVE";
    public static final String bigFishSmsTemplateCodeIdentityChannel = "SMS_16775072";

    //信息变更验证码 300
    public static final String bigFishSmsFreeSignNameChangeMobileChannel = "直播LIVE";
    public static final String bigFishSmsTemplateCodeChangeMobileChannel = "SMS_16775065";

    //登录确认验证码  ? 绑定手机号短信配置信息  400
    public static final String bigFishSmsFreeSignBindMobileChannel = "直播LIVE";
    public static final String bigFishSmsTemplateCodeBindMobileChannel = "SMS_16775070";

    //修改密码验证码 500
    public static final String bigFishSmsFreeSignStarModifyPassworChannel = "直播LIVE";
    public static final String bigFishSmsTemplateCodeStarModifyPassworChannel = "SMS_16775066";
    /**
     * IOS马甲包短信模版 -E
     */
}
