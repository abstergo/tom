package com.cat.common.constant;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by kevin_zhang on 08/12/2016.
 */
public class ZucpSmsConstants {
    public static String serviceURL = "http://sdk2.entinfo.cn:8060/webservice.asmx";
    public static String sn =  "SDK-WSS-010-09969";
    public static String password = "8eecD70-1f7";

    public static String getPwd() {
        return getMD5(sn + password);
    }

    /*
     * 方法名称：getMD5
     * 功    能：字符串MD5加密
     * 参    数：待转换字符串
     * 返 回 值：加密之后字符串
     */
    private static String getMD5(String sourceStr) {
        String resultStr = "";
        try {
            byte[] temp = sourceStr.getBytes();
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(temp);
            // resultStr = new String(md5.digest());
            byte[] b = md5.digest();
            for (int i = 0; i < b.length; i++) {
                char[] digit = {'0', '1', '2', '3', '4', '5', '6', '7', '8',
                        '9', 'A', 'B', 'C', 'D', 'E', 'F'};
                char[] ob = new char[2];
                ob[0] = digit[(b[i] >>> 4) & 0X0F];
                ob[1] = digit[b[i] & 0X0F];
                resultStr += new String(ob);
            }
            return resultStr;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
