package com.cat.common.constant;

/**
 * Created by lxw on 2016/9/28.
 */
public class RedisKeyConstant {

    /**
     * 用户登陆token的Key
     */
    public final static String LONG_TOKEN_KEY = "loginToken_%s";

    /**
     * 短信验证码的Key
     */
    public final static String SMS_CODE_KEY = "smsCode_%d_%s";

    /**
     * 用户等级的KEY
     */
    public final static String USER_LEVEL_CFG_KEY = "userLevelCfg";

    /**
     * 网红等级的KEY
     */
    public final static String USER_RED_LEVEL_CFG_KEY = "userNetworkStirLevelCfg";

    /**
     * 红包钱的Key
     */
    public final static String RED_PACKET_MONEY_KEY = "redPacketMoney_%d";
    /**
     * 红包人的Key
     */
    public final static String RED_PACKET_PEOPLE_KEY = "redPacketPeople_%d";

    /**
     * 用户基本信息缓存KEY
     */
    public final static String WEB_USER_INFO_KEY = "userInfo_%s";

    /**
     * 用户基本信息缓存失效时间 单位秒
     */
    public final static int WEB_USER_INFO_SECONDS = 3600;

    /**
     * 用户认证信息缓存KEY
     */
    public final static String WEB_USER_IDENTITY_KEY = "userIdentity_%s";

    /**
     * 用户认证信息缓存失效时间 单位秒
     */
    public final static int WEB_USER_IDENTITY_SECONDS = 300;

    public final static String CONTINUE_PRESENT_NUM_KEY = "continuePresentNum_%s";

    public final static int CONTINUE_PRESENT_NUM_SECONDS  = 6000;

    /**
     * hf发布活动key
     */
    public final static String HF_ACTIVITY_CFG_KEY = "hfActivityCfgInfo_%d";

    /**
     * hf发布活动失效时间设置为1天
     */
    public final static int HF_ACTIVITY_CFG__SECONDS  = 1*24*60*60;

    /**
     * 用户经验
     */
    public final static String USER_EXP_KEY = "userExp_%s";

    /**
     * 用户剩余饭票
     */
    public final static String USER_BALANCE_KEY = "userBalance_%s";

    /**
     * 用户累计盒饭
     */
    public final static String USER_HEFAN_TOTAL_KEY = "userHefanTotal_%s";

    /**
     * 用户累计贡献
     */
    public final static String USER_PAY_COUNT_KEY = "userPayCount_%s";

    /**
     * hf发布活动属性信息key
     */
    public final static String HF_ACTIVITY_ATTR_KEY ="hfActivityAttr_%d";

    /**
     * hf发布活动属性失效时间设置为1天
     */
    public final static int HF_ACTIVITY_ATTR_SECONDS  = 1*24*60*60;

    /**
     * 发布活动属性信息剩余有效次数
     */
    public final static String HF_ACTIVITY_ATTR_AVLID_NUM_KEY = "hfActivityAttrAvlidNum_%d_%d";

    /**
     * 活动频繁请求验证key
     */
    public final static String HF_ACTIVITY_FREQUENTLY_REQ_KEY = "hfActivityFrequentlyReq_%s_%d";

    /**
     * 活动频繁请求key存在时间3秒
     */
    public final static int  HF_ACTIVITY_FREQUENTLY_SECONDS = 2;

    /**
     * 原主播积分榜,新需求总榜
     */
    public final static String ANCHOR__RANKING_LIST ="anchorRankingList:";

    /**
     * 实力榜
     */
    public final static String ANCHOR__HF_RANKING_LIST ="anchorhFRankingList:";

    /**
     * 主播排行榜 月榜
     */
    public final static String ANCHOR__HF_RANKING_LIST_EVERYMONTH ="anchorhFRankingListEveryMonth:";
    /**
     * redis记录
     */
    public static final String KEY_RECORD = "record:";

    /**
     * redis活跃列表
     */
    public static final String KEY_ACTIVE_LIST = "active";

    /**
     * 用户观看时长
     */
    public final static String KEY_WATCH_TIME="watchTime_%s_%s";

    /**
     * 每次直播收入
     */
    public final static String ANCHOR_LIVE_INCOME="anchorLiveIncome_%s";

    /**
     * 行政区redis_key
     */
    public final static String HF_DISRICT_CFG_LIST = "hf_disrictCfgList";

    /**
     *
     * 首次进入俱乐部弹窗状态
     */
    public final static String FIRST_TO_CLUB_ALERT = "first_to_club_alert_%s";

    /**
     * 首次进入俱乐部弹窗数据
     */
    public final static String FIRST_TO_CLUB_ALERT_DATA = "first_to_club_alert_data";

    /**
     * 敏感词
     */
    public final static String SENSITIVE_LIST = "sensitive_list";

    /*
     * 直播预约数据
     */
    public final static String LIVE_BOOKING_DATA = "live_booking_data_%s";

    /**
     * 直播预约列表 存储预约该直播的用户id
     */
    public final static String LIVE_BOOKING_LIST = "live_booking_list_%s";


    /**
     * 直播预约数据sortSet score为开播时间
     */
    public final static String LIVE_BOOKING_SET = "live_booking_set";

}
