package com.cat.common.meta;

/**
 * 支付方式
 * Created by lxw on 2016/10/12.
 */
public enum PaymentTypeEnum {

    /**
     * IOS 的APPLE app Store PAY
     */
    IOS_APPLE_PAY(1,6,0,TradeLogPaySourceEnum.ISO_SOURCE_TYPE.getPaySource(),
            TradeLogAccountTypeEnum.APPLE_PAY_ACCOUNT_TYPE.getAccountType(),
            "","",7, "1","100000000"),

    /**
     * Android 微信支付
     */
    ANDROID_WECHAT_PAY(2,5,1,TradeLogPaySourceEnum.ANDROID_SOURCE_TYPE.getPaySource(),
            TradeLogAccountTypeEnum.WECHAT_ACCOUNT_TYPE.getAccountType(),
            "APP","",7, "1","100000000"),

    /**
     * Android 支付宝支付
     */
    ANDROID_ALIPAY_PAY(3,5,1,TradeLogPaySourceEnum.ANDROID_SOURCE_TYPE.getPaySource(),
            TradeLogAccountTypeEnum.ALIPAY_ACCOUNT_TYPE.getAccountType(),
            "","",7, "1","100000000"),

    /**
     * PC 微信支付
     */
    PC_WECHAT_PAY(4,4,2,TradeLogPaySourceEnum.PC_SOURCE_TYPE.getPaySource(),
            TradeLogAccountTypeEnum.WECHAT_ACCOUNT_TYPE.getAccountType()
            ,"NATIVE","",7, "1","100000000"),

    /**
     * PC 支付宝支付
     */
    PC_ALIPAY_PAY(5,4,2,TradeLogPaySourceEnum.PC_SOURCE_TYPE.getPaySource(),
            TradeLogAccountTypeEnum.ALIPAY_ACCOUNT_TYPE.getAccountType(),
            "","alipay.scanCodePayPc.return_url",7, "0","100000000"),


    /**
     * 微信公共账号 微信支付
     */
    WECHAT_PUBLIC_ACOOUNT_PAY(6,3,3,TradeLogPaySourceEnum.WECHAT_PUBLIC_ACOOUNT_SOURCE_TYPE.getPaySource(),
            TradeLogAccountTypeEnum.APPLE_PAY_ACCOUNT_TYPE.getAccountType(),
            "NATIVE","",7, "1","100000000"),

    /**
     * HTML5 支付宝wap支付
     */
    ALIPAY_HTML5_PAY(7,4,2,TradeLogPaySourceEnum.PC_SOURCE_TYPE.getPaySource(),
            TradeLogAccountTypeEnum.ALIPAY_ACCOUNT_TYPE.getAccountType(),
            "","",7, "0","100000000");

    /**
     * 支付请求类型
     * (1 IOS 的APPLE PAY, 2 Android 微信支付, 3 Android 支付宝支付, 4 PC 微信宝支付, 5 PC 支付宝支付, 6 微信公共账号 微信支付)
     */
    private int paymentType;

    /**
     * currency_exchange 表中exchange_rule_type值
     * 兑换规则类型：(0:盒饭兑换饭票  1:盒饭兑换人民币  2:饭票兑换盒饭  3:公共账户兑换比例  4：PC充值比例  5:安卓充值比例 6，iOS充值比例)
     */
    private int exchangeRuleType;

    /**
     * currency_recharge 表中 status 类型
     * (0:ios,1:android,2:pc,3:公共账号)
     */
    private int rechargeStatus;

    /**
     * trade_log 中 pay_source
     */
    private int paySource;

    /**
     * trade_log 中 account_type 类型（1,支付宝2：微信 3:苹果内购）
     */
    private int accountType;

    /**
     * 微信支付时  交易类型  trade_type  [JSAPI，NATIVE，APP]
     */
    private String wechatTradeType;

    /**
     * 回调页面地址路径属性文件中的属性名 (支付宝及时到账pc才会有)
     */
    private String returnUrlPropName;

    /**
     * 反对换比例类型 7  根据饭票计算平台所得费用使用
     */
    private int counterExchangeType;

    /**
     * 支付最小值
     */
    private String payMinVal;

    /**
     * 支付最大值
     */
    private String payMaxVal;

    public int getPaymentType() {
        return paymentType;
    }

    public int getExchangeRuleType() {
        return exchangeRuleType;
    }

    public int getRechargeStatus() {
        return rechargeStatus;
    }

    public int getPaySource() {
        return paySource;
    }

    public int getAccountType() {
        return accountType;
    }

    public String getWechatTradeType() {
        return wechatTradeType;
    }

    public String getReturnUrlPropName() {
        return returnUrlPropName;
    }

    public int getCounterExchangeType() {
        return counterExchangeType;
    }

    public String getPayMinVal() {
        return payMinVal;
    }

    public String getPayMaxVal() {
        return payMaxVal;
    }

    private PaymentTypeEnum(int paymentType, int exchangeRuleType,
                            int rechargeStatus, int paySource, int accountType,
                            String wechatTradeType, String returnUrlPropName, int counterExchangeType,
                            String payMinVal, String payMaxVal) {
        this.paymentType = paymentType;
        this.exchangeRuleType = exchangeRuleType;
        this.rechargeStatus = rechargeStatus;
        this.paySource = paySource;
        this.accountType = accountType;
        this.wechatTradeType = wechatTradeType;
        this.returnUrlPropName = returnUrlPropName;
        this.counterExchangeType = counterExchangeType;
        this.payMinVal = payMinVal;
        this.payMaxVal = payMaxVal;
    }

    /**
     * 根据支付类型值获取支付类型枚举类
     * @param paymentType
     * @return
     */
    public static PaymentTypeEnum getPaymentTypeEnumByPaymentType(int paymentType) {
        for (PaymentTypeEnum penum:PaymentTypeEnum.values()) {
            if(paymentType == penum.getPaymentType()) {
                return penum;
            }
        }
        return null;
    }

    /**
     * 验证是否apple app store pay
     * @param paymentType
     * @return
     */
    public static boolean isAppleAppStorePaymentType(int paymentType) {
        if(IOS_APPLE_PAY.getPaymentType() == paymentType) {
            return true;
        }
        return false;
    }

    /**
     * 获取支付来源
     * @param paymentType
     * @return
     */
    public static int getPayAccountByPaymentType(int paymentType) {
        for (PaymentTypeEnum penum:PaymentTypeEnum.values()) {
            if(paymentType == penum.getPaymentType()) {
                return penum.getAccountType();
            }
        }
        return 0;
    }
}
