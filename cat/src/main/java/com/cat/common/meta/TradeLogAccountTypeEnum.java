package com.cat.common.meta;

/**
 * trade_log 表中 account_type
 * Created by lxw on 2016/10/12.
 */
public enum TradeLogAccountTypeEnum {

    /**
     * 支付宝
     */
    ALIPAY_ACCOUNT_TYPE (1),

    /**
     * 微信
     */
    WECHAT_ACCOUNT_TYPE (2),
    /**
     * ApplePay
     */
    APPLE_PAY_ACCOUNT_TYPE (3),

    /**
     * 充值码
     */
    RECHARGE_CODE_ACCOUNT_TYPE (4),
    /**
     * 任务
     */
    TASK_CODE_ACCOUNT_TYPE (5),

    /**
     * 后台
     */
    BACKSTAGE_ACCOUNT_TYPE (6),

    /**
     * 活动赠送
     */
    ACTIVITY_ACCOUNT_TYPE (8);

    /**
     * 类型（1,支付宝2：微信 3:ApplePay 4: 充值码 5:任务 6:后台）
     * @return
     */
    private int accountType;

    public int getAccountType() {
        return accountType;
    }

    private TradeLogAccountTypeEnum(int accountType) {
        this.accountType = accountType;
    }
}
