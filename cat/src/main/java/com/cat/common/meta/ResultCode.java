package com.cat.common.meta;

/**
 * Created by wangpengyan on 2016/8/01.
 */
/**
 * @author waasdasd
 *
 */
public enum ResultCode {

	/**
	 * 成功. ErrorCode : 0
	 */
	SUCCESS(1000, "成功"),

	// 2000段异常系统异常
	/**
	 *
	 * 未知异常. ErrorCode : 01
	 */
	UnknownException(2001, "未知异常"),

	UNSUCCESS(2000, "失败"),

	RturnNullException(2002, "无返回结果"),


	// 3000段是单独处理
	ParamException(3000, "参数验证错误"),

	//暂无数据
	No_Data(3001, "暂无数据"),

	//[3050-3100}
	LoginAutoOutTime(3050,"自动登录超时,请重新登录"),

	//[3101-3199}
	Meal_Ticket_Not_Enough(3101,"可兑换盒饭数不足"),

	Meal_Ticket_Exchange_Rule_Is_Err(3102,"兑换规则错误"),

	Live_Room_Info_Is_Err(3103,"直播间信息不存在"),

	// end
	// 4000段是提示message
	ApplyCodeInvalid(4011,"申请主播申请码无效"),

	/**
	 * 虚拟用户不能进行操作
	 */
	VirtualUserNotOpreate(4040,"虚拟用户不能进行该操作"),
	/**
	 * 第三方openid非法
	 */
	ThridUserOpenIdError(4041,"第三方openid非法"),

	ThridUserWxError(4042,"微信登录的openid异常,请联系客服人员"),
	ThridUserVersionError(4043,"您的版本过低,请下载最新版"),

	/**
	 * 用户注册im失败
	 */
	LoginUserRegistImError(4048,"用户注册云信IM失败"),
	LoginUserRegistRyImError(4039,"用户注册融云IM失败"),

	/**
	 * 用户未登录
	 */
	LoginUserIsNotLogin(4049,"用户未登录"),

	/**
	 * 用户信息不存在
	 */
	LoginUserIsNotExist(4050,"用户信息不存在"),
	/**
	 * 用户密码错误
	 */
	LoginUserPasswordError(4051,"用户密码错误"),

	/**
	 * 用户账号被锁定
	 */
	LoginUserIsLock(4052, "用户账号被冻结"),

	/**
	 * 新账号登录注册时,账号被其它机器注册
	 */
	LoginUserAgain(4053, "网络异常,请重试"),

	/**
	 * 主播已经被关注
	 */
	UserWatched(4054,"用户已关注"),
	/**
	 * 主播已经被取消关注
	 */
	CancelWatched(4063,"用户已取消关注"),

	/**
	 * 用户关注数已达上限
	 */
	WatchedMaxLimit(4064,"关注已达上限"),

	/**
	 * 用户当日关注数已达上限
	 */
	WatchedMaxLimitForDay(4065,"当日关注已达上限"),

	/**
	 * 主播不存在
	 */
	AnchorNotFound(4055,"主播不存在"),

	/**
	 * 主播被冻结
	 */
	AnchorFrozen(4056,"主播被冻结"),
	/**
	 * 主播所在经济公司被冻结
	 */
	AnchorCompanyFrozen(4057,"主播所在经济公司被冻结"),

	/**
	 * 用户不存在
	 */
	UserNotFound(4058,"用户不存在"),
	/**
	 * 用户被删除
	 */
	userIsDel(4059,"用户被删除"),
	/**
	 * 关注的人不是主播
	 */
	WATCHEDISNOTANCHOR(4047,"用户关注的人非主播"),



	/**
	 * 绑定第三方账号|手机号|改变手机号
	 */
	AccountIsUsed(4060, "该信息已被其他用户使用"),
	AccountFial(4061, "绑定|解绑操作失败"),
	AccountOnlayFial(4062, "亲,请先使用其它方式进行绑定，才能进行解绑！"),
	/**
	 * 管理员超量
	 */
	AdminCount(4069,"管理员已达到上限"),

	/**
	 * 主播已被禁播
	 */
	LiveIsBanned(4070,"主播已被禁播"),
	/**
	 * 主播存在未关闭的直播
	 */
	LiveIsLiveing(4071,"主播存在未关闭的直播"),

	/**
	 * 直播已结束不能再结束
	 */
	LiveIsEnd(4072,"直播已经结束"),

	/**
	 * 直播处于关闭
	 */
	LiveIsClose(4073,"直播已经关闭"),

	LiveIsNull(4074, "直播不存在"),

	UserIsNotAnchor(4075,"用户没有主播权限" ),

	AuthError(4076,"权限错误"),

	LiveRoomRepeat(4078,"用户已经进入其他直播间"),

	ResultCode_4079(4079,"不能进入自己的直播间"),

	userIsOut(4080,"用户已被踢出该直播间"),
	/**
	 * 开播token异常，重试
	 */
	TOKEN_TRY_AGAIN(4083,"开播token获取异常，重试" ),
	TOKEN_ERROR(4082,"开播token验证失败"),
	/**
	 * 心跳失联，直播已结束
	 */
	HeartBeatBroken(4081, "心跳失联"),
	LivingWaitQueue(4084, "正在排队进入直播间"),
	RESULT_CODE_4085(4085, "内部员工不能被踢出"),

	RepeatOper(4089, "重复操作"),
	/**
	 * 发送验证码失败
	 */
	SendSmsCodeFial(4090,"发送验证码失败，请重发"),
	SmsCodeTimeOut(4091,"验证码已过期，请重发"),
	SmsCodeCheckFial(4092,"验证码错误"),
	SmsCodeParamError(4093,"发送短信验证码参数错误"),
	SmsCodeSaveError(4094,"短信验证码存储redis失败"),
	// end
	//[101-150) 被占用
	ParamsIdentityRepeat(4101, "认证信息不能重复提交"),

	//im用户已存在(新增时注册im返回已存在)
	ImAlreadyRegistered(4111,"im用户已存在"),

	//充值码充值
	RechangeCodePayErr1(4200, "充值码错误"),
	RechangeCodePayErr2(4201, "用户信息不存在"),
	LoginUserAuthFailed(4202, "用户授权不通过"),
	RechangeCodeBatchIsUsed(4203,"您已使用过该类型充值卡"),


	//支付
	PayWechatReqError(4500,"微信http请求失败"),
	PayOrderError(4501,"订单不存在"),
	PayWechatPreReqError(4501,"微信统一下单返回失败"),
	PayCurrencyRechargeObjIsEmpty(4502,"充值赠送信息已不存在"),
	PayExchangeRatioObjectIsEmpty(4502,"兑换比例不存在"),
	PayExchangeRatioValIsZero(4503,"兑换比例系数为0"),
	PayAcountIsError(4504,"支付金额不符合额度限制"),
	PayNotifyParamEmpty(4510,"回调通知参数为空"),
	PayWechatSingError(4511,"微信支付回调签名检查失败"),
	PayWechatNotifyError(4512,"微信支付回调通知错误"),
	PayAlipayNotifyIsNotValidUrl(4513,"支付宝回调通知非有效的请求"),
	PayAlipayNotifyIsPayFail(4514,"支付宝回调通知支付失败"),
	PayAppleAppStoreVerifyReceiptFail(4515,"苹果内购验证错误"),
	PayAppleAppSandboxVerifyCode(4516,"苹果内购支付为沙盒测试支付"),
	PayOrderIsValid(4520,"该订单号不存在对应的充值记录"),
	PayOrderIsNotfiyHandler(4521,"充值订单已被处理"),
	PayMentTypeIsValid(4522,"支付订单方式不存在"),
	PayAppleStoreReceiptIsHad(4523,"苹果内购验证码重复使用"),
	PayAppleStoreReceiptError(4524,"苹果内购验证码不为盒饭LIVE程序支付所得"),
	PayAppleStorePorductError(4525,"苹果返回商品与订单商品不符"),
	PayAppleStoreNoPorductError(4526,"苹果验证未返回商品记录"),

	RebalancePriceIsValid(4601,"扣费价格异常"),
	RebalanceBalanceNotEnough(4602,"扣费余额不足"),

	UserLevelInsufficient(4700,"用户等级不满32级"),

	/**
	 * 活动异常code
	 */
	ActivityIsNotAvlid(60001,"活动未启动"),
	ActivityIsAttended(60002,"活动参加过"),
	ActivityAttendFailed(60003,"活动参加失败"),
	ActivityAttendNext(60004,"没有抽奖次数，请下次再来"),
	ActivityFrequentlyReq(60005,"操作频繁,请稍后再试"),

	LastPic(4,"最后一张图片，是否删除动态"),
	/**
	 * 存在敏感词
	 */
	HasSensitiveWord(7000,"存在敏感词"),


	/**
	 * 断流请求失败
	 */
	LiveCloseReqError(70100,"断流请求失败"),

	/**
	 *
	 */
	MessageUpperLimit(8000,"动态数量超限啦，明天再与好友分享精彩内容吧");




	// end

	private int _code;
	private String _msg;

	public int get_code() {
		return _code;
	}

	public void set_code(int _code) {
		this._code = _code;
	}

	private ResultCode(int _code, String _msg) {
		this._code = _code;
		this._msg = _msg;
	}

	public String getMsg() {
		return _msg;
	}

	public static ResultCode getByCode(int code) {
		for (ResultCode ec : ResultCode.values()) {
			if (ec.get_code() == code) {
				return ec;
			}
		}
		return null;
	}
}
