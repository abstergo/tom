package com.cat.common.meta;

import com.cat.tiger.util.GlobalConstants;

/**
 * Created by hbchen on 2016/10/20.
 */
public enum ExpSourceEnum {

    WatchExp(1),   //观看经验
    InteractionExp(2),//互动经验
    ShareExp(3); //分享经验


    private int type;

    public int getType() {
        return type;
    }

    private ExpSourceEnum(int type) {
        this.type = type;
    }

    /**
     * 判断毫秒数是否超过3小时
     * @param type
     * @param time 毫秒数
     * @return
     */
    public static boolean checkIsLimit(int type,long time){
        if (type == WatchExp.getType()){
            if (time / (1000 * 60 * 60) >= GlobalConstants.EXP_WATCH_TIME_LIMIT){
                return true;
            }else {
                return false;
            }
        }
        return false;
    }
}
