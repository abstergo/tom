package com.cat.common.meta;

import com.cat.common.constant.AliSmsConstants;
import org.apache.commons.lang.StringUtils;

/**
 * 发送手机验证码类型枚举
 * Created by lxw on 2016/9/28.
 */
public enum SmsCodeTypeEnum {

    /**
     * 登录手机验证码类型
     */
    LOGIN_SMS_CODE_TYPE(1, AliSmsConstants.bigFishSmsFreeSignNameLogin, AliSmsConstants.bigFishSmsTemplateCodeLogin),

    /**
     * 身份验证验证码类型
     */
    IDENTITY_SMS_CODE_TYPE(2, AliSmsConstants.bigFishSmsFreeSignIdentity, AliSmsConstants.bigFishSmsTemplateCodeIdentity),

    /**
     * 变更|绑定手机号验证码类型
     */
    CHANGE_SMS_CODE_TYPE(3, AliSmsConstants.bigFishSmsFreeSignBindMobile, AliSmsConstants.bigFishSmsTemplateCodeBindMobile),

    /**
     * 明星后台修改密码发短信
     */
    STAR_MODIFY_PASSWOR_SMS_CODE_TYPE(5, AliSmsConstants.bigFishSmsFreeSignStarModifyPasswor, AliSmsConstants.bigFishSmsTemplateCodeStarModifyPasswor),


    /**
     * IOS马甲包短信模版 -S
     */

    /**
     * 登录手机验证码类型
     */
    LOGIN_SMS_CODE_TYPE_CHANNEL(100, AliSmsConstants.bigFishSmsFreeSignNameLoginChannel, AliSmsConstants.bigFishSmsTemplateCodeLoginChannel),

    /**
     * 身份验证验证码类型
     */
    IDENTITY_SMS_CODE_TYPE_CHANNEL(200, AliSmsConstants.bigFishSmsFreeSignIdentityChannel, AliSmsConstants.bigFishSmsTemplateCodeIdentityChannel),

    /**
     * 变更|绑定手机号验证码类型
     */
    CHANGE_SMS_CODE_TYPE_CHANNEL(300, AliSmsConstants.bigFishSmsFreeSignBindMobileChannel, AliSmsConstants.bigFishSmsTemplateCodeBindMobileChannel),

    /**
     * 明星后台修改密码发短信
     */
    STAR_MODIFY_PASSWOR_SMS_CODE_TYPE_CHANNEL(500, AliSmsConstants.bigFishSmsFreeSignStarModifyPassworChannel, AliSmsConstants.bigFishSmsTemplateCodeStarModifyPassworChannel);

    /**
     * IOS马甲包短信模版 -E
     */

    private int type;

    private String smsFreeSignName;
    private String smsTemplateCode;

    public int getType() {
        return type;
    }

    public String getSmsFreeSignName() {
        return smsFreeSignName;
    }

    public String getSmsTemplateCode() {
        return smsTemplateCode;
    }

    SmsCodeTypeEnum(int type, String smsFreeSignName, String smsTemplateCode) {
        this.type = type;
        this.smsFreeSignName = smsFreeSignName;
        this.smsTemplateCode = smsTemplateCode;
    }

    public static boolean enumTypeIsExist(int type) {
        for (SmsCodeTypeEnum typeEnum : SmsCodeTypeEnum.values()) {
            if (typeEnum.getType() == type) {
                return true;
            }
        }
        return false;
    }

    public static SmsCodeTypeEnum getEnumByType(int type) {
        for (SmsCodeTypeEnum typeEnum : SmsCodeTypeEnum.values()) {
            if (typeEnum.getType() == type) {
                return typeEnum;
            }
        }
        return null;
    }

    public static SmsCodeTypeEnum getEnumByType(int type, String channelNo) {
        //HeFanTV 非官方包 iosaso1, iosaso2, iosaso3, iosaso4, iosaso5, iosaso6, iosaso7
        if (StringUtils.isNotBlank(channelNo) && channelNo.contains("iosaso")) {
            for (SmsCodeTypeEnum typeEnum : SmsCodeTypeEnum.values()) {
                if (typeEnum.getType() == type * 100) {
                    return typeEnum;
                }
            }
        } else {  //HeFanTV 官方主包
            for (SmsCodeTypeEnum typeEnum : SmsCodeTypeEnum.values()) {
                if (typeEnum.getType() == type) {
                    return typeEnum;
                }
            }
        }
        return null;
    }

    public static String getProductName(String channelNo) {
        //HeFanTV 非官方包 iosaso1, iosaso2, iosaso3, iosaso4, iosaso5, iosaso6, iosaso7
        if (StringUtils.isNotBlank(channelNo) && channelNo.contains("iosaso")) {
            return AliSmsConstants.productNameChannel;
        } else {  //HeFanTV 官方主包
            return AliSmsConstants.productName;
        }
    }
}
