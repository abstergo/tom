package com.cat.common.meta;

import com.cat.common.constant.RedisKeyConstant;

/**
 * Created by lxw on 2016/11/23.
 */
public enum WebUserCachTypeEnum {

    /**
     * 经验缓存信息
     */
    Exp_type(1,RedisKeyConstant.USER_EXP_KEY, 500, "userExpUpdateBorder"),

    /**
     * 饭票余额缓存信息
     */
    Balance_type(2,RedisKeyConstant.USER_BALANCE_KEY, 100,"userBalanceUpdaateBorder"),
    /**
     * 明细历史盒饭缓存信息
     */
    HefanTotal_type(3,RedisKeyConstant.USER_HEFAN_TOTAL_KEY, 2000,"userHefanTotalUpdateBorder"),


    /**
     * 贡献累计值
     */
    PayCount_type(4,RedisKeyConstant.USER_PAY_COUNT_KEY, 100, "userPayCountUpdateBorder");

    private int type;

    private String cachKey;

    private long upBorder;

    private String upBorderZooKey;

    public int getType() {
        return type;
    }

    public String getCachKey() {
        return cachKey;
    }

    public long getUpBorder() {
        return upBorder;
    }

    public String getUpBorderZooKey() { return upBorderZooKey; }

    /**
     * 用户信息类型自增类型
     * @param type   1 经验 2 饭票余额 3 累计盒饭数  4 累计贡献值
     * @param cachKey  缓存key
     * @param upBorder 触发更新值[默认值]
     * @param upBorderZooKey 触发更新值zookeep的key值
     */
    WebUserCachTypeEnum(int type, String cachKey, long upBorder, String upBorderZooKey) {
        this.type = type;
        this.cachKey = cachKey;
        this.upBorder = upBorder;
        this.upBorderZooKey =  upBorderZooKey;
    }

    /**
     * 获取对应缓存的key
     * @param webUserCachTypeEnum
     * @param userId
     * @return
     */
    public static String  getCachKey(WebUserCachTypeEnum webUserCachTypeEnum, String userId) {
        return String.format(webUserCachTypeEnum.cachKey,userId);
    }

    /**
     * 是否进行db更新
     * @param webUserCachTypeEnum
     * @param cachVal
     * @param dbVal
     * @return
     */
    public static boolean checkChangeBorderModifyToDb(WebUserCachTypeEnum webUserCachTypeEnum, long cachVal, long dbVal, long thresholdVal) {
        if(Math.abs(cachVal-dbVal) >= thresholdVal) {
            return true;
        }
        return false;
    }
}
