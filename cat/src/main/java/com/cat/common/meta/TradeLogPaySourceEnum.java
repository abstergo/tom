package com.cat.common.meta;

/**
 * trade_log 表中 pay_source
 * Created by lxw on 2016/10/12.
 */
public enum TradeLogPaySourceEnum {

    /**
     * PC
     */
    PC_SOURCE_TYPE (1),

    /**
     * IOS端
     */
    ISO_SOURCE_TYPE (2),
    /**
     * 安卓端
     */
    ANDROID_SOURCE_TYPE (3),

    /**
     * 公共账号
     */
    WECHAT_PUBLIC_ACOOUNT_SOURCE_TYPE(4);
	
	
	


    /**
     * 支付来源：1：PC端 2：IOS端 3：安卓端  4: 公共账号
     * @return
     */
    private int paySource;

    public int getPaySource() {
        return paySource;
    }

    private TradeLogPaySourceEnum(int paySource) {
        this.paySource = paySource;
    }
}
