package com.cat.common.meta;

import com.cat.tiger.util.GlobalConstants;

/**
 * 上传OSS文件类型枚举类
 * Created by lxw on 2016/10/9.
 */
public enum OssFileTypeEnum {

    /**
     * 图片OSS上传类型
     */
    PIC_OSS_TYPE("0", "oss.pic.bucketName","oss.pic.access.domain", "oss.pic.endpoint"),

    /**
     * 视频OSS上传类型
     */
    VIDEO_OSS_TYPE("1","oss.video.bucketName","oss.video.access.domain","oss.video.endpoint");

    private String type;
    private String bucketNameProp;
    private String accessDmainProp;
    private String ossEndpointProp;

    public String getType() {
        return type;
    }

    public String getBucketNameProp() {
        return bucketNameProp;
    }

    public String getAccessDmainProp() {
        return accessDmainProp;
    }

    public String getOssEndpointProp() {
        return ossEndpointProp;
    }

    /**
     * 图片|视频上传枚举类型
     * @param type   资源类型  1 图片  2 视频
     * @param bucketNameProp  oss bucket name
     * @param accessDmainProp  oss bucket name
     * @param ossEndpointProp  oss bucket name
     */
    private OssFileTypeEnum(String type, String bucketNameProp, String accessDmainProp, String ossEndpointProp) {
        this.type = type;
        this.bucketNameProp = bucketNameProp;
        this.accessDmainProp = accessDmainProp;
        this.ossEndpointProp = ossEndpointProp;
    }

    public static OssFileTypeEnum findOssFileTypeEnumByType(String type) {
        for (OssFileTypeEnum ossEnum:OssFileTypeEnum.values()) {
            if(ossEnum.getType().equals(type)) {
                return ossEnum;
            }
        }
        return null;
    }

}
