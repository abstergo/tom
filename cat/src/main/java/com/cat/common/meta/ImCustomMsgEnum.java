package com.cat.common.meta;

/**
 * IM自定义消息类型
 *
 */
public enum ImCustomMsgEnum {

	/*type:
	0：普通文字
	100：点亮【广播】
	101：弹幕【广播】
	102：分享【广播】
	103：关注【广播】
	104：礼物（0红包、1不连发小礼物、2连发小礼物、3大礼物、4世界礼物）【广播】
	 105：高级会员进入【广播】
	 113：等级提示【广播】106废弃
	 107：向主播发警告【广播】
	 108：踢人【私信，广播】
	 109：禁言【私信，广播】
	 110：设置管理员【广播】
	 111：取消管理员【广播】
	 112：普通用户进入
	 200：盒饭数变化【广播】
	 201：直播结束【广播】
	 202：直播间在线人数【广播】
	 203：清空聊天室【广播】
	 204：主播暂时离开【广播】
	 205：成员列表刷新【广播】
	 206：主播回来(前端) 【广播】
	 207：同一账号开直播主动退出他人直播间 (后端) 【广播】
	 208：退出登录（违规、冻结等要求前端主动退出登录） 【私信】
	 209：禁播【广播】
	 210：泡泡【广播】
	 300: 私信-自定义表情【私信】
	 301: 私信-普通文本【私信】
*/
	BarrageMsg("101", "弹幕", "HFIMCustomMSgContent1"),
	LivePresent("104","送出礼物","HFIMCustomMSgContent1"),
	PresentChange("200","盒饭数变化","HFIMCustomMSgContent1"),
	LiveEndMsg("201","本次直播结束","HFIMCustomMSgContent1"),
	IM_MSG_208("208","退出登录","HFIMCustomMSgContent1"),
	Live_Ban("209","您的直播涉嫌违规，已被关闭！","HFIMCustomMSgContent1"),
	IM_MSG_206("206","主播回来","HFIMCustomMSgContent1"),
	IM_MSG_204("204","主播暂时离开","HFIMCustomMSgContent1"),


	IM_MSG_203("203","清空聊天室","HFIMCustomMSgContent2"),
	LiveOnLineNum("202","直播间在线人数","HFIMCustomMSgContent2"),
	IM_MSG_205("205","成员列表刷新","HFIMCustomMSgContent2"),
	IM_MSG_207("207","你已在另一设备登录","HFIMCustomMSgContent2"),

	AnchorWaringMsg("107","您已涉嫌违规，警告1次","HFIMCustomMSgContent3"),
	LiveOuterMsg("108","已被请出直播间！","HFIMCustomMSgContent3"),
	LiveShutupMsg("109","已被禁言了","HFIMCustomMSgContent3"),
	LiveSetAdmin("110","成为管理员","HFIMCustomMSgContent3"),
	LiveUnSetAdmin("111","不是管理员了","HFIMCustomMSgContent3"),

	TEXT_MESSAGE("0","普通文字消息","HFIMCustomMSgContent4"),
	WATCH_ANTHER("103","关注","HFIMCustomMSgContent4"),
	IM_MSG_105("105","高级会员进入","HFIMCustomMSgContent4"),
	IM_MSG_112("112","普通用户进入","HFIMCustomMSgContent4"),
	UserLevelMsg_2("113","等级提升","HFIMCustomMSgContent4"),
	IM_MSG_102("102","分享","HFIMCustomMSgContent4"),

	LIGHT_ROOM("100","点亮","HFIMCustomMSgContent5"),
	LIGHT_ROOM_PAOPAO("210","泡泡","HFIMCustomMSgContent5"),

	IM_MSG_300("300","私信-自定义表情","HFIMCustomMSgContent6"),

	IM_MSG_301("301","私信-普通文本","HFIMCustomMSgContent7"),

	IM_MSG_308("308","退出登录","HFIMCustomMSgContent8"),
	IM_MSG_307("307","你已在另一设备登录","HFIMCustomMSgContent8");



	private String type;
	private String msg;
	private String customTypeName;

	private ImCustomMsgEnum(String type, String msg,String customTypeName) {
		this.type = type;
		this.msg = msg;
		this.customTypeName = customTypeName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}


	public String getCustomTypeName() {
		return customTypeName;
	}

	public void setCustomTypeName(String customTypeName) {
		this.customTypeName = customTypeName;
	}

}
