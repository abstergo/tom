package com.cat.common.meta;

/**
 * drds 全局索引类型对应索引名的枚举
 * Created by lxw on 2016/10/26.
 */
public enum DrdsSequenceEnum {

    PUB_USER_ID_SEQUECE(0, "user_id_pub_seq");

    private int type;

    private String sequeceName;

    public int getType() {
        return type;
    }

    public String getSequeceName() {
        return sequeceName;
    }


    private DrdsSequenceEnum(int type, String sequeceName) {
        this.type = type;
        this.sequeceName = sequeceName;
    }

    /**
     * 获取sequeceName
     * @param type
     * @return
     */
    public static String getSequenceNameByType(int type) {
        for (DrdsSequenceEnum obj : DrdsSequenceEnum.values()) {
            if(obj.getType() == type) {
                return obj.getSequeceName();
            }
        }
        return null;
    }
}
