package com.cat.common.meta;

/**
 * Created by lxw on 2016/10/9.
 */
public enum UserTypeEnum {

    /**
     * 普通用户
     */
    PUB_USER(0),

    /**
     * 网红
     */
    RED_USER(1),

    /**
     * 明星
     */
    STAR_USER(2),

    /**
     * 片场
     */
    LOCATION_USER(3),

    /**
     *  内部员工
     */
    INTERNALLY_USER(4),

    /**
     * 经纪人
     */
    ECONOMIC_USER(5),

    /**
     * 虚拟币用户
     */
    VIRTUAL_USER(6);


    private int userType;

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    private UserTypeEnum(int userType) {
        this.userType = userType;
    }

    /**
     * 是否主播
     * @param userType
     * @return
     */
    public static boolean isAnchor(int userType) {
        if(UserTypeEnum.RED_USER.getUserType()<=userType && userType<= UserTypeEnum.LOCATION_USER.getUserType()) {
            return true;
        }
        return false;
    }

    /**
     * 网红
     * @param userType
     * @return
     */
    public static boolean isRedAnchor(int userType) {
        if(UserTypeEnum.RED_USER.getUserType() ==userType) {
            return true;
        }
        return false;
    }

    /**
     * 明星|片场
     * @param userType
     * @return
     */
    public static boolean isStarAnchor(int userType) {
        if(UserTypeEnum.STAR_USER.getUserType() ==userType || UserTypeEnum.LOCATION_USER.getUserType() ==userType) {
            return true;
        }
        return false;
    }

    /**
     * 虚拟用户
     * @param userType
     * @return
     */
    public static boolean isVirtualUser(int userType) {
        if(UserTypeEnum.VIRTUAL_USER.getUserType() == userType) {
            return true;
        }
        return false;
    }

}
