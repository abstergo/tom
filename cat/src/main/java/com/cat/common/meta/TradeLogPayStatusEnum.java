package com.cat.common.meta;

/**
 * 支付结果
 * Created by lxw on 2016/10/12.
 */
public enum TradeLogPayStatusEnum {

    /**
     * 支付失败
     */
    PAY_ERROR(1),

    /**
     * 未支付
     */
    PAY_NOT_PAY(2),

    /**
     * 支付回调通知成功
     */
    PAY_NOTIFY_SUCCESS(4),

    /**
     * 支付成功
     */
    PAY_SUCCESS(3);

    private int payStatus;

    public int getPayStatus() {
        return payStatus;
    }

    private TradeLogPayStatusEnum(int payStatus) {
        this.payStatus = payStatus;
    }

    /**
     * 检查支付通知是否可以处理
     * @param payStatus
     * @return
     */
    public static boolean checkPayNotifyIsHandler(int payStatus) {
        for (TradeLogPayStatusEnum objEnum: TradeLogPayStatusEnum.values()) {
            //支付记录已被处理(支付回调通知失败 | 成功 |支付成功 表示已处理过，不能再处理)
            if( payStatus == TradeLogPayStatusEnum.PAY_ERROR.getPayStatus() || payStatus == TradeLogPayStatusEnum.PAY_NOTIFY_SUCCESS.getPayStatus() ||
                    payStatus == TradeLogPayStatusEnum.PAY_SUCCESS.getPayStatus()) {
                return false;
            }
        }
        return true;
    }
}
