package com.cat.common.util;

import java.util.UUID;

/**
 * Created by lxw on 2016/9/28.
 */
public class GuuidUtil {

    /**
     * 生成uuid
     * @return
     */
    public static String getUuid() {
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.replaceAll("-", "");
        return uuid;
    }
}
