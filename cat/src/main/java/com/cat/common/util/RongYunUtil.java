package com.cat.common.util;

import java.security.MessageDigest;

import org.apache.commons.codec.binary.Hex;

/**
 * Created by liteng on 2017/2/7.
 */
public class RongYunUtil {
    /**
     * url
     */
    //获取Token方法
    public static final String getToken = "http://api.cn.ronghub.com/user/getToken.json";
    //发送聊天室消息方法
    public static final String publishPrivate = "http://api.cn.ronghub.com/message/private/publish.json";
    //创建聊天室
    public static final String createChatroom = "http://api.cn.ronghub.com/chatroom/create.json";
    //发送聊天室消息
    public static final String publishChatroom = "http://api.cn.ronghub.com/message/chatroom/publish.json";
    //添加聊天室禁言
    public static final String addTemporaryMute = "http://api.cn.ronghub.com/chatroom/user/gag/add.json";
    //移除聊天室禁言
    public static final String delTemporaryMute = "http://api.cn.ronghub.com/chatroom/user/gag/rollback.json";
    //查询被禁言成员信息
    public static final String getGagList = "http://api.cn.ronghub.com/chatroom/user/gag/list.json";
    public static String hexSHA1(String value) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(value.getBytes("utf-8"));
            byte[] digest = md.digest();
            return byteToHexString(digest);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public static String byteToHexString(byte[] bytes) {
        return String.valueOf(Hex.encodeHex(bytes));
    }

}
