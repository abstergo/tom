package com.cat.common.util;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * <p>Title:RandomUtil</p>
 * <p>Description: </p>
 * <p>Company: SAGA</p> 
 * @author zhaoning 
 * @date 2015-10-19下午11:20:32
 */
public class RandomUtil {

	/**
	 * @Description: TODO 10位随机数
	 * @param @return 
	 * @return String
	 * @author zhaoning
	 * @date 2015-10-19下午11:21:16
	 */
	public static String getRandom(){
		Random r = new Random();
        long num = Math.abs(r.nextLong() % 10000000000L);
        String s = String.valueOf(num); 
        return s;
    }
	
	public static int getRandomForRead() {
		int number = 0;
		number=1000+(int)(Math.random()*500);
		return number;
		}
	
	/**
	 * 
	 * @Description: TODO
	 * @return int 手机验证码 
	 * @author ning
	 * @date 2016-1-24
	 */
	public static int getPhoneSignCode(){
		Random rm = new Random();
		return (int)(Math.random()*9000+999);
	}

	/**
	 * 生成9位随机数
	 * @Description: &{todo}
	 * @param @return
	 * @return int
	 * @author geyuqi
	 * @date 2015-12-25下午5:46:26
	 */
	public static int getRandomN(){
		Random rm = new Random();
		return (int)((1+rm.nextDouble())*Math.pow(10, 9));
	}
	
	/**
	 * 
	 * @Description: &{todo}
	 * @param @param x
	 * @param @return
	 * @return int
	 * @author geyuqi
	 * @date 2016-1-11下午6:22:39
	 */
	public static String getRandomByNum(int x){
		int xx = (int)(Math.random()*x);
		if(xx<10){
			return "000"+String.valueOf(xx);
		}else if(xx>=10 && xx<=99){
			return "00"+String.valueOf(xx);
		}else if(xx>99 && xx<999){
			return "0"+String.valueOf(xx);
		}else{
			return String.valueOf(xx);
		}
	}
	/**
	 * 生成从start到end的随机数  包括开始不包括结束
	 * @Description: TODO
	 * @param start 
	 * @param end
	 * @return int  
	 * @author ning
	 * @date 2016-1-28
	 */
	public static int getRandom(int start ,int end){
		return ThreadLocalRandom.current().nextInt(start, end);
	}

	/**
	 * 生成从start到end的随机数  包括开始不包括结束
	 * @param start
	 * @param end
     * @return
     */
	public static long getRandom(long start, long end) {
		return ThreadLocalRandom.current().nextLong(start, end);
	}
}
