package com.cat.common.util;

import java.math.BigDecimal;

public class BigDecimalCalUtil {
	// 保留精度位数，默认为2位
	private static int scalePub = 2;
	
	/**
	 * 加法
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static String add(String v1, String v2) {
		BigDecimal b1 = new BigDecimal(v1);
		BigDecimal b2 = new BigDecimal(v2);
		return (round(b1.add(b2),scalePub)).toString();
	}

	/**
	 * 加法
	 * @param v1
	 * @param v2
     * @return
     */
	public static BigDecimal dd(BigDecimal v1, BigDecimal v2) {
		return roundBigDecimal(v1.add(v2),scalePub);
	}
	


	/**
	 * 减法
	 * @param v1
	 * @param v2
     * @return
     */
	public static String sub(String v1, String v2) {
		BigDecimal b1 = new BigDecimal(v1);
		BigDecimal b2 = new BigDecimal(v2);
		return (round(b1.subtract(b2),scalePub)).toString();
	}

	/**
	 * 减法
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static BigDecimal sub(BigDecimal v1, BigDecimal v2) {
		return roundBigDecimal(v1.subtract(v2),scalePub);
	}

	
	/**
	 * 乘法
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static String mul(String v1, String v2) {
		BigDecimal b1 = new BigDecimal(v1);
		BigDecimal b2 = new BigDecimal(v2);
		return round(b1.multiply(b2),scalePub);
	}

	/**
	 * 乘法
	 * @param v1
	 * @param v2
     * @return
     */
	public static BigDecimal mul(BigDecimal v1, BigDecimal v2) {
		return roundBigDecimal(v1.multiply(v2),scalePub);
	}
	
	/**
	 * 乘法
	 * @param v1
	 * @param v2
	 * @param scalePubInput 保留小数位
	 * @return
	 */
	public static String mul(String v1, String v2, int scalePubInput) {
		BigDecimal b1 = new BigDecimal(v1);
		BigDecimal b2 = new BigDecimal(v2);
		return round(b1.multiply(b2),scalePubInput);
	}

	/**
	 * 乘法
	 * @param v1
	 * @param v2
	 * @param scalePubInput 保留小数位
	 * @return
	 */
	public static BigDecimal mul(BigDecimal v1, BigDecimal v2, int scalePubInput) {
		return roundBigDecimal(v1.multiply(v2),scalePubInput);
	}
	
	/**
	 * 除法
	 * 
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static String div(String v1, String v2) {
		return div(v1, v2, scalePub);
	}

	/**
	 * 除法
	 *
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static BigDecimal div(BigDecimal v1, BigDecimal v2) {
		return div(v1, v2, scalePub);
	}

	/**
	 * 除法
	 * @param v1
	 * @param v2
	 * @param scale
	 * @return
	 */
	public static String div(String v1, String v2, int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");

		}
		BigDecimal b1 = new BigDecimal(v1);
		BigDecimal b2 = new BigDecimal(v2);
		return (b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP)).toString();
	}

	/**
	 * 除法
	 * @param v1
	 * @param v2
	 * @param scale
	 * @return
	 */
	public static BigDecimal div(BigDecimal v1, BigDecimal v2, int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");

		}
		return v1.divide(v2, scale, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * 比较大小
	 * @param v1
	 * @param v2
	 * @return   -1 v1<v2  0 v1=v2 1 v1>v2
	 */
	public static int compareTo(String v1, String v2) {
		return new BigDecimal(v1).compareTo(new BigDecimal(v2));
	}

	/**
	 * 比较大小
	 * @param v1
	 * @param v2
	 * @return   -1 v1<v2  0 v1=v2 1 v1>v2
	 */
	public static int compareTo(BigDecimal v1, BigDecimal v2) {
		return v1.compareTo(v2);
	}
	
	/**
	 * 设置double数字保留精度，并将精度后面的数据进行四舍五入
	 * 
	 * @param v
	 * @param scale
	 * @return
	 */
	public static String round(BigDecimal v, int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal one = new BigDecimal("1");
		return (v.divide(one, scale, BigDecimal.ROUND_HALF_UP)).toString();

	}

	public static BigDecimal roundBigDecimal(BigDecimal v, int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The scale must be a positive integer or zero");
		}
		BigDecimal one = new BigDecimal("1");
		return v.divide(one, scale, BigDecimal.ROUND_HALF_UP);

	}

	/**
	 * 格式化数据---去除后边的0
	 * @Title: formatDecimalStr   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param v1
	 * @param: @return      
	 * @return: String
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2015年11月5日 下午12:11:06
	 */
	public static String formatDecimalStr(String v1){
		BigDecimal b1 = new BigDecimal(v1);
		return b1.stripTrailingZeros().toPlainString();
	}
	
	public static void main(String[] args) {
		System.out.println(BigDecimalCalUtil.mul("1","0.006"));
	}
	
}
