package com.cat.common.util;

import java.util.Random;

/**
 * 云信账号,url管理类
 * @ClassName:  YunXinUtil   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: LiTeng  
 * @date:   2016年9月29日 下午2:16:22   
 *
 */
public class YunXinUtil {
	public static final String ContentType = "application/x-www-form-urlencoded;charset=utf-8";

	/**
	 * url
	 */
	//创建云信ID
	public static final String createUser = "https://api.netease.im/nimserver/user/create.action";
	//更新云信ID
	public static final String updateUser = "https://api.netease.im/nimserver/user/update.action";
	//更新并获取新的token
	public static final String refreshToken = "https://api.netease.im/nimserver/user/refreshToken.action";
	//封禁云信ID
	public static final String blockUser = "https://api.netease.im/nimserver/user/block.action";
	//解禁云信ID
	public static final String unblockUser = "https://api.netease.im/nimserver/user/unblock.action";
	//发送普通消息
	public static final String msgSendMsg = "https://api.netease.im/nimserver/msg/sendMsg.action";
	//创建聊天室
	public static final String createChatroom = "https://api.netease.im/nimserver/chatroom/create.action";
	//查看聊天室信息
	public static final String getChatroom = "https://api.netease.im/nimserver/chatroom/get.action";
	//更新聊天室信息
	public static final String updateChatroom = "https://api.netease.im/nimserver/chatroom/update.action";
	//修改聊天室开/关闭状态
	public static final String toggleCloseStat = "https://api.netease.im/nimserver/chatroom/toggleCloseStat.action";
	//设置聊天室内用户角色
	public static final String setMemberRole = "https://api.netease.im/nimserver/chatroom/setMemberRole.action";
	//往聊天室内发消息
	public static final String chatroomSendMsg = "https://api.netease.im/nimserver/chatroom/sendMsg.action";
	//将聊天室内成员设置为临时禁言
	public static final String temporaryMute = "https://api.netease.im/nimserver/chatroom/temporaryMute.action";
	//往聊天室内添加机器人
	public static final String addRobot = "https://api.netease.im/nimserver/chatroom/addRobot.action";
	//从聊天室内删除机器人
	public static final String removeRobot = "https://api.netease.im/nimserver/chatroom/removeRobot.action";
	
	public static String Nonce() {
		int[] array = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		Random rand = new Random();
		for (int i = 10; i > 1; i--) {
			int index = rand.nextInt(i);
			int tmp = array[index];
			array[index] = array[i - 1];
			array[i - 1] = tmp;
		}
		int result = 0;
		for (int i = 0; i < 8; i++)
			result = result * 10 + array[i];
		return String.valueOf(result);
	}
}
