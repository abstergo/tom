package com.cat.common.service;


import com.cat.common.entity.Page;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Map;

/**
 * Created by Administrator on 2015/5/28.
 */
public interface PageService<T> {
        public  Class<T> getParameterizedType();
        public Page<T> getpageBySingeleTable(Page<T> page, String sql, Map<String, Object> searchinfo);

    public Page<T> getPageByDateSourceSingeleTable(Page<T> page, String sql, Map<String, Object> searchinfo, JdbcTemplate jdbcTemplate);
    public Page<T> findPage(Page<T> page, String sql, Object... params);
    public Page<T> findPageOnGroupOrder(Page<T> page, String sql, Object... params);
}
