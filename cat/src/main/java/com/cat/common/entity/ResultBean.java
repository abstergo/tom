package com.cat.common.entity;

import com.alibaba.fastjson.JSON;
import com.cat.common.meta.ResultCode;

import java.io.Serializable;
import java.util.ArrayList;

/**
* @Title: ResultBean.java 
* @Package mob.interfaces.util 
* @Description:  返回的结果集
* @author 王鹏焱
* @date 2016年7月19日 
* @version V1.0   
*/
public class ResultBean<T> implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer code;

	private String msg="";

	private T data;


	public ResultBean() {
	}

	public ResultBean(Integer code, String msg, T data) {
		this.setCode(code);
		this.setMsg(msg);
		this.setData(data);
	}

	public ResultBean(ResultCode resultCode, T data) {
		this.setCode(resultCode.get_code());
		this.setMsg(resultCode.getMsg());
		this.setData(data);
	}

	/**
	 * 无结果／错误信息返回
	 * @param code
	 * @param msg
	 */
	public ResultBean(Integer code, String msg) {
		this.setCode(code);
		this.setMsg(msg);
	}

	/**
	 * 无结果／错误信息返回
	 * @param resultCode
	 */
	public ResultBean(ResultCode resultCode) {
		this.setCode(resultCode.get_code());
		this.setMsg(resultCode.getMsg());
	}

	/**
	 * 无结果／错误信息返回
	 * @param resultCode
	 */
	public void setResult(ResultCode resultCode) {
		this.setCode(resultCode.get_code());
		this.setMsg(resultCode.getMsg());
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
