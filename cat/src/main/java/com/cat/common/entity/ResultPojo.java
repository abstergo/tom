package com.cat.common.entity;

import com.cat.common.meta.ResultCode;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nigle on 2016/10/18.
 */
public class ResultPojo<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer code;

    private String msg="";

    private List<T> data;

    public ResultPojo() {
    }
    public ResultPojo(Integer code, String msg, List<T> data) {
        this.setCode(code);
        this.setMsg(msg);
        this.setData(data);
    }
    public ResultPojo(ResultCode resultCode, List<T> data) {
        this.setCode(resultCode.get_code());
        this.setMsg(resultCode.getMsg());
        this.setData(data);
    }

    /**
     * 无结果／错误信息返回
     * @param resultCode
     */
    public ResultPojo(ResultCode resultCode) {
        this.setCode(resultCode.get_code());
        this.setMsg(resultCode.getMsg());
        this.setData(new ArrayList<T>());
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
