package com.cat.common.entity;

/**
 * 微信用户同意授权，获取code的参数bean
 * Created by LiYaguang on 2016/1/27.
 */
public class WXOAuthCode {

    private String appId;

    private String redirectUrl;

    private String state = "STATE";

    private String scope = "snsapi_base";

    private String wxOauthDomain;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getWxOauthDomain() {
        return wxOauthDomain;
    }

    public void setWxOauthDomain(String wxOauthDomain) {
        this.wxOauthDomain = wxOauthDomain;
    }
}
