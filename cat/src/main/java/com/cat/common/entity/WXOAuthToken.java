package com.cat.common.entity;

/**
 * 微信通过code换取网页授权token的参数bean
 * Created by LiYaguang on 2016/1/27.
 */
public class WXOAuthToken {

    private String appId;

    private String code;

    private String secret;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
