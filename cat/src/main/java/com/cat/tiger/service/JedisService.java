package com.cat.tiger.service;

import com.cat.common.entity.Page;
import redis.clients.jedis.Jedis;
import redis.clients.util.Pool;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface JedisService {

    public abstract Long del(String key) throws Exception;

    public abstract Object get(String key) throws Exception;

    public abstract Pool<Jedis> getJedisPool() throws Exception;

    public abstract String getStr(String key) throws Exception;

    public Long hdel(final String key, final String field) throws Exception;

    public Object hget(String key, String field) throws Exception;

    public Map<byte[], byte[]> hgetAll(String key) throws Exception;

    public Long hset(final String key, final String field, final Object value, int seconds) throws Exception;

    public Long hlen(final String key) throws Exception;

    public abstract boolean isExist(String key) throws Exception;

    public Object lindex(String key, int index) throws Exception;

    public Object lpop(String key) throws Exception;

    public abstract void lpush(String key, Object value) throws Exception;

    public List<Object> lrange(String key, int start, int end) throws Exception;

    public void lset(String key, int index, Object value) throws Exception;

    public abstract Long rpush(String key, Object value) throws Exception;

    public abstract void set(String key, Object value) throws Exception;

    public abstract void set(String key, Object value, int seconds) throws Exception;

    public abstract void setJedisPool(Pool<Jedis> jedisPool) throws Exception;

    public void setStr(String key, String value) throws Exception;

    public String setexStr(String key, String value, int seconds) throws Exception;

    public abstract Long zadd(String key, double score, Object member) throws Exception;

    public Long zadd2(String key, double score, long member) throws Exception;

    Long zaddStr(String key, double score, String member) throws Exception;

    public abstract Long zcard(String key) throws Exception;

    public Long zcount(String key, double min, double max) throws Exception;

    public Long zrank(String key, Object member) throws Exception;

    public abstract Long zrem(String key, Object member) throws Exception;

    Long zremrangeByRank(String key, long start, long end) throws Exception;

    public abstract List<Object> zrevrange(String key, int start, int end) throws Exception;

    public abstract List<Long> zrevrange2(String key, int start, int end) throws Exception;

    List<Long> zrevrange3(String key, int start, int end) throws Exception;

    List<String> zrevrange4(String key, int start, int end) throws Exception;

    public Long zrevrank(String key, Object member) throws Exception;

    Long zrevrank2(String key, String member) throws Exception;

    public long incr(String key) throws Exception;

    public long incrBy(String key, long integer) throws Exception;

    public long incrBy2(String key, long integer) throws Exception;

    public long expire(String key, int seconds) throws Exception;

    public String spop(String key) throws Exception;

    public long decr(String key) throws Exception;

    public long decrBy(String key, long integer) throws Exception;

    Long sadd(String key, String... member) throws Exception;

    public Set<String> keys(String key) throws Exception;

    public Long rpush(String key, String... strings) throws Exception;

    public String lpopStr(String key) throws Exception;

    public Long hset(String key, String field, String value) throws Exception;

    public Boolean hexists(String key, String field) throws Exception;

    public Long hdel(String key, String... fields) throws Exception;

    public Long llen(String key) throws Exception;

    public List<Object> zrange(String key, long start, long end) throws Exception;

    public Long lrem(String key, long count, String value) throws Exception;

    public Double zscore(String key, Object member) throws Exception;

    public Double zincrby(String key, double score, Object member) throws Exception;

    Double zincrby2(String key, long score, String member) throws Exception;

    public Map<String, String> hgetAllStr(String key) throws Exception;

    public Long hsetStr(String key, String field, String value, int seconds) throws Exception;

    public String hgetStr(String key, String field) throws Exception;

    public Long hsetStr(String key, String field, String value) throws Exception;

    boolean cacheObjectWithSort(String key, Object id, Map<String, String> object, double score, Integer expiredTime) throws Exception;

    <T> Page<T> getObjectByPage(String key, Page<T> page) throws Exception;

    List<String> hvals(String key) throws Exception;

    Set<String> smembers(String key) throws Exception;

    List<Object> zrangeBySore(String key, double startScore, double endScore) throws Exception;

    public void ltrim(String key, int start, int end) throws Exception;
}
