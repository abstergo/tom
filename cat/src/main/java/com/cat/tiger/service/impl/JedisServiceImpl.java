package com.cat.tiger.service.impl;

import com.cat.common.constant.RedisKeyConstant;
import com.cat.common.entity.Page;
import com.cat.tiger.service.JedisService;
import com.cat.tiger.util.ByteArrayHelper;
import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import redis.clients.jedis.Jedis;
import redis.clients.util.Pool;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class JedisServiceImpl implements JedisService {
    private Logger logger = LoggerFactory.getLogger(JedisServiceImpl.class);

    protected Pool<Jedis> jedisPool;

    @Override
    public Long del(String key) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.del(key);
        } catch (Exception e) {
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Object get(String key) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return ByteArrayHelper.byte2Object(jedis.get(key.getBytes()));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Pool<Jedis> getJedisPool() throws Exception {
        return jedisPool;
    }

    @Override
    public String getStr(String key) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.get(key);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Long hdel(String key, String field) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.hdel(key, field);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Object hget(String key, String field) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return ByteArrayHelper.byte2Object(jedis.hget(key.getBytes(), field.getBytes()));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Map<byte[], byte[]> hgetAll(String key) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.hgetAll(key.getBytes());
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Long hset(String key, String field, Object value, int seconds) throws Exception {

        Jedis jedis = getJedisPool().getResource();
        try {
            Long ret = jedis.hset(key.getBytes(), field.getBytes(), ByteArrayHelper.object2Bytes(value));
            if (seconds > 0) {
                jedis.expire(key, seconds);
            }
            return ret;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    public Long hlen(final String key) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.hlen(key);
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public boolean isExist(String key) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.exists(key);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return false;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Object lindex(String key, int index) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return ByteArrayHelper.byte2Object(jedis.lindex(key.getBytes(), index));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Object lpop(String key) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            Object object = ByteArrayHelper.byte2Object(jedis.lpop(key.getBytes()));
            return object;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public String spop(String key) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return new String(jedis.spop(key.getBytes()));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Long sadd(String key, String... member) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.sadd(key, member);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public void lpush(String key, Object value) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            jedis.lpush(key.getBytes(), ByteArrayHelper.object2Bytes(value));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public List<Object> lrange(String key, int start, int end) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        List<Object> ret = new ArrayList<Object>();
        try {
            List<byte[]> list = jedis.lrange(key.getBytes(), start, end);
            for (byte[] i : list) {
                ret.add(ByteArrayHelper.byte2Object(i));
            }
            return ret;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public void lset(String key, int index, Object value) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            jedis.lset(key.getBytes(), index, ByteArrayHelper.object2Bytes(value));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Long rpush(String key, Object value) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.rpush(key.getBytes(), ByteArrayHelper.object2Bytes(value));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }

    }

    @Override
    public void set(String key, Object value) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            jedis.set(key.getBytes(), ByteArrayHelper.object2Bytes(value));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            getJedisPool().returnResource(jedis);
        }

    }

    @Override
    public void set(String key, Object value, int seconds) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            jedis.setex(key.getBytes(), seconds, ByteArrayHelper.object2Bytes(value));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public void setJedisPool(Pool<Jedis> jedisPool) throws Exception {
        this.jedisPool = jedisPool;
    }

    @Override
    public void setStr(String key, String value) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            jedis.set(key, value);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public String setexStr(String key, String value, int seconds) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        String res = null;
        try {
            res = jedis.setex(key, seconds, value);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            throw e;
        } finally {
            getJedisPool().returnResource(jedis);
        }
        return res;
    }

    @Override
    public Long zadd(String key, double score, Object member) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zadd(key.getBytes(), score, ByteArrayHelper.object2Bytes(member));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long zadd2(String key, double score, long member) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zadd(key.getBytes(), score, ByteArrayHelper.longToByteArray(member));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long zaddStr(String key, double score, String member) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zadd(key.getBytes(), score, member.getBytes());
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }


    @Override
    public Long zcard(String key) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zcard(key.getBytes());
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long zcount(String key, double min, double max) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zcount(key.getBytes(), min, max);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long zrank(String key, Object member) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zrank(key.getBytes(), ByteArrayHelper.object2Bytes(member));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long zrem(String key, Object member) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zrem(key.getBytes(), ByteArrayHelper.object2Bytes(member));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long zremrangeByRank(String key, long start, long end) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zremrangeByRank(key.getBytes(), start, end);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public List<Object> zrevrange(String key, int start, int end) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        List<Object> returnSet = new ArrayList<Object>();
        try {
            Set<byte[]> set = jedis.zrevrange(key.getBytes(), start, end);
            for (byte[] s : set) {
                returnSet.add(ByteArrayHelper.byte2Object(s));
            }
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
        return returnSet;
    }

    @Override
    public List<Long> zrevrange3(String key, int start, int end) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        List<Long> returnSet = new ArrayList<>();
        try {
            Set<byte[]> set = jedis.zrevrange(key.getBytes(), start, end);
            for (byte[] s : set) {
                returnSet.add(Long.valueOf(ByteArrayHelper.byteArrayToLong(s)));
            }
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
        return returnSet;
    }

    @Override
    public List<Long> zrevrange2(String key, int start, int end) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        List<Long> returnSet = new ArrayList<Long>();
        try {
            Set<byte[]> set = jedis.zrevrange(key.getBytes(), start, end);
            for (byte[] s : set) {
                returnSet.add(Long.valueOf(ByteArrayHelper.byte2Object(s).toString()));
            }
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
        return returnSet;
    }


    @Override
    public List<String> zrevrange4(String key, int start, int end) throws Exception{
        Jedis jedis = getJedisPool().getResource();
        List<String> returnSet = new ArrayList<>();
        try {
            Set<byte[]> set = jedis.zrevrange(key.getBytes(), start, end);
            for (byte[] s : set) {
                returnSet.add(String.valueOf(s));
            }
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
        return returnSet;
    }

    @Override
    public Long zrevrank(String key, Object member) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zrevrank(key.getBytes(), ByteArrayHelper.object2Bytes(member)) == null ? -1 : jedis.zrevrank(
                    key.getBytes(), ByteArrayHelper.object2Bytes(member));
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long zrevrank2(String key, String member) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            Long position = jedis.zrevrank(key.getBytes(), member.getBytes());
            return position == null ? -1 : position;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public long incr(String key) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.incr(key);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public long incrBy(String key, long integer) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.incrBy(key, integer);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public long incrBy2(String key, long integer) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.incrBy(key, integer);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            throw e;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public long expire(String key, int seconds) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.expire(key, seconds);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    public long decr(String key) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.decr(key);
        } catch (Exception e) {
            logger.error("redis incrBy err：", e);
            return 0;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    /**
     * REDIS自减指定值
     *
     * @param integer
     * @return
     */
    public long decrBy(String key, long integer) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.decrBy(key, integer);
        } catch (Exception e) {
            logger.error("redis incrBy err：", e);
            return 0;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Set<String> keys(String key) throws Exception {
        // TODO Auto-generated method stub
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.keys(key);
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return null;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long rpush(String key, String... strings) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.rpush(key, strings);
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public String lpopStr(String key) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.lpop(key);
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return null;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long hset(String key, String field, String value) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.hset(key, field, value);
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Boolean hexists(String key, String field) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.hexists(key, field);
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return false;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long hdel(String key, String... fields) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.hdel(key, fields);
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long llen(String key) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.llen(key);
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public List<Object> zrange(String key, long start, long end) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        List<Object> ret = new ArrayList<>();
        try {
            Set<byte[]> set = jedis.zrange(key.getBytes(), start, end);
            for (byte[] i : set) {
                ret.add(ByteArrayHelper.byte2Object(i));
            }
            return ret;
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return null;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }


    @Override
    public Long lrem(String key, long count, String value) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.lrem(key.getBytes(), count, ByteArrayHelper.object2Bytes(value));
//            return jedis.lrem(key,count,value);
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Double zscore(String key, Object member) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zscore(key.getBytes(), ByteArrayHelper.object2Bytes(member));
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return -1d;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Double zincrby(String key, double score, Object member) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zincrby(key.getBytes(), score, ByteArrayHelper.object2Bytes(member));
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return -1d;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Double zincrby2(String key, long score, String member) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        try {
            return jedis.zincrby(key.getBytes(), score, member.getBytes());
        } catch (Exception e) {
            logger.error("redis keys err：", e);
            return -1d;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Map<String, String> hgetAllStr(String key) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.hgetAll(key);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Long hsetStr(String key, String field, String value, int seconds) throws Exception {

        Jedis jedis = getJedisPool().getResource();
        try {
            long ret = jedis.hset(key, field, value);
            if (seconds > 0) {
                jedis.expire(key, seconds);
            }
            return ret;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public Long hsetStr(String key, String field, String value) throws Exception {

        Jedis jedis = getJedisPool().getResource();
        try {
            long ret = jedis.hset(key, field, value);
            return ret;
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return -1l;
        } finally {
            getJedisPool().returnResource(jedis);
        }
    }

    @Override
    public boolean cacheObjectWithSort(String key, Object id, Map<String, String> object, double score, Integer expiredTime) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        boolean result = false;
        try {
            //Transaction trans = jedis.multi();
            jedis.hmset(key + RedisKeyConstant.KEY_RECORD + id, object);
            jedis.zadd(key + RedisKeyConstant.KEY_ACTIVE_LIST, score, id.toString());
            if (expiredTime != null && expiredTime > 0) {
                jedis.expire(key + RedisKeyConstant.KEY_RECORD + id.toString(), expiredTime);
                jedis.expire(key + RedisKeyConstant.KEY_ACTIVE_LIST, expiredTime);
            }
            //List<Object> list = trans.exec();
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("缓存对象出现异常", e);
            result = false;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
        return result;

    }

    public <T> Page<T> getObjectByPage(String key, Page<T> page) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        List transResult = Lists.newArrayList();
        try {
            int pageNo = page.getPageNo();
            if (pageNo <= 0) {
                pageNo = 1;
            }
            int pageSize = page.getPageSize();
            int start = (pageNo - 1) * pageSize;
            int end = pageNo * pageSize - 1;
            Set<String> idSet = jedis.zrevrange(key + RedisKeyConstant.KEY_ACTIVE_LIST, start, end);
            if (CollectionUtils.isEmpty(idSet)) {
                return page;
            }
            // 取出所有记录
            //Transaction trans = jedis.multi();
            for (String cacheId : idSet) {
                Map map = jedis.hgetAll(key + RedisKeyConstant.KEY_RECORD + cacheId);
                transResult.add(map);
            }
            if (CollectionUtils.isEmpty(transResult)) {
                return page;
            }

            if (page.getTotalItems() <= 0 && page.getPageSize() > 0) {
                // 获取记录数
                int count = this.getCacheCount(key);
                page.setTotalItems(count);
            }
            page.setResult(transResult);

        } catch (Exception e) {
            logger.error("分页获取缓存记录异常", e);
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
        return page;
    }

    public int getCacheCount(String key) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        int count = 0;
        try {
            Set<String> activeSet = jedis.zrange(key + RedisKeyConstant.KEY_ACTIVE_LIST, 0, -1);
            if (activeSet != null) {
                count = activeSet.size();
            }
        } catch (Exception e) {
            logger.error("获取缓存记录数异常", e);
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
        return count;
    }

    @Override
    public String hgetStr(String key, String field) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.hget(key, field);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public List<String> hvals(String key) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.hvals(key);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
            return null;
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

    @Override
    public Set<String> smembers(String key) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            return jedis.smembers(key);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public List<Object> zrangeBySore(String key, double startScore, double endScore) throws Exception {
        Jedis jedis = getJedisPool().getResource();
        List<Object> returnSet = Lists.newArrayList();
        try {
            Set<byte[]> set = jedis.zrangeByScore(key.getBytes(), startScore, endScore);
            for (byte[] s : set) {
                returnSet.add(ByteArrayHelper.byte2Object(s));
            }
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
        return returnSet;
    }

    @Override
    public void ltrim(String key, int start, int end) throws Exception {
        Jedis jedis = null;
        try {
            jedis = getJedisPool().getResource();
            jedis.ltrim(key, start, end);
        } catch (Exception e) {
            logger.error(e.getLocalizedMessage());
        } finally {
            if (jedis != null) {
                getJedisPool().returnResource(jedis);
            }
        }
    }

}
