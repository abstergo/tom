package com.cat.tiger.util;

/**
 * 全局常量
 * <p>
 * 全局统一的常量在这里配置
 * <p>
 * 注:使用interface有两个考虑:
 * <ul>
 * <li>这个类不需要实例化;</li>
 * <li>内部属性默认就是 <code>public static final</code>,不需要再手动添加了.</li>
 * </ul>
 *
 * @author diguage
 * @since 2016-01-23
 */
public interface GlobalConstants {

    /**
     * 用户密码加密后缀
     */
    String PASSWORD_SUFFIX = "g7ZOWGgg6J@heFanTV";

    int PAGE_SIZE = 20;
    /**
     * 是否删除标识, 0表示正常, 1表示已删除
     */
    int DELETE_FLAG_YES = 1;
    /**
     * 是否删除标识, 0表示正常, 1表示已删除
     */
    int DELETE_FLAG_NO = 0;
    /**
     * 接口返回成功消息文字
     */
    String SUCCESS_MSG = "成功";
    /**
     * 接口返回失败消息文字
     */
    String FAIL_MSG = "失败";

    /**
     * 接口返回成功编码
     */
    int SUCCESS_CODE = 1000;
    /**
     * 接口返回失败编码
     */
    int FAIL_CODE = 4000;

    /**
     * 接口返回参数错误
     */
    int FAIL_PARAM_CODE = 4100;

    //'0直播间 1俱乐部 2 私信 3 弹幕'
    int SOURCE_TYPE_LIVE = 0;
    int SOURCE_TYPE_CLUB = 1;
    int SOURCE_TYPE_MESSAGE = 2;
    int SOURCE_TYPE_BARRAGE = 3;

    //礼物类型 0：小礼物 1：大礼物 2：世界礼物'
    int PRESENT_TYPE_SMALL = 0;
    int PRESENT_TYPE_BIG = 1;
    int PRESENT_TYPE_WORLD = 2;
    int PRESENT_TYPE_RED = 4;
    //可连送0  不可连送
    int PRESENT_ISCONTINUE = 0;
    int PRESENT_ISNOTCONTINUE = 1;

    //真饭票 1 假饭票0
    int REAL_FANPIAO = 1;
    int FALSE_FANPIAO = 0;

    /**
     * 直播异常关闭
     */
    int ERR_LIVE_END = 1;

    /**
     * 主播直播状态
     */
    int AUTHOR_LIVING = 1;
    int AUTHOR_LIVEEND = 0;

    /**
     * 主播禁播状态
     */
    int AUTHOR_LOCK = 1;
    int AUTHOR_UNLOCK = 0;

    //1 动态举报 2 评论举报 3 举报主播 4 举报观看直播用户
    int REPORT_TYPE_DYNAMIC = 1;
    int REPORT_TYPE_COMMENT = 2;
    int REPORT_TYPE_ANCHOR = 3;
    int REPORT_TYPE_VIEWER = 4;

    //兑换规则
    int EX_RULE_HEFAN_FANPIAO = 0;
    int EX_RULE_HEFAN_RMB = 1;
    int EX_RULE_FANPIAO_HEFAN = 2;
    int EX_RULE_GONGGONG = 3;
    int EX_RULE_PC = 4;
    int EX_RULE_ANDRIOD = 5;
    int EX_RULE_IOS = 6;
    int EX_RULE_FANPIAO_RMB = 7;

    /**
     * redis setex 操作成功标识
     */
    String REDIS_SETEX_SUCCESS = "OK";

    /**
     * OSS对应图片Bucket管理
     */
    String PIC_OSS_BUCKET = "hefanimage";

    /**
     * OSS对视频Bucket管理
     */
    String VIDEO_OSS_BUCKET = "hefanvideo";

    //用户类型
    int USER_TYPE_USER = 0;
    int USER_TYPE_FAMOUS = 1;
    int USER_TYPE_STAR = 2;
    int USER_TYPE_SITE = 3;
    int USER_TYPE_INNER = 4;
    int USER_TYPE_BROKER = 5;
    int USER_TYPE_FLASE = 6;
    int USER_TYPE_ROBOT = 7;

    /**
     * 消息详情Redis_Key
     */
    String MESSAGE_GETMSGDETAIL_KEY = "message_getMsgDetail_";
    int MESSAGE_GETMSGDETAIL_CACHE_TIME = 60 * 60;// 1hour

    /**
     * 支付默认商品名
     */
    String PAY_PRODUCT_NAME = "饭票";
    /**
     * 微信支付下单需要使用的商品id
     */
    String PAY_PRODUCT_ID = "fanpiao10001";

    /**
     * 微信支付成功字符串
     */
    String WECHAT_PAY_SUCCESS_CODE = "SUCCESS";
    /**
     * 微信支付回调通知返回成功串
     */
    String WECHAT_PAY_NOTIFY_RETURN_SUCCESS = "SUCCESS";
    /**
     * 微信支付回调通知返回失败串
     */
    String WECHAT_PAY_NOTIFY_RETURN_FAIL = "FAIL";

    int PAY_SOURCE_ALIPAY = 1;
    int PAY_SOURCE_WECHATPAY = 2;
    int PAY_SOURCE_APPLEPAY = 3;
    int PAY_SOURCE_RECHARECODE = 4;
    int PAY_SOURCE_MISSION = 5;
    int PAY_SOURCE_BACK = 6;
    int PAY_SOURCE_REDPACK = 7;

    /**
     * 开播提醒
     */
    int OPRN_REMIND = 1;
    int CLOSE_REMIND = 0;

    //每天超过3小时就不加经验了
    int EXP_WATCH_TIME_LIMIT = 3;
    //观看1分钟增长10经验
    int EXP_WATCH_TIME_EXP = 10;

    /**
     * 盒饭官方号ID
     */
    String HEFAN_OFFICIAL_ID = "100000001";

    /**
     * 关注数rediskey
     */
    String WATCH_NUM_REDIS_KEY = "watch_num_redis_key";

    /**
     * 粉丝数rediskey
     */
    String FANS_NUM_REDIS_KEY = "fans_num_redis_key";
    /**
     * 额外粉丝数
     */
    String EXTRA_FANS_NUM_REDIS_KEY = "extra_fans_num_redis_key";

    /**
     * 禁言时间
     */
    int ROOM_SHUTUP_TIME = 12;

    /**
     * 禁言用户信息
     * key：room_shutup_liveUuid_user_id
     * value:RoomShutup的json
     */
    String ROOM_SHUTUP_KEY = "room_shutup_";
    int ROOM_SHUTUP_KEY_CACHE_TIME = 12 * 60 * 60;
    /**
     * 默认location
     */
    String DEFAULT_LOCATION = "异次元";

    // IM Message
    /**
     * IM用户列表消息发送Redis_Key
     */
    String IM_USER_LIST_KEY = "im_user_list_";

    /**
     * IM用户特效消息发送Redis_Key
     */
    String IM_SPECIAL_EFFECTS_LIST_KEY = "im_special_effects_list_";

    /**
     * 开播Token
     */
    String LIVE_TOKEN = "live_token_userid_";
    int LIVE_TOKEN_CACHE_TIME = 60 * 10;

    /**
     * 直播预约发推送时间[开播前x分钟]
     */
    Integer LIVE_BOOK_TIME = 30;

    /**
     * 直播预约定时推送任务执行间隔[每隔x分钟执行一次]
     */
    Integer LIVE_BOOK_TASK_TIME = 5;

    /**
     * 未预约
     */
    Integer LIVE_BOOK_STATUS_NOBOOK = 0;
    /**
     * 已预约
     */
    Integer LIVE_BOOK_STATUS_ISBOOK = 1;
    /**
     * 已结束
     */
    Integer LIVE_BOOK_STATUS_ENDBOOK = 2;
    /**
     * 预约直播不存在
     */
    Integer LIVE_BOOK_STATUS_NOTEXISTBOOK = 3;
    /**
     * 预约直播开播时间不存在
     */
    Integer LIVE_BOOK_STATUS_STARTIMRNOTEXISTENDBOOK = 4;


    /** 直播间相关redis操作ksy -Start **/

    /**
     * 正在直播的直播信息
     * key：living_'userId'
     * value：直播间信息
     */
    String LIVING_USER_KEY = "living_";
    int LIVING_USER_KEY_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 开播暂存入库的liveLog数据
     * key：live_log_data_anthId
     * value:liveLog
     */
    String LIVE_LOG_DATA = "live_log_data_";
    int LIVE_LOG_DATA_CACHE_TIME = 60 * 10;//10min

    /**
     * 开播暂存入库的liveRoom数据
     * key：live_room_data_anthId
     * value:liveRoom
     */
    String LIVE_ROOM_DATA = "live_room_data_";
    int LIVE_ROOM_DATA_CACHE_TIME = 60 * 10;//10min

    /**
     * 开播暂存直播信息
     * key：live_info_data_anthId
     * value:LivingRoomInfoVo
     */
    String LIVE_INFO_DATA = "live_info_data_";
    int LIVE_INFO_DATA_CACHE_TIME = 60 * 10;//10min

    /**
     * 正在直播的直播间列表Hash
     * keyName：living_room_hash
     * field：authId
     * value：直播间信息
     */
    String LIVING_ROOM_HASH_KEY = "living_room_hash";

    /**
     * 关播后DB操作失败的直播Hash
     * keyName：living_fail_room_hash
     * field：authId
     * value：直播信息liveLog
     */
    String LIVING_FAIL_ROOM_HASH_KEY = "living_fail_room_hash";

    /**
     * 用户进入直播间检查
     * keyName：living_check_enter_hash
     * field：userId
     * value：{"userId":"0","deviceToken":"1","authId":"2","chatRoomId":"3","liveUuid":"4"}
     */
    String LIVING_CHECK_ENTER_HASH_KEY = "living_check_enter_hash";

    /**
     * 直播间用户列表Hash
     * keyName：living_room_user_hash_authId
     * field：userId
     * value：用户信息
     */
    String LIVING_ROOM_USER_HASH_KEY = "living_room_user_hash_";
    int LIVING_ROOM_USER_HASH_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 直播间用户列表SortedSet
     * keyName：living_room_user_sorted_authId
     * score：排序分值
     * value：userId
     */
    String LIVING_ROOM_USER_SORTED_KEY = "living_room_user_sorted_";
    int LIVING_ROOM_USER_SORTED_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 最後進入直播間的時間
     * keyName：join_room_last_time_authId
     * value：System.currentTimeMillis();
     */
    String JOIN_ROOM_LAST_TIME = "join_room_last_time_";
    int JOIN_ROOM_LAST_TIME_CACHE_TIME = 60 * 60;// 1day

    /**
     * 直播间机器人缓存
     * keyName：living_room_robot_list_authId
     * value：robot id
     */
    String LIVING_ROOM_ROBOT_LIST_KEY = "living_room_robot_list_";
    int LIVING_ROOM_ROBOT_LIST_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 直播间机器人缓存
     * keyName：living_room_robot_hashset_authId
     * filed：robot id
     * value：robotInfo
     */
    String LIVING_ROOM_ROBOT_HASHSET_KEY = "living_room_robot_hashset_";

    /**
     * 所有机器人缓存
     * keyName：living_robot_list
     * value：robot id
     */
    String LIVING_ROBOT_LIST_KEY = "living_robot_list";

    /**
     * 所有机器人缓存
     * keyName：living_robot_hashset
     * filed：robot id
     * value：robotInfo
     */
    String LIVING_ROBOT_HASHSET_KEY = "living_robot_hashset";

    /**
     * 机器人所在位置
     *  keyName：living_robot_location_hashset
     *  filed：robot id
     *  value：anchId
     */
    String LIVING_ROBOT_LOCATION_HASHSET = "living_robot_location_hashset";

    /**
     * 直播间第一页用户列表Redis_Key
     * key：live_room_home_page_user_list_'liveUuid'
     * value：第一页用户列表
     */
    String LIVE_ROOM_HOME_PAGE_USER_LIST_KEY = "live_room_home_page_user_list";
    int LIVE_ROOM_HOME_PAGE_USER_LIST_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 直播间心跳Redis_Key
     * key：live_room_heart_beat_'liveUuid'
     * value：心跳信息
     */
    String LIVE_ROOM_HEART_BEAT_KEY = "live_room_heart_beat_";
    int LIVE_ROOM_HEART_BEAT_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 直播间观看人次
     * key：living_watch_num_'liveUuid'
     * value：观看人次
     */
    String LIVING_WATCH_NUM_KEY = "living_watch_num_";
    int LIVING_WATCH_NUM_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 禁止进入直播间
     * key：is_forbidden_enter_'liveUuid'_'userId'
     * value：RoomOuter json
     */
    String IS_FORBIDDEN_ENTER_KEY = "is_forbidden_enter_";
    int IS_FORBIDDEN_ENTER_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 直播间排队等待队列
     * key：living_waiting_queue_userId_chatRoomId_'liveUuid'
     * value：userId
     */
    String LIVING_WAITING_QUEUE_KEY = "living_waiting_queue_";
    int LIVING_WAITING_QUEUE_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 直播间排队可进入队列
     * key：living_prepare_queue_userId_chatRoomId_'liveUuid'
     * field：userId
     * value：userId
     */
    String LIVING_PREPARE_QUEUE_KEY = "living_prepare_queue_";
    int LIVING_PREPARE_QUEUE_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 直播间VIP排队可进入队列
     * key：living_vip_prepare_queue_userId_chatRoomId_'liveUuid'
     * field：userId
     * value：userId
     */
    String LIVING_VIP_PREPARE_QUEUE_KEY = "living_vip_prepare_queue_";
    int LIVING_VIP_PREPARE_QUEUE_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 额外人数
     * key author_add_number_anchid
     */
    String AUTHOR_ADD_NUMBER = "author_add_number_%s";
    int AUTHOR_ADD_NUMBER_CACHE_TIME = 24 * 60 * 60;// 1day

    /**
     * 直播间真实用户数量
     * key author_real_number_anchid
     */
    String AUTHOR_REAL_NUMBER = "author_real_number_%s";

    //废弃：
    String LIVING_ROOM_ROBOT_BACK_LIST_KEY = "";

    /**
     * 直播间真人人数
     * live_room_real_people_count_liveUuid
     */
    String LIVE_ROOM_REAL_PEOPLE_COUNT = "live_room_real_people_count_%s";

    /**
     * 直播间相关redis操作ksy -End
     **/


     /*
     * 动态常量
     */
    String DYNAMIC_WORDS = "1";
    String DYNAMIC_PICTURE = "2";
    String DYNAMIC_VIDEOS = "3";
    String DYNAMIC_ISSYNC = "1";
    String DYNAMIC_NOSYNC = "0";

    String DYNAMIC_COMMENTSCOUT = "1";
    String DYNAMIC_PRESENTCOUNT = "2";
    String DYNAMIC_PRAISECOUNT = "3";

    /**
     * IM发送限制
     */
    String IM_SEND_CHECK_LITTLE_PRESENT = "im_send_check_little_present_%s";
    String IM_SEND_CHECK_HEFAN_NUM = "im_send_check_hefan_num_%s";
    String IM_SEND_CHECK_LIGHT = "im_send_check_light_%s";
    String IM_SEND_CHECK_ATTENTION = "im_send_check_attention_%s";
    String IM_SEND_CHECK_BARRAGE = "im_send_check_barrage_%s";
    String IM_SEND_CHECK_SPECIAL_EFFICIENCY = "im_send_check_special_efficiency_%s";
    String IM_SEND_CHECK_NORMAL_USER_ENTER = "im_send_check_normal_user_enter_%s";

}
