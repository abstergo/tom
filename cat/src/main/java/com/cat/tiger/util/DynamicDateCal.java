package com.cat.tiger.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DynamicDateCal {

	public static String getDaysBeforeNow(Date date) {
		long sysTime = Long.parseLong(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		long ymdhms = Long.parseLong(new SimpleDateFormat("yyyyMMddHHmmss").format(date));
		String strYear = "年前";
		String strMonth = "月前";
		String strDay = "天前";
		String yesDay = "昨天";
		String strHour = "小时前";
		String strMinute = "分钟前";
		try {
			if (ymdhms == 0) {
				return "";
			}
			long between = (sysTime / 10000000000L) - (ymdhms / 10000000000L);
			if (between > 0) {
				return String.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
			}
			between = (sysTime / 100000000L) - (ymdhms / 100000000L);
			if (between > 0) {
				return  String.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
			}
			between = (sysTime / 1000000L) - (ymdhms / 1000000L);
			if (between == 1) {
				return yesDay+String.valueOf(new SimpleDateFormat(" HH:mm:ss").format(date));
			}
			if (between > 0) {
				return String.valueOf(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
			}

			between = (sysTime / 10000) - (ymdhms / 10000);
			if (between > 0) {
				return between + strHour;
			}
			between = (sysTime / 100) - (ymdhms / 100);
			if (between > 0) {
				return between + strMinute;
			}
			return "1" + strMinute;
		} catch (Exception e) {
			return "";
		}
	}
}
