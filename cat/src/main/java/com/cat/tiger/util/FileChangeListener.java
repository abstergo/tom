package com.cat.tiger.util;

/**
 * @author ripflowers
 */
public interface FileChangeListener {
    public void fileChanged(String filename);
}
