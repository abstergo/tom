package com.cat.tiger.util;
/**
 * 简单分数计算类
 * @author sagagyq
 *
 */
public class Fraction {
	/**
	 * 分子
	 */
	private int numerator;
	/**
	 * 分母
	 */
	private int denominator;
	
	public Fraction(int numerator,int denominator){
		this.numerator = numerator;
		this.denominator = denominator;
		yueFen();
	}
	
	public int getNumerator() {
		return numerator;
	}
	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}
	public int getDenominator() {
		return denominator;
	}
	public void setDenominator(int denominator) {
		this.denominator = denominator;
	}
	
	/**
	 * 分母乘以整数
	 * @return
	 */
	public Fraction division(int beichu){
		this.denominator =this.denominator *beichu;
		yueFen();
		return this;
	}
	
	/**
	 * 约分
	 * @return
	 */
	public Fraction yueFen(){
		int gcdd = gcd(this.denominator, this.numerator);
		this.denominator = this.denominator/gcdd;
		this.numerator = this.numerator/gcdd;
		return this;
	}
	
	/**
	 * 取得最大公约数
	 * @param x
	 * @param y
	 * @return
	 */
	public int  gcd(int x,int y){
		if(y == 0)
	        return x;
     else
	        return gcd(y,x%y);
	}
	
	/**
	 * 分数除以该整数
	 * @param exchangeRatioOld
	 * @return
	 */
	public Fraction division1(Integer exchangeRatioOld) {
		this.numerator = this.numerator*exchangeRatioOld;
		yueFen();
		return this;
	}
	
	  
}
