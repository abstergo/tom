package com.cat.tiger.util;


import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * Created by Administrator on 2015/7/22.
 */
public class DbUtil {

    public  static String getSingleWhereSql(Map<String,Object> searchinfo){
        if(searchinfo == null) return  "";
        StringBuffer wheresql = new StringBuffer();
        for(Object key :searchinfo.keySet()){
            Object value = searchinfo.get(key);
            if(value instanceof  Integer){
                if(!Integer.valueOf(value+"").equals(-1))
                    wheresql.append(" and t.").append(key).append("=").append(value);
            }else if(value instanceof Long){
                if(!Long.valueOf(value+"").equals(-1L))
                    wheresql.append(" and t.").append(key).append("=").append(value);
            }else if(value instanceof Float){
                if(!Float.valueOf(value+"").equals(-1F))
                    wheresql.append(" and t.").append(key).append("=").append(value);
            }else if(value instanceof  String){
                if(StringUtils.isNotEmpty(value.toString())){
                    //处理时间
                    if("starttime".equals(key)){
                        wheresql.append(" and t.create_time >= '" + value + "'");
                    }else if("endtime".equals(key)){
                        wheresql.append(" and t.create_time <= '"+value+"'");
                    }else{
                        wheresql.append(" and t.").append(key).append("=").append(value);
                    }
                }
            }else{

            }
        }
        return  wheresql.toString();
    }


    public  static  HashMap<String,Object> getSingleWhereSqlAndParameter(Map<String,Object> searchinfo){
        HashMap<String,Object> results = new HashMap<>();
        StringBuffer wheresql = new StringBuffer();
        List<Object> paramtes = new ArrayList<>();
        int i = 0;
        for(Object key :searchinfo.keySet()){
            Object value = searchinfo.get(key);
            if(value instanceof  Integer){
                if(!Integer.valueOf(value+"").equals(-1)){
                    wheresql.append(" and t.").append(key).append("= ? ");
                    paramtes.add(value);
                }
            }else if(value instanceof Long){
                if(!Long.valueOf(value+"").equals(-1L)){
                    wheresql.append(" and t.").append(key).append("= ? ");
                    paramtes.add(value);
                }
            }else if(value instanceof Float){
                if(!Float.valueOf(value+"").equals(-1F)){
                    wheresql.append(" and t.").append(key).append("= ? ");
                    paramtes.add(value);
                }
            }else if(value instanceof  String){
                if(StringUtils.isNotEmpty(value.toString())){
                    wheresql.append(" and t.").append(key).append("= ? ");
                    paramtes.add(value);
                }
            }else{

            }
        }
        results.put("wheresql",wheresql);
        results.put("parames",paramtes.toArray());
        return  results;
    }

}
