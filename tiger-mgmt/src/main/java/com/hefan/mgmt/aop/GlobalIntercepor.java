package com.hefan.mgmt.aop;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Service
public class GlobalIntercepor implements HandlerInterceptor {

  Logger logger = LoggerFactory.getLogger(GlobalIntercepor.class);

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
    String showFlag = request.getParameter("showFlag");
    request.setAttribute("showFlag", showFlag);
    Object userMap = request.getSession().getAttribute("user");
    if (userMap == null) {
      response.sendRedirect("/goLogin.shtml");
      return false;
    }
    return true;
  }

  @Override
  public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse response, Object o, ModelAndView modelAndView) throws Exception {

  }

  @Override
  public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse response, Object o, Exception e) throws Exception {
  }
}
