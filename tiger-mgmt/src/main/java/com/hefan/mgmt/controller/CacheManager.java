package com.hefan.mgmt.controller;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.hefan.club.dynamic.itf.SquareService;
import com.hefan.user.itf.WebUserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * @author wangchao
 * @title: base
 * @package com.hefan.mgmt.controller.task
 * @copyright: Copyright (c) 2017
 * @date 2017/2/8 14:21
 */
@RequestMapping("/cache")
@Controller
public class CacheManager {

  @Resource
  private WebUserService webUserService;
  @Resource
  private SquareService squareService;

  private static Logger logger = LoggerFactory.getLogger(CacheManager.class);

  @RequestMapping("/refresh")
  @ResponseBody
  public String refreshCache(HttpServletRequest request) {
    String type = request.getParameter("type");
    try {
      int category = StringUtils.isBlank(type) ? 1 : Integer.parseInt(type);
      if (category == 1) {
        webUserService.refreshRanking();
      } else if (category == 2) {
        ResultBean rb  = squareService.initSquareList(1000);
        logger.info(JSON.toJSONString(rb));
      } else if (category == 3){
        ResultBean rb = squareService.initPraiseForMessageCache();
        logger.info(JSON.toJSONString(rb));
      }
    } catch (Exception e) {
      logger.error("同步主播排行榜异常", e);
      return "error";
    }
    return type;
  }
  @RequestMapping("/list")
  public String list() {
    return "/cache/list";
  }

}
