var pine = $.extend({}, pine);/* 全局对象 */
//金额千分位
function num2str(val){//失焦
	return transStr(val, 2);
}

var qfwhan="";
var qfwFocus="";
function qfwTohan(obj,num){
	if(obj==1){
		qfwhan= "您输入的金额数目过大，小数点前不超过13位!";
	}
	if(obj==0){
		qfwhan=amountInWords(num,null);
	}
	if(obj==2){
		qfwhan="请输入正确金额";
	}
}
function setNumComm(obj) {  //转换为数字   得到焦点
	var value = obj.value;
	var returnStr=value.replace(/,/g, "");
	obj.value=returnStr;
}

function formatAccount2(obj){
	var value=obj.val();
	if(value=="" || value==null || value==undefined){
		return "0.00";
	}
	var account = new String(value);
	account = account.substring(0, 29); /* 帐号的总数, 包括空格在内 */
	var reg = /\s{1,}/g;
	var card_ = "";
	//去除空格    
	value = account.replace(reg, "");
 for ( var i = 0; i < value.length; i++) {
		if (i%4==3) {
			card_ = card_ + value.charAt(i) + " ";
		} else {
			card_ = card_ + value.charAt(i);
		}
	} 
 obj.val(card_);
}

//+---------------------------------------------------   
//| 帐号格式化
//| 格式为：1234 5678 9012 3456   
//+---------------------------------------------------   
function formatAccount(obj){
	var value=obj.value;
	if(value=="" || value==null || value==undefined){
		return "";
	}
	var account = new String(value);
	account = account.substring(0, 29); /* 帐号的总数, 包括空格在内 */
	var reg = /\s{1,}/g;
	var card_ = "";
	//去除空格    
	value = account.replace(reg, "");
 for ( var i = 0; i < value.length; i++) {
		if (i%4==3) {
			card_ = card_ + value.charAt(i) + " ";
		} else {
			card_ = card_ + value.charAt(i);
		}
	} 
 obj.value = card_;
}
/**
 * 金额千分位
 * @param str
 * @return
 */
function transStr(obj, maxDec){
	var dd =obj+"";
	if(dd=="" || dd =="null")
	{
		return "";
	}
	
	qfwhan=""; qfwFocus="";//清除提示
	//if(!/^\d*(\.\d*)?$/.test(obj))return obj;
	if(!/^\-?\d+\.?\d*$/.test(obj)) return obj;//包括正数、负数、小数

	var dianIndex=0;
	var isjfu=false;
	if(typeof(obj)=="string"){
		startFu=obj.indexOf("-");
		if(startFu!=-1 && obj.length>1){
			isjfu=true;obj=obj.substr(1,obj.length);
		}
	}
	  // 验证输入金额数值或数值字符串：
	obj = obj.toString().replace(/,/g, "");  obj = obj.replace(/^0+/, "");
	var startFu=0;
	if(isjfu){obj="-"+obj;}
	if(obj!=null && obj!=undefined){
		if(obj.indexOf(".")==0){obj="0"+obj;}
		if(obj.indexOf("-")==0 && obj.indexOf(".")==1){
			obj="-0"+obj.substr(1,obj.length);
		}
	}
	var num = obj;
	var falg=0;
	var zeroNum = "0";
	if(typeof(maxDec) != "undefined"){
		if(maxDec > 0){
			zeroNum += "."
			for(var i = 0;i < maxDec; i++){
				zeroNum = zeroNum + "0";
			}
		}
	}
	if (num == "") {qfwFocus=zeroNum;qfwTohan(falg,zeroNum);return zeroNum;} 
	if(isNaN(num)){return zeroNum;}
	var dian=num.indexOf(".");
	var fu=num.indexOf("-");
	if(dian!=-1){   //有小数点
		var num2=num.substr(0,num.indexOf("."));
		if(num2.length>(fu==-1?13:14)){//小数点前超过13位数
			falg=1;qfwTohan(falg,null);return num;
		}
	}else{                    //没有小数点
		if(num.length>(fu==-1?13:14)){
			falg=1;qfwTohan(falg,null);return num;
		}
	}
	if(obj.indexOf(".")==0){
		obj="0"+obj;
	}
	qfwTohan(falg,num);
	
	var minus = "";                             // 负数的符号“-”的大写：“负”字。可自定义字符，如“（负）”。
	if(num.length > 1){
        if (num.indexOf("-") == 0){num = num.replace("-", ""); minus = "-";}   // 处理负数符号“-”
    }
	var NoneDecLen = (typeof(maxDec) == "undefined" || maxDec == null || Number(maxDec) < 0 || Number(maxDec) > 8);
	if(NoneDecLen){maxDec = vDec.length > 5 ? 5 : vDec.length;}
	var focusAfter=zeroNum.substr(zeroNum.indexOf("."),zeroNum.length);
	//alert(focusAfter);
	var begin ="";
	var after ="";
	var l;
	var str2="";
	//if(num.indexOf(".")<0)num=num+".00";//-------------
	if(num.indexOf(".")>0){
		begin = num.substring(0,num.indexOf("."));
		after = num.substring(num.indexOf(".")+1, num.length);
		var rDec = Number("0." + after);
		rDec *= Math.pow(10, maxDec); rDec = Math.round(Math.abs(rDec)); rDec /= Math.pow(10, maxDec);  // 小数四舍五入
		//alert("rDec.tostring()=" + rDec.toString());
		var aIntDec = rDec.toString().split(".");
		if(Number(aIntDec[0]) == 1) { begin = (Number(begin) + 1).toString(); }                           // 小数部分四舍五入后有可能向整数部分的个位进位（值1）
		if(aIntDec.length > 1) { 
			after = aIntDec[1]; 
			var focusAfters="";
			if(aIntDec[1].length < focusAfter.length-1){
				for(i=0;i<aIntDec[1].length;i++){
					focusAfters+=aIntDec[1].charAt(i);
				}
				for(i=0;i<focusAfter.length-1-aIntDec[1].length;i++){
					focusAfters+="0";
				}
				qfwFocus=minus+begin +"."+focusAfters;
			}else{
				for(i=0;i<aIntDec[1].length;i++){
					focusAfters+=aIntDec[1].charAt(i);
				}
				qfwFocus=minus+begin +"."+focusAfters;
			}
		} else { after = "";qfwFocus=minus+begin+ focusAfter; }
	}else{
		begin = num;
		qfwFocus=minus+num+focusAfter;
	}
	//alert("begin=" + begin);
	//alert("after=" + after);
	l=begin.length/3;
	if(l>1){
		for(var i=0;i<l;){
			str2=","+begin.substring(begin.length-3,begin.length)+str2;
			begin=begin.substring(0,begin.length-3);
			l=begin.length/3;
		}
		if(after.length < maxDec){
//			str2=begin+str2+"."+after+"0";
			if(after.length === maxDec){
				str2=begin+str2+"."+after;
			}else{
				var zero = 0;
				for(var i = 0;i < maxDec - after.length -1; i++){
					zero = zero + "0";
				}
				str2=begin+str2+"."+after + zero;
			}
		}else{
			str2=begin+str2+"."+after
		}
		return minus + str2.substring(1);
	}else{
		if(after.length < maxDec){
			if(after.length === maxDec){
				return minus + begin + "." + after;
			}else{
				var zero = 0;
				for(var i = 0;i < maxDec - after.length -1; i++){
					zero = zero + "0";
				}
				return minus + begin + "." + after + zero;
			}
		}
		return minus + begin + "." + after;
	}
}
function numChangHanComm(obj,maxDec,showid){  //金额千分位to汉字  按键按下
	if(typeof(maxDec) == "undefined"){
		maxDec = 2;
	}
	var str = amountInWords(obj.value,obj.id,maxDec);
	$("#"+showid).text(str);
}

/**
 * 小写金额转换成大写（人民币元）
 * 入参：dValue 金额数值或数值字符串
 *		a, 也支持千分位格式（#,##0.00）输入；
 *		b, 支持负数，将前导“-”号转为“负”字，将前导的“+”号忽略或转为“正”字；
 *		c1, 值域之整数部分上限（超限则返回错误）：
 *		c2, 值域之小数部分上限：5位或用户参数指定（角/分/厘/毫/丝，超限则四舍五入）。
 *
 * 入参：maxDec 精确小数位数（值域0~5，不指定则默认为2，超限归为默认）。
 *
 * 返回：大写字符串
 */
function amountInWords(dValue, id,maxDec){
    
	var val="";
	if(id!=null){val=$("#"+id).val();}
	var isZero=true;var isZero2=true;
	if(val.length>0 && val!=""){
		var t=val.split(".");
		for(i=0;i<t[0].length;i++){if(t[0].charAt(i)!="0"){isZero=false;}}
		if(t.length>1){for(i=0;i<t[1].length;i++){if(t[1].charAt(i)!="0"){isZero2=false;}}}
		if(t.length>1){if(isZero && isZero2){return "零元整" ;}}else{if(isZero){return "零元整" ;}}
	}else{
		if(id==null){
			var t=dValue.toString().split(".");
			for(i=0;i<t[0].length;i++){if(t[0].charAt(i)!="0"){isZero=false;}}
			if(t.length>1){for(i=0;i<t[1].length;i++){if(t[1].charAt(i)!="0"){isZero2=false;}}}
		    if(t.length>1){if(isZero && isZero2){return "零元整" ;}}else{if(isZero){return "零元整" ;}}
		}else{
			return "";
		}
	}
	var startFu=-1;
	if(typeof(dValue)=="string"){startFu=dValue.indexOf("-");if(startFu!=-1 && dValue.length>1){
			dValue=dValue.substr(1,dValue.length);}
	}
    // 验证输入金额数值或数值字符串：
    dValue = dValue.toString().replace(/,/g, "");  dValue = dValue.replace(/^0+/, "");      // 金额数值转字符、移除逗号、移除前导零
    if(typeof(dValue)=="string"){
	    if(startFu!=-1 && dValue.length>1){dValue="-"+dValue;}
    }
    if (dValue == "") { return ""; }      // （错误：金额为空！）
    else if (isNaN(dValue)) { return "请输入合法金额"; } 
    var minus = "";                             // 负数的符号“-”的大写：“负”字。可自定义字符，如“（负）”。
    var CN_SYMBOL = "";                         // 币种名称（如“人民币”，默认空）
    if (dValue.length > 1)
    {
        if (dValue.indexOf("-") == 0) { dValue = dValue.replace("-", ""); minus = "负"; }   // 处理负数符号“-”
        if (dValue.indexOf("+") == 0) { dValue = dValue.replace("+", ""); }                 // 处理前导正数符号“+”（无实际意义）
    }

    // 变量定义：
    var vInt = ""; var vDec = "";               // 字符串：金额的整数部分、小数部分
    var resAIW;                                 // 字符串：要输出的结果
    var parts;                                  // 数组（整数部分.小数部分），length=1时则仅为整数。
    var digits, radices, bigRadices, decimals;  // 数组：数字（0~9——零~玖）；基（十进制记数系统中每个数字位的基是10——拾,佰,仟）；大基（万,亿,兆,京,垓,杼,穰,沟,涧,正）；辅币（元以下，角/分/厘/毫/丝）。
    var zeroCount;                              // 零计数
    var i, p, d;                                // 循环因子；前一位数字；当前位数字。
    var quotient, modulus;                      // 整数部分计算用：商数、模数。

    // 金额数值转换为字符，分割整数部分和小数部分：整数、小数分开来搞（小数部分有可能四舍五入后对整数部分有进位）。
    var NoneDecLen = (typeof(maxDec) == "undefined" || maxDec == null || Number(maxDec) < 0 || Number(maxDec) > 5);     // 是否未指定有效小数位（true/false）
    parts = dValue.split(".");                      // 数组赋值：（整数部分.小数部分），Array的length=1则仅为整数。
    if (parts.length > 1) 
    {
        vInt = parts[0]; vDec = parts[1];           // 变量赋值：金额的整数部分、小数部分
        if(NoneDecLen) { maxDec = vDec.length > 5 ? 5 : vDec.length; }                                  // 未指定有效小数位参数值时，自动取实际小数位长但不超5。
        var rDec = Number("0." + vDec);     
        rDec *= Math.pow(10, maxDec); rDec = Math.round(Math.abs(rDec)); rDec /= Math.pow(10, maxDec);  // 小数四舍五入
        var aIntDec = rDec.toString().split(".");
        if(Number(aIntDec[0]) == 1) { vInt = (Number(vInt) + 1).toString(); }                           // 小数部分四舍五入后有可能向整数部分的个位进位（值1）
        if(aIntDec.length > 1) { vDec = aIntDec[1]; } else { vDec = ""; }
    }
    else { vInt = dValue; vDec = ""; if(NoneDecLen) { maxDec = 0; } } 
    var type=(id!=null?$("#"+id):null);
    var maxVal=(id!=null?$("#"+id).value:null);
    var maxlength;
    if(maxVal!=null){
    	maxlength=maxVal.indexOf(".")==-1?maxVal.length:maxVal.indexOf(".")
    }
    var isF; //是否有负数 -
    if(maxVal!=null){
    	isF=maxVal.indexOf("-")==-1?false:true;
    }
    var returnMsg="错误：金额值太大了！";
    if(type!=null){
	    if(type.vtype==null && type.kind=="float"){
	    	 if(vInt.length > 13) {
	    	   return returnMsg;
	    	 }
	    }
	    if(type.vtype=="numWanstr"){
	    	if(maxVal.substr(0,maxlength).length > (isF?16:15)){
	    		return returnMsg;
	    	}
	    }
	    if(type.vtype=="numBaiWan"){
	    	if(maxVal.substr(0,maxlength).length > (isF?14:13)){
	    		return returnMsg;
	    	}
	    }
	    if(type.vtype=="numYiYuan"){
	    	if(maxVal.substr(0,maxlength).length > (isF?12:11)){
	    		return returnMsg;
	    	}
	    }
	}
    // 准备各字符数组 Prepare the characters corresponding to the digits:
    digits = new Array("零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖");         // 零~玖
    radices = new Array("", "拾", "佰", "仟");                                              // 拾,佰,仟
    bigRadices = new Array("", "万", "亿", "兆", "京", "垓", "杼", "穰" ,"沟", "涧", "正"); // 万,亿,兆,京,垓,杼,穰,沟,涧,正
    decimals = new Array("角", "分", "厘", "毫", "丝");                                     // 角/分/厘/毫/丝
    
    resAIW = "";  // 开始处理
    
    // 处理整数部分（如果有）
   //if (Number(vInt) > 0)
    if (Number(vInt))
    {
//    	 if(k%4 == 2 && a[0].charAt(i)=="0" && a[0].charAt(i+2) != "0") re = AA[0] + re;
//         if(a[0].charAt(i) != 0) re = AA[a[0].charAt(i)] + BB[k%4] + re; k++;
        zeroCount = 0;
        for (i = 0; i < vInt.length; i++) 
        {
            p = vInt.length - i - 1; 
            d = vInt.substr(i, 1); 
            quotient = p / 4; 
            modulus = p % 4;
            if (d == "0") { zeroCount++; }
            if (d == "-") {}
            else 
            {
                if (zeroCount > 0) { resAIW += digits[0]; }
                zeroCount = 0; 
                resAIW += digits[Number(d)] + radices[modulus];
            }
            if (modulus == 0 && zeroCount < 4) { resAIW += bigRadices[quotient]; }
        }
        resAIW += "元";
    }
    
    // 处理小数部分（如果有）
    for (i = 0; i < vDec.length; i++) { d = vDec.substr(i, 1); if (d != "0") { resAIW += digits[Number(d)] + decimals[i]; } }
    
    // 处理结果
    if (resAIW == "") { resAIW = "零" + "元"; }     // 零元
    if (vDec == "") { resAIW += "整"; }             // ...元整
    //alert("负数minus="+minus)
    resAIW = CN_SYMBOL + minus + resAIW;            // 人民币/负......元角分/整
    if(resAIW=="零元整"){return "零元整";}
    var outResAIW = repace_acc(resAIW);
    
    
    return outResAIW;
    
    // 备注：
    /**********************************************************************************
    毫 .... 10^-4 ...... 0.0001
    厘 .... 10^-3 ...... 0.001
    分 .... 10^-2 ...... 0.01
    角 .... 10^-1 ...... 0.1
    -----------------------------
    个位（元）10^0      1
    -----------------------------
    拾 .... 10^1 ...... 10
    佰 .... 10^2 ...... 100
    仟 .... 10^3 ...... 1,000
    万 .... 10^4 ...... 10,000
    亿 .... 10^8 ...... 100,000,000
    兆 .... 10^12 ..... 1,000,000,000,000
    京 .... 10^16 ..... 10,000,000,000,000,000
    垓 .... 10^20 ..... 100,000,000,000,000,000,000             gāi
    杼 .... 10^24 ..... 1,000,000,000,000,000,000,000,000       zhù 
    穰 .... 10^28 ..... 10,000,000,000,000,000,000,000,000,000  rǎng 国际制用“艾可萨”,简称“艾”,符号E 
    沟 .... 10^32 ..... 100,000,000,000,000,000,000,000,000,000,000
    涧 .... 10^36 ..... 1,000,000,000,000,000,000,000,000,000,000,000,000
    正 .... 10^40 ..... 10,000,000,000,000,000,000,000,000,000,000,000,000,000
    载 .... 10^44
    极 .... 10^48
    恒河沙 ..... 10^52
    阿僧祇 ..... 10^56
    那由他 ..... 10^60
    不可思议 ... 10^64
    无量 ....... 10^68
    大数 ....... 10^72
    **********************************************************************************/
}

function repace_acc(Money){
	Money=Money.replace("零分","");
	Money=Money.replace("零角","零");
	var yy;
	var outmoney;
	outmoney=Money;
	yy=0;
	while(true){
	var lett= outmoney.length;
	outmoney= outmoney.replace("零元","元");
	outmoney= outmoney.replace("零万","万");
	outmoney= outmoney.replace("零亿","亿");
	outmoney= outmoney.replace("零兆","兆");
	outmoney= outmoney.replace("零京","京");
	outmoney= outmoney.replace("零仟","零");
	outmoney= outmoney.replace("零佰","零");
	outmoney= outmoney.replace("零零","零");
	outmoney= outmoney.replace("零拾","零");

	outmoney= outmoney.replace("京兆","京零");
	outmoney= outmoney.replace("京亿","京零");
	outmoney= outmoney.replace("京万","京零");
	
	outmoney= outmoney.replace("兆亿","兆零");
	outmoney= outmoney.replace("兆万","兆零");
	
	outmoney= outmoney.replace("亿万","亿零");
	
	outmoney= outmoney.replace("万仟","万零");
	
	outmoney= outmoney.replace("仟佰","仟零");
	yy= outmoney.length;
	if(yy==lett) break;
	}
	yy = outmoney.length;
	if ( outmoney.charAt(yy-1)=='零'){
	outmoney=outmoney.substring(0,yy-1);
	}
	yy = outmoney.length;
	if ( outmoney.charAt(yy-1)=='元'){
	outmoney=outmoney +'整';
	}
	return outmoney;
}




/**  
 * 身份证15位编码规则：dddddd yymmdd xx p   
 * dddddd：地区码   
 * yymmdd: 出生年月日   
 * xx: 顺序类编码，无法确定   
 * p: 性别，奇数为男，偶数为女  
 * <p />  
 * 身份证18位编码规则：dddddd yyyymmdd xxx y   
 * dddddd：地区码   
 * yyyymmdd: 出生年月日   
 * xxx:顺序类编码，无法确定，奇数为男，偶数为女   
 * y: 校验码，该位数值可通过前17位计算获得  
 * <p />  
 * 18位号码加权因子为(从右到左) Wi = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2,1 ]  
 * 验证位 Y = [ 1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2 ]   
 * 校验位计算公式：Y_P = mod( ∑(Ai×Wi),11 )   
 * i为身份证号码从右往左数的 2...18 位; Y_P为脚丫校验码所在校验码数组位置  
 *   
 */  
var Wi = [ 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1 ];// 加权因子   
var ValideCode = [ 1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2 ];// 身份证验证位值.10代表X   
function IdCardValidate(idCard) {   
    idCard = trim(idCard.replace(/ /g, ""));   
    if (idCard.length == 15) {   
        return isValidityBrithBy15IdCard(idCard);   
    } else if (idCard.length == 18) {   
        var a_idCard = idCard.split("");// 得到身份证数组   
        if(isValidityBrithBy18IdCard(idCard)&&isTrueValidateCodeBy18IdCard(a_idCard)){   
            return true;   
        }else {   
            return false;   
        }   
    } else {   
        return false;   
    }   
}   
/**  
 * 判断身份证号码为18位时最后的验证位是否正确  
 * @param a_idCard 身份证号码数组  
 * @return  
 */  
function isTrueValidateCodeBy18IdCard(a_idCard) {   
    var sum = 0; // 声明加权求和变量   
    if (a_idCard[17].toLowerCase() == 'x') {   
        a_idCard[17] = 10;// 将最后位为x的验证码替换为10方便后续操作   
    }   
    for ( var i = 0; i < 17; i++) {   
        sum += Wi[i] * a_idCard[i];// 加权求和   
    }   
    valCodePosition = sum % 11;// 得到验证码所位置   
    if (a_idCard[17] == ValideCode[valCodePosition]) {   
        return true;   
    } else {   
        return false;   
    }   
}   
/**  
 * 通过身份证判断是男是女  
 * @param idCard 15/18位身份证号码   
 * @return 'female'-女、'male'-男  
 */  
function maleOrFemalByIdCard(idCard){   
    idCard = trim(idCard.replace(/ /g, ""));// 对身份证号码做处理。包括字符间有空格。   
    if(idCard.length==15){   
        if(idCard.substring(14,15)%2==0){   
            return 'female';   
        }else{   
            return 'male';   
        }   
    }else if(idCard.length ==18){   
        if(idCard.substring(14,17)%2==0){   
            return 'female';   
        }else{   
            return 'male';   
        }   
    }else{   
        return null;   
    }   
}   
 /**  
  * 验证18位数身份证号码中的生日是否是有效生日  
  * @param idCard 18位书身份证字符串  
  * @return  
  */  
function isValidityBrithBy18IdCard(idCard18){   
    var year =  idCard18.substring(6,10);   
    var month = idCard18.substring(10,12);   
    var day = idCard18.substring(12,14);   
    var temp_date = new Date(year,parseFloat(month)-1,parseFloat(day));   
    // 这里用getFullYear()获取年份，避免千年虫问题   
    if(temp_date.getFullYear()!=parseFloat(year)   
          ||temp_date.getMonth()!=parseFloat(month)-1   
          ||temp_date.getDate()!=parseFloat(day)){   
            return false;   
    }else{   
        return true;   
    }   
}   
  /**  
   * 验证15位数身份证号码中的生日是否是有效生日  
   * @param idCard15 15位书身份证字符串  
   * @return  
   */  
  function isValidityBrithBy15IdCard(idCard15){   
      var year =  idCard15.substring(6,8);   
      var month = idCard15.substring(8,10);   
      var day = idCard15.substring(10,12);   
      var temp_date = new Date(year,parseFloat(month)-1,parseFloat(day));   
      // 对于老身份证中的你年龄则不需考虑千年虫问题而使用getYear()方法   
      if(temp_date.getYear()!=parseFloat(year)   
              ||temp_date.getMonth()!=parseFloat(month)-1   
              ||temp_date.getDate()!=parseFloat(day)){   
                return false;   
        }else{   
            return true;   
        }   
  }   
//去掉字符串头尾空格   
function trim(str) {   
    return str.replace(/(^\s*)|(\s*$)/g, "");   
}  

/**
 * 将千分位转成数字
 * 
 * @param num
 * @return
 */
function parseNum(num) {
	return num.replace(/,/g, "");
}

/**
 * @author 许松
 * 
 * @requires jQuery
 * 
 * 将form表单元素的值序列化成对象
 * 
 * @returns object
 */
serializeObject = function(form) {
	var o = {};
	$.each(form.serializeArray(), function(index) {
		if (o[this['name']]) {
			o[this['name']] = o[this['name']] + "," + this['value'];
		} else {
			o[this['name']] = this['value'];
		}
	});
	return o;
};