/**
   * jquery ajax分页
   * @author xs
   * @date 2014-12-16
   * @param config 
*/
jQuery.fn.pagejs = function(config){
	//  翻页
	var currentPage = 1;		//当前页
	var totalPage	= 0;		//总页数
	var totalRows	= 0;		//总记录数
	var pageSize	= 20;		//每页记录数
	var startRecord = 0;		//本页开始记录数
	var endRecord	= 0;		//本页结束记录数 = startRecord + pageSize
	var showPage = config.showPage == false ? false:true;
	var dataContent = config.dataContent == false ? false:true;
	//  请求
	var url = config.url;		//请求URL
	var param = config.param;	//请求参数
	var columns = config.columns;//列定义
	var $table = $(this); 
	
	var $thead = $table.find("thead");
	if($thead.length ==0){
		//创建表头
		$thead = $("<thead>").appendTo($table);
		var $tr = $("<tr>").appendTo($thead);
		$.each(columns,function(idx,column){
			$("<th>").html(column['title']).appendTo($tr);
		});
	}
	var $tbody = $table.find("tbody");
	if($tbody.length ==0){
		$tbody = $("<tbody>").attr("id","databody").appendTo($table);
	}else{
		$tbody.empty();
	}
	//是否显示分页信息
	if(showPage){ 
		var $tfoot = $table.find("tfoot");
		
		if($tfoot.length ==0){
			$tfoot = $("<tfoot>").appendTo($table);
			$tfoot.html('<tr><td colspan="'+columns.length+'" id="showdata"></td></tr>'); 
			
		}
		var $tfootCotnent = $tfoot.find("td");
		
		//处理中文乱码
		//param = encodeURI(param);
	
		var toolbar = "";
		toolbar += '<div class="pgToolbar">';
		toolbar += '	<div class="pgPanel">';
		toolbar += '		每页<input onafterpaste="this.value=this.value.replace(/[^\\d]/g,\'\') " onkeyup="this.value=this.value.replace(/[^\\d]/g,\'\') "  style="width:26px;height:20px; line-height:20px; margin: 5px 4px 0px 4px;"  id="goPageSize" type="text" value="'+pageSize+'" ><div class="pgSearchInfo">  </div>';
		toolbar += '		<div class="separator">|</div>';
		
	
		toolbar += '		<div class="separator" />';
		toolbar += '		<div>';
		toolbar += '			第 <span class="pgCurrentPage" >'+currentPage+'</span>页 ';
		toolbar += '			/ 共  <span class="pgTotalPage">' + totalPage + '</span>页';
		toolbar += '		</div>';
	
	
		toolbar += '		<div class="pgBtn goPage"><a >跳转</a></div>';
	
	
		
		toolbar += '		<div>第';
		toolbar += '			<input id="goPage" type="text" value="' + currentPage + '" style="width:30px;height:20px; line-height:20px; margin: 5px 4px 0px 4px;" maxlength="4" ';
		toolbar += '				onkeyup="this.value=this.value.replace(/[^\\d]/g,\'\') " ';
		toolbar += '				onafterpaste="this.value=this.value.replace(/[^\\d]/g,\'\') " ';
		toolbar += '			/>';
		toolbar += '		页</div>';
		
		toolbar += '		<div class="pgBtn pgLast"><a >末页</a></div>';
		toolbar += '		<div class="pgBtn pgNext" ><a >下一页</a></div>';
		toolbar += '		<div class="pgBtn pgPrev"><a >上一页</a></div>';
	
		toolbar += '		<div class="pgBtn pgFirst"><a >首页</a></div>';
		
		//toolbar += '		<div class="pgBtn"><select style="width:40px;height:20px;margin: 5px 4px 0px 4px; line-height:20px; "> <option value="5">5</option><option value="10">10</option><option value="50">50</option></select></div>';
	
		toolbar += '	</div>';
		toolbar += '	<div class="mask" ></div>';
		toolbar += '</div>';
		
		$tfootCotnent.html(toolbar);
	}
	var tag = "#"+this.attr("id");
	var btnRefresh = $(tag+" .pgRefresh");			//刷新按钮
	var btnNext =$(tag+" .pgNext");					//下一页按钮
	var btnPrev = $(tag+" .pgPrev");				//上一页按钮
	var btnFirst = $(tag+" .pgFirst");				//首页按钮
	var btnLast = $(tag+" .pgLast");				//末页按钮
	var btnGoPage = $(tag+" .goPage");				//跳转到第N页
	var valCurrentPage = $(tag+" .pgCurrentPage");		//当前页展示位置
	var valTotalPage = $(tag+" .pgTotalPage");			//总页数展示位置
	var valSearchInfo = $(tag+" .pgSearchInfo");		//查询信息展示位置
	var mask = $(tag+" .mask");

	// 根据url 与 param 加载 page
	loadData = function (){
		beforeLoadPage();
		valSearchInfo.html("");
		//loading mask show
		$(mask).css("display","block");
		var params = param;
		if(showPage){
			params += "&page.pn=" + currentPage+"&page.size="+$("#goPageSize").val();
		}
		
		$.getJSON( url
			,params
			,function(data){
			if(showPage){
				currentPage		= data.number ;
				totalPage		= data.totalPages ;
				totalRows		= data.totalElements ;
				pageSize		= data.size ;
//				startRecord		= data.startRecord ;
//				endRecord		= startRecord + startRecord;
				if(showPage){
					valCurrentPage.text(totalPage==0?"0":currentPage);	
					$("#goPage").val(totalPage==0?"0":currentPage);
					$("#goPageSize").val(goPageSize==0?"10":pageSize);
					valTotalPage.text(totalPage);
					valSearchInfo.append('共' + totalRows + ' 条');
				}
				//根据数据刷新按钮显示
				refreshButton(currentPage,totalPage);
			}
				//loading mask hidden
				$(mask).css("display","none");
				//回调函数，返回数据，列表加载
				//需要重载实现
				var content = data;
				if(dataContent){
					content = data.content;				
				} 
				if(content != null && content.length > 0){ 
					$.each(content,function(idx,row){
						var $tr = $("<tr>").appendTo($tbody);
						$.each(columns,function(jdx,column){
							var name =  column.name ;
							var tdContent ="";
							if(typeof name != undefined && name != null && name != ''){
								var cbFunc = column.callback;
								if(typeof cbFunc != undefined && cbFunc != null && cbFunc != ''){
									tdContent = cbFunc(row[name]);
								}else{
									tdContent = row[name];
								}
								
							}else {
								var template = column.template;
								if(typeof template != undefined && template != null && template != ''){
									//解析模板
									var compileTemplate = Handlebars.compile(template);
									tdContent = compileTemplate(row);
								}
							}					
							$("<td>").html(tdContent).appendTo($tr);
						
						});
						
					});
				}
				if(afterLoadPage){
					afterLoadPage(data);
				} 
			}
		);
		
	}
	//调用加载
	loadData();
	
	//根据数据刷新按钮显示
	refreshButton = function (currentPage,totalPage){
		if(totalPage < 2){ 
			//总页数为 0 或 1 ，不显示任何按钮
			btnPrev.hide();
			btnFirst.hide();
			btnNext.hide();
			btnLast.hide();
		}else if(currentPage == totalPage ){
			//最后一页 ，不显示 后一页，末后 
			btnPrev.show();
			btnFirst.show();
			btnNext.hide();
			btnLast.hide();
		}else if(currentPage == 1){
			//第一页 ，不显示 前一页，首页 
			btnPrev.hide();
			btnFirst.hide();
			btnNext.show();
			btnLast.show();
		}else{
			btnPrev.show();
			btnFirst.show();
			btnNext.show();
			btnLast.show();
		}
	};
	
	//按钮监听
	btnNext.click(//下一页按钮
		function(){
			if(currentPage < totalPage){
				currentPage = currentPage +1;
				loadData();	
			}			
		}
	);	
	btnPrev.click(//上一页按钮
		function(){
			if(currentPage > 1){
				currentPage = currentPage -1;
				loadData();
			}
		}
	);
	btnFirst.click(//第一页按钮
		function(){
			if( currentPage > 1){
				currentPage =1;	
				loadData();
			}
		}
	);
	btnLast.click(//最末页按钮
		function(){
			if( currentPage < totalPage){
				currentPage = totalPage;
				loadData();
			}
		}
	);
	btnRefresh.click(//刷新
		function(){
			loadData();
		}
	);
	btnGoPage.click(//页码跳转
		function(){
			var targetPage = $("#goPage").val();
			try{
				targetPage = parseInt(targetPage);
				if(targetPage>=1 && targetPage<=totalPage){
					currentPage = targetPage;
					loadData();
				}else{
					alert("你输入的页码不正确！");
				}
			}catch(e){
				alert("你输入的页码不正确！");
			}
		}
	);

}
//加载翻页参数之前调用，用于功能扩展
function beforeLoadPage(data){	
	$("#databody tr td").remove();
}; 
  