<%@ page language="java" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">

                <div class="dropdown profile-element"> <span><img alt="image" class="img-circle" src="${pageContext.request.contextPath}/images/index/tx_woman.bmp" height="64px;" width="64px;"/>
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">${user.userName }</strong>
                             </span>  </span>
                    </a>
                </div>
                <div class="logo-element">
                    xx
                </div>

            </li>

            <li <c:if test ="${empty showFlag ||showFlag eq 'rwdd_rwgl'}">class="active"</c:if>>
                <a href="${pageContext.request.contextPath}/task/index.shtml" target="_self"><i class="fa"></i> <span class="nav-label">
                    任务调度</span></a>
            </li>
            <li <c:if test ="${showFlag eq 'cache'}">class="active"</c:if>>
                <a href="index.html#"><i class="fa"></i> <span class="nav-label">
                    缓存管理</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li <c:if test ="${showFlag eq 'cache'}">class="active"</c:if>><a
                            href="${pageContext.request.contextPath}/cache/list.shtml?showFlag=cache" target="_self">缓存管理</a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>