<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<div class="row border-bottom">
    <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <ul class="nav navbar-top-links navbar-right">
            <li>
                            <span class="m-r-sm text-muted welcome-message">
                           <i class="fa fa-home"></i>
                            欢迎使用</span>
            </li>
            <li>
                <a href="${pageContext.request.contextPath}/user/logout.shtml" target="_self">
                    <i class="fa fa-sign-out"></i> 退出
                </a>
            </li>
        </ul>

    </nav>
</div>
