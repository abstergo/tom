<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<jsp:include page="/common/meta.jsp" />
<title>盒饭任务调度后台管理系统</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css">
</link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/buttons/buttons.css">
</link>

<link rel="stylesheet" href="${pageContext.request.contextPath}/js/poshytip/tip-yellowsimple/tip-yellowsimple.css">
</link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/poshytip/tip-orange/tip-orange.css">
</link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/poshytip/tip-twitter/tip-twitter.css">
</link>
</head>
<body style="background:#eee;">
    <div class="yk_wrap" style="margin-top:150px; padding-bottom:0px;">
    <div class="yk_product">
        <!--新用户登录-->
        <div class="login clearfx">
            <div class="zcdl">
                <p class="p1 clearfx">
                    <%-- <img alt="image"  src="${pageContext.request.contextPath}/images/org.png" height="60px;" width="60px;" /> --%>
                    <h3 style="text-align:center;">盒饭任务调度后台管理系统</h3>
                 </p>

                <div class="p2 clearfx">
                    <div class="bg" id="nicknamebg">
                        <span class="newuser"><img src="${pageContext.request.contextPath}/images/login/newuser.jpg"/></span>
                        <input style="border:0;  padding:6px 0px 6px 0px; color:#A8A8A8;" type="text" name="nickname"
                               id="nickname"
                               value="用户名"
                               onfocus="if(this.value == '用户名') {this.value = '';closeMsg('nickname');this.style.color='#333'};setFocusBorderColor('nicknamebg');"
                               onblur="if(this.value == ''){this.value = '用户名';this.style.color='#A8A8A8'};setBlurBorderColor('nicknamebg');"
                                />
                    </div>
                </div>

                <div class="p2 clearfx">
                    <div class="bg" id="passwordbg">
                        <span class="newuser"><img src="${pageContext.request.contextPath}/images/login/newpassword.jpg"/></span>

                        <input style="display:none;" type="password"/>
                        <input style="border:0; padding:6px 0px 6px 0px; " name="password" id="password"
                               type="password"
                               onfocus="if(this.value ==''){ closeMsg('password');this.style.color='black'; hider();}; setFocusBorderColor('passwordbg');"
                               onblur="if(this.value == ''){this.style.color='#A8A8A8'; showr();}; setBlurBorderColor('passwordbg');"/>

                        <p class="placeholder" style="display: block;">输入密码</p>

                    </div>
                </div>
                <div class="p4"><a href="#" id="login" onclick="loginSubmit()">登&nbsp;&nbsp;&nbsp;&nbsp;录</a>
                    <img src="${pageContext.request.contextPath}/images/login/loading.gif" id="loginloading"
                         style="width:20px;height:20px;position:absolute; right:60px; top:8px;display: none;"/>
                </div>
                
            </div>
        </div>
        <!--新用户登录结束-->
    </div>
</div>
<div class="bottom2" style="border-top:1px solid #d1d1d1; width:960px;  height:53px; color:#a3a3a3; line-height:53px; text-align:center; margin:0px auto; margin:70px auto 40px; ">
   ©&nbsp;2015&nbsp;xxx.com&nbsp;&nbsp;|&nbsp;&nbsp; X计划 版权所有
</div>
    <script src="${pageContext.request.contextPath}/js/jquery-1.8.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.md5.js"></script>
    <script src="${pageContext.request.contextPath}/js/poshytip/jquery.poshytip.js"></script>
    <script type="text/javascript">

	$(function(){
		setTimeout(function() {
			$('#nickname').focus();
		}, 1);
	$("input").focus(function(){
			$("#error_info").hide();
		});
    errorNoneRightTip("nickname", 12);
    errorNoneRightTip("password", 12);
    errorNoneRightTip("loginCode",46)

		$(this).keydown(function(event){
			if(event.keyCode == '13'){
				loginSubmit();
			}
		});

    $(".placeholder").click(function () {

        $(".placeholder").hide();
        $("#password").focus();
    })

	});



	//登录
	 function loginSubmit(){
		  var nkName = $("#nickname").val();
	        if (nkName == "" || nkName == null || $.trim(nkName) == "用户名") {
	            errorTip("nickname", "请输入用户名");
	            return;
	        }
	        else {
	            closeMsg('nickname');
	        }

	        var password = $("#password").val();
	        if (password == "" || password == null || $.trim(password) == "输入密码") {
	            errorTip("password", "请输入密码");
	            return;
	        }
	        else {
	            closeMsg('password');
	        }


	     $.post('${pageContext.request.contextPath}/goToLogin.shtml', {
                "userName": $("#nickname").val(),
                "userPwd": $.md5($("#password").val())
            }, function (data) {

				if(data==0){
					location.href="${pageContext.request.contextPath}/task/index.shtml";
				}else {
						 if(data==3)
						 {
							 errorTip("nickname", "用户名已被注销");
						 }
						 else if(data==4)
						 {
							 errorTip("nickname", "用户名已被冻结");
						 }
						 else
						 {
							 errorTip("nickname", "用户名或密码错误");
						 }

				}
	});
};

//放上时 边框变颜色
function setFocusBorderColor(id)
{
    $("#"+id).css("border","1px solid #e77d00");
}

//放上时 边框变颜色
function setBlurBorderColor(id)
{
    $("#"+id).css("border","1px solid #DADADA");
}
function hider()
{
	$(".placeholder").hide();
};

function showr()
{
	$(".placeholder").show();
}
function closeMsg(id)
{
	$('#'+id).poshytip('hide');
}

function errorNoneRightTip(id,content,x,y)
{
	if(y)
	$('#'+id).poshytip({
		className: 'tip-yellowsimple',
		content: content,
		showOn: 'none',
		alignTo: 'target',
		alignX: 'right',
		offsetX: x||5,
		offsetY: y||-25
	});
	else
	$('#'+id).poshytip({
		className: 'tip-yellowsimple',
		content: content,
		showOn: 'none',
		alignTo: 'target',
		alignX: 'right',
		alignY: 'center',
		offsetX: x||5

	});
}

function errorTip(id,errorInfo,color)
{
	if(color)errorInfo="<font color='"+color+"'>"+errorInfo+"</font>";
	$('#'+id).poshytip('update',  errorInfo);
	$('#'+id).poshytip('show');
}
function msgTip(id,errorInfo,color)
{
	if(color)errorInfo="<font color='"+color+"'>"+errorInfo+"</font>";
	$('#'+id).poshytip('update',  errorInfo);
	$('#'+id).poshytip('show');
}
function closeMsg(id)
{
	$('#'+id).poshytip('hide');
}

</script>
</body>
</html>