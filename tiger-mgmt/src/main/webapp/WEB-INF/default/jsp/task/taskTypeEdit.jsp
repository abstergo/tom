<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<jsp:include page="/common/meta.jsp" />
<base target="workspace" />
<title><c:if test="${isNew }">任务新增</c:if><c:if test="${!isNew }">任务修改</c:if></title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/js/bootstrap/css/bootstrap.min.css" />
<link
	href="${pageContext.request.contextPath}/css/font-awesome/css/font-awesome-v=1.7.css"
	rel="stylesheet" />
<!-- Morris -->
<link
	href="${pageContext.request.contextPath}/css/morris/morris-0.4.3.min.css"
	rel="stylesheet" />

<!-- Gritter -->
<link
	href="${pageContext.request.contextPath}/js/plugins/gritter/jquery.gritter.css"
	rel="stylesheet" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/animate.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style-v=1.7.css" />
<style>
.clearfx {
	zoom: 1;
}

.clearfx:after {
	display: block;
	clear: both;
	content: "";
}

.piginmag img {
	float: left;
	width: 100px;
	margin-right: 20px;
}

.piginmag .list {
	float: left;
}
</style>

</head>
<body>
	<div id="wrapper">

		<jsp:include page="/common/left.jsp" />

		<div id="page-wrapper" class="gray-bg dashbard-1">

			<jsp:include page="/common/head.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-sm-4">
					<h2>
						<c:if test="${isNew }">任务新增</c:if>
						<c:if test="${!isNew }">任务修改</c:if>
					</h2>
					<ol class="breadcrumb">
						<li>任务调度</li>
						<li><a
							href="${pageContext.request.contextPath}/task/index.shtml?showFlag=rwdd_rwgl"
							target="_self">任务管理</a></li>
						<li><strong><c:if test="${isNew }">任务新增</c:if>
						<c:if test="${!isNew }">任务修改</c:if></strong></li>
					</ol>
				</div>
			</div>
			<div class="wrapper wrapper-content   animated fadeInRight">
				<div class="col-lg-8">
					<div class="ibox ">
						<div class="ibox-content">
							<form class="m-t" id="mainForm">
								<input type="hidden" name="action" value="${actionName}"/>
								<input type="hidden" name="sts" value="${taskType.sts}"/>
								<c:if test="${isNew }">
									<div class="form-group">
										<input id="taskType" name="taskType" placeholder="请输入任务类型"
											class="form-control" type="text" aria-required="true"
											aria-invalid="false" class="valid" />
									</div>
									<div class="form-group">
										<input id="dealBean" name="dealBean" placeholder="请输入任务处理Bean"
											class="form-control" type="text" aria-required="true"
											aria-invalid="false" class="valid" />
									</div>
									<div class="form-group">
										<input id="taskDescription" name="taskDescription" placeholder="请输入任务描述"
											class="form-control" type="text" aria-required="true"
											aria-invalid="false" class="valid" />
									</div>
									<div class="form-group">
										<input id="permitRunStartTime" name="permitRunStartTime"
											placeholder="请输入执行开始时间" class="form-control" type="text"
											aria-required="true" aria-invalid="false" class="valid" />
									</div>
									 <div class="form-group">
										<input id="ips" name="ips"
											placeholder="请输入执行主机IP，逗号分隔" class="form-control" type="text"
											aria-required="true" aria-invalid="false" class="valid" />
									</div>
								</c:if>
								<c:if test="${!isNew }">
									<div class="form-group">
										<input id="taskType" name="taskType" value="${taskType.baseTaskType }"
											class="form-control" type="text" aria-required="true"
											aria-invalid="false" class="valid" />
									</div>
									<div class="form-group">
										<input id="dealBean" name="dealBean" value="${taskType.dealBeanName }"
											class="form-control" type="text" aria-required="true"
											aria-invalid="false" class="valid" />
									</div>
									<div class="form-group">
										<input id="taskDescription" name="taskDescription" value="${taskType.taskDescription }"
											class="form-control" type="text" aria-required="true"
											aria-invalid="false" class="valid" />
									</div>
									<div class="form-group">
										<input id="permitRunStartTime" name="permitRunStartTime"
											value="${taskType.permitRunStartTime }" class="form-control" type="text"
											aria-required="true" aria-invalid="false" class="valid" />
									</div>
									<div class="form-group">
										<input id="ips" name="ips"
											value="${ips }" class="form-control" type="text"
											aria-required="true" aria-invalid="false" class="valid" />
									</div>
								</c:if>
								
							</form>
							<button id="save" class="btn btn-primary block full-width  m-b"
								type="submit">保存任务</button>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="/common/bottom.jsp" />
		</div>
	</div>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/bootstrap/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script
		src="${pageContext.request.contextPath}/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/hplus-v=1.7.js"></script>
	<!-- jQuery Validation plugin javascript-->
	<script
		src="${pageContext.request.contextPath}/js/plugins/validate/jquery.validate.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/js/plugins/validate/messages_zh.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.form.js"></script>
	<script src="${pageContext.request.contextPath}/js/jquery.json-2.4.js"></script>

	<script type="text/javascript">
        
        
         //以下为官方示例
        $().ready(function () {
         
	        $("#save").click(function() {
	        	
	        	var taskType = document.all("taskType").value;
	        	var reg = /.*[\u4e00-\u9fa5]+.*$/; 
	        	if(reg.test(taskType)){
	        	   alert('任务类型不能含中文');
	        	   return;
	        	}
	        	if(taskType==null||taskType==''||isContainSpace(taskType)){
	        		alert('任务类型不能为空或存在空格');
	        		return;
	        	}
	        	var str = document.all("dealBean").value;
	        	if(str == null || str.length==0){
	        		alert("请输入处理任务的bean名称");
	        		return;
	        	}
	        	if(isContainSpace(str)){
	        		alert('处理任务的bean名称不能存在空格');
	        		return;
	        	}
	        	if(reg.test(str)){
	        	   alert('bean名称不能含中文');
	        	   return;
	        	}
	        	
	        	var ips = document.all("ips").value;
	        	if(ips == null || ips.length==0){
	        		alert("请输入处理任务的主机ip");
	        		return;
	        	}
	
	       		//保存
	           	$.ajax({
	           		url : '${pageContext.request.contextPath}/task/taskTypeSave.shtml',
	           		data : {
	           			mydata:$("#mainForm").serializeObjectToJson()
	           		},
	           		cache : false,
	           		dataType : "text",
	           		success : function(data) {
	           			if(data){
	           				alert(decodeURI(data));
	           				window.location.href ="${pageContext.request.contextPath}/task/index.shtml?showFlag=rwdd_rwgl";
	           			}
	           		}
	           	}); 
	        });
		});
        
        function isContainSpace(array) {   
        	if(array.indexOf(' ')>=0){
        		return true;
        	}
            return false;
        }
    </script>

</body>
</html>
