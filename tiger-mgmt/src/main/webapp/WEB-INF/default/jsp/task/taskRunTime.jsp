<?xml version="1.0" encoding="UTF-8"?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<base target="workspace" />
<jsp:include page="/common/meta.jsp" />
<title>任务管理 - 任务运行时详情</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/js/bootstrap/css/bootstrap.min.css" />
<link
	href="${pageContext.request.contextPath}/css/font-awesome/css/font-awesome-v=1.7.css"
	rel="stylesheet" />

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/animate.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style-v=1.7.css" />
<style type="">
/* .pgBtn {
	width: 46px;
	height: 20px;
	cursor: pointer;
} */
.pgToolbar {
	width: 100%;
	height: 30px;
	line-height: 30px;
	font-size: 12px;
	text-align: right;
	font-weight: 400;
}

.pgPanel {
	height: 30px;
}

.pgPanel div {
	float: right;
	margin: 0px 6px;
	display: inline;
}
</style>
</head>
<body>
	<div id="wrapper">

		<jsp:include page="/common/left.jsp" />

		<div id="page-wrapper" class="gray-bg dashbard-1">

			<jsp:include page="/common/head.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-sm-4">
					<h2>任务执行列表</h2>
				</div>
			</div>
			<div class="wrapper wrapper-content   animated fadeInRight">

				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<%-- <a onclick="javascript:clearTaskType('${taskType}')" target="_self"
								class="btn btn-white  btn-sm"> <i class="fa fa-plus"></i>清除运行期详情
							</a> --%>
							<i class="fa fa-plus"></i>运行期详情
							<div class="ibox-content">

								<table id="taskList"
									class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th>序号</th>
											<th>IP地址</th>
											<th>主机名</th>
											<th>注册时间</th>
											<th>版本</th>
											<th>下次开始</th>
											<!-- <th>处理详情</th>
											<th>处理机器</th> -->
										</tr>
									</thead>
									<tbody id="databody">
									</tbody>
									<tfoot>
										<tr>
											<td colspan="9" id="showdata"></td>
										</tr>
									</tfoot>
									<tbody>
										<c:forEach items="${serverList }" var="server"
											varStatus="status">
											<tr id="cloneTr">
												<td>${ status.index + 1}</td>
												<td>${server.ip }</td>
												<td>${server.hostName }</td>
												<td>${server.registerTime }</td>
												<td>${server.version }</td>
												<td>${server.nextRunStartTime }</td>
												<%-- <td>${server.dealInfoDesc }</td> --%>
												<%-- <td>${server.managerFactoryUUID }</td> --%>
											</tr>
										</c:forEach>
										
									</tbody>
								</table>

							</div>
						</div>
						
						<div class="ibox float-e-margins">
							<i class="fa fa-plus"></i>任务执行记录
							<div class="ibox-content">

								<table id="taskList"
									class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th>序号</th>
											<th>任务类型</th>
											<th>执行时间</th>
											<th>下次执行时间</th>
											<th>执行耗时</th>
											<th>执行结果</th>
											<th>执行主机名</th>
											<th>执行主机IP</th>
											<th>执行状态</th>
										</tr>
									</thead>
									<tbody id="databody">
									</tbody>
									<tfoot>
										<tr>
											<td colspan="9" id="showdata"></td>
										</tr>
									</tfoot>
									<tbody>
										<c:forEach items="${scheduleExecuteRecordList }" var="scheduleExecuteRecord">
											<tr id="cloneTr">
												<td>${ scheduleExecuteRecord.id}</td>
												<td>${scheduleExecuteRecord.taskType }</td>
												<td>${scheduleExecuteRecord.executeTime }</td>
												<td>${scheduleExecuteRecord.nextExecuteTime }</td>
												<td>${scheduleExecuteRecord.consumingTime }</td>
												<td>${scheduleExecuteRecord.executeResult }</td>
												<td>${scheduleExecuteRecord.hostName }</td>
												<td>${scheduleExecuteRecord.ip }</td>
												<td>${scheduleExecuteRecord.status eq 1 ?'成功':'失败'}</td>
											</tr>
										</c:forEach>
										
									</tbody>
								</table>

							</div>
						</div>

					</div>

				</div>
			</div>
			<jsp:include page="/common/bottom.jsp" />

		</div>
	</div>


	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/bootstrap/js/bootstrap.min.js"></script>
	<script
		src="${pageContext.request.contextPath}/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script
		src="${pageContext.request.contextPath}/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${pageContext.request.contextPath}/js/hplus-v=1.7.js"></script>
	<script
		src="${pageContext.request.contextPath}/js/plugins/pace/pace.min.js"></script>
	<script>
     function clearTaskType(taskType){
    	 if(confirm("是否确定要清除任务"+taskType+"的运行期数据")){
    		 $.ajax({
	           		url : '${pageContext.request.contextPath}/task/taskTypeManager.shtml',
	           		data : {
	           			"taskType":taskType,
	           			"action":"clearTaskType"
	           		},
	           		cache : false,
	           		dataType : "json",
	           		success : function(data) {
	           			if(data){
	           				alert(data);
	           				window.location.href ="${pageContext.request.contextPath}/task/taskRunTime.shtml?taskType=${taskType}";
	           			}
	           		}
	           	}); 
    	 }
     }
    </script>
</body>
</html>
