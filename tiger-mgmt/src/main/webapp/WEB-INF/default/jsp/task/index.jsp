<?xml version="1.0" encoding="UTF-8"?> 
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<base target="workspace" />
<jsp:include page="/common/meta.jsp" />
<title>任务管理</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/js/bootstrap/css/bootstrap.min.css"/>
<link href="${pageContext.request.contextPath}/css/font-awesome/css/font-awesome-v=1.7.css" rel="stylesheet"/>

<link rel="stylesheet" href="${pageContext.request.contextPath}/css/animate.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style-v=1.7.css"/>
<style type="">
/* .pgBtn {
	width: 46px;
	height: 20px;
	cursor: pointer;
} */
.pgToolbar {
	width:100%;
	height:30px;
	line-height:30px;
	font-size:12px;
	text-align:right;
	font-weight:400;
}
.pgPanel{height:30px;}
.pgPanel div{float:right; margin:0px 6px; display:inline;}
</style>
</head>
<body >
   <div id="wrapper">
        
      <jsp:include page="/common/left.jsp" />
 
        <div id="page-wrapper" class="gray-bg dashbard-1" >
        
            <jsp:include page="/common/head.jsp" />
                <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-sm-4">
                    <h2>任务列表</h2>
                </div>
            </div>
        <div class="wrapper wrapper-content   animated fadeInRight">
               
           <div class="row">
            <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                         <a onclick="javascript:addTaskType()" target="_self"   
                   class="btn btn-white  btn-sm"> <i class="fa fa-plus"></i>新增</a>
                            <div class="ibox-content">

                                <table id="taskList" class="table table-striped table-bordered table-hover dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>序号</th>
                                            <th>任务类型</th>
                                            <th>任务处理Bean</th>
                                            <th>任务描述</th>
                                            <th>执行开始时间</th>
                                            <th>任务状态</th>
                                            <th align="center">操作</th>
                                        </tr>
                                    </thead>
                                    <tbody id="databody" > </tbody>
                                    <tfoot>
										<tr><td colspan="7" id="showdata"></td></tr>
									</tfoot>
										<tbody >
											
											<c:forEach items="${taskTypes }" var="taskType" varStatus="status">
											    <tr id="cloneTr" >
											    <td>${ status.index + 1}</td>
											    <td style="width:15%;word-break:break-all">${taskType.baseTaskType }</td>
												<td style="width:15%;word-break:break-all">${taskType.dealBeanName }</td>
												<td >${taskType.taskDescription }</td>
												<td >${taskType.permitRunStartTime }</td>
												<td ><font color="red"><c:if test="${taskType.sts == 'pause' }">暂停中</c:if>
												<c:if test="${taskType.sts == 'resume' }">运行中</c:if></font>
												</td>
												<td >
												<a target='_self' class="btn-white btn-sm" href="${pageContext.request.contextPath}/task/taskRunTime.shtml?taskType=${taskType.baseTaskType }"><i class="fa fa-folder"></i>详情</a>
												<a target='_self' class=' btn-white btn-sm' href="${pageContext.request.contextPath}/task/taskTypeEdit.shtml?taskType=${taskType.baseTaskType }"><i class='fa fa-pencil'></i>修改</a>
												<c:if test="${taskType.sts == 'pause' }"><a target='_self' class="btn-white btn-sm" href="javascript:void(0)" onclick="resumeTaskType('${taskType.baseTaskType }')"><i class="fa fa-rocket"></i>重启</a></c:if>
												<c:if test="${taskType.sts == 'resume' }"><a target='_self' class="btn-white btn-sm" href="javascript:void(0)" onclick="pauseTaskType('${taskType.baseTaskType }')"><i class="fa fa-flickr"></i>暂停</a></c:if>
												<a target='_self' class="btn-white btn-sm" href="javascript:void(0)" onclick="deleteTaskType('${taskType.baseTaskType }')"><i class="fa fa-crop"></i>删除</a></td>
												</tr>
											</c:forEach>
										</tbody>
                                </table>

                            </div>
                        </div>

           </div>
           
           </div>
            </div>
             <jsp:include page="/common/bottom.jsp" />
            
        </div>
    </div>
    

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap/js/bootstrap.min.js"></script>
  <script src="${pageContext.request.contextPath}/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="${pageContext.request.contextPath}/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/hplus-v=1.7.js"></script>
    <script src="${pageContext.request.contextPath}/js/plugins/pace/pace.min.js"></script>
     <script>
     
     function addTaskType(){
    	 window.location.href ="${pageContext.request.contextPath}/task/taskTypeEdit.shtml?showFlag=rwdd_rwgl";
     }
     
     function deleteTaskType(taskType){
    	 if(confirm("是否确定要删除任务"+taskType)){
    		 $.ajax({
	           		url : '${pageContext.request.contextPath}/task/taskTypeManager.shtml',
	           		data : {
	           			"taskType":taskType,
	           			"action":"deleteTaskType"
	           		},
	           		cache : false,
	           		dataType : "json",
	           		success : function(data) {
	           			if(data){
	           				alert(decodeURI(data));
	           				window.location.href ="${pageContext.request.contextPath}/task/index.shtml";
	           			}
	           		}
	           	}); 
    	 }
     }
     
     function resumeTaskType(taskType){
    	 if(confirm("是否确定要重启任务"+taskType)){
    		 $.ajax({
	           		url : '${pageContext.request.contextPath}/task/taskTypeManager.shtml',
	           		data : {
	           			"taskType":taskType,
	           			"action":"resumeTaskType"
	           		},
	           		cache : false,
	           		dataType : "json",
	           		success : function(data) {
	           			if(data){
	           				alert(decodeURI(data));
	           				window.location.href ="${pageContext.request.contextPath}/task/index.shtml?showFlag=rwdd_rwgl";
	           			}
	           		}
	           	}); 
    	 }
     }
     
     function pauseTaskType(taskType){
    	 if(confirm("是否确定要暂停任务"+taskType)){
    		 $.ajax({
	           		url : '${pageContext.request.contextPath}/task/taskTypeManager.shtml',
	           		data : {
	           			"taskType":taskType,
	           			"action":"pauseTaskType"
	           		},
	           		cache : false,
	           		dataType : "json",
	           		success : function(data) {
	           			if(data){
	           				alert(decodeURI(data));
	           				window.location.href ="${pageContext.request.contextPath}/task/index.shtml?showFlag=rwdd_rwgl";
	           			}
	           		}
	           	}); 
    	 }
     }
    </script>
</body>
</html>
