<?xml version="1.0" encoding="UTF-8"?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <base target="workspace"/>
    <jsp:include page="/common/meta.jsp"/>
    <title>任务管理 - 任务运行时详情</title>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/js/bootstrap/css/bootstrap.min.css"/>
    <link
            href="${pageContext.request.contextPath}/css/font-awesome/css/font-awesome-v=1.7.css"
            rel="stylesheet"/>

    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/css/animate.css"/>
    <link rel="stylesheet"
          href="${pageContext.request.contextPath}/css/style-v=1.7.css"/>
    <style type="">
        /* .pgBtn {
            width: 46px;
            height: 20px;
            cursor: pointer;
        } */
        .pgToolbar {
            width: 100%;
            height: 30px;
            line-height: 30px;
            font-size: 12px;
            text-align: right;
            font-weight: 400;
        }

        .pgPanel {
            height: 30px;
        }

        .pgPanel div {
            float: right;
            margin: 0px 6px;
            display: inline;
        }
    </style>
</head>
<body>
<div id="wrapper">

    <jsp:include page="/common/left.jsp"/>

    <div id="page-wrapper" class="gray-bg dashbard-1">

        <jsp:include page="/common/head.jsp"/>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-sm-4">
                <h2>缓存管理列表</h2>
            </div>
        </div>
        <div class="wrapper wrapper-content   animated fadeInRight">

            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-content">

                            <table id="taskList"
                                   class="table table-striped table-bordered table-hover dataTables-example">
                                <thead>
                                <tr>
                                    <th>缓存服务名</th>
                                    <th align="center">操作</th>
                                </tr>
                                </thead>
                                <tbody id="databody">
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="2" id="showdata"></td>
                                </tr>
                                </tfoot>
                                <tbody>
                                <tr id="cloneTr">
                                    <td>用户缓存管理</td>
                                    <td>
                                        <a target='_self' onclick="javascript:syncCache(1);"
                                           class=' btn-white btn-sm'><i class='fa fa-pencil'></i>缓存同步</a>
                                    </td>
                                </tr>
                                <tr id="cloneTr2">
                                    <td>动态缓存管理</td>
                                    <td>
                                        <a target='_self' onclick="javascript:syncCache(2);"
                                           class=' btn-white btn-sm'><i class='fa fa-pencil'></i>缓存动态</a>
                                    </td>
                                </tr>
                                <tr id="cloneTr3">
                                    <td>动态点赞缓存管理</td>
                                    <td>
                                        <a target='_self' onclick="javascript:syncCache(3);"
                                           class=' btn-white btn-sm'><i class='fa fa-pencil'></i>缓存动态点赞</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>

            </div>
        </div>
        <jsp:include page="/common/bottom.jsp"/>

    </div>
</div>


<script type="text/javascript"
        src="${pageContext.request.contextPath}/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/js/bootstrap/js/bootstrap.min.js"></script>
<script
        src="${pageContext.request.contextPath}/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script
        src="${pageContext.request.contextPath}/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="${pageContext.request.contextPath}/js/hplus-v=1.7.js"></script>
<script
        src="${pageContext.request.contextPath}/js/plugins/pace/pace.min.js"></script>
<script type="text/javascript">
    function syncCache(type) {
        if (!confirm("是否确定要同步该缓存？")) {
            return;
        }
        $.ajax({
            url: '${pageContext.request.contextPath}/cache/refresh.shtml',
            data: { type: type},
            cache: false,
            dataType: "text",
            success: function (data) {
                if (data != null && data != '') {
                    if (data == "1") {
                        alert("缓存同步用户排行榜信息成功");
                    }else if (data == "2"){
                        alert("缓存动态信息成功");
                    }else if(data == "3"){
                        alert("缓存动态点赞信息成功");
                    }else if (data == "error"){
                        alert("缓存同步信息失败");
                    }
                }
            }
        });
    }
</script>
</body>
</html>
