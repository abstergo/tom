package com.hefan.api.bean;

import java.io.Serializable;

/**
 * Created by kevin_zhang on 27/02/2017.
 */
@SuppressWarnings("serial")
public class LiveActivityVo implements Serializable {
    private String chartletName; //贴图名称
    private String chartletPic; //贴图地址
    private int linkType; //1：站内打开，2：站外打开
    private String linkAddress; //链接地址",
    private int isTiming; //是否定时开启：0否，1是
    private long startTime;
    private long endTime;

    public String getChartletName() {
        return chartletName;
    }

    public void setChartletName(String chartletName) {
        this.chartletName = chartletName;
    }

    public String getChartletPic() {
        return chartletPic;
    }

    public void setChartletPic(String chartletPic) {
        this.chartletPic = chartletPic;
    }

    public int getLinkType() {
        return linkType;
    }

    public void setLinkType(int linkType) {
        this.linkType = linkType;
    }

    public String getLinkAddress() {
        return linkAddress;
    }

    public void setLinkAddress(String linkAddress) {
        this.linkAddress = linkAddress;
    }

    public int getIsTiming() {
        return isTiming;
    }

    public void setIsTiming(int isTiming) {
        this.isTiming = isTiming;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }
}
