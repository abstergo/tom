package com.hefan.api.bean;

import java.io.Serializable;

/**
 * Created by lxw on 2016/9/28.
 */
public class LoginTokenVo implements Serializable {

    /**
     * 登陆token值
     */
    public String token;

    /**
     * 登陆用户Id
     */
    public String userId;

    /**
     * 登陆设备号
     */
    public String deviceNo;

    public LoginTokenVo() {
    }
    public LoginTokenVo(String token, String userId, String deviceNo) {
        this.token = token;
        this.userId = userId;
        this.deviceNo = deviceNo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }
}
