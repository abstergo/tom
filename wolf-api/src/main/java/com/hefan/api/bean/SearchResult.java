package com.hefan.api.bean;

import java.util.List;
import java.util.Map;

/**
 * Created by hbchen on 2016/10/22.
 */
public class SearchResult {

    private String searchtime;

    private String total;

    private String num;

    private String viewtotal;

    private List<Map> items;

    public String getSearchtime() {
        return searchtime;
    }

    public void setSearchtime(String searchtime) {
        this.searchtime = searchtime;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getViewtotal() {
        return viewtotal;
    }

    public void setViewtotal(String viewtotal) {
        this.viewtotal = viewtotal;
    }

    public List<Map> getItems() {
        return items;
    }

    public void setItems(List<Map> items) {
        this.items = items;
    }
}
