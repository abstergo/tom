package com.hefan.api.bean;


import java.io.Serializable;

/**
 * Created by hbchen on 2016/10/19.
 */
public class SearchResultVo implements Serializable{

    private String id;
    private String name;
    private int type;
    private String headImg;
    private  long watchedCount;
    private  long fansCount;
    private int isWatched;//0关注1互粉2未关注

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public long getWatchedCount() {
        return watchedCount;
    }

    public void setWatchedCount(long watchedCount) {
        this.watchedCount = watchedCount;
    }

    public long getFansCount() {
        return fansCount;
    }

    public void setFansCount(long fansCount) {
        this.fansCount = fansCount;
    }

    public int getIsWatched() {
        return isWatched;
    }

    public void setIsWatched(int isWatched) {
        this.isWatched = isWatched;
    }
}
