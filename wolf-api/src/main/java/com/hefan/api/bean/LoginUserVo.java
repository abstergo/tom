package com.hefan.api.bean;

import java.io.Serializable;

/**
 * Created by lxw on 2016/9/28.
 */
public class LoginUserVo implements Serializable {

    private String userId;
    private String deviceNo;
    private Integer thirdType; //第三方登陆类型   1 微博   2 微信  3 qq  4 手机号   5 微信unionid
    private String mobile;
    private String msCode;
    private String openid;
    private String headImg;
    private String nickName;
    private String qq;
	private String deviceToken;

    private String wxuid;
    //微信的unionId
    private String unionidWx;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDeviceNo() {
        return deviceNo;
    }

    public void setDeviceNo(String deviceNo) {
        this.deviceNo = deviceNo;
    }

    public Integer getThirdType() {
        return thirdType == null ? 0 : thirdType;
    }

    public void setThirdType(Integer thirdType) {
        this.thirdType = thirdType;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMsCode() {
        return msCode;
    }

    public void setMsCode(String msCode) {
        this.msCode = msCode;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

    public String getWxuid() {
        return wxuid;
    }

    public void setWxuid(String wxuid) {
        this.wxuid = wxuid;
    }

    public String getUnionidWx() {
        return unionidWx;
    }

    public void setUnionidWx(String unionidWx) {
        this.unionidWx = unionidWx;
    }
}
