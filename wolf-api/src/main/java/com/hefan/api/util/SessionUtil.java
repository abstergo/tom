package com.hefan.api.util;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;

/**
 * 会话工具类
 *
 * @author diguage
 * @since 2016/01/27
 */
public class SessionUtil {
    /**
     * 获取当前会话
     *
     * @return
     */
    public static Session getCurrentSession() {
        Session session = SecurityUtils.getSubject().getSession();
        return session;
    }


}
