package com.hefan.api.util;

/**
 * User: criss
 * Date: 16/9/30
 * Time: 15:22
 */
public class Constants {

    public static final String HTTP_METHOD_GET = "GET" ;

    public static final String HTTP_METHOD_POST = "POST" ;

    public static final String HTTP_METHOD_KEY = "_key_" ;

    public static final String HTTP_CHARSET = "UTF-8" ;

    public static final String HTTP_JSON = "application/json; charset=utf-8" ;

    public static final long HTTP_TIMEOUT = 60000;

}
