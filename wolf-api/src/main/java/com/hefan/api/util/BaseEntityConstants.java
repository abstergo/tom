package com.hefan.api.util;

/**
 * 数据库对应实体状态
 * @author sagagyq
 *
 */
public class BaseEntityConstants {
	 /**
	  * 被删除状态
	  */
	 public static final Integer ISDEL = 1;
	 /**
	  * 没有被删除状态
	  */
	 public static final Integer NODEL = 0;
	 /**
	  * 被冻结状态
	  */
	 public static final Integer ISFROZEN = 1;
	 /**
	  * 没有被冻结状态
	  */
	 public static final Integer NOFROZEN = 0;
	 /**
	  * 被解约的状态
	  */
	 public static final Integer SURREDDERED=1;
	 
	 /**
	  * 没有被解约的状态
	  */
	 public static final Integer NOSURREDDERED=0;
}
