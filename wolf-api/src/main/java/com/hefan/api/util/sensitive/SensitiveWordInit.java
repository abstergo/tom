package com.hefan.api.util.sensitive;

import com.cat.tiger.util.HttpClientUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * 初始化敏感词库，将敏感词加入到HashMap中，构建DFA算法模型
 *
 * @ClassName: SensitiveWordInit
 * @Description:TODO(这里用一句话描述这个类的作用)
 * @author: LiTeng
 * @date: 2016年10月29日 上午10:42:38
 */
public class SensitiveWordInit {
  private String ENCODING = "UTF-8";    //字符编码
  @SuppressWarnings("rawtypes")
  public HashMap sensitiveWordMap;

  public SensitiveWordInit() {
    super();
  }

  /**
   * 初始化敏感词
   *
   * @return
   * @throws
   * @Title: initKeyWord
   * @Description: TODO(这里用一句话描述这个方法的作用)
   * @return: Map
   * @author: LiTeng
   * @date: 2016年10月29日 上午10:42:54
   */
  @SuppressWarnings("rawtypes")
  public Map initKeyWord() {
    try {
      //读取敏感词库
      Set<String> keyWordSet = readSensitiveWordFile();
      //将敏感词库加入到HashMap中
      addSensitiveWordToHashMap(keyWordSet);
      //spring获取application，然后application.setAttribute("sensitiveWordMap",sensitiveWordMap);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sensitiveWordMap;
  }

  /**
   * 读取敏感词库，将敏感词放入HashSet中，构建一个DFA算法模型：<br>
   * 中 = {
   * isEnd = 0
   * 国 = {<br>
   * isEnd = 1
   * 人 = {isEnd = 0
   * 民 = {isEnd = 1}
   * }
   * 男  = {
   * isEnd = 0
   * 人 = {
   * isEnd = 1
   * }
   * }
   * }
   * }
   * 五 = {
   * isEnd = 0
   * 星 = {
   * isEnd = 0
   * 红 = {
   * isEnd = 0
   * 旗 = {
   * isEnd = 1
   * }
   * }
   * }
   * }
   *
   * @param keyWordSet
   * @throws
   * @Title: addSensitiveWordToHashMap
   * @Description: TODO(这里用一句话描述这个方法的作用)
   * @return: void
   * @author: LiTeng
   * @date: 2016年10月29日 上午10:43:39
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  private void addSensitiveWordToHashMap(Set<String> keyWordSet) {
    sensitiveWordMap = new HashMap(keyWordSet.size());     //初始化敏感词容器，减少扩容操作
    String key = null;
    Map nowMap = null;
    Map<String, String> newWorMap = null;
    //迭代keyWordSet
    Iterator<String> iterator = keyWordSet.iterator();
    while (iterator.hasNext()) {
      key = iterator.next();    //关键字
      nowMap = sensitiveWordMap;
      for (int i = 0; i < key.length(); i++) {
        char keyChar = key.charAt(i);       //转换成char型
        Object wordMap = nowMap.get(keyChar);       //获取

        if (wordMap != null) {        //如果存在该key，直接赋值
          nowMap = (Map) wordMap;
        } else {     //不存在则，则构建一个map，同时将isEnd设置为0，因为他不是最后一个
          newWorMap = new HashMap<String, String>();
          newWorMap.put("isEnd", "0");     //不是最后一个
          nowMap.put(keyChar, newWorMap);
          nowMap = newWorMap;
        }

        if (i == key.length() - 1) {
          nowMap.put("isEnd", "1");    //最后一个
        }
      }
    }
  }

  /**
   * 读取敏感词库中的内容，将内容添加到set集合中
   *
   * @return
   * @throws Exception
   * @throws
   * @Title: readSensitiveWordFile
   * @Description: TODO(这里用一句话描述这个方法的作用)
   * @return: Set<String>
   * @author: LiTeng
   * @date: 2016年10月29日 上午11:09:24
   */
  @SuppressWarnings("resource")
  private Set<String> readSensitiveWordFile() throws Exception {
    Set<String> set = new HashSet<>();

    String sensitiveWord = HttpClientUtils.doGet("http://hefanimage.oss-cn-beijing-internal.aliyuncs.com/hefantv/SensitiveWord.txt", null);
    if (StringUtils.isBlank(sensitiveWord)) {
      return set;
    }
    String[] words = sensitiveWord.split("\n");
    try {
      set = new HashSet(Arrays.asList(words));
    } catch (Exception e) {
      throw e;
    } finally {
      return set;
    }
    /*
		 URL oracle = new URL(DynamicProperties.getString("oss.sensitive.word"));    //读取文件
    String sensitiveWord = HttpClientUtils.doGet(DynamicProperties.getString("oss.sensitive.word"), null);
		InputStreamReader read = new InputStreamReader(oracle.openStream(),ENCODING);
    try {
			BufferedReader bufferedReader = new BufferedReader(read);
			String txt = null;
			while((txt = bufferedReader.readLine()) != null){    //读取文件，将文件内容放入到set中
				set.add(txt);
		    }
		} catch (Exception e) {
			throw e;
		}finally{
			read.close();     //关闭文件流
		}*/
  }

}
