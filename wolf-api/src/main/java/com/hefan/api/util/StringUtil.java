package com.hefan.api.util;

import java.util.ArrayList;
import java.util.List;

public class StringUtil {
		
	/**
     * 是否是逗号分隔的字符串(字母数字下划线和'-')   ""  和null 返回true
     * @param src the src
     * @return the boolean
     */
    public static boolean isCommaSplitStr(String src){
        if(src == null) return true;
        if("".equals(src)) return true;
        return src.matches("([a-zA-Z0-9-_]+[,])*([a-zA-Z0-9-_]+)");
    }
	
	
	 /**
     * 自定义的分隔字符串函数 例如: 1,2,3 =>[1,2,3] 3个元素 ,2,3=>[,2,3] 3个元素 ,2,3,=>[,2,3,] 4个元素 ,,,=>[,,,] 4个元素
     * 5.22算法修改，为提高速度不用正则表达式 两个间隔符,,返回""元素
     * @param split 分割字符 默认,
     * @param src 输入字符串
     * @return 分隔后的list
     */
    public static List<String> splitToList(String split, String src) {
        // 默认,
        String sp = ",";
        if (split != null && split.length() == 1) {
            sp = split;
        }
        List<String> r = new ArrayList<String>();
        int lastIndex = -1;
        int index = src.indexOf(sp);
        if (-1 == index && src != null) {
            r.add(src);
            return r;
        }
        while (index >= 0) {
            if (index > lastIndex) {
                r.add(src.substring(lastIndex + 1, index));
            } else {
                r.add("");
            }

            lastIndex = index;
            index = src.indexOf(sp, index + 1);
            if (index == -1) {
                r.add(src.substring(lastIndex + 1, src.length()));
            }
        }
        return r;
    }
}
