package com.hefan.api.util;


import com.aliyun.opensearch.CloudsearchClient;
import com.aliyun.opensearch.object.KeyTypeEnum;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hbchen on 2016/10/21.
 */
public class OpenSearchUtil {
    private static final String ACCESSKEY = DynamicProperties.getApplicationString("oss.AccessKey");
    private static final String SECRET = DynamicProperties.getApplicationString("oss.SecretKey");
    private static final String URL = DynamicProperties.getApplicationString("opensearch.url");
    private static Object lock = new Object();
    private static CloudsearchClient client;

    public static CloudsearchClient getInstance() throws UnknownHostException {
        if (client == null) {
            synchronized (lock) {
                if (client == null) {
                    Map<String, Object> opts = new HashMap<String, Object>();
                    opts.put("format","json");
                    client = new CloudsearchClient(ACCESSKEY, SECRET ,URL, opts, KeyTypeEnum.ALIYUN);
                }
            }
        }
        return client;
    }

}
