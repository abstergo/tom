package com.hefan.api.util;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
public class DynamicProperties implements Serializable {

    private static final long serialVersionUID = 4697358361020431226L;

    private static final String DEFAULT_SPLIT_CHAR = ",";

    private static PropertiesConfiguration applicationConfig = null;

    static {
        try {
            PropertiesConfiguration.setDefaultListDelimiter('+');
            applicationConfig = new PropertiesConfiguration("application.properties");
            applicationConfig.setReloadingStrategy(new FileChangedReloadingStrategy());
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static int getApplicationInt(String key) {
        return applicationConfig.getInt(key);
    }

    public static long getApplicationLong(String key) {
        return applicationConfig.getLong(key);
    }

    public static String getApplicationString(String key) {
        return applicationConfig.getString(key);
    }

    public static boolean getApplicationBoolean(String key) {
        return applicationConfig.getBoolean(key);
    }
    public static String[] getApplicationStringArr(String key) {
        return applicationConfig.getString(key).split(DEFAULT_SPLIT_CHAR);
    }

}
