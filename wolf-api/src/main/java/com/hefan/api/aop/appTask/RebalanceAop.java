package com.hefan.api.aop.appTask;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Repository;

import com.alibaba.dubbo.common.logger.Logger;
import com.alibaba.dubbo.common.logger.LoggerFactory;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cat.common.entity.ResultBean;
import com.cat.tiger.util.CollectionUtils;
import com.cat.tiger.util.GlobalConstants;
import com.hefan.activity.bean.AppTaskInfo;
import com.hefan.activity.bean.AppTaskRelation;
import com.hefan.activity.bean.AppTaskVo;
import com.hefan.api.service.activity.AppTaskLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.oms.bean.RebalanceVo;

/**
 * 直播间给主播送礼物   type=3  直播间中发弹幕  type=6
 * @ClassName:  HFDynamicAop   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: LiTeng  
 * @date:   2016年11月17日 下午3:16:54   
 *
 */
@Aspect
@Repository
public class RebalanceAop {
	@Resource
	AppTaskLocalService appTaskLocalService;
	
	private Logger logger = LoggerFactory.getLogger(CommentsGiftAop.class);
	
	@AfterReturning(value="execution(* com.hefan.api.controller.oms.ReBalanceController.rebalance(..))",returning="rtv")
    public void afterInsertMethod(JoinPoint jp, Object rtv) throws Throwable {  
		//返回成功才处理
		try {
			if(((ResultBean)rtv).getCode()==1000){
				HttpServletRequest request = (HttpServletRequest) jp.getArgs()[0];
				RebalanceVo rebalanceVo = HttpUtils.initParam(request,RebalanceVo.class);
				System.out.println("userId:"+rebalanceVo.getFromId());
				//送礼
				if(rebalanceVo.getSource() == GlobalConstants.SOURCE_TYPE_LIVE){
					this.giteReb(rebalanceVo);
			    }else if(rebalanceVo.getSource() == GlobalConstants.SOURCE_TYPE_BARRAGE){
			    	//弹幕
			    	this.dealBarrage(rebalanceVo);
			    }
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("直播间给主播送礼物任务处理失败:"+e.getMessage());
		}
    }  
	
	/**
	 * 礼物礼物
	 * @Title: giteReb   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param rebalanceVo
	 * @return      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月17日 下午5:18:39
	 */
	private boolean giteReb(RebalanceVo rebalanceVo){
		AppTaskRelation taskRe = appTaskLocalService.getAppTaskRelationByUserId(rebalanceVo.getFromId());
		AppTaskInfo apt = appTaskLocalService.getAppTaskInfoListByType(3);
//		Map<String,Object> numMap =appTaskLocalService.getGiftUserNum(rebalanceVo.getFromId(), rebalanceVo.getToId(), apt.getBeginTime(), apt.getEndTime(), GlobalConstants.SOURCE_TYPE_LIVE);
//		int uCount = 0;
		int uHas = 0;
//		if(!CollectionUtils.isEmpty(numMap)){
//			uCount = Integer.parseInt(numMap.get("u_count").toString());
//			uHas = Integer.parseInt(numMap.get("u_has").toString());
//		}
		if(new Date().before(apt.getBeginTime()) || new Date().after(apt.getEndTime()) || apt.getDeleteFlag() !=0){
			return false;
		}
		List<AppTaskVo> voList = new ArrayList<AppTaskVo>();
		if(taskRe == null){
			//新增
			AppTaskVo vo = new AppTaskVo();
			vo.setTaskType(apt.getTaskType());
			vo.setTaskName(apt.getTaskName());
			//任务说明
			vo.setTaskExplain(apt.getTaskExplain());
			//任务图标
			vo.setTaskIcon(apt.getTaskIcon());
			//完成能获得的经验
			vo.setExpValue(apt.getExpValue());
			//完成能获得的饭票
			vo.setTicketValue(apt.getTicketValue());
			//进度
			vo.setSetbacks(1);
			//状态  1-未领取  2-未完成  4-已领取
			vo.setTaskStatus((apt.getScaleValue()-vo.getSetbacks())<=0?1:2);
			//总刻度 （任务完成需要的数量）
			vo.setScaleValue(apt.getScaleValue());
			//拓展字段，已送主播id
			vo.setTaskExpand(rebalanceVo.getToId());
			voList.add(vo);
			String tasksJson = JSONObject.toJSONString(voList);
			AppTaskRelation atr =  new AppTaskRelation();
			atr.setUserId(rebalanceVo.getFromId());
			atr.setTasksJson(tasksJson);
			atr.setDeleteFlag(0);
			atr.setCreateTime(new Date());
			atr.setUpdateTime(new Date());
			appTaskLocalService.saveOrUpAppTaskRelation(atr);
		}else{
			//修改
			if(taskRe !=null && StringUtils.isNotBlank(taskRe.getTasksJson())){
	    		voList = JSON.parseArray(taskRe.getTasksJson(), AppTaskVo.class);
	    	}
			int f=0;
			for(int i=0;i<voList.size();i++){
				AppTaskVo vo = voList.get(i);
				if(vo.getTaskType()==3){
					String [] toUs = new String[]{};
					if(StringUtils.isNotBlank(vo.getTaskExpand())){
						toUs = vo.getTaskExpand().split(",");
					}
					if(toUs !=null && toUs.length >0){
						for(int x =0;x<toUs.length;x++){
							if(toUs[x].equals(rebalanceVo.getToId())){
								uHas =1;
							}
						}
					}
					if(vo.getTaskStatus()==2 && uHas ==0){
						f=1;
						//进度
						vo.setSetbacks(vo.getSetbacks()+1);
						//状态  1-未领取  2-未完成  4-已领取
						vo.setTaskStatus((apt.getScaleValue()-vo.getSetbacks())<=0?1:2);
						//总刻度 （任务完成需要的数量）
						vo.setScaleValue(apt.getScaleValue());
						//拓展字段，已送主播id
						vo.setTaskExpand(vo.getTaskExpand()+","+rebalanceVo.getToId());
					}else{
						f=2;
					}
				}
			}
			if(f==0){
				AppTaskVo vo = new AppTaskVo();
				vo.setTaskType(apt.getTaskType());
				vo.setTaskName(apt.getTaskName());
				//任务说明
				vo.setTaskExplain(apt.getTaskExplain());
				//任务图标
				vo.setTaskIcon(apt.getTaskIcon());
				//完成能获得的经验
				vo.setExpValue(apt.getExpValue());
				//完成能获得的饭票
				vo.setTicketValue(apt.getTicketValue());
				//进度
				vo.setSetbacks(1);
				//状态  1-未领取  2-未完成  4-已领取
				vo.setTaskStatus((apt.getScaleValue()-vo.getSetbacks())<=0?1:2);
				//总刻度 （任务完成需要的数量）
				vo.setScaleValue(apt.getScaleValue());
				//拓展字段，已送主播id
				vo.setTaskExpand(rebalanceVo.getToId());
				voList.add(vo);
			}
			if(f==0 || f==1){
				String tasksJson = JSONObject.toJSONString(voList);
				taskRe.setTasksJson(tasksJson);
				appTaskLocalService.saveOrUpAppTaskRelation(taskRe);
			}
		}
		return true;
	}
	
	private boolean dealBarrage(RebalanceVo rebalanceVo){
		AppTaskInfo apt = appTaskLocalService.getAppTaskInfoListByType(6);
		if(new Date().before(apt.getBeginTime()) || new Date().after(apt.getEndTime()) || apt.getDeleteFlag() !=0){
			return false;
		}
		AppTaskRelation taskRe = appTaskLocalService.getAppTaskRelationByUserId(rebalanceVo.getFromId());
		List<AppTaskVo> voList = new ArrayList<AppTaskVo>();
		if(taskRe == null){
			//新增
			AppTaskVo vo = new AppTaskVo();
			vo.setTaskType(apt.getTaskType());
			vo.setTaskName(apt.getTaskName());
			//任务说明
			vo.setTaskExplain(apt.getTaskExplain());
			//任务图标
			vo.setTaskIcon(apt.getTaskIcon());
			//完成能获得的经验
			vo.setExpValue(apt.getExpValue());
			//完成能获得的饭票
			vo.setTicketValue(apt.getTicketValue());
			//状态  1-未领取  2-未完成  4-已领取
			vo.setTaskStatus(1);
			//进度
			vo.setSetbacks(apt.getScaleValue());
			//总刻度 （任务完成需要的数量）
			vo.setScaleValue(apt.getScaleValue());
			voList.add(vo);
			String tasksJson = JSONObject.toJSONString(voList);
			AppTaskRelation atr =  new AppTaskRelation();
			atr.setUserId(rebalanceVo.getFromId());
			atr.setTasksJson(tasksJson);
			atr.setDeleteFlag(0);
			atr.setCreateTime(new Date());
			atr.setUpdateTime(new Date());
			appTaskLocalService.saveOrUpAppTaskRelation(atr);
		}else{
			//修改
			if(taskRe !=null && StringUtils.isNotBlank(taskRe.getTasksJson())){
	    		voList = JSON.parseArray(taskRe.getTasksJson(), AppTaskVo.class);
	    	}
			int f=0;
			for(int i=0;i<voList.size();i++){
				AppTaskVo vo = voList.get(i);
				if(vo.getTaskType()==6){
					f=1;
				}
			}
			if(f==0){
				AppTaskVo vo = new AppTaskVo();
				vo.setTaskType(apt.getTaskType());
				vo.setTaskName(apt.getTaskName());
				//任务说明
				vo.setTaskExplain(apt.getTaskExplain());
				//任务图标
				vo.setTaskIcon(apt.getTaskIcon());
				//完成能获得的经验
				vo.setExpValue(apt.getExpValue());
				//完成能获得的饭票
				vo.setTicketValue(apt.getTicketValue());
				//状态  1-未领取  2-未完成  4-已领取
				vo.setTaskStatus(1);
				//进度
				vo.setSetbacks(apt.getScaleValue());
				//总刻度 （任务完成需要的数量）
				vo.setScaleValue(apt.getScaleValue());
				voList.add(vo);
				String tasksJson = JSONObject.toJSONString(voList);
				taskRe.setTasksJson(tasksJson);
				appTaskLocalService.saveOrUpAppTaskRelation(taskRe);
			}
		}
		return true;
	}
}
