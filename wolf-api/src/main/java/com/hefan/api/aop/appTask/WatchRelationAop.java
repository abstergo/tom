package com.hefan.api.aop.appTask;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import com.alibaba.dubbo.rpc.Result;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Repository;

import com.alibaba.dubbo.common.logger.Logger;
import com.alibaba.dubbo.common.logger.LoggerFactory;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hefan.activity.bean.AppTaskInfo;
import com.hefan.activity.bean.AppTaskRelation;
import com.hefan.activity.bean.AppTaskVo;
import com.hefan.api.service.activity.AppTaskLocalService;

/**
 * 关注任务  type=4
 * @ClassName:  WatchRelationAop   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: LiTeng  
 * @date:   2016年11月17日 下午3:09:58   
 *
 */
@Aspect
@Repository
public class WatchRelationAop {
	
	@Resource
	AppTaskLocalService appTaskLocalService;
	
	private Logger logger = LoggerFactory.getLogger(WatchRelationAop.class);
	
	@AfterReturning(value="execution(* com.hefan.api.service.WatchLocalService.fork(..))",returning="rtv")
    public void afterInsertMethod(JoinPoint jp, Object rtv) {
		//返回成功才处理
		try {
			logger.info("关注抽奖任务执行参数：" + JSON.toJSONString(rtv));
			ResultBean resultBean = JSON.parseObject(JSON.toJSONString(rtv), ResultBean.class);
			if (null != resultBean && resultBean.getCode() == ResultCode.SUCCESS.get_code()) {
				String userId = jp.getArgs()[0].toString();
				logger.info("关注抽奖任务执行参数 userId：" + userId);
				this.dealWatchRelation(userId);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("关注任务处理失败:" + e);
		}
	}

	private boolean dealWatchRelation(String userId){
		AppTaskInfo apt = appTaskLocalService.getAppTaskInfoListByType(4);
		logger.info("关注抽奖任务获取信息！"+ JSON.toJSONString(apt));
		if(null == apt || new Date().before(apt.getBeginTime()) || new Date().after(apt.getEndTime()) || apt.getDeleteFlag() !=0){
			return false;
		}
		AppTaskRelation taskRe = appTaskLocalService.getAppTaskRelationByUserId(userId);
		List<AppTaskVo> voList = new ArrayList<AppTaskVo>();
		if(taskRe == null){
			logger.info("关注抽奖任务执行初始化！");
			//新增
			AppTaskVo vo = new AppTaskVo();
			vo.setTaskType(apt.getTaskType());
			vo.setTaskName(apt.getTaskName());
			//任务说明
			vo.setTaskExplain(apt.getTaskExplain());
			//任务图标
			vo.setTaskIcon(apt.getTaskIcon());
			//完成能获得的经验
			vo.setExpValue(apt.getExpValue());
			//完成能获得的饭票
			vo.setTicketValue(apt.getTicketValue());
			//进度
			vo.setSetbacks(1);
			//状态  1-未领取  2-未完成  4-已领取
			vo.setTaskStatus((apt.getScaleValue()-vo.getSetbacks())<=0?1:2);
			//总刻度 （任务完成需要的数量）
			vo.setScaleValue(apt.getScaleValue());
			voList.add(vo);
			String tasksJson = JSONObject.toJSONString(voList);
			AppTaskRelation atr =  new AppTaskRelation();
			atr.setUserId(userId);
			atr.setTasksJson(tasksJson);
			atr.setDeleteFlag(0);
			atr.setCreateTime(new Date());
			atr.setUpdateTime(new Date());
			appTaskLocalService.saveOrUpAppTaskRelation(atr);
		}else{
			logger.info("关注抽奖任务执行更新！");
			//修改
			if(taskRe !=null && StringUtils.isNotBlank(taskRe.getTasksJson())){
	    		voList = JSON.parseArray(taskRe.getTasksJson(), AppTaskVo.class);
	    	}
			int f=0;
			for(int i=0;i<voList.size();i++){
				AppTaskVo vo = voList.get(i);
				if(vo.getTaskType()==4){
					if(vo.getTaskStatus()==2){
						f=1;
						//进度
						vo.setSetbacks(vo.getSetbacks()+1);
						//状态  1-未领取  2-未完成  4-已领取
						vo.setTaskStatus((apt.getScaleValue()-vo.getSetbacks())<=0?1:2);
						//总刻度 （任务完成需要的数量）
						vo.setScaleValue(apt.getScaleValue());
					}else{
						f=2;
					}
				}
			}
			if(f==0){
				AppTaskVo vo = new AppTaskVo();
				vo.setTaskType(apt.getTaskType());
				vo.setTaskName(apt.getTaskName());
				//任务说明
				vo.setTaskExplain(apt.getTaskExplain());
				//任务图标
				vo.setTaskIcon(apt.getTaskIcon());
				//完成能获得的经验
				vo.setExpValue(apt.getExpValue());
				//完成能获得的饭票
				vo.setTicketValue(apt.getTicketValue());
				//进度
				vo.setSetbacks(1);
				//状态  1-未领取  2-未完成  4-已领取
				vo.setTaskStatus((apt.getScaleValue()-vo.getSetbacks())<=0?1:2);
				//总刻度 （任务完成需要的数量）
				vo.setScaleValue(apt.getScaleValue());
				voList.add(vo);
			}
			if(f==0 || f==1){
				String tasksJson = JSONObject.toJSONString(voList);
				taskRe.setTasksJson(tasksJson);
				appTaskLocalService.saveOrUpAppTaskRelation(taskRe);
			}
		}
		logger.error("关注抽奖任务执行成功！");
		return true;
	}
}
