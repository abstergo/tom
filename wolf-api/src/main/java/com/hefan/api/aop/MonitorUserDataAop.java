package com.hefan.api.aop;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.alibaba.fastjson.JSON;
import com.hefan.api.service.monitor.MonitorDataLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.monitor.bean.MoniProfileDataVo;
import com.hefan.monitor.bean.MoniUserDataVo;
import com.hefan.user.bean.WebUser;

/**
 * 资料监控aop-用户资料
 * @ClassName:  MonitorDataAop
 * @Description:TODO(这里用一句话描述这个类的作用)
 * @author: LiTeng
 * @date:   2016年12月2日 下午6:06:21   
 *
 */
@Aspect
@Repository
public class MonitorUserDataAop {

	@Resource
	MonitorDataLocalService monitorDataLocalService;

	private Logger logger = LoggerFactory.getLogger(MonitorUserDataAop.class);

	@AfterReturning(value = "execution(* com.hefan.api.service.UserLocalService.saveRegisterUserInfo(..))"
			+ " || execution(* com.hefan.api.controller.user.UserController.editUserInfo(..))"
			+ " || execution(* com.hefan.api.controller.user.UserController.modifyPersonalProfile(..))", returning = "rtv")
	public void afterInsertMethod(JoinPoint jp, Object rtv) throws Throwable {
		// 返回成功才处理
		Signature signature = jp.getSignature();
		logger.info("MonitorUserDataAop:监控资料开始");
		try {
			String aopName = signature.getName();
			if(aopName.equals("saveRegisterUserInfo")){
				logger.info("MonitorUserDataAop:[saveRegisterUserInfo]:"+JSON.parseObject(JSON.toJSONString(rtv)).getString("code"));
			}
			if(aopName.equals("saveRegisterUserInfo") && JSON.parseObject(JSON.toJSONString(rtv)).getString("code").equals("1000")){
				//处理新增用户
				WebUser user = (WebUser) jp.getArgs()[0];
				logger.info("MonitorUserDataAop:[saveRegisterUserInfo-webUser]:"+JSON.toJSONString(user));
				this.addUserInterface(user);
			}else if(aopName.equals("editUserInfo") && JSON.parseObject(rtv.toString()).getString("code").equals("1000")){
				//处理修改个人信息
				HttpServletRequest request = (HttpServletRequest) jp.getArgs()[0];
				WebUser user1 = HttpUtils.initParam(request,WebUser.class);
				this.updateUserInterface(user1);
			}else if(aopName.equals("modifyPersonalProfile") && JSON.parseObject(rtv.toString()).getString("code").equals("1000")){
				//处理个人简介
				HttpServletRequest request1 = (HttpServletRequest) jp.getArgs()[0];
				Map paramsMap = HttpUtils.initParam(request1,HashMap.class);
				this.updatePersonalProfile(paramsMap);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("MonitorUserDataAop:监控资料用户部分加入队列失败:" + e.getMessage()+"["+signature.getName()+"]，入参："+jp.getArgs()[0]+",接口返回："+rtv.toString());
		}
	}

	/**
	 * 新增用户接口
	 * @Title: addUserInterface
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param user
	 * @return: void
	 * @author: LiTeng
	 * @throws
	 * @date:   2016年12月5日 下午7:01:52
	 */
	public void addUserInterface(WebUser user){
		WebUser ur = this.monitorDataLocalService.getWebUserByUserId(user.getUserId());
		MoniUserDataVo vo = new MoniUserDataVo();
		vo.setUserId(user.getUserId());
		vo.setUserType(ur.getUserType());
		vo.setNickName(user.getNickName());
		vo.setHeadImg(user.getHeadImg());
		vo.setPersonSign(user.getPersonSign());
		vo.setOptFrom(0);
		logger.info("MonitorUserDataAop:[saveRegisterUserInfo-MoniUserDataVo]:"+JSON.toJSONString(vo));
		this.monitorDataLocalService.sendMessToQueue("1", JSON.toJSONString(vo));
	}

	/**
	 * 修改用户信息
	 * @Title: updateUserInterface
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param user
	 * @return: void
	 * @author: LiTeng
	 * @throws
	 * @date:   2016年12月5日 下午7:05:20
	 */
	public void updateUserInterface(WebUser user){
		WebUser ur = this.monitorDataLocalService.getWebUserByUserId(user.getUserId());
		MoniUserDataVo vo = new MoniUserDataVo();
		vo.setUserId(user.getUserId());
		vo.setUserType(ur.getUserType());
		vo.setNickName(user.getNickName());
		vo.setHeadImg(user.getHeadImg());
		vo.setPersonSign(user.getPersonSign());
		vo.setOptFrom(0);
		this.monitorDataLocalService.sendMessToQueue("1", JSON.toJSONString(vo));
	}

	private void updatePersonalProfile(Map map){
		String userId = String.valueOf(map.get("userId"));
		String nickName = String.valueOf(map.get("nickName"));
		WebUser user = monitorDataLocalService.getWebUserByUserId(userId);
		if(user !=null && user.getId()>0 && !user.getNickName().equals(nickName)){
			MoniUserDataVo vo = new MoniUserDataVo();
			vo.setUserId(userId);
			vo.setUserType(user.getUserType());
			vo.setNickName(nickName);
			vo.setHeadImg("");
			vo.setPersonSign("");
			vo.setOptFrom(0);
			this.monitorDataLocalService.sendMessToQueue("1", JSON.toJSONString(vo));
		}
		MoniProfileDataVo vpo = new MoniProfileDataVo();
		vpo.setUserId(userId);
		vpo.setUserType(user.getUserType());
		vpo.setInfoFile("");
		vpo.setBgimg(String.valueOf(map.get("bgimg")));
		vpo.setProfiles(String.valueOf(map.get("profiles")));
		vpo.setOptFrom(0);
		this.monitorDataLocalService.sendMessToQueue("2", JSON.toJSONString(vpo));
	}
}
