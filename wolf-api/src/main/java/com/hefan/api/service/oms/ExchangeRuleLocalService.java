package com.hefan.api.service.oms;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.common.meta.UserTypeEnum;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.api.service.UserLocalService;
import com.hefan.oms.bean.ExchangeRuleVo;
import com.hefan.oms.itf.AnchorTicketMonthService;
import com.hefan.oms.itf.ExchangeRuleService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WebUserAccountService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ExchangeRuleLocalService {
	
	
	@Reference
	ExchangeRuleService exchangeRuleService;
	@Reference
	AnchorTicketMonthService anchorTicketMonthService;
	@Resource
	private UserLocalService userLocalService;
	@Reference
	private WebUserAccountService webUserAccountService;
	
	public Map<String,Object> ExchangeRuleForHeFanOrFanPiaoByUserId(String userId) {
		return	exchangeRuleService.ExchangeRuleForHeFanOrFanPiaoByUserId(userId);
	}

	public Map<String,Object> ExchangeRuleForHeFanOrFanPiaoByUserId(String userId, int userType) {
		return	exchangeRuleService.ExchangeRuleForHeFanOrFanPiaoByUserId(userId, userType);
	}

	/**
	 * 根据用户id获取兑换中心相关数据
	 * 【
	 * ①初始化饭票兑换盒饭的列表
	 * ②兑换比例用于自定义兑换
	 * ③可兑换饭票数
	 * 】
	 * @param userId
	 * @return
     */
	public ResultBean exchangeRuleAndData(String userId){
		ResultBean resultBean = new  ResultBean(ResultCode.SUCCESS);
		Map<String ,Object> returnMap = new HashMap<String ,Object>();
		WebUser webUser = userLocalService.findUserInfoFromCache(userId);
		if(webUser == null) {
			return new ResultBean(ResultCode.ParamException.get_code(),"用户信息不存在");
		}
		List<Integer> fanpiaoList =  exchangeRuleService.getCurrencyExchangeCenterPrice();
		Integer hefanTotal = 0;
		if(UserTypeEnum.isAnchor(webUser.getUserType())) { //主播
			hefanTotal = anchorTicketMonthService.selectAvailableTickets(userId);
		} else { //普通用户
			hefanTotal = Long.valueOf(webUserAccountService.getBaseUserIncome(userId)).intValue();
		}
		//可兑换盒饭数
		Integer hefanNum = 0;
		Map<String,Object> exchangeRuleMap = exchangeRuleService.ExchangeRuleForHeFanOrFanPiaoByUserId(userId,webUser.getUserType());
		if(CollectionUtils.isEmpty(exchangeRuleMap)) {
			return new ResultBean(ResultCode.UNSUCCESS.get_code(),"兑换比例配置错误");
		}
		List<ExchangeRuleVo> exchangeRuleVoList = new ArrayList<ExchangeRuleVo>();
		for(Integer fanpiao :fanpiaoList){
			ExchangeRuleVo exchangeRuleVo = new ExchangeRuleVo();
			exchangeRuleVo.setFanpiaoNum(fanpiao);
			hefanNum = getHefanNum(exchangeRuleMap,fanpiao);
			exchangeRuleVo.setHefanNum(hefanNum);
			if(hefanTotal>=hefanNum){
				exchangeRuleVo.setFlag(1);
			}
			exchangeRuleVoList.add(exchangeRuleVo);
		}
		//兑换比例
		returnMap.put("exchangeRule",exchangeRuleMap);
		//默认兑换初始化数据
		returnMap.put("exchangeRuleVoList" ,exchangeRuleVoList);
		//可兑换饭票数
		returnMap.put("hefanTotal",hefanTotal);
		resultBean.setData(returnMap);
		return resultBean;
	}


	private Integer getHefanNum(Map<String,Object> map ,Integer fanpiao){
		Integer beExchangeRatio = (Integer)map.get("beExchangeRatio");
		Integer exchangeRatio = (Integer)map.get("exchangeRatio");
		double exchangeRatioRule = ((double)beExchangeRatio)/exchangeRatio;
		BigDecimal bd = new BigDecimal(fanpiao*exchangeRatioRule).setScale(0, BigDecimal.ROUND_HALF_UP);
		Integer result = bd.intValue();
		return result;
	}

}
