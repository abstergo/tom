package com.hefan.api.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.Page;
import com.hefan.club.dynamic.bean.TripInfo;
import com.hefan.club.dynamic.itf.TripInfoService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class TripInfoLocalService {
	
	@Reference
	TripInfoService tripInfoService;
	
	public List findTripsTypeInfoList(){
		return this.tripInfoService.findTripsTypeInfoList();
	}
	
	public int findTripsCountByTime(String userId,String time,String timeFormat,long tripId){
		return this.tripInfoService.findTripsCountByTime(userId, time, timeFormat, tripId);
	}
	
	public int saveTripInfo(TripInfo tripInfo) {
		return this.tripInfoService.saveTripInfo(tripInfo);
	}
	
	public int findTripsTypeCountById(String type){
		return this.tripInfoService.findTripsTypeCountById(type);
	}
	
	public int updateTripInfo(TripInfo tripInfo){
		return this.tripInfoService.updateTripInfo(tripInfo);
	}
	
	public int deleteTripInfo(long id){
		return this.tripInfoService.deleteTripInfo(id);
	}
	
	public Map findTripsInfoLately(String userId,String isOpen){
		return this.tripInfoService.findTripsInfoLately(userId, isOpen);
	}
	
	public List findTripsForInit(String userId, String isOpen, int mpageSize, String time) {
		return this.tripInfoService.findTripsForInit(userId, isOpen, mpageSize, time);
	}
	
	public List findTripsByTimeDirection(String userId, String isOpen, String tripTime, int mpageSize, String direction) {
		return this.tripInfoService.findTripsByTimeDirection(userId, isOpen, tripTime, mpageSize, direction);
	}
	
	public Page findTripsPage(String userId, String isOpen,Page page) {
		return this.tripInfoService.findTripsPage(userId, isOpen, page);
	}
	
	public List findTripsStarChart(String userId, String isOpen,String time){
		return this.tripInfoService.findTripsStarChart(userId, isOpen, time);
	}

}
