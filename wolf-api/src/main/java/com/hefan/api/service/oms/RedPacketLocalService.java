package com.hefan.api.service.oms;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.oms.bean.RedPacket;
import com.hefan.oms.itf.RedPacketService;
import org.springframework.stereotype.Component;

/**
 * Created by hbchen on 2016/10/12.
 */

@Component
public class RedPacketLocalService {
    @Reference
    RedPacketService redPacketService;

    public RedPacket selectRedPackInfo(int id){
        return redPacketService.selectRedPackInfo(id);
    }

    /**
     * 红包超时或者被领光了，更新主表红包领取状态。
     * @param orderId
     */
    public void updateRedPackOrderStatus(long orderId)throws Exception{
        redPacketService.updateRedPackOrderStatus(orderId);
    }

    /**
     * 红包被抢到了，更新一下领取信息
     * @param detailId
     * @param userId
     */
    public void updateRedPackDetail(long detailId,String userId)throws Exception{
        redPacketService.updateRedPackDetail(detailId,userId);
    }

    /**
     * 判断领取明细count来判断，整个大红包是否领光了
     * @param orderId
     * @return
     */
    public int selectRedPackDetailCount(long orderId){
        return redPacketService.selectRedPackDetailCount(orderId);
    }
}
