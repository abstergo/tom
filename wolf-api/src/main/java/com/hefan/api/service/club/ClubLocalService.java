package com.hefan.api.service.club;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hefan.club.dynamic.bean.Message;
import com.hefan.club.dynamic.itf.DynamicService;
import com.hefan.club.dynamic.itf.SquareService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by nigle on 2017/3/4.
 */
@Component
public class ClubLocalService {

    @Reference
    SquareService squareService;
    @Reference
    DynamicService dynamicService;

    public Logger logger = LoggerFactory.getLogger(ClubLocalService.class);
    /**
     * 广场首页信息
     * @return
     */
    public String squareIndex(Page po,String userId) {
        ResultBean resultBean = new ResultBean();
        try {
            po = squareService.nextPageSquare("", userId, po);
            Map<String, Object> temp = JSONObject.parseObject(JSON.toJSONString(po));
            if (null != temp) {
                if (po.isLastPage()){
                    temp.put("isEnd", 0);
                }else{
                    temp.put("isEnd", 1);
                }
            }
            resultBean.setData(temp);
            if (null == po) {
                Page<Message> page = new Page();
                page.setResult(Lists.newArrayList());
                resultBean.setData(page);
            }
            resultBean.setCode(ResultCode.SUCCESS.get_code());
            resultBean.setMsg(ResultCode.SUCCESS.getMsg());
            return JSON.toJSONString(resultBean);
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setCode(ResultCode.UNSUCCESS.get_code());
            resultBean.setMsg("系统异常");
            return JSON.toJSONString(resultBean);
        }
    }

    /**
     * 广场列表页下一页
     * @param po
     * @param userId
     * @param minMessageId
     * @return
     */
    public String nextPageSquare(Page po,String userId,String minMessageId) {
        ResultBean resultBean = new ResultBean();
        try {
            po = squareService.nextPageSquare(minMessageId,userId,po);
            Map<String, Object> temp = JSONObject.parseObject(JSON.toJSONString(po));
            if (null != temp) {
                if (po.isLastPage()){
                    temp.put("isEnd", 0);
                }else{
                    temp.put("isEnd", 1);
                }
            }
            resultBean.setData(temp);
            if (null == po) {
                Page<Message> page = new Page();
                page.setResult(Lists.newArrayList());
                resultBean.setData(page);
            }
            resultBean.setCode(ResultCode.SUCCESS.get_code());
            resultBean.setMsg(ResultCode.SUCCESS.getMsg());
            return JSON.toJSONString(resultBean);
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setCode(ResultCode.UNSUCCESS.get_code());
            resultBean.setMsg("系统异常");
            return JSON.toJSONString(resultBean);
        }
    }

    /**
     * 广场页下拉刷新
     * @param po
     * @param userId
     * @param maxMessageId
     * @return
     */
    public String flushSquare(Page po,String userId,String maxMessageId) {
        ResultBean resultBean = new ResultBean();
        try {
            po = squareService.flushSquare(maxMessageId, userId, po);
            if (null == po) {
                logger.error("下拉刷新获取数据为空！！！！");
                Page<Message> page = new Page();
                page.setResult(Lists.newArrayList());
                resultBean.setData(page);
                resultBean.setCode(ResultCode.SUCCESS.get_code());
                resultBean.setMsg(ResultCode.SUCCESS.getMsg());
                return JSON.toJSONString(resultBean);
            }
            if (po.getResult().size() == 0) {
                logger.info("已经是最新数据了");
                Page<Message> page = new Page();
                page.setResult(Lists.newArrayList());
                resultBean.setData(page);
                resultBean.setCode(ResultCode.SUCCESS.get_code());
                resultBean.setMsg(ResultCode.SUCCESS.getMsg());
                return JSON.toJSONString(resultBean);
            }
            Map<String, Object> temp = JSONObject.parseObject(JSON.toJSONString(po));
            temp.put("isEnd", 1);
            resultBean.setData(temp);
            resultBean.setCode(ResultCode.SUCCESS.get_code());
            resultBean.setMsg(ResultCode.SUCCESS.getMsg());
            return JSON.toJSONString(resultBean);
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setCode(ResultCode.UNSUCCESS.get_code());
            resultBean.setMsg("系统异常");
            return JSON.toJSONString(resultBean);
        }
    }

    /**
     * 关注小红点
     * @param userId
     * @param msgId
     * @return
     */
    public boolean followRedDot(String userId, int msgId) {
        return dynamicService.followRedDot(userId,msgId);
    }
}
