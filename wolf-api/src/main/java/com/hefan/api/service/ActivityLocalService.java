package com.hefan.api.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.Page;
import com.hefan.activity.itf.ActivityService;
import org.springframework.stereotype.Component;

import java.util.List;

import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 活动
 * 
 * @author kevin_zhang
 *
 */
@Component
public class ActivityLocalService {

	@Reference
  ActivityService activityService;

	/**
	 * 获取推荐活动
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List getHotActivity() {
		return activityService.getHotActivity();
	}

	/**
	 * 获取更多活动
	 * 
	 * @param page
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Page getMoreActivityList(Page page) {
		return activityService.getMoreActivityList(page);
	}

	/**
	 * 获取俱乐部活动
	 * 
	 * @param page
	 * @param clubType:(俱乐部类型:1:网红
	 *            2:明星 3:片场)
	 * @param clubUserId:俱乐部对应的userId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Page getClubActivityList(Page page, int clubType, String clubUserId) {
		return activityService.getClubActivityList(page, clubType, clubUserId);
	}
}
