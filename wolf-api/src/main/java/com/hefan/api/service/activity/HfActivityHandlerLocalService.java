package com.hefan.api.service.activity;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.constant.RedisKeyConstant;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.service.JedisService;
import com.hefan.activity.bean.HfActivityAttend;
import com.hefan.activity.bean.HfActivityCfgAttr;
import com.hefan.activity.bean.HfActivityCfgInfo;
import com.hefan.activity.itf.HfActivityService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by lxw on 2016/11/17.
 */
@Component
public class HfActivityHandlerLocalService {

    @Reference
    private HfActivityService hfActivityService;

    @Resource
    private JedisService jedisService;

    /**
     * 获取活动信息
     * @param activityId
     * @return
     */
    public HfActivityCfgInfo findActivityCfg(long activityId) {
        return hfActivityService.findHfActivityCfgInfo(activityId);
    }

    /**
     * 检查活动是否失效
     * @param hfActivityCfgInfo
     * @return
     */
    public ResultBean checkActivityIsValid (HfActivityCfgInfo hfActivityCfgInfo) {
        ResultBean res = new ResultBean(ResultCode.SUCCESS, null);
        int isValid = 1;
        if(hfActivityCfgInfo != null) {
            long currentTime = new Date().getTime();
            if(!(hfActivityCfgInfo.getBeginDate().getTime() <= currentTime && currentTime <= hfActivityCfgInfo.getEndDate().getTime())) {
                isValid = 0;
            }
        } else  {
            isValid = 0;
        }
        if(isValid == 0) {
           res = new ResultBean(ResultCode.ActivityIsNotAvlid, null);
        }
        return res;
    }

    /**
     * 保存参加活动记录
     * @param hfActivityAttend
     * @return
     */
    public int saveActivityAttendInfo(HfActivityAttend hfActivityAttend) {
        return hfActivityService.saveActivityAttend(hfActivityAttend);
    }

    /**
     * 获取参与人数
     * @param activityId
     * @param userId
     * @return
     */
    public int  findActivityAttendNum(long activityId , String userId) {
        return hfActivityService.findActivityAttendNum(activityId,userId);
    }

    /**
     * 根据获取活动id获取最近参与活动的记录
     * @param activityId
     * @param rowNum 最大记录数
     * @return
     */
    public List findActivityAttendHis(long activityId, int rowNum) {
        return hfActivityService.findActivityAttendHis(activityId,rowNum);
    }


    /**
     * 根据活动id获取活动属性信息
     * @param activityId
     * @return
     */
    public List<HfActivityCfgAttr> findActivityCfgAttr(long activityId) {
        return hfActivityService.findActivityAttrList(activityId);
    }

    /**
     * 获取存在有效区间的活动属性值
     * @param activityId
     * @return
     */
    public List<HfActivityCfgAttr> findAvlidActivitCfgAttrList(long activityId) {
        return hfActivityService.findAvlidActivitCfgAttrList(activityId);
    }

    /**
     * 累计活动属性的有效值
     * @param activityId
     * @param attrId
     * @param avlidNum
     * @return
     */
    public int incrActivityAttrAvlidNum(long activityId, long attrId, long avlidNum) {
        return hfActivityService.incrActivityCfgAttrAvlidNum(activityId,attrId,avlidNum);
    }

    /**
     * 检查用户是否频繁操作
     * @param userId
     * @param activityId
     * @return
     */
    public int activityFrequentlyReqCheck (String userId, long activityId) {
        String key = String.format(RedisKeyConstant.HF_ACTIVITY_FREQUENTLY_REQ_KEY,userId,activityId);
        try {
            boolean isHand = jedisService.isExist(key);
            if(isHand) {
                return 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 向缓存中保存频繁刷新的key信息
     * @param userId
     * @param activityId
     * @return
     */
    public int setActivityFrequentlyReqCach(String userId,long activityId) {
        String key = String.format(RedisKeyConstant.HF_ACTIVITY_FREQUENTLY_REQ_KEY,userId,activityId);
        try {
            jedisService.setexStr(key, "0", RedisKeyConstant.HF_ACTIVITY_FREQUENTLY_SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }

}
