package com.hefan.api.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.ResultBean;
import com.hefan.user.bean.LiveNoticeReserve;
import com.hefan.user.itf.LiveBookingService;

@Component
public class LiveBookLocalService {
	
	
	@Reference
	LiveBookingService liveBookingService;
	
	/**
	 * 用户预约动作
	 * @param liveNoticeReserve
	 * @return
	 */
	public ResultBean liveBooking(LiveNoticeReserve liveNoticeReserve) {return liveBookingService.saveLiveNoticeReserve(liveNoticeReserve);}
	
	/**
	 * 获取预约状态
	 * @param liveNoticeReserve
	 * @return
	 */
	public ResultBean liveBookingStatus(List<LiveNoticeReserve> liveNoticeReserveList) {return liveBookingService.liveBookingStatus( liveNoticeReserveList);}

}
