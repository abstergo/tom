package com.hefan.api.service.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.common.logger.Logger;
import com.alibaba.dubbo.common.logger.LoggerFactory;
import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.activity.bean.AppTaskInfo;
import com.hefan.activity.bean.AppTaskRelation;
import com.hefan.activity.bean.AppTaskVo;
import com.hefan.activity.itf.AppTaskService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WebUserService;

@Component
public class AppTaskLocalService {

	@Reference
    private AppTaskService appTaskService;
	@Reference
	private WebUserService webUserService;

    private Logger logger = LoggerFactory.getLogger(AppTaskLocalService.class);
    
    /**
     * 获取任务列表
     * @Title: getAppTaskInfoList   
     * @Description: TODO(这里用一句话描述这个方法的作用)   
     * @param userId
     * @return      
     * @return: List<AppTaskVo>
     * @author: LiTeng      
     * @throws 
     * @date:   2016年11月16日 上午10:26:05
     */
    public List<AppTaskVo> getAppTaskInfoList(String userId) {
		// TODO Auto-generated method stub
    	List<AppTaskVo> voList = new ArrayList<AppTaskVo>();
    	List<AppTaskInfo> infoList = appTaskService.getAppTaskInfoList();
    	AppTaskRelation taskRe = appTaskService.getAppTaskRelationByUserId(userId);
    	if(taskRe !=null && StringUtils.isNotBlank(taskRe.getTasksJson())){
    		voList = JSON.parseArray(taskRe.getTasksJson(), AppTaskVo.class);
    	}
    	Map<String, AppTaskVo> resultMap= new HashMap<String, AppTaskVo>(); //按类型分组
		for(int i=0;i<voList.size();i++){
			AppTaskVo at= voList.get(i);
		    resultMap.put(at.getTaskType()+"",at);
		}
		if(!CollectionUtils.isEmptyList(infoList)){
			for(int j=0;j<infoList.size();j++){
				AppTaskInfo apt = infoList.get(j);
				for (int x = 0;x<voList.size();x++){
					AppTaskVo vo = voList.get(x);
					if(vo.getTaskType() == apt.getTaskType()){
						vo.setTaskName(apt.getTaskName());
						//任务说明
						vo.setTaskExplain(apt.getTaskExplain());
						//任务图标
						vo.setTaskIcon(apt.getTaskIcon());
						if(vo.getTaskStatus() !=4){
							//完成能获得的经验
							vo.setExpValue(apt.getExpValue());
							//完成能获得的饭票
							vo.setTicketValue(apt.getTicketValue());
						}
					}
				}
				if(!resultMap.containsKey(apt.getTaskType()+"")){
					AppTaskVo vo = new AppTaskVo();
					vo.setTaskType(apt.getTaskType());
					vo.setTaskName(apt.getTaskName());
					//任务说明
					vo.setTaskExplain(apt.getTaskExplain());
					//任务图标
					vo.setTaskIcon(apt.getTaskIcon());
					//完成能获得的经验
					vo.setExpValue(apt.getExpValue());
					//完成能获得的饭票
					vo.setTicketValue(apt.getTicketValue());
					//状态  1-未领取  2-未完成  4-已领取
					vo.setTaskStatus(3);
					//进度
					vo.setSetbacks(0);
					//总刻度 （任务完成需要的数量）
					vo.setScaleValue(apt.getScaleValue());
					voList.add(vo);
				}
			}
		}
		//排序
		Collections.sort(voList, new Comparator() {
            public int compare(Object a, Object b) {
                int one = ((AppTaskVo) a).getTaskStatus();
                int two = ((AppTaskVo) b).getTaskStatus();
                return one - two;
            }
        });
		return voList;
	}
    
    public AppTaskRelation getAppTaskRelationByUserId(String userId){
    	return appTaskService.getAppTaskRelationByUserId(userId);
    }

    public AppTaskInfo getAppTaskInfoListByType(int type) {
		// TODO Auto-generated method stub
    	return appTaskService.getAppTaskInfoListByType(type);
	}
    
    public AppTaskRelation saveOrUpAppTaskRelation(AppTaskRelation appTaskRe) {
		// TODO Auto-generated method stub
		return appTaskService.saveOrUpAppTaskRelation(appTaskRe);
	}

    public int receiveTaskAwards(AppTaskRelation appTaskRe, AppTaskInfo appTaskInfo){
    	return appTaskService.updateAppTaskStatus(appTaskRe, appTaskInfo);
    }
    public WebUser findMyUserInfo(String userId){
    	return webUserService.findMyUserInfo(userId);
    }
    
    //分享动态
    public boolean hFDynamic(String userId){
		AppTaskInfo apt = appTaskService.getAppTaskInfoListByType(2);
		if(new Date().before(apt.getBeginTime()) || new Date().after(apt.getEndTime()) || apt.getDeleteFlag() !=0){
			return false;
		}
		AppTaskRelation taskRe = appTaskService.getAppTaskRelationByUserId(userId);
		List<AppTaskVo> voList = new ArrayList<AppTaskVo>();
		if(taskRe == null){
			//新增
			AppTaskVo vo = new AppTaskVo();
			vo.setTaskType(apt.getTaskType());
			vo.setTaskName(apt.getTaskName());
			//任务说明
			vo.setTaskExplain(apt.getTaskExplain());
			//任务图标
			vo.setTaskIcon(apt.getTaskIcon());
			//完成能获得的经验
			vo.setExpValue(apt.getExpValue());
			//完成能获得的饭票
			vo.setTicketValue(apt.getTicketValue());
			//进度
			vo.setSetbacks(1);
			//状态  1-未领取  2-未完成  4-已领取
			vo.setTaskStatus((apt.getScaleValue()-vo.getSetbacks())<=0?1:2);
			//总刻度 （任务完成需要的数量）
			vo.setScaleValue(apt.getScaleValue());
			voList.add(vo);
			String tasksJson = JSONObject.toJSONString(voList);
			AppTaskRelation atr =  new AppTaskRelation();
			atr.setUserId(userId);
			atr.setTasksJson(tasksJson);
			atr.setDeleteFlag(0);
			atr.setCreateTime(new Date());
			atr.setUpdateTime(new Date());
			appTaskService.saveOrUpAppTaskRelation(atr);
		}else{
			//修改
			if(taskRe !=null && StringUtils.isNotBlank(taskRe.getTasksJson())){
	    		voList = JSON.parseArray(taskRe.getTasksJson(), AppTaskVo.class);
	    	}
			int f=0;
			for(int i=0;i<voList.size();i++){
				AppTaskVo vo = voList.get(i);
				if(vo.getTaskType()==2){
					f=1;
				}
			}
			if(f==0){
				AppTaskVo vo = new AppTaskVo();
				vo.setTaskType(apt.getTaskType());
				vo.setTaskName(apt.getTaskName());
				//任务说明
				vo.setTaskExplain(apt.getTaskExplain());
				//任务图标
				vo.setTaskIcon(apt.getTaskIcon());
				//完成能获得的经验
				vo.setExpValue(apt.getExpValue());
				//完成能获得的饭票
				vo.setTicketValue(apt.getTicketValue());
				//状态  1-未领取  2-未完成  4-已领取
				vo.setTaskStatus(1);
				//进度
				vo.setSetbacks(apt.getScaleValue());
				//总刻度 （任务完成需要的数量）
				vo.setScaleValue(apt.getScaleValue());
				voList.add(vo);
				String tasksJson = JSONObject.toJSONString(voList);
				taskRe.setTasksJson(tasksJson);
				appTaskService.saveOrUpAppTaskRelation(taskRe);
			}
		}
		return true;
	}
    //分享直播间
    public boolean hFLive(String userId){
		AppTaskInfo apt = appTaskService.getAppTaskInfoListByType(1);
		if(new Date().before(apt.getBeginTime()) || new Date().after(apt.getEndTime()) || apt.getDeleteFlag() !=0){
			return false;
		}
		AppTaskRelation taskRe = appTaskService.getAppTaskRelationByUserId(userId);
		List<AppTaskVo> voList = new ArrayList<AppTaskVo>();
		if(taskRe == null){
			//新增
			AppTaskVo vo = new AppTaskVo();
			vo.setTaskType(apt.getTaskType());
			vo.setTaskName(apt.getTaskName());
			//任务说明
			vo.setTaskExplain(apt.getTaskExplain());
			//任务图标
			vo.setTaskIcon(apt.getTaskIcon());
			//完成能获得的经验
			vo.setExpValue(apt.getExpValue());
			//完成能获得的饭票
			vo.setTicketValue(apt.getTicketValue());
			//进度
			vo.setSetbacks(1);
			//状态  1-未领取  2-未完成  4-已领取
			vo.setTaskStatus((apt.getScaleValue()-vo.getSetbacks())<=0?1:2);
			//总刻度 （任务完成需要的数量）
			vo.setScaleValue(apt.getScaleValue());
			voList.add(vo);
			String tasksJson = JSONObject.toJSONString(voList);
			AppTaskRelation atr =  new AppTaskRelation();
			atr.setUserId(userId);
			atr.setTasksJson(tasksJson);
			atr.setDeleteFlag(0);
			atr.setCreateTime(new Date());
			atr.setUpdateTime(new Date());
			appTaskService.saveOrUpAppTaskRelation(atr);
		}else{
			//修改
			if(taskRe !=null && StringUtils.isNotBlank(taskRe.getTasksJson())){
	    		voList = JSON.parseArray(taskRe.getTasksJson(), AppTaskVo.class);
	    	}
			int f=0;
			for(int i=0;i<voList.size();i++){
				AppTaskVo vo = voList.get(i);
				if(vo.getTaskType()==1){
					f=1;
				}
			}
			if(f==0){
				AppTaskVo vo = new AppTaskVo();
				vo.setTaskType(apt.getTaskType());
				vo.setTaskName(apt.getTaskName());
				//任务说明
				vo.setTaskExplain(apt.getTaskExplain());
				//任务图标
				vo.setTaskIcon(apt.getTaskIcon());
				//完成能获得的经验
				vo.setExpValue(apt.getExpValue());
				//完成能获得的饭票
				vo.setTicketValue(apt.getTicketValue());
				//进度
				vo.setSetbacks(1);
				//状态  1-未领取  2-未完成  4-已领取
				vo.setTaskStatus((apt.getScaleValue()-vo.getSetbacks())<=0?1:2);
				//总刻度 （任务完成需要的数量）
				vo.setScaleValue(apt.getScaleValue());
				voList.add(vo);
				String tasksJson = JSONObject.toJSONString(voList);
				taskRe.setTasksJson(tasksJson);
				appTaskService.saveOrUpAppTaskRelation(taskRe);
			}
		}
		return true;
	}
}
