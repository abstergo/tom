package com.hefan.api.service.live;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.hefan.common.util.DynamicProperties;
import com.hefan.live.bean.LiveRoom;
import com.hefan.live.bean.LivingRoomInfoVo;
import com.hefan.live.itf.LiveRoomService;
import com.hefan.live.itf.LivingRedisOptService;
import com.hefan.notify.itf.ItemsStaticService;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by nigle on 2016/9/28.
 */
@Component
public class LiveRoomLocalService {
    @Reference
    LiveRoomService liveRoomService;
    @Reference
    ItemsStaticService feeItemsStaticService;
    @Reference
    LivingRedisOptService livingRedisOptService;

    /**
     * 获取直播间信息
     *
     * @param userId
     * @return
     */
    public LiveRoom getLiveRoomByUserId(String userId) {
        return liveRoomService.getLiveRoom(userId);
    }


    /**
     * 获取直播间
     *
     * @param userId
     * @return
     * @throws Exception
     */
    public LivingRoomInfoVo getLiveRoom(String userId) throws Exception {
        String str = livingRedisOptService.getLivingInfo_Hash(userId);
        return JSON.parseObject(str, LivingRoomInfoVo.class);
    }

    /**
     * 下单后更新直播盒饭收入 ticket_history
     *
     * @param liveRoom
     * @return
     */
    public int updateLiveRoomForRebalance(LiveRoom liveRoom) {
        return liveRoomService.updateLiveRoomForRebalance(liveRoom);
    }

    ;

    /**
     * @param liveRoom
     * @return
     */
    private static String pullpre = DynamicProperties.getString("pull.domain.http");
    private static String pullend = DynamicProperties.getString("replay.domain.suf");
    private static String repre = DynamicProperties.getString("replay.domain.pre");

    public LivingRoomInfoVo findLiveRoomById(String roomId) throws Exception {

        LivingRoomInfoVo livingRoomInfoVo = liveRoomService.findLiveRoomById(roomId);
        livingRoomInfoVo.setPullUrls(pullpre + livingRoomInfoVo.getLiveUuid() + pullend);
        livingRoomInfoVo.setReUrl(repre + livingRoomInfoVo.getLiveUuid() + pullend);
        return livingRoomInfoVo;
    }

    /**
     * 是否直播中
     *
     * @param
     * @return
     */
    public boolean isExistsLivingInfo_Hash(String userId) {
        return livingRedisOptService.isExistsLivingInfo_Hash(userId);

    }

    /**
     * 人数
     *
     * @param
     * @return
     */
    public long peopleCount(String userId) {
        long watchNum = livingRedisOptService.getLivingUserCount_SortedSet(userId);
        try {
            String addNUm = livingRedisOptService.getAddNumForRoom(userId);
            if ( null != addNUm && Long.valueOf(addNUm) >= 0) {
                watchNum += Long.valueOf(addNUm);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return watchNum;
    }

    public List<LivingRoomInfoVo> recommendLive() throws Exception {
        List<LivingRoomInfoVo> list = liveRoomService.recommendLive();
        for (LivingRoomInfoVo liveRoom : list) {
            liveRoom.setPullUrls(pullpre + liveRoom.getLiveUuid() + pullend);
            liveRoom.setReUrl(repre + liveRoom.getLiveUuid() + pullend);
        }
        return list;
    }

    public LiveRoom getLiveRoomByChatRoomId(int chatRoomId) {
        return liveRoomService.getLiveRoomByChatRoomId(chatRoomId);
    }

    /**
     * 热门页
     */
    public ResultBean listHotIndex(HttpServletRequest req) {
        return liveRoomService.listHostIndex();
    }

    public List getshareWordsForBefore() {
        return liveRoomService.getshareWordsForBefore();
    }

    public List getshareWordsForAfter() {
        return liveRoomService.getshareWordsForAfter();
    }

}
