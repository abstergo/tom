package com.hefan.api.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.constant.RedisKeyConstant;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.common.meta.SmsCodeTypeEnum;
import com.cat.tiger.service.JedisService;
import com.hefan.common.util.DynamicProperties;
import com.hefan.notify.itf.SmsSendService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by lxw on 2016/9/28.
 */
@Component
public class SmsLocalService {

    @Reference
    private SmsSendService smsSendService;

    @Resource
    private JedisService jedisService;

    /**
     * 验证短信验证码是否正确
     * @param type
     * @param moblie
     * @param smsCode
     * @return
     */
    public ResultBean checkSmsCode(int type, String moblie, String smsCode) {
        //测试时使用---begin-----
       if(DynamicProperties.getBoolean("test.open")) {
            return new ResultBean(ResultCode.SUCCESS, null);
        }
        //测试时使用---end-----
        String key = String.format(RedisKeyConstant.SMS_CODE_KEY,new Object[] {type,moblie});
        try {
            String checkSmsCode = jedisService.getStr(key);
            if(StringUtils.isBlank(checkSmsCode)) {
                return new ResultBean(ResultCode.SmsCodeTimeOut, null);
            } else  {
                if(!checkSmsCode.equals(smsCode)) {
                    return new ResultBean(ResultCode.SmsCodeCheckFial, null);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultBean(ResultCode.SmsCodeSaveError, null);
        }
        return new ResultBean(ResultCode.SUCCESS, null);
    }

    /**
     * 发送短信验证码
     * @param type
     * @param mobile
     * @return
     */
    public ResultBean sendSmsCode(int type, String mobile) {
        //发送短信验证码
        ResultBean resultBean =  smsSendService.aliSendSmsCode(type,mobile);
        if(resultBean.getCode() != ResultCode.SUCCESS.get_code()) {
            return resultBean;
        }
        String code = String.valueOf(resultBean.getData());
        if(StringUtils.isBlank(code)) {
            return new ResultBean(ResultCode.SendSmsCodeFial,null);
        }
        String key = String.format(RedisKeyConstant.SMS_CODE_KEY,new Object[] {type,mobile});
        int seconds = 120;
        if(type == SmsCodeTypeEnum.IDENTITY_SMS_CODE_TYPE.getType()) { //身份验证发送验证码，cach时间为300秒
            seconds = 300;
        }
        try {
            String resStr = jedisService.setexStr(key, code, seconds);
            if (StringUtils.isNotBlank(resStr)) {
                resultBean.setData(null);
                return resultBean;
            } else {
                return new ResultBean(ResultCode.SmsCodeSaveError, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultBean(ResultCode.SmsCodeSaveError, null);
        }
    }
}
