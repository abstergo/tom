package com.hefan.api.service.live;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.club.dynamic.itf.DynamicService;
import com.hefan.live.bean.*;
import com.hefan.live.itf.LivingRedisOptService;
import com.hefan.live.itf.MemberRoleService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WebUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by nigle on 2016/10/9.
 */
@Component
public class RoomOptLocalService {

    @Reference
    MemberRoleService memberRoleService;
    @Reference
    WebUserService webUserService;
    @Reference
    DynamicService dynamicService;
    @Reference
    LivingRedisOptService livingRedisOptService;

    public Logger logger = LoggerFactory.getLogger(RoomOptLocalService.class);

    public int save(MemberRole memberRole) {
        return memberRoleService.save(memberRole);
    }

    /**
     * 检查用户是否是直播间管理员
     * @param userId
     * @param chatRoomId
     * @return
     */

    public boolean findIsAdminForAnchorByUserId(String userId, int chatRoomId) {
        return memberRoleService.findIsAdminForAnchorByUserId(userId, chatRoomId);
    }

    /**
     * 检查用户是否被禁言
     * @param userId
     * @param liveUuid
     * @return
     */
    public boolean findIsShutupForAnchorByUserId(String userId, String liveUuid) {
        return livingRedisOptService.isShutUpUser(liveUuid,userId);
    }

    /**
     * 获取直播间管理员列表
     * @param chatRoomId
     * @return
     */
    public List<MemberRole> findRoomAdminList(int chatRoomId){
        return memberRoleService.findRoomAdminList(chatRoomId);
    }

    /**
     * 获取直播间管理员信息列表
     * @param listMem
     * @return
     */
    public List<LiveReturnVo> findAdminInfo(List<MemberRole> listMem) {
        return memberRoleService.findAdminInfo(listMem);
    }

    /**
     * 删除直播间管理员
     * @param chatRoomId
     * @param userId
     * @return
     */
    public int del(int chatRoomId, String userId) {
        return memberRoleService.del(chatRoomId, userId);
    }

    public WebUser findOtherUserInfo(String userId,String optUserId) {
        return webUserService.findOtherUserInfo(optUserId, userId);
    }

    /**
     * 明星自动分享动态
     * @param liveDynamicVo
     * @return
     */
    @Async
    public void shareLive(LiveDynamicVo liveDynamicVo) {
        int i = dynamicService.shareLive(liveDynamicVo);
        if (i != 1) {
            logger.error("明星自动分享动态失败--{}" ,liveDynamicVo.getPathTrans());
        }
    }
    /**
     * 主播分享动态
     * @param liveDynamicVo
     * @return
     */
    public int anchorShareLive(LiveDynamicVo liveDynamicVo) {
        return dynamicService.shareLive(liveDynamicVo);
    }


    /**
     * 获取直播间管理员列表
     * @param chatRoomId
     * @return
     */
    public long getAdminCount(int chatRoomId){
        return memberRoleService.getAdminCount(chatRoomId);
    }

    /**
     * 查看直播是否被分享过动态
     * false:未分享过
     * @return
     */
    public boolean isShareToDynamic(String userId, String liveUuid) {
        return dynamicService.isShareToDynamic(userId, liveUuid);
    }
}
