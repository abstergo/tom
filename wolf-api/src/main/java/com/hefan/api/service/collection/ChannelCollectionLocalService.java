package com.hefan.api.service.collection;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.cat.tiger.util.HttpClientUtils;
import com.hefan.monitor.bean.AppChannelStatistics;
import com.hefan.monitor.itf.AppChannelStatisticsService;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import java.net.URLDecoder;

/**
 * IOS推广渠道信息收集
 * Created by kevin_zhang on 27/03/2017.
 */
@Component
public class ChannelCollectionLocalService {
    public static Logger logger = LoggerFactory.getLogger(ChannelCollectionLocalService.class);

    @Reference
    AppChannelStatisticsService appChannelStatisticsService;

    /**
     * 第三方IOS设备的IDFA收集
     *
     * @param model
     */
    @Async
    public void idfaCollection(AppChannelStatistics model) {
        try {
            AppChannelStatistics result = appChannelStatisticsService.submitForThirdParty(model);
            logger.info("第三方IOS设备的IDFA收集操作 result为" + JSON.toJSONString(result));
            if (null != result && result.getIsActivity() == 0) {
                logger.info("第三方IOS设备的IDFA收集操作 idfa为" + model.getIdfa() + " 记录已存在");
                if (result.getAppid() <= 0) {
                    logger.info("第三方IOS设备的IDFA收集操作 初次提交为APP主动提交");
                    if (appChannelStatisticsService.updateAppChannelStatistics(model) > 0) {
                        logger.info("第三方IOS设备的IDFA收集操作 更新补全第三方IOS设备的IDFA收集信息，并激活");
                        appChannelStatisticsService.activityAppChannelStatistics(1, result.getIdfa());

                        /**
                         * 激活
                         */
                        String urlStr = URLDecoder.decode(model.getCallback(), "UTF-8");
                        logger.info("IOS设备的IDFA收集操作 初次提交为第三方主动提交，激活链接：" + urlStr);
                        activityLink(urlStr);
                    }
                }
            } else {
                logger.info("第三方IOS设备的IDFA收集操作 idfa为" + model.getIdfa() + " 记录未存在");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("第三方IOS设备的IDFA收集操作 操作失败");
        }
    }

    /**
     * IOS设备的IDFA收集
     *
     * @param model
     */
    @Async
    public void idfaSubmit(AppChannelStatistics model) {
        try {
            AppChannelStatistics result = appChannelStatisticsService.submitForApp(model);
            logger.info("IOS设备的IDFA收集操作 result为" + JSON.toJSONString(result));
            if (null != result && result.getIsActivity() == 0) {
                logger.info("IOS设备的IDFA收集操作 idfa为" + model.getIdfa() + " 记录已存在");
                if (result.getAppid() > 0) {
                    logger.info("IOS设备的IDFA收集操作 初次提交为第三方主动提交，直接激活");
                    appChannelStatisticsService.activityAppChannelStatistics(1, result.getIdfa());

                    /**
                     * 激活
                     */
                    String urlStr = URLDecoder.decode(result.getCallback(), "UTF-8");
                    logger.info("IOS设备的IDFA收集操作 初次提交为第三方主动提交，激活链接：" + urlStr);
                    activityLink(urlStr);
                }
            } else {
                logger.info("IOS设备的IDFA收集操作 idfa为" + model.getIdfa() + " 记录未存在");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("IOS设备的IDFA收集操作 操作失败");
        }
    }

    /**
     * 激活第三方IOS推广
     *
     * @param url
     */
    public void activityLink(String url) {
        try {
            String resultStr = HttpClientUtils.doGet(url, new HashedMap());
            logger.info("激活第三方IOS推操作 操作结果：" + resultStr);
            logger.info("激活第三方IOS推操作 操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("激活第三方IOS推操作 操作失败");
        }
    }
}
