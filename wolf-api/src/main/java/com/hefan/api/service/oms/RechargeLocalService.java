package com.hefan.api.service.oms;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.ResultBean;
import com.hefan.oms.itf.RechargeCodePayService;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @author diguage
 * @since 2016/1/26.
 */
@Component
public class RechargeLocalService {

    @Reference
    private RechargeCodePayService rechargeCodePayService;

    public Integer rechargeCodePayOpreate(Map userMap, Map rechangeCodeMap,Integer paySource) throws Exception{
        return rechargeCodePayService.rechargeCodePayOpreate(userMap,rechangeCodeMap,paySource);
    }

    /**
     * 查询充值时验证
     * @param code
     * @param userId
     * @return
     */
    public ResultBean findValidRechargeInfoByCode(String code, String userId){
        return rechargeCodePayService.findValidRechargeInfoByCodeForPay(code,userId);
    }

    /**
     * 充值码查询时验证
     * @param code
     * @return
     */
    public Map findValidRechargeInfoByCodeForQuery(String code){
        return rechargeCodePayService.findValidRechargeInfoByCode(code);
    }

}
