package com.hefan.api.service.push;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.ResultBean;
import com.hefan.notify.bean.PubParams;
import com.hefan.notify.itf.ImUserService;
import com.hefan.notify.itf.UmengPushService;

@Component
public class PushLocalService {

	@Reference
	UmengPushService umengPushService;
	
	public void uPushBroadcast(PubParams params){
		try {
			//and广播推送
			boolean ia= umengPushService.sendAndroidBroadcast(params);
			//ios广播推送
			boolean ii = umengPushService.sendIOSBroadcast(params);
		} catch (Exception e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
		}
	}
}
