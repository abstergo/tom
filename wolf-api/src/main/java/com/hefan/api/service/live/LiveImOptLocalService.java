package com.hefan.api.service.live;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.ResultBean;
import com.hefan.live.itf.LiveImOptService;
import com.hefan.user.bean.WebUser;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


/**
 * Created by nigle on 2016/10/11.
 */
@Component
public class LiveImOptLocalService {

    @Reference
    LiveImOptService liveImOptService;

    /**
     * 关播发IM关播
     * @param user
     * @param chatRoomId
     * @param liveUuid
     * @param watchNum
     * @return
     */
    public ResultBean liveEndIm(WebUser user,int chatRoomId,String liveUuid,String watchNum,String liveLength,String heFanNum){
        return liveImOptService.liveEndIm(user, chatRoomId, liveUuid, watchNum,liveLength,heFanNum);
    }
    /**
     * 禁言IM消息处理
     * @param optUser 操作者
     * @param user 被操作者
     * @param chatRoomId
     * @param liveUuid
     * @param
     * @return
     */
    public boolean liveShutUpIm(WebUser optUser,WebUser user,int chatRoomId,String liveUuid,String superAdmin) {
        return liveImOptService.liveShutUpIm(optUser, user, chatRoomId, liveUuid,superAdmin);
    }

    /**
     * 踢出IM消息处理
     * @param optUser 操作者
     * @param user 被操作者
     * @param chatRoomId
     * @param liveUuid
     * @return
     */
    public void liveOutIm(WebUser optUser, WebUser user, int chatRoomId, String liveUuid) {
        liveImOptService.liveOutIm(optUser, user, chatRoomId, liveUuid);
    }

    /**
     * 设置和取消管理员
     * @param optUser 操作者
     * @param user 被操作者
     * @param action 动作：SET 设置管理员 UNSET 取消管理医院
     * @param chatRoomId
     * @return
     */
    public ResultBean liveOptAdmin(WebUser optUser, WebUser user,int chatRoomId,String liveUuid ,String action){
        return liveImOptService.liveOptAdmin(optUser,user,chatRoomId,liveUuid,action);
    }

    /**
     * 异步发送IM消息,发送三次
     */
    @Async
    public void sendImMessage(WebUser user, int chatRoomId, String liveUuid, String watchNum, String liveLength,String heFanNum) {
        try {
            liveEndIm(user,chatRoomId,liveUuid,watchNum,liveLength,heFanNum);
            liveEndIm(user,chatRoomId,liveUuid,watchNum,liveLength,heFanNum);
            liveEndIm(user,chatRoomId,liveUuid,watchNum,liveLength,heFanNum);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
