package com.hefan.api.service;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.user.bean.FeedBack;
import com.hefan.user.bean.FindBug;
import com.hefan.user.bean.HfFindBugRecord;
import com.hefan.user.itf.FeedBackService;

@Component
public class FeedBackLocalService {
	
	
	@Reference
	FeedBackService feedBackService;
	
	public int submit(FeedBack feedBack) {
		return feedBackService.submit(feedBack);
		
	}

	public void findBug(FindBug findBug) {
		 feedBackService.findBug(findBug);
	}


	public void visitRecord(HfFindBugRecord visitRecord) {
		feedBackService.visitRecord(visitRecord);
	}


}
