package com.hefan.api.service.aop;

import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * User: criss
 * Date: 16/1/21
 * Time: 15:31
 */
@Component
@Aspect
public class AccessAspect {

    private final Logger logger = LoggerFactory.getLogger(AccessAspect.class);

    @Resource
    HttpServletRequest request;

//
//    @Pointcut("")
//    public void excute() {
//    }
//    @Before("excute()")
//    public void check(JoinPoint pjp) throws Throwable {
//
//
//    }

}
