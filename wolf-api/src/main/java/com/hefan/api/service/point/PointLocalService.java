package com.hefan.api.service.point;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.common.point.bean.BuriedPointVo;
import com.hefan.common.point.bean.HfChannelPointInfo;
import com.hefan.common.point.itf.ChannelPointService;
import org.springframework.stereotype.Component;

/**
 * Created by lxw on 2016/11/28.
 */
@Component
public class PointLocalService {

    @Reference
    private ChannelPointService channelPointService;

    public int recordChannelPointInfo(String userId, BuriedPointVo buriedPointVo) {
        if(buriedPointVo != null) {
            HfChannelPointInfo hfChannelPointInfo = new HfChannelPointInfo();
            hfChannelPointInfo.setUserId(userId);
            hfChannelPointInfo.setChannelNo(buriedPointVo.getChannelNo());
            return channelPointService.initChannelPointInfo(hfChannelPointInfo);
        }
        return 0;
    }
}
