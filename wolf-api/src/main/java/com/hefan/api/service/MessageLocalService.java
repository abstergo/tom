package com.hefan.api.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.Page;
import com.hefan.club.comment.itf.CommentsService;
import com.hefan.notify.bean.SysMsgVo;
import com.hefan.notify.itf.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 消息
 * 
 * @author kevin_zhang
 *
 */
@Component
public class MessageLocalService {
	public static Logger logger = LoggerFactory.getLogger(MessageLocalService.class);
	/**
	 * 0:系统消息
	 */
	private static final int SYSTEM_MSG_TYPE = 0;
	/**
	 * 1:活动消息
	 */
	private static final int ACTIVITY_MSG_TYPE = 1;

	@Reference
	MessageService messageService;

	@Reference
	CommentsService commentsService;

	/**
	 * 获取未读消息数
	 * 
	 * @param userId
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public SysMsgVo getMsgCount(String userId) {
		return messageService.getMsgCount(userId);
	}

	/**
	 * 获取消息主页信息
	 * 
	 * @param userId
	 * @param userType:(0:普通用户
	 *            1：网红 2：明星／片场)
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Map getMsgPageInfo(String userId, int userType) {
		Map result = new HashMap();
		result.put("systemMes", new HashMap());
		result.put("activityMes", new HashMap());
		result.put("commentMes", new HashMap());
		result.put("giftMes", new HashMap());
		SysMsgVo msgCountMap = messageService.getMsgCount(userId);
		if (null != msgCountMap) {
			// system message
			int sysMsgCount = msgCountMap.getSysmessagecount();
			if (sysMsgCount > 0) {
				Map sysMsgMap = messageService.getMsgPageInfo(SYSTEM_MSG_TYPE, userId, userType);
				if (null != sysMsgMap) {
					sysMsgMap.put("count", sysMsgCount);
					result.put("systemMes", sysMsgMap);
				}
			}

			// activity message
			int activeMsgCount = msgCountMap.getActivemessagecount();
			if (activeMsgCount > 0) {
				Map activityMsgMap = messageService.getMsgPageInfo(ACTIVITY_MSG_TYPE, userId, userType);
				if (null != activityMsgMap) {
					activityMsgMap.put("count", activeMsgCount);
					result.put("activityMes", activityMsgMap);
				}
			}

			// comment message
			int commetMsgCount = msgCountMap.getCommetmessagecount();
			if (commetMsgCount > 0) {
				Map commentMesMap = commentsService.getCommentsFristByUserId(userId, 0);
				if (null != commentMesMap) {
					commentMesMap.put("count", commetMsgCount);
					result.put("commentMes", commentMesMap);
				}
			}

			// present message
			int presentMsgCount = msgCountMap.getPresentmessagecount();
			if (presentMsgCount > 0) {
				Map giftMesMap = commentsService.getCommentsFristByUserId(userId, 1);
				if (null != giftMesMap) {
					giftMesMap.put("count", presentMsgCount);
					result.put("giftMes", giftMesMap);
				}
			}
		}

		//清空用户中心消息
		messageService.cleanMsgCount(userId, 0);
		return result;
	}

	/**
	 * 获取系统消息列表
	 * 
	 * @param page
	 * @param userId
	 * @param userType:(0:普通用户
	 *            1：网红 2：明星／片场)
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Page getSysMsgList(Page page, String userId, int userType) {
		return messageService.getSysMsgList(page, userId, userType);
	}

	/**
	 * 获取活动消息列表
	 * 
	 * @param page
	 * @param userId
	 * @param userType:(0:普通用户
	 *            1：网红 2：明星／片场)
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Page getActivityMsgList(Page page, String userId, int userType) {
		return messageService.getActivityMsgList(page, userId, userType);
	}

	/**
	 * 获取消息详情
	 * 
	 * @param msgId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Map getMsgDetail(String msgId) {
		return messageService.getMsgDetail(msgId);
	}

	/**
	 * 清空所有用户消息缓存（慎重操作）
	 */
	public void cleanAllMsgCache() {
		messageService.cleanAllMsgCache();
	}
}
