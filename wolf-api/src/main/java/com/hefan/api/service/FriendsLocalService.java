package com.hefan.api.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.ResultBean;
import com.hefan.user.itf.WatchCacheService;
import com.hefan.user.itf.WatchRecommendCacheService;
import org.springframework.stereotype.Component;

/**
 * Created by sagagyq on 2017/3/22.
 */
@Component
public class FriendsLocalService {
    @Reference
    WatchRecommendCacheService watchRecommendCacheService;

    @Reference
    private WatchCacheService watchCacheService;

    public ResultBean getHotRecommend(){
        return watchRecommendCacheService.getHotRecommend();
    }
}
