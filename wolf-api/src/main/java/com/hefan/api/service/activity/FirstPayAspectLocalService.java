package com.hefan.api.service.activity;

import com.cat.common.meta.TradeLogPayStatusEnum;
import com.hefan.pay.bean.TradeLog;
import org.aspectj.lang.JoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by lxw on 2016/11/16.
 */
@Component
public class FirstPayAspectLocalService {

    private Logger logger = LoggerFactory.getLogger(FirstPayAspectLocalService.class);

    @Resource
    private FirstPayAndShareLocalService firstPayAndShareLocalService;

    public void firstPayHandler(JoinPoint joinPoint) {
        logger.info("firstPayHandlerAop --- begin");
        TradeLog tradeLog =  (TradeLog)joinPoint.getArgs()[0];
        if(tradeLog.getPayStatus() == TradeLogPayStatusEnum.PAY_SUCCESS.getPayStatus()) { //支付成功
            firstPayAndShareLocalService.firstPayHandler(1,tradeLog.getUserId(),tradeLog);
        }
        logger.info("firstPayHandlerAop --- end");
    }

}
