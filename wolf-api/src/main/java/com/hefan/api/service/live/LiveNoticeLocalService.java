package com.hefan.api.service.live;

import java.util.Map;
import org.springframework.stereotype.Component;
import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.live.itf.LiveNoticeService;

/**
 * 直播预告类 直播预告 最新直播
 * @author sagagyq
 *
 */
@Component
public class LiveNoticeLocalService {
	
	@Reference
	LiveNoticeService liveNoticeService;
	
	public Map<String,Object> liveNotice() {return liveNoticeService.liveNotice();}
}
