package com.hefan.api.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.hefan.club.dynamic.bean.DynamicAlbum;
import com.hefan.club.dynamic.bean.Message;
import com.hefan.club.dynamic.itf.DynamicAlbumService;
import com.hefan.club.dynamic.itf.DynamicService;
import com.hefan.live.bean.LiveDynamicVo;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WebUserService;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class DynamicLocalService {

    @Reference
    DynamicService dynamicService;

    @Reference
    DynamicAlbumService dynamicAlbumService;
    @Reference
    private WebUserService webUserService;

    /**
     * 获取推荐活动
     *
     * @return
     * @throws ParseException
     */
    @SuppressWarnings("rawtypes")
    public Page<Message> videoList(Page po, String userId, String isSync, String authorId) throws ParseException {
        return dynamicService.videoList(po, isSync, authorId, userId);
    }

    /**
     * 删除动态
     *
     * @param mes
     * @return
     */
    public ResultBean deleteMessage(Message mes) throws Exception {
        return dynamicService.deleteMessage(mes);

    }


    /**
     * 删除动态
     *
     * @param messageId
     * @return
     */
    public int checkLast(String messageId) {
        return dynamicAlbumService.checkLast(messageId);

    }


    /**
     * 查询关注人的微博
     *
     * @param po
     * @return
     */
    public Page<Message> listClubIndex(Page po, String userId) {

        return dynamicService.listClubIndex(po, userId);
    }

    /**
     * 查询主播所有微博
     *
     * @param po
     * @return
     */
    public Page<Message> listMessage(Page po, String userId, String authorId) throws Exception {
        return dynamicService.listMessage(po, userId, authorId);
    }

    /**
     * 置顶
     *
     * @param messageId
     * @return
     */
    public ResultBean top(String userId, String messageId) {
        return dynamicService.top(userId, messageId);
    }

    /**
     * 取消置顶
     *
     * @param messageId
     * @return
     */
    public int unTop(String userId, String messageId) throws Exception {

        return dynamicService.unTop(userId, messageId);
    }

    /**
     * 查询图片
     *
     * @param po
     * @param isSync
     * @param userId
     * @return
     */
    public Page<Map<String, Object>> photoList(Page po, String isSync, String userId, String messageId) {

        return dynamicService.photoList(po, isSync, messageId, userId);
    }

    /**
     * 点赞
     *
     * @param userId
     * @param userId
     * @return
     */
    public int admireMsg(String userId, String messageId) throws Exception {

        return dynamicService.admire(userId, messageId);
    }

    /**
     * 粉丝排行榜
     *
     * @param userId
     * @return
     */
    public List listFans(String userId) {
        List list;
        try {
            list = dynamicService.listFans(userId);
        } catch (Exception e) {
            e.printStackTrace();
            list = new ArrayList();
        }
        return list;
    }

    /**
     * 删除相册
     *
     * @param record
     * @return
     */
    public int deleteAlbumByParams(DynamicAlbum record) throws Exception

    {
        return dynamicAlbumService.deleteAlbumByParams(record);
    }

    /**
     * 判断最后一张
     *
     * @param record
     * @return
     */
    public int dcheckLast(DynamicAlbum record) throws Exception

    {
        return dynamicAlbumService.deleteAlbumByParams(record);
    }

    /**
     * 添加动态
     *
     * @param message
     * @return
     */
    @SuppressWarnings("rawtypes")
    public ResultBean insertMessage(Message message) throws Exception {
        return dynamicService.insertMessage(message);
    }

    /**
     * 视频回调
     *
     * @param jobId
     * @param pathTrans
     */
    public void videoCallback(String jobId, String pathTrans) throws Exception {
        dynamicService.videoCallback(jobId, pathTrans);
    }

    /**
     * 用户首部信息
     *
     * @param userId
     * @return
     */
    public WebUser getDynaUser(String userId) {
        return dynamicService.getDynaUser(userId);

    }

    /**
     * 用户首部信息
     *
     * @param messageId
     * @return
     */
    public Message getMessageById(String messageId,String userId) {
        return dynamicService.getCacheMessageInfo(messageId,userId);

    }

    /**
     * 用户首部信息
     *
     * @param userId
     * @return
     */
    public List recommendMesStarClubFor(String userId) {
        return dynamicService.recommendMesStarClubFor(userId);
    }

    /**
     * 明星分享动态
     *
     * @param liveDynamicVo
     * @return
     */
    public int shareLive(LiveDynamicVo liveDynamicVo) {
        return dynamicService.shareLive(liveDynamicVo);
    }

    /**
     * 查看用户当日已放动态数
     *
     * @param userId
     * @return a
     * @throws a
     * @description （用一句话描述该方法的用条件、执行流程、适用方法、注意项 - 可选）
     * @auwangchaogchao
     * @cr2017-03-23013:4313:43
     */
    public boolean isSendMessage(String userId) throws Exception {

        return dynamicService.isSendMessage(userId);
    }
}
