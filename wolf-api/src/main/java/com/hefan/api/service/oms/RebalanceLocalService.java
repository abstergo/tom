package com.hefan.api.service.oms;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.oms.bean.Present;
import com.hefan.oms.itf.PresentService;
import org.springframework.stereotype.Component;

/**
 * Created by hbchen on 2016/9/29.
 */
@Component
public class RebalanceLocalService {

    @Reference
    PresentService presentService;

    public int getPresentPriceById(Long presentId){
        return  presentService.getPresentPriceById(presentId);
    }
}
