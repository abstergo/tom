package com.hefan.api.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.pay.bean.CurrencyExchange;
import com.hefan.pay.bean.CurrencyRecharge;
import com.hefan.pay.itf.CurrentRechargeService;

@Component
public class CurrencyRechargeLocalService {

	@Reference
	private CurrentRechargeService currentRechargeService;

	/**
	 * 饭票数
	 * 
	 * @param status
	 * @return
	 */
	public List<CurrencyRecharge> getCurrencyRecharge(int status, String id) {
		return currentRechargeService.getCurrencyRecharge(status, id);
	}

	/**
	 * 兑换比例
	 * 
	 * @param status
	 * @return
	 */
	public CurrencyExchange getExchange(int status, String id) {
		return currentRechargeService.getCurrencyexchange(status);
	}
}
