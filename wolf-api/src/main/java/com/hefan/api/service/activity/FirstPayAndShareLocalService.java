package com.hefan.api.service.activity;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.*;
import com.cat.common.util.GuuidUtil;
import com.hefan.activity.bean.HfActivityAttend;
import com.hefan.activity.bean.HfActivityCfgInfo;
import com.hefan.pay.bean.TradeLog;
import com.hefan.pay.itf.TradeLogService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WebUserService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 首充和首次分享活动处理
 * Created by lxw on 2016/11/17.
 */
@Component
public class FirstPayAndShareLocalService {

    @Resource
    private HfActivityHandlerLocalService hfActivityHandlerLocalService;

    @Reference
    private WebUserService webUserService;

    @Reference
    private TradeLogService tradeLogService;

    /**
     * 检查活动是否存在
     * @param hfActivityCfgInfo
     * @param userId
     * @return
     */
    public ResultBean checkUserIsAttened(HfActivityCfgInfo hfActivityCfgInfo, String userId) {
        ResultBean res = hfActivityHandlerLocalService.checkActivityIsValid(hfActivityCfgInfo);
        if(res.getCode() == ResultCode.SUCCESS.get_code()) {
            //是否参加了首充|首次分享活动
            int rowNum = hfActivityHandlerLocalService.findActivityAttendNum(hfActivityCfgInfo.getId(),userId);
            if(rowNum > 0) {
                res = new ResultBean(ResultCode.ActivityIsAttended,null);
            }
        }
        return res;
    }

    /**
     * 首充处理
     * @param activityId
     * @param userId
     * @param tradeLogInfo
     * @return
     */
    public ResultBean firstPayHandler(long activityId, String userId, TradeLog tradeLogInfo) {
        HfActivityCfgInfo hfActivityCfgInfo = hfActivityHandlerLocalService.findActivityCfg(activityId);
        ResultBean res = this.checkUserIsAttened(hfActivityCfgInfo, userId);
        if(res.getCode() == ResultCode.SUCCESS.get_code()) {
            WebUser webUser = webUserService.findMyUserInfo(userId);
            if(UserTypeEnum.isVirtualUser(webUser.getUserType())) {  //虚拟用户不能进行该操作
                return new ResultBean(ResultCode.VirtualUserNotOpreate,null);
            }
            //新增用户饭票
            int rowNum = webUserService.incrWebUserBalance(userId,hfActivityCfgInfo.getGiveNum());
            if(rowNum > 0) {
                //保存充值信息
                TradeLog tradeLog = new TradeLog();
                tradeLog.setOrderId(GuuidUtil.getUuid());
                tradeLog.setUserId(userId);
                tradeLog.setUserType(webUser.getUserType());
                tradeLog.setNickName(webUser.getNickName());
                tradeLog.setPayAmount(new BigDecimal(0));
                tradeLog.setIncome(0);
                tradeLog.setExchangeAmount(new BigDecimal(0));
                tradeLog.setChannelFee(new BigDecimal(0));
                tradeLog.setPlatformCost(new BigDecimal(0));
                tradeLog.setIncomeAmount(hfActivityCfgInfo.getGiveNum());
                tradeLog.setRewardFanpiao(hfActivityCfgInfo.getGiveNum());
                tradeLog.setAccountType(TradeLogAccountTypeEnum.ACTIVITY_ACCOUNT_TYPE.getAccountType());
                tradeLog.setPaySource(0);
                tradeLog.setPayBeforeFanpiao(webUser.getBalance());
                tradeLog.setPayAfterFanpiao(webUser.getBalance()+hfActivityCfgInfo.getGiveNum());
                tradeLog.setPayStatus(TradeLogPayStatusEnum.PAY_SUCCESS.getPayStatus());
                tradeLog.setPaymentExtend(JSON.toJSONString(hfActivityCfgInfo));
                tradeLog.setPaymentType(0);
                tradeLog.setThirdPartyTradeNo(String.valueOf(hfActivityCfgInfo.getId()));
                long tradLogId = tradeLogService.saveAndCalTradeLog(tradeLog);

                //保存活动参加信息
                HfActivityAttend hfActivityAttend = new HfActivityAttend();
                hfActivityAttend.setActivityId(activityId);
                hfActivityAttend.setUserId(userId);
                hfActivityAttend.setGiveNum(hfActivityCfgInfo.getGiveNum());
                hfActivityAttend.setExtId(tradeLog.getOrderId());
                hfActivityAttend.setExtData(tradeLogInfo.getOrderId());
                hfActivityAttend.setExtNum(String.valueOf(tradeLogInfo.getIncome()));
                hfActivityHandlerLocalService.saveActivityAttendInfo(hfActivityAttend);
            }
        }

        return res;
    }

    /**
     * 首次分享处理
     * @param hfActivityCfgInfo
     * @param userId
     * @return
     */
    public ResultBean firstShareHandler(HfActivityCfgInfo hfActivityCfgInfo,String userId) {
        ResultBean res = this.checkUserIsAttened(hfActivityCfgInfo, userId);
        if(res.getCode() == ResultCode.SUCCESS.get_code()) {
            WebUser webUser = webUserService.findMyUserInfo(userId);
            if(UserTypeEnum.isVirtualUser(webUser.getUserType())) {  //虚拟用户不能进行该操作
                return new ResultBean(ResultCode.VirtualUserNotOpreate,null);
            }
            //新增用户饭票
            int rowNum = webUserService.incrWebUserBalance(userId,hfActivityCfgInfo.getGiveNum());
            if(rowNum > 0) {
                //保存充值信息
                TradeLog tradeLog = new TradeLog();
                tradeLog.setOrderId(GuuidUtil.getUuid());
                tradeLog.setUserId(userId);
                tradeLog.setUserType(webUser.getUserType());
                tradeLog.setNickName(webUser.getNickName());
                tradeLog.setPayAmount(new BigDecimal(0));
                tradeLog.setIncome(0);
                tradeLog.setExchangeAmount(new BigDecimal(0));
                tradeLog.setChannelFee(new BigDecimal(0));
                tradeLog.setPlatformCost(new BigDecimal(0));
                tradeLog.setIncomeAmount(hfActivityCfgInfo.getGiveNum());
                tradeLog.setRewardFanpiao(hfActivityCfgInfo.getGiveNum());
                tradeLog.setAccountType(TradeLogAccountTypeEnum.ACTIVITY_ACCOUNT_TYPE.getAccountType());
                tradeLog.setPaySource(0);
                tradeLog.setPayBeforeFanpiao(webUser.getBalance());
                tradeLog.setPayAfterFanpiao(webUser.getBalance()+hfActivityCfgInfo.getGiveNum());
                tradeLog.setPayStatus(TradeLogPayStatusEnum.PAY_SUCCESS.getPayStatus());
                tradeLog.setPaymentExtend(JSON.toJSONString(hfActivityCfgInfo));
                tradeLog.setPaymentType(0);
                tradeLog.setThirdPartyTradeNo(String.valueOf(hfActivityCfgInfo.getId()));
                long tradLogId = tradeLogService.saveAndCalTradeLog(tradeLog);

                //保存活动参加信息
                HfActivityAttend hfActivityAttend = new HfActivityAttend();
                hfActivityAttend.setActivityId(hfActivityCfgInfo.getId());
                hfActivityAttend.setUserId(userId);
                hfActivityAttend.setGiveNum(hfActivityCfgInfo.getGiveNum());
                hfActivityAttend.setExtId(tradeLog.getOrderId());
                hfActivityAttend.setExtData("");
                hfActivityAttend.setExtNum("");
                hfActivityHandlerLocalService.saveActivityAttendInfo(hfActivityAttend);

                res.setData(hfActivityAttend);
            } else {
                res = new ResultBean(ResultCode.ActivityAttendFailed,null);
            }
        }
        return res;
    }
}
