package com.hefan.api.service.monitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.monitor.itf.MonitorDataService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WebUserService;

@Component
public class MonitorDataLocalService {
	
	private Logger logger = LoggerFactory.getLogger(MonitorDataLocalService.class);
	
	@Reference
	MonitorDataService monitorDataService;
	
	@Reference
	WebUserService webUserService;
	
	public void sendMessToQueue(String type,String value){
		this.monitorDataService.sendMessToQueue(type, value);
	}
	
	public WebUser getWebUserByUserId(String userId){
		return webUserService.getWebUserInfoByUserId(userId);
	}

}
