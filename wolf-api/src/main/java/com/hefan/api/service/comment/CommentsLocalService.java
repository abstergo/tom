package com.hefan.api.service.comment;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.club.comment.bean.Comments;
import com.hefan.club.comment.bean.CommentsResVo;
import com.hefan.club.comment.bean.MessageResVo;
import com.hefan.club.comment.bean.ParentCommentsVo;
import com.hefan.club.comment.itf.CommentsService;
import com.hefan.club.dynamic.bean.Message;
import com.hefan.club.dynamic.itf.DynamicService;
import com.hefan.oms.bean.Present;
import com.hefan.oms.itf.PresentService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WebUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CommentsLocalService {
    @Reference
    CommentsService commentsService;
    @Reference
    DynamicService dynamicService;
    @Reference
    WebUserService webUserService;
    @Reference
    PresentService presentService;

    public Comments getCommentsByIdForCheck(long commentsId) {
        return this.commentsService.getCommentsByIdForCheck(commentsId);
    }

    /**
     * 校验动态
     *
     * @param messageId
     * @return
     * @throws
     * @Title: getMessageInfoById
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @return: Message
     * @author: LiTeng
     * @date: 2016年10月15日 下午1:14:01
     */
    public Message getMessageInfoByIdForCheck(int messageId, String userId) {
        return dynamicService.getCacheMessageInfo(String.valueOf(messageId),userId);
    }

    /**
     * 获取正文下所有品论信息
     *
     * @throws
     * @Title: getMessageComments
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param: @param messageId
     * @param: @param page
     * @param: @return
     * @return: Page
     * @author: LiTeng
     * @date: 2016年9月27日 下午5:55:36
     */
    public Map getMessageComments(int messageId, Page page) {
        List<CommentsResVo> resList = new ArrayList<CommentsResVo>();
        List<Integer> pids = new ArrayList<Integer>();
        List<Integer> mids = new ArrayList<Integer>();
        page = commentsService.getComments(messageId, "", 0, page);
        for (Object object : page) {
            Map map = (Map) object;
            if (map == null)
                continue;
            CommentsResVo com = new CommentsResVo();
            if (map.containsKey("user_id"))
                com.resUserId = (map.get("user_id") == null ? "" : map.get("user_id").toString());
            if (map.containsKey("head_img"))
                com.resHeadImg = (map.get("head_img") == null ? "" : map.get("head_img").toString());
            if (map.containsKey("nick_name"))
                com.resNickName = (map.get("nick_name") == null ? "" : map.get("nick_name").toString());
            if (map.containsKey("ancNickName"))
                com.ancNickName = (map.get("ancNickName") == null ? "" : map.get("ancNickName").toString());
            if (map.containsKey("user_type"))
                com.resUserType = (map.get("user_type") == null ? 0
                        : Integer.parseInt(map.get("user_type").toString()));
            if (map.containsKey("sex"))
                com.resSex = (map.get("sex") == null ? 2 : Integer.parseInt(map.get("sex").toString()));
            if (map.containsKey("user_level"))
                com.resLevel = (map.get("user_level") == null ? "1"
                        : StringUtils.isBlank(map.get("user_level").toString()) ? "1"
                        : map.get("user_level").toString());
            if (map.containsKey("id"))
                com.resCommentsId = (map.get("id") == null ? 0 : Integer.parseInt(map.get("id").toString()));
            if (map.containsKey("comments_info"))
                com.resCommentsInfo = (map.get("comments_info") == null ? "" : map.get("comments_info").toString());
            if (map.containsKey("comments_infostatus")) {
                com.resStatus = 0;
                if (map.get("comments_infostatus") != null) {
                    if (!map.get("comments_infostatus").toString().equals("1")) {
                        com.resStatus = 1;
                    }
                }
            }
            // 礼物id(为0不是礼物评论)
            if (map.containsKey("present_id"))
                com.presentId = (map.get("present_id") == null ? 0
                        : Integer.parseInt(map.get("present_id").toString()));
            if (map.containsKey("present_name"))
                com.resPresentName = (map.get("present_name") == null ? "" : map.get("present_name").toString());
            if (map.containsKey("present_icon"))
                com.resPresentIcon = (map.get("present_icon") == null ? "" : map.get("present_icon").toString());
            if (map.containsKey("comments_time"))
                com.resCommentsTime = (map.get("comments_time") == null ? "" : map.get("comments_time").toString());
            if (map.containsKey("parent_comments_id"))
                com.parentCommentsId = (map.get("parent_comments_id") == null ? 0
                        : Integer.parseInt(map.get("parent_comments_id").toString()));
            if (map.containsKey("message_id"))
                com.messageId = (map.get("message_id") == null ? 0
                        : Integer.parseInt(map.get("message_id").toString()));
            if (com.parentCommentsId > 0)
                pids.add(com.parentCommentsId);
            resList.add(com);
        }
        if (pids != null && pids.size() > 0) {
            Map<Integer, ParentCommentsVo> pMap = commentsService.getParentCommentsByIds(pids);
            for (CommentsResVo co : resList) {
                co.parentComment = pMap.get(co.parentCommentsId);
            }
        }
        page.setResult(resList);
        Map<String, Integer> coMap = new HashMap<String, Integer>();
        coMap.put("commentsCount", 0);
        coMap.put("presentCount", 0);
        coMap.put("praiseCount", 0);
        Map map = dynamicService.getMessageInfoById(messageId);
        if (map != null && !map.isEmpty()) {
            coMap.put("commentsCount", map.get("comments_count") == null ? 0 : Integer.parseInt(map.get("comments_count").toString()));
            coMap.put("presentCount", map.get("present_count") == null ? 0 : Integer.parseInt(map.get("present_count").toString()));
            coMap.put("praiseCount", (map.get("praise_count") == null ? 0 : Integer.parseInt(map.get("praise_count").toString())) + (map.get("ewaizan") == null ? 0 : Integer.parseInt(map.get("ewaizan").toString())));
        }
        Map resMap = new HashMap();
        resMap.put("commentsPage", page);
        resMap.put("countMs", coMap);
        return resMap;
    }

    /**
     * 获取发给我评论/回复/礼物消息
     *
     * @throws
     * @Title: getCommentsForMeByType
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param: @param userId
     * @param: @param isPresent  1-普通评论  2-礼物评论
     * @param: @param page
     * @param: @return
     * @return: Page
     * @author: LiTeng
     * @date: 2016年9月28日 下午8:42:07
     */
    public Page getCommentsForMeByType(String userId, int isPresent, Page page) {
        List<CommentsResVo> resList = new ArrayList<CommentsResVo>();
        List<Integer> pids = new ArrayList<Integer>();
        List<Integer> mids = new ArrayList<Integer>();
        page = commentsService.getComments(0, userId, isPresent, page);
        for (Object object : page) {
            Map map = (Map) object;
            if (map == null)
                continue;
            CommentsResVo com = new CommentsResVo();
            if (map.containsKey("user_id"))
                com.resUserId = (map.get("user_id") == null ? "" : map.get("user_id").toString());
            if (map.containsKey("head_img"))
                com.resHeadImg = (map.get("head_img") == null ? "" : map.get("head_img").toString());
            if (map.containsKey("nick_name"))
                com.resNickName = (map.get("nick_name") == null ? "" : map.get("nick_name").toString());
            if (map.containsKey("ancNickName"))
                com.ancNickName = (map.get("ancNickName") == null ? "" : map.get("ancNickName").toString());
            if (map.containsKey("user_type"))
                com.resUserType = (map.get("user_type") == null ? 0 : Integer.parseInt(map.get("user_type").toString()));
            if (map.containsKey("sex"))
                com.resSex = (map.get("sex") == null ? 2 : Integer.parseInt(map.get("sex").toString()));
            if (map.containsKey("user_level"))
                com.resLevel = (map.get("user_level") == null ? "1" : StringUtils.isBlank(map.get("user_level").toString()) ? "1" : map.get("user_level").toString());
            if (map.containsKey("id"))
                com.resCommentsId = (map.get("id") == null ? 0 : Integer.parseInt(map.get("id").toString()));
            if (map.containsKey("comments_info"))
                com.resCommentsInfo = (map.get("comments_info") == null ? "" : map.get("comments_info").toString());
            if (map.containsKey("comments_infostatus")) {
                com.resStatus = 0;
                if (map.get("comments_infostatus") != null) {
                    if (!map.get("comments_infostatus").toString().equals("1")) {
                        com.resStatus = 1;
                    }
                }
            }
            //礼物id(为0不是礼物评论)
            if (map.containsKey("present_id"))
                com.presentId = (map.get("present_id") == null ? 0 : Integer.parseInt(map.get("present_id").toString()));
            if (map.containsKey("present_name"))
                com.resPresentName = (map.get("present_name") == null ? "" : map.get("present_name").toString());
            if (map.containsKey("present_icon"))
                com.resPresentIcon = (map.get("present_icon") == null ? "" : map.get("present_icon").toString());
            if (map.containsKey("comments_time"))
                com.resCommentsTime = (map.get("comments_time") == null ? "" : map.get("comments_time").toString());
            if (map.containsKey("parent_comments_id"))
                com.parentCommentsId = (map.get("parent_comments_id") == null ? 0 : Integer.parseInt(map.get("parent_comments_id").toString()));
            if (map.containsKey("message_id"))
                com.messageId = (map.get("message_id") == null ? 0 : Integer.parseInt(map.get("message_id").toString()));
            if (com.parentCommentsId > 0) pids.add(com.parentCommentsId);
            if (com.messageId > 0) mids.add(com.messageId);
            resList.add(com);
        }
        if (pids != null && pids.size() > 0) {
            Map<Integer, ParentCommentsVo> pMap = commentsService.getParentCommentsByIds(pids);
            for (CommentsResVo co : resList) {
                co.parentComment = pMap.get(co.parentCommentsId);
            }
        }
        if (mids != null && mids.size() > 0) {
            Map<Integer, MessageResVo> mMap = dynamicService.getMessageResByIds(mids);
            for (CommentsResVo co : resList) {
                co.messageRes = mMap.get(co.messageId);
            }
        }
        page.setResult(resList);
        return page;
    }

    /**
     * 新增评论/回复/礼物评论
     *
     * @throws
     * @Title: saveCommentsInfo
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param: @param com
     * @param: @return
     * @return: int
     * @author: LiTeng
     * @date: 2016年9月28日 下午8:41:45
     */
    public ResultBean saveCommentsInfo(Comments com) {
        ResultBean res = new ResultBean();
        int uc = this.commentsService.saveCommentsInfo(com);
        if (uc > 0) {
            res.setCode(ResultCode.SUCCESS.get_code());
            Map<String, Integer> coMap = new HashMap<String, Integer>();
            Map map = dynamicService.getMessageInfoById(com.getMessageId());
            coMap.put("commentsCount", 0);
            coMap.put("presentCount", 0);
            coMap.put("praiseCount", 0);
            if (map != null && !map.isEmpty()) {
                coMap.put("commentsCount", map.get("comments_count") == null ? 0 : Integer.parseInt(map.get("comments_count").toString()));
                coMap.put("presentCount", map.get("present_count") == null ? 0 : Integer.parseInt(map.get("present_count").toString()));
                coMap.put("praiseCount", (map.get("praise_count") == null ? 0 : Integer.parseInt(map.get("praise_count").toString())) + (map.get("ewaizan") == null ? 0 : Integer.parseInt(map.get("ewaizan").toString())));
            }
            res.setData(coMap);
        } else {
            res.setCode(ResultCode.UNSUCCESS.get_code());
            res.setMsg("添加失败");
        }
        return res;
    }

    /**
     * 删除评论/回复
     *
     * @throws
     * @Title: deleteCommentsById
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param: @param commentsId
     * @param: @return
     * @return: int
     * @author: LiTeng
     * @date: 2016年9月28日 下午8:41:31
     */
    public ResultBean deleteCommentsById(long commentsId, int messageId, boolean isPresent,String userId) {
        ResultBean res = new ResultBean();
        int uc = this.commentsService.deleteCommentsById(commentsId, messageId, isPresent,userId);
        if (uc > 0) {
            res.setCode(ResultCode.SUCCESS.get_code());
            Map<String, Integer> coMap = new HashMap<String, Integer>();
            Map map = dynamicService.getMessageInfoById(messageId);
            coMap.put("commentsCount", 0);
            coMap.put("presentCount", 0);
            coMap.put("praiseCount", 0);
            if (map != null && !map.isEmpty()) {
                coMap.put("commentsCount", map.get("comments_count") == null ? 0 : Integer.parseInt(map.get("comments_count").toString()));
                coMap.put("presentCount", map.get("present_count") == null ? 0 : Integer.parseInt(map.get("present_count").toString()));
                coMap.put("praiseCount", (map.get("praise_count") == null ? 0 : Integer.parseInt(map.get("praise_count").toString())) + (map.get("ewaizan") == null ? 0 : Integer.parseInt(map.get("ewaizan").toString())));
            }
            res.setData(coMap);
        } else {
            res.setCode(ResultCode.UNSUCCESS.get_code());
            res.setMsg("删除失败");
        }
        return res;
    }

    public WebUser getWebUserInfoById(String userId) {
        return this.webUserService.getWebUserInfoByUserId(userId);
    }

    public Present getPresentById(Long presentId) {
        return this.presentService.getPresentById(presentId);
    }
}
