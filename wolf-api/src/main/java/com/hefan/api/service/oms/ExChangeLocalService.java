package com.hefan.api.service.oms;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.ResultBean;
import com.hefan.oms.bean.ExchangeDetail;
import com.hefan.oms.itf.AnchorTicketMonthService;
import com.hefan.oms.itf.CurrencyExchangeService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.bean.WebUserIdentity;
import org.springframework.stereotype.Component;

/**
 * Created by hbchen on 2016/10/10.
 */
@Component
public class ExChangeLocalService {

    @Reference
    AnchorTicketMonthService anchorTicketMonthService;
    @Reference
    CurrencyExchangeService currencyExchangeService;

    public int selectAvailableTickets(String userId){
        return anchorTicketMonthService.selectAvailableTickets(userId);
    }

    public void doMealTicketExchange(ExchangeDetail exchangeDetail){ currencyExchangeService.doMealTicketExchange(exchangeDetail);}

    /**
     * 用户用盒饭兑换饭票处理
     * @param exchangeDetail
     * @param webUser
     * @param webUserIdentity
     */
    public ResultBean doMealTicketExchange(ExchangeDetail exchangeDetail, WebUser webUser, WebUserIdentity webUserIdentity) {
        return currencyExchangeService.doMealTicketExchange(exchangeDetail,webUser,webUserIdentity);
    }
}
