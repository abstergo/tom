package com.hefan.api.service.comment;

import com.alibaba.dubbo.config.annotation.Reference;

import com.hefan.user.bean.Report;
import com.hefan.user.itf.ReportService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Component
public class ReportLocalService {

    @Reference
    ReportService reportService;

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public Long report(Report report){
        return reportService.report(report);
    }
}
