package com.hefan.api.controller.activity;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.api.service.activity.AppTaskLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;

@Controller
@RequestMapping("/v1/appTask")
public class AppShareTaskController {
	private Logger logger = LoggerFactory.getLogger(AppShareTaskController.class);
	@Resource
	private AppTaskLocalService appTaskLocalService;
	
	/**
	 * 分享任务接口
	 * @Title: appShareTask   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param request
	 * @return      
	 * @return: String
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月26日 下午12:18:05
	 */
	@RequestMapping(value = "/appShareTask")
	@ResponseBody
    public String appShareTask(HttpServletRequest request) {
        ResultBean res = new ResultBean(ResultCode.SUCCESS, null);
        try {
            Map paramsMap = HttpUtils.initParam(request, Map.class);
            String userId =MapUtils.getStrValue(paramsMap,"userId","");
            int shareType = MapUtils.getIntValue(paramsMap,"shareType",-1);//分享类型  1-直播间 2-俱乐部
            long shareMode = MapUtils.getIntValue(paramsMap,"shareMode",-1);//分享方式-备用
            String lUserId = HttpUtils.getParam(request, "consume", "");
			if(!userId.equals(lUserId)){
				res.setCode(ResultCode.ParamException.get_code());
    			res.setMsg("身份验证失败");
    			return JSON.toJSONString(res);
			}
            if(StringUtils.isBlank(userId)){
            	res.setCode(ResultCode.ParamException.get_code());
    			res.setMsg("盒饭ID不合法");
    			return JSON.toJSONString(res);
            }
            
            if(shareType == 1){
            	//分享直播间
            	appTaskLocalService.hFLive(userId);
            }else if(shareType ==2){
            	//分享俱乐部
            	appTaskLocalService.hFDynamic(userId);
            }
        } catch (Exception e) {
            logger.error("AppShareTaskController---appShareTask--Exception:"+e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
        }
        return JSON.toJSONString(res);
    }
}
