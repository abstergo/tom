package com.hefan.api.controller.comment;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.util.GlobalConstants;
import com.hefan.api.service.comment.ReportLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.user.bean.Report;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by hbchen on 2016/9/29.
 */
@Controller
@RequestMapping("/v1/message")
public class ReportController {

    @Resource
    ReportLocalService reportLocalService;

    public static Logger logger = LoggerFactory.getLogger(ReportController.class);

    @RequestMapping(value="/report",method= RequestMethod.GET)
    @ResponseBody
    public ResultBean report(HttpServletRequest request){

        Report report = HttpUtils.initParam(request, Report.class);
        Boolean paramsCheck = true;
        if (report == null || StringUtils.isEmpty(report.getUserId()) || StringUtils.isEmpty(report.getReportedUserId())
                || report.getReasonType() == null || report.getReportType() == null) {
            return new ResultBean(ResultCode.ParamException.get_code(), ResultCode.ParamException.getMsg());
        }
        // 检查举报类型 及对应参数
        if (report.getReportType() >= GlobalConstants.REPORT_TYPE_DYNAMIC
                && report.getReportType() <= GlobalConstants.REPORT_TYPE_COMMENT) {
            if (report.getContendId() == null) {
                paramsCheck = false;
            }
        } else if (report.getReportType() >= GlobalConstants.REPORT_TYPE_ANCHOR
                && report.getReportType() <= GlobalConstants.REPORT_TYPE_VIEWER) {
            if (StringUtils.isEmpty(report.getLiveUuid())) {
                paramsCheck = false;
            }
        } else {
            paramsCheck = false;
        }

        if (!paramsCheck) {
            return new ResultBean(ResultCode.ParamException.get_code(), ResultCode.ParamException.getMsg());
        }
        try {
            Long rowCount = reportLocalService.report(report);
        }catch (Exception e){
            e.printStackTrace();
            return new ResultBean(ResultCode.UNSUCCESS.get_code(), ResultCode.UNSUCCESS.getMsg());
        }

        return new ResultBean(ResultCode.SUCCESS.get_code(), ResultCode.SUCCESS.getMsg());
    }

}
