package com.hefan.api.controller.dynamic;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.api.service.club.ClubLocalService;
import com.hefan.club.dynamic.bean.SquareVo;
import com.hefan.common.util.HttpUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wangchao
 * @title: wolf-api
 * @package com.hefan.api.controller.club
 * @copyright: Copyright (c) 2017
 * @date 2017-03-03 15:39
 */
@Controller
@RequestMapping("/v1/square")
public class ClubController {

  @Autowired
  private ClubLocalService clubLocalService;
  public Logger logger = LoggerFactory.getLogger(ClubController.class);


  /**
   * 获取广场页数据
   */
  @RequestMapping(value = "/listSquare", method = RequestMethod.GET)
  @ResponseBody
  public String listSquare1(Page po,HttpServletRequest request) {
    ResultBean resultBean = new ResultBean();
    // 接参
    SquareVo vo = HttpUtils.initParam(request, SquareVo.class);
    if (null == vo || StringUtils.isBlank(vo.getUserId())) {
      resultBean.setCode(ResultCode.ParamException.get_code());
      resultBean.setMsg("userId is null");
      return JSON.toJSONString(resultBean);
    }
    if (vo.getPageNo() > 0) {
      po.setPageNo(vo.getPageNo());
    }
    //获取首页数据
    if (StringUtils.isBlank(vo.getMinMsgId()) && StringUtils.isBlank(vo.getMaxMsgId())) {
      return clubLocalService.squareIndex(po,vo.getUserId());
    }
    //下拉刷新加载新动态
    if (StringUtils.isNotBlank(vo.getMaxMsgId())) {
      return clubLocalService.flushSquare(po,vo.getUserId(),vo.getMaxMsgId());
    }
    //上滑获取下页数据
    if (StringUtils.isNotBlank(vo.getMinMsgId())) {
      return clubLocalService.nextPageSquare(po,vo.getUserId(),vo.getMinMsgId());
    }
    resultBean.setCode(ResultCode.UNSUCCESS.get_code());
    resultBean.setMsg("系统异常");
    return JSON.toJSONString(resultBean);
  }


}