package com.hefan.api.controller.user;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.ResultBean;
import com.hefan.user.itf.UserLevelService;
import com.hefan.user.itf.WebUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户数据修复:用户经验
 *
 * @author wangchao
 * @description
 * @date 2016/11/8 15:46
 */
@Controller
@RequestMapping("/v1/recovery")
public class DataRecoveryController {
  private Logger logger = LoggerFactory.getLogger(DataRecoveryController.class);
  @Reference
  private WebUserService webUserService;
  @Reference
  private UserLevelService userLevelService;

  /**
   * 用户经验修复
   *
   * @param req
   * @return
   */
  @RequestMapping(value = "/experience", method = RequestMethod.GET)
  @ResponseBody
  public ResultBean experience(HttpServletRequest req) {
    /*String userId = req.getParameter("userId");
    ResultBean<Object> resultBean = new ResultBean<>();

    if (StringUtils.isNotBlank(userId)) {
      try {
        WebUser user = webUserService.getWebUserInfoByUserId(userId);
        if (user == null) {
          resultBean.setCode(ResultCode.UserNotFound.get_code());
          resultBean.setMsg(ResultCode.UserNotFound.getMsg());
          return resultBean;
        }
        userLevelService.countExperienceByUserId(userId,user.getWatchTotalTime());
        resultBean.setCode(ResultCode.SUCCESS.get_code());
        resultBean.setMsg(ResultCode.SUCCESS.getMsg());
      } catch (Exception e) {
        logger.error("处理用户{}经验和等级异常{}", userId, e);
        resultBean.setCode(ResultCode.UNSUCCESS.get_code());
        resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
      }
      return resultBean;
    }
    Page<WebUser> page = new Page<>();
    int pageNo = 1;
    page.setPageNo(pageNo);
    page.setPageSize(2);
    Page<WebUser> data = webUserService.getWebListByPage(page);

    while (data != null && !CollectionUtils.isEmpty(data.getResult())) {

      MyThread myThread = new MyThread(data.getResult());
      Thread thread = new Thread(myThread);
      thread.start();
      pageNo++;
      page.setPageNo(pageNo);
      data = webUserService.getWebListByPage(page);
    }
    resultBean.setCode(ResultCode.SUCCESS.get_code());
    resultBean.setMsg(ResultCode.SUCCESS.getMsg());*/
    return null;
  }

 /* class MyThread implements Runnable {
    private List<WebUser> list;

    MyThread(List<WebUser> result) {
      list = result;
    }

    public void run() {
      for (WebUser user : list)
        try {
          userLevelService.countExperienceByUserId(user.getUserId(), user.getWatchTotalTime());
        } catch (Exception e) {
          e.printStackTrace();
          logger.error("统计用户{}经验异常{}", user.getUserId(), e);
        }

    }
  }*/

}


