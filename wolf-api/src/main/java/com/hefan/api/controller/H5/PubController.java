package com.hefan.api.controller.H5;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kevin_zhang on 01/03/2017.
 */
@Controller
@RequestMapping("/v1/h5")
public class PubController {

    /**
     * 获取系统时间
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/getSystemTime")
    @ResponseBody
    public String getSystemTime(HttpServletRequest request) {
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS, null);
        try {
            long currentTime = System.currentTimeMillis();
            resultBean.setData(currentTime);
            resultBean.setCode(ResultCode.SUCCESS.get_code());
            resultBean.setMsg(ResultCode.SUCCESS.getMsg());
            return JSON.toJSONString(resultBean);
        } catch (Exception e) {
            e.printStackTrace();
            return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
        }
    }
}
