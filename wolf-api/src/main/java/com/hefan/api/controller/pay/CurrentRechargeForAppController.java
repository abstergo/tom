package com.hefan.api.controller.pay;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.google.common.collect.Maps;
import com.hefan.api.service.CurrencyRechargeLocalService;
import com.hefan.api.service.UserLocalService;
import com.hefan.api.service.pay.PayLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.user.bean.WebUser;

@Controller
@RequestMapping("/pay")
public class CurrentRechargeForAppController {

	private Logger logger = LoggerFactory.getLogger(CurrentRechargeForAppController.class);

	@Resource
	private CurrencyRechargeLocalService currencyRechargeLocalService;
	@Resource
	private PayLocalService payLocalService;
	@Resource
	private UserLocalService userLocalService;

	@RequestMapping("/rechargeForAndriod")
	@ResponseBody
	public String getRechargeForApp(HttpServletRequest request) {
		ResultBean result = new ResultBean();
		Map mapUser = HttpUtils.initParam(request, Map.class);

		if (!mapUser.containsKey("userId")) {
			result.setCode(ResultCode.UNSUCCESS.get_code());
			result.setMsg("userId is null");
			return JSON.toJSONString(result);
		}
		String userId = mapUser.get("userId").toString();
		try {
			WebUser user = userLocalService.getWebUserByUserId(userId);
			Map map = Maps.newHashMap();
			if (user == null) {
				map.put("user", new WebUser());
			} else {
				map.put("user", user);
			}
			map.put("rechargeForAndriod", currencyRechargeLocalService.getCurrencyRecharge(1, null));
			map.put("exchange", currencyRechargeLocalService.getExchange(5, null));
			result.setCode(ResultCode.SUCCESS.get_code());
			result.setData(map);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setCode(ResultCode.UNSUCCESS.get_code());
		}
		return JSON.toJSONString(result);

	}

	@RequestMapping("/rechargeForIos")
	@ResponseBody
	public String rechargeForIos(HttpServletRequest request) {
		ResultBean result = new ResultBean();
		Map mapUser = HttpUtils.initParam(request, Map.class);
		try {
			if (!mapUser.containsKey("userId")) {
				result.setCode(ResultCode.UNSUCCESS.get_code());
				result.setMsg("userId is null");
			}
			String userId = mapUser.get("userId").toString();
			WebUser user = userLocalService.getWebUserByUserId(userId);
			Map map = Maps.newHashMap();
			if (user == null) {
				map.put("user", new WebUser());
			} else {
				map.put("user", user);
			}

			map.put("rechargeForIOS", currencyRechargeLocalService.getCurrencyRecharge(0, null));
			map.put("exchange", currencyRechargeLocalService.getExchange(6, null));
			result.setCode(ResultCode.SUCCESS.get_code());
			result.setData(map);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setCode(ResultCode.UNSUCCESS.get_code());
		}
		return JSON.toJSONString(result);

	}

	/**
	 * 根据ID查询订单状态
	 * 
	 * @param orderId
	 * @return
	 */
	@RequestMapping("/getTradeLogByOrderId")
	@ResponseBody
	public String getTradeLogByOrderId(HttpServletRequest req) {
		Map map = HttpUtils.initParam(req, Map.class);
		ResultBean resultBean = new ResultBean();
		String orderId = String.valueOf(map.get("orderId"));
		resultBean.setCode(ResultCode.SUCCESS.get_code());
		resultBean.setData(payLocalService.getTradeLogByOrderId(orderId));
		return JSON.toJSONString(resultBean);
	}

}
