package com.hefan.api.controller.H5;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.api.service.WatchLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 初始化操作接口（内部使用）
 * Created by kevin_zhang on 06/04/2017.
 */
@Controller
@RequestMapping("/v1/h5")
public class InternalController {
    @Resource
    WatchLocalService watchLocalService;

    /**
     * 恢复指定人的关注信息
     *
     * @return
     */
    @RequestMapping(value = "/recoverWatchRelationCacheByUserId", method = RequestMethod.GET)
    @ResponseBody
    @SuppressWarnings({"rawtypes", "unchecked"})
    public String recoverWatchRelationCacheByUserId(HttpServletRequest req) {
        Map<String, Object> map = HttpUtils.initParam(req, Map.class);
        String userId = MapUtils.getStrValue(map, "userId", "");

        //参数验证
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
        if (StringUtils.isEmpty(userId)) {
            resultBean.setResult(ResultCode.ParamException);
            return JSON.toJSONString(resultBean);
        }

        try {
            watchLocalService.recoverWatchRelationCacheByUserId(userId);
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setResult(ResultCode.UNSUCCESS);
        }
        return JSON.toJSONString(resultBean);
    }

    /**
     * 清空所有关注缓存（慎重）
     *
     * @return
     */
    @RequestMapping(value = "/clearWatchRelationCache", method = RequestMethod.GET)
    @ResponseBody
    @SuppressWarnings({"rawtypes", "unchecked"})
    public String clearWatchRelationCache(HttpServletRequest req) {
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);

        try {
            watchLocalService.clearWatchRelationCache();
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setResult(ResultCode.UNSUCCESS);
        }
        return JSON.toJSONString(resultBean);
    }

    /**
     * 恢复所有关注缓存（慎重）
     *
     * @return
     */
    @RequestMapping(value = "/initWatchRelationCache", method = RequestMethod.GET)
    @ResponseBody
    @SuppressWarnings({"rawtypes", "unchecked"})
    public String initWatchRelationCache(HttpServletRequest req) {
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);

        try {
            watchLocalService.initWatchRelationCache(0);
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setResult(ResultCode.UNSUCCESS);
        }
        return JSON.toJSONString(resultBean);
    }
}
