package com.hefan.api.controller.activity;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.service.JedisService;
import com.hefan.api.bean.LiveActivityVo;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kevin_zhang on 27/02/2017.
 */
@Controller
@RequestMapping("/v1/liveActivity")
public class LiveActivityController {
    private Logger logger = LoggerFactory.getLogger(LiveActivityController.class);

    @Resource
    JedisService jedisService;

    private static final String LIVE_ACTIVITY_KEY = "live_room_chartlet";

    /**
     * 获取直播间活动信息
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/getLiveActivity")
    @ResponseBody
    public String getLiveActivity(HttpServletRequest request) {
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS, null);
        // 接参
        Map paramMap = HttpUtils.initParam(request, Map.class);
        if (null == paramMap || paramMap.isEmpty()) {
            resultBean.setCode(ResultCode.ParamException.get_code());
            resultBean.setMsg(ResultCode.ParamException.getMsg());
            return JSON.toJSONString(resultBean);
        }
        String userId = MapUtils.getStrValue(paramMap, "userId", "");// 进入直播间用户id
        String authoruserId = MapUtils.getStrValue(paramMap, "authoruserId", "");// 主播id
        int chatRoomId = MapUtils.getIntValue(paramMap, "chatRoomId", -1);// 进入直播间对应聊天室id
        String liveUuid = MapUtils.getStrValue(paramMap, "liveUuid", "");// 直播间UUID
        if (StringUtils.isBlank(userId) || chatRoomId < 0 || StringUtils.isBlank(liveUuid) || StringUtils.isBlank(authoruserId)) {
            resultBean.setCode(ResultCode.ParamException.get_code());
            resultBean.setMsg(ResultCode.ParamException.getMsg());
            return JSON.toJSONString(resultBean);
        }

        try {
            List arr = new ArrayList();
            String infoRedis = jedisService.getStr(LIVE_ACTIVITY_KEY);
            if (StringUtils.isNotBlank(infoRedis)) {
                LiveActivityVo liveActivityVo = JSON.parseObject(infoRedis, LiveActivityVo.class);
                if (null != liveActivityVo) {
                    //定时活动
                    if (liveActivityVo.getIsTiming() == 1) {
                        if (System.currentTimeMillis() >= liveActivityVo.getStartTime()
                                && System.currentTimeMillis() <= liveActivityVo.getEndTime()) {
                            arr.add(liveActivityVo);
                        }
                    } else {
                        arr.add(liveActivityVo);
                    }
                }
            }
            resultBean.setData(arr);
            resultBean.setCode(ResultCode.SUCCESS.get_code());
            resultBean.setMsg(ResultCode.SUCCESS.getMsg());
            return JSON.toJSONString(resultBean);
        } catch (Exception e) {
            e.printStackTrace();
            return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
        }
    }
}
