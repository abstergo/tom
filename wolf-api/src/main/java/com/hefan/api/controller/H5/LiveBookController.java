package com.hefan.api.controller.H5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.api.service.LiveBookLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.StringUtil;
import com.hefan.user.bean.LiveNoticeReserve;

/**
 * 直播预约h5相关
 * @author sagagyq
 *
 */
@Controller
@RequestMapping("/v1/h5")
public class LiveBookController {
	@Resource
	LiveBookLocalService liveBookLocalService;
	private Logger logger = LoggerFactory.getLogger(LiveBookController.class);
	
	/**
	 * 直播预约功能
	 * @param req
	 * @return
	 */
	@RequestMapping(value="/liveBooking")
	@ResponseBody
	public ResultBean liveBooking(HttpServletRequest req){
		
		LiveNoticeReserve liveNoticeReserve  = HttpUtils.initParam(req, LiveNoticeReserve.class);
		if(liveNoticeReserve == null){
			return new ResultBean(ResultCode.ParamException.get_code(),ResultCode.ParamException.getMsg());
		}
		if(liveNoticeReserve.getLiveNoticeId() == 0){
			return new ResultBean(ResultCode.ParamException.get_code(),"liveNoticeId为空");
		}
		if(StringUtils.isBlank(liveNoticeReserve.getUserId())){
			return new ResultBean(ResultCode.ParamException.get_code(),"userId为空");
		}
		try {
			return liveBookLocalService.liveBooking(liveNoticeReserve);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return new ResultBean(ResultCode.UnknownException.get_code(),e.getMessage());
		}
	}
	
	/**
	 * 直播预约状态查询
	 * @param req
	 * @return
	 */
	@RequestMapping(value="/liveBookingStatus")
	@ResponseBody
	public ResultBean liveBookingStatus(HttpServletRequest req){
		Map params = HttpUtils.initParam(req, HashMap.class);
		
		
		String liveNoticeId = String.valueOf(params.get("liveNoticeId"));
		String userId = String.valueOf(params.get("userId"));
		if(params==null || params.isEmpty()  ){
			return new ResultBean(ResultCode.ParamException.get_code(),ResultCode.ParamException.getMsg());
		}
		if(StringUtils.isBlank(liveNoticeId)){
			return new ResultBean(ResultCode.ParamException.get_code(),"liveNoticeId为空");
		}
		if(StringUtils.isBlank(userId)){
			return new ResultBean(ResultCode.ParamException.get_code(),"userId为空");
		}
		try {
			List<String> idList =  StringUtil.splitToList(",", liveNoticeId);
			
			List<LiveNoticeReserve> liveNoticeReserveList = new ArrayList<LiveNoticeReserve>();
			for(String id:idList){
				LiveNoticeReserve liveNoticeReserve = new LiveNoticeReserve();
				liveNoticeReserve.setUserId(userId);
				liveNoticeReserve.setLiveNoticeId(Integer.valueOf(id));
				liveNoticeReserveList.add(liveNoticeReserve);
			}
			return liveBookLocalService.liveBookingStatus(liveNoticeReserveList);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		return new ResultBean(ResultCode.UnknownException.get_code(),e.getMessage());
		}
	}
	
}
