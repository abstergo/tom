package com.hefan.api.controller.user;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.api.service.WatchLocalService;
import com.hefan.api.util.StringUtil;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author sagagyq
 */
@Controller
@RequestMapping("/v1/watch")
public class WatchController {

    @Resource
    WatchLocalService watchLocalService;

    /**
     * 用户关注的所有用户的id列表
     *
     * @return
     */
    @RequestMapping(value = "/listWatchId", method = RequestMethod.GET)
    @ResponseBody
    public String listWatchId(HttpServletRequest req) {
        Map<String, Object> map = HttpUtils.initParam(req, Map.class);
        String userId = MapUtils.getStrValue(map, "userId", "");

        //参数验证
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
        if (StringUtils.isEmpty(userId)) {
            resultBean.setResult(ResultCode.ParamException);
            return JSON.toJSONString(resultBean);
        }

        List<String> ids = new ArrayList<>();
        try {
            if (StringUtils.isEmpty(userId)) {
                resultBean.setResult(ResultCode.ParamException);
            } else {
                ids = watchLocalService.listWatchId(userId);
            }
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setResult(ResultCode.UnknownException);
        }
        resultBean.setData(ids);
        return JSON.toJSONString(resultBean);
    }

    /**
     * 关注
     *
     * @return
     */
    @RequestMapping(value = "/fork", method = RequestMethod.GET)
    @ResponseBody
    public String fork(HttpServletRequest req) {
        Map<String, Object> map = HttpUtils.initParam(req, Map.class);
        String userId = MapUtils.getStrValue(map, "userId", "");
        String mainUserId = MapUtils.getStrValue(map, "mainUserId", "");
        int liveRemind = MapUtils.getIntValue(map, "liveRemind", 1); //0：不接受主播的开播提醒 1：接受主播的开播提醒

        //参数验证
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(mainUserId)) {
            resultBean.setResult(ResultCode.ParamException);
            return JSON.toJSONString(resultBean);
        }

        try {
            resultBean = watchLocalService.fork(userId, mainUserId, liveRemind);
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setResult(ResultCode.UNSUCCESS);
        }
        return JSON.toJSONString(resultBean);
    }

    /**
     * 批量关注 并设置用户进入俱乐部的状态
     *
     * @return
     */
    @RequestMapping(value = "/batchFork", method = RequestMethod.GET)
    @ResponseBody
    public String batchFork(HttpServletRequest req) {
        Map<String, Object> map = HttpUtils.initParam(req, Map.class);
        String userId = MapUtils.getStrValue(map, "userId", "");
        String mainUserIds = MapUtils.getStrValue(map, "mainUserIds", "");

        //参数验证
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(mainUserIds)
                || !StringUtil.isCommaSplitStr(mainUserIds)
                || StringUtil.splitToList(",", mainUserIds).size() <= 0) {
            resultBean.setResult(ResultCode.ParamException);
            return JSON.toJSONString(resultBean);
        }

        try {
            resultBean = watchLocalService.batchFork(userId, mainUserIds);
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setResult(ResultCode.UNSUCCESS);
        }
        return JSON.toJSONString(resultBean);
    }

    /**
     * 取消关注
     *
     * @return
     */
    @RequestMapping(value = "/unfork", method = RequestMethod.GET)
    @ResponseBody
    public String unfork(HttpServletRequest req) {
        Map<String, Object> map = HttpUtils.initParam(req, Map.class);
        String userId = MapUtils.getStrValue(map, "userId", "");
        String mainUserId = MapUtils.getStrValue(map, "mainUserId", "");

        //参数验证
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(mainUserId) || "100000001".equals(mainUserId)) {
            resultBean.setResult(ResultCode.ParamException);
            return JSON.toJSONString(resultBean);
        }

        try {
            resultBean = watchLocalService.unfork(userId, mainUserId);
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setResult(ResultCode.UNSUCCESS);
        }
        return JSON.toJSONString(resultBean);
    }

    /**
     * 我关注的用户列表分页
     *
     * @return
     */
    @RequestMapping(value = "/listWatchAnchorInfo", method = RequestMethod.GET)
    @ResponseBody
    @SuppressWarnings({"rawtypes", "unchecked"})
    public String listWatchAnchorInfo(HttpServletRequest req) {
        Map<String, Object> map = HttpUtils.initParam(req, Map.class);
        String userId = MapUtils.getStrValue(map, "userId", "");
        int pageNo = MapUtils.getIntValue(map, "pageNo", 1);
        int pageSize = MapUtils.getIntValue(map, "pageSize", 20);
        double latestScore = MapUtils.getDoubleValue(map, "uniqueId", -1);

        //参数验证
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
        if (StringUtils.isEmpty(userId)) {
            resultBean.setResult(ResultCode.ParamException);
            return JSON.toJSONString(resultBean);
        }

        try {
            resultBean = watchLocalService.listWatchAnchorInfo(pageNo, pageSize, userId, BigDecimal.valueOf(latestScore));
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setResult(ResultCode.UNSUCCESS);
        }
        return JSON.toJSONString(resultBean);
    }


    /**
     * 我的粉丝
     *
     * @return
     */
    @RequestMapping(value = "/listFansInfo", method = RequestMethod.GET)
    @ResponseBody
    @SuppressWarnings({"rawtypes", "unchecked"})
    public String listFansInfo(HttpServletRequest req) {

        Map<String, Object> map = HttpUtils.initParam(req, Map.class);
        String userId = MapUtils.getStrValue(map, "userId", "");
        Integer pageNo = MapUtils.getIntValue(map, "pageNo", 1);
        Integer pageSize = MapUtils.getIntValue(map, "pageSize", 10);
        ResultBean resultBean = new ResultBean();
        //参数验证标记
        if (StringUtils.isEmpty(userId)) {
            resultBean.setCode(ResultCode.ParamException.get_code());
            resultBean.setMsg(ResultCode.ParamException.getMsg());
            return JSON.toJSONString(resultBean);
        }
        try {
            Page page = new Page();
            page.setPageNo(pageNo);
            page.setOrderBy("time");
            page.setOrder("DESC");
            page.setPageSize(pageSize);
            page = watchLocalService.listFansInfo(page, userId);
            if (page != null) {
                List<Map<String, Object>> list = page.getResult();
                for (Map<String, Object> temp : list) {
                    String user = String.valueOf(temp.get("userId"));
                    int type = watchLocalService.getWatchRelationByUserId(userId, user);
                    temp.put("isFollow", type);
                }
            }
            resultBean.setData(page);
            resultBean.setCode(ResultCode.SUCCESS.get_code());
            resultBean.setMsg(ResultCode.SUCCESS.getMsg());
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setCode(ResultCode.UNSUCCESS.get_code());
            resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
        }
        return JSON.toJSONString(resultBean);
    }

    /**
     * 获取关注人数
     *
     * @return
     */
    @RequestMapping(value = "/watchNum", method = RequestMethod.GET)
    @ResponseBody
    @SuppressWarnings({"rawtypes", "unchecked"})
    public String watchNum(HttpServletRequest req) {
        Map<String, Object> map = HttpUtils.initParam(req, Map.class);
        String userId = MapUtils.getStrValue(map, "userId", "");

        //参数验证
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
        if (StringUtils.isEmpty(userId)) {
            resultBean.setResult(ResultCode.ParamException);
            return JSON.toJSONString(resultBean);
        }

        try {
            resultBean.setData(watchLocalService.watchNum(userId));
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setResult(ResultCode.UNSUCCESS);
        }
        return JSON.toJSONString(resultBean);
    }

    /**
     * 获取关注人列表
     *
     * @return
     */
    @RequestMapping(value = "/getAllWatchList", method = RequestMethod.GET)
    @ResponseBody
    @SuppressWarnings({"rawtypes", "unchecked"})
    public String getAllWatchList(HttpServletRequest req) {
        Map<String, Object> map = HttpUtils.initParam(req, Map.class);
        String userId = MapUtils.getStrValue(map, "userId", "");

        //参数验证
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
        if (StringUtils.isEmpty(userId)) {
            resultBean.setResult(ResultCode.ParamException);
            return JSON.toJSONString(resultBean);
        }

        try {
            resultBean.setData(watchLocalService.getAllWatchList(userId));
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setResult(ResultCode.UNSUCCESS);
        }
        return JSON.toJSONString(resultBean);
    }
}
