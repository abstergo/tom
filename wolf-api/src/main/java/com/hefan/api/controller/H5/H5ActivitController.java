package com.hefan.api.controller.H5;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.common.meta.UserTypeEnum;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.activity.bean.HfActivityAttend;
import com.hefan.activity.bean.HfActivityCfgInfo;
import com.hefan.api.service.UserLocalService;
import com.hefan.api.service.activity.ConferenceActivityLocalService;
import com.hefan.api.service.activity.FirstPayAndShareLocalService;
import com.hefan.api.service.activity.HfActivityHandlerLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.user.bean.WebUser;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lxw on 2016/11/17.
 */
@Controller
@RequestMapping("/v1/h5")
public class H5ActivitController {
    private Logger logger = LoggerFactory.getLogger(H5ActivitController.class);
    @Resource
    private HfActivityHandlerLocalService hfActivityHandlerLocalService;
    @Resource
    private FirstPayAndShareLocalService firstPayAndShareLocalService;
    @Resource
    private UserLocalService userLocalService;
    @Resource
    private ConferenceActivityLocalService conferenceActivityLocalService;
    /**
     * 首次分享进入查询
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/firstShareQuery")
    @ResponseBody
    public String firstShareQuery(HttpServletRequest request) {
        ResultBean res = new ResultBean(ResultCode.SUCCESS, null);
        try {
            Map paramsMap = HttpUtils.initParam(request, Map.class);
            if (CollectionUtils.isEmpty(paramsMap) || !paramsMap.containsKey("userId") ||
                    StringUtils.isBlank((String) paramsMap.get("userId"))) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            String userId = (String) paramsMap.get("userId");
            WebUser webUser = userLocalService.getWebUserByUserId(userId);
            if(webUser == null) {
                return JSON.toJSONString(new ResultBean(ResultCode.LoginUserIsNotExist, null));
            }
            HfActivityCfgInfo hfActivityCfgInfo = hfActivityHandlerLocalService.findActivityCfg(2);
            ResultBean checkRes = firstPayAndShareLocalService.checkUserIsAttened(hfActivityCfgInfo,userId);
            Map resMap = new HashMap();
            String reqData = "";
            int needReqFlag = 2;
            if(checkRes.getCode() == ResultCode.SUCCESS.get_code()) {
                needReqFlag = 1;
                Map appReqData = new HashMap();
                appReqData.put("activityId",hfActivityCfgInfo.getId());
                appReqData.put("userId",userId);
                reqData = JSON.toJSONString(appReqData);
            }
            resMap.put("needReqFlag",needReqFlag);
            resMap.put("appReqData",reqData);
            resMap.put("userType",webUser.getUserType());
            res.setData(resMap);
        } catch (Exception e) {
            logger.error("H5ActivitController---firstShareQuery--Exception:"+e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
        }
        return JSON.toJSONString(res);
    }

    /**
     * 发布会活动进入接口
     * @return
     */
    @RequestMapping(value = "/conferenceActivityShow")
    @ResponseBody
    public String conferenceActivityShow(HttpServletRequest request) {
        ResultBean res = new ResultBean(ResultCode.SUCCESS, null);
        try {
            Map paramsMap = HttpUtils.initParam(request, Map.class);
            if (CollectionUtils.isEmpty(paramsMap) || !paramsMap.containsKey("userId") ||
                    StringUtils.isBlank((String) paramsMap.get("userId"))) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            String userId = (String) paramsMap.get("userId");
            res = conferenceActivityLocalService.getConferenceActivityPageShow(userId,3);
        } catch (Exception e) {
            logger.error("H5ActivitController---conferenceActivityShow--Exception:"+e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
        }
        return JSON.toJSONString(res);
    }

    /**
     * 参加活动的历史
     * @param request
     * @return
     */
    @RequestMapping(value = "/activityAttendHis")
    @ResponseBody
    public String findActivityAttendHis(HttpServletRequest request) {
        ResultBean res = new ResultBean(ResultCode.SUCCESS, null);
        try {
                Map paramsMap = HttpUtils.initParam(request, Map.class);
            if (CollectionUtils.isEmpty(paramsMap) ||
                    !paramsMap.containsKey("activityId") ||
                    !paramsMap.containsKey("rowNum")
                    ) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            long activityId = Long.valueOf(String.valueOf(paramsMap.get("activityId")));
            int rowNum = Integer.valueOf(String.valueOf(paramsMap.get("rowNum")));

            List<HfActivityAttend> hisList = hfActivityHandlerLocalService.findActivityAttendHis(activityId,rowNum);
            res.setData(hisList);
        } catch (Exception e) {
            logger.error("H5ActivitController---findActivityAttendHis--Exception:"+e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
        }
        return JSON.toJSONString(res);
    }

    /**
     * 保存用户抽奖信息
     * @param request
     * @return
     */
    @RequestMapping(value = "/saveConferenceAwardInfo")
    @ResponseBody
    public String saveConferenceAwardInfo(HttpServletRequest request) {
        ResultBean res = new ResultBean(ResultCode.SUCCESS, null);
        try {
            Map paramsMap = HttpUtils.initParam(request, Map.class);
            if (CollectionUtils.isEmpty(paramsMap)
                    || !paramsMap.containsKey("userId")
                    || !paramsMap.containsKey("activityId")
                    ) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            long activityId = Long.valueOf(String.valueOf(paramsMap.get("activityId")));
            if(activityId != 3) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            String userId = (String) paramsMap.get("userId");
            res = conferenceActivityLocalService.saveActivityAttendOprate(userId,activityId);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("H5ActivitController---saveConferenceAwardInfo--Exception:"+e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
        }
        return JSON.toJSONString(res);
    }
}
