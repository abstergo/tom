package com.hefan.api.controller.dynamic;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.google.common.collect.Maps;
import com.hefan.api.service.DynamicLocalService;
import com.hefan.api.service.club.ClubLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by nigle on 2017/3/8.
 */
@Controller
@RequestMapping("/v1/follow")
public class FollowController {

    private Logger logger = LoggerFactory.getLogger(FollowController.class);

    @Autowired
    private ClubLocalService clubLocalService;

    /**
     * 关注小红点
     */
    @RequestMapping(value = "/redDot", method = RequestMethod.GET)
    @ResponseBody
    public String followRedDot(HttpServletRequest request) {
        ResultBean resultBean = new ResultBean();
        Map paramMap = HttpUtils.initParam(request, Map.class);
        String userId = MapUtils.getStrValue(paramMap,"userId","");
        int msgId = MapUtils.getIntValue(paramMap,"msgId",0);
        if (StringUtils.isBlank(userId) || msgId <=0) {
            resultBean.setCode(ResultCode.ParamException.get_code());
            resultBean.setMsg(ResultCode.ParamException.getMsg());
            return JSON.toJSONString(resultBean);
        }
        resultBean.setCode(ResultCode.SUCCESS.get_code());
        resultBean.setMsg(ResultCode.SUCCESS.getMsg());
        Map map = Maps.newHashMap();
        if (clubLocalService.followRedDot(userId,msgId)) {
            map.put("latest", 1);
        }else {
            map.put("latest", 0);
        }
        resultBean.setData(map);
        return JSON.toJSONString(resultBean);
    }
}
