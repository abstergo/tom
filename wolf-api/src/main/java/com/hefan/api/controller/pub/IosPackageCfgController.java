package com.hefan.api.controller.pub;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.api.bean.LoginUserVo;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.bean.Message;
import com.hefan.common.util.DynamicProperties;
import com.hefan.common.util.HttpUtils;
import com.hefan.user.itf.HfIosPackageCfgService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/27.
 */
@Controller
@RequestMapping("/pub/v1")
public class IosPackageCfgController {
    Logger logger = LoggerFactory.getLogger(ReceiveImCcController.class);
    @Reference
    private HfIosPackageCfgService hfIosPackageCfgService;

    /**
     * ios获取马甲包是否可用接口
     * @param request
     * @return
     */
    @RequestMapping("/getIosPackageIsUse")
    @ResponseBody
    public String getIosPackageIsUse(HttpServletRequest request) {
        ResultBean res = new ResultBean(ResultCode.SUCCESS);
        try {
            Map<String,String> paramMap = HttpUtils.initParam(request,Map.class);
            if(CollectionUtils.isEmpty(paramMap) || !paramMap.containsKey("bundleId")
                    || StringUtils.isBlank(paramMap.get("bundleId"))) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            int isUse = hfIosPackageCfgService.getIosPackageCfgIsUse(paramMap.get("bundleId"));
            res.setData(isUse);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UNSUCCESS,null));
        }
        return JSON.toJSONString(res);
    }
}
