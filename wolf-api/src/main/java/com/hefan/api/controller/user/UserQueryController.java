package com.hefan.api.controller.user;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.common.meta.UserTypeEnum;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.api.bean.LoginUserVo;
import com.hefan.api.service.UserLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.user.bean.WebUser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * Created by lxw on 2016/10/9.
 */
@Controller
@RequestMapping("/v1/user")
public class UserQueryController {

    public static Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource
    private UserLocalService userLocalService;

    /**
     * 根据多个userId的拼接串，获取用户基本信息列表
     * @param request
     * @return
     */
    @RequestMapping("/findUserByIds")
    @ResponseBody
    public String findUserList(HttpServletRequest request) {
        //Profiler profiler = new Profiler("myUserInfo--start");
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS,null);
        try {
            Map<String,String> paramMap = HttpUtils.initParam(request,HashMap.class);
            if(CollectionUtils.isEmpty(paramMap) || !paramMap.containsKey("userIds")
                    || StringUtils.isBlank(paramMap.get("userIds"))) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            String userIds = paramMap.get("userIds");
            List userIdList = Arrays.asList(userIds.split(","));
            if(userIdList == null || userIdList.isEmpty()) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            resultBean = userLocalService.queryUserInfoByUserIdList(userIdList);
        }catch (Exception e) {
            logger.error(e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UNSUCCESS,null));
        } finally {
            //profiler.stop().print();
        }
        return JSON.toJSONString(resultBean);
    }

    /**
     * 获取用户我的信息
     * @param request
     * @return
     */
    @RequestMapping("/myUserInfo")
    @ResponseBody
    public String myUserInfo(HttpServletRequest request) {
        //Profiler profiler = new Profiler("myUserInfo--start");
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS,null);
        try {
            Map<String,String> paramMap = HttpUtils.initParam(request,HashMap.class);
            if(CollectionUtils.isEmpty(paramMap) || !paramMap.containsKey("userId")
                    || StringUtils.isBlank(paramMap.get("userId"))) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }

            String userId = paramMap.get("userId");
            resultBean = userLocalService.findMyUserInfo(userId);
        }catch (Exception e) {
            logger.error(e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UNSUCCESS,null));
        } finally {
           //profiler.stop().print();
        }
        return JSON.toJSONString(resultBean);
    }

    /**
     * 获取用户的基础信息
     * @param request
     * @return
     */
    @RequestMapping("/getUserBaseInfo")
    @ResponseBody
    public String getUserBaseInfo(HttpServletRequest request) {
        //Profiler profiler = new Profiler("getUserBaseInfo--start");
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS,null);
        try {
            Map<String,String> paramMap = HttpUtils.initParam(request,HashMap.class);
            if(CollectionUtils.isEmpty(paramMap) || !paramMap.containsKey("userId")
                    || StringUtils.isBlank(paramMap.get("userId"))) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            String userId = paramMap.get("userId");
            WebUser webUser = userLocalService.getWebUserByUserId(userId);
            resultBean.setData(webUser);
        }catch (Exception e) {
            logger.error(e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UNSUCCESS,null));
        } finally {
           //profiler.stop().print();
        }
        return JSON.toJSONString(resultBean);
    }

    /**
     * 获取用户可用盒饭数
     * @param request
     * @return
     */
    @RequestMapping("/getValidHeFanCount")
    @ResponseBody
    public String getUserValidHeFanCount(HttpServletRequest request) {
        //Profiler profiler = new Profiler("getUserValidHeFanCount--start");
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS,null);
        try {
            Map<String,String> paramMap = HttpUtils.initParam(request,HashMap.class);
            if(CollectionUtils.isEmpty(paramMap) || !paramMap.containsKey("userId")
                    || StringUtils.isBlank(paramMap.get("userId"))) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            String userId = paramMap.get("userId");
            WebUser webUser = userLocalService.findUserInfoFromCache(userId);
            if(webUser == null) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            long validHeFanCount = 0L;
            if(UserTypeEnum.isAnchor(webUser.getUserType())) {
                //获取主播可用盒饭数
                validHeFanCount = userLocalService.getWebUserValidHeFanCount(userId);
            } else {
                //获取普通用户的可用盒饭数
                validHeFanCount = userLocalService.getCommonUserValidHeFanCount(userId);
            }
            Map returnMap = new HashMap();
            returnMap.put("validHeFanCount",validHeFanCount);
            resultBean.setData(returnMap);
        }catch (Exception e) {
            logger.error(e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UNSUCCESS,null));
        } finally {
            //profiler.stop().print();
        }
        return JSON.toJSONString(resultBean);
    }


    /**
     * 用简介查询
     * @param request
     * @return
     */
    @RequestMapping(value = "/getProfileApp")
    @ResponseBody
    public String getPersonalProfile(HttpServletRequest request) {
        //Profiler profiler = new Profiler("getPersonalProfile--start");
        ResultBean res = new ResultBean(ResultCode.SUCCESS,null);
        try {
            Map<String,String> paramMap = HttpUtils.initParam(request,HashMap.class);
            if(CollectionUtils.isEmpty(paramMap) || !paramMap.containsKey("userId") || StringUtils.isBlank(String.valueOf(paramMap.get("userId")))) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException,null));
            }
            String userId = String.valueOf(paramMap.get("userId"));
            //获取用户基本信息|粉丝|关注数
            Map resMap = userLocalService.getUserInfoAndIdentity(userId);
            res.setData(resMap);
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UnknownException,null));
        } finally {
            //profiler.stop().print();
        }
        return JSON.toJSONString(res);
    }


    /**
     * 用户缓存信息操作测试
     * @param request
     * @return
     */
   /* @RequestMapping(value = "/userCachOprateTest")
    @ResponseBody*/
    public String userCachOprateTest(HttpServletRequest request) {
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS,null);
        try {
            Map paramMap = HttpUtils.initParam(request,HashMap.class);
            if(CollectionUtils.isEmpty(paramMap) || !paramMap.containsKey("userId")
                    || StringUtils.isBlank(String.valueOf(paramMap.get("userId")))) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            String userId = String.valueOf(paramMap.get("userId"));
            int cachType = Integer.valueOf(String.valueOf(paramMap.get("cachType")));
            long icrVal = Long.valueOf(String.valueOf(paramMap.get("icrVal")));
            Map resMap = userLocalService.userCachOpTest(userId,cachType,icrVal);
            resultBean.setData(resMap);
            }catch (Exception e) {
            logger.error(e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UNSUCCESS,null));
        }
        return JSON.toJSONString(resultBean);
    }

    @RequestMapping("/userPreInitTest")
    @ResponseBody
    public  String userPreInitTest(HttpServletRequest request) {
        String userId = HttpUtils.getParam(request,"userId","0");
        WebUser webUser = new WebUser();
        webUser.setUserId(userId);
        webUser.setNickName(userId);
        String imToke = userLocalService.getImTokenOprate(webUser);
        return JSON.toJSONString(new ResultBean(ResultCode.SUCCESS,imToke));
    }

}
