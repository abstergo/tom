package com.hefan.api.controller.push;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.api.service.push.PushLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.notify.bean.PubParams;

@Controller
@RequestMapping("/v1/push")
public class PushMessageController {

	public static Logger logger = LoggerFactory.getLogger(PushMessageController.class);

	@Resource
	PushLocalService pushLocalService;

	/**
	 * 广播
	 * @Title: uPushBroadcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param request
	 * @return      
	 * @return: String
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月9日 下午7:14:14
	 */
	@RequestMapping("/uPushBroadcast")
	@ResponseBody
	public String uPushBroadcast(HttpServletRequest request) {
		ResultBean res = new ResultBean();
		try {
			// 接参
			PubParams params = HttpUtils.initParam(request, PubParams.class);
			if (params == null || StringUtils.isEmpty(params.getTicker()) || StringUtils.isEmpty(params.getTitle())
					|| StringUtils.isEmpty(params.getText()) || params.getAfterOpenType() < 1
					|| params.getAfterOpenType() > 4) {
				res.setCode(ResultCode.ParamException.get_code());
    			res.setMsg(ResultCode.ParamException.getMsg());
    			return JSON.toJSONString(res);
			}
			pushLocalService.uPushBroadcast(params);
			res.setCode(ResultCode.SUCCESS.get_code());
			return JSON.toJSONString(res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return JSON.toJSONString(res);
		}
	}
}
