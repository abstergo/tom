package com.hefan.api.controller.pub;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.OssFileTypeEnum;
import com.cat.common.meta.ResultCode;
import com.cat.common.util.GuuidUtil;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.common.util.DynamicProperties;
import com.hefan.common.util.HttpUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by lxw on 2016/10/9.
 */
@Controller
@RequestMapping("/pub/fileRule")
public class OssFileRuleCreaterController {

    private Logger logger = LoggerFactory.getLogger(OssFileRuleCreaterController.class);

    /**
     * 生成上传OSS文件相应规则信息
     * @param request
     * @return
     */
    @RequestMapping("/fileRuleCreater")
    @ResponseBody
    public String fileRuleCreater(HttpServletRequest request) {
        try {
            Map paramsMap = HttpUtils.initParam(request, HashMap.class);
            if (CollectionUtils.isEmpty(paramsMap) || !paramsMap.containsKey("type") || !paramsMap.containsKey("createNum")) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            String type = String.valueOf(paramsMap.get("type"));
            if (StringUtils.isBlank(type)) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            OssFileTypeEnum ossFileTypeEnum = OssFileTypeEnum.findOssFileTypeEnumByType(type);
            if(ossFileTypeEnum == null
                    || StringUtils.isBlank(DynamicProperties.getString(ossFileTypeEnum.getBucketNameProp()))
                    || StringUtils.isBlank(DynamicProperties.getString(ossFileTypeEnum.getAccessDmainProp()))
                    || StringUtils.isBlank(DynamicProperties.getString(ossFileTypeEnum.getOssEndpointProp()))
                    ) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            String bucketName = DynamicProperties.getString(ossFileTypeEnum.getBucketNameProp());
            int createNum = Integer.valueOf(String.valueOf(paramsMap.get("createNum"))).intValue();
            createNum = createNum == 0 ? 1 : createNum;
            String dateDir = new SimpleDateFormat("yyyyMMdd").format(new Date());
            List<Map<String, String>> resList = new ArrayList<Map<String, String>>();
            for (int i = 0; i < createNum; i++) {
                Map<String, String> temMap = new HashMap<String, String>();
                temMap.put("bucketName", bucketName);
                temMap.put("filePrefix", dateDir + "/" + GuuidUtil.getUuid());
                temMap.put("accessDomain", DynamicProperties.getString(ossFileTypeEnum.getAccessDmainProp()));
                resList.add(temMap);
            }

            Map resMap = new HashMap();
            resMap.put("ossEndpoint",DynamicProperties.getString(ossFileTypeEnum.getOssEndpointProp()));
            resMap.put("accessKey", DynamicProperties.getString("oss.AccessKey"));
            resMap.put("secretKey",DynamicProperties.getString("oss.SecretKey"));
            resMap.put("rules",resList);
            return JSON.toJSONString(new ResultBean(ResultCode.SUCCESS, resMap));
        } catch (Exception e) {
            logger.error(e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
        }
    }
}
