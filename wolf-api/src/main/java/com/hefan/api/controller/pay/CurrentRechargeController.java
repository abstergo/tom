package com.hefan.api.controller.pay;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.google.common.collect.Maps;
import com.hefan.api.service.CurrencyRechargeLocalService;
import com.hefan.api.service.UserLocalService;
import com.hefan.api.service.oms.RechargeLocalService;
import com.hefan.user.bean.WebUser;

/**
 * Created by lxw on 2016/10/12.
 */
@Controller
@RequestMapping("/pub")
public class CurrentRechargeController {

	private Logger logger = LoggerFactory.getLogger(CurrentRechargeController.class);

	@Resource
	private CurrencyRechargeLocalService currencyRechargeLocalService;
	@Resource
	private UserLocalService userLocalService;

	@RequestMapping("/recharge")
	@ResponseBody
	public String getRecharge() {
		ResultBean result = new ResultBean();
		try {
            WebUser user=new WebUser();
			Map map = Maps.newHashMap();
//			user=userLocalService.getWebUserByUserId(userId);
//			map.put("user",user );
			map.put("recharge", currencyRechargeLocalService.getCurrencyRecharge(2,null));
			map.put("exchange", currencyRechargeLocalService.getExchange(2,null));
			result.setCode(ResultCode.SUCCESS.get_code());
			result.setData(map);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			result.setCode(ResultCode.UNSUCCESS.get_code());
		}
		return JSON.toJSONString(result);

	}

}
