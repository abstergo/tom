package com.hefan.api.controller.dynamic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.alibaba.fastjson.JSON;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.service.JedisService;
import com.hefan.api.service.ActivityLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;

/**
 * 活动
 * 
 * @author kevin_zhang
 *
 */
@Controller
@RequestMapping("/v1/activity")
public class ActivityController {
	public static Logger logger = LoggerFactory.getLogger(ActivityController.class);

	@Resource
	ActivityLocalService activityLocalService;

	@Resource
    JedisService jedisService;

	private static final String ACTIVITY_GETHOTACTIVITY_KEY = "activity_getHotActivity";
	private static final int ACTIVITY_GETHOTACTIVITY_CACHE_TIME = 5 * 60;

	/**
	 * 获取推荐活动
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RequestMapping(value = "/getHotActivity", method = RequestMethod.GET)
	@ResponseBody
	public String getHotActivity(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean();
		try {
			String infoRedis = jedisService.getStr(ACTIVITY_GETHOTACTIVITY_KEY);
			if (StringUtils.isNotBlank(infoRedis)) {
				List resultMap = JSON.parseObject(infoRedis, ArrayList.class);
				resultBean.setData(resultMap);
			} else {
				List resultMap = activityLocalService.getHotActivity();
				resultBean.setData(resultMap);
				/**
				 * cache time:1 hour
				 */
				jedisService.setexStr(ACTIVITY_GETHOTACTIVITY_KEY,
						JSON.toJSONString(resultMap), ACTIVITY_GETHOTACTIVITY_CACHE_TIME);
			}
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setMsg(ResultCode.SUCCESS.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return JSON.toJSONString(resultBean);
		}
		return JSON.toJSONString(resultBean);
	}

	/**
	 * 获取俱乐部活动
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/getClubActivityList", method = RequestMethod.GET)
	@ResponseBody
	public String getClubActivityList(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean();
		// 接参
		Map paramMap = HttpUtils.initParam(request, Map.class);
		if (null == paramMap || paramMap.isEmpty()) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		int pageNo = MapUtils.getIntValue(paramMap, "pageNo", -1);
		int pageSize = MapUtils.getIntValue(paramMap, "pageSize", -1);
		/**
		 * 俱乐部类型:1:网红 2:明星 3:片场
		 */
		int clubType = MapUtils.getIntValue(paramMap, "clubType", -1);
		String clubUserId = MapUtils.getStrValue(paramMap, "clubUserId", "");
		if (pageNo <= 0 || pageSize <= 0 || clubType == -1 || StringUtils.isBlank(clubUserId)) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		try {
			Page page = new Page();
			page.pageNo = pageNo;
			page.pageSize = pageSize;
			page = activityLocalService.getClubActivityList(page, clubType, clubUserId);

			resultBean.setData(page);
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setMsg(ResultCode.SUCCESS.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return JSON.toJSONString(resultBean);
		}
		return JSON.toJSONString(resultBean);
	}
}