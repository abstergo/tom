package com.hefan.api.controller.live;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.entity.ResultPojo;
import com.cat.common.meta.ImCustomMsgEnum;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.util.DateUtils;
import com.cat.tiger.util.GlobalConstants;
import com.hefan.api.service.UserLocalService;
import com.hefan.api.service.WatchLocalService;
import com.hefan.api.service.live.*;
import com.hefan.api.service.monitor.LiveMonitorLocalService;
import com.hefan.common.ons.service.ONSProducer;
import com.hefan.common.util.DynamicProperties;
import com.hefan.common.util.HttpUtils;
import com.hefan.live.bean.*;
import com.hefan.live.itf.LivingRedisOptService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WatchCacheService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 直播间用户的处理操作
 * Created by nigle on 2016/10/10.
 */
@Controller
@RequestMapping("/v1/live")
public class LiveUserOptController {

    public Logger logger = LoggerFactory.getLogger(LiveUserOptController.class);

    @Resource
    LiveLogLocalService liveLogLocalService;
    @Resource
    UserLocalService userLocalService;
    @Resource
    LiveRoomLocalService liveRoomLocalService;
    @Resource
    RoomOptLocalService roomOptLocalService;
    @Resource
    LiveImOptLocalService liveImOptLocalService;
    @Resource
    LiveMonitorLocalService liveMonitorLocalService;
    @Resource
    LivingLocalService livingLocalService;
    @Reference
    LivingRedisOptService livingRedisOptService;
    @Reference
    WatchCacheService watchCacheService;
    
    /**
     * 直播间禁言用户
     * @param req
     * @return
     */
    @RequestMapping("/dealRoomShutupUser")
    @ResponseBody
    public String dealRoomShutupUser(HttpServletRequest req){
        try{
            LiveUserOptVo vo = HttpUtils.initParam(req, LiveUserOptVo.class);
            //参数异常
            if(null == vo || StringUtils.isBlank(vo.getUserId()) || StringUtils.isBlank(vo.getLiveUuid()) || StringUtils.isBlank(vo.getOptUserId()) || vo.getOptUserId().equals(vo.getUserId())){
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
//            WebUser user = userLocalService.getWebUserByUserId(vo.getUserId());
            WebUser user = userLocalService.findUserInfoFromCache(vo.getUserId());
            if ( null == user ){
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
//            WebUser optUser = userLocalService.getWebUserByUserId(vo.getOptUserId());
            WebUser optUser = userLocalService.findUserInfoFromCache(vo.getOptUserId());
            if ( null == optUser ){
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
            RoomShutup roomShutup = new RoomShutup();
            LiveLog liveLog = liveLogLocalService.getLiveLogByUuid(vo.getLiveUuid());
            //直播不存在
            if (liveLog == null){
                return JSON.toJSONString(new ResultPojo(ResultCode.LiveIsNull));
            }

            //被禁言用户是房主，没有操作权限
            if(vo.getUserId().equals(liveLog.getUserId())){
                return JSON.toJSONString(new ResultPojo(ResultCode.AuthError));
            }

            //禁言机器人直接返回成功
            if(user.getUserType() == GlobalConstants.USER_TYPE_ROBOT){
                liveImOptLocalService.liveShutUpIm(optUser, user, liveLog.getChatRoomId(), vo.getLiveUuid(),liveLog.getUserId());
                roomShutup.setUserId(vo.getUserId());
                roomShutup.setChatRoomId(liveLog.getChatRoomId());
                roomShutup.setLiveUuid(vo.getLiveUuid());
                roomShutup.setOptUserId(vo.getOptUserId());
                //禁言记录存储在redis中
                livingRedisOptService.addShutUpUser(roomShutup, vo.getLiveUuid(), vo.getUserId());
                return JSON.toJSONString(new ResultPojo(ResultCode.SUCCESS));
            }
            
            //重复操作（已被禁言）
            boolean isShutUp = roomOptLocalService.findIsShutupForAnchorByUserId(vo.getUserId(),vo.getLiveUuid());
            if(isShutUp){
                liveImOptLocalService.liveShutUpIm(optUser, user, liveLog.getChatRoomId(), vo.getLiveUuid(),liveLog.getUserId());
                return JSON.toJSONString(new ResultPojo(ResultCode.SUCCESS));
            }
            //被操作者是否管理员
            boolean uIsAdmin = roomOptLocalService.findIsAdminForAnchorByUserId(vo.getUserId(),liveLog.getChatRoomId());
            if (uIsAdmin && !liveLog.getUserId().equals(optUser.getUserId())){
                return JSON.toJSONString(new ResultPojo(ResultCode.AuthError));
            }
            boolean optIsadmin = roomOptLocalService.findIsAdminForAnchorByUserId(vo.getOptUserId(), liveLog.getChatRoomId());
            boolean b = false;
            //验证操作者权限(主播、管理员、内部号)
            if (optIsadmin || liveLog.getUserId().equals(optUser.getUserId())
                    || optUser.getUserType() == GlobalConstants.USER_TYPE_INNER) {
                    b = liveImOptLocalService.liveShutUpIm(optUser, user, liveLog.getChatRoomId(), vo.getLiveUuid(),liveLog.getUserId());
                if(b){
                    roomShutup.setUserId(vo.getUserId());
                    roomShutup.setChatRoomId(liveLog.getChatRoomId());
                    roomShutup.setLiveUuid(vo.getLiveUuid());
                    roomShutup.setOptUserId(vo.getOptUserId());
//                    RoomShutup res = roomOptLocalService.save(roomShutup);
                    //禁言记录存储在redis中
                    livingRedisOptService.addShutUpUser(roomShutup, vo.getLiveUuid(), vo.getUserId());
                    return JSON.toJSONString(new ResultPojo(ResultCode.SUCCESS));
                }
            }else {
                return JSON.toJSONString(new ResultPojo(ResultCode.AuthError));
            }
        }catch (Exception e){
            logger.error("禁言失败",e);
            return JSON.toJSONString(new ResultPojo(ResultCode.UnknownException));
        }
        return JSON.toJSONString(new ResultPojo(ResultCode.UnknownException));
    }


    /**
     * 直播间踢出用户
     */
    @RequestMapping("/dealRoomOuterUser")
    @ResponseBody
    public String dealRoomOuterUser(HttpServletRequest req) {
        try {
            LiveUserOptVo vo = HttpUtils.initParam(req, LiveUserOptVo.class);
            //参数异常
            if (null == vo || StringUtils.isBlank(vo.getUserId()) || StringUtils.isBlank(vo.getLiveUuid())
                    || StringUtils.isBlank(vo.getOptUserId()) || vo.getOptUserId().equals(vo.getUserId())) {
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
//            WebUser user = userLocalService.getWebUserByUserId(vo.getUserId());
//            WebUser optUser = userLocalService.getWebUserByUserId(vo.getOptUserId());
            WebUser user = userLocalService.findUserInfoFromCache(vo.getUserId());
            WebUser optUser = userLocalService.findUserInfoFromCache(vo.getOptUserId());

            if (user == null || optUser == null) {
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
            //内部账号不能被踢出
            if(user.getUserType() == GlobalConstants.USER_TYPE_INNER){
                return JSON.toJSONString(new ResultPojo(ResultCode.RESULT_CODE_4085));
            }

            LiveLog liveLog = liveLogLocalService.getLiveLogByUuid(vo.getLiveUuid());
            //直播不存在
            if (liveLog == null) {
                return JSON.toJSONString(new ResultPojo(ResultCode.LiveIsNull));
            }
            //被踢出用户是房主，没有操作权限
            if (vo.getUserId().equals(liveLog.getUserId())) {
                return JSON.toJSONString(new ResultPojo(ResultCode.AuthError));
            }
            //执行踢出(房主或内部或管理员)
            if (checkOutAuth(optUser,liveLog)) {
                //执行用户离开直播间操作，清除redis
                livingLocalService.exitLiveRoomOperate(vo.getUserId(), liveLog.getChatRoomId(), vo.getLiveUuid(), liveLog.getUserId());
                try {
                    RoomOuter roomOuter = new RoomOuter();
                    roomOuter.setOptUserId(vo.getOptUserId());
                    roomOuter.setUserId(vo.getUserId());
                    roomOuter.setChatRoomId(liveLog.getChatRoomId());
                    roomOuter.setLiveUuid(vo.getLiveUuid());
                    livingRedisOptService.setLivingForbiddenEnter(liveLog.getLiveUuid(), roomOuter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                liveImOptLocalService.liveOutIm(optUser, user, liveLog.getChatRoomId(), liveLog.getLiveUuid());
                //异步执行，离开直播间触发减少机器人队列消息发送
                if(user.getUserType() == 7){
                    livingLocalService.outRobot(liveLog.getUserId(),vo.getUserId());
                }
                return JSON.toJSONString(new ResultPojo(ResultCode.SUCCESS));
            } else {
                return JSON.toJSONString(new ResultPojo(ResultCode.AuthError));
            }
        } catch (Exception e) {
            logger.error("踢出失败", e);
            return JSON.toJSONString(new ResultPojo(ResultCode.UnknownException));
        }
    }

    /**
     * 检查是否有踢出权限
     * @param optUser
     * @param liveLog
     * @return
     */
    private boolean checkOutAuth(WebUser optUser, LiveLog liveLog) {
        //是房主自己
        if (optUser.getUserId().equals(liveLog.getUserId())){
            logger.info("踢出者是主播本人:{}",optUser.getUserId());
            return true;
        }
        //是内部账号
        if (optUser.getUserType() == GlobalConstants.USER_TYPE_INNER){
            logger.info("踢出者是内部账号:{}",optUser.getUserId());
            return true;
        }
        //是管理员
        if (roomOptLocalService.findIsAdminForAnchorByUserId(optUser.getUserId(),liveLog.getChatRoomId())){
            logger.info("踢出者是管理员:{}",optUser.getUserId());
            return true;
        }
        return false;
    }

    /**
     * 获取直播间管理员
     */
    @RequestMapping("/findRoomAdminList")
    @ResponseBody
    public String findRoomAdminList(HttpServletRequest req){
        try {
            LiveVo vo = HttpUtils.initParam(req, LiveVo.class);
            int chatRoomId = vo.getChatRoomId();
            //参数异常
            if (null == vo || chatRoomId <= 0) {
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
            List<MemberRole> listMem = roomOptLocalService.findRoomAdminList(chatRoomId);
            //无管理员
            if (null == listMem) {
                return JSON.toJSONString(new ResultPojo(ResultCode.SUCCESS));
            } else {//填充用户信息
                List<LiveReturnVo> listReturn = roomOptLocalService.findAdminInfo(listMem);
                return JSON.toJSONString(new ResultPojo(ResultCode.SUCCESS, listReturn));
            }
        }catch (Exception e){
            logger.error("获取直播间管理员",e);
            return JSON.toJSONString(new ResultPojo(ResultCode.UnknownException));
        }
    }

    /**
     * 添加直播间管理员
     */
    @RequestMapping("/addRoomAdmin")
    @ResponseBody
    public String addRoomAdmin(HttpServletRequest req) {
        try {
            LiveUserOptVo vo = HttpUtils.initParam(req, LiveUserOptVo.class);
            //参数异常
            if(null == vo || StringUtils.isBlank(vo.getOptUserId())
                    || StringUtils.isBlank(vo.getUserId())
                    || vo.getChatRoomId()<=0
                    || StringUtils.isBlank(vo.getLiveUuid())){
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
            //验证直播间管理员个数   大于10个 则报错
            long count = roomOptLocalService.getAdminCount(vo.getChatRoomId());
            if(count >=10){
                return JSON.toJSONString(new ResultPojo(ResultCode.AdminCount));
            }
            //重复操作
            if(roomOptLocalService.findIsAdminForAnchorByUserId(vo.getUserId(),vo.getChatRoomId())){
                return JSON.toJSONString(new ResultPojo(ResultCode.RepeatOper));
            }

            WebUser optUser = userLocalService.getWebUserByUserId(vo.getOptUserId());
            LiveRoom liveRoom = liveRoomLocalService.getLiveRoomByUserId(vo.getOptUserId());
            WebUser user = userLocalService.getWebUserByUserId(vo.getUserId());
            //验证用户
            if (liveRoom == null || optUser == null || user == null || !optUser.getUserId().equals(liveRoom.getUserId())) {
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException ));
            }

            //判断操作者是否和该chatRoomId匹配
            if(liveRoom.getChatRoomId() == vo.getChatRoomId()){
                    MemberRole memberRole = new MemberRole();
                    memberRole.setChatRoomId(vo.getChatRoomId());
                    memberRole.setCreateUser("api");
                    memberRole.setOperatorAccid(vo.getOptUserId());
                    memberRole.setTargetAccid(vo.getUserId());
                    if(roomOptLocalService.save(memberRole) > 0){
                        //发IM
                        try {
                            liveImOptLocalService.liveOptAdmin(optUser,user,vo.getChatRoomId(),liveRoom.getLiveUuid(),"SET");
                        }catch (Exception e){
                            logger.error("设置管理员发送IM失败",e);
                        }
                        return JSON.toJSONString(new ResultPojo(ResultCode.SUCCESS));
                    }else {
                        return JSON.toJSONString(new ResultPojo(ResultCode.UNSUCCESS));
                    }
            }else {
                //无操作权限
                return JSON.toJSONString(new ResultPojo(ResultCode.AuthError));
            }
        }catch (Exception e){
            logger.error("设置管理员失败",e);
            return JSON.toJSONString(new ResultPojo(ResultCode.UnknownException));
        }
    }

    /**
     * 取消直播间管理员
     */
    @RequestMapping("/delRoomAdmin")
    @ResponseBody
    public String delRoomAdmin(HttpServletRequest req) {
        try {
            LiveUserOptVo vo = HttpUtils.initParam(req, LiveUserOptVo.class);
            //参数异常
            if(null == vo || StringUtils.isBlank(vo.getOptUserId())
                    || StringUtils.isBlank(vo.getUserId())
                    || vo.getChatRoomId() <= 0 ){
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }

            WebUser optUser = userLocalService.getWebUserByUserId(vo.getOptUserId());
            LiveRoom liveRoom = liveRoomLocalService.getLiveRoomByUserId(vo.getOptUserId());
            WebUser user = userLocalService.getWebUserByUserId(vo.getUserId());
            //验证用户
            if (liveRoom == null || optUser == null || user == null || !optUser.getUserId().equals(liveRoom.getUserId())) {
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
                //判断操作者是否主播以及是否和该chatRoomId匹配
            if(liveRoom.getChatRoomId() == (vo.getChatRoomId())){
               int i = roomOptLocalService.del(vo.getChatRoomId(),vo.getUserId());
                if(i>0){
                    //发IM
                    try {
                        liveImOptLocalService.liveOptAdmin(optUser,user,vo.getChatRoomId(),liveRoom.getLiveUuid(),"UNSET");
                    } catch (Exception e) {
                        logger.error("撤销管理员发送IM失败",e);
                    }
                    return JSON.toJSONString(new ResultPojo(ResultCode.SUCCESS));
                }else {
                    return JSON.toJSONString(new ResultPojo(ResultCode.UNSUCCESS));
                }
            }else {
                //无操作权限
                return JSON.toJSONString(new ResultPojo(ResultCode.AuthError));
            }
        }catch (Exception e){
            logger.error("取消管理员失败",e);
            return JSON.toJSONString(new ResultPojo(ResultCode.UnknownException));
        }
    }

    /**
     * 直播间点头像查看信息
     * optUser为查看者
     * user为被查看者
     * @param req
     * @return
     */
    @RequestMapping("/findLiveUserInfoByUserId")
    @ResponseBody
    public String findLiveUserInfoByUserId(HttpServletRequest req) {
        try {
            LiveUserOptVo vo = HttpUtils.initParam(req, LiveUserOptVo.class);
			if (null == vo || StringUtils.isBlank(vo.getUserId()) || StringUtils.isBlank(vo.getOptUserId())
					|| StringUtils.isBlank(vo.getLiveUuid()) || vo.getChatRoomId() <= 0) {
				return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
			}
            LiveRoom liveRoom = liveRoomLocalService.getLiveRoomByChatRoomId(vo.getChatRoomId());
            if (liveRoom == null || liveRoom.getStatus() == GlobalConstants.AUTHOR_LIVEEND) {
                return JSON.toJSONString(new ResultPojo(ResultCode.LiveIsEnd));
            }
            LiveLog liveLog = liveLogLocalService.getLiveLogByUuid(vo.getLiveUuid());
            if(liveLog == null){
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
            if(liveLog.getChatRoomId() != vo.getChatRoomId()){
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
//            WebUser optUser = userLocalService.getWebUserByUserId(vo.getOptUserId());
            WebUser optUser = userLocalService.findUserInfoFromCache(vo.getOptUserId());
//            WebUser user = userLocalService.getWebUserByUserId(vo.getUserId());
            WebUser user = userLocalService.findUserInfoFromCache(vo.getUserId());
            if (optUser == null || user == null) {
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
            LiveReturnVo returnVo = new LiveReturnVo();
            //查看者是否管理员
            returnVo.setSelfIsAdmin(roomOptLocalService.findIsAdminForAnchorByUserId(optUser.getUserId(),vo.getChatRoomId())?1:0);
            //查看者：直播间主播本人不算管理员
            if(liveLog.getUserId().equals(vo.getOptUserId())){ returnVo.setSelfIsAdmin(0); }
            //被查看者是否管理员
            returnVo.setIsAdmin(roomOptLocalService.findIsAdminForAnchorByUserId(user.getUserId(),vo.getChatRoomId())?1:0);
            //是否被禁言
            returnVo.setIsShutup(roomOptLocalService.findIsShutupForAnchorByUserId(user.getUserId(),vo.getLiveUuid())?1:0);
            WebUser returnUser = roomOptLocalService.findOtherUserInfo(vo.getUserId(),vo.getOptUserId());
            if (returnUser == null) {
                return JSON.toJSONString(new ResultPojo(ResultCode.UserNotFound));
            }
            returnVo.setBirthday(DateUtils.date2SimpleStr(returnUser.getBirthday()));
            returnVo.setHeadImg(StringUtils.isBlank(returnUser.getHeadImg())?DynamicProperties.getString("default.head.img"):returnUser.getHeadImg());
            returnVo.setLevel(String.valueOf(returnUser.getUserLevel()));
            returnVo.setWatcherCount(returnUser.getWatcherCount());
            returnVo.setNickName(returnUser.getNickName());
            returnVo.setSex(returnUser.getSex());
            returnVo.setpersonSign(returnUser.getPersonSign());
            returnVo.setFansCount(returnUser.getFansCount());
            returnVo.setIsWatched(returnUser.getIsWatched());
            returnVo.setUserId(vo.getUserId());
            returnVo.setAuthInfo(returnUser.getAuthInfo());
            returnVo.setGpsLocation(returnUser.getGpsLocation());
            returnVo.setPayCount(returnUser.getPayCount());
            returnVo.setUserType(returnUser.getUserType());
            returnVo.setTicketFact(returnUser.getHefanTotal());
            int relation =  watchCacheService.getWatchRelationByUserId(optUser.getUserId(),user.getUserId());
            returnVo.setRelation(relation);
            return JSON.toJSONString(new ResultBean(ResultCode.SUCCESS, returnVo));
        }catch (Exception e){
            logger.error("查看直播间用户信息失败",e);
            return JSON.toJSONString(new ResultPojo(ResultCode.UnknownException));
        }
    }
    /**
     * 主播分享动态
     * @return
     */
    @RequestMapping("/shareVideo")
    @ResponseBody
    public String shareVideo(HttpServletRequest req) {
        try {
            LiveUserOptVo vo = HttpUtils.initParam(req, LiveUserOptVo.class);
            if (null == vo || StringUtils.isBlank(vo.getLiveUuid()) || StringUtils.isBlank(vo.getUserId())){
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
            //此次直播（liveuuid）已经被分享过
            if (roomOptLocalService.isShareToDynamic(vo.getUserId(), vo.getLiveUuid())) {
                return JSON.toJSONString(new ResultPojo(ResultCode.RepeatOper));
            }
            LiveLog liveLog = liveLogLocalService.getLiveLogByUuid(vo.getLiveUuid());
            WebUser webUser = userLocalService.getWebUserByUserId(vo.getUserId());
            LiveRoom liveRoom = liveRoomLocalService.getLiveRoomByUserId(vo.getUserId());
            if(null == liveLog || null == webUser || null == liveRoom){
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
            if(liveRoom.getStatus() == GlobalConstants.AUTHOR_LIVING){
                return JSON.toJSONString(new ResultPojo(ResultCode.LiveIsLiveing));
            }
            if (!liveLog.getUserId().equals(webUser.getUserId())) {
                return JSON.toJSONString(new ResultPojo(ResultCode.AuthError));
            }
            LiveDynamicVo lVo = new LiveDynamicVo();
            String pre = DynamicProperties.getString("replay.domain.pre");
            String suf = DynamicProperties.getString("replay.domain.suf");
            lVo.setTranscode(true);
            lVo.setUserId(vo.getUserId());
            lVo.setFromType("0");//来源
            lVo.setPathTrans(pre+liveLog.getLiveUuid()+suf);//拉流地址)
            lVo.setMessageInfo(liveLog.getLiveName());
            lVo.setBackImg(liveLog.getLiveImg().substring(0, 10) + "1" + liveLog.getLiveImg().substring(10));
            lVo.setLength(com.hefan.common.util.DateUtils.getHms(liveLog.getLiveLength() * 1000));
            lVo.setIsSync(1);//是否同步到视频
            lVo.setMessageType("3");//表示视频
            lVo.setTimes(liveLog.getWatchNum());//观看次数
            int i = roomOptLocalService.anchorShareLive(lVo);
            if(i == 1){
                return JSON.toJSONString(new ResultPojo(ResultCode.SUCCESS));
            }else {
                return JSON.toJSONString(new ResultPojo(ResultCode.RepeatOper));
            }
        }catch (Exception e){
            logger.error("分享动态失败",e);
            return JSON.toJSONString(new ResultPojo(ResultCode.UnknownException));
        }
    }

    /**
     * 内部号警告
     * @return
     */
    @RequestMapping("/liveWarning")
    @ResponseBody
    public String liveWarning(HttpServletRequest req) {
        try {
            // 接参
            WarnLog wlParam = HttpUtils.initParam(req, WarnLog.class);
            if (null == wlParam) {
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
            if (StringUtils.isBlank(wlParam.getUserId()) || wlParam.getChatRoomId() <=0 ||
                    StringUtils.isBlank(wlParam.getLiveUuid()) || StringUtils.isBlank(wlParam.getOptUserId())){
                return JSON.toJSONString(new ResultPojo(ResultCode.ParamException));
            }
            WebUser user = userLocalService.getWebUserByUserId(wlParam.getOptUserId());
            if ( null == user ) {
                return JSON.toJSONString(new ResultPojo(ResultCode.UserNotFound));
            }
            if ( user.getUserType() != GlobalConstants.USER_TYPE_INNER ) {
                return JSON.toJSONString(new ResultPojo(ResultCode.AuthError));
            }
            int us = liveMonitorLocalService.warningLiving(wlParam, ImCustomMsgEnum.AnchorWaringMsg.getMsg());
            if (us <= 0) {
                return JSON.toJSONString(new ResultPojo(ResultCode.UNSUCCESS));
            }
            return JSON.toJSONString(new ResultPojo(ResultCode.SUCCESS));
        }catch (Exception e){
            logger.error("内部号警告失败：" + e.getMessage());
            return JSON.toJSONString(new ResultPojo(ResultCode.UnknownException));
        }
    }
}
