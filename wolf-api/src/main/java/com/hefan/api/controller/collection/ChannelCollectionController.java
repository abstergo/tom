package com.hefan.api.controller.collection;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.api.service.collection.ChannelCollectionLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;
import com.hefan.monitor.bean.AppChannelStatistics;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * IOS推广渠道信息收集
 * Created by kevin_zhang on 24/03/2017.
 */
@Controller
@RequestMapping("/v1/collection")
public class ChannelCollectionController {
    public static Logger logger = LoggerFactory.getLogger(ChannelCollectionController.class);

    @Resource
    ChannelCollectionLocalService channelCollectionLocalService;

    /**
     * 第三方IOS设备的IDFA收集
     *
     * @param request
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @RequestMapping(value = "/click", method = RequestMethod.GET)
    @ResponseBody
    public String idfaCollection(HttpServletRequest request) {
        Map<String, Object> result = new HashedMap();
        result.put("status", -1);

        //接参
        String source; //渠道来源
        int appid;//iTunesId获约定ID
        String idfa; //用户唯一标识
        String model; //设备型号：iphone5
        String version; //设备系统版本：8.1.3
        long reportTime;//应用安装时间戳，10位数字，单位(ms)：1426478023
        String callback; //激活时候的回调地址
        try {
            Map<String, String[]> param = request.getParameterMap();
            source = (param.get("source") != null && param.get("source").length > 0) ? param.get("source")[0] : "";
            appid = Integer.parseInt(((param.get("appid") != null && param.get("appid").length > 0) ? param.get("appid")[0] : "0"));
            idfa = (param.get("idfa") != null && param.get("idfa").length > 0) ? param.get("idfa")[0] : "";
            model = (param.get("model") != null && param.get("model").length > 0) ? param.get("model")[0] : "";
            version = (param.get("version") != null && param.get("version").length > 0) ? param.get("version")[0] : "";
            reportTime = Long.parseLong(((param.get("reportTime") != null && param.get("reportTime").length > 0) ? param.get("reportTime")[0] : "0"));
            callback = (param.get("callback") != null && param.get("callback").length > 0) ? param.get("callback")[0] : "";

            if (appid <= 0) {
                result.put("result", "appid 不合法" + appid);
                return JSON.toJSONString(result);
            }
            if (StringUtils.isBlank(source)) {
                result.put("result", "source 不能为空");
                return JSON.toJSONString(result);
            }
            if (StringUtils.isBlank(idfa)) {
                result.put("result", "idfa 不能为空");
                return JSON.toJSONString(result);
            }
            if (StringUtils.isBlank(model)) {
                result.put("result", "model 不能为空");
                return JSON.toJSONString(result);
            }
            if (StringUtils.isBlank(version)) {
                result.put("result", "version 不能为空");
                return JSON.toJSONString(result);
            }
            if (StringUtils.isBlank(callback)) {
                result.put("result", "callback 不能为空");
                return JSON.toJSONString(result);
            }
            if (reportTime <= 0) {
                result.put("result", "reportTime 不合法" + reportTime);
                return JSON.toJSONString(result);
            }

            AppChannelStatistics params = new AppChannelStatistics();
            params.setAppid(appid);
            params.setIdfa(idfa);
            params.setModel(model);
            params.setVersion(version);
            params.setCallback(callback);
            params.setIsActivity(0);
            params.setAppType(0);
            params.setSourceType(source);
            result.put("status", 0);
            result.put("result", "提交成功");
            channelCollectionLocalService.idfaCollection(params);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return JSON.toJSONString(result);
    }

    /**
     * IOS设备的IDFA收集
     *
     * @param request
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @RequestMapping(value = "/idfaSubmit", method = RequestMethod.GET)
    @ResponseBody
    public String idfaSubmit(HttpServletRequest request) {
        ResultBean resultBean = new ResultBean();
        // 接参
        Map paramMap = HttpUtils.initParam(request, Map.class);
        if (null == paramMap || paramMap.isEmpty()) {
            resultBean.setCode(ResultCode.ParamException.get_code());
            resultBean.setMsg(ResultCode.ParamException.getMsg());
            return JSON.toJSONString(resultBean);
        }
        String idfaStr = MapUtils.getStrValue(paramMap, "idfaStr", "");
        if (StringUtils.isBlank(idfaStr)) {
            resultBean.setCode(ResultCode.ParamException.get_code());
            resultBean.setMsg(ResultCode.ParamException.getMsg());
            return JSON.toJSONString(resultBean);
        }

        try {
            AppChannelStatistics params = new AppChannelStatistics();
            params.setIdfa(idfaStr);
            params.setIsActivity(0);
            params.setAppType(0);
            params.setSourceType("");
            channelCollectionLocalService.idfaSubmit(params);

            resultBean.setCode(ResultCode.SUCCESS.get_code());
            resultBean.setMsg(ResultCode.SUCCESS.getMsg());
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setCode(ResultCode.UNSUCCESS.get_code());
            resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
            return JSON.toJSONString(resultBean);
        }
        return JSON.toJSONString(resultBean);
    }
}
