package com.hefan.api.controller.notify;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.notify.bean.SysMsgVo;
import com.hefan.user.itf.FansCacheService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.api.service.MessageLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;

/**
 * 消息
 *
 * @author kevin_zhang
 *
 */
@Controller
@RequestMapping("/v1/message")
public class MessageController {
	public static Logger logger = LoggerFactory.getLogger(MessageController.class);

	@Resource
	MessageLocalService messageLocalService;

	@Reference
	FansCacheService fansCacheService;

	/**
	 * 获取未读消息数
	 *
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/getMsgCount", method = RequestMethod.GET)
	@ResponseBody
	public String getMsgCount(HttpServletRequest request) {
        ResultBean resultBean = new ResultBean();
        // 接参
        Map paramMap = HttpUtils.initParam(request, Map.class);
        if (null == paramMap || paramMap.isEmpty()) {
            resultBean.setCode(ResultCode.ParamException.get_code());
            resultBean.setMsg(ResultCode.ParamException.getMsg());
            return JSON.toJSONString(resultBean);
        }
        String userId = MapUtils.getStrValue(paramMap, "userId", "");
        if (StringUtils.isBlank(userId)) {
            resultBean.setCode(ResultCode.ParamException.get_code());
            resultBean.setMsg(ResultCode.ParamException.getMsg());
            return JSON.toJSONString(resultBean);
        }
        try {
            SysMsgVo vo = messageLocalService.getMsgCount(userId);
            vo.setNewFansCount(fansCacheService.newFansNum(userId));
            vo.setTotalMsgCount();
			vo.setTotalSysMsgCount();

            resultBean.setData(vo);
            resultBean.setCode(ResultCode.SUCCESS.get_code());
            resultBean.setMsg(ResultCode.SUCCESS.getMsg());
        } catch (Exception e) {
            e.printStackTrace();
            resultBean.setCode(ResultCode.UNSUCCESS.get_code());
            resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
            return JSON.toJSONString(resultBean);
        }
        return JSON.toJSONString(resultBean);
    }

	/**
	 * 获取消息主页信息
	 *
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/getMsgPageInfo", method = RequestMethod.GET)
	@ResponseBody
	public String getMsgPageInfo(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean();
		// 接参
		Map paramMap = HttpUtils.initParam(request, Map.class);
		if (null == paramMap || paramMap.isEmpty()) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		String userId = MapUtils.getStrValue(paramMap, "userId", "");
		/**
		 * 0:普通用户 1：网红 2：明星／片场
		 */
		int userType = MapUtils.getIntValue(paramMap, "userType", -1);
		//临时处理
		if(userType == 3)
			userType = 2;
		if(userType > 3)
			userType = 0;
		if (StringUtils.isBlank(userId) || userType < 0) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}

		try {
			resultBean.setData(messageLocalService.getMsgPageInfo(userId, userType));
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setMsg(ResultCode.SUCCESS.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return JSON.toJSONString(resultBean);
		}
		return JSON.toJSONString(resultBean);
	}

	/**
	 * 清空所有用户消息缓存（慎重操作）
	 *
	 * @param request
	 * @return
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/cleanAllMsgCacheo", method = RequestMethod.GET)
	@ResponseBody
	public String cleanAllMsgCache(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean();
		try {
			messageLocalService.cleanAllMsgCache();
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setMsg(ResultCode.SUCCESS.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return JSON.toJSONString(resultBean);
		}
		return JSON.toJSONString(resultBean);
	}
}
