package com.hefan.api.controller.H5;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.service.JedisService;
import com.cat.tiger.util.GlobalConstants;
import com.google.common.collect.Maps;
import com.hefan.api.service.*;
import com.hefan.api.service.comment.CommentsLocalService;
import com.hefan.api.service.live.LiveLogLocalService;
import com.hefan.api.service.live.LiveRoomLocalService;
import com.hefan.club.dynamic.bean.Message;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;
import com.hefan.live.bean.LivingRoomInfoVo;
import com.hefan.live.itf.LivingRedisOptService;
import com.hefan.live.itf.RoomEnterExitOptService;
import com.hefan.user.bean.WebUser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@RequestMapping("/v1/h5")
public class H5Controller {
	private Logger logger = LoggerFactory.getLogger(H5Controller.class);

	@Reference
	private RoomEnterExitOptService roomEnterExitOptService;

	@Resource
	WatchLocalService watchLocalService;

	@Resource
	UserLocalService userLocalService;

	@Resource
	ActivityLocalService activityLocalService;

	@Resource
	MessageLocalService messageLocalService;

	@Resource
	LiveRoomLocalService liveRoomLocalService;
	
	@Resource
	LiveLogLocalService liveLogLocalService;

	@Resource
	DynamicLocalService dynamicLocalService;

	@Resource
	CommentsLocalService commentsLocalService;

	@Resource
	JedisService jedisService;

	@Resource
	TripInfoLocalService tripInfoLocalService;

    @Reference
    LivingRedisOptService livingRedisOptService;

	/**
	 * 关注
	 *
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/fork")
	@ResponseBody
	public String fork(HttpServletRequest req) {
		Map<String, Object> map = HttpUtils.initParam(req, Map.class);
		String userId = MapUtils.getStrValue(map, "userId", "");
		String mainUserId = MapUtils.getStrValue(map, "mainUserId", "");
		int liveRemind = MapUtils.getIntValue(map, "liveRemind", 1); //0：不接受主播的开播提醒 1：接受主播的开播提醒

		//参数验证
		ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(mainUserId)) {
			resultBean.setResult(ResultCode.ParamException);
			return JSON.toJSONString(resultBean);
		}

		try {
			resultBean = watchLocalService.fork(userId, mainUserId, liveRemind);
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.setResult(ResultCode.UNSUCCESS);
		}
		return JSON.toJSONString(resultBean);
	}

	/**
	 * 取消关注
	 *
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/unfork")
	@ResponseBody
	public String unfork(HttpServletRequest req) {
		Map<String, Object> map = HttpUtils.initParam(req, Map.class);
		String userId = MapUtils.getStrValue(map, "userId", "");
		String mainUserId = MapUtils.getStrValue(map, "mainUserId", "");

		//参数验证
		ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
		if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(mainUserId)) {
			resultBean.setResult(ResultCode.ParamException);
			return JSON.toJSONString(resultBean);
		}

		try {
			resultBean = watchLocalService.unfork(userId, mainUserId);
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.setResult(ResultCode.UNSUCCESS);
		}
		return JSON.toJSONString(resultBean);
	}

	/**
	 * 主播排行榜
	 * 
	 * @param userId
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/getAnchorRankingList")
	@ResponseBody
	public String getAnchorRankingList(String userId, Integer pageNo, Integer pageSize, HttpServletRequest req) {
		Map<String, Object> map = HttpUtils.initParam(req, Map.class);
		userId = MapUtils.getStrValue(map, "userId", "");
		//0积分1 实力排行榜
		int type = MapUtils.getIntValue(map, "type", 1);
		pageNo = MapUtils.getIntValue(map, "pageNo", 1);
		pageSize = MapUtils.getIntValue(map, "pageSize", 10);
		ResultBean resultBean = new ResultBean();
		if (StringUtils.isEmpty(userId)) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
		} else {
			try {
				Page page = new Page();
				page.setPageNo(pageNo);
				page.setOrderBy("ticketHistory");
				page.setOrder(Page.DESC);
				page.setPageSize(pageSize);
				page = userLocalService.getAnchorRankingList(page, userId,type);
				resultBean.setData(page);
				resultBean.setCode(ResultCode.SUCCESS.get_code());
				resultBean.setMsg(ResultCode.SUCCESS.getMsg());
			} catch (Exception e) {
				resultBean.setCode(ResultCode.UNSUCCESS.get_code());
				resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			}
		}

		return JSON.toJSONString(resultBean);
	}

	/**
	 *  主播排行榜
	 *  [需求修改为榜单改为总榜和月榜  两个榜单的排名依据按现在的实力榜逻辑计算]
	 * @param userId
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @param type 0总榜 2月榜
     * @return
     */
	@RequestMapping(value = "/getAllAnchorRankingList2")
	@ResponseBody
	public String getAnchorRankingList2(String userId, Integer pageNo, Integer pageSize, HttpServletRequest req) {
		Map<String, Object> map = HttpUtils.initParam(req, Map.class);
		userId = MapUtils.getStrValue(map, "userId", "");
		int type = MapUtils.getIntValue(map, "type", 0);
		pageNo = MapUtils.getIntValue(map, "pageNo", 1);
		pageSize = MapUtils.getIntValue(map, "pageSize", 10);
		/*if (StringUtils.isEmpty(userId)) {
			return  JSON.toJSONString( new ResultBean(ResultCode.ParamException.get_code(),ResultCode.ParamException.getMsg()));
		}*/
		try {
			Page page = new Page(pageNo,pageSize);
			page = userLocalService.getAnchorRankingList2(page,type);
			ResultBean resultBean = new ResultBean();
			resultBean.setData(page);
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setMsg(ResultCode.SUCCESS.getMsg());
			return  JSON.toJSONString(resultBean);
		}catch (Exception e) {
			return  JSON.toJSONString( new ResultBean(ResultCode.UnknownException.get_code(),e.getMessage()));
		}

	}


		/**
         * 获取更多活动
         *
         * @param request
         * @return
         */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/getMoreActivityList", method = RequestMethod.GET)
	@ResponseBody
	public String getMoreActivityList(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean();
		// 接参
		Map paramMap = HttpUtils.initParam(request, Map.class);
		if (null == paramMap || paramMap.isEmpty()) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		int pageNo = MapUtils.getIntValue(paramMap, "pageNo", -1);
		int pageSize = MapUtils.getIntValue(paramMap, "pageSize", -1);
		if (pageNo <= 0 || pageSize <= 0) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		try {
			Page page = new Page();
			page.pageNo = pageNo;
			page.pageSize = pageSize;
			page = activityLocalService.getMoreActivityList(page);

			resultBean.setData(page);
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setMsg(ResultCode.SUCCESS.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return JSON.toJSONString(resultBean);
		}
		return JSON.toJSONString(resultBean);
	}

	/**
	 * 获取系统消息列表
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/getSysMsgList", method = RequestMethod.GET)
	@ResponseBody
	public String getSysMsgList(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean();
		// 接参
		Map paramMap = HttpUtils.initParam(request, Map.class);
		if (null == paramMap || paramMap.isEmpty()) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		int pageNo = MapUtils.getIntValue(paramMap, "pageNo", -1);
		int pageSize = MapUtils.getIntValue(paramMap, "pageSize", -1);
		String userId = MapUtils.getStrValue(paramMap, "userId", "");
		/**
		 * 0:普通用户 1：网红 2：明星／片场
		 */
		int userType = MapUtils.getIntValue(paramMap, "userType", -1);
		//临时处理 -Start
		if(userType == 3){
			userType = 2;
		}
		if(userType > 3){
			userType = 0;
		}
		//临时处理 -End

		if (pageNo <= 0 || pageSize <= 0 || StringUtils.isBlank(userId) || userType < 0) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		try {
			Page page = new Page();
			page.pageNo = pageNo;
			page.pageSize = pageSize;
			page = messageLocalService.getSysMsgList(page, userId, userType);

			resultBean.setData(page);
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setMsg(ResultCode.SUCCESS.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return JSON.toJSONString(resultBean);
		}
		return JSON.toJSONString(resultBean);
	}

	/**
	 * 获取活动消息列表
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/getActivityMsgList", method = RequestMethod.GET)
	@ResponseBody
	public String getActivityMsgList(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean();
		// 接参
		Map paramMap = HttpUtils.initParam(request, Map.class);
		if (null == paramMap || paramMap.isEmpty()) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		int pageNo = MapUtils.getIntValue(paramMap, "pageNo", -1);
		int pageSize = MapUtils.getIntValue(paramMap, "pageSize", -1);
		String userId = MapUtils.getStrValue(paramMap, "userId", "");
		/**
		 * 0:普通用户 1：网红 2：明星／片场
		 */
		int userType = MapUtils.getIntValue(paramMap, "userType", -1);
		//临时处理 -Start
		if(userType == 3){
			userType = 2;
		}
		if(userType > 3){
			userType = 0;
		}
		//临时处理 -End
		if (pageNo <= 0 || pageSize <= 0 || StringUtils.isBlank(userId) || userType < 0) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		try {
			Page page = new Page();
			page.pageNo = pageNo;
			page.pageSize = pageSize;
			page = messageLocalService.getActivityMsgList(page, userId, userType);

			resultBean.setData(page);
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setMsg(ResultCode.SUCCESS.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return JSON.toJSONString(resultBean);
		}
		return JSON.toJSONString(resultBean);
	}

	/**
	 * 获取消息详情
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/getMsgDetail", method = RequestMethod.GET)
	@ResponseBody
	public String getMsgDetail(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean();
		// 接参
		Map paramMap = HttpUtils.initParam(request, Map.class);
		if (null == paramMap || paramMap.isEmpty()) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		String msgId = MapUtils.getStrValue(paramMap, "msgId", "");
		if (StringUtils.isBlank(msgId)) {
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
			return JSON.toJSONString(resultBean);
		}
		try {
			String infoRedis = jedisService.getStr(GlobalConstants.MESSAGE_GETMSGDETAIL_KEY + msgId);
			if (StringUtils.isNotBlank(infoRedis)) {
				Map resultMap = JSON.parseObject(infoRedis, Map.class);
				resultBean.setData(resultMap);
			} else {
				Map resultMap = messageLocalService.getMsgDetail(msgId);
				resultBean.setData(resultMap);
				/**
				 * cache time:1 hour
				 */
				jedisService.setexStr(GlobalConstants.MESSAGE_GETMSGDETAIL_KEY + msgId, JSON.toJSONString(resultMap),
						GlobalConstants.MESSAGE_GETMSGDETAIL_CACHE_TIME);
			}
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setMsg(ResultCode.SUCCESS.getMsg());
		} catch (Exception e) {
			e.printStackTrace();
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return JSON.toJSONString(resultBean);
		}
		return JSON.toJSONString(resultBean);
	}

	/**
	 * 分享直播间
	 *
	 * @return
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/shareLive", method = RequestMethod.GET)
	@ResponseBody
	public String shareLive(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean();
		Map map = Maps.newHashMap();
		Map paramMap = HttpUtils.initParam(request, Map.class);
		String roomId = MapUtils.getStrValue(paramMap, "roomId", "");
		try {
			map.put("live", liveRoomLocalService.findLiveRoomById(roomId));
			map.put("recommand", liveRoomLocalService.recommendLive());
			map.put("shareWordsForBefore", liveRoomLocalService.getshareWordsForBefore());
			map.put("shareWordsForAfter", liveRoomLocalService.getshareWordsForAfter());
			resultBean.setData(map);
			resultBean.setCode(ResultCode.SUCCESS.get_code());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return JSON.toJSONString(resultBean);

	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	@RequestMapping(value = "/checkLiveStatus", method = RequestMethod.GET)
	@ResponseBody
	public String checkLiveStatus(HttpServletRequest request) {
		logger.info("检查直播状态");
		ResultBean resultBean = new ResultBean();
		Map map = Maps.newHashMap();
		Map paramMap = HttpUtils.initParam(request, Map.class);
		String userId = MapUtils.getStrValue(paramMap, "userId", "");
		try {
			map.put("status", liveRoomLocalService.isExistsLivingInfo_Hash(userId));
			map.put("peopleCount", liveRoomLocalService.peopleCount(userId));
			resultBean.setData(map);
			resultBean.setCode(ResultCode.SUCCESS.get_code());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return JSON.toJSONString(resultBean);

	}

	/**
	 * 分享动态
	 * 
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@RequestMapping(value = "/HFDynamic", method = RequestMethod.GET)
	@ResponseBody
	public String HFDynamic(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean();
		Page page = new Page();
		Map map = Maps.newHashMap();

		Map paramMap = HttpUtils.initParam(request, Map.class);
		String messageId = MapUtils.getStrValue(paramMap, "messageId", "");
		String userId = MapUtils.getStrValue(paramMap, "userId", "");
		try {
			Message message=dynamicLocalService.getMessageById(messageId,userId);
			if(message ==null){
				message =new Message();
				message.setIsDel(1);
			}
			map.put("dynamic", message);
			map.put("comment", commentsLocalService.getMessageComments(Integer.valueOf(messageId), page));
			map.put("recommand", liveRoomLocalService.recommendLive());
			map.put("user", liveRoomLocalService.recommendLive());
			resultBean.setData(map);
			resultBean.setCode(ResultCode.SUCCESS.get_code());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
		}
		return JSON.toJSONString(resultBean, SerializerFeature.DisableCircularReferenceDetect);

	}

	/**
	 * 获取行程列表(星空图)
	 * 
	 * @Title: findTripsStarByUserId
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param
	 *             model
	 * @param: @param
	 *             userId
	 * @param: @param
	 *             isOpen 是否查询公开 1-查看所有 2-只查看公开
	 * @param: @param
	 *             tripTime 获取时间点(不包含此时间，yyyy-MM-dd HH:mm:ss 为空查询最近指定条数)
	 * @param: @param
	 *             mpageSize 获取条数(默认20)
	 * @param: @param
	 *             direction 0-获取过去 1-获取未来
	 * @param: @return
	 * @return: ResultBean
	 * @author: LiTeng
	 * @throws @date:
	 *             2016年8月26日 下午5:25:45
	 */
	@RequestMapping("/findTripsByUserId")
	@ResponseBody
	public String findTripsStarByUserId(HttpServletRequest request) {
		ResultBean res = new ResultBean();
		List list = new ArrayList<>();
		int size = 20;
		try {
			// 接参
			Map paramMap = HttpUtils.initParam(request, Map.class);
			if (null == paramMap || paramMap.isEmpty()) {
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg(ResultCode.ParamException.getMsg());
				return JSON.toJSONString(res);
			}
			String userId = MapUtils.getStrValue(paramMap, "userId", "");
			if (StringUtils.isBlank(userId)) {
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg("用户获取失败");
				return JSON.toJSONString(res);
			}
			String mpageSize = MapUtils.getStrValue(paramMap, "mpageSize", "");
			if (!StringUtils.isBlank(mpageSize) && this.isNumeric(mpageSize)) {
				size = Integer.parseInt(mpageSize);
			}
			String tripTime = MapUtils.getStrValue(paramMap, "tripTime", "");
			String isOpen = MapUtils.getStrValue(paramMap, "isOpen", "");
			String direction = MapUtils.getStrValue(paramMap, "direction", "");
			if (StringUtils.isBlank(tripTime)) {
				Map map = tripInfoLocalService.findTripsInfoLately(userId, isOpen);
				if (map != null) {
					if (map.containsKey("time") && map.get("time") != null
							&& !StringUtils.isBlank(map.get("time").toString())) {
						list = tripInfoLocalService.findTripsForInit(userId, isOpen, size, map.get("time").toString());
					}
				}

			} else {
				if (StringUtils.isBlank(mpageSize) || (!direction.equals("1") && !direction.equals("0"))) {
					res.setCode(ResultCode.ParamException.get_code());
					res.setMsg("请求方向出错");
					return JSON.toJSONString(res);
				}
				list = tripInfoLocalService.findTripsByTimeDirection(userId, isOpen, tripTime, size, direction);
			}
			res.setCode(ResultCode.SUCCESS.get_code());
			res.setMsg(ResultCode.SUCCESS.getMsg());
			res.setData(list);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			res.setCode(ResultCode.UNSUCCESS.get_code());
			res.setMsg("获取星空图失败");
			return JSON.toJSONString(res);
		}
		return JSON.toJSONString(res);
	}

	/**
	 * 用简介查询
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getPersonalProfile", method = RequestMethod.GET)
	@ResponseBody
	public String getPersonalProfile(HttpServletRequest request) {
		ResultBean res = new ResultBean(ResultCode.SUCCESS, null);
		String userId = HttpUtils.getParam(request, "userId", "");
		try {
			if (StringUtils.isBlank(userId)) {
				return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
			}
			// 获取用户基本信息|粉丝|关注数
			Map resMap = userLocalService.getUserInfoAndIdentity(userId);
			res.setData(resMap);
		} catch (Exception e) {
			logger.debug(e.getMessage());
			return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
		}
		return JSON.toJSONString(res);
	}

	public static boolean isNumeric(String src) {
		boolean return_value = false;
		if (src != null && src.length() > 0) {
			Matcher m = Pattern.compile("^[0-9\\-]+$").matcher(src);
			if (m.find()) {
				return_value = true;
			}
		}
		return return_value;
	}

	/**
	 * html5获取用户信息根据userId
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/findUserInfo")
	@ResponseBody
	public String getUserInfo(HttpServletRequest request) {
		ResultBean res = new ResultBean(ResultCode.SUCCESS, null);
		String userId = HttpUtils.getParam(request, "userId", "");
		try {
			if (StringUtils.isBlank(userId)) {
				return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
			}
			// 获取用户基本信息|粉丝|关注数
			WebUser webUser = userLocalService.findUserInfoFromCache(userId);
			if (webUser != null) {
				webUser.setPasswords(null);
				webUser.setMobile(null);
				webUser.setOpenidWx(null);
				webUser.setOpenidWb(null);
				webUser.setOpenidQq(null);
				res.setData(webUser);
			} else {
				res = new ResultBean(ResultCode.LoginUserIsNotExist, null);
			}
		} catch (Exception e) {
			logger.debug(e.getMessage());
			return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
		}
		return JSON.toJSONString(res);
	}

	/**
	 * h5根据userId获取主播直播状态及获取进入直播间数据
	 * 修改时间:2017/03/13
	 * 修改内容:修改查询方式及返回数据结构
	 *修改人:葛玉琦
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getLiveStatusByUserId")
	@ResponseBody
	public ResultBean getLiveStatusByUserId(HttpServletRequest request) {
		Map<String, Object> map = HttpUtils.initParam(request, Map.class);
		String userId = MapUtils.getStrValue(map, "userId", "");
		if (StringUtils.isBlank(userId)) {
			return new ResultBean(ResultCode.ParamException.get_code(), "userId为空");
		}
		ResultBean res = new ResultBean(ResultCode.SUCCESS);
		Map<String,Object> resultMap = Maps.newHashMap();
		LivingRoomInfoVo livingRoomInfoVo =  roomEnterExitOptService.getLivingRoomInfoByAuthId(userId);
		if(livingRoomInfoVo!=null){
			resultMap.put("status",1);
			resultMap.put("data",livingRoomInfoVo);
		}else{
			resultMap.put("status",0);
		}
		res.setData(resultMap);
		return res;
	}
}
