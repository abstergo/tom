package com.hefan.api.controller.activity;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.activity.bean.HfActivityCfgInfo;
import com.hefan.api.service.activity.FirstPayAndShareLocalService;
import com.hefan.api.service.activity.HfActivityHandlerLocalService;
import com.hefan.api.service.pay.PayLocalService;
import com.hefan.common.util.HttpUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by lxw on 2016/11/17.
 */
@Controller
@RequestMapping("/v1/appActivity")
public class AppActivityController {
    private Logger logger = LoggerFactory.getLogger(AppActivityController.class);
    @Resource
    private HfActivityHandlerLocalService hfActivityHandlerLocalService;
    @Resource
    private FirstPayAndShareLocalService firstPayAndShareLocalService;

    @Resource
    private PayLocalService payLocalService;

    /**
     * 活动参与请求
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/appActivityAttend")
    @ResponseBody
    public String appActivityAttend(HttpServletRequest request) {
        ResultBean res = new ResultBean(ResultCode.SUCCESS, null);
        try {
            Map paramsMap = HttpUtils.initParam(request, Map.class);
            if (CollectionUtils.isEmpty(paramsMap) || !paramsMap.containsKey("userId")
                    || !paramsMap.containsKey("activityId") || StringUtils.isBlank((String) paramsMap.get("userId"))) {
                return JSON.toJSONString(new ResultBean(ResultCode.ParamException, null));
            }
            String userId = (String) paramsMap.get("userId");
            long activityId = Long.valueOf(String.valueOf(paramsMap.get("activityId")));
            HfActivityCfgInfo hfActivityCfgInfo = hfActivityHandlerLocalService.findActivityCfg(activityId);
            if(hfActivityCfgInfo != null) {
                if (hfActivityCfgInfo.getId() == 2) {
                    res = firstPayAndShareLocalService.firstShareHandler(hfActivityCfgInfo, userId);
                } else {
                    return JSON.toJSONString(new ResultBean(ResultCode.ActivityIsNotAvlid, null));
                }
            } else {
                return JSON.toJSONString(new ResultBean(ResultCode.ActivityIsNotAvlid, null));
            }
        } catch (Exception e) {
            logger.error("AppActivityController---appActivityAttend--Exception:"+e.getMessage());
            return JSON.toJSONString(new ResultBean(ResultCode.UnknownException, null));
        }
        return JSON.toJSONString(res);
    }

}
