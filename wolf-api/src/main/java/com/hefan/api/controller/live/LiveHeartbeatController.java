package com.hefan.api.controller.live;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.live.bean.LivingRoomInfoVo;
import com.hefan.live.itf.RoomEnterExitOptService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.service.JedisService;
import com.hefan.common.util.HttpUtils;
import com.hefan.live.bean.LivingHeartBeatVo;
import com.hefan.live.itf.LivingRedisOptService;

/**
 * 直播间心跳模块 Created by nigle on 2016/10/10.
 */
@Controller
@RequestMapping("/v1/live")
public class LiveHeartbeatController {
	public Logger logger = LoggerFactory.getLogger(LiveHeartbeatController.class);

	@Resource
    JedisService jedisService;

    @Reference
    LivingRedisOptService livingRedisOptService;

	@Reference
	RoomEnterExitOptService roomEnterExitOptService;

	/**
	 * 心跳
	 * 
	 * @param request
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@ResponseBody
	@RequestMapping(value = "/heartBeat", method = RequestMethod.GET)
	public String liveNotice(HttpServletRequest request) {
		ResultBean resultBean = new ResultBean(ResultCode.SUCCESS.get_code(), ResultCode.SUCCESS.getMsg());
		try {
			// 接参
			LivingHeartBeatVo livingHeartBeatVo = HttpUtils.initParam(request, LivingHeartBeatVo.class);
			if (null == livingHeartBeatVo || livingHeartBeatVo.getChatRoomId() <= 0
					|| StringUtils.isBlank(livingHeartBeatVo.getLiveUuid())
					|| StringUtils.isBlank(livingHeartBeatVo.getAuthoruserId())) {
				resultBean.setCode(ResultCode.ParamException.get_code());
				resultBean.setMsg(ResultCode.ParamException.getMsg());
				return JSON.toJSONString(resultBean);
			}

			LivingRoomInfoVo livingRoomInfoVo = roomEnterExitOptService
					.getLivingRoomInfoByAuthId(livingHeartBeatVo.getAuthoruserId());
			if (null == livingRoomInfoVo || !livingRoomInfoVo.getLiveUuid().equals(livingHeartBeatVo.getLiveUuid())) {
				logger.info("直播间心跳接收 -心跳失联");
				resultBean.setCode(ResultCode.HeartBeatBroken.get_code());
				resultBean.setMsg(ResultCode.HeartBeatBroken.getMsg());
				return JSON.toJSONString(resultBean);
			}

			// 心跳超时
			String infoRedis = livingRedisOptService.getLivingHeartBeatInfo(livingHeartBeatVo.getLiveUuid());
			if (StringUtils.isNotBlank(infoRedis)) {
				LivingHeartBeatVo livingHeartBeatVo2 = JSON.parseObject(infoRedis, LivingHeartBeatVo.class);
				if (null != livingHeartBeatVo2 && livingHeartBeatVo2.getChatRoomId() > 0
						&& StringUtils.isNotBlank(livingHeartBeatVo2.getLiveUuid())
						&& StringUtils.isNotBlank(livingHeartBeatVo2.getAuthoruserId())
						&& livingHeartBeatVo2.getUpdateTime() > 0) {
					long currentTime = System.currentTimeMillis();
					// 超时关闭直播间
					if ((currentTime - livingHeartBeatVo2.getUpdateTime()) >= (60 * 1000)) {
						logger.info("直播间心跳接收 -心跳超时");
						resultBean.setCode(ResultCode.HeartBeatBroken.get_code());
						resultBean.setMsg(ResultCode.HeartBeatBroken.getMsg());
						return JSON.toJSONString(resultBean);
					}
				}
			}

			livingHeartBeatVo.setUpdateTime(System.currentTimeMillis());
			if (StringUtils.isNotBlank(infoRedis)) {
				livingRedisOptService.addLivingHeartBeatInfo(livingHeartBeatVo.getLiveUuid(),
						JSON.toJSONString(livingHeartBeatVo));
				logger.info("直播间心跳接收 -心跳更新:",
						livingHeartBeatVo.getLiveUuid() + "=" + JSON.toJSONString(livingHeartBeatVo));
			} else {
				/**
				 * cache time:1 day
				 */
				livingRedisOptService.initLivingHeartBeatInfo(livingHeartBeatVo.getLiveUuid(),
						JSON.toJSONString(livingHeartBeatVo));
				logger.info("直播间心跳接收 -心跳保存:",
						livingHeartBeatVo.getLiveUuid() + "=" + JSON.toJSONString(livingHeartBeatVo));
			}
		} catch (Exception e) {
			logger.error("直播间心跳接收 -更新失败", e);
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
		}
		return JSON.toJSONString(resultBean);
	}
}
