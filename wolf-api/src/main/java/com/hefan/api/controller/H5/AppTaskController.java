package com.hefan.api.controller.H5;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.activity.bean.AppTaskInfo;
import com.hefan.activity.bean.AppTaskRelation;
import com.hefan.activity.bean.AppTaskVo;
import com.hefan.api.service.activity.AppTaskLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;
import com.hefan.user.bean.WebUser;

@Controller
@RequestMapping("/v1/h5")
public class AppTaskController {
	private Logger logger = LoggerFactory.getLogger(AppTaskController.class);
	@Resource
	AppTaskLocalService appTaskLocalService;
	/**
	 * 获取用户任务列表
	 * @Title: getAppTaskInfoList   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param request
	 * @return      
	 * @return: String
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月17日 下午9:02:36
	 */
	@RequestMapping("/getAppTaskInfoList")
	@ResponseBody
	public String getAppTaskInfoList(HttpServletRequest request) {
		// 参数验证标记
		ResultBean<Object> res = new ResultBean<Object>();
		try {
			Map paramMap = HttpUtils.initParam(request, Map.class);
			if (null == paramMap || paramMap.isEmpty()) {
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg(ResultCode.ParamException.getMsg());
				return JSON.toJSONString(res);
			}
			String userId = MapUtils.getStrValue(paramMap, "userId", "");//用户id
			if(StringUtils.isBlank(userId)){
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg("用户标识不合法");
				return JSON.toJSONString(res);
			}
			List<AppTaskVo> list = appTaskLocalService.getAppTaskInfoList(userId);
			res.setCode(ResultCode.SUCCESS.get_code());
			res.setMsg(ResultCode.SUCCESS.getMsg());
			res.setData(list);
		} catch (Exception e) {
			res.setCode(ResultCode.UNSUCCESS.get_code());
			res.setMsg(ResultCode.UNSUCCESS.getMsg());
		}
		return JSON.toJSONString(res);
	}
	
	/**
	 * 领取奖励
	 * @Title: receiveTaskAwards   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param request
	 * @return      
	 * @return: String
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月17日 下午9:16:37
	 */
	@RequestMapping("/receiveTaskAwards")
	@ResponseBody
	public String receiveTaskAwards(HttpServletRequest request) {
		// 参数验证标记
		ResultBean<Object> res = new ResultBean<Object>();
		try {
			Map paramMap = HttpUtils.initParam(request, Map.class);
			if (null == paramMap || paramMap.isEmpty()) {
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg(ResultCode.ParamException.getMsg());
				return JSON.toJSONString(res);
			}
			String userId = MapUtils.getStrValue(paramMap, "userId", "");// 进入直播间用户id
			int taskType = MapUtils.getIntValue(paramMap, "taskType", 0);//任务类型
			if(StringUtils.isBlank(userId)){
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg("用户标识不合法");
				return JSON.toJSONString(res);
			}
			WebUser user = appTaskLocalService.findMyUserInfo(userId);
			if(user == null || StringUtils.isBlank(user.getUserId())){
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg("用户获取失败");
				return JSON.toJSONString(res);
			}
			if(user.getUserType() == 6){
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg("虚拟用户不能领取任务奖励");
				return JSON.toJSONString(res);
			}
			if(taskType <=0){
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg("任务标识不合法");
				return JSON.toJSONString(res);
			}
			AppTaskInfo apt = appTaskLocalService.getAppTaskInfoListByType(taskType);
			AppTaskRelation atr = appTaskLocalService.getAppTaskRelationByUserId(userId);
			if(atr == null || apt == null){
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg("获取完成情况失败");
				return JSON.toJSONString(res);
			}
			if(StringUtils.isBlank(atr.getTasksJson())){
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg("获取完成情况失败");
				return JSON.toJSONString(res);
	    	}
			List<AppTaskVo> voList = JSON.parseArray(atr.getTasksJson(), AppTaskVo.class);
			if(CollectionUtils.isEmptyList(voList)){
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg("获取完成情况失败");
				return JSON.toJSONString(res);
			}
			int f=0;
			for(int i=0;i<voList.size();i++){
				AppTaskVo vo = voList.get(i);
				if(vo.getTaskType()==taskType && vo.getTaskStatus()==1){
					f=1;
					vo.setTaskStatus(4);
					vo.setTaskName(apt.getTaskName());
					//任务说明
					vo.setTaskExplain(apt.getTaskExplain());
					//任务图标
					vo.setTaskIcon(apt.getTaskIcon());
					//完成能获得的经验
					vo.setExpValue(apt.getExpValue());
					//完成能获得的饭票
					vo.setTicketValue(apt.getTicketValue());
				}
			}
			if(f !=1){
				res.setCode(ResultCode.ParamException.get_code());
				res.setMsg("该任务不可领取");
				return JSON.toJSONString(res);
			}
			String tasksJson = JSONObject.toJSONString(voList);
			atr.setTasksJson(tasksJson);
			int st = appTaskLocalService.receiveTaskAwards(atr, apt);
			if(st>0){
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setMsg(ResultCode.SUCCESS.getMsg());
			}else{
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(ResultCode.UNSUCCESS.getMsg());
			}
		} catch (Exception e) {
			res.setCode(ResultCode.UNSUCCESS.get_code());
			res.setMsg(ResultCode.UNSUCCESS.getMsg());
		}
		return JSON.toJSONString(res);
	}
}
