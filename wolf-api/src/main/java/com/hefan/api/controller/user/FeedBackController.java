package com.hefan.api.controller.user;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.service.JedisService;
import com.hefan.api.service.FeedBackLocalService;
import com.hefan.api.util.ValidateCode;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;
import com.hefan.user.bean.FeedBack;
import com.hefan.user.bean.FindBug;
import com.hefan.user.bean.HfFindBugRecord;



@Controller
@RequestMapping("/v1/feedBack")
public class FeedBackController {
	
	public static Logger logger = LoggerFactory.getLogger(FeedBackController.class);
	/**
	 * 严证码redis前缀
	 */
	private final String PHOTOVERIFYCODE = "photoVerifyCode_";
	
	@Resource
	private FeedBackLocalService feedBackLocalService;
	
	
	@Resource
	private JedisService jedisService;
	
	/**
	 * 问题提交反馈
	 * @param feedBack
	 * @param req
	 * @return
	 */
	@RequestMapping(value="/submit",method=RequestMethod.GET)
	@ResponseBody
	public ResultBean<Object> submit(HttpServletRequest req){
		FeedBack feedBack  = HttpUtils.initParam(req, FeedBack.class);
		ResultBean<Object> resultBean = new ResultBean<Object>();
		if(feedBack==null || StringUtils.isBlank(feedBack.getUserId())  || StringUtils.isBlank(feedBack.getContact()) || StringUtils.isBlank(feedBack.getContent())){
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
		}else{
			try {
				feedBackLocalService.submit(feedBack);
				resultBean.setCode(ResultCode.SUCCESS.get_code());
				resultBean.setMsg(ResultCode.SUCCESS.getMsg());
			} catch (Exception e) {
				e.printStackTrace();
				resultBean.setCode(ResultCode.UNSUCCESS.get_code());
				resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			}
		}
		return resultBean;
	}
	
	/**
	 * 提交bug
	 * @param findBug
	 * @param req
	 * @return
	 */
	@RequestMapping(value="/findBug",method=RequestMethod.GET)
	@ResponseBody
	public String findBug(FindBug findBug, HttpServletRequest req){
		findBug  = HttpUtils.initParam(req, FindBug.class);
		ResultBean<Object> resultBean = new ResultBean<Object>();
		if(findBug ==null || StringUtils.isBlank(findBug.getUserId())  || StringUtils.isBlank(findBug.getBugPosition()) || StringUtils.isBlank(findBug.getContent())){
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
		}else{
			Map<String,Object> map = HttpUtils.initParam(req, Map.class);
			String code = MapUtils.getStrValue(map, "code", "");
			if(StringUtils.isBlank(code)){
				resultBean.setCode(ResultCode.UNSUCCESS.get_code());
				resultBean.setMsg("验证码为空");
				return JSON.toJSONString(resultBean);
			}
			String userId = MapUtils.getStrValue(map, "userId", "");
			String codeOld = "";
			try {
				codeOld = jedisService.getStr(PHOTOVERIFYCODE+userId);
			} catch (Exception e1) {
				e1.printStackTrace();
				logger.error("找bug活动校验验证码redis异常");
			}
			if(code.equalsIgnoreCase(codeOld)){
				try {
					feedBackLocalService.findBug(findBug);
					resultBean.setCode(ResultCode.SUCCESS.get_code());
					resultBean.setMsg(ResultCode.SUCCESS.getMsg());
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("找bug活动保存异常");
					resultBean.setCode(ResultCode.UNSUCCESS.get_code());
					resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
				}
			}else{
				resultBean.setCode(ResultCode.UNSUCCESS.get_code());
				resultBean.setMsg("图片验证码错误");
			}
		}
		return JSON.toJSONString(resultBean);
	}
	
	/**
	 * 访问记录生成
	 * @return
	 */
	@RequestMapping(value="/visitRecord",method=RequestMethod.GET)
	@ResponseBody
	public String visitRecord(HfFindBugRecord visitRecord, HttpServletRequest req){
		visitRecord = HttpUtils.initParam(req, HfFindBugRecord.class);
		ResultBean resultBean = new ResultBean();
		if(visitRecord==null || StringUtils.isBlank(visitRecord.getUserId())){
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg(ResultCode.ParamException.getMsg());
		}else{
			try {
				feedBackLocalService.visitRecord(visitRecord);
				resultBean.setCode(ResultCode.SUCCESS.get_code());
				resultBean.setMsg(ResultCode.SUCCESS.getMsg());
			} catch (Exception e) {
				e.printStackTrace();
				resultBean.setCode(ResultCode.UNSUCCESS.get_code());
				resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			}
		}
		return JSON.toJSONString(resultBean);
	}
	
	/**
	 * 提交bug图片验证码生成
	 * @param req
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="/photoVerifyCode",method=RequestMethod.GET)
	public String photoVerifyCode(HttpServletRequest req,HttpServletResponse response){
		ResultBean resultBean = new  ResultBean();
		Map<String,Object> map = HttpUtils.initParam(req, Map.class);
		String userId = MapUtils.getStrValue(map, "userId", "");
		if(StringUtils.isEmpty(userId)){
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg("userId 为空");
		}else{
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setMsg(ResultCode.SUCCESS.getMsg());
			 ValidateCode vCode = new ValidateCode(420,90,4,10);
			 try {
				 jedisService.setexStr(PHOTOVERIFYCODE+userId, vCode.getCode(),6000);
				vCode.write(response.getOutputStream());
			} catch (Exception e) {
				e.printStackTrace();
				resultBean.setCode(ResultCode.UNSUCCESS.get_code());
				resultBean.setMsg("验证码生成失败");
			}
		}
		return JSON.toJSONString(resultBean);
	}
}
