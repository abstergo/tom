package com.hefan.api.controller.user;

import com.alibaba.fastjson.JSONObject;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hefan.api.service.FriendsLocalService;
import com.hefan.api.service.WatchLocalService;
import com.hefan.common.util.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * app-新的好友页面数据
 *
 * @author wangchao
 * @title: wolf-api
 * @package com.hefan.api.controller.user
 * @copyright: Copyright (c) 2017
 * @date 2017-03-15 10:41
 */
@Controller
@RequestMapping("/v1/friend")
public class FriendsController {

  @Resource
  private FriendsLocalService friendsLocalService;

  @Resource
  private WatchLocalService watchLocalService;

  /**
   * 新的好友
   *
   * @param req
   * @return a
   * @throws a
   * @description （用一句话描述该方法的适用条件执行流程、适用方法、注意事项- 可选）
   * @author wangchao
   * @create 2017-03-15 10:43
   */
  @RequestMapping(value = "newFriends", method = RequestMethod.GET)
  @ResponseBody
  public String friends(HttpServletRequest request) {
    ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
    try {
      Map<String, Object> map = Maps.newHashMap();
      List<Map<String, Object>> followList = Lists.newArrayList();
      Map<String, String> paramMap = com.hefan.common.util.HttpUtils.initParam(request, HashMap.class);
      if (CollectionUtils.isEmpty(paramMap)) {
        resultBean = new ResultBean(ResultCode.ParamException);
        return JSONObject.toJSONString(resultBean);
      }
      // 用户ID
      String userId = MapUtils.getStrValue(paramMap, "userId", "");
      if (StringUtils.isBlank(userId)) {
        resultBean = new ResultBean(ResultCode.ParamException);
        return JSONObject.toJSONString(resultBean);
      }
      Page page = new Page();
      page.setPageNo(1);
      page.setPageSize(5);
      page.setOrderBy("time");
      page.setOrder(Page.DESC);
      page = watchLocalService.listFansInfo(page,userId);
      if(page !=null){
        List<Map<String,Object>> list=page.getResult();
        for (Map<String,Object> temp:list) {
          String user=String.valueOf(temp.get("userId"));
          int type=watchLocalService.getWatchRelationByUserId(userId,user);
          temp.put("isFollow",type);
          followList.add(temp);
        }
      }
      /**/
      map.put("followLists", followList);
      resultBean.setData(map);
    } catch (Exception e) {

    }
    return JSONObject.toJSONString(resultBean);
  }

  /**
   * 热门推荐
   * @param req
   * @return
     */
  @RequestMapping(value = "hotRecommend", method = RequestMethod.GET)
  @ResponseBody
  public String hotRecommend(HttpServletRequest req) {
    ResultBean resultBean = friendsLocalService.getHotRecommend();
    return JSONObject.toJSONString(resultBean);
  }
}
