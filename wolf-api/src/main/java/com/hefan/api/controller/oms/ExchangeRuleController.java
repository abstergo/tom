package com.hefan.api.controller.oms;

import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.cat.common.meta.UserTypeEnum;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.api.service.UserLocalService;
import com.hefan.user.bean.WebUser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.api.service.oms.ExchangeRuleLocalService;
import com.hefan.common.util.HttpUtils;
import com.hefan.common.util.MapUtils;

@Controller
@RequestMapping("/v1/user")
public class ExchangeRuleController {

	public static Logger logger = LoggerFactory.getLogger(ExchangeRuleController.class);
	@Resource
	ExchangeRuleLocalService exchangeRuleLocalService;

	@Resource
	private UserLocalService userLocalService;
	
	/**
	 * 兑换规则
	 * @param userId  主播id
	 * @param req
	 * @return
	 */
    @RequestMapping("/exchangeRule")
    @ResponseBody
	public String exchangeRule(String userId,HttpServletRequest req){
    	ResultBean resultBean = new  ResultBean(ResultCode.SUCCESS);
    	Map<String, Object> map = HttpUtils.initParam(req, Map.class);
		userId = MapUtils.getStrValue(map, "userId", "");
		
		if(StringUtils.isEmpty(userId)){
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg("用户id为空");
		}else{
			try {
				WebUser webUser = userLocalService.findUserInfoFromCache(userId);
				if(webUser == null) {
					resultBean.setCode(ResultCode.ParamException.get_code());
					resultBean.setMsg("用户信息不存在");
				} else {
					Map<String,Object> exchangeRuleMap = exchangeRuleLocalService.ExchangeRuleForHeFanOrFanPiaoByUserId(userId,webUser.getUserType());
					if(CollectionUtils.isEmpty(exchangeRuleMap)) {
						resultBean.setCode(ResultCode.UNSUCCESS.get_code());
						resultBean.setMsg("兑换比例配置错误");
					} else {
						resultBean.setData(exchangeRuleMap);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				resultBean.setCode(ResultCode.UNSUCCESS.get_code());
				resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			}
		}
		return JSON.toJSONString(resultBean);
	}

	/**
	 * 兑换规则与初始化兑换的数据
	 * @param userId  主播id
	 * @param req
	 * @return
	 */
	@RequestMapping("/exchangeRuleAndData")
	@ResponseBody
	public String exchangeRuleAndData(String userId,HttpServletRequest req){
		ResultBean resultBean = new  ResultBean();
		Map<String, Object> map = HttpUtils.initParam(req, Map.class);
		userId = MapUtils.getStrValue(map, "userId", "");

		if(StringUtils.isEmpty(userId)){
			resultBean.setCode(ResultCode.ParamException.get_code());
			resultBean.setMsg("用户信息不存在");
		}else{
			try {
				resultBean =  exchangeRuleLocalService.exchangeRuleAndData(userId);
			} catch (Exception e) {
				logger.error(e.getMessage());
				resultBean.setCode(ResultCode.UNSUCCESS.get_code());
				resultBean.setMsg(e.getMessage());
			}
		}

		return JSON.toJSONString(resultBean);
	}
	
}
