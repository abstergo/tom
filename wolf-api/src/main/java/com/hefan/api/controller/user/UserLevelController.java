package com.hefan.api.controller.user;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.google.common.collect.Maps;
import com.hefan.user.itf.WebUserPrivilegeService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * Created by hbchen on 2016/10/17.
 * 我的等级
 */
@Controller
@RequestMapping("/v1/userLevel")
public class UserLevelController {
    @Reference
    WebUserPrivilegeService webUserPrivilegeService;

    @RequestMapping(value = "/findUserLevelPrivilege", method = RequestMethod.GET)
    @ResponseBody
    public ResultBean findUserLevelPrivilege(String userId) {

        if(StringUtils.isBlank(userId)){
            return new ResultBean(ResultCode.ParamException.get_code(), "userId验证错误",Maps.newHashMap());
        }
        Map resultMap = Maps.newHashMap();
        try{
            resultMap = webUserPrivilegeService.getUserLevelInfo(userId);
            if (resultMap == null){
                resultMap = Maps.newHashMap();
            }
        }catch (Exception e){
            e.printStackTrace();
            if (resultMap == null)
                resultMap = Maps.newHashMap();
        }

        return new ResultBean(ResultCode.SUCCESS, resultMap);
    }
}
