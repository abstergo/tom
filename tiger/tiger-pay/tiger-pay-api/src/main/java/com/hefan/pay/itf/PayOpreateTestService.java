package com.hefan.pay.itf;

import com.cat.common.entity.ResultBean;

import java.util.Map;

/**
 * Created by lxw on 2016/10/17.
 */
public interface PayOpreateTestService {

    /**
     * 第三方充值测试
     * @return
     */
    public ResultBean payOpreataTest(Map paramMap);

    /**
     * 支付通知测试
     * @param paramMap
     * @return
     */
    public ResultBean payNotifyHandlerTest(Map paramMap);

    /**
     * 苹果内购验证测试
     * @return
     */
    public ResultBean appleStoreCheckTest();

}
