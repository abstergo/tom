package com.hefan.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class CurrencyRecharge implements Serializable {
	private int id;
	private String curname;
	private int amount;
	private int extamout;
	private String productId;
	private BigDecimal price;
	private int status;
	private int zf;
	private String iosProductCode;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId == null ? "" : productId.trim();
	}

	public int getExtamout() {
		return extamout;
	}

	public void setExtamout(int extamout) {
		this.extamout = extamout;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCurname() {
		return curname;
	}

	public void setCurname(String curname) {
		this.curname = curname;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getZf() {
		return zf;
	}

	public void setZf(int zf) {
		this.zf = zf;
	}

	public String getIosProductCode() {
		return iosProductCode;
	}

	public void setIosProductCode(String iosProductCode) {
		this.iosProductCode = iosProductCode;
	}
}
