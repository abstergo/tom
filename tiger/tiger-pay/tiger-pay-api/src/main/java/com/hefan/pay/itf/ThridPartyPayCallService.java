package com.hefan.pay.itf;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.PaymentTypeEnum;
import com.hefan.pay.bean.PayExtendVo;
import com.hefan.pay.bean.ThridPartyNotifyVo;
import com.hefan.pay.bean.TradeLog;

import java.util.Map;

/**
 * 第三方支付调用接口
 * Created by lxw on 2016/10/12.
 */
public interface ThridPartyPayCallService {
    /**
     * 通知成功|异常也需要继续处理
     */
    int NOTFIY_EXCEPTION_IS_HANDLER = 1;
    /**
     * 通过异常不需要继续处理
     */
    int NOTFIY_EXCEPTION_IS_NOT_HANDLER = 2;

    /**
     * 第三方支付发起调用接口
     * @param paymentTypeEnum 支付方式枚举类
     * @param tradeLog 支付方式枚举类
     * @param payExtendVo 支付扩展类
     * @return
     * @throws Exception
     */
    public ResultBean thridPartyPayCall(PaymentTypeEnum paymentTypeEnum, TradeLog tradeLog, PayExtendVo payExtendVo);


    /**
     * 第三方支付回调通知接口
     * @param orderId
     * @param thridPartyNotifyVo
     * @return
     * @throws Exception
     */
    public ResultBean thridPartyPayNotify(String orderId, ThridPartyNotifyVo thridPartyNotifyVo);

    /**
     * 第三方支付回调通知信息解析
     * @param thridPartyNotifyVo
     * @return
     */
    public ResultBean thridPartyPayNotifyParse(ThridPartyNotifyVo thridPartyNotifyVo);
}
