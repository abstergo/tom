package com.hefan.pay.itf;

/**
 * Created by lxw on 2016/10/13.
 */
public interface PayThridPartyFactoryService {

    /**
     * 获取第三方支付实现对象
     * @param payMonthType
     * @return
     */
    public ThridPartyPayCallService payThridPartyFactory(int payMonthType);
}
