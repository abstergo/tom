package com.hefan.pay.bean;

import java.io.Serializable;

/**
 * Created by lxw on 2016/10/13.
 */
public class PayExtendVo implements Serializable{

    /**
     * 用户userId
     */
    private String userId;
    /**
     * 支付订单号
     */
    private String orderId;

    /**
     * 支付金额
     */
    private String payAmount;

    /**
     * 支付客户端ip
     */
    private String clientIp;

    /**
     * 需要页面返回通知的页面url
     */
    private String returnUrl;

    /**
     * 支付宝扫码支付时 自定义二维码宽度
     */
    private String qrcodeWidth;


    /**
     * apple app store 记录请求日志记录 客户端请求支付信息
     */
    private String paymentExtend;
    
    
    
    public PayExtendVo() {
    }

    public PayExtendVo(String userId, String orderId, String payAmount, String clientIp, String qrcodeWidth, String returnUrl) {
        this.userId = userId;
        this.orderId = orderId;
        this.payAmount = payAmount;
        this.clientIp = clientIp;
        this.qrcodeWidth = qrcodeWidth;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getQrcodeWidth() {
        return qrcodeWidth;
    }

    public void setQrcodeWidth(String qrcodeWidth) {
        this.qrcodeWidth = qrcodeWidth;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getPaymentExtend() {
        return paymentExtend;
    }

    public void setPaymentExtend(String paymentExtend) {
        this.paymentExtend = paymentExtend;
    }
}
