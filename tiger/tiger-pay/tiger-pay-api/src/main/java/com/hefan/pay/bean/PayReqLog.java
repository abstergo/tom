package com.hefan.pay.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by lxw on 2016/10/12.
 */
@Entity(tableName="pay_req_log")
public class PayReqLog extends BaseEntity {

    /**
     * 订单流水号
     */
    @Column("order_id")
    private String orderId;

    /**
     * 支付用户userId
     */
    @Column("user_id")
    private String userId;

    /**
     * 请求的url
     */
    @Column("req_url")
    private String reqUrl;

    /**
     * 支付方式（1 ios apple pay  2 安卓微信支付 3 安卓支付宝支付 4 PC 微信宝支付 5 PC 支付宝支付 6 微信公共账号 微信支付）
     */
    @Column("payment_type")
    private int paymentType;

    /**
     * 请求串
     */
    @Column("req_params")
    private String reqParams;

    /**
     * 响应串
     */
    @Column("resp_params")
    private String respParams;

    /**
     * 请求：1000 成功 其它失败
     */
    private int status;

    /**
     * 回调通知结果状态
     */
    @Column("notify_status")
    private int notifyStatus;

    /**
     * 回调通知结果描述
     */
    @Column("notify_result")
    private String notifyResult;

    /**
     * 回调通知返回参数
     */
    @Column("notify_params")
    private String notifyParams;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getReqUrl() {
        return reqUrl;
    }

    public void setReqUrl(String reqUrl) {
        this.reqUrl = reqUrl;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public String getReqParams() {
        return reqParams;
    }

    public void setReqParams(String reqParams) {
        this.reqParams = reqParams;
    }

    public String getRespParams() {
        return respParams;
    }

    public void setRespParams(String respParams) {
        this.respParams = respParams;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getNotifyStatus() {
        return notifyStatus;
    }

    public void setNotifyStatus(int notifyStatus) {
        this.notifyStatus = notifyStatus;
    }

    public String getNotifyResult() {
        return notifyResult;
    }

    public void setNotifyResult(String notifyResult) {
        this.notifyResult = notifyResult;
    }

    public String getNotifyParams() {
        return notifyParams;
    }

    public void setNotifyParams(String notifyParams) {
        this.notifyParams = notifyParams;
    }
}
