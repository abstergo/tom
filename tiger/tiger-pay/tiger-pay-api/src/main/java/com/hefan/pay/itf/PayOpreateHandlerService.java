package com.hefan.pay.itf;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.PaymentTypeEnum;
import com.hefan.pay.bean.PayExtendVo;
import com.hefan.pay.bean.ThridPartyNotifyVo;
import com.hefan.pay.bean.TradeLog;

import java.util.Map;

/**
 * 第三方支付处理类
 * Created by lxw on 2016/10/15.
 */
public interface PayOpreateHandlerService {

    /**
     * 第三方支付回调通知处理
     * @param thridPartyNotifyVo
     * @return
     */
    public ResultBean thridPartyPayNotfiyOprater(ThridPartyNotifyVo thridPartyNotifyVo);

    /**
     * 发起第三方充值
     * @param paymentTypeEnum
     * @param tradeLog
     * @param payExtendVo
     * @return
     */
    public ResultBean payOpreataHandler(PaymentTypeEnum paymentTypeEnum, TradeLog tradeLog, PayExtendVo payExtendVo);


    /**
     * 发起第三方充值 测试
     * @param paymentTypeEnum
     * @param tradeLog
     * @param payExtendVo
     * @return
     */
    public ResultBean payOpreataHandlerTest(PaymentTypeEnum paymentTypeEnum, TradeLog tradeLog, PayExtendVo payExtendVo);


}
