package com.hefan.pay.itf;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hefan.pay.bean.CurrencyExchange;
import com.hefan.pay.bean.CurrencyRecharge;
import com.hefan.pay.bean.CurrentTypePrice;

/**
 * @author waasdasd
 *
 */
public interface CurrentRechargeService {
	/**
	 * 按种类查饭票
	 * 
	 * @param status
	 * @return
	 */
	List<CurrencyRecharge> getCurrencyRecharge(int status,String  id);

	/**
	 * 获取充值赠送信息对象
	 * @param status
	 * @param id
     * @return
     */
	public CurrencyRecharge getCurrencyRechargeInfo(int status, int id);

	/**
	 * byId 
	 * @param id
	 * @param id
	 * @return
	 */
	CurrentTypePrice findCurrentPriceById( int id);
	
	
	/**
	 * check 
	 * 
	 * @param status
	 * @return
	 */
	CurrencyExchange getCurrencyexchange(int status);


	/**
	 * 根据兑换比例类型Set获取兑换比例列表
	 * @param types
	 * @return
     */
	public List<CurrencyExchange> queryCurrencyExchangeList(List<Integer> types);

	/**
	 * check 
	 * 
	 * @param status
	 * @return
	 */
//	boolean checkCurrencyExchange(CurrencyRecharge recharge);
	
	
}
