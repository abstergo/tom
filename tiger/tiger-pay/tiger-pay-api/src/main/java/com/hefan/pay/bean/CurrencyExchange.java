package com.hefan.pay.bean;

import java.io.Serializable;

public class CurrencyExchange implements Serializable{
       private int id;
       private int exchangeRuleType;
       private int exchangeRatio;
       private int beExchangeRatio;
       public int getBeExchangeRatio() {
		return beExchangeRatio;
	}
	public void setBeExchangeRatio(int beExchangeRatio) {
		this.beExchangeRatio = beExchangeRatio;
	}
	private String note;
       private int zf;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getExchangeRuleType() {
		return exchangeRuleType;
	}
	public void setExchangeRuleType(int exchangeRuleType) {
		this.exchangeRuleType = exchangeRuleType;
	}
	public int getExchangeRatio() {
		return exchangeRatio;
	}
	public void setExchangeRatio(int exchangeRatio) {
		this.exchangeRatio = exchangeRatio;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public int getZf() {
		return zf;
	}
	public void setZf(int zf) {
		this.zf = zf;
	}
}
