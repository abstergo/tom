package com.hefan.pay.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 支付订单流水表 Created by lxw on 2016/10/15.
 */
public class TradeLog implements Serializable {

	private int id;

	/**
	 * 支付订单号
	 */
	private String orderId;
	private Date addTime;
	/**
	 * 用户userId
	 */
	private String UserId;
	private String nickName;
	/**
	 * 充值金额(实际支付RMB金额)
	 */
	private BigDecimal payAmount;
	/**
	 * 充值饭票数
	 */
	private int income;
	/**
	 * 实际收入饭票(充值饭票+赠送饭票)
	 */
	private int incomeAmount;
	/**
	 * 实收金额 （充值金额 - 渠道费用）
	 */
	private BigDecimal exchangeAmount;
	/**
	 * 渠道手续费
	 */
	private BigDecimal channelFee;
	/**
	 * 赠送的饭票
	 */
	private int rewardFanpiao;
	/**
	 * 赠送的饭票价值金额
	 */
	private BigDecimal rewardFanpiaoAmount;
	/**
	 * 类型（1,支付宝2：微信 3:ApplePay 4: 充值码 5:任务 6:后台）
	 */
	private int accountType;
	/**
	 * 支付来源：1：PC端 2：IOS端 3：安卓端 4: 公共账号
	 */
	private int paySource;
	/**
	 * 支付前饭票数
	 */
	private long payBeforeFanpiao;
	/**
	 * 支付后饭票
	 */
	private long payAfterFanpiao;
	private String remark;
	private Date updateTime;
	/**
	 * 针对失败情况进行手工处理 0：未处理 1：已处理
	 */
	private int isHandle;
	private String handleContent;
	private int handleUserId;
	private String handleUser;
	private Date handleTime;
	private int userType;
	private Date payNotifyDate;
	/**
	 * 充值码充值的充值码code
	 */
	private String rechargeCode;
	/**
	 * 支付状态 '0 作废 1 支付失败 2 未支付 4 支付回调通知成功 3 支付成功 '
	 */
	private int payStatus;
	/**
	 * 平台担负费用 (微信支付和支付支付手续费需要平台自己承担)
	 */
	private BigDecimal platformCost;
	/**
	 * 付当时兑换扩展信息json (支付时的兑换比例及赠送等规则信息)
	 */
	private String paymentExtend;
	/**
	 * 支付方式：支付调用使用: 0 其它充值使用, 1 IOS 的APPLE PAY, 2 Android 微信支付, 3 Android 支付宝支付,
	 * 4 PC 微信宝支付, 5 PC 支付宝支付, 6 微信公共账号 微信支付
	 */
	private int paymentType;

	/**
	 * 第三方支付的订单号
	 */
	private String thirdPartyTradeNo;

	/**
	 * 苹果内购的产品code
	 */
	private String iosProductCode;


	private String accountStatus;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getAddTime() {
		return addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public String getUserId() {
		return UserId;
	}

	public void setUserId(String userId) {
		UserId = userId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public BigDecimal getPayAmount() {
		return payAmount;
	}

	public void setPayAmount(BigDecimal payAmount) {
		this.payAmount = payAmount;
	}

	public int getIncome() {
		return income;
	}

	public void setIncome(int income) {
		this.income = income;
	}

	public int getIncomeAmount() {
		return incomeAmount;
	}

	public void setIncomeAmount(int incomeAmount) {
		this.incomeAmount = incomeAmount;
	}

	public BigDecimal getExchangeAmount() {
		return exchangeAmount;
	}

	public void setExchangeAmount(BigDecimal exchangeAmount) {
		this.exchangeAmount = exchangeAmount;
	}

	public BigDecimal getChannelFee() {
		return channelFee;
	}

	public void setChannelFee(BigDecimal channelFee) {
		this.channelFee = channelFee;
	}

	public int getRewardFanpiao() {
		return rewardFanpiao;
	}

	public void setRewardFanpiao(int rewardFanpiao) {
		this.rewardFanpiao = rewardFanpiao;
	}

	public BigDecimal getRewardFanpiaoAmount() {
		return rewardFanpiaoAmount;
	}

	public void setRewardFanpiaoAmount(BigDecimal rewardFanpiaoAmount) {
		this.rewardFanpiaoAmount = rewardFanpiaoAmount;
	}

	public int getAccountType() {
		return accountType;
	}

	public void setAccountType(int accountType) {
		this.accountType = accountType;
	}

	public int getPaySource() {
		return paySource;
	}

	public void setPaySource(int paySource) {
		this.paySource = paySource;
	}

	public long getPayBeforeFanpiao() {
		return payBeforeFanpiao;
	}

	public void setPayBeforeFanpiao(long payBeforeFanpiao) {
		this.payBeforeFanpiao = payBeforeFanpiao;
	}

	public long getPayAfterFanpiao() {
		return payAfterFanpiao;
	}

	public void setPayAfterFanpiao(long payAfterFanpiao) {
		this.payAfterFanpiao = payAfterFanpiao;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public int getIsHandle() {
		return isHandle;
	}

	public void setIsHandle(int isHandle) {
		this.isHandle = isHandle;
	}

	public String getHandleContent() {
		return handleContent;
	}

	public void setHandleContent(String handleContent) {
		this.handleContent = handleContent;
	}

	public int getHandleUserId() {
		return handleUserId;
	}

	public void setHandleUserId(int handleUserId) {
		this.handleUserId = handleUserId;
	}

	public String getHandleUser() {
		return handleUser;
	}

	public void setHandleUser(String handleUser) {
		this.handleUser = handleUser;
	}

	public Date getHandleTime() {
		return handleTime;
	}

	public void setHandleTime(Date handleTime) {
		this.handleTime = handleTime;
	}

	public String getRechargeCode() {
		return rechargeCode;
	}

	public void setRechargeCode(String rechargeCode) {
		this.rechargeCode = rechargeCode;
	}

	public int getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(int payStatus) {
		this.payStatus = payStatus;
	}

	public BigDecimal getPlatformCost() {
		return platformCost;
	}

	public void setPlatformCost(BigDecimal platformCost) {
		this.platformCost = platformCost;
	}

	public String getPaymentExtend() {
		return paymentExtend;
	}

	public void setPaymentExtend(String paymentExtend) {
		this.paymentExtend = paymentExtend;
	}

	public int getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(int paymentType) {
		this.paymentType = paymentType;
	}

	public String getThirdPartyTradeNo() {
		return thirdPartyTradeNo;
	}

	public void setThirdPartyTradeNo(String thirdPartyTradeNo) {
		this.thirdPartyTradeNo = thirdPartyTradeNo;
	}

	public Date getPayNotifyDate() {
		return payNotifyDate;
	}

	public void setPayNotifyDate(Date payNotifyDate) {
		this.payNotifyDate = payNotifyDate;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getIosProductCode() {
		return iosProductCode;
	}

	public void setIosProductCode(String iosProductCode) {
		this.iosProductCode = iosProductCode;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}
}
