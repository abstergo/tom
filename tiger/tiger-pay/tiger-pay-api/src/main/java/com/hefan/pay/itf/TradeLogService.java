package com.hefan.pay.itf;

import com.hefan.pay.bean.TradeLog;

import java.math.BigDecimal;

/**
 * Created by lxw on 2016/10/15.
 */
public interface TradeLogService {

	/**
	 * 根据支付订单号获取支付下单信息
	 * 
	 * @param orderId
	 * @return
	 */
	public TradeLog getTradeLogByOrderId(String orderId);

	/**
	 * 更新支付状态|第三方订单号|平台担负的费率
	 * 
	 * @param orderId
	 * @param payStatus
	 * @param thirdPartyTradeNo
	 * @param platformCost
	 * @param payBeforeFanpiao
	 * @param payAfterFanpiao
	 * @return
	 */
	public int updateTradeLogPayStatus(String orderId, int payStatus, String thirdPartyTradeNo, BigDecimal platformCost,
			long payBeforeFanpiao, long payAfterFanpiao);

	/**
	 * 第三方通知回调后进行更新充值订单信息
	 * 
	 * @param tradeLog
	 * @return
	 */
	public int thridPartyNotfiySuccessOprate(TradeLog tradeLog);

	/**
	 * 初始化充值订单
	 * 
	 * @param tradeLog
	 * @return
	 */
	public long initTradeLogInfo(TradeLog tradeLog);

	public int checkOrderId(String orderId);

	/**
	 * 计算并保存充值信息
	 * @param tradeLog
	 * @return
     */
	public long saveAndCalTradeLog(TradeLog tradeLog);

}
