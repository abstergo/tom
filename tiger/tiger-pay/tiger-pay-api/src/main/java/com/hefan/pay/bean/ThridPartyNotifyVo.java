package com.hefan.pay.bean;

import java.io.Serializable;
import java.util.Map;

/**
 * 第三方回调通知返回参数Vo
 * Created by lxw on 2016/10/14.
 */
public class ThridPartyNotifyVo implements Serializable {

    /**
     * 微信解析结果code
     */
    private int wechatParseCode;
    /**
     * 微信解析结果详情
     */
    private String wechatParseMsg;

    /**
     * 微信回调通知返回参数(xml格式字符串)
     */
    private String wechatRequstParamStr = "";

    /**
     * 订单号
     */
    private String orderId;

    /**
     * 支付方式类型
     */
    private int paymentType;
    /**
     * 第三方支付订单号
     */
    private String thirdPartyTradeNo;

    /**
     * 支付宝支付回调通知的订单状态
     */
    private String alipayTradeStatus;

    /**
     * 支付宝卖家sellerId
     */
    private String alipaySellerId;

    /**
     * 回调通知返回的支付金额（单位元,保留2位小数）
     */
    private String totalAmount;

    /**
     * 阿里回调通知返回的参数Map对象
     */
    private Map aliPayRequestParamsMap;

    /**
     * Apple app store pay 客户端发送苹果内购的验证收据base64加密串
     */
    private String appleReceipt;

    private String userId;

    /**
     * 苹果内购时的商品code码
     */
    private String iosProductCode;

    /**
     * 苹果内购订单码
     */
    private String transactionId;

    public int getWechatParseCode() {
        return wechatParseCode;
    }

    public void setWechatParseCode(int wechatParseCode) {
        this.wechatParseCode = wechatParseCode;
    }

    public String getWechatParseMsg() {
        return wechatParseMsg == null ?"":wechatParseMsg;
    }

    public void setWechatParseMsg(String wechatParseMsg) {
        this.wechatParseMsg = wechatParseMsg;
    }

    public String getWechatRequstParamStr() {
        return wechatRequstParamStr;
    }

    public void setWechatRequstParamStr(String wechatRequstParamStr) {
        this.wechatRequstParamStr = wechatRequstParamStr;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(int paymentType) {
        this.paymentType = paymentType;
    }

    public String getThirdPartyTradeNo() {
        return thirdPartyTradeNo;
    }

    public void setThirdPartyTradeNo(String thirdPartyTradeNo) {
        this.thirdPartyTradeNo = thirdPartyTradeNo;
    }

    public String getAlipayTradeStatus() {
        return alipayTradeStatus;
    }

    public void setAlipayTradeStatus(String alipayTradeStatus) {
        this.alipayTradeStatus = alipayTradeStatus;
    }

    public String getAlipaySellerId() {
        return alipaySellerId;
    }

    public void setAlipaySellerId(String alipaySellerId) {
        this.alipaySellerId = alipaySellerId;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Map getAliPayRequestParamsMap() {
        return aliPayRequestParamsMap;
    }

    public void setAliPayRequestParamsMap(Map aliPayRequestParamsMap) {
        this.aliPayRequestParamsMap = aliPayRequestParamsMap;
    }

    public String getAppleReceipt() {
        return appleReceipt;
    }

    public void setAppleReceipt(String appleReceipt) {
        this.appleReceipt = appleReceipt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getIosProductCode() {
        return iosProductCode;
    }

    public void setIosProductCode(String iosProductCode) {
        this.iosProductCode = iosProductCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}
