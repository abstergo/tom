package com.hefan.pay.bean;

import java.io.Serializable;

/**
 * 第三方费率配置表
 * Created by lxw on 2016/10/18.
 */
public class PayThridPartyRate implements Serializable {

    private int id;

    /**
     * 第三方类型
     */
    private int thridType;

    /**
     * 费率
     */
    private String rateVal;
}
