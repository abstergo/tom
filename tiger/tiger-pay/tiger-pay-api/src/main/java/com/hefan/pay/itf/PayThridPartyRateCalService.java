package com.hefan.pay.itf;

import java.math.BigDecimal;

/**
 * Created by lxw on 2016/10/18.
 */
public interface PayThridPartyRateCalService {

    /**
     * 根据第三支付平台类型获取平台费率
     * @param thridType
     * @return
     */
    public String getPayThridRateVal(int thridType);

    /**
     * 计算出平台担负的费率
     * @param thridType
     * @param payAmount
     * @return
     */
    public BigDecimal platformCostCal(int thridType, BigDecimal payAmount);

}
