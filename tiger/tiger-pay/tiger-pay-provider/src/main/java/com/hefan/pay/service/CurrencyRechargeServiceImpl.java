package com.hefan.pay.service;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import com.hefan.pay.dao.CurrencyExchangeDao;
import org.springframework.stereotype.Component;

import com.hefan.pay.bean.CurrencyExchange;
import com.hefan.pay.bean.CurrencyRecharge;
import com.hefan.pay.bean.CurrentTypePrice;
import com.hefan.pay.dao.CurrencyRechargeDao;
import com.hefan.pay.itf.CurrentRechargeService;
import org.springframework.transaction.annotation.Transactional;

@Component("currentRechargeService")
public class CurrencyRechargeServiceImpl implements CurrentRechargeService {

	@Resource
	private CurrencyRechargeDao currencyRechargeDao;

	@Resource
	private CurrencyExchangeDao currencyExchangeDaoPay;

	@Override
	public List<CurrencyRecharge> getCurrencyRecharge(int status, String id) {
		// TODO Auto-generated method stub
		return currencyRechargeDao.getCurrencyRecharge(status, id);
	}

	@Override
	@Transactional(readOnly = true)
	public CurrencyRecharge getCurrencyRechargeInfo(int status, int id) {
		return currencyRechargeDao.getCurrencyRechargeInfo(status, id);
	}

	@Override
	public CurrentTypePrice findCurrentPriceById(int id) {
		// TODO Auto-generated method stub
		return currencyRechargeDao.findCurrentPriceById(id);
	}

	@Override
	public CurrencyExchange getCurrencyexchange(int status) {
		// TODO Auto-generated method stub
		return currencyRechargeDao.getCurrencyExchange(status);
	}

	@Override
	@Transactional(readOnly = true)
	public List<CurrencyExchange> queryCurrencyExchangeList(List<Integer> types) {
		return currencyExchangeDaoPay.getListByRuleTypes(types);
	}

	// @Override
	// public boolean checkCurrencyExchange(CurrencyRecharge recharge) {
	// // TODO Auto-generated method stub
	// int i = currencyRechargeDao.checkCurrencyExchange(recharge);
	// if (i > 0) {
	// return true;
	// } else {
	// return false;
	// }
	// }

}
