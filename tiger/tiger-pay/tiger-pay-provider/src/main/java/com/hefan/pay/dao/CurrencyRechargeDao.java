package com.hefan.pay.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.jsoup.helper.StringUtil;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.common.orm.domain.BaseEntity;
import com.hefan.pay.bean.CurrencyExchange;
import com.hefan.pay.bean.CurrencyRecharge;
import com.hefan.pay.bean.CurrentTypePrice;

@Repository
public class CurrencyRechargeDao extends BaseDaoImpl<BaseEntity> {

	/**
	 * 饭票
	 * 
	 * @param status
	 * @return
	 */
	public List<CurrencyRecharge> getCurrencyRecharge(int status, String id) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" id,curname ,amount,extamout,price,ios_product_code productId from currency_recharge");
		sql.append(" where status=" + status + " and zf=0 order by price asc");
		if (!StringUtil.isBlank(id)) {
			sql.append(" and id=" + id + "");
		}
		List<CurrencyRecharge> list = getJdbcTemplate().query(sql.toString(),
				new BeanPropertyRowMapper(CurrencyRecharge.class));
		System.out.println(sql);
		return list;
	}

	/**
	 * 获取充值赠送配置信息
	 * 
	 * @param status
	 * @param id
	 * @return
	 */
	public CurrencyRecharge getCurrencyRechargeInfo(int status, int id) {
		String sql = "select * from currency_recharge where zf=0 and  status=? and id=?";
		List params = new ArrayList();
		params.add(status);
		params.add(id);
		List<CurrencyRecharge> list = getJdbcTemplate().query(sql, params.toArray(),
				new BeanPropertyRowMapper<CurrencyRecharge>(CurrencyRecharge.class));
		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 弹幕私信饭票
	 * 
	 * @param id
	 * @return
	 */
	public CurrentTypePrice findCurrentPriceById(int id) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" price ");
		sql.append(" from currency_type_price  where  id=" + id + " and zf=0");
		List<CurrentTypePrice> list = getJdbcTemplate().query(sql.toString(),
				new BeanPropertyRowMapper(CurrentTypePrice.class));
		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;

	}

	/**
	 * 饭票兑换比
	 * 
	 * @param status
	 * @return
	 */
	public CurrencyExchange getCurrencyExchange(int status) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(
				" id, exchange_ratio exchangeRatio, be_exchange_ratio beExchangeRatio ,exchange_rule_type exchangeRuleType from currency_exchange");
		sql.append(" where exchange_rule_type=" + status + " and zf=0");
		List<CurrencyExchange> list = getJdbcTemplate().query(sql.toString(),
				new BeanPropertyRowMapper(CurrencyExchange.class));
		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}
	//
	// /**
	// * check 比率
	// *
	// * @param status
	// * @return
	// */
	// public int checkCurrencyExchange(CurrencyRecharge recharge) {
	// StringBuilder sql = new StringBuilder();
	// sql.append(" SELECT");
	// sql.append(" * from currency_recharge");
	// sql.append(" where id=" + recharge.getId() + " and amount=" +
	// recharge.getAmount() + " and extamount="
	// + recharge.getPrice() + " and price =" + recharge.getPrice() + " and
	// status =" + recharge.getStatus()
	// + " and zf =0");
	// return getJdbcTemplate().queryForList(sql.toString()).size();
	// }
}
