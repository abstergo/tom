package com.hefan.pay.service;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.PaymentTypeEnum;
import com.cat.common.meta.ResultCode;
import com.cat.common.util.BigDecimalCalUtil;
import com.hefan.pay.bean.PayExtendVo;
import com.hefan.pay.bean.ThridPartyNotifyVo;
import com.hefan.pay.bean.TradeLog;
import com.hefan.pay.itf.PayOpreateHandlerService;
import com.hefan.pay.itf.PayOpreateTestService;
import com.hefan.pay.itf.PayThridPartyFactoryService;
import com.hefan.pay.itf.ThridPartyPayCallService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;
import java.util.Map;

/**
 * Created by lxw on 2016/10/17.
 */
@Path("/paytest")
@Component("payOpreateTestService")
public class PayOpreateTestServiceImpl implements PayOpreateTestService {

    @Resource
    private PayOpreateHandlerService payOpreateHandlerService;

    @Resource
    private PayThridPartyFactoryService payThridPartyFactoryService;

    @POST
    @Path("/payOpreataTest")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResultBean payOpreataTest(Map paramMap) {

        System.out.println(paramMap);
        int paymentType = Integer.valueOf(String.valueOf(paramMap.get("paymentType")));
        String userId = String.valueOf(paramMap.get("userId"));
        int userType  = Integer.valueOf(String.valueOf(paramMap.get("userType")));
        String payAmount = String.valueOf(paramMap.get("payAmount"));
        String clientIp = "127.0.0.1";
        String qrcodeWidth = String.valueOf(paramMap.get("qrcodeWidth"));
        int fanpsh = new BigDecimal(BigDecimalCalUtil.mul(payAmount,"10",0)).intValue();

        PaymentTypeEnum payEnum = PaymentTypeEnum.getPaymentTypeEnumByPaymentType(paymentType);
        TradeLog tradeLog = new TradeLog();
        tradeLog.setAccountType(payEnum.getAccountType());
        tradeLog.setPaySource(payEnum.getPaySource());
        tradeLog.setPaymentType(payEnum.getPaymentType());
        tradeLog.setIncome(fanpsh);
        tradeLog.setPayAmount(new BigDecimal(payAmount));
        tradeLog.setExchangeAmount(new BigDecimal(payAmount));
        tradeLog.setUserId(userId);
        tradeLog.setUserType(userType);

        PayExtendVo payExtendVo = new PayExtendVo();
        payExtendVo.setClientIp(clientIp);
        payExtendVo.setPayAmount(payAmount);
        payExtendVo.setUserId(userId);
        payExtendVo.setQrcodeWidth(qrcodeWidth);

        //ResultBean resBean = payOpreateHandlerService.payOpreataHandler(payEnum,tradeLog,payExtendVo);

        ResultBean resBean = payOpreateHandlerService.payOpreataHandlerTest(payEnum,tradeLog,payExtendVo);

        return resBean;
    }

    @POST
    @Path("/payNotifyTest")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResultBean payNotifyHandlerTest(Map paramMap) {
        String orderId = String.valueOf(paramMap.get("orderId"));
        String appleReceipt = String.valueOf(paramMap.get("appleReceipt"));
        ThridPartyNotifyVo thridPartyNotifyVo = new ThridPartyNotifyVo();
        thridPartyNotifyVo.setOrderId(orderId);
        thridPartyNotifyVo.setAppleReceipt(appleReceipt);
        ResultBean resBean = payOpreateHandlerService.thridPartyPayNotfiyOprater(thridPartyNotifyVo);
        return resBean;
    }

    @POST
    @Path("/appleCheckTest")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public ResultBean appleStoreCheckTest() {
        ThridPartyPayCallService thridPartyPayCallService = payThridPartyFactoryService.payThridPartyFactory(PaymentTypeEnum.IOS_APPLE_PAY.getPaymentType());
        ThridPartyNotifyVo thridPartyNotifyVo = new ThridPartyNotifyVo();
        thridPartyNotifyVo.setUserId("-1");
        String str = "MIIT8AYJKoZIhvcNAQcCoIIT4TCCE90CAQExCzAJBgUrDgMCGgUAMIIDkQYJKoZIhvcNAQcBoIIDggSCA34xggN6MAoCARQCAQEEAgwAMAsCAQMCAQEEAwwBMzALAgEOAgEBBAMCAXkwCwIBEwIBAQQDDAEzMAsCARkCAQEEAwIBAzANAgEKAgEBBAUWAzE3KzANAgENAgEBBAUCAwGHaDAOAgEBAgEBBAYCBEYEdhkwDgIBCQIBAQQGAgRQMjQ3MA4CAQsCAQEEBgIEBw+GKzAOAgEQAgEBBAYCBDDjtlwwEAIBDwIBAQQIAgY6O91F85YwFAIBAAIBAQQMDApQcm9kdWN0aW9uMBgCAQQCAQIEEGufUgVP8fNB9fKVsy92ZLswHAIBBQIBAQQUSpGO78wA4siUfrMP1a1FFo/oMBswHgIBCAIBAQQWFhQyMDE2LTEyLTI4VDEzOjI2OjQzWjAeAgEMAgEBBBYWFDIwMTYtMTItMjhUMTM6MjY6NDRaMB4CARICAQEEFhYUMjAxNi0xMi0yN1QyMjoxOTozNlowIQIBAgIBAQQZDBdjb20uc3RhcnVuaW9uLmhlZmFubGl2ZTA+AgEHAgEBBDalYBNzLBeoFlllKCL+GAy9Gqtj75yt6vZ9op4wnJNqEj2qHF/uY1DGUB+eNkw3YT/Rz4lVvS0wRwIBBgIBAQQ/PVLdHDrMHScrjor0W8hzwQvl0bMFFlY/DPUeLmHrNKmj91kbPyZoSzmfvCjiv0Bbxt9RIL2jICzTrMHDO8+FMIIBbAIBEQIBAQSCAWIxggFeMAsCAgasAgEBBAIWADALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgEBMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAPAgIGrgIBAQQGAgRGBID/MBoCAganAgEBBBEMDzI0MDAwMDI5MzM4MDM4NTAaAgIGqQIBAQQRDA8yNDAwMDAyOTMzODAzODUwHwICBqgCAQEEFhYUMjAxNi0xMi0yOFQxMzoyNjo0M1owHwICBqoCAQEEFhYUMjAxNi0xMi0yOFQxMzoyNjo0M1owMQICBqYCAQEEKAwmY29tLnN0YXJ1bmlvbi5oZWZhbmxpdmVfUHVyY2hhc2VfNll1YW6ggg5lMIIFfDCCBGSgAwIBAgIIDutXh+eeCY0wDQYJKoZIhvcNAQEFBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTUxMTEzMDIxNTA5WhcNMjMwMjA3MjE0ODQ3WjCBiTE3MDUGA1UEAwwuTWFjIEFwcCBTdG9yZSBhbmQgaVR1bmVzIFN0b3JlIFJlY2VpcHQgU2lnbmluZzEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApc+B/SWigVvWh+0j2jMcjuIjwKXEJss9xp/sSg1Vhv+kAteXyjlUbX1/slQYncQsUnGOZHuCzom6SdYI5bSIcc8/W0YuxsQduAOpWKIEPiF41du30I4SjYNMWypoN5PC8r0exNKhDEpYUqsS4+3dH5gVkDUtwswSyo1IgfdYeFRr6IwxNh9KBgxHVPM3kLiykol9X6SFSuHAnOC6pLuCl2P0K5PB/T5vysH1PKmPUhrAJQp2Dt7+mf7/wmv1W16sc1FJCFaJzEOQzI6BAtCgl7ZcsaFpaYeQEGgmJjm4HRBzsApdxXPQ33Y72C3ZiB7j7AfP4o7Q0/omVYHv4gNJIwIDAQABo4IB1zCCAdMwPwYIKwYBBQUHAQEEMzAxMC8GCCsGAQUFBzABhiNodHRwOi8vb2NzcC5hcHBsZS5jb20vb2NzcDAzLXd3ZHIwNDAdBgNVHQ4EFgQUkaSc/MR2t5+givRN9Y82Xe0rBIUwDAYDVR0TAQH/BAIwADAfBgNVHSMEGDAWgBSIJxcJqbYYYIvs67r2R1nFUlSjtzCCAR4GA1UdIASCARUwggERMIIBDQYKKoZIhvdjZAUGATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMA4GA1UdDwEB/wQEAwIHgDAQBgoqhkiG92NkBgsBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEADaYb0y4941srB25ClmzT6IxDMIJf4FzRjb69D70a/CWS24yFw4BZ3+Pi1y4FFKwN27a4/vw1LnzLrRdrjn8f5He5sWeVtBNephmGdvhaIJXnY4wPc/zo7cYfrpn4ZUhcoOAoOsAQNy25oAQ5H3O5yAX98t5/GioqbisB/KAgXNnrfSemM/j1mOC+RNuxTGf8bgpPyeIGqNKX86eOa1GiWoR1ZdEWBGLjwV/1CKnPaNmSAMnBjLP4jQBkulhgwHyvj3XKablbKtYdaG6YQvVMpzcZm8w7HHoZQ/Ojbb9IYAYMNpIr7N4YtRHaLSPQjvygaZwXG56AezlHRTBhL8cTqDCCBCIwggMKoAMCAQICCAHevMQ5baAQMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0xMzAyMDcyMTQ4NDdaFw0yMzAyMDcyMTQ4NDdaMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyjhUpstWqsgkOUjpjO7sX7h/JpG8NFN6znxjgGF3ZF6lByO2Of5QLRVWWHAtfsRuwUqFPi/w3oQaoVfJr3sY/2r6FRJJFQgZrKrbKjLtlmNoUhU9jIrsv2sYleADrAF9lwVnzg6FlTdq7Qm2rmfNUWSfxlzRvFduZzWAdjakh4FuOI/YKxVOeyXYWr9Og8GN0pPVGnG1YJydM05V+RJYDIa4Fg3B5XdFjVBIuist5JSF4ejEncZopbCj/Gd+cLoCWUt3QpE5ufXN4UzvwDtIjKblIV39amq7pxY1YNLmrfNGKcnow4vpecBqYWcVsvD95Wi8Yl9uz5nd7xtj/pJlqwIDAQABo4GmMIGjMB0GA1UdDgQWBBSIJxcJqbYYYIvs67r2R1nFUlSjtzAPBgNVHRMBAf8EBTADAQH/MB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMC4GA1UdHwQnMCUwI6AhoB+GHWh0dHA6Ly9jcmwuYXBwbGUuY29tL3Jvb3QuY3JsMA4GA1UdDwEB/wQEAwIBhjAQBgoqhkiG92NkBgIBBAIFADANBgkqhkiG9w0BAQUFAAOCAQEAT8/vWb4s9bJsL4/uE4cy6AU1qG6LfclpDLnZF7x3LNRn4v2abTpZXN+DAb2yriphcrGvzcNFMI+jgw3OHUe08ZOKo3SbpMOYcoc7Pq9FC5JUuTK7kBhTawpOELbZHVBsIYAKiU5XjGtbPD2m/d73DSMdC0omhz+6kZJMpBkSGW1X9XpYh3toiuSGjErr4kkUqqXdVQCprrtLMK7hoLG8KYDmCXflvjSiAcp/3OIK5ju4u+y6YpXzBWNBgs0POx1MlaTbq/nJlelP5E3nJpmB6bz5tCnSAXpm4S6M9iGKxfh44YGuv9OQnamt86/9OBqWZzAcUaVc7HGKgrRsDwwVHzCCBLswggOjoAMCAQICAQIwDQYJKoZIhvcNAQEFBQAwYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMB4XDTA2MDQyNTIxNDAzNloXDTM1MDIwOTIxNDAzNlowYjELMAkGA1UEBhMCVVMxEzARBgNVBAoTCkFwcGxlIEluYy4xJjAkBgNVBAsTHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRYwFAYDVQQDEw1BcHBsZSBSb290IENBMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5JGpCR+R2x5HUOsF7V55hC3rNqJXTFXsixmJ3vlLbPUHqyIwAugYPvhQCdN/QaiY+dHKZpwkaxHQo7vkGyrDH5WeegykR4tb1BY3M8vED03OFGnRyRly9V0O1X9fm/IlA7pVj01dDfFkNSMVSxVZHbOU9/acns9QusFYUGePCLQg98usLCBvcLY/ATCMt0PPD5098ytJKBrI/s61uQ7ZXhzWyz21Oq30Dw4AkguxIRYudNU8DdtiFqujcZJHU1XBry9Bs/j743DN5qNMRX4fTGtQlkGJxHRiCxCDQYczioGxMFjsWgQyjGizjx3eZXP/Z15lvEnYdp8zFGWhd5TJLQIDAQABo4IBejCCAXYwDgYDVR0PAQH/BAQDAgEGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFCvQaUeUdgn+9GuNLkCm90dNfwheMB8GA1UdIwQYMBaAFCvQaUeUdgn+9GuNLkCm90dNfwheMIIBEQYDVR0gBIIBCDCCAQQwggEABgkqhkiG92NkBQEwgfIwKgYIKwYBBQUHAgEWHmh0dHBzOi8vd3d3LmFwcGxlLmNvbS9hcHBsZWNhLzCBwwYIKwYBBQUHAgIwgbYagbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjANBgkqhkiG9w0BAQUFAAOCAQEAXDaZTC14t+2Mm9zzd5vydtJ3ME/BH4WDhRuZPUc38qmbQI4s1LGQEti+9HOb7tJkD8t5TzTYoj75eP9ryAfsfTmDi1Mg0zjEsb+aTwpr/yv8WacFCXwXQFYRHnTTt4sjO0ej1W8k4uvRt3DfD0XhJ8rxbXjt57UXF6jcfiI1yiXV2Q/Wa9SiJCMR96Gsj3OBYMYbWwkvkrL4REjwYDieFfU9JmcgijNq9w2Cz97roy/5U2pbZMBjM3f3OgcsVuvaDyEO2rpzGU+12TZ/wYdV2aeZuTJC+9jVcZ5+oVK3G72TQiQSKscPHbZNnF5jyEuAF1CqitXa5PzQCQc3sHV1ITGCAcswggHHAgEBMIGjMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5AggO61eH554JjTAJBgUrDgMCGgUAMA0GCSqGSIb3DQEBAQUABIIBAB6s4CoGISdXaRHB+VS/+z7rGid1l+6VpAJLGk7GnWeoptK8XiSUVvaWZEileB4OFO2M9fj2g1K+YHDBUx5WDXq2hXwRc7lwh+3FueMw9WKmeen0wW0PkEEsjD6VPNMbTD8+BamLmW2KtJ2aA7mVwXfHrDPLDlQSeFd67r53Af+hsMML54LBUmcKLGPR+oe6Qs0k+l8BzBec9mByWzS6iGgrK9iCUgaEswnJhLUnxWFDO0fu0e/X7xhHRiWaHgqf503fk1xWnldia5qaDV+lcaWcL1+tttjpz3IvU6A23cPQXHLf/IULjj4rbX/sr5Iwt3HovDGuo8ZEYvgv6wrTIwc=";
        thridPartyNotifyVo.setAppleReceipt(str);
        return thridPartyPayCallService.thridPartyPayNotify("-1111",thridPartyNotifyVo);
    }
}
