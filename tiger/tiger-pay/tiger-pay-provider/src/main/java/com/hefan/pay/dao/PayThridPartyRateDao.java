package com.hefan.pay.dao;

import com.hefan.common.orm.dao.CommonDaoImpl;
import org.apache.commons.lang.StringUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

/**
 * Created by lxw on 2016/10/18.
 */
@Repository
public class PayThridPartyRateDao extends CommonDaoImpl {

    /**
     * 根据支付第三方支付类型 费率
     * @param thridType
     * @return
     */
    public String getPayThridPartRateByThridType(int thridType) {
        String  sql = "select rate_val as rateVal from pay_thrid_party_rate where thrid_type=?";
        String rateVal = "0";
        try {
            rateVal = getJdbcTemplate().queryForObject(sql, new Object[] {thridType}, String.class);
            if(StringUtils.isBlank(rateVal)) {
                rateVal = "0";
            }
        }catch (EmptyResultDataAccessException e){
            return  "0";
        }catch (NullPointerException e){
            return "0";
        }
        return rateVal;
    }
}
