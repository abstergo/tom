package com.hefan.pay.service;

import com.cat.common.util.BigDecimalCalUtil;
import com.hefan.pay.dao.PayThridPartyRateDao;
import com.hefan.pay.itf.PayThridPartyRateCalService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * Created by lxw on 2016/10/18.
 */
@Component("payThridPartyRateCalService")
public class PayThridPartyRateCalServiceImpl implements PayThridPartyRateCalService {

    @Resource
    private PayThridPartyRateDao payThridPartyRateDao;


    @Override
    @Transactional(readOnly = true)
    public String getPayThridRateVal(int thridType) {
        return payThridPartyRateDao.getPayThridPartRateByThridType(thridType);
    }

    @Override
    @Transactional(readOnly = true)
    public BigDecimal platformCostCal(int thridType, BigDecimal payAmount) {
        //获取第三方支付平台费率
        String rateVal = getPayThridRateVal(thridType);
        String feeVal  = BigDecimalCalUtil.div(BigDecimalCalUtil.mul(String.valueOf(payAmount),rateVal),"100");
        return new BigDecimal(feeVal);
    }
}
