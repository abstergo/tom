package com.hefan.pay.service;

import com.cat.common.meta.PaymentTypeEnum;
import com.cat.common.meta.TradeLogPayStatusEnum;
import com.cat.common.util.BigDecimalCalUtil;
import com.hefan.pay.bean.CurrencyExchange;
import com.hefan.pay.bean.TradeLog;
import com.hefan.pay.dao.TradeLogDao;
import com.hefan.pay.itf.CurrentRechargeService;
import com.hefan.pay.itf.PayThridPartyRateCalService;
import com.hefan.pay.itf.TradeLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * Created by lxw on 2016/10/15.
 */
@Component("tradeLogService")
public class TradeLogServiceImpl implements TradeLogService {

	@Resource
	private TradeLogDao tradeLogPayDao;

	@Resource
	private PayThridPartyRateCalService payThridPartyRateCalService;

	@Resource
	private CurrentRechargeService currentRechargeService;

	@Override
	@Transactional(readOnly = true)
	public TradeLog getTradeLogByOrderId(String orderId) {
		return tradeLogPayDao.getTradeLogInfoByOrderId(orderId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateTradeLogPayStatus(String orderId, int payStatus, String thirdPartyTradeNo, BigDecimal platformCost,
			long payBeforeFanpiao, long payAfterFanpiao) {
		return tradeLogPayDao.updateTradeLogPayStatus(orderId, payStatus, thirdPartyTradeNo, platformCost,
				payBeforeFanpiao, payAfterFanpiao);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public long initTradeLogInfo(TradeLog tradeLog) {
		return tradeLogPayDao.saveTradeLog(tradeLog);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int thridPartyNotfiySuccessOprate(TradeLog tradeLog) {
		BigDecimal platformCost = new BigDecimal(0);

		if (tradeLog.getPayStatus() == TradeLogPayStatusEnum.PAY_SUCCESS.getPayStatus()) {
			// 获取第三方支付类型
			int thridType = PaymentTypeEnum.getPayAccountByPaymentType(tradeLog.getPaymentType());
			if (thridType != 0) {
				// 计算第三方费率
				platformCost = payThridPartyRateCalService.platformCostCal(thridType, tradeLog.getPayAmount());
			}
		}
		// 处理失败更新充值订单状态为失败
		return updateTradeLogPayStatus(tradeLog.getOrderId(), tradeLog.getPayStatus(),
				StringUtils.isBlank(tradeLog.getThirdPartyTradeNo()) ? "" : tradeLog.getThirdPartyTradeNo(),
				platformCost, tradeLog.getPayBeforeFanpiao(), tradeLog.getPayAfterFanpiao());
	}

	@Override
	public int checkOrderId(String orderId) {
		return tradeLogPayDao.checkOrderId(orderId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public long saveAndCalTradeLog(TradeLog tradeLog) {
		//获取反算比例
		CurrencyExchange fpExchangeRmb = currentRechargeService.getCurrencyexchange(7);
		tradeLog.setRewardFanpiaoAmount(new BigDecimal(0));
		if(fpExchangeRmb != null) {
			tradeLog.setRewardFanpiaoAmount(BigDecimalCalUtil.div(
					BigDecimalCalUtil.mul(new BigDecimal(tradeLog.getRewardFanpiao()),
							new BigDecimal(fpExchangeRmb.getBeExchangeRatio())),
					new BigDecimal(fpExchangeRmb.getExchangeRatio())));
		}
		return initTradeLogInfo(tradeLog);
	}

}
