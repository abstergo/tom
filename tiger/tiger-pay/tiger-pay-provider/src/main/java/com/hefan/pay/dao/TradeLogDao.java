package com.hefan.pay.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.hefan.common.orm.dao.CommonDaoImpl;
import com.hefan.pay.bean.TradeLog;

/**
 * Created by lxw on 2016/10/15.
 */
@Repository("tradeLogPayDao")
public class TradeLogDao extends CommonDaoImpl {

	/**
	 * 根据orderId获取trade_log记录
	 * 
	 * @param orderId
	 * @return
	 */
	public TradeLog getTradeLogInfoByOrderId(String orderId) {
		String sql = "select * from trade_log where order_id='" + orderId + "'";
		System.out.println(sql.toString());
		List<TradeLog> list = getJdbcTemplate().query(sql.toString(), new BeanPropertyRowMapper(TradeLog.class));
		return list.get(0);
	}

	/**
	 * 更新充值日志中状态和第三方订单号
	 * 
	 * @param oerderId
	 * @param payStatus
	 * @param thirdPartyTradeNo
	 * @param platformCost
	 * @param payBeforeFanpiao
	 * @param payAfterFanpiao
	 * @return
	 */
	public int updateTradeLogPayStatus(String oerderId, int payStatus, String thirdPartyTradeNo,
			BigDecimal platformCost, long payBeforeFanpiao, long payAfterFanpiao) {
		String sql = "update trade_log set pay_status=?,third_party_trade_no=?,platform_cost=?,pay_before_fanpiao=?,pay_after_fanpiao=?,pay_notify_date=now() where order_id=?";
		return this.update(sql, new Object[] { payStatus, thirdPartyTradeNo, platformCost, payBeforeFanpiao,
				payAfterFanpiao, oerderId });
	}

	/**
	 * 初始化充值订单表
	 * 
	 * @param tradeLog
	 * @return
	 */
	public long saveTradeLog(TradeLog tradeLog) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("order_id", tradeLog.getOrderId());
		parameters.put("user_id", tradeLog.getUserId());
		parameters.put("user_type", tradeLog.getUserType());
		parameters.put("nick_name", StringUtils.isBlank(tradeLog.getNickName()) ? "" : tradeLog.getNickName());
		parameters.put("pay_amount", tradeLog.getPayAmount() == null ? 0 : tradeLog.getPayAmount());
		parameters.put("income", tradeLog.getIncome());
		parameters.put("income_amount", tradeLog.getIncomeAmount());
		parameters.put("exchange_amount", tradeLog.getExchangeAmount() == null ? 0 : tradeLog.getExchangeAmount());
		parameters.put("channel_fee", tradeLog.getChannelFee() == null ? 0 : tradeLog.getChannelFee());
		parameters.put("reward_fanpiao", tradeLog.getRewardFanpiao());
		parameters.put("reward_fanpiao_amount",
				tradeLog.getRewardFanpiaoAmount() == null ? 0 : tradeLog.getRewardFanpiaoAmount());
		parameters.put("account_type", tradeLog.getAccountType());
		parameters.put("pay_source", tradeLog.getPaySource());
		parameters.put("pay_before_fanpiao", tradeLog.getPayBeforeFanpiao());
		parameters.put("pay_after_fanpiao", tradeLog.getPayAfterFanpiao());
		parameters.put("channel_fee", tradeLog.getChannelFee() == null ? 0 : tradeLog.getChannelFee());
		parameters.put("pay_status", tradeLog.getPayStatus());
		parameters.put("platform_cost", tradeLog.getPlatformCost() == null ? 0 : tradeLog.getPlatformCost());
		parameters.put("payment_extend",
				StringUtils.isBlank(tradeLog.getPaymentExtend()) ? "" : tradeLog.getPaymentExtend());
		parameters.put("payment_type", tradeLog.getPaymentType());
		parameters.put("pay_notify_date",tradeLog.getPayNotifyDate() == null ? new Date() : tradeLog.getPayNotifyDate());
		parameters.put("third_party_trade_no",tradeLog.getThirdPartyTradeNo() == null ? "" : tradeLog.getThirdPartyTradeNo());
		parameters.put("ios_product_code",tradeLog.getIosProductCode() == null ? "" : tradeLog.getIosProductCode());
		String[] columns = parameters.keySet().toArray(new String[] {});
		SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(getJdbcTemplate().getDataSource())
				.usingColumns(columns).withTableName("trade_log").usingGeneratedKeyColumns("id");
		Number id = simpleJdbcInsert.executeAndReturnKey(parameters);
		return id.longValue();
	}

	/**
	 * 更新充值日志中状态和第三方订单号
	 * 
	 * @param orderId
	 * @return
	 */
	public int checkOrderId(String orderId) {
		String sql = " select count(*) count from trade_log where order_id= '" + orderId + "'";
		Map map = getJdbcTemplate().queryForMap(sql.toString());
		return Integer.valueOf(map.get("count").toString());
	}
}
