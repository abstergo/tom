package com.hefan.pay.service;

import com.cat.common.meta.PaymentTypeEnum;
import com.hefan.pay.itf.PayThridPartyFactoryService;
import com.hefan.pay.itf.ThridPartyPayCallService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 根据第三方支付类型 获取支付接口实现对象
 * Created by lxw on 2016/10/13.
 */
@Component("payThridPartyFactoryService")
public class PayThridPartyFactoryServiceImpl implements PayThridPartyFactoryService {

    @Resource
    private ThridPartyPayCallService alipayAppPayService;
    @Resource
    private ThridPartyPayCallService alipayScanCodePayService;
    @Resource
    private ThridPartyPayCallService wechatPayService;
    @Resource
    private ThridPartyPayCallService appleAppStorePayService;
    @Resource
    ThridPartyPayCallService alipayWapPayService;

    @Override
    public ThridPartyPayCallService payThridPartyFactory(int payMonthType) {
        ThridPartyPayCallService thridPartyPayCallService = null;
        switch (payMonthType) {
            case 1 :
                //ios 苹果内购
                thridPartyPayCallService = appleAppStorePayService;
                break;
            case 2 : //android 微信支付
            case 4 : //PC 微信支付
            case 6 : //微信公共账号 微信支付
                //返回wechatPayService对象
                thridPartyPayCallService = wechatPayService;
                break;
            case 3 : //android 支付宝支付 APP支付
                thridPartyPayCallService = alipayAppPayService;
                break;
            case 5 :
                //PC 支付宝支付 扫码支付
                thridPartyPayCallService = alipayScanCodePayService;
                break;
            case 7:
                //html5 支付宝网页支付
                thridPartyPayCallService = alipayWapPayService;
                break;
            default:
                thridPartyPayCallService = null;
                break;
        }
        
        return thridPartyPayCallService;
    }

}
