package com.hefan.pay.service;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.PaymentTypeEnum;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.util.GlobalConstants;
import com.hefan.common.pay.alipay.AlipayCore;
import com.hefan.common.pay.alipay.AlipayNotify;
import com.hefan.common.pay.alipay.Signature;
import com.hefan.common.pay.alipay.UtilDate;
import com.hefan.common.util.PayPropertiesUtils;
import com.hefan.pay.bean.PayExtendVo;
import com.hefan.pay.bean.ThridPartyNotifyVo;
import com.hefan.pay.bean.TradeLog;
import com.hefan.pay.dao.PayReqLogDao;
import com.hefan.pay.itf.ThridPartyPayCallService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 支付宝 app支付
 * Created by lxw on 2016/10/13.
 */
@Component("alipayScanCodePayService")
public class AlipayScanCodePayServiceImpl implements ThridPartyPayCallService {

    private Logger logger = LoggerFactory.getLogger(AlipayScanCodePayServiceImpl.class);

    @Resource
    private PayReqLogDao payReqLogDao;

    private static String RSA_PRIVATE_KEY = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJusnY2S8e8y3wcr\n" +
            "/FDXVjFQjOZVGH28jSfeG1OAyH4SKTXpglufVygYewCuHFiC2NI75KIvD3/sp5DI\n" +
            "lgD7R5JY0U0CbBmrBqkT5gesDQxUFoYZOKR5h03UAUN7VCmAe+1RsQsmt/xdHy2v\n" +
            "3K3SFmdH9Do726AVzrUjWL9SYHFFAgMBAAECgYAU9JmI0z0KC/kFyCAA6dvKa6Nr\n" +
            "5gyT8Gu38CgRh4Z1ohA2F6bamopq9VCpeMaMC6EQO8u9IUSe3cZ4sOewXiL2FItp\n" +
            "4kZXrg1W3apGsx6bby0C3nfin0QZtU4GwTi6p8fnPVl5b4ZMkaWZXCDdpio3w9bS\n" +
            "2DKZjP5Ae4WiA33RCQJBAMjsmH26Ni49QkV1N7ZcmDnx6p/bXYqLqZ1BrduObA8D\n" +
            "aAkbLlcWy6AuyQ3eyu5/P6ZeaD9mlC6NMs9VIrZOP28CQQDGWLVIJ+/Hn+Dxz1f1\n" +
            "EivaSifhSUfjAaYldgzadmhJ359OQJV0di61VNNRFP3JxJl/YnAbSnRwdd+ZiewW\n" +
            "XACLAkA15zNp14EdmpWi6LWPmPvF397gEAviVWGlrK8lzemzhWhtuPsPpi/uw15w\n" +
            "+CpdHYpH6d/x0mVCr+LHuMFdQ1TFAkBsEpesGj6XObD6cKyPVvhX94HlSeWGdjaO\n" +
            "QNVeD5hhcBxAKgaFL4Phv7dofZSO5LVyaDOHWzk4sbFE2pat1DFjAkEAo1TonGNv\n" +
            "Fj+hWniVv4U+0j8TSmxjE9Hti+4SQ7AhGZs2SzzIRHFe4aU8HikxRsyTgeSduDRb\n" +
            "/7naA9TUREzq3Q==";

    @Override
    public ResultBean thridPartyPayCall(PaymentTypeEnum paymentTypeEnum, TradeLog tradeLog, PayExtendVo payExtendVo) {
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS,null);
        try {
            Map<String, String> paramMap = new HashMap<String, String>();
            paramMap.put("service",PayPropertiesUtils.getString("alipay.scanCodePay.service"));
            paramMap.put("partner",PayPropertiesUtils.getString("alipay.scanCodePay.partner"));
            paramMap.put("_input_charset",PayPropertiesUtils.getString("alipay.inputCharset"));
            paramMap.put("notify_url",PayPropertiesUtils.getString("alipay.scanCodePay.notifyUrl"));
            if(StringUtils.isBlank(payExtendVo.getReturnUrl())) {
                if(!StringUtils.isBlank(paymentTypeEnum.getReturnUrlPropName())) {
                    paramMap.put("return_url",PayPropertiesUtils.getString(paymentTypeEnum.getReturnUrlPropName()));
                }
            } else {
                paramMap.put("return_url",payExtendVo.getReturnUrl());
            }
            paramMap.put("out_trade_no",payExtendVo.getOrderId());
            paramMap.put("subject", GlobalConstants.PAY_PRODUCT_NAME);
            paramMap.put("payment_type", PayPropertiesUtils.getString("alipay.scanCodePay.paymentType"));
            paramMap.put("total_fee", String.valueOf(payExtendVo.getPayAmount()));
            paramMap.put("seller_id", PayPropertiesUtils.getString("alipay.scanCodePay.partner"));
            paramMap.put("seller_email", PayPropertiesUtils.getString("alipay.scanCodePay.sellerEmail"));
            paramMap.put("body", GlobalConstants.PAY_PRODUCT_NAME);
            //show_url 收银台页面上，商品展示的超链接。可空
            //paymethod 取值范围： creditPay（信用支付） directPay（余额支付） 如果不设置，默认识别为余额支付。
            //enable_paymethod 支付渠道 用于控制收银台支付渠道显示，该值的取值范围请参见支付渠道。 可支持多种支付渠道显示，以“^”分隔。可空
            //anti_phishing_key 防钓鱼时间戳
            //exter_invoke_ip 防钓鱼保护时必填写
            //extra_common_param 公用回传参数 可控
            //token  如果开通了快捷登录产品，则需要填写；如果没有开通，则为空。
            //need_buyer_realnamed 是否需要买家实名认证 T表示需要买家实名认证； 不传或者传其它值表示不需要买家实名认证。可空
            //hb_fq_param 花呗分期参数
            paramMap.put("it_b_pay", PayPropertiesUtils.getString("alipay.scanCodePay.itBPay"));
            paramMap.put("qr_pay_mode", PayPropertiesUtils.getString("alipay.scanCodePay.qrPayMode"));
            //商户自定二维码宽度 当qr_pay_mode=4时，该参数生效。
            if(StringUtils.isNotBlank(payExtendVo.getQrcodeWidth())) {
                paramMap.put("qrcode_width",payExtendVo.getQrcodeWidth());
            }
            paramMap.put("goods_type",PayPropertiesUtils.getString("alipay.scanCodePay.goodsType"));
            logger.info("alipayScanCodePayService----thridPartyPayCall-----");
            //去除空值
            Map<String,String> signMap = AlipayCore.paraAppFilter(paramMap);
            //生成签名
            String sign = Signature.buildRequestMysign(signMap, PayPropertiesUtils.getString("alipay.scanCodePay.signType"), PayPropertiesUtils.getString("alipay.scanCodePay.md5Key"));
            signMap.put("sign",sign);
            logger.info("alipayScanCodePayService----thridPartyPayCall-----sign="+sign);
            //获取业务参数串
            String payReqStr = PayPropertiesUtils.getString("alipay.scanCodePay.requestUrl") + AlipayCore.createLinkString(signMap);
            logger.info("alipayScanCodePayService----thridPartyPayCall-----payReqStr=" + payReqStr);
            Map returnMap = new HashMap();
            returnMap.put("scanCodeUrl",payReqStr);
            returnMap.put("orderId",payExtendVo.getOrderId());
            returnMap.put("payAmount",tradeLog.getPayAmount());
            returnMap.put("income",tradeLog.getIncome());
            returnMap.put("rewardFanpiao",tradeLog.getRewardFanpiao());
            returnMap.put("incomeAmount",tradeLog.getIncomeAmount());
            resultBean.setData(returnMap);
            //记录操作日志
            this.payReqLogDao.savePayReqLog(payExtendVo.getOrderId(), payExtendVo.getUserId(), PayPropertiesUtils.getString("alipay.scanCodePay.service"), paymentTypeEnum.getPaymentType(), payReqStr, "", resultBean.getCode());
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResultBean(ResultCode.UNSUCCESS,null);
        }
        return resultBean;
    }

    @Override
    public ResultBean thridPartyPayNotify(String orderId, ThridPartyNotifyVo thridPartyNotifyVo) {
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS, NOTFIY_EXCEPTION_IS_HANDLER);
        String tradeStatus = String.valueOf(thridPartyNotifyVo.getAlipayTradeStatus());
        try {
            Map<String, String> checkParamsMap = new HashMap<String,String>();
            Map requestParams = thridPartyNotifyVo.getAliPayRequestParamsMap();
            for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
                String name = (String) iter.next();
                String[] values = (String[]) requestParams.get(name);
                String valueStr = "";
                for (int i = 0; i < values.length; i++) {
                    valueStr = (i == values.length - 1) ? valueStr + values[i]
                            : valueStr + values[i] + ",";
                }
                //乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
                //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
                checkParamsMap.put(name, valueStr);
            }

            if (AlipayNotify.verify(checkParamsMap,PayPropertiesUtils.getString("alipay.scanCodePay.signType"),PayPropertiesUtils.getString("alipay.scanCodePay.md5Key"))) { //验证响应请求正确
                if(!"TRADE_FINISHED".equals(tradeStatus) && !"TRADE_SUCCESS".equals(tradeStatus)) { //支付失败
                    resultBean = new ResultBean(ResultCode.PayAlipayNotifyIsPayFail.get_code(),ResultCode.PayAlipayNotifyIsPayFail.getMsg()+";返回误码："+tradeStatus, NOTFIY_EXCEPTION_IS_HANDLER);
                }
            } else {
                resultBean = new ResultBean(ResultCode.UNSUCCESS.get_code(),ResultCode.PayAlipayNotifyIsNotValidUrl.getMsg()+";返回码："+tradeStatus, NOTFIY_EXCEPTION_IS_NOT_HANDLER);
            }
            payReqLogDao.updatePayReqNotifyInfo(orderId,resultBean.getCode(),resultBean.getMsg(), JSON.toJSONString(checkParamsMap));
        }catch (Exception e) {
            logger.error(e.getMessage());
            resultBean = new ResultBean(ResultCode.UNSUCCESS,NOTFIY_EXCEPTION_IS_NOT_HANDLER);
        }
        return resultBean;
    }

    @Override
    public ResultBean<ThridPartyNotifyVo> thridPartyPayNotifyParse(ThridPartyNotifyVo thridPartyNotifyVo) {
        return new ResultBean<ThridPartyNotifyVo>(ResultCode.SUCCESS, thridPartyNotifyVo);
    }
}
