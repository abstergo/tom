package com.hefan.pay.dao;

import com.cat.tiger.util.GlobalConstants;
import com.hefan.common.orm.dao.CommonDaoImpl;
import com.hefan.pay.bean.CurrencyExchange;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by lxw on 2016/10/19.
 */
@Repository("currencyExchangeDaoPay")
public class CurrencyExchangeDao extends CommonDaoImpl {

    /**
     * 根据多个类型获取兑换比例参数
     * @param types
     * @return
     */
    public List<CurrencyExchange> getListByRuleTypes(List<Integer> types) {
        StringBuilder sql  = new StringBuilder(" select * from currency_exchange where zf=0 and exchange_rule_type IN (");

        for(int i=0; i<types.size();i++) {
            if(i == 0) {
                sql.append("?");
            } else {
                sql.append(",?");
            }
        }
        sql.append(") ");
        return  this.query(sql.toString(),types.toArray(),CurrencyExchange.class);
    }
}
