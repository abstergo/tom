package com.hefan.pay.service;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.PaymentTypeEnum;
import com.cat.common.meta.ResultCode;
import com.cat.common.meta.TradeLogPayStatusEnum;
import com.cat.common.util.GuuidUtil;
import com.hefan.pay.bean.PayExtendVo;
import com.hefan.pay.bean.ThridPartyNotifyVo;
import com.hefan.pay.bean.TradeLog;
import com.hefan.pay.itf.PayOpreateHandlerService;
import com.hefan.pay.itf.PayThridPartyFactoryService;
import com.hefan.pay.itf.ThridPartyPayCallService;
import com.hefan.pay.itf.TradeLogService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.Map;

/**
 * Created by lxw on 2016/10/15.
 */
@Component("payOpreateHandlerService")
public class PayOpreateHandlerServiceImpl implements PayOpreateHandlerService {

    @Resource
    private TradeLogService tradeLogService;

    @Resource
    private PayThridPartyFactoryService payThridPartyFactoryService;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ResultBean thridPartyPayNotfiyOprater(ThridPartyNotifyVo thridPartyNotifyVo) {
        ResultBean resultBean = new ResultBean(ResultCode.SUCCESS,null);
        //根据支付方式获取处理解析处理回调通知实现类
        ThridPartyPayCallService thridPartyPayCallService = payThridPartyFactoryService.payThridPartyFactory(thridPartyNotifyVo.getPaymentType());
        if(thridPartyPayCallService == null) {
            //支付方式不存在
            return new ResultBean(ResultCode.PayMentTypeIsValid, null);
        }
        //对接收的信息进行前置解析
        ResultBean<ThridPartyNotifyVo> notifyParseBean =  thridPartyPayCallService.thridPartyPayNotifyParse(thridPartyNotifyVo);
        if(notifyParseBean.getCode() == ResultCode.UNSUCCESS.get_code()) {
            return notifyParseBean;
        }
        thridPartyNotifyVo = notifyParseBean.getData();
        if(StringUtils.isBlank(thridPartyNotifyVo.getOrderId())) { //订单号是否存在
            return new ResultBean(ResultCode.PayOrderIsValid, null);
        } else { //根据订单号是否查询充值记录
            TradeLog tradeLog = tradeLogService.getTradeLogByOrderId(thridPartyNotifyVo.getOrderId());
            if(tradeLog == null) {
                return new ResultBean(ResultCode.PayOrderIsValid, null);
            }
            //检查充值信息是否已经被处理
            if(!TradeLogPayStatusEnum.checkPayNotifyIsHandler(tradeLog.getPayStatus())) {
                return new ResultBean(ResultCode.PayOrderIsNotfiyHandler, null);
            }
            //进行
            thridPartyNotifyVo.setUserId(tradeLog.getUserId());
            thridPartyNotifyVo.setIosProductCode(tradeLog.getIosProductCode());
            resultBean = thridPartyPayCallService.thridPartyPayNotify(tradeLog.getOrderId(),thridPartyNotifyVo);
            if((int)resultBean.getData() == thridPartyPayCallService.NOTFIY_EXCEPTION_IS_HANDLER) {
                //--------------更新充值订单支付信息-----
                int payStatus = TradeLogPayStatusEnum.PAY_ERROR.getPayStatus(); //支付失败
                if(resultBean.getCode() == ResultCode.SUCCESS.get_code()) { //支付通知成功
                    payStatus = TradeLogPayStatusEnum.PAY_SUCCESS.getPayStatus();
                }
                tradeLog.setPayStatus(payStatus);
                tradeLog.setThirdPartyTradeNo(thridPartyNotifyVo.getThirdPartyTradeNo());
                //回传充值记录对象
                resultBean.setData(tradeLog);

            } else {
                resultBean.setData(null);
                return resultBean;
            }
        }
        return resultBean;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ResultBean payOpreataHandler(PaymentTypeEnum paymentTypeEnum, TradeLog tradeLog, PayExtendVo payExtendVo) {
        ResultBean resultBean = null;
        //根据支付类型获取支付实现类
        ThridPartyPayCallService thridPartyPayCallService = payThridPartyFactoryService.payThridPartyFactory(paymentTypeEnum.getPaymentType());
        if(thridPartyPayCallService == null) {
            //支付方式不存在
            return new ResultBean(ResultCode.PayMentTypeIsValid, null);
        } else {
            String orderId = GuuidUtil.getUuid();
            tradeLog.setOrderId(orderId);
            payExtendVo.setOrderId(orderId);
            payExtendVo.setPaymentExtend(tradeLog.getPaymentExtend());
            resultBean = thridPartyPayCallService.thridPartyPayCall(paymentTypeEnum, tradeLog,payExtendVo);
            if(resultBean.getCode() == ResultCode.SUCCESS.get_code())  {
                tradeLog.setPayStatus(TradeLogPayStatusEnum.PAY_NOT_PAY.getPayStatus());
                //进行保存充值订单
                tradeLogService.initTradeLogInfo(tradeLog);
            }
            return resultBean;
        }
    }



    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ResultBean payOpreataHandlerTest(PaymentTypeEnum paymentTypeEnum, TradeLog tradeLog, PayExtendVo payExtendVo) {
        String orderId = GuuidUtil.getUuid();
        tradeLog.setOrderId(orderId);
        payExtendVo.setOrderId(orderId);
        tradeLog.setPaySource(TradeLogPayStatusEnum.PAY_NOT_PAY.getPayStatus());
        //进行保存充值订单
        tradeLogService.initTradeLogInfo(tradeLog);
        return new ResultBean(ResultCode.SUCCESS, null);
    }
}
