package com.hefan.monitor.listener;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.TopicRegistryDev;
import com.hefan.common.ons.TopicRegistryTest;
import com.hefan.common.ons.listener.GLLMessageListener;
import com.hefan.common.util.MapUtils;
import com.hefan.monitor.bean.MoniProfileDataVo;
import com.hefan.monitor.bean.MoniUserDataVo;
import com.hefan.monitor.bean.UserChange;
import com.hefan.monitor.bean.UserChangeLog;
import com.hefan.monitor.bean.UserProfileLog;
import com.hefan.monitor.itf.MonitorDataService;

@Component
public class MonitorDataListener implements GLLMessageListener {

	Logger logger = LoggerFactory.getLogger(MonitorDataListener.class);
	
	@Resource
    MonitorDataService monitorDataService;
	
	@Override
	public TopicRegistry getTopicRegistry() {
		// TODO Auto-generated method stub
		return TopicRegistry.HEFAN_MONI_DATA;
	}

	@Override
	public TopicRegistryTest getTopicRegistryTest() {
		// TODO Auto-generated method stub
		return TopicRegistryTest.HEFAN_MONI_DATA_TEST;
	}

	@Override
	public TopicRegistryDev getTopicRegistryDev() {
		// TODO Auto-generated method stub
		return TopicRegistryDev.HEFAN_MONI_DATA_DEV;
	}
	
	@Override
	public Action consume(Message message, ConsumeContext context) {
		// TODO Auto-generated method stub
		String realTopic = message.getTag();
		logger.info("topic: {}, realTopic : {}, msg_id , msg_body: {} , tag : {}", message.getTopic(), realTopic, "监控资料队列开始",message.getTag());
		try {
			Map map = new ObjectMapper().readValue(message.getBody(), Map.class);
			String type = MapUtils.getStrValue(map, "type", "");
			String monitorData = MapUtils.getStrValue(map, "monitorData", "");
			if(StringUtils.isBlank(type)){
				// 类型为空丢弃
				logger.info("监控资料队列处理类型为空");
				return Action.CommitMessage;
			}
			if(type.equals("1")){
				MoniUserDataVo vo = JSON.parseObject(monitorData, MoniUserDataVo.class);
				this.addUserInterface(vo);
			}else if(type.equals("2")){
				MoniProfileDataVo vo = JSON.parseObject(monitorData, MoniProfileDataVo.class);
				this.updatePersonalProfile(vo);
			}
			logger.info("MonitorDataListener write ok!");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("监控资料队列处理失败"+e.getMessage());
			// 失败丢弃
			return Action.CommitMessage;
		}
		logger.info("MonitorDataListener complete!");
		return Action.CommitMessage;
	}
	/**
	 * 用户资料变动
	 * @Title: addUserInterface   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param vo      
	 * @return: void
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年12月5日 下午5:08:27
	 */
	private void addUserInterface(MoniUserDataVo vo){
		if(StringUtils.isNotBlank(vo.getHeadImg())){
			//头像变更
			UserChange uc = new UserChange();
			uc.setUserId(vo.getUserId());
			uc.setUserType(vo.getUserType());
			uc.setChangeType(0);
			monitorDataService.saveUserChange(uc);
			UserChangeLog ucl = new UserChangeLog();
			ucl.setUserId(vo.getUserId());
			ucl.setChangeType(0);
			ucl.setChangedValue(vo.getHeadImg());
			ucl.setOptFrom(vo.getOptFrom());
			monitorDataService.saveUserChangeLog(ucl);
		}
		if(StringUtils.isNotBlank(vo.getNickName())){
			//昵称变更
			UserChange uc = new UserChange();
			uc.setUserId(vo.getUserId());
			uc.setUserType(vo.getUserType());
			uc.setChangeType(1);
			monitorDataService.saveUserChange(uc);
			UserChangeLog ucl = new UserChangeLog();
			ucl.setUserId(vo.getUserId());
			ucl.setChangeType(1);
			ucl.setChangedValue(vo.getNickName());
			ucl.setOptFrom(vo.getOptFrom());
			monitorDataService.saveUserChangeLog(ucl);
		}
		if(StringUtils.isNotBlank(vo.getPersonSign())){
			//个性签名变更
			UserChange uc = new UserChange();
			uc.setUserId(vo.getUserId());
			uc.setUserType(vo.getUserType());
			uc.setChangeType(2);
			monitorDataService.saveUserChange(uc);
			UserChangeLog ucl = new UserChangeLog();
			ucl.setUserId(vo.getUserId());
			ucl.setChangeType(2);
			ucl.setChangedValue(vo.getPersonSign());
			ucl.setOptFrom(vo.getOptFrom());
			monitorDataService.saveUserChangeLog(ucl);
		}
	}
	
	private void updatePersonalProfile(MoniProfileDataVo vo){
		UserChange uc = new UserChange();
		uc.setUserId(vo.getUserId());
		uc.setUserType(vo.getUserType());
		uc.setChangeType(3);
		monitorDataService.saveUserChange(uc);
		UserProfileLog up = new UserProfileLog();
		up.setUserId(vo.getUserId());
		up.setInfoFile(vo.getInfoFile());
		up.setBgimg(vo.getBgimg());
		up.setProfiles(vo.getProfiles());
		up.setOptFrom(vo.getOptFrom());
		monitorDataService.saveUserProfileLog(up);
	}

}
