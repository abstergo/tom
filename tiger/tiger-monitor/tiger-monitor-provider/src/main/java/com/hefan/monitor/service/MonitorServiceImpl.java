package com.hefan.monitor.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.tiger.util.GlobalConstants;
import com.hefan.live.bean.LiveLog;
import com.hefan.live.bean.LiveRoomVo;
import com.hefan.live.bean.WarnLog;
import com.hefan.live.itf.LiveLogService;
import com.hefan.live.itf.LiveMonitorService;
import com.hefan.live.itf.LiveRoomService;
import com.hefan.live.itf.LiveWarnService;
import com.hefan.monitor.itf.MonitorService;
import com.hefan.notify.itf.ImChatroomService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WebUserService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;


/**
 * Created by sagagyq on 2016/12/5.
 */
@Component("monitorService")
public class MonitorServiceImpl implements  MonitorService {

    @Reference
    LiveWarnService liveWarnService;

    @Reference
    ImChatroomService imChatroomService;


    @Reference
    LiveRoomService liveRoomService;

    @Reference
    WebUserService webUserService;

    @Reference
    private LiveMonitorService liveMonitorService;

    @Reference
    LiveLogService liveLogService;

    @Override
    public ResultBean closeLiving(String userId, String chartRoomId, String liveUuid,String promptCopy) {
        return liveMonitorService.closeLiving(userId,chartRoomId,liveUuid,promptCopy);
    }

    @Override
    public int saveWarnLog(WarnLog warnLog) {
        return this.liveWarnService.saveWarnLog(warnLog);
    }

    @Override
    public int updateLiveHighStatus(String userId, String liveUuid, int risk, String optUserId) {
        return this.liveWarnService.updateLiveHighStatus(userId,liveUuid,risk,optUserId);
    }

    @Override
    public List getPunishTypeList() {
        return this.liveWarnService.getPunishTypeList();
    }

    @Override
    public int dealPunishDetal(String userId, String optUserId, Map map, String punishReason, String illegalPic, String liveUuid,int punishTypeFromTb, int punishDataType) {
        return this.liveWarnService.dealPunishDetal(userId, optUserId, map, punishReason, illegalPic,liveUuid, punishTypeFromTb,  punishDataType);
    }

    @Override
    public Map getPunishTypeById(long typeId,int punishTypeFromTb) {
        return this.liveWarnService.getPunishTypeById(typeId,punishTypeFromTb);
    }

    @Override
    public List<LiveRoomVo> getLivingList(Page po) {
        return this.liveRoomService.getLivingList(po);
    }

    @Override
    public LiveRoomVo findLiveRoomByIdFormonitor(String userId, String uuid) {
        return liveRoomService.findLiveRoomByIdFormonitor(userId, uuid);
    }

    @Override
    public List getHighRiskLiveList(String userId, String nickName, String startTime, String endTime) {
        return this.liveMonitorService.getHighRiskLiveList(userId, nickName, startTime, endTime);
    }

    @Override
    public List getPunishDetail() {
        return this.webUserService.getPunishDetail();
    }

    @Override
    public WebUser getWebUserInfoByUserId(String userId) {
        return webUserService.getWebUserInfoByUserId(userId);
    }

    @Override
    public ResultBean chatroomSendMsg(long roomid, String fromAccid, int msgType, int resendFlag, String attach, String ext) {
        return imChatroomService.chatroomSendMsg(roomid, GlobalConstants.HEFAN_OFFICIAL_ID, 100, 0, attach, ext);
    }

    @Override
    public LiveLog getLiveLogByUuid(String liveUuid) {
        return liveLogService.getLiveLogByUuid(liveUuid);
    }

	@Override
	public ResultBean updateLiveInfoForMonitor(String liveName, String liveImg, String liveUuid, String anthId) {
		// TODO Auto-generated method stub
		return liveLogService.updateLiveInfoForMonitor(liveName, liveImg, liveUuid, anthId);
	}
}
