package com.hefan.monitor.dao;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.monitor.bean.LiveSnapshot;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Repository
public class LiveSnapshotDao  extends BaseDaoImpl<LiveSnapshot> {
	
	public List<LiveSnapshot> getLiveSnapshotList(){
		String sql = "select a.id,a.user_id,a.live_uuid,a.img_url,"
				+"a.task_id,a.label,a.rate,a.img_no,a.status,"
				+"a.delete_flag,a.create_time from live_snapshot a where status=0 limit 100";
		List<LiveSnapshot> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<LiveSnapshot>(LiveSnapshot.class));
		return CollectionUtils.isEmpty(list) ? null : list;
	}
	
	public int updateLiveSnapshotByTaskId(String label,String rate,String taskId){
		
		String sql = "update live_snapshot set label=?,rate=?,status=1 where task_id=?";
	   	return jdbcTemplate.update(sql,label,rate,taskId);
	}

	public int delKamPictures(int days,int rate){
		String sql = "DELETE FROM live_snapshot where DATE_ADD(create_time,INTERVAL ? DAY) < NOW() " +
				" and label=0 and rate<?";
		return jdbcTemplate.update(sql,days,rate);
	}
	
}
