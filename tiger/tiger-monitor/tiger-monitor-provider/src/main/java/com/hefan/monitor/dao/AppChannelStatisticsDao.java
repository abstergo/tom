package com.hefan.monitor.dao;

import com.hefan.common.orm.dao.CommonDaoImpl;
import com.hefan.monitor.bean.AppChannelStatistics;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * IOS推广渠道信息收集
 * Created by kevin_zhang on 27/03/2017.
 */
@Repository
public class AppChannelStatisticsDao extends CommonDaoImpl {

    @Resource
    JdbcTemplate jdbcTemplate;

    /**
     * 第三方提交IOS推广渠道信息
     *
     * @param model
     * @return
     */
    public AppChannelStatistics saveForThirdParty(AppChannelStatistics model) {
        List params = new ArrayList();
        params.add(model.getAppid());
        params.add(model.getIdfa());
        params.add(model.getModel());
        params.add(model.getVersion());
        params.add(model.getCallback());
        params.add(model.getIsActivity());
        params.add(model.getAppType());
        params.add(model.getSourceType());
        StringBuilder sql = new StringBuilder("insert ignore into app_channel_statistics(appid, idfa, model, version, callback, is_activity, app_type, source_type )");
        sql.append("values ( ?, ?, ?, ?, ?, ?, ?, ? )");
        int row = this.update(sql.toString(), params);
        if (row == 0) {
            return this.queryByIdfa(model.getIdfa());
        } else {
            return null;
        }
    }

    /**
     * APP提交IOS推广渠道信息
     *
     * @param model
     * @return
     */
    public AppChannelStatistics saveForApp(AppChannelStatistics model) {
        List params = new ArrayList();
        params.add(model.getIdfa());
        params.add(model.getIsActivity());
        params.add(model.getAppType());
        params.add(model.getSourceType());
        StringBuilder sql = new StringBuilder("insert ignore into app_channel_statistics(idfa, is_activity, app_type, source_type )");
        sql.append("values ( ?, ?, ?, ? )");
        int row = this.update(sql.toString(), params);
        if (row == 0) {
            return this.queryByIdfa(model.getIdfa());
        } else {
            return null;
        }
    }

    /**
     * 查询IOS推广渠道信息
     *
     * @param idfaStr
     * @return
     */
    public AppChannelStatistics queryByIdfa(String idfaStr) {
        StringBuilder sql = new StringBuilder();
        sql.append(" select id, appid, idfa, model, version, callback, is_activity isActivity, app_type appType, ");
        sql.append(" source_type sourceType, create_time createTime, update_time updateTime from app_channel_statistics ");
        sql.append(" where idfa= ? ");
        List<AppChannelStatistics> result = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper(AppChannelStatistics.class),
                idfaStr);
        return CollectionUtils.isEmpty(result) ? null : result.get(0);
    }

    /**T
     * 更新IOS推广渠道信息
     *
     * @param model
     * @return
     */
    public int update(AppChannelStatistics model) {
        String sql = "update app_channel_statistics set appid=?, idfa=?, model=?, version=?, callback=?, is_activity=?, app_type=?, source_type=? where idfa=? ";
        return this.update(sql, new Object[]{model.getAppid(), model.getIdfa(), model.getModel(), model.getVersion(), model.getCallback(), model.getIsActivity(), model.getAppType(), model.getSourceType(), model.getIdfa()});
    }

    /**
     * 激活IOS推广渠道信息
     *
     * @param isActivity
     * @param idfaStr
     * @return
     */
    public int updateActivityStatus(int isActivity, String idfaStr) {
        String sql = "update app_channel_statistics set is_activity=? where idfa=? ";
        return this.update(sql, new Object[]{isActivity, idfaStr});
    }
}
