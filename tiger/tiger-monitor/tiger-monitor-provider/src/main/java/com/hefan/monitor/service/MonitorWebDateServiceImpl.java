package com.hefan.monitor.service;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.monitor.bean.MoniProfileDataVo;
import com.hefan.monitor.bean.MoniUserDataVo;
import com.hefan.monitor.bean.MoniWebUserDataVo;
import com.hefan.monitor.itf.MonitorDataService;
import com.hefan.monitor.itf.MonitorWebDateService;

@Component("monitorWebDateService")
public class MonitorWebDateServiceImpl  implements  MonitorWebDateService {
	
	Logger logger = LoggerFactory.getLogger(MonitorWebDateServiceImpl.class);
	
	@Resource
	MonitorDataService monitorDataService;

	@Override
	public ResultBean createUser(MoniWebUserDataVo moniWebUserDataVo) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean(ResultCode.SUCCESS.get_code(),"成功");
		logger.info("web用户信息变更:"+JSON.toJSONString(moniWebUserDataVo));
		try {
			//校验
			if(moniWebUserDataVo == null){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("未接收到参数");
				return res;
			}
			if(StringUtils.isBlank(moniWebUserDataVo.getUserId())){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("盒饭ID不能为空");
				return res;
			}
			if(moniWebUserDataVo.getUserType() == null || moniWebUserDataVo.getUserType()<=0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("用户类型不能为空");
				return res;
			}
			if(StringUtils.isNotBlank(moniWebUserDataVo.getNickName()) ||
					StringUtils.isNotBlank(moniWebUserDataVo.getHeadImg()) ||
					StringUtils.isNotBlank(moniWebUserDataVo.getPersonSign())){
				MoniUserDataVo vo = new MoniUserDataVo();
				vo.setUserId(moniWebUserDataVo.getUserId());
				vo.setUserType(moniWebUserDataVo.getUserType());
				vo.setOptFrom(1);
				if(StringUtils.isNotBlank(moniWebUserDataVo.getNickName())){
					vo.setNickName(moniWebUserDataVo.getNickName());
				}
				if(StringUtils.isNotBlank(moniWebUserDataVo.getHeadImg())){
					vo.setNickName(moniWebUserDataVo.getHeadImg());
				}
				if(StringUtils.isNotBlank(moniWebUserDataVo.getPersonSign())){
					vo.setNickName(moniWebUserDataVo.getPersonSign());
				}
				this.monitorDataService.sendMessToQueue("1", JSON.toJSONString(vo));
			}
			if(StringUtils.isNotBlank(moniWebUserDataVo.getInfoFile())
					|| StringUtils.isNotBlank(moniWebUserDataVo.getBgimg())
					|| StringUtils.isNotBlank(moniWebUserDataVo.getProfiles())){
				MoniProfileDataVo vpo = new MoniProfileDataVo();
				vpo.setUserId(moniWebUserDataVo.getUserId());
				vpo.setUserType(moniWebUserDataVo.getUserType());
				vpo.setOptFrom(1);
				if(StringUtils.isNotBlank(moniWebUserDataVo.getInfoFile())){
					vpo.setInfoFile(moniWebUserDataVo.getInfoFile());
				}
				if(StringUtils.isNotBlank(moniWebUserDataVo.getBgimg())){
					vpo.setBgimg(moniWebUserDataVo.getBgimg());
				}
				if(StringUtils.isNotBlank(moniWebUserDataVo.getProfiles())){
					vpo.setProfiles(moniWebUserDataVo.getProfiles());
				}
				this.monitorDataService.sendMessToQueue("2", JSON.toJSONString(vpo));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("web用户信息变更失败"+e.getMessage());
		}
		return res;
	}

}
