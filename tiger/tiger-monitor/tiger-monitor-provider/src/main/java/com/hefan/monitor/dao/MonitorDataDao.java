package com.hefan.monitor.dao;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hefan.monitor.bean.UserChange;
import com.hefan.monitor.bean.UserChangeLog;
import com.hefan.monitor.bean.UserProfileLog;

@Repository
public class MonitorDataDao {
	@Resource
    JdbcTemplate jdbcTemplate;

	public int deleteUserChange(String userId,int changeType){
		String sql = "delete from user_change where user_id=? and change_type=?";
		return jdbcTemplate.update(sql, userId,changeType);
	}
	
	public int saveUserChange(UserChange userChange){
		// TODO Auto-generated method stub
		String insertSql = "insert into user_change(user_id,user_type,change_type,create_time)"
				+"VALUES(?,?,?,now())";
		//参数数组
	   	Object[]  paramArr =new Object[] {
	   			 userChange.getUserId(),
	   			 userChange.getUserType(),
	   			 userChange.getChangeType()
	   			 }; 
	   	 //参数类型数组
	   	 int[] paramtypeArr = new int[]{
				java.sql.Types.VARCHAR,
				java.sql.Types.INTEGER,
				java.sql.Types.INTEGER
			   }; 
	   	 return jdbcTemplate.update(insertSql,paramArr,paramtypeArr);
	}
	
	public int saveUserChangeLog(UserChangeLog userChangeLog){
		// TODO Auto-generated method stub
		String insertSql = "insert into user_change_log(user_id,change_type,changed_value,opt_from,create_time)"
				+"VALUES(?,?,?,?,now())";
		//参数数组
	   	Object[]  paramArr =new Object[] {
	   			userChangeLog.getUserId(),
	   			userChangeLog.getChangeType(),
	   			userChangeLog.getChangedValue(),
	   			userChangeLog.getOptFrom()
	   			 }; 
	   	 //参数类型数组
	   	 int[] paramtypeArr = new int[]{
				java.sql.Types.VARCHAR,
				java.sql.Types.INTEGER,
				java.sql.Types.VARCHAR,
				java.sql.Types.INTEGER
			   }; 
	   	 return jdbcTemplate.update(insertSql,paramArr,paramtypeArr);
	}
	
	public int saveUserProfileLog(UserProfileLog userProfileLog) {
		// TODO Auto-generated method stub
		String insertSql = "insert into user_profile_log(user_id,info_file,bgimg,profiles,opt_from,create_time)"
				+"VALUES(?,?,?,?,?,now())";
		//参数数组
	   	Object[]  paramArr =new Object[] {
	   			userProfileLog.getUserId(),
	   			userProfileLog.getInfoFile(),
	   			userProfileLog.getBgimg(),
	   			userProfileLog.getProfiles(),
	   			userProfileLog.getOptFrom()
	   			 }; 
	   	 //参数类型数组
	   	 int[] paramtypeArr = new int[]{
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.INTEGER
			   }; 
	   	 return jdbcTemplate.update(insertSql,paramArr,paramtypeArr);
	}
}
