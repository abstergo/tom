package com.hefan.monitor.task;

import com.hefan.monitor.configCenter.MonitorConfigCenter;
import com.hefan.monitor.itf.LiveYellowService;
import com.hefan.schedule.model.ScheduleExecuteRecord;
import com.hefan.schedule.model.ScheduleServer;
import com.hefan.schedule.service.IScheduleExecuteRecordService;
import com.hefan.schedule.service.IScheduleTaskDeal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * Created by liteng on 2017/3/13.
 */
@Service("kamPicturesDelTask")
public class KamPicturesDelTask implements IScheduleTaskDeal {
    private Logger logger = LoggerFactory.getLogger(KamPicturesDelTask.class);

    @Resource
    private LiveYellowService liveYellowService;
    @Resource
    private IScheduleExecuteRecordService scheduleExecuteRecordService;
    @Resource
    private MonitorConfigCenter monitorConfigCenter;
    @Override
    public boolean execute(ScheduleServer scheduleServer) {
        logger.info("鉴黄图片清理任务－－start");
        ScheduleExecuteRecord scheduleExecuteRecord = new ScheduleExecuteRecord();
        scheduleExecuteRecord.setExecuteTime(new Date());
        long startTime = System.currentTimeMillis();
        boolean rst = true;
        int successCount = 0;
        int failCount = 0;
        Map<String, String> config = monitorConfigCenter.getPublicConfig();
        int days=Integer.parseInt(config.get("delPic.days"));
        int rate=Integer.parseInt(config.get("delPic.rate"));
        try {
            liveYellowService.delKamPictures(days,rate);
            successCount++;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("鉴黄图片清理任务－－执行失败：" , e);
            failCount++;
            rst = false;
        }
        long endTime = System.currentTimeMillis();
        scheduleExecuteRecord.setConsumingTime((endTime - startTime));
        scheduleExecuteRecord.setNextExecuteTime(scheduleServer.getNextRunStartTime());
        scheduleExecuteRecord.setTaskType(scheduleServer.getBaseTaskType());
        scheduleExecuteRecord.setStatus(rst ? (short) 1 : (short) 2);
        scheduleExecuteRecord.setExecuteResult(String.format("鉴黄图片清理任务，成功处理%s条，失败%s条", successCount, failCount));
        scheduleExecuteRecord.setHostName(scheduleServer.getHostName());
        scheduleExecuteRecord.setIp(scheduleServer.getIp());
        scheduleExecuteRecordService.addScheduleExecuteRecord(scheduleExecuteRecord);
        return true;
    }
}
