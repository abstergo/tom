package com.hefan.monitor.service;

import com.hefan.monitor.bean.AppChannelStatistics;
import com.hefan.monitor.configCenter.MonitorConfigCenter;
import com.hefan.monitor.dao.AppChannelStatisticsDao;
import com.hefan.monitor.itf.AppChannelStatisticsService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * IOS推广渠道信息收集
 * Created by kevin_zhang on 27/03/2017.
 */
@Component("appChannelStatisticsService")
public class AppChannelStatisticsServiceImpl implements AppChannelStatisticsService {
    public static Logger logger = LoggerFactory.getLogger(AppChannelStatisticsServiceImpl.class);
    @Resource
    AppChannelStatisticsDao appChannelStatisticsDao;

    @Resource
    MonitorConfigCenter monitorConfigCenter;

    /**
     * 第三方提交IOS推广渠道信息
     *
     * @param model
     * @return
     */
    @Override
    public AppChannelStatistics submitForThirdParty(AppChannelStatistics model) {
        try {
            //获取IOS第三方推广信息
            Map<String, String> config = monitorConfigCenter.getPublicConfig();
            String iosChannel = config.get("ios_channel");
            logger.info("第三方提交IOS推广渠道信息操作 获取的渠道信息：" + iosChannel);
            logger.info("第三方提交IOS推广渠道信息操作 提交的渠道信息：" + model.getSourceType());
            if (StringUtils.isNotBlank(iosChannel)) {
                String[] list = iosChannel.split("_");
                if (null != list && list.length > 0) {
                    for (String str : list) {
                        if (StringUtils.isNotBlank(str) && str.equals(model.getSourceType())) {
                            logger.info("第三方提交IOS推广渠道信息操作 获取的渠道信息与后端配置匹配");
                            AppChannelStatistics result = appChannelStatisticsDao.saveForThirdParty(model);
                            if (null != result && StringUtils.isNotBlank(result.getSourceType()) && !result.getSourceType().equals(model.getSourceType())) {
                                logger.info("第三方提交IOS推广渠道信息操作 {} 平台的 {} 已被 {} 平台抢占", model.getSourceType(), model.getIdfa(), result.getSourceType());
                            }
                            return result;
                        }
                    }
                }
            }
            model.setSourceType("");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.info("第三方提交IOS推广渠道信息操作 提交失败" + ex);
            model.setSourceType("");
        }
        return appChannelStatisticsDao.saveForThirdParty(model);
    }

    /**
     * APP提交IOS推广校验参数
     *
     * @param model
     * @return
     */
    @Override
    public AppChannelStatistics submitForApp(AppChannelStatistics model) {
        return appChannelStatisticsDao.saveForApp(model);
    }

    /**
     * 获取IOS推广信息
     *
     * @param idfaStr
     * @return
     */
    @Override
    public AppChannelStatistics getChannelStatisticsInfoByIdfa(String idfaStr) {
        return appChannelStatisticsDao.queryByIdfa(idfaStr);
    }

    /**
     * 更新IOS推广链接
     *
     * @param model
     * @return
     */
    @Override
    public int updateAppChannelStatistics(AppChannelStatistics model) {
        return appChannelStatisticsDao.update(model);
    }

    /**
     * 激活IOS推广链接
     *
     * @param isActivity
     * @param idfaStr
     */
    @Override
    public int activityAppChannelStatistics(int isActivity, String idfaStr) {
        return appChannelStatisticsDao.updateActivityStatus(isActivity, idfaStr);
    }
}
