package com.hefan.monitor.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.bean.Message;
import com.hefan.common.ons.service.ONSProducer;
import com.hefan.monitor.bean.UserChange;
import com.hefan.monitor.bean.UserChangeLog;
import com.hefan.monitor.bean.UserProfileLog;
import com.hefan.monitor.configCenter.MonitorConfigCenter;
import com.hefan.monitor.dao.MonitorDataDao;
import com.hefan.monitor.itf.MonitorDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

@Component("monitorDataService")
public class MonitorDataServiceImpl implements MonitorDataService {

	private Logger logger = LoggerFactory.getLogger(MonitorDataService.class);

	@Reference
	ONSProducer onsProducer;
	@Resource
	MonitorDataDao monitorDataDao;
	@Resource
	private MonitorConfigCenter monitorConfigCenter;
	
	@Override
	public void sendMessToQueue(String type, String value) {
		// TODO Auto-generated method stub
		try {
			// 给队列发消息，完成后续工作
			Message message = new Message();
			message.put("type", type+"");
			message.put("monitorData", value);
			message.setTopic(TopicRegistry.HEFAN_MONI_DATA);
			Map<String, String> config = monitorConfigCenter.getPublicConfig();
			String onsEnv=config.get("ons.env");
			message.setTag(onsEnv);
			boolean flag=onsProducer.sendMsg(message);
			logger.error(flag+"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.debug("监控资料加入队列失败,类型："+type+",内容:"+value);
		}
	}

	@Override
	public int saveUserChange(UserChange userChange) {
		// TODO Auto-generated method stub
		monitorDataDao.deleteUserChange(userChange.getUserId(),userChange.getChangeType());
		return monitorDataDao.saveUserChange(userChange);
	}

	@Override
	public int saveUserChangeLog(UserChangeLog userChangeLog) {
		// TODO Auto-generated method stub
		return monitorDataDao.saveUserChangeLog(userChangeLog);
	}

	@Override
	public int saveUserProfileLog(UserProfileLog userProfileLog) {
		// TODO Auto-generated method stub
		return monitorDataDao.saveUserProfileLog(userProfileLog);
	}

}
