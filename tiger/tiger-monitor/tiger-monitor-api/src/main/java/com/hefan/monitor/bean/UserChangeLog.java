package com.hefan.monitor.bean;

import java.io.Serializable;
import java.util.Date;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;

@Entity(tableName = "user_change_log")
public class UserChangeLog implements Serializable{
	
	private Long id;
	@Column("user_id")
	private String userId;
	@Column("change_type")
	private Integer changeType;
	@Column("changed_value")
	private String changedValue;
	@Column("create_time")
	private Date createTime;
	@Column("opt_from")
	private Integer optFrom;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getChangeType() {
		return changeType;
	}
	public void setChangeType(Integer changeType) {
		this.changeType = changeType;
	}
	public String getChangedValue() {
		return changedValue;
	}
	public void setChangedValue(String changedValue) {
		this.changedValue = changedValue;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getOptFrom() {
		return optFrom;
	}
	public void setOptFrom(Integer optFrom) {
		this.optFrom = optFrom;
	}

}
