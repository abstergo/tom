package com.hefan.monitor.bean;

import java.io.Serializable;

public class MoniUserDataVo implements Serializable {
	
	private String userId;//盒饭id
	private Integer userType;//用户类型
	private String nickName;//昵称
	private String headImg;//头像
	private String personSign;//个性签名
	private Integer optFrom;//来源 0-app  1-web
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getHeadImg() {
		return headImg;
	}
	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}
	public String getPersonSign() {
		return personSign;
	}
	public void setPersonSign(String personSign) {
		this.personSign = personSign;
	}
	public Integer getOptFrom() {
		return optFrom;
	}
	public void setOptFrom(Integer optFrom) {
		this.optFrom = optFrom;
	}
	
}
