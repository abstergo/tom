package com.hefan.monitor.itf;

import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.hefan.live.bean.LiveLog;
import com.hefan.live.bean.LiveRoomVo;
import com.hefan.live.bean.WarnLog;
import com.hefan.user.bean.WebUser;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import java.util.List;
import java.util.Map;

/**
 * Created by sagagyq on 2016/12/5.
 */
public interface MonitorService {

    /**
     * 直播断流处理
     * @param userId
     * @param chartRoomId
     * @param liveUuid
     * @return
     */
    public ResultBean closeLiving(String userId, String chartRoomId, String liveUuid,String promptCopy);




    /**
     * 保存警告日志表
     * @Title: saveWarnLog
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param warnLog
     * @return
     * @return: int
     * @author: LiTeng
     * @throws
     * @date:   2016年10月29日 下午1:57:37
     */
    int saveWarnLog(WarnLog warnLog);



    /**
     * 改为高危直播
     * @Title: updateLiveHighStatus
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param userId
     * @return
     * @return: int
     * @author: LiTeng
     * @throws
     * @date:   2016年10月29日 下午2:01:29
     */
    int updateLiveHighStatus(String userId,String liveUuid,int risk,String optUserId);


    /**
     * 获取违规配置信息
     * @Title: getPunishTypeList
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @return
     * @return: List
     * @author: LiTeng
     * @throws
     * @date:   2016年10月29日 下午3:23:34
     */
    public List getPunishTypeList();


    /**
     * 提交违规处理
     * @Title: dealPunishDetal
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param userId
     * @param optUserId
     * @param map (punish_type)
     * @param punishReason
     * @param illegalPic
     * @return
     * @return: int
     * @author: LiTeng
     * @throws
     * @date:   2016年10月29日 下午5:11:23
     */
    public int dealPunishDetal(String userId, String optUserId, Map map, String punishReason, String illegalPic, String liveUuid,int punishTypeFromTb, int punishDataType);


    /**
     * 根据id获取违规配置信息
     * @Title: getPunishTypeById
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param typeId
     * @return
     * @return: Map
     * @author: LiTeng
     * @throws
     * @date:   2016年10月29日 下午5:19:42
     */
    public Map getPunishTypeById(long typeId,int punishTypeFromTb);

    List<LiveRoomVo> getLivingList(Page po);

    LiveRoomVo findLiveRoomByIdFormonitor(String userId, String uuid);

    /**
     *  获取高危视频库
     * @Title: getHighRiskLiveList
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param userId
     * @param nickName
     * @param startTime
     * @param endTime
     * @return: void
     * @author: LiTeng
     * @throws
     * @date:   2016年11月24日 下午2:41:01
     */
    public List getHighRiskLiveList(String userId,String nickName,String startTime,String endTime);

    public List getPunishDetail();

    /**
     * 根据userId获取用户基本信息
     * @param userId
     * @return
     */
    public WebUser getWebUserInfoByUserId(String userId);

    /**
     * 往聊天室内发消息
     * @Title: chatroomSendMsg
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param roomid (必传) 聊天室id
     * @param msgId (必传) 客户端消息id，使用uuid等随机串，msgId相同的消息会被客户端去重
     * @param fromAccid (必传) 消息发出者的账号accid
     * @param msgType (必传) 消息类型： 0: 表示文本消息，1: 表示图片，2: 表示语音，3: 表示视频，4: 表示地理位置信息，6: 表示文件，10: 表示Tips消息，100: 自定义消息类型
     * @param resendFlag (可选) 重发消息标记，0：非重发消息，1：重发消息，如重发消息会按照msgid检查去重逻辑
     * @param attach (可选) 消息内容，格式同消息格式示例中的body字段,长度限制2048字符
     * @param ext (可选) 消息扩展字段，内容可自定义，请使用JSON格式，长度限制4096
     * @return
     * @return: ResultBean
     * @author: LiTeng
     * @throws
     * @date:   2016年9月29日 下午5:16:11
     */
    public ResultBean chatroomSendMsg(long roomid, String fromAccid, int msgType, int resendFlag, String attach, String ext);

    LiveLog getLiveLogByUuid(String liveUuid);
    
    /**
     * 监控端修改直播封面或直播描述
     * @Title: updateLiveInfoForMonitor   
     * @Description: TODO(这里用一句话描述这个方法的作用)   
     * @param liveName
     * @param liveImg
     * @param liveUuid
     * @param anthId
     * @return      
     * @return: ResultBean
     * @author: LiTeng      
     * @throws 
     * @date:   2016年12月6日 下午6:53:20
     */
    ResultBean updateLiveInfoForMonitor(String liveName, String liveImg, String liveUuid, String anthId);
}
