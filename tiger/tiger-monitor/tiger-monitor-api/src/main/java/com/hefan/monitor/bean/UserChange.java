package com.hefan.monitor.bean;

import java.io.Serializable;
import java.util.Date;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;

@Entity(tableName = "user_change")
public class UserChange implements Serializable{
	
	private Long id;
	@Column("user_id")
	private String userId;
	@Column("user_type")
	private Integer userType;
	@Column("change_type")
	private Integer changeType;
	@Column("create_time")
	private Date createTime;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public Integer getChangeType() {
		return changeType;
	}
	public void setChangeType(Integer changeType) {
		this.changeType = changeType;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
