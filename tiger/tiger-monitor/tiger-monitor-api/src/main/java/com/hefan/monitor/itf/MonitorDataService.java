package com.hefan.monitor.itf;

import com.hefan.monitor.bean.UserChange;
import com.hefan.monitor.bean.UserChangeLog;
import com.hefan.monitor.bean.UserProfileLog;

public interface MonitorDataService {
	
	/**
	 * 加入队列
	 * @Title: sendMessToQueue   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param type  1-用户  2-个人简介
	 * @param value      
	 * @return: void
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年12月5日 上午11:36:07
	 */
	void sendMessToQueue(String type,String value);
	
	/**
	 * 保存用户信息变更表
	 * @Title: saveUserChange   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userChange
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年12月5日 下午5:38:34
	 */
	int saveUserChange(UserChange userChange);
	
	/**
	 * 保存用户信息变更日志表
	 * @Title: saveUserChangeLog   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userChangeLog
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年12月5日 下午5:38:56
	 */
	int saveUserChangeLog(UserChangeLog userChangeLog);
	
	/**
	 * 保存个人简介变更日志
	 * @Title: saveUserProfileLog   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userProfileLog
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年12月6日 上午11:44:23
	 */
	int saveUserProfileLog(UserProfileLog userProfileLog);
	
}
