package com.hefan.monitor.bean;

import java.io.Serializable;

public class MoniProfileDataVo implements Serializable {
	
	private String userId;//盒饭id
	private Integer userType;//用户类型
	private String infoFile;
	private String bgimg;
	private String profiles;
	private Integer optFrom;//来源 0-app  1-web
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public String getInfoFile() {
		return infoFile;
	}
	public void setInfoFile(String infoFile) {
		this.infoFile = infoFile;
	}
	public String getBgimg() {
		return bgimg;
	}
	public void setBgimg(String bgimg) {
		this.bgimg = bgimg;
	}
	public String getProfiles() {
		return profiles;
	}
	public void setProfiles(String profiles) {
		this.profiles = profiles;
	}
	public Integer getOptFrom() {
		return optFrom;
	}
	public void setOptFrom(Integer optFrom) {
		this.optFrom = optFrom;
	}

}
