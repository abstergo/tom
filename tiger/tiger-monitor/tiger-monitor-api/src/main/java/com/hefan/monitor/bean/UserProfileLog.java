package com.hefan.monitor.bean;

import java.io.Serializable;
import java.util.Date;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;

@Entity(tableName = "user_profile_log")
public class UserProfileLog implements Serializable{
	
	private Long id;
	@Column("user_id")
	private String userId;
	@Column("info_file")
	private String infoFile;
	@Column("bgimg")
	private String bgimg;
	@Column("profiles")
	private String profiles;
	@Column("create_time")
	private Date createTime;
	@Column("opt_from")
	private Integer optFrom;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getInfoFile() {
		return infoFile;
	}
	public void setInfoFile(String infoFile) {
		this.infoFile = infoFile;
	}
	public String getBgimg() {
		return bgimg;
	}
	public void setBgimg(String bgimg) {
		this.bgimg = bgimg;
	}
	public String getProfiles() {
		return profiles;
	}
	public void setProfiles(String profiles) {
		this.profiles = profiles;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Integer getOptFrom() {
		return optFrom;
	}
	public void setOptFrom(Integer optFrom) {
		this.optFrom = optFrom;
	}
}
