package com.hefan.monitor.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

@Entity(tableName = "live_snapshot")
public class LiveSnapshot extends BaseEntity{

	@Column("user_id")
    private String userId;
	@Column("live_uuid")
    private String liveUuid;
	@Column("img_url")
    private String imgUrl;
	@Column("task_id")
    private String taskId;
    private Integer label;
    private String rate;
	@Column("img_no")
    private Integer imgNo;
    private Integer status;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getLiveUuid() {
		return liveUuid;
	}
	public void setLiveUuid(String liveUuid) {
		this.liveUuid = liveUuid;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public Integer getLabel() {
		return label;
	}
	public void setLabel(Integer label) {
		this.label = label;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public Integer getImgNo() {
		return imgNo;
	}
	public void setImgNo(Integer imgNo) {
		this.imgNo = imgNo;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
}
