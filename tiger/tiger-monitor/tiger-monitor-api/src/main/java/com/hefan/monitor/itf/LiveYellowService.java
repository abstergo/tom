package com.hefan.monitor.itf;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.cat.common.entity.ResultBean;

@Path("/jh")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public interface LiveYellowService {
	
	/**
	 * 上传图片到鉴黄接口
	 * @Title: uploadLiveSnapshot   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月10日 下午4:06:01
	 */
	@GET
	@Path("/uploadLiveSnapshot")
	public ResultBean uploadLiveSnapshot();
	
	/**
	 * 查询处理鉴黄接口返回的结果
	 * @Title: dealLiveSnapshot   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月10日 下午4:10:18
	 */
	@GET
	@Path("/dealLiveSnapshot")
	public ResultBean dealLiveSnapshot();

	/**
	 * 删除正常的图片数据
	 * @return
     */
	@GET
	@Path("/delKamPictures")
	public ResultBean delKamPictures(int days,int rate);

}
