package com.hefan.monitor.itf;

import com.hefan.monitor.bean.AppChannelStatistics;

/**
 * IOS推广渠道信息收集
 * Created by kevin_zhang on 27/03/2017.
 */
public interface AppChannelStatisticsService {
    /**
     * 第三方提交IOS推广渠道信息
     *
     * @param model
     * @return
     */
    AppChannelStatistics submitForThirdParty(AppChannelStatistics model);

    /**
     * APP提交IOS推广校验参数
     *
     * @param model
     * @return
     */
    AppChannelStatistics submitForApp(AppChannelStatistics model);

    /**
     * 获取IOS推广信息
     *
     * @param idfaStr
     * @return
     */
    AppChannelStatistics getChannelStatisticsInfoByIdfa(String idfaStr);

    /**
     * 更新IOS推广链接
     *
     * @param model
     * @return
     */
    int updateAppChannelStatistics(AppChannelStatistics model);

    /**
     * 激活IOS推广链接
     *
     * @param isActivity
     * @param idfaStr
     */
    int activityAppChannelStatistics(int isActivity, String idfaStr);
}
