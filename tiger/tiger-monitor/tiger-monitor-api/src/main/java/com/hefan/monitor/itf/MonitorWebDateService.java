package com.hefan.monitor.itf;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.web.bind.annotation.RequestBody;

import com.cat.common.entity.ResultBean;
import com.hefan.monitor.bean.MoniWebUserDataVo;

@Path("/moniData")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public interface MonitorWebDateService {
	
	@POST
	@Path("/modifUserIfo")
	public ResultBean createUser(@RequestBody MoniWebUserDataVo moniWebUserDataVo);

}
