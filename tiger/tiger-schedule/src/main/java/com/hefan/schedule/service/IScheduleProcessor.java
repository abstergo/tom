package com.hefan.schedule.service;

public interface IScheduleProcessor {

  /**
   * 判断进程是否处于休眠状态
   *
   * @return
   */
  boolean isSleeping();

  /**
   * 停止任务处理器
   *
   * @throws Exception
   */
  void stopSchedule() throws Exception;
}
