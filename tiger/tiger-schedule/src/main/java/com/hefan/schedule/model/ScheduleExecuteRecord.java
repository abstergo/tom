package com.hefan.schedule.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 任务执行记录
 *
 * @author wangchao
 * @version 1.0.0
 * @since 2015年8月7日 下午4:16:13
 */
public class ScheduleExecuteRecord implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 6584209572712098206L;

  private int id;

  /**
   * 任务类型
   */
  private String taskType;

  /**
   * 任务执行时间
   */
  private Date executeTime;

  /**
   * 下次执行时间
   */
  private String nextExecuteTime;

  /**
   * 任务执行耗时
   */
  private long consumingTime;

  /**
   * 任务执行结果描述
   */
  private String executeResult;

  /**
   * 任务执行主机IP
   */
  private String ip;

  /**
   * 任务执行主机名
   */
  private String hostName;

  /**
   * 任务执行状态：0，初始化；1，成功；2，失败
   */
  private short status;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getTaskType() {
    return taskType;
  }

  public void setTaskType(String taskType) {
    this.taskType = taskType;
  }

  public Date getExecuteTime() {
    return executeTime;
  }

  public void setExecuteTime(Date executeTime) {
    this.executeTime = executeTime;
  }

  public String getNextExecuteTime() {
    return nextExecuteTime;
  }

  public void setNextExecuteTime(String nextExecuteTime) {
    this.nextExecuteTime = nextExecuteTime;
  }

  public long getConsumingTime() {
    return consumingTime;
  }

  public void setConsumingTime(long consumingTime) {
    this.consumingTime = consumingTime;
  }

  public String getExecuteResult() {
    return executeResult;
  }

  public void setExecuteResult(String executeResult) {
    this.executeResult = executeResult;
  }

  public short getStatus() {
    return status;
  }

  public void setStatus(short status) {
    this.status = status;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getHostName() {
    return hostName;
  }

  public void setHostName(String hostName) {
    this.hostName = hostName;
  }
}
