package com.hefan.schedule.model;

public class Version {

  public final static String version = "hefan-schedule-1.0.0";

  public static String getVersion() {
    return version;
  }

  public static boolean isCompatible(String dataVersion) {
    return version.compareTo(dataVersion) >= 0;
  }

}
