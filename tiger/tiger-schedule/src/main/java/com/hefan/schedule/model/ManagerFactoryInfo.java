package com.hefan.schedule.model;

/**
 * 管理工厂信息
 *
 * @author wangchao
 * @version 1.0.0
 * @since 2016/10/31 上午10:25:21
 */
public class ManagerFactoryInfo {

  private String uuid;

  private boolean start;

  public boolean isStart() {
    return start;
  }

  public void setStart(boolean start) {
    this.start = start;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }
}
