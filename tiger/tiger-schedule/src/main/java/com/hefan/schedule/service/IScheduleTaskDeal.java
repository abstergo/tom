package com.hefan.schedule.service;

import com.hefan.schedule.model.ScheduleServer;

/**
 * 任务执行
 *
 * @author wangchao
 * @version 1.0.0
 * @since 2016/10/31 下午12:25:05
 */
public interface IScheduleTaskDeal {

  /**
   * 默认执行方法（后面改进）
   *
   * @param scheduleServer
   * @author wangchao
   * @version 1.0.0
   * @since 2016/10/31 下午12:25:16
   */
  boolean execute(ScheduleServer scheduleServer);
}
