package com.hefan.schedule.taskmanager;

import com.hefan.schedule.model.ScheduleExecuteRecord;
import com.hefan.schedule.service.IScheduleExecuteRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ScheduleExecuteRecordServiceImpl implements IScheduleExecuteRecordService {

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Override
  public void addScheduleExecuteRecord(ScheduleExecuteRecord scheduleExecuteRecord) {

    String sql = "insert into schedule_execute_record (taskType,executeTime,nextExecuteTime,consumingTime,executeResult,hostName,ip,status) VALUES(?,?,?,?,?,?,?,?)";

    jdbcTemplate.update(sql, scheduleExecuteRecord.getTaskType(), scheduleExecuteRecord.getExecuteTime(), scheduleExecuteRecord.getNextExecuteTime(), scheduleExecuteRecord.getConsumingTime(),
        scheduleExecuteRecord.getExecuteResult(), scheduleExecuteRecord.getHostName(), scheduleExecuteRecord.getIp(), scheduleExecuteRecord.getStatus());
  }

  @Override
  public List<Map<String, Object>> getScheduleExecuteRecordList(String taskType) {
    String sql = "select * from schedule_execute_record where taskType=? order by executeTime desc limit 30";
    List<Map<String, Object>> list = jdbcTemplate.queryForList(sql, taskType);
    return list;
  }

}
