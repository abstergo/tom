package com.hefan.schedule.annotation;

import java.lang.annotation.*;

/**
 * 定时任务注解
 *
 * @version 1.0.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.ANNOTATION_TYPE, ElementType.METHOD })
public @interface Scheduled {

  String value() default "";

}
