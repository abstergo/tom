package com.hefan.schedule.taskmanager;

import com.hefan.schedule.service.IScheduleDataManager;
import com.hefan.schedule.strategy.ScheduleManagerFactory;
import com.hefan.schedule.zk.ScheduleStrategyDataManager4ZK;
import com.hefan.schedule.zk.ZKManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class ConsoleManager {
  public final static String configFile = System.getProperty("user.dir") + File.separator + "ScheduleConfig.properties";
  protected static transient Logger log = LoggerFactory.getLogger(ConsoleManager.class);
  private static ScheduleManagerFactory scheduleManagerFactory;

  public static boolean isInitial() throws Exception {
    return scheduleManagerFactory != null;
  }

  public static boolean initial() throws Exception {
    if (scheduleManagerFactory != null) {
      return true;
    }
    File file = new File(configFile);
    scheduleManagerFactory = new ScheduleManagerFactory();
    scheduleManagerFactory.start = false;

    if (file.exists() == true) {
      //Console不启动调度能力
      Properties p = new Properties();
      FileReader reader = new FileReader(file);
      p.load(reader);
      reader.close();
      scheduleManagerFactory.init(p);
      log.info("加载Schedule配置文件：" + configFile);
      return true;
    } else {
      return false;
    }
  }

  public static ScheduleManagerFactory getScheduleManagerFactory() throws Exception {
    if (isInitial() == false) {
      initial();
    }
    return scheduleManagerFactory;
  }

  public static void setScheduleManagerFactory(ScheduleManagerFactory scheduleManagerFactory) {
    ConsoleManager.scheduleManagerFactory = scheduleManagerFactory;
  }

  public static IScheduleDataManager getScheduleDataManager() throws Exception {
    if (isInitial() == false) {
      initial();
    }
    return scheduleManagerFactory.getScheduleDataManager();
  }

  public static ScheduleStrategyDataManager4ZK getScheduleStrategyManager() throws Exception {
    if (isInitial() == false) {
      initial();
    }
    return scheduleManagerFactory.getScheduleStrategyManager();
  }

  public static Properties loadConfig() throws IOException {
    File file = new File(configFile);
    Properties properties;
    if (file.exists() == false) {
      properties = ZKManager.createProperties();
    } else {
      properties = new Properties();
      FileReader reader = new FileReader(file);
      properties.load(reader);
      reader.close();
    }
    return properties;
  }

  public static void saveConfigInfo(Properties p) throws Exception {
    try {
      FileWriter writer = new FileWriter(configFile);
      p.store(writer, "");
      writer.close();
    } catch (Exception ex) {
      throw new Exception("不能写入配置信息到文件：" + configFile, ex);
    }
    if (scheduleManagerFactory == null) {
      initial();
    } else {
      scheduleManagerFactory.reInit(p);
    }
  }

}
