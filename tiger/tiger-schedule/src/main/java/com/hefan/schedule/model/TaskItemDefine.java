package com.hefan.schedule.model;

/**
 * 任务定义，提供关键信息给使用者
 *
 * @author wangchao
 * @version 1.0.0
 * @since 2016/10/31 下午3:36:27
 */
public class TaskItemDefine {
  /**
   * 任务项ID
   */
  private String taskItemId;
  /**
   * 任务项自定义参数
   */
  private String parameter;

  public String getParameter() {
    return parameter;
  }

  public void setParameter(String parameter) {
    this.parameter = parameter;
  }

  public String getTaskItemId() {
    return taskItemId;
  }

  public void setTaskItemId(String taskItemId) {
    this.taskItemId = taskItemId;
  }

  @Override
  public String toString() {
    return "(t=" + taskItemId + ",p=" + parameter + ")";
  }

}
