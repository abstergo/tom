package com.hefan.schedule.service;

import com.hefan.schedule.model.ScheduleExecuteRecord;

import java.util.List;
import java.util.Map;

public interface IScheduleExecuteRecordService {

  /**
   * 添加任务执行记录
   *
   * @param scheduleExecuteRecord
   * @author wangchao
   * @version 1.0.0
   * @since 2015年8月7日 下午5:38:22
   */
  void addScheduleExecuteRecord(ScheduleExecuteRecord scheduleExecuteRecord);

  /**
   * 获取任务执行列表
   *
   * @param taskType
   * @return
   * @author wangchao
   * @version 1.0.0
   * @since 2015年8月10日 下午5:18:14
   */
  List<Map<String, Object>> getScheduleExecuteRecordList(String taskType);
}
