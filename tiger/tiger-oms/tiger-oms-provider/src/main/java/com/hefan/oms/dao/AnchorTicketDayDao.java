package com.hefan.oms.dao;

import com.hefan.common.orm.dao.BaseDao;
import com.hefan.oms.bean.AnchorTicketDay;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by hbchen on 2016/9/29.
 */

@Repository
public class AnchorTicketDayDao{

    @Resource
    private  JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public int initAnchorTicket(AnchorTicketDay record){
        String sql = "insert into anchor_ticket_day (user_id,years_month,create_date) " +
                "  (select ?,?,? FROM DUAL WHERE NOT EXISTS(select 1 from anchor_ticket_day where user_id=? and create_date=?))";

        int result = getJdbcTemplate().update(sql, record.getUserId(),record.getYearsMonth(),record.getCreateDate(),record.getUserId(),record.getCreateDate());
        return  result;
    }

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public int updateAnchorTicket(AnchorTicketDay anchorTicketDay)  {
        int result = 0;
        String sql = " update anchor_ticket_day set ";
        if (anchorTicketDay.getRealTicket()>0){
            sql += " real_ticket  += ? ";
        }else if (anchorTicketDay.getFalseTicket()>0){
            sql += " false_ticket += ?";
        }
        sql += " where user_id= ? and create_date = ? ";

        if (anchorTicketDay.getRealTicket()>0){
            result =  getJdbcTemplate().update(sql,
                    new Object[]{anchorTicketDay.getRealTicket(),anchorTicketDay.getUserId(),anchorTicketDay.getCreateDate()});
        }else if (anchorTicketDay.getFalseTicket()>0){
            result = getJdbcTemplate().update(sql,
                    new Object[]{anchorTicketDay.getFalseTicket(),anchorTicketDay.getUserId(),anchorTicketDay.getCreateDate()});
        }
        return result;
    }

    public int selectAvailableTickets(String userId){

        String sql = " select  SUM(real_ticket - pay_ticket) availableTickets FROM " +
                " anchor_ticket_day WHERE years_month=DATE_FORMAT(NOW(), \"%Y-%m\") AND user_id= ?";

        try {
            return jdbcTemplate.queryForObject(sql,Integer.class,userId);
        }catch (EmptyResultDataAccessException e){
            return  0;
        }catch (NullPointerException e){
            return 0;
        }
    }
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public int insertMealTicketUsedRecord(AnchorTicketDay record){
        String sql = "insert into anchor_ticket_day (user_id,pay_ticket,years_month,create_date) " +
                "  (select ?,?,DATE_FORMAT(NOW(), \"%Y-%m\"),NOW() FROM DUAL WHERE NOT EXISTS(select 1 from anchor_ticket_day where user_id=? and create_date=DATE_FORMAT(NOW(), \"%Y-%m-%d\")))";

        int result = getJdbcTemplate().update(sql, record.getUserId(),record.getPayTicket(),record.getUserId());
        return  result;
    }

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public int updateAnchorPayTicket(AnchorTicketDay anchorTicketDay)  {
        int result = 0;
        String sql = " update anchor_ticket_day set  pay_ticket=pay_ticket + ? " +
                " where user_id = ? and create_date=DATE_FORMAT(NOW(), \"%Y-%m-%d\")";
        result =  getJdbcTemplate().update(sql,anchorTicketDay.getPayTicket(),anchorTicketDay.getUserId());
        return  result;
    }



}
