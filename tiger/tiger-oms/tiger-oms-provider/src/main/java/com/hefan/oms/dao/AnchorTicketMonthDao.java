package com.hefan.oms.dao;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.oms.bean.AnchorTicketMonth;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by hbchen on 2016/10/11.
 */
@Repository
public class AnchorTicketMonthDao  extends BaseDaoImpl<AnchorTicketMonth> {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    Logger logger = LoggerFactory.getLogger(AnchorTicketMonthDao.class);

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public int initAnchorTicket(AnchorTicketMonth record) throws Exception{
        String sql = "insert IGNORE into anchor_ticket_month (user_id,years_month,real_ticket,false_ticket) " +
                " values (?,?,?,?)";

        int result = getJdbcTemplate().update(sql, record.getUserId(),record.getYearsMonth()
                ,record.getRealTicket(),record.getFalseTicket());
        return  result;
    }

    public int selectAnchorTicketMonthCount(AnchorTicketMonth record){
        String sql = " select count(1) from anchor_ticket_month where user_id = ? and years_month=?";
        return getJdbcTemplate().queryForObject(sql,new Object[] {record.getUserId(),record.getYearsMonth()},Integer.class);
    }

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public int updateAnchorTicket(AnchorTicketMonth anchorTicketMonth) {
        int result = 0;
        String sql = " update anchor_ticket_month set ";
        if (anchorTicketMonth.getRealTicket()>0){
            sql += " real_ticket  = real_ticket + ? ";
        }else if (anchorTicketMonth.getFalseTicket()>0){
            sql += " false_ticket = false_ticket+ ?";
        }
        sql += " where user_id= ? and years_month = ? ";

        if (anchorTicketMonth.getRealTicket()>0){
            result =  getJdbcTemplate().update(sql,
                    new Object[]{anchorTicketMonth.getRealTicket(),anchorTicketMonth.getUserId(),anchorTicketMonth.getYearsMonth()});
        }else if (anchorTicketMonth.getFalseTicket()>0){
            result = getJdbcTemplate().update(sql,
                    new Object[]{anchorTicketMonth.getFalseTicket(),anchorTicketMonth.getUserId(),anchorTicketMonth.getYearsMonth()});
        }
        return result;
    }

    public int selectAvailableTickets(String userId,Integer years_month){
        logger.info("selectAvailableTickets");
        logger.info("userId"+userId);
        logger.info("years_month"+years_month);
        String sql = " select  real_ticket - pay_ticket as  availableTickets FROM " +
                " anchor_ticket_month WHERE years_month=? AND user_id= ?";

        try {
            return jdbcTemplate.queryForObject(sql,new Object[] {years_month,userId},Integer.class);
        }catch (EmptyResultDataAccessException e){
            return  0;
        }catch (NullPointerException e){
            return 0;
        }
    }


    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public int insertMealTicketUsedRecord(AnchorTicketMonth record){
        String sql = "insert into anchor_ticket_month (user_id,pay_ticket,years_month)  values (?,?,?)";

        int result = getJdbcTemplate().update(sql, record.getUserId(),record.getPayTicket(),record.getYearsMonth());
        return  result;
    }

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public int updateAnchorPayTicket(AnchorTicketMonth anchorTicketMonth)  {
        int result = 0;
        String sql = " update anchor_ticket_month set  pay_ticket=pay_ticket + ? " +
                " where user_id = ? and years_month = ? ";
        result =  getJdbcTemplate().update(sql,anchorTicketMonth.getPayTicket(),anchorTicketMonth.getUserId(),anchorTicketMonth.getYearsMonth());
        return  result;
    }

}
