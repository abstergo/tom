package com.hefan.oms.service;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.common.util.GuuidUtil;
import com.cat.tiger.util.GlobalConstants;
import com.hefan.oms.bean.TradeLog;
import com.hefan.oms.dao.RechargeCodeKindItemDao;
import com.hefan.oms.dao.TradeLogDao;
import com.hefan.oms.itf.PlatformRechargeService;
import com.hefan.oms.itf.RechargeCodePayService;
import com.hefan.user.itf.WebUserService;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;


@Component("rechargeCodePayService")
public class RechargeCodePayServiceImpl implements RechargeCodePayService {

	@Resource
	RechargeCodeKindItemDao rechargeCodeKindItemDao;
	@Resource
	PlatformRechargeService platformRechargeService;
	@Resource
	TradeLogDao tradeLogDao;
    @Resource
	WebUserService webUserService;
	/*
	 * @see cn.saga.service.RechargeCodePayService#findValidRechargeInfoByCode(java.lang.String, java.util.Date, java.util.Date)
	 */
	@Transactional(readOnly=true)
	public Map findValidRechargeInfoByCode(String code) {
		return  rechargeCodeKindItemDao.findValidRechargeCode(code);
	}

	@Transactional(readOnly=true)
	public ResultBean findValidRechargeInfoByCodeForPay(String code, String userId) {
		ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);

		Map rechargeMap = rechargeCodeKindItemDao.findValidRechargeCode(code);
		if(rechargeMap == null || rechargeMap.isEmpty()) {
			return  new ResultBean(ResultCode.RechangeCodePayErr1);
		}
		int isOnly = Integer.valueOf(String.valueOf(rechargeMap.get("isOnly")));
		if(isOnly == 1) { //唯一判断
			int rcId = Integer.valueOf(String.valueOf(rechargeMap.get("rcId")));
			int isUsed =  rechargeCodeKindItemDao.checkRechangeCodeIsOnly(rcId,userId);
			if(isUsed > 0) {
				return  new ResultBean(ResultCode.RechangeCodeBatchIsUsed);
			}
		}
		resultBean.setData(rechargeMap);
		return resultBean;
	}
	
	/*
	 * @see cn.saga.service.RechargeCodePayService#rechargeCodePayOpreate(cn.saga.model.WebUser, java.util.Map, java.lang.Integer)
	 */
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public Integer rechargeCodePayOpreate(Map webUserMap,Map rechangeCodeMap, Integer paySource)throws Exception {
		//更新充值为已使用

		Integer kindNameIn = Integer.valueOf(String.valueOf(rechangeCodeMap.get("kindName")));
		Integer incomFp = platformRechargeService.getMealTicketsByRechargeRuleInfo(4, kindNameIn);
		if (incomFp == null) {
			return null;
		}
		//TradeLogDao
		String orderId = GuuidUtil.getUuid();
		BigDecimal amount = StringUtils.isBlank(String.valueOf(rechangeCodeMap.get("kindName"))) ? BigDecimal.valueOf(0) : BigDecimal.valueOf(kindNameIn);

		long userId = MapUtils.getLongValue(webUserMap,"userId",0);
		String nickName = MapUtils.getString(webUserMap,"nickName","");
		Integer balance = MapUtils.getIntValue(webUserMap,"balance",0);

		//新增充值码充值订单充值码算赠送饭票
		/*TradeLog tradeLog = new TradeLog(orderId, amount,
				incomFp, BigDecimal.valueOf(incomFp), amount, 0,
				BigDecimal.valueOf(0), 4, paySource, "SUCESS", String.valueOf(userId), nickName, BigDecimal.valueOf(0), String.valueOf(rechangeCodeMap.get("code")),
				balance, balance + incomFp.intValue());*/
		TradeLog tradeLog = new TradeLog();
		tradeLog.setOrderId(orderId);
		tradeLog.setPayAmount(BigDecimal.ZERO);
		tradeLog.setIncome(0);
		tradeLog.setIncomeAmount(BigDecimal.ZERO);
		tradeLog.setExchangeAmount(BigDecimal.ZERO);
		tradeLog.setRewardFanpiao(incomFp);
		tradeLog.setRewardFanpiaoAmount(amount);
		tradeLog.setAccountType(GlobalConstants.PAY_SOURCE_RECHARECODE);
		tradeLog.setPaySource(paySource);
		tradeLog.setPayStatus(3);
		tradeLog.setAccountStatus("SUCESS");
		tradeLog.setUserId(String.valueOf(userId));
		tradeLog.setNickName(nickName);
		tradeLog.setChannelFee(BigDecimal.valueOf(0));
		tradeLog.setRechargeCode(String.valueOf(rechangeCodeMap.get("code")));
		tradeLog.setPayBeforeFanpiao(balance);
		tradeLog.setPayAfterFanpiao(balance+incomFp);
		tradeLog.setPayNotifyDate(new Date());
		tradeLogDao.insertNewRecord(tradeLog);

		//更新用户余额
		webUserService.incrWebUserBalance(String.valueOf(userId),(long)incomFp);

		rechargeCodeKindItemDao.updateRechargeCodeIsUsed(Integer.valueOf(String.valueOf(rechangeCodeMap.get("id"))), paySource,String.valueOf(userId));

		return incomFp;
	}
}
