package com.hefan.oms.service;

import com.cat.tiger.util.GlobalConstants;
import com.hefan.oms.bean.RebalanceVo;
import com.hefan.oms.bean.WebUserIncome;
import com.hefan.oms.dao.InComeDao;
import com.hefan.oms.itf.InComeService;
import com.hefan.user.bean.WebUserIdentity;
import com.hefan.user.itf.UserIdentityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by hbchen on 2016/10/28.
 */
@Component("inComeService")
public class InComeServiceImpl implements InComeService {

    @Resource
    private InComeDao inComeDao;

    private Logger logger = LoggerFactory.getLogger(LiveReBalanceServiceImpl.class);
    @Resource
    private UserIdentityService userIdentityService;
    @Override
    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
    public void
    saveWebIncome(RebalanceVo rebalanceVo,  int hefanNum) throws Exception {
        WebUserIdentity webUserIdentity = userIdentityService.getUserIndentityInfo(rebalanceVo.getToId());
        if (webUserIdentity == null) {
            logger.info("主播用户没有分成比例！！！！userId:{}", rebalanceVo.getToId());
            webUserIdentity = new WebUserIdentity();
            webUserIdentity.setPersonalIntoRatio(0);
            webUserIdentity.setPlatformIntoRatio(0);
            webUserIdentity.setGroupIntoRatio(0);
        }
        if (rebalanceVo.getSource() == GlobalConstants.SOURCE_TYPE_BARRAGE ||rebalanceVo.getSource() == GlobalConstants.SOURCE_TYPE_MESSAGE) {
            webUserIdentity.setPersonalIntoRatio(0);
            webUserIdentity.setPlatformIntoRatio(100);
            webUserIdentity.setGroupIntoRatio(0);
        }
        WebUserIncome wi = new WebUserIncome();
        wi.setUserId(rebalanceVo.getToId());
        wi.setLiveUuid(rebalanceVo.getLiveUuid());
        wi.setPresentId(rebalanceVo.getPresentId());
        wi.setSource(rebalanceVo.getSource());
        wi.setHefanNum(hefanNum);//收入盒饭
        wi.setType(rebalanceVo.getIsFalse());//区分真实和虚拟账户
        wi.setPersonHefanRadio(webUserIdentity.getPersonalIntoRatio());
        wi.setSuperiorHefanRadio(webUserIdentity.getGroupIntoRatio());
        wi.setPlatformHefanRadio(webUserIdentity.getPlatformIntoRatio());
        wi.setFromId(rebalanceVo.getFromId());
        wi.setPresentName(rebalanceVo.getPresentName());
        wi.setPresentNum(1);
        wi.setIsOld(1);
        WebUserIdentity userIdentity= userIdentityService.getUserIndentityInfo(rebalanceVo.getToId());
        wi.setPid(userIdentity == null ? 0 : userIdentity.getPid());
        wi.setMessageId(rebalanceVo.getMessageId());
        inComeDao.insertNewRecord(wi);
    }

    /**
     * 统计直播间收入
     *
     * @param liveUuid 直播id
     * @return WebUserIncome
     * @throws
     * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
     * @author wangchao
     * @create 2016/12/28 15:39
     */
    @Override
    public List<Map<String,Object>> getTotalByLiveUuid(String liveUuid) {
        return inComeDao.getTotalByLiveUuid(liveUuid);
    }

    /**
     * 统计每天主播收入
     *
     * @param start
     * @param end
     * @return
     * @throws
     * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
     * @author wangchao
     * @create 2016/12/28 16:58
     */
    @Override
    public List<Map<String, Object>> getUserIncomeByDay(String start, String end) {
        return inComeDao.getUserIncomeByDay(start,end);
    }
}
