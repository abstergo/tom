package com.hefan.oms.dao;

import com.hefan.common.exception.DataIllegalException;
import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.oms.bean.OrderRedPacketDetail;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hbchen on 2016/10/12.
 */
@Repository
public class OrderRedPacketDetailDao extends BaseDaoImpl<OrderRedPacketDetail> {


    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public long insertNewRecord(OrderRedPacketDetail record) {

        record = super.save(record);
        return record.getId();

    }

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public void batchInsertNewRecord(List<OrderRedPacketDetail> list){
        String sql = "INSERT INTO order_redpacket_detail(order_id,user_id,fanpiao_num,live_uuid) VALUES(?,?,?,?)";
         //执行批量sql 处理多次插入操作
         int[] count = this.getJdbcTemplate().batchUpdate(sql,
                         new BatchPreparedStatementSetter(){
            @Override
            public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                preparedStatement.setLong(1,list.get(i).getOrderId());
                preparedStatement.setString(2,list.get(i).getUserId());
                preparedStatement.setInt(3,list.get(i).getFanpiaoNum());
                preparedStatement.setString(4,list.get(i).getLiveUuid());
            }

            @Override
            public int getBatchSize() {
                return list.size();
            }
        });
    }

    public List selectRedPackDetailIds(long orderId) {

        String sql = " select id,fanpiao_num from order_redpacket_detail where order_id = ? ";
        List<Object> params = new ArrayList<Object>();
        params.add(orderId);
        List<OrderRedPacketDetail> list = getJdbcTemplate().query(sql, params.toArray(), new BeanPropertyRowMapper<OrderRedPacketDetail>(OrderRedPacketDetail.class));
        if (CollectionUtils.isNotEmpty(list)) {
            return list;
        }
        return null;
    }
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public void updateRedPackDetail(long detailId,String userId) throws Exception{
        String sql = " update order_redpacket_detail set user_id = ? where id=  ? ";
        int id=getJdbcTemplate().update(sql, new Object[]{userId,detailId});
        if(id<=0){
            throw new DataIllegalException(String.format("更新红包记录失败detailId=%s", detailId));
        }
    }

    public int selectRedPackDetailCount(long orderId) {

        String sql = " select count(1) from order_redpacket_detail where order_id = ? and user_id ='' ";
        return jdbcTemplate.queryForObject(sql, new Object[] { orderId }, int.class);
    }

}
