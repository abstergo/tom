package com.hefan.oms.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * oms服务配置中心
 *
 * @author wangchao
 * @create 2016/11/16 15:10
 */
@Repository
public class OmsConfigCenter {

  /**
   * 配置中心
   */
  @Value("#{publicConfig}")
  private Map<String, String> publicConfig;

  public Map<String, String> getPublicConfig() {
    return publicConfig;
  }

  public void setPublicConfig(Map<String, String> publicConfig) {
    this.publicConfig = publicConfig;
  }
}
