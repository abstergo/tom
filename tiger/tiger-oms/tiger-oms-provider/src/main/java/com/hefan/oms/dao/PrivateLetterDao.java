package com.hefan.oms.dao;

import com.hefan.common.exception.DataIllegalException;
import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.oms.bean.PrivateLetter;
import org.springframework.stereotype.Repository;

/**
 * Created by hbchen on 2016/10/19.
 */
@Repository
public class PrivateLetterDao extends BaseDaoImpl<PrivateLetter> {

    public long insertNewRecord(PrivateLetter record) {
        record = super.save(record);
        long id= record.getId();
        if(id <=0 ){
            throw new DataIllegalException(String.format("新增私信内容异常%s", record.toString()));
        }
        return id;
    }

}
