package com.hefan.oms.service;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.cat.tiger.util.GlobalConstants;
import com.hefan.common.exception.DataIllegalException;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.bean.Message;
import com.hefan.common.ons.service.ONSProducer;
import com.hefan.common.util.DynamicProperties;
import com.hefan.oms.bean.Present;
import com.hefan.oms.bean.RebalanceVo;
import com.hefan.oms.dao.PresentDao;
import com.hefan.oms.itf.AccountService;
import com.hefan.oms.itf.CommentReBalanceService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WebUserService;

/**
 * Created by hbchen on 2016/10/9.
 */
@Component("commentReBalanceService")
public class CommentReBalanceServiceImpl implements CommentReBalanceService {

	 @Resource
    private WebUserService webUserService;
    @Resource
    private PresentDao presentDao;
    @Resource
    private AccountService accountService;

    @Resource
    ONSProducer onsProducer;
    private Logger logger = LoggerFactory.getLogger(CommentReBalanceServiceImpl.class);

    @Override
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public boolean reBalance(RebalanceVo rebalanceVo) throws Exception{

            logger.info("评论送礼扣费rebalanceVo={}", JSON.toJSONString(rebalanceVo));
            WebUser webUser = webUserService.getWebUserInfoByUserId(rebalanceVo.getFromId());
            if(webUser==null){
                throw new DataIllegalException(String.format("没有该礼物presentId=%s", rebalanceVo.getPresentId()));
            }
            rebalanceVo.setBeforeFanpiao(webUser.getBalance());
            rebalanceVo.setUserType(webUser.getUserType());
            logger.info("开始查询礼物信息" + rebalanceVo.getPresentId());
            Present present = presentDao.getPresentByPresentId(rebalanceVo.getPresentId());
            if (present == null) {
                throw new DataIllegalException(String.format("没有该礼物presentId=%s", rebalanceVo.getPresentId()));
            }
            rebalanceVo.setPresentName(present.getPresentName());
            rebalanceVo.setPrice(present.getPrice());
            Integer fanPiaoNum = present.getPrice();
            if (webUser.getBalance() < fanPiaoNum) {
                throw new DataIllegalException(String.format("用户%s余额%s不足",webUser.getUserId(),webUser.getBalance()));
            }
            logger.info("{}开始扣费{}" ,webUser.getUserId(), fanPiaoNum);
            webUserService.incrWebUserBalance(rebalanceVo.getFromId(), -1 * fanPiaoNum);
            rebalanceVo.setSource(GlobalConstants.SOURCE_TYPE_CLUB);
            rebalanceVo.setExp(present.getExperience());
            // 给队列发消息，完成后续工作
            String onsEnv = DynamicProperties.getString("ons.env");

            Message messageOrder = new Message();
            messageOrder.setTag(onsEnv);
            messageOrder.put("rebalanceVoOrder", JSON.toJSONString(rebalanceVo));
            messageOrder.setTopic(TopicRegistry.HEFAN_ORDER);
            onsProducer.sendMsg(messageOrder);
            // 给队列发消息，完成后续工作
            Message message = new Message();
            message.setTag(onsEnv);
            message.put("rebalanceVo", JSON.toJSONString(rebalanceVo));
            message.setTopic(TopicRegistry.HEFANTV_OMS);
            onsProducer.sendMsg(message);
            return true;
    }
}