package com.hefan.oms.algorithm;

/**
 * Created by hbchen on 2016/10/13.
 * 以后替换算法，直接替换不同的实现类就好
 */
public interface RedPacketAlgorithm {

    public double[] decompose(double total , int num);
}
