package com.hefan.oms.task;

import com.hefan.schedule.model.ScheduleExecuteRecord;
import com.hefan.schedule.model.ScheduleServer;
import com.hefan.schedule.service.IScheduleExecuteRecordService;
import com.hefan.schedule.service.IScheduleTaskDeal;
import com.hefan.user.itf.WebUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/*
*Created by wangchao on 2017/02/16
*统计主播收入排行榜
*/
@Service("anchorRankingTask")
public class AnchorIncomeRankingTask implements IScheduleTaskDeal {

  private static Logger logger = LoggerFactory.getLogger(AnchorIncomeRankingTask.class);

  @Resource
  private IScheduleExecuteRecordService scheduleExecuteRecordService;
  @Resource
  private WebUserService webUserService;

  /*
   * 主播月收入排行定时任务 0 10 0/3 * * ?
   * 每隔3小时执行一次
   * @param scheduleServer
   * @author wangchao
   * @version 1.0.0
   * @since 2017/02/16 09:25:16
  */
  @Override
  public boolean execute(ScheduleServer scheduleServer) {
    ScheduleExecuteRecord scheduleExecuteRecord = new ScheduleExecuteRecord();
    scheduleExecuteRecord.setExecuteTime(new Date());
    long startTime = System.currentTimeMillis();
    boolean rst = true;
    try {
      webUserService.refreshRanking();
    } catch (Exception e) {
      rst=false;
      logger.error("刷新主播排行榜异常", e);
    }
    long endTime = System.currentTimeMillis();
    scheduleExecuteRecord.setConsumingTime((endTime - startTime));
    scheduleExecuteRecord.setNextExecuteTime(scheduleServer.getNextRunStartTime());
    scheduleExecuteRecord.setTaskType(scheduleServer.getBaseTaskType());
    scheduleExecuteRecord.setStatus(rst ? (short) 1 : (short) 2);
    scheduleExecuteRecord.setExecuteResult("主播月收入排行定时任务");
    scheduleExecuteRecord.setHostName(scheduleServer.getHostName());
    scheduleExecuteRecord.setIp(scheduleServer.getIp());
    scheduleExecuteRecordService.addScheduleExecuteRecord(scheduleExecuteRecord);
    return true;
  }

}
