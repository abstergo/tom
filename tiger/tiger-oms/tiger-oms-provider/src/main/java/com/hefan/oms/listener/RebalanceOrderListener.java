package com.hefan.oms.listener;

import com.alibaba.fastjson.JSON;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.cat.tiger.util.CollectionUtils;
import com.cat.tiger.util.GlobalConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.TopicRegistryDev;
import com.hefan.common.ons.TopicRegistryTest;
import com.hefan.common.ons.listener.GLLMessageListener;
import com.hefan.oms.bean.RebalanceVo;
import com.hefan.oms.bean.WebUserOrder;
import com.hefan.oms.dao.OrderDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by hbchen on 2016/10/29.
 */
@Component
public class RebalanceOrderListener implements GLLMessageListener {

    Logger logger = LoggerFactory.getLogger(RebalanceOrderListener.class);
    @Resource
    OrderDao orderDao;

    @Override
    public TopicRegistry getTopicRegistry() {
        return TopicRegistry.HEFAN_ORDER;
    }

    @Override
    public TopicRegistryDev getTopicRegistryDev() {
        return TopicRegistryDev.HEFAN_ORDER_DEV;
    }

    @Override
    public TopicRegistryTest getTopicRegistryTest() {
        return TopicRegistryTest.HEFAN_ORDER_TEST;
    }

    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {
        logger.info(" oms  RebalanceListener topic={},message={},tag={}", message.getTopic(), new String(message.getBody()), message.getTag());
        try {
            Map map = new ObjectMapper().readValue(message.getBody(), Map.class);
            if (!CollectionUtils.isEmpty(map) && map.containsKey("rebalanceVoOrder")) {
                String rebalanceStr = (String) map.get("rebalanceVoOrder");
                RebalanceVo rebalanceVo = JSON.parseObject(rebalanceStr, RebalanceVo.class);


                if (rebalanceVo.getUserType() == GlobalConstants.USER_TYPE_FLASE){
                    rebalanceVo.setIsFalse(GlobalConstants.FALSE_FANPIAO);
                } else {
                    rebalanceVo.setIsFalse(GlobalConstants.REAL_FANPIAO);
                }


                WebUserOrder webUserOrder = new WebUserOrder();
                webUserOrder.setLiveUuid(rebalanceVo.getLiveUuid()==null?"":rebalanceVo.getLiveUuid());
                webUserOrder.setUserId(rebalanceVo.getFromId());
                webUserOrder.setPresentId(rebalanceVo.getPresentId());
                webUserOrder.setSource(rebalanceVo.getSource());
                webUserOrder.setFanpiaoNum(Double.valueOf(rebalanceVo.getPrice()).intValue());//支出饭票
                webUserOrder.setType(rebalanceVo.getIsFalse());
                webUserOrder.setPresentName(rebalanceVo.getPresentName()==null?"":rebalanceVo.getPresentName());
                webUserOrder.setToUserId(rebalanceVo.getToId());
                webUserOrder.setPresentNum(rebalanceVo.getPresentNum()<=0?1:rebalanceVo.getPresentNum());
                webUserOrder.setBeforeFanpiao(rebalanceVo.getBeforeFanpiao());
                webUserOrder.setUniqueKey(rebalanceVo.getUniqueKey()==null?"":rebalanceVo.getUniqueKey());
                webUserOrder.setMessageId(rebalanceVo.getMessageId());
                orderDao.insertNewRecord(webUserOrder);

            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString());
            return Action.ReconsumeLater;
        }
        return Action.CommitMessage;
    }
}
