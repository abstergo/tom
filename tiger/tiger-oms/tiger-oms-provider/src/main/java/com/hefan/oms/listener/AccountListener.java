package com.hefan.oms.listener;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.TopicRegistryDev;
import com.hefan.common.ons.TopicRegistryTest;
import com.hefan.common.ons.listener.GLLMessageListener;
import com.hefan.oms.itf.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 记录账目明细
 *
 * @author wangchao
 * @create 2016/11/4 10:14
 */
@Component
public class AccountListener implements GLLMessageListener {
  @Override
  public TopicRegistry getTopicRegistry() {
    return TopicRegistry.HEFANTV_OMS_LOG;
  }

  @Override
  public TopicRegistryDev getTopicRegistryDev() {
    return TopicRegistryDev.HEFANTV_OMS_LOG_DEV;
  }

  @Override
  public TopicRegistryTest getTopicRegistryTest() {return TopicRegistryTest.HEFANTV_OMS_LOG_TEST;}

  private Logger logger = LoggerFactory.getLogger(AccountListener.class);

  @Resource
  private AccountService accountService;

  @Override
  @Deprecated
  public Action consume(Message message, ConsumeContext context) {
    /*try {
      if (message == null) {
        logger.error("直播间账目consumer错误，参数信息为空");
        return Action.CommitMessage;
      }
      logger.info(" oms账目  AccountListener topic={},message={},tag={}", message.getTopic(), new String(message.getBody()), message.getTag());
      //解析参数

      Map map = new ObjectMapper().readValue(new String(message.getBody()), Map.class);
      if (CollectionUtils.isEmpty(map) || !map.containsKey("rebalanceVo")) {
        logger.error("直播间账目记录失败，参数信息为空{}", map);
        return Action.CommitMessage;
      }
      String rebalanceVo = MapUtils.getStrValue(map, "rebalanceVo", "");
      logger.info("账目内容1{}", rebalanceVo);
      rebalanceVo= URLDecoder.decode(rebalanceVo,"UTF-8");
      if (StringUtils.isBlank(rebalanceVo)) {
        logger.error("直播间账目记录失败，rebalanceVo={}",rebalanceVo);
        return Action.CommitMessage;
      }

      logger.info("账目内容2{}", rebalanceVo);
      accountService.rebalanceLivePresent(JSON.parseObject(rebalanceVo, RebalanceVo.class));
    } catch (Exception e) {
      logger.error("账目扣费后IM通知异常{}", e);
      return Action.ReconsumeLater;
    }*/
    return Action.CommitMessage;
  }


}
