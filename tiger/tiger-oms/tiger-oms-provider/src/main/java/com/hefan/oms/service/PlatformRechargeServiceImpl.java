package com.hefan.oms.service;

import com.hefan.oms.dao.CurrencyExchangeDao;
import com.hefan.oms.itf.PlatformRechargeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * Created by hbchen on 2016/9/27.
 */
@Component("platformRechargeService")
public class PlatformRechargeServiceImpl implements PlatformRechargeService {

    public static Logger logger = LoggerFactory.getLogger(PresentServiceImpl.class);

    @Resource
    CurrencyExchangeDao currencyExchangeDao;

    public Integer getMealTicketsByRechargeRuleInfo(Integer rechargeType, Integer rechargeMoney){

        if (null == rechargeType || null == rechargeMoney)
            return null;
        Map rechargeRuleMap = currencyExchangeDao.selectByRuleType(rechargeType);
        if (null != rechargeRuleMap && !rechargeRuleMap.isEmpty()) {
            Integer exchangeRatio = (int) rechargeRuleMap.get("exchangeRatio");
            Integer beExchangeRatio = (int) rechargeRuleMap.get("beExchangeRatio");
            return rechargeMoney * beExchangeRatio / exchangeRatio;
        }
        return null;
    }

}
