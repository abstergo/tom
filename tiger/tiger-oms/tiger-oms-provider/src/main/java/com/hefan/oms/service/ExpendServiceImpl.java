package com.hefan.oms.service;

import com.hefan.oms.bean.RebalanceVo;
import com.hefan.oms.bean.WebUserExpend;
import com.hefan.oms.dao.ExpendDao;
import com.hefan.oms.itf.ExpendService;
import com.hefan.user.bean.WebUserIdentity;
import com.hefan.user.itf.UserIdentityService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by hbchen on 2016/10/28.
 */
@Component("expendService")
public class ExpendServiceImpl implements ExpendService {

    @Resource
    ExpendDao expendDao;

    @Resource
    private UserIdentityService userIdentityService;

    @Override
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public void saveExpend(RebalanceVo rebalanceVo,int fanPiaoNum)throws Exception {
        WebUserExpend we = new WebUserExpend();
        we.setLiveUuid(rebalanceVo.getLiveUuid());
        we.setUserId(rebalanceVo.getFromId());
        we.setPresentId(rebalanceVo.getPresentId());
        we.setSource(rebalanceVo.getSource());
        we.setFanpiaoNum(fanPiaoNum);//支出饭票
        we.setType(rebalanceVo.getIsFalse());
        we.setPresentName(rebalanceVo.getPresentName());
        we.setToUserId(rebalanceVo.getToId());
        we.setPresentNum(1);
        we.setBeforeFanpiao(rebalanceVo.getBeforeFanpiao());
        WebUserIdentity userIdentity= userIdentityService.getUserIndentityInfo(rebalanceVo.getFromId());
        we.setPid(userIdentity == null ? 0 : userIdentity.getPid());
        we.setMessageId(rebalanceVo.getMessageId());
        expendDao.insertNewRecord(we);
    }

    @Override
    @Transactional(readOnly = true)
    public long findUserExpendFianpiao(String userId, Date beginDate, Date endDate) {
        return expendDao.findUserExpendFanpiao(userId,beginDate,endDate);
    }
}
