package com.hefan.oms.dao;

import com.hefan.oms.bean.Present;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * User: criss Date: 16/9/21 Time: 15:20
 */
@Repository
public class PresentDao {

	@Resource
	private JdbcTemplate jdbcTemplate;

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}


	/**
	 * 直播间礼物列表
	 * 
	 * @return
	 */
	public List getLivePresent() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELEC ");
		sql.append(" type,present_name,icon,showpic,price,experience,rank,iscontinue,iseffect,exclusive");
		sql.append(" FROM present");
		sql.append(" WHERE");
		sql.append(" isputaway=0");
		sql.append(" AND status=0");
		sql.append(" zf=0");
		sql.append(" unusable_icon");
		return jdbcTemplate.queryForList(sql.toString());
	}

	/**
	 * 动态礼物
	 * 
	 * @return
	 */
	public List getDynamicPresent() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELEC ");
		sql.append(" type,present_name,icon,showpic,price,experience,rank,iscontinue,iseffect,exclusive");
		sql.append(" FROM present");
		sql.append(" WHERE");
		sql.append(" isputaway=0");
		sql.append(" AND status=1");
		sql.append(" zf=0");
		sql.append(" unusable_icon");
		return jdbcTemplate.queryForList(sql.toString());
	}

	/**
	 * version
	 * 
	 * @return
	 */
	public List getPresentVersion() {
		StringBuilder sql = new StringBuilder();
		sql.append("SELEC ");
		sql.append(" present_version Version");
		sql.append(" FROM app_present_info");
		sql.append(" WHERE");
		sql.append(" delete_flag=0");
		sql.append(" order by create_time desc");
		sql.append("  limit 1");
		return jdbcTemplate.queryForList(sql.toString());
	}

	public int getPresentPriceById(long presentId) {
		String sql = " select price from present where id=? ";
		try {
			return jdbcTemplate.queryForObject(sql, Integer.class, presentId);
		} catch (EmptyResultDataAccessException e) {
			return 0;
		}
	}

	public Present getPresentById(long presentId) {
		String sql = " select id,price,experience from present where id = ? ";
		try {
			return jdbcTemplate.queryForObject(sql, new PresentRowMapper(), presentId);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}

	/**
	 * 获取礼物全部信息
	 * @param presentId
	 * @return
     */
	public Present getPresentByPresentId(long presentId){
		String sql = " select * from present where id = ? and isputaway=0 and zf=0 limit 1";
		List<Object> params = new ArrayList<Object>();
		params.add(presentId);
		List<Present> list = getJdbcTemplate().query(sql, params.toArray(), new BeanPropertyRowMapper<Present>(Present.class));
		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	/**
	 * 根据礼物ID获取礼物信息
	 * @param presentId
	 * @return
	 */
	public Present getPresentInfoByPresentId(long presentId){
		String sql = " select * from present where id = ? limit 1";
		List<Object> params = new ArrayList<Object>();
		params.add(presentId);
		List<Present> list = getJdbcTemplate().query(sql, params.toArray(), new BeanPropertyRowMapper<Present>(Present.class));
		if (CollectionUtils.isNotEmpty(list)) {
			return list.get(0);
		}
		return null;
	}

	class PresentRowMapper implements RowMapper<Present> {
		// rs为返回结果集，以每行为单位封装着
		public Present mapRow(ResultSet rs, int rowNum) throws SQLException {

			Present present = new Present();
			present.setId(rs.getLong("id"));
			present.setPrice(rs.getInt("price"));
			present.setExperience(rs.getInt("experience"));
			present.setType(rs.getInt("type"));
			present.setIscontinue(rs.getInt("iscontinue"));
			return present;

		}

	}
}
