package com.hefan.oms.dao;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.oms.bean.ExchangeDetail;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by hbchen on 2016/10/10.
 */
@Repository
public class ExchangeDao extends BaseDaoImpl<ExchangeDetail> {
    
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public long insertExchangeDetail(ExchangeDetail exchangeDetail){
        exchangeDetail =  super.save(exchangeDetail);
        return exchangeDetail.getId();
    }

}
