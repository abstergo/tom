package com.hefan.oms.dao;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.oms.bean.WebUserExpend;
import com.hefan.oms.bean.WebUserOrder;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by hbchen on 2016/10/8.
 */
@Repository
public class OrderDao extends BaseDaoImpl<WebUserOrder> {

    public long insertNewRecord(WebUserOrder record) {

        record = super.save(record);

        return record.getId();

    }
}
