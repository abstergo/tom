package com.hefan.oms.service;

import com.cat.common.meta.UserTypeEnum;
import com.cat.tiger.util.CollectionUtils;
import com.cat.tiger.util.Fraction;
import com.cat.tiger.util.GlobalConstants;
import com.google.common.collect.Interner;
import com.google.common.collect.Maps;
import com.hefan.oms.config.OmsConfigCenter;
import com.hefan.oms.dao.CurrencyExchangeDao;
import com.hefan.oms.itf.ExchangeRuleService;
import com.hefan.user.bean.WebUserIdentity;
import com.hefan.user.itf.UserIdentityService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component("exchangeRuleService")
public class ExchangeRuleServiceImpl implements ExchangeRuleService{
	
	
	@Resource
	CurrencyExchangeDao currencyExchangeDao;
	
	@Resource
	UserIdentityService userIdentityService;

	@Resource
	private OmsConfigCenter omsConfigCenter;
	
	@Override
	public Map<String, Object> ExchangeRuleForHeFanOrFanPiaoByUserId(String userId) {
		/**
		 * 盒饭饭票兑换比例
		 */
		Map exRuleMap = currencyExchangeDao.selectByRuleType(GlobalConstants.EX_RULE_HEFAN_FANPIAO);
		/**
		 * 主播盒饭分成比例
		 */
		WebUserIdentity webUserIdentity = userIdentityService.getUserIndentityInfo(userId);
		return getExchangeRuleForHeFanOrFanPiao(exRuleMap,webUserIdentity);
	}

	@Override
	public Map<String,Object> ExchangeRuleForHeFanOrFanPiaoByUserId(String userId, int userType) {
		if(UserTypeEnum.isAnchor(userType)) { //主播兑换比例
			return ExchangeRuleForHeFanOrFanPiaoByUserId(userId);
		} else {
			//普通用户兑换比例
			Map exRuleMap = currencyExchangeDao.selectByRuleType(GlobalConstants.EX_RULE_HEFAN_FANPIAO);
			return getExchangeRuleForHeFanOrFanPiao(exRuleMap);
		}
	}

	@Override
	public List<Integer> getCurrencyExchangeCenterPrice() {
		return currencyExchangeDao.getCurrencyExchangeCenterPrice();
	}

	/**
	 * 计算兑换比例
	 * @param exRuleMap
	 * @param webUserIdentity
	 * @return
	 */
	private Map<String, Object> getExchangeRuleForHeFanOrFanPiao(Map exRuleMap,WebUserIdentity webUserIdentity){
		Map<String, Object> resultMap = Maps.newHashMap();
		Integer exchangeRatioOld =  (Integer)exRuleMap.get("exchangeRatio"); //1
		Integer beExchangeRatioOld =  (Integer)exRuleMap.get("beExchangeRatio");//10
		Integer platformIntoRatio = webUserIdentity.getPlatformIntoRatio();//60
        Integer groupIntoRatio = webUserIdentity.getGroupIntoRatio();//10
        Integer personalIntoRatio = webUserIdentity.getPersonalIntoRatio();//30
		if(personalIntoRatio.intValue() == 0) {
			return null;
		}
        /*Fraction fraction = new Fraction(personalIntoRatio,100);
        Fraction fraction1 = fraction.division(beExchangeRatioOld);
        Fraction fraction2 = fraction1.division1(exchangeRatioOld);*/
		Fraction fraction = new Fraction(exchangeRatioOld,beExchangeRatioOld);
		Fraction fraction1 = fraction.division(personalIntoRatio);
		Fraction fraction2 = fraction1.division1(100);
        resultMap.put("exchangeRatio", fraction2.getDenominator());
        resultMap.put("beExchangeRatio", fraction2.getNumerator());
		return resultMap;
	}

	/**
	 * 普通用户盒饭兑换饭票实际比例兑换
	 * @param exRuleMap
	 * @return
     */
	private Map<String,Object> getExchangeRuleForHeFanOrFanPiao(Map exRuleMap){
		Map<String, Object> resultMap = Maps.newHashMap();
		Integer exchangeRatioOld =  (Integer)exRuleMap.get("exchangeRatio"); //1
		Integer beExchangeRatioOld =  (Integer)exRuleMap.get("beExchangeRatio");//10
		Map<String,String> configMap = omsConfigCenter.getPublicConfig();
		Integer personalIntoRatio = 0;
		if(!CollectionUtils.isEmpty(configMap) && configMap.containsKey("commUserHfToFpRatio")) {
			personalIntoRatio = Integer.valueOf(String.valueOf(configMap.get("commUserHfToFpRatio")));
		} else {
			return null;
		}
		Fraction fraction = new Fraction(exchangeRatioOld,beExchangeRatioOld);
		Fraction fraction1 = fraction.division(personalIntoRatio);
		Fraction fraction2 = fraction1.division1(100);
		resultMap.put("exchangeRatio", fraction2.getDenominator());
		resultMap.put("beExchangeRatio", fraction2.getNumerator());
		return resultMap;
	}

}
