package com.hefan.oms.service;

import com.hefan.oms.bean.AnchorTicketDay;
import com.hefan.oms.dao.AnchorTicketDayDao;
import com.hefan.oms.itf.AnchorTicketDayService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by hbchen on 2016/9/29.
 */
@Component("anchorTicketDayService")
public class AnchorTicketDayServiceImpl implements AnchorTicketDayService {

    @Resource
    AnchorTicketDayDao anchorTicketDayDao;

    public int addNewAnchorTicketDay(AnchorTicketDay record){
        return anchorTicketDayDao.initAnchorTicket(record);
    }

    @Override
    public int selectAvailableTickets(String userId) {
        return anchorTicketDayDao.selectAvailableTickets(userId);
    }


    public void initAnchorTicketDayWhenLiveStart(String userId, Date sateDate){
        try {
            SimpleDateFormat ymFormat =	new SimpleDateFormat("yyyy-MM");
            SimpleDateFormat dayFormat =	new SimpleDateFormat("yyyy-MM-dd");
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(sateDate);
            String cym = ymFormat.format(calendar.getTime());
            Date cDay = dayFormat.parse(dayFormat.format(calendar.getTime()));
            AnchorTicketDay cAnchorTicketDay = new AnchorTicketDay();
            cAnchorTicketDay.setUserId(userId);
            cAnchorTicketDay.setYearsMonth(cym);
            cAnchorTicketDay.setCreateDate(cDay);
            addNewAnchorTicketDay(cAnchorTicketDay);

            calendar.add(Calendar.HOUR_OF_DAY, 24);
            String nym = ymFormat.format(calendar.getTime());
            Date nDay = dayFormat.parse(dayFormat.format(calendar.getTime()));
            AnchorTicketDay nAnchorTicketDay = new AnchorTicketDay();
            nAnchorTicketDay.setUserId(userId);
            nAnchorTicketDay.setYearsMonth(nym);
            nAnchorTicketDay.setCreateDate(nDay);
            addNewAnchorTicketDay(nAnchorTicketDay);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
