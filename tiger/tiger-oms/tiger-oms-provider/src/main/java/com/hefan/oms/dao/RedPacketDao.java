package com.hefan.oms.dao;

import com.hefan.common.exception.DataIllegalException;
import com.hefan.oms.bean.OrderRedPacket;
import com.hefan.oms.bean.RedPacket;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hbchen on 2016/10/12.
 */
@Repository
public class RedPacketDao {

  @Resource
  private JdbcTemplate jdbcTemplate;

  public JdbcTemplate getJdbcTemplate() {
    return jdbcTemplate;
  }

  public RedPacket selectRedPackInfo(int id) {
    String sql = " select id,name,amount,number,create_user,create_time,update_user,update_time,delete_flag from red_packet where id= ? limit 1";
    List<Object> params = new ArrayList<Object>();
    params.add(id);
    List<RedPacket> list = getJdbcTemplate().query(sql, params.toArray(), new BeanPropertyRowMapper<RedPacket>(RedPacket.class));
    if (CollectionUtils.isNotEmpty(list)) {
      return list.get(0);
    }
    return null;

  }

  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
  public void updateRedPackOrderStatus(long orderId) throws Exception {

    String sql = " update order_redpacket set status = 1 where id=  ? ";

    int id = getJdbcTemplate().update(sql, new Object[] { orderId });
    if (id <= 0) {
      throw new DataIllegalException(String.format("更新红包=%s状态失败", orderId));
    }
  }

  public void updateRedPackOrderStatus(String liveUuid) {

    String sql = " update order_redpacket set status = 1 where live_uuid=  ? and status = 0";

    getJdbcTemplate().update(sql, new Object[] { liveUuid });

  }

  public OrderRedPacket getOrderPacketbyId(long id) {

    String sql = " select id,user_id,packet_num,fanpiao_num,live_uuid,create_time,delete_flag,update_time,status from  order_redpacket where id =? and status=0";
    List<Object> params = new ArrayList<Object>();
    params.add(id);
    List<OrderRedPacket> list = jdbcTemplate.query(sql, params.toArray(), new BeanPropertyRowMapper<OrderRedPacket>(OrderRedPacket.class));
    if (CollectionUtils.isNotEmpty(list)) {
      return list.get(0);
    }
    return null;

  }

  public boolean isGrabRedPack(long orderId, String userId) {
    String sql = " select count(1) from  order_redpacket_detail where order_id =? and user_id=? and delete_flag=0";
    return jdbcTemplate.queryForObject(sql, new Object[] { orderId, userId }, int.class) >0;
  }
  @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
  public Integer updateRedPack2Platform(String liveUuid) {
    String sql = " select sum(fanpiao_num) from order_redpacket_detail where live_uuid=? and delete_flag=0 and user_id =''";
    return jdbcTemplate.queryForObject(sql, new Object[] {liveUuid}, Integer.class);
  }
}
