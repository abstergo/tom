package com.hefan.oms.listener;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.util.CollectionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.TopicRegistryDev;
import com.hefan.common.ons.TopicRegistryTest;
import com.hefan.common.ons.listener.GLLMessageListener;
import com.hefan.common.util.MapUtils;
import com.hefan.notify.itf.ImChatroomService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 扣费成功后im通知前端
 *
 * @author wangchao
 * @create 2016/11/2 15:00
 */
@Component
public class RebalanceImListener implements GLLMessageListener {

  private Logger logger = LoggerFactory.getLogger(RebalanceListener.class);
  @Resource
  private ImChatroomService imChatroomService;

  @Override
  public TopicRegistry getTopicRegistry() {
    return TopicRegistry.HEFANTV_OMS_IM;
  }

  @Override
  public TopicRegistryDev getTopicRegistryDev() {
    return TopicRegistryDev.HEFANTV_OMS_IM_DEV;
  }

  @Override
  public TopicRegistryTest getTopicRegistryTest() {return TopicRegistryTest.HEFANTV_OMS_IM_TEST;}

  @Override
  public Action consume(Message message, ConsumeContext context) {
    try {
      if (message == null) {
        logger.error("IM通知失败，参数信息为空");
        return Action.CommitMessage;
      }
      logger.info(" oms  RebalanceImListener topic={},message={},tag={}", message.getTopic(), new String(message.getBody()), message.getTag());
      //解析参数
      Map map = new ObjectMapper().readValue(new String(message.getBody()), Map.class);
      if (CollectionUtils.isEmpty(map)) {
        logger.error("IM扣费失败，参数信息为空{}", message.getBody());
        return Action.CommitMessage;
      }
      String roomId = MapUtils.getStrValue(map, "roomId", "");
      String imMessage = MapUtils.getStrValue(map, "imMessage", "");
      String toUserId = MapUtils.getStrValue(map, "toUserId", "");
      String fromUser = MapUtils.getStrValue(map, "fromUser", "");
      /*imMessage = URLDecoder.decode(imMessage, "UTF-8");*/
      if (StringUtils.isBlank(imMessage) || StringUtils.isBlank(roomId) || StringUtils.isBlank(fromUser) || StringUtils.isBlank(toUserId)) {
        logger.error("IM扣费后IM通知失败，roomId={},imMessage={},fromUser={},toUser={}", roomId, imMessage, fromUser, toUserId);
        return Action.CommitMessage;
      }

      logger.info("IM通知内容 房间号={},消息发送人={},消息内容={},送礼人={}", roomId, toUserId, imMessage, fromUser);
      ResultBean resultBean = imChatroomService.chatroomSendMsg(Long.parseLong(roomId), toUserId, 100, 0, imMessage, fromUser);
      if (resultBean.getCode() == ResultCode.UNSUCCESS.get_code()) {
        logger.error("IM error : " + resultBean.getMsg());
      }
    } catch (Exception e) {
      logger.error("IM扣费后IM通知异常{}", e);
      return Action.ReconsumeLater;
    }
    return Action.CommitMessage;

  }
}
