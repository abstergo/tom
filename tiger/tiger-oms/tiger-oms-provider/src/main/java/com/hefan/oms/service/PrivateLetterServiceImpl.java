package com.hefan.oms.service;

import com.hefan.oms.bean.PrivateLetter;
import com.hefan.oms.dao.PrivateLetterDao;
import com.hefan.oms.itf.PrivateLetterService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by hbchen on 2016/10/19.
 */
@Component("privateLetterService")
public class PrivateLetterServiceImpl implements PrivateLetterService {
    @Resource
    PrivateLetterDao privateLetterDao;
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public long addNewPrivateLetter(PrivateLetter privateLetter) {
        return privateLetterDao.insertNewRecord(privateLetter);
    }


}
