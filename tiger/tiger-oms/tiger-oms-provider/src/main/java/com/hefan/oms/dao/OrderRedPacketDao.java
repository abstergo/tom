package com.hefan.oms.dao;

import com.alibaba.fastjson.JSON;
import com.hefan.common.exception.DataIllegalException;
import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.oms.bean.OrderRedPacket;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hbchen on 2016/10/12.
 */
@Repository
public class OrderRedPacketDao  extends BaseDaoImpl<OrderRedPacket> {

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public long insertNewRecord(OrderRedPacket record) throws Exception{
        record = super.save(record);
        if(record.getId()<=0){
            throw new DataIllegalException(String.format("保存红包记录失败%s", JSON.toJSONString(record)));
        }
        return record.getId();

    }

    public List<OrderRedPacket> selectUnFinishRedPackList(String liveUuid){
        String sql = " select id,user_id,packet_num,fanpiao_num,live_uuid,create_time,delete_flag,update_time,status from order_redpacket where status = 0 and delete_flag = 0  and live_uuid = ? ";
        List<Object> params = new ArrayList<Object>();
        params.add(liveUuid);
        List<OrderRedPacket> list = getJdbcTemplate().query(sql, params.toArray(), new BeanPropertyRowMapper(OrderRedPacket.class));
        if (CollectionUtils.isNotEmpty(list)) {
            return list;
        }
        return null;

    }
}
