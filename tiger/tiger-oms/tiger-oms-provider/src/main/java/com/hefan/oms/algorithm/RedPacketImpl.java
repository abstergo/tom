package com.hefan.oms.algorithm;

import com.cat.common.util.BigDecimalCalUtil;

import java.math.BigDecimal;
import java.util.Random;

/**
 * Created by hbchen on 2016/10/13.
 */
public class RedPacketImpl implements RedPacketAlgorithm{
    @Override
    public  double[] decompose(double money, int num) {
        Random r = new Random();
       // DecimalFormat format = new DecimalFormat(".");

        double middle = Math.floor(money/num);
        double [] dou = new double[num];
        double redMoney = 0;
        double nextMoney = money;
        double sum = 0;
        int index = 0;
        for(int i=num;i>0;i--){
            if(i == 1){
                dou[index] = nextMoney;
            }else{
                while(true){
                    redMoney = Math.ceil(r.nextDouble()*middle);
                    if(BigDecimalCalUtil.compareTo(BigDecimal.valueOf(redMoney),BigDecimal.ZERO) == 1){
                        break;
                    }
                }
                nextMoney = nextMoney - redMoney;
                sum  = sum + redMoney;
                dou[index] = redMoney;
                middle =  Math.floor(nextMoney/(i-1));
                index++;
            }
        }
        return dou;
    }

    /*public static void main(String args []){
        RedPacketImpl redPacket = new RedPacketImpl();

       double [] dou =  redPacket.decompose(50,46);
        for (double i :dou){
            System.out.println(i);
        }
    }*/
}
