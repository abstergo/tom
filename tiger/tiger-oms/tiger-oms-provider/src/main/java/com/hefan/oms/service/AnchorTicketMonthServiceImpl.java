package com.hefan.oms.service;

import com.alibaba.fastjson.JSON;
import com.hefan.common.exception.DataIllegalException;
import com.hefan.common.util.DateUtils;
import com.hefan.live.bean.LiveLog;
import com.hefan.live.bean.LiveRoom;
import com.hefan.live.itf.LiveLogService;
import com.hefan.live.itf.LiveRoomService;
import com.hefan.oms.bean.AnchorTicketMonth;
import com.hefan.oms.dao.AnchorTicketMonthDao;
import com.hefan.oms.itf.AnchorTicketMonthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by hbchen on 2016/9/29.
 */
@Component("anchorTicketMonthService")
public class AnchorTicketMonthServiceImpl implements AnchorTicketMonthService {

    @Resource
    private AnchorTicketMonthDao anchorTicketMonthDao;
    @Resource
    private LiveLogService liveLogService;
    @Resource
    private LiveRoomService liveRoomService;

    private Logger logger = LoggerFactory.getLogger(AnchorTicketMonthServiceImpl.class);

    @Override
    public int selectAvailableTickets(String userId) {
        Integer year_months = Integer.valueOf(DateUtils.currentDate("yyyyMM"));
        return anchorTicketMonthDao.selectAvailableTickets(userId,year_months);
    }
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public void updateAnchorMonth(String userId, long HefanNum, boolean isTrue, int month) throws Exception {
        logger.info("更新主播id={}月表,盒饭数={},真实虚拟{}",userId,HefanNum,isTrue);
        //Integer year_months = Integer.valueOf(DateUtils.currentDate("yyyyMM"));
        //检查后台统计表是否有该主播的数据，若无，初始化，若有，无操作
        AnchorTicketMonth anchorTicketMonth = new AnchorTicketMonth();
        anchorTicketMonth.setUserId(userId);
        anchorTicketMonth.setYearsMonth(month);
        if (isTrue) {
            anchorTicketMonth.setRealTicket(HefanNum);
            anchorTicketMonth.setFalseTicket(0);
        }else {
            anchorTicketMonth.setFalseTicket(HefanNum);
            anchorTicketMonth.setRealTicket(0);
        }
        //先查询当前月份是否记录
        int result = anchorTicketMonthDao.selectAnchorTicketMonthCount(anchorTicketMonth);
        if (result <= 0) {
            if (anchorTicketMonthDao.initAnchorTicket(anchorTicketMonth) < 1) {
                logger.error("新增主播月统计失败事务回滚{}", JSON.toJSONString(anchorTicketMonth));
                throw new RuntimeException("初始化月表数据失败" + anchorTicketMonth.getUserId() + " month" + anchorTicketMonth.getYearsMonth());
            }
        } else {
            int updateResult = anchorTicketMonthDao.updateAnchorTicket(anchorTicketMonth);
            if (updateResult <= 0) {
                logger.error("更新主播月统计失败事务回滚{}", JSON.toJSONString(anchorTicketMonth));
                throw new RuntimeException("更新月表数据失败" + anchorTicketMonth.getUserId() + " month" + anchorTicketMonth.getYearsMonth());
            }
        }
    }
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void dealAnchorLiveDetail(LiveLog liveLog) {
        logger.info("统计主播{}liveLog{}", liveLog.getUserId(), JSON.toJSONString(liveLog));
        int result = liveLogService.updateLiveLogForRebalance(liveLog);
        if (result <= 0) {
            logger.error("统计主播{}直播id={}异常", liveLog.getUserId(), liveLog.getLiveUuid());
            throw new DataIllegalException(String.format("统计主播%s直播id=%s异常", liveLog.getUserId(), liveLog.getLiveUuid()));
        }
        LiveRoom liveRoom = new LiveRoom();
        liveRoom.setTicketHistory(liveLog.getFalseTicketCount() + liveLog.getTicketCount());
        liveRoom.setUserId(liveLog.getUserId());
        int id = liveRoomService.updateLiveRoomForRebalance(liveRoom);
        if (id != 1) {
            throw new DataIllegalException(String.format("更新主播的固定直播间信息异常数据回滚%s", JSON.toJSONString(liveLog)));
        }

    }
}
