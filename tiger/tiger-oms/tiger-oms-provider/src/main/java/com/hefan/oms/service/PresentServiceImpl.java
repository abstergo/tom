package com.hefan.oms.service;

import java.util.List;

import javax.annotation.Resource;

import com.hefan.oms.bean.Present;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.hefan.oms.dao.PresentDao;
import com.hefan.oms.itf.PresentService;

/**
 * User: criss Date: 16/9/20 Time: 08:34
 */

@Component("presentService")
public class PresentServiceImpl implements PresentService {

	public static Logger logger = LoggerFactory.getLogger(PresentServiceImpl.class);

	@Resource
	private PresentDao presentDao;


	public int getPresentPriceById(Long presentId) {
		return presentDao.getPresentPriceById(presentId);
	}

	@Override
	public Present getPresentById(Long presentId) {
		return presentDao.getPresentByPresentId(presentId);
	}

	/**
	 * 根据礼物ID获取礼物信息
	 * @param presentId
	 * @return
	 */
	@Override
	public Present getPresentInfoById(Long presentId) {
		logger.info("获取礼物信息");
		return presentDao.getPresentInfoByPresentId(presentId);
	}

	@Override
	public List dynamicPresent() {
		// TODO Auto-generated method stub
		return presentDao.getDynamicPresent();
	}


	@Override
	public List livePresent() {
		// TODO Auto-generated method stub
		return presentDao.getLivePresent();
	}


	@Override
	public List presentVersion() {
		// TODO Auto-generated method stub
		return presentDao.getPresentVersion();
	}
}
