package com.hefan.oms.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by hbchen on 2016/9/27.
 */
@Repository
public class CurrencyExchangeDao {

    @Resource
    JdbcTemplate jdbcTemplate;

    public Map selectByRuleType(Integer exchangeRuleType) {
        String sql = "SELECT exchange_ratio exchangeRatio,be_exchange_ratio beExchangeRatio, note " +
                "FROM currency_exchange ";
        sql+=" WHERE exchange_rule_type = ? AND zf = 0 LIMIT 1 ";
        return jdbcTemplate.queryForMap(sql,exchangeRuleType);
    }

    /**
     * 获取兑换中心兑换饭票
     * @return
     */
    public List<Integer> getCurrencyExchangeCenterPrice(){
        String sql = "SELECT c.price FROM currency_exchange_center_price c WHERE c.status = 0 ORDER BY c.price ASC";
        return jdbcTemplate.queryForList(sql,Integer.class);
    }


}
