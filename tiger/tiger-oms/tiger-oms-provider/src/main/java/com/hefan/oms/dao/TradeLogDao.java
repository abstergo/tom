package com.hefan.oms.dao;

import com.alibaba.fastjson.JSON;
import com.hefan.common.exception.DataIllegalException;
import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.oms.bean.TradeLog;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by hbchen on 2016/9/27.
 */
@Repository
public class TradeLogDao extends BaseDaoImpl<TradeLog> {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    private static String TABLE_NAME = "trade_log";

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public long insertNewRecord(TradeLog record) throws Exception {

        record = super.save(record);
        if(record.getId()<=0){
            throw new DataIllegalException(String.format("保存红包记录失败%s", JSON.toJSONString(record)));
        }

        return record.getId();
    }


}
