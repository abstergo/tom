package com.hefan.oms.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by hbchen on 2016/9/30.
 * 对应支出表
 */
@Entity(tableName = "web_user_order")
public class WebUserOrder extends BaseEntity implements Serializable {

    private long id;
    @Column("user_id")
    private String userId;//用户id
    @Column("present_id")
    private long presentId;//礼物id
    @Column("fanpiao_num")
    private int fanpiaoNum;//饭票数量
    @Column("source")
    private int source;//来源：'0直播间 1俱乐部 2 私信 3 弹幕'

    private Date createTime;
    @Column("message_id")
    private int messageId;//动态id，如果是俱乐部礼物，要记录是哪个动态
    @Column("live_uuid")
    private String liveUuid;//直播的uuid
    @Column("type")
    private int type;//支出类型 真1  假0
    @Column("to_user_id")
    private String toUserId;//收礼人
    @Column("present_num")
    private int presentNum;  //礼物数量
    @Column("present_name")
    private String presentName;//礼物名称
    @Column("balance")
    private long beforeFanpiao;//扣款时余额
    @Column("unique_key")
    private String uniqueKey;

    @Override
    public long getId() {
        return id;
    }

    @Override
    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getPresentId() {
        return presentId;
    }

    public void setPresentId(long presentId) {
        this.presentId = presentId;
    }

    public int getFanpiaoNum() {
        return fanpiaoNum;
    }

    public void setFanpiaoNum(int fanpiaoNum) {
        this.fanpiaoNum = fanpiaoNum;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public int getPresentNum() {
        return presentNum;
    }

    public void setPresentNum(int presentNum) {
        this.presentNum = presentNum;
    }

    public String getPresentName() {
        return presentName;
    }

    public void setPresentName(String presentName) {
        this.presentName = presentName;
    }

    public long getBeforeFanpiao() {
        return beforeFanpiao;
    }

    public void setBeforeFanpiao(long beforeFanpiao) {
        this.beforeFanpiao = beforeFanpiao;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }
}
