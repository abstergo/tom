package com.hefan.oms.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by hbchen on 2016/9/30.
 */
@Entity(tableName = "web_user_income")
public class WebUserIncome extends BaseEntity implements Serializable {

    private long id;
    @Column("user_id")
    private String userId;//用户id
    @Column("present_id")
    private long presentId;//礼物id

    @Column("hefan_num")
    private int hefanNum;//盒饭数量
    @Column("source")
    private int source;//来源：'0直播间 1俱乐部 2 私信 3 弹幕'
    @Column("create_time")
    private Date createTime;
    @Column("message_id")
    private int messageId;//动态id，如果是俱乐部礼物，要记录是哪个动态
    @Column("live_uuid")
    private String liveUuid;//直播的uuid
    @Column("person_hefan_radio")
    private int personHefanRadio;//个人应得比例
    @Column("superior_hefan_radio")
    private int superiorHefanRadio;//上级应得比例
    @Column("platform_hefan_radio")
    private int platformHefanRadio;//平台应得比例
    @Column("present_num")
    private int presentNum;
    @Column("from_user_id")
    private String fromId;
    @Column("present_name")
    private String presentName;
    @Column("type")
    private int type;//0虚拟支出，1正常支出
    @Column("pid")
    private int pid;//送礼人父id
    @Column("is_old")
    private int isOld;//扣费优化上线，区分新老数据 老数据默认0，新增默认1

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getPresentId() {
        return presentId;
    }

    public void setPresentId(long presentId) {
        this.presentId = presentId;
    }

    public int getHefanNum() {
        return hefanNum;
    }

    public void setHefanNum(int hefanNum) {
        this.hefanNum = hefanNum;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public int getPersonHefanRadio() {
        return personHefanRadio;
    }

    public void setPersonHefanRadio(int personHefanRadio) {
        this.personHefanRadio = personHefanRadio;
    }

    public int getSuperiorHefanRadio() {
        return superiorHefanRadio;
    }

    public void setSuperiorHefanRadio(int superiorHefanRadio) {
        this.superiorHefanRadio = superiorHefanRadio;
    }

    public int getPlatformHefanRadio() {
        return platformHefanRadio;
    }

    public void setPlatformHefanRadio(int platformHefanRadio) {
        this.platformHefanRadio = platformHefanRadio;
    }

    public int getPresentNum() {
        return presentNum;
    }

    public void setPresentNum(int presentNum) {
        this.presentNum = presentNum;
    }

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getPresentName() {
        return presentName;
    }

    public void setPresentName(String presentName) {
        this.presentName = presentName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getIsOld() {
        return isOld;
    }

    public void setIsOld(int isOld) {
        this.isOld = isOld;
    }
}
