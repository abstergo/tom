package com.hefan.oms.itf;

import com.hefan.oms.bean.RebalanceVo;
import com.hefan.oms.bean.RedPacketVo;
import com.hefan.user.bean.WebUser;

import java.util.List;
import java.util.Map;

/**
 * Created by hbchen on 2016/10/28.
 */
public interface MessageService {

  /**
   * 记录私信
   *
   * @param rebalanceVo
   * @return
   * @throws
   * @author wangchao
   * @create 2016/11/3 17:34
   */
  void reBalanceMessage(RebalanceVo rebalanceVo);

  /**
   * 直播间IM通讯
   *
   * @param rebalanceVo
   * @param type 业务类型
   * @param content 内容
   * @param webUser 发送者
   * @param userLevel 等级
   * @author wangchao
   * @throws Exception
   */
  void dealImMessage(RebalanceVo rebalanceVo, String type, String content, WebUser webUser, int userLevel) throws Exception;

  /**
   * 私信IM通讯
   * @param rebalanceVo
   * @param webUser
   * @author wangchao
   */
  void dealPrivateMessage(RebalanceVo rebalanceVo, WebUser webUser) throws Exception;


  /**
   * //TODO (这里用一句话描述这个方法的作用 - 必填)
   * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
   * @param redPacketVo
   * @param type 自定义消息类型
   * @param content 消息内容
   * @param webUser 发送user
   * @param plist 中奖人list
   * @param orderId 红包orderId
   * @return
   * @throws
   * @author wangchao
   * @create 2016/12/7 18:12
   */
  void dealRedPacketImMessage(RedPacketVo redPacketVo, String type,String content, WebUser webUser,List<Map<String,String>> plist,long orderId) ;
}
