package com.hefan.oms.itf;

import com.hefan.live.bean.LiveLog;
import com.hefan.oms.bean.RebalanceVo;
import com.hefan.user.bean.WebUser;

import java.util.List;
import java.util.Map;

/**
 * 账目明细记录
 *
 * @author wangchao
 * @create 2016/11/4 10:23
 */
public interface AccountService {

  /**
   * 记录用户消费明细、升级IM，明星收入明细、统计月收入、直播收入等
   * @description Deprecated  by 20161228 王超，取消异步操作，改为定时
   * @param rebalanceVo
   * @param webUser
   * @throws Exception
   */
  @Deprecated
  void rebalanceLivePresent(RebalanceVo rebalanceVo) throws Exception;

  /**
   * 处理用户经验
   * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
   * @param rebalanceVo
   * @param webUser
   * @throws Exception
   * @return
   * @author wangchao
   * @create 2016/12/7 17:58
   */
  void dealAccountExp(RebalanceVo rebalanceVo, WebUser webUser) throws Exception ;

  /**
   * 处理用户经验和扣费、收入明细
   * @param rebalanceVo
   * @param hefanNum
   * @param fanPiaoNum
   * @param webUser
   * @throws Exception
   */
  void dealAccountDetail(RebalanceVo rebalanceVo, int hefanNum, int fanPiaoNum, WebUser webUser) throws Exception;

}
