package com.hefan.oms.itf;

import com.hefan.live.bean.LiveLog;

/**
 * Created by hbchen on 2016/9/29.
 */
public interface AnchorTicketMonthService {

    /**
     * 兑换时查看可兑换的盒饭
     * @param userId
     * @return
     */
    int selectAvailableTickets(String userId);

    /**
     * 每次直播间收入
     * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
     * @param list
     * @return
     * @throws
     * @author wangchao
     * @create 2016/12/29 15:40
     */
    void dealAnchorLiveDetail(LiveLog list);
    /**
    * 统计主播每天收入
    * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
    * @param list
    * @param month
     * @return
    * @throws
    * @author wangchao
    * @create 2016/12/29 15:40
    */
    void updateAnchorMonth(String userId, long HefanNum, boolean isTrue, int month) throws Exception;
}
