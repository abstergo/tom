package com.hefan.oms.itf;

import com.hefan.oms.bean.RebalanceVo;

import java.util.Date;

/**
 * Created by hbchen on 2016/10/28.
 */

public interface ExpendService {
    public void saveExpend(RebalanceVo rebalanceVo,int fanPiaoNum) throws Exception;

    /**
     * 获取时间段内用户消费饭票数据
     * @param userId
     * @param beginDate
     * @param endDate
     * @return
     */
    public long findUserExpendFianpiao(String userId, Date beginDate, Date endDate);
}
