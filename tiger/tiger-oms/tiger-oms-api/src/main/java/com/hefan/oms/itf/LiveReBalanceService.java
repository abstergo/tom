package com.hefan.oms.itf;

import com.hefan.oms.bean.RebalanceVo;
import com.hefan.user.bean.WebUser;

/**
 * Created by hbchen on 2016/10/30.
 */

public interface LiveReBalanceService {

  /**
   * 处理账户明细和
   *
   * @param rebalanceVo
   * @param webUser
   * @return
   * @throws
   * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
   * @author wangchao
   * @create 2016/11/2 17:45
   */
   void dealPresentDetail(RebalanceVo rebalanceVo, WebUser webUser) throws Exception;


}
