package com.hefan.oms.bean;

import java.io.Serializable;
import java.util.Date;

public class AnchorTicketDay implements Serializable{
	
    private int id;

    private String userId;

    private long realTicket;

    private long falseTicket;

    private long payTicket;

    private String yearsMonth;

    private Date createDate;

    private long liveRealTicket;

    private long liveFalseTicket;

    private long clubRealTicket;

    private long clubFalseTicket;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getRealTicket() {
        return realTicket;
    }

    public void setRealTicket(long realTicket) {
        this.realTicket = realTicket;
    }

    public long getFalseTicket() {
        return falseTicket;
    }

    public void setFalseTicket(long falseTicket) {
        this.falseTicket = falseTicket;
    }

    public long getPayTicket() {
        return payTicket;
    }

    public void setPayTicket(long payTicket) {
        this.payTicket = payTicket;
    }

    public String getYearsMonth() {
        return yearsMonth;
    }

    public void setYearsMonth(String yearsMonth) {
        this.yearsMonth = yearsMonth;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public long getLiveRealTicket() {
        return liveRealTicket;
    }

    public void setLiveRealTicket(long liveRealTicket) {
        this.liveRealTicket = liveRealTicket;
    }

    public long getLiveFalseTicket() {
        return liveFalseTicket;
    }

    public void setLiveFalseTicket(long liveFalseTicket) {
        this.liveFalseTicket = liveFalseTicket;
    }

    public long getClubRealTicket() {
        return clubRealTicket;
    }

    public void setClubRealTicket(long clubRealTicket) {
        this.clubRealTicket = clubRealTicket;
    }

    public long getClubFalseTicket() {
        return clubFalseTicket;
    }

    public void setClubFalseTicket(long clubFalseTicket) {
        this.clubFalseTicket = clubFalseTicket;
    }
}