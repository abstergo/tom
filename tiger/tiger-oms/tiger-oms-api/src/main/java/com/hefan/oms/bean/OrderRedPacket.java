package com.hefan.oms.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by hbchen on 2016/10/12.
 */
@Entity(tableName = "order_redpacket")
public class OrderRedPacket extends BaseEntity {

    private long id;
    @Column("user_id")
    private String userId;//发红包的人
    @Column("fanpiao_num")
    private int fanpiaoNum;//饭票数量
    @Column("packet_num")
    private int packetNum;//可抢的人数
    @Column("live_uuid")
    private String liveUuid;//直播id
    @Column("status")
    private int status;//订单状态 0 可抢  1 关闭 ：超时或者抢完了

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getFanpiaoNum() {
        return fanpiaoNum;
    }

    public void setFanpiaoNum(int fanpiaoNum) {
        this.fanpiaoNum = fanpiaoNum;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPacketNum() {
        return packetNum;
    }

    public void setPacketNum(int packetNum) {
        this.packetNum = packetNum;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }
}
