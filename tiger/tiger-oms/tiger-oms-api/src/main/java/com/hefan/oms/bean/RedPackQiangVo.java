package com.hefan.oms.bean;

/**
 * Created by hbchen on 2016/10/15.
 */
public class RedPackQiangVo {

    private long orderId;//红包订单id

    private long detailId;//红包明细id

    private int fanpiaoNum;//中奖饭票数

    private String userId;//中奖人

    private String nickName;//中奖人昵称

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public long getDetailId() {
        return detailId;
    }

    public void setDetailId(long detailId) {
        this.detailId = detailId;
    }

    public int getFanpiaoNum() {
        return fanpiaoNum;
    }

    public void setFanpiaoNum(int fanpiaoNum) {
        this.fanpiaoNum = fanpiaoNum;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
