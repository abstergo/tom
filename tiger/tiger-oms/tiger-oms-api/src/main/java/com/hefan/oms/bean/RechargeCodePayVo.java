package com.hefan.oms.bean;

public class RechargeCodePayVo {
	
	private String userId;
	
	private String rechargeCode;
	
	private int paySource;
	
	private long reqTimeStamped;
	
	private String sign;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRechargeCode() {
		return rechargeCode;
	}

	public void setRechargeCode(String rechargeCode) {
		this.rechargeCode = rechargeCode;
	}

	public int getPaySource() {
		return paySource;
	}

	public void setPaySource(int paySource) {
		this.paySource = paySource;
	}

	public long getReqTimeStamped() {
		return reqTimeStamped;
	}

	public void setReqTimeStamped(long reqTimeStamped) {
		this.reqTimeStamped = reqTimeStamped;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}
	
} 
