package com.hefan.oms.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by hbchen on 2016/10/8.
 */
public class ImCustomMsgVo implements Serializable {

    public ImCustomMsgVo() {
        super();
        this.level = "";
        this.nickName = "";
        this.content = "";
        this.type = "";
        this.gcount = "";
        this.watchNum = "";
        this.liveUuid = "";
    }

    public ImCustomMsgVo(String level, String name, String content, String type, String gcount,
                         String watchNum, String liveUuid) {
        super();
        this.level = level;
        this.nickName = name;
        this.content = content;
        this.type = type;
        this.gcount = gcount;
        this.watchNum = watchNum;
        this.liveUuid = liveUuid;
    }

    private static final long serialVersionUID = 1L;

    /**
     * 等级
     */
    private String level;

    /**
     * 用户名
     */
    private String nickName;

    /**
     * 消息内容
     */
    private String content;

    /**
     * 消息类型
     */
    private String type;

    /**
     * 礼物数
     */
    private String gcount;
    /**
     * 礼物类型
     */
    private String giftType;
    /**
     * 礼物url
     */
    private String giftUrl;
    /**
     * 礼物名称
     */
    private String giftName;

    /**
     * 礼物id
     */
    private String giftId;

    private List redList;

    private List<String> membersList;

    private String redId;//红包id：对于server来说就是订单id，每次发红包，红包id都是不同的

    private String userId;

    /**
     * 观看人数
     */
    private String watchNum;

    /**
     * 直播uuid(踢人判断是否从当前房间提出)
     */
    private String liveUuid;
    /**
     * 盒饭数
     */
    private String heFanNum;
    /**
     * 私信，弹幕消息体
     */
    private String textContent;

    /**
     * 是否被禁言 1被禁言 0未被禁言
     */
    private int isMute;

    /**
     * 点亮参数，0-9的随机数
     */
    private int indexOfClickScreen;

    /**
     * 播放时长
     */
    private String liveLength;
    
    private String deviceToken;


    public String getLiveLength() {
        return liveLength;
    }

    public void setLiveLength(String liveLength) {
        this.liveLength = liveLength;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public int getIndexOfClickScreen() {
        return indexOfClickScreen;
    }

    public void setIndexOfClickScreen(int indexOfClickScreen) {
        this.indexOfClickScreen = indexOfClickScreen;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }



    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGcount() {
        return gcount;
    }

    public void setGcount(String gcount) {
        this.gcount = gcount;
    }


    public String getWatchNum() {
        return watchNum;
    }

    public void setWatchNum(String watchNum) {
        this.watchNum = watchNum;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public String getGiftType() {
        return giftType;
    }

    public void setGiftType(String giftType) {
        this.giftType = giftType;
    }

    public String getGiftUrl() {
        return giftUrl;
    }

    public void setGiftUrl(String giftUrl) {
        this.giftUrl = giftUrl;
    }

    public String getGiftName() {
        return giftName;
    }

    public void setGiftName(String giftName) {
        this.giftName = giftName;
    }

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public List<String> getMembersList() {
        return membersList;
    }

    public void setMembersList(List<String> membersList) {
        this.membersList = membersList;
    }

    public List getRedList() {
        return redList;
    }

    public void setRedList(List redList) {
        this.redList = redList;
    }

    public String getHeFanNum() {
		return heFanNum;
	}

	public void setHeFanNum(String heFanNum) {
		this.heFanNum = heFanNum;
	}

	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRedId() {
        return redId;
    }

    public void setRedId(String redId) {
        this.redId = redId;
    }

    public int getIsMute() {
        return isMute;
    }

    public void setIsMute(int isMute) {
        this.isMute = isMute;
    }

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}
}
