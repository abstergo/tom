package com.hefan.oms.itf;

import com.hefan.oms.bean.RebalanceVo;

import java.util.List;
import java.util.Map;

/**
 * Created by hbchen on 2016/10/28.
 */
public interface InComeService {

  void saveWebIncome(RebalanceVo rebalanceVo, int hefanNum) throws Exception;

  /**
   * 统计每次直播间收入
   * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
   * @param liveUuid 直播id
   * @return list
   * @throws
   * @author wangchao
   * @create 2016/12/28 15:39
   */
  List<Map<String,Object>> getTotalByLiveUuid(String liveUuid);

  /**
   * 统计每天主播收入
   * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
   * @param start
   * @param end
   * @return
   * @throws
   * @author wangchao
   * @create 2016/12/28 16:58
   */
  List<Map<String,Object>> getUserIncomeByDay(String start, String end);
}
