package com.hefan.oms.itf;

import com.hefan.oms.bean.RebalanceVo;

/**
 * Created by hbchen on 2016/10/9.
 */
public interface CommentReBalanceService {

    boolean reBalance(RebalanceVo rebalanceVo) throws Exception;
}
