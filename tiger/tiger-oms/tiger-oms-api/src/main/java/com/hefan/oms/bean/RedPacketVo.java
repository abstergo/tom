package com.hefan.oms.bean;

import java.io.Serializable;

/**
 * Created by hbchen on 2016/10/12.
 */
public class RedPacketVo implements Serializable{

    private long redPacketId;//红包id

    private String userId;//发红包的人

    private String liveUuid;//直播间id

    private int fanpiaoNum;//红包饭票数

    private int personNum;//可拆分成多少份

    private String presentName;//礼物名称

    private long presentId;//礼物id

    private int presentType;//礼物类型

    private String roomId;//房间号

    private String presentUrl;//礼物url

    private String zhuboId;//主播id

    private int presentFanpiao;//礼物需扣费数

    private long beforeFanpiao;//扣费前余额

    private long exp;

    public long getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(long redPacketId) {
        this.redPacketId = redPacketId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public int getFanpiaoNum() {
        return fanpiaoNum;
    }

    public void setFanpiaoNum(int fanpiaoNum) {
        this.fanpiaoNum = fanpiaoNum;
    }

    public int getPersonNum() {
        return personNum;
    }

    public void setPersonNum(int personNum) {
        this.personNum = personNum;
    }

    public String getPresentName() {
        return presentName;
    }

    public void setPresentName(String presentName) {
        this.presentName = presentName;
    }

    public long getPresentId() {
        return presentId;
    }

    public void setPresentId(long presentId) {
        this.presentId = presentId;
    }

    public int getPresentType() {
        return presentType;
    }

    public void setPresentType(int presentType) {
        this.presentType = presentType;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public String getPresentUrl() {
        return presentUrl;
    }

    public void setPresentUrl(String presentUrl) {
        this.presentUrl = presentUrl;
    }

    public String getZhuboId() {
        return zhuboId;
    }

    public void setZhuboId(String zhuboId) {
        this.zhuboId = zhuboId;
    }

    public int getPresentFanpiao() {
        return presentFanpiao;
    }

    public void setPresentFanpiao(int presentFanpiao) {
        this.presentFanpiao = presentFanpiao;
    }

    public long getBeforeFanpiao() {
        return beforeFanpiao;
    }

    public void setBeforeFanpiao(long beforeFanpiao) {
        this.beforeFanpiao = beforeFanpiao;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }
}
