package com.hefan.oms.itf;

import java.util.List;
import java.util.Map;

/**
 * 兑换规则
 * @author sagagyq
 *
 */
public interface ExchangeRuleService {
	
	/**
	 * 根据主播id查询该主播的盒饭饭票兑换比例
	 * @return
	 */
	public Map<String,Object> ExchangeRuleForHeFanOrFanPiaoByUserId(String userId);

	/**
	 * 根据userId获取用户盒饭饭票兑换比例[主播和普通用户类型兑换比例计算方式不同]
	 * @param userId
	 * @param userType
     * @return
     */
	public Map<String,Object> ExchangeRuleForHeFanOrFanPiaoByUserId(String userId, int userType);

	/**
	 * 获取兑换中心的可兑换饭票
	 * eg:100/500/1000
	 * @return
     */
	public List<Integer> getCurrencyExchangeCenterPrice();
}
