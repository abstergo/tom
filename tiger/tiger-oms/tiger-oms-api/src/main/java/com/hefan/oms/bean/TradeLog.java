package com.hefan.oms.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

@Entity(tableName = "trade_log")
public class TradeLog extends BaseEntity {

    private long id;
    @Column("order_id")
    private String orderId;//交易流水号

    private Date addTime;//充值时间
    @Column("user_id")
    private String userId;//充值人(用户ID)
    @Column("nick_name")
    private String nickName;//充值人昵称（用户昵称）
    @Column("pay_amount")
    private BigDecimal payAmount;//充值金额(实际支付RMB金额)
    @Column("income")
    private int income;//充值饭票
    @Column("income_amount")
    private BigDecimal incomeAmount;//充值饭票(收入总饭票数)
    @Column("exchange_amount")
    private BigDecimal exchangeAmount;//实收
    @Column("reward_fanpiao")
    private int rewardFanpiao;//赠送的饭票
    @Column("reward_fanpiao_amount")
    private BigDecimal rewardFanpiaoAmount;//赠送的饭票金额
    @Column("account_type")
    private int accountType;//类型（1,支付宝2：微信 3:ApplePay 4: 充值码 5:任务 6:后台 7：红包 ）
    @Column("pay_source")
    private int paySource;//支付来源：1：PC端 2：IOS端3：安卓端
    @Column("account_status")
    private String accountStatus;//充值回调成功或失败，默认失败
    @Column("pay_before_fanpiao")
    private int payBeforeFanpiao;//支付前饭票
    @Column("pay_after_fanpiao")
    private int payAfterFanpiao;//支付后饭票
    @Column("channel_fee")
    private BigDecimal channelFee;//渠道手续费
    @Column("remark")
    private String remark;//备注
    @Column("pay_status")
    private int payStatus;//支付状态
    @Column("pay_notify_date")
    private Date payNotifyDate;

    private Date updateTime;//更新时间

    private Byte isHandle;//针对失败情况进行处理 0：未处理 1：已处理

    private String handleContent;//处理内容

    private int handleUserId;//处理人id

    private String handleUser;//处理人

    private Date handleTime;//处理时间
    @Column("recharge_code")
    private String rechargeCode;//充值码充值的充值码code


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public int getIncome() {
        return income;
    }

    public void setIncome(int income) {
        this.income = income;
    }

    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    public void setIncomeAmount(BigDecimal incomeAmount) {
        this.incomeAmount = incomeAmount;
    }

    public BigDecimal getExchangeAmount() {
        return exchangeAmount;
    }

    public void setExchangeAmount(BigDecimal exchangeAmount) {
        this.exchangeAmount = exchangeAmount;
    }

    public int getRewardFanpiao() {
        return rewardFanpiao;
    }

    public void setRewardFanpiao(int rewardFanpiao) {
        this.rewardFanpiao = rewardFanpiao;
    }

    public BigDecimal getRewardFanpiaoAmount() {
        return rewardFanpiaoAmount;
    }

    public void setRewardFanpiaoAmount(BigDecimal rewardFanpiaoAmount) {
        this.rewardFanpiaoAmount = rewardFanpiaoAmount;
    }

    public int getAccountType() {
        return accountType;
    }

    public void setAccountType(int accountType) {
        this.accountType = accountType;
    }

    public int getPaySource() {
        return paySource;
    }

    public void setPaySource(int paySource) {
        this.paySource = paySource;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public int getPayBeforeFanpiao() {
        return payBeforeFanpiao;
    }

    public void setPayBeforeFanpiao(int payBeforeFanpiao) {
        this.payBeforeFanpiao = payBeforeFanpiao;
    }

    public int getPayAfterFanpiao() {
        return payAfterFanpiao;
    }

    public void setPayAfterFanpiao(int payAfterFanpiao) {
        this.payAfterFanpiao = payAfterFanpiao;
    }

    public BigDecimal getChannelFee() {
        return channelFee;
    }

    public void setChannelFee(BigDecimal channelFee) {
        this.channelFee = channelFee;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Byte getIsHandle() {
        return isHandle;
    }

    public void setIsHandle(Byte isHandle) {
        this.isHandle = isHandle;
    }

    public String getHandleContent() {
        return handleContent;
    }

    public void setHandleContent(String handleContent) {
        this.handleContent = handleContent;
    }

    public int getHandleUserId() {
        return handleUserId;
    }

    public void setHandleUserId(int handleUserId) {
        this.handleUserId = handleUserId;
    }

    public String getHandleUser() {
        return handleUser;
    }

    public void setHandleUser(String handleUser) {
        this.handleUser = handleUser;
    }

    public Date getHandleTime() {
        return handleTime;
    }

    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    public String getRechargeCode() {
        return rechargeCode;
    }

    public void setRechargeCode(String rechargeCode) {
        this.rechargeCode = rechargeCode;
    }

    public int getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(int payStatus) {
        this.payStatus = payStatus;
    }

    public Date getPayNotifyDate() {
        return payNotifyDate;
    }

    public void setPayNotifyDate(Date payNotifyDate) {
        this.payNotifyDate = payNotifyDate;
    }

    public TradeLog(String orderId, BigDecimal payAmount, Integer income, BigDecimal incomeAmount, BigDecimal exchangeAmount,
                    Integer rewardFanpiao, BigDecimal rewardFanpiaoAmount, Integer accountType, Integer paySource, String accountStatus,
                    String userId, String nickName, BigDecimal channelFee, String rechargeCode, int payBeforeFanpiao, int payAfterFanpiao) {
		this.orderId = orderId;
		this.payAmount = payAmount;
		this.income = income;
		this.incomeAmount = incomeAmount;
		this.exchangeAmount = exchangeAmount;
		this.rewardFanpiao = rewardFanpiao;
		this.rewardFanpiaoAmount = rewardFanpiaoAmount;
		this.accountType = accountType;
		this.paySource = paySource;
		this.accountStatus = accountStatus;
		this.userId = userId;
		this.nickName = nickName;
		this.channelFee = channelFee;
		this.rechargeCode = rechargeCode;
		this.payBeforeFanpiao = payBeforeFanpiao;
		this.payAfterFanpiao = payAfterFanpiao;
	}

    public TradeLog(){

    }

}