package com.hefan.oms.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

import java.io.Serializable;
import java.util.Date;
@Entity(tableName = "anchor_ticket_month")
public class AnchorTicketMonth extends BaseEntity implements Serializable{
    @Column("user_id")
    private String userId;
    @Column("real_ticket")
    private long realTicket;
    @Column("false_ticket")
    private long falseTicket;
    @Column("pay_ticket")
    private long payTicket;
    @Column("years_month")
    private int yearsMonth;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getRealTicket() {
        return realTicket;
    }

    public void setRealTicket(long realTicket) {
        this.realTicket = realTicket;
    }

    public long getFalseTicket() {
        return falseTicket;
    }

    public void setFalseTicket(long falseTicket) {
        this.falseTicket = falseTicket;
    }

    public long getPayTicket() {
        return payTicket;
    }

    public void setPayTicket(long payTicket) {
        this.payTicket = payTicket;
    }

    public int getYearsMonth() {
        return yearsMonth;
    }

    public void setYearsMonth(int yearsMonth) {
        this.yearsMonth = yearsMonth;
    }
}