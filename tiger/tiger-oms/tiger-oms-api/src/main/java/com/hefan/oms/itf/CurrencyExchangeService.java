package com.hefan.oms.itf;

import com.cat.common.entity.ResultBean;
import com.hefan.oms.bean.ExchangeDetail;
import com.hefan.user.bean.WebUser;
import com.hefan.user.bean.WebUserIdentity;

import java.util.Map;

/**
 * Created by hbchen on 2016/10/10.
 */
public interface CurrencyExchangeService {

    public void doMealTicketExchange(ExchangeDetail exchangeDetail);

    public ResultBean doMealTicketExchange(ExchangeDetail exchangeDetail, WebUser webUser, WebUserIdentity webUserIdentity);

    public Map selectByRuleType(Integer exchangeRuleType);

    /**
     * 根据兑换规则，实现饭票兑换盒饭
     * @param ticket
     * @param exchangeRuleType
     * @return
     * @throws Exception
     * @author wangchao
     * @create 2016/11/4 9:32
     */
    int exchangeFanPiao2HeFan(long ticket,Integer exchangeRuleType) throws  Exception;

    /**
     * 根据兑换规则，实现饭票兑换RMB
     * @param ticket
     * @return
     * @throws Exception
     * @author wangchao
     * @create 2016/11/4 9:32
     */
    double exchangeFanPiao2Object(long ticket) throws  Exception;
}
