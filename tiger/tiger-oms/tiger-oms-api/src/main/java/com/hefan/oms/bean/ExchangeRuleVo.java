package com.hefan.oms.bean;

/**
 *
 * 返回兑换中心的初始化数据vo
 *
 * Created by sagagyq on 2016/12/29.
 */
public class ExchangeRuleVo {

    //左侧显示饭票数
    private int fanpiaoNum;

    //右侧显示可兑换盒饭数
    private int hefanNum;

    //是否可以兑换  0 不能 1 能
    private int flag = 0;

    public int getFanpiaoNum() {
        return fanpiaoNum;
    }

    public void setFanpiaoNum(int fanpiaoNum) {
        this.fanpiaoNum = fanpiaoNum;
    }

    public int getHefanNum() {
        return hefanNum;
    }

    public void setHefanNum(int hefanNum) {
        this.hefanNum = hefanNum;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}
