package com.hefan.oms.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by hbchen on 2016/10/19.
 */
@Entity(tableName = "private_letter")
public class PrivateLetter extends BaseEntity {
    @Column("sender_uid")
    private String fromId;
    @Column("addressee_uid")
    private String toId;
    @Column("sender_nickname")
    private String fromNickName;
    @Column("content")
    private String content;
    @Column("address_nickname")
    private  String addressNickName;
    @Column("chat_room_flag")
    private String chatRoomFlag;

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public String getFromNickName() {
        return fromNickName;
    }

    public void setFromNickName(String fromNickName) {
        this.fromNickName = fromNickName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAddresseeNickName() {
        return addressNickName;
    }

    public void setAddressNickName(String addressNickName) {
        this.addressNickName = addressNickName;
    }

    public String getChatRoomFlag() {
        return chatRoomFlag;
    }

    public void setChatRoomFlag(String chatRoomFlag) {
        this.chatRoomFlag = chatRoomFlag;
    }

    @Override
    public String toString() {
        return "PrivateLetter{" + "fromId='" + fromId + '\'' + ", toId='" + toId + '\'' + ", fromNickName='" + fromNickName + '\'' + ", content='" + content + '\'' + ", addressNickName='"
            + addressNickName + '\'' + ", chatRoomFlag='" + chatRoomFlag + '\'' + '}';
    }
}
