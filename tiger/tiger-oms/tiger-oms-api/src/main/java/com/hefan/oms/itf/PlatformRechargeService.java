package com.hefan.oms.itf;

/**
 * Created by hbchen on 2016/9/27.
 */
public interface PlatformRechargeService {

    /**
     * 根据充值规则类型和充值钱数，计算得到的饭票数
     *
     * @param rechargeType：(0:盒饭兑换饭票
     *            1:人民币兑换饭票 2:饭票兑换盒饭 3:公共账户兑换比例 4：PC充值比例 5:安卓充值比例 6，iOS充值比例)
     * @param rechargeMoney
     * @return
     */
    public Integer getMealTicketsByRechargeRuleInfo(Integer rechargeType, Integer rechargeMoney);
}
