package com.hefan.oms.itf;

import com.hefan.oms.bean.Present;

import java.util.List;

/**
 * User: criss Date: 16/9/20 Time: 08:32
 */
public interface PresentService {

	
	List dynamicPresent();
	List livePresent();
	List presentVersion();
	
	int getPresentPriceById(Long presentId);

	/**
	 * 取礼物信息
	 * @param presentId
	 * @return
     */
	Present getPresentById(Long presentId);

	/**
	 * 根据礼物ID获取礼物信息
	 * @param presentId
	 * @return
	 */
	Present getPresentInfoById(Long presentId);
}
