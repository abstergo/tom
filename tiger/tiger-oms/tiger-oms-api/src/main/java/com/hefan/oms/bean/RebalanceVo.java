package com.hefan.oms.bean;

import java.io.Serializable;

/**
 * Created by hbchen on 2016/9/30.
 */
public class RebalanceVo implements Serializable {

    private String fromId;//送礼人

    private String nickName;//送礼人昵称

    private String toId;//收礼人

    private int source;//来源

    private long  presentId;//礼物id

    private double price;//礼物价格

    private String presentName;//礼物名称

    private int presentNum;//礼物数量

    private String roomId;//房间id,直播间送礼物时用到

    private long exp;//经验

    private int messageId;//动态id，如果是俱乐部礼物，要记录是哪个动态

    private String liveUuid;//直播的uuid

    private int isFalse;//0 假 1 真  默认1

    private int presentType; //礼物类型

    private int houtaiPresentType;//礼物类型

    private int isContinue;

    private String presentUrl;//礼物url

    private String content; //私信或弹幕内容

    private long beforeFanpiao;//扣款前余额

    private String uniqueKey;

    private int userType;//用户类型

    public String getFromId() {
        return fromId;
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getToId() {
        return toId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }

    public int getSource() {
        return source;
    }

    public void setSource(int source) {
        this.source = source;
    }

    public long getPresentId() {
        return presentId;
    }

    public void setPresentId(long presentId) {
        this.presentId = presentId;
    }

    public String getPresentName() {
        return presentName;
    }

    public void setPresentName(String presentName) {
        this.presentName = presentName;
    }

    public int getPresentNum() {
        return presentNum;
    }

    public void setPresentNum(int presentNum) {
        this.presentNum = presentNum;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public int getIsFalse() {
        return isFalse;
    }

    public void setIsFalse(int isFalse) {
        this.isFalse = isFalse;
    }

    public int getPresentType() {
        return presentType;
    }

    public void setPresentType(int presentType) {
        this.presentType = presentType;
    }

    public String getPresentUrl() {
        return presentUrl;
    }

    public void setPresentUrl(String presentUrl) {
        this.presentUrl = presentUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getHoutaiPresentType() {
        return houtaiPresentType;
    }

    public void setHoutaiPresentType(int houtaiPresentType) {
        this.houtaiPresentType = houtaiPresentType;
    }


    public int getIsContinue() {
        return isContinue;
    }

    public void setIsContinue(int isContinue) {
        this.isContinue = isContinue;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public long getBeforeFanpiao() {
        return beforeFanpiao;
    }

    public void setBeforeFanpiao(long beforeFanpiao) {
        this.beforeFanpiao = beforeFanpiao;
    }

    public String getUniqueKey() {
        return uniqueKey;
    }

    public void setUniqueKey(String uniqueKey) {
        this.uniqueKey = uniqueKey;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }
}
