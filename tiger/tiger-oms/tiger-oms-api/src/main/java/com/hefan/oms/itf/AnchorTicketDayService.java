package com.hefan.oms.itf;

import com.hefan.oms.bean.AnchorTicketDay;

import java.util.Date;

/**
 * Created by hbchen on 2016/9/29.
 */
public interface AnchorTicketDayService {

    /**
     * 直播开始时初始化主播盒饭天表
     * @param userId
     * @param sateDate
     */
    public void initAnchorTicketDayWhenLiveStart(String userId, Date sateDate);

    /**
     * 初始化一条主播盒饭天表记录【存在不添加】
     * @param ticketBean
     * @return
     */
    public int addNewAnchorTicketDay(AnchorTicketDay ticketBean);

    /**
     * 兑换时查看可兑换的饭票
     * @param userId
     * @return
     */
    public int selectAvailableTickets(String userId);
}
