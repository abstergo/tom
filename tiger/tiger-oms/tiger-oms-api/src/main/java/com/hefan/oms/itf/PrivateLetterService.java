package com.hefan.oms.itf;

import com.hefan.oms.bean.PrivateLetter;

/**
 * Created by hbchen on 2016/10/19.
 */
public interface PrivateLetterService {

    public long addNewPrivateLetter(PrivateLetter privateLetter);
}
