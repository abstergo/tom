package com.hefan.oms.bean;

import java.io.Serializable;

/**
 * Created by hbchen on 2016/10/12.
 */
public class RedPacket  implements Serializable{

    private long id;
    private String name;//红包名称
    private int amount;//红包金额
    private int number;//红包数量

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
