package com.hefan.oms.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

import java.util.Date;

/**
 * Created by hbchen on 2016/10/10.
 */
@Entity(tableName = "exchange_detail")
public class ExchangeDetail extends BaseEntity {

    private long id;
    @Column("transaction_number")
    private String transactionNumber;//流水号
    @Column("user_id")
    private String userId;//主播ID
    @Column("exchange_time")
    private Date exchangeTime;//兑换时间
    @Column("fanpiao_num")
    private int fanpiaoNum;//兑换饭票数量
    @Column("hefan_num")
    private int hefanNum;
    @Column("before_hefan_num")
    private int beforeHefanNum;
    @Column("superior_hefan_num")
    private int superiorHefanNum;
    @Column("platform_hefan_num")
    private int platformHefanNum;

    private Date createTime;

    @Column("pid")
    private int pid;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTransactionNumber() {
        return transactionNumber;
    }

    public void setTransactionNumber(String transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getExchangeTime() {
        return exchangeTime;
    }

    public void setExchangeTime(Date exchangeTime) {
        this.exchangeTime = exchangeTime;
    }

    public int getFanpiaoNum() {
        return fanpiaoNum;
    }

    public void setFanpiaoNum(int fanpiaoNum) {
        this.fanpiaoNum = fanpiaoNum;
    }

    public int getHefanNum() {
        return hefanNum;
    }

    public void setHefanNum(int hefanNum) {
        this.hefanNum = hefanNum;
    }

    public int getBeforeHefanNum() {
        return beforeHefanNum;
    }

    public void setBeforeHefanNum(int beforeHefanNum) {
        this.beforeHefanNum = beforeHefanNum;
    }

    public int getSuperiorHefanNum() {
        return superiorHefanNum;
    }

    public void setSuperiorHefanNum(int superiorHefanNum) {
        this.superiorHefanNum = superiorHefanNum;
    }

    public int getPlatformHefanNum() {
        return platformHefanNum;
    }

    public void setPlatformHefanNum(int platformHefanNum) {
        this.platformHefanNum = platformHefanNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }
}