package com.hefan.oms.itf;

import com.hefan.oms.bean.OrderRedPacket;
import com.hefan.oms.bean.RedPackQiangVo;
import com.hefan.oms.bean.RedPacket;
import com.hefan.oms.bean.RedPacketVo;
import com.hefan.user.bean.WebUser;

/**
 * Created by hbchen on 2016/10/12.
 */
public interface RedPacketService {

    public RedPacket selectRedPackInfo(int id);

    /**
     * 红包超时或者被领光了，更新主表红包领取状态。
     * @param orderId
     */
    public void updateRedPackOrderStatus(long orderId) throws Exception;

    /**
     * 红包被抢到了，更新一下领取信息
     * @param detailId
     * @param userId
     */
    public void updateRedPackDetail(long detailId,String userId) throws Exception;

    /**
     * 判断领取明细count来判断，整个大红包是否领光了
     * @param orderId
     * @return
     */
    public int selectRedPackDetailCount(long orderId);

    /**
     * 直播结束，更新红包状态
     * @param liveUuid
     */
    public void updateRedPackOrderStatus(String liveUuid) throws Exception;

    /**
     * 发红包
     * @param redPacketVo
     * @return
     */
    public long pushRedPacket(RedPacketVo redPacketVo, WebUser webUser) throws Exception;

    /**
     * 抢红包
     * @param redPackQiangVo
     */
    public void grapRedPacket(RedPackQiangVo redPackQiangVo) throws Exception;

    /**
     * 通过id获取红包订单表
     * @param id
     * @return
     * @throws
     * @author wangchao
     * @create 2016/11/3 15:42
     */
    OrderRedPacket getOrderPacketbyId(long id);

    /**
     * 校验用户是否已抢本次红包
     * @param redOrderId OrderRedpacket id
     * @param userId 用户id
     * @return
     * @throws
     * @author wangchao
     * @create 2016/11/3 16:49
     */
    boolean isGrabRedPack(long redOrderId,String userId);

}
