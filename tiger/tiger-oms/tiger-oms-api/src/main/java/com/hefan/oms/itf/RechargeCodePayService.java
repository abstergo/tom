package com.hefan.oms.itf;

import com.cat.common.entity.ResultBean;

import java.util.Date;
import java.util.Map;

public interface RechargeCodePayService {

	/**
	 * 根据充值码获取有效的的充值信息
	 * @param code
	 * @return
     */
	public Map findValidRechargeInfoByCode(String code);

	/**
	 * 充值码充值进行验证|并获取充值码信息
	 * @param code
	 * @param userId
     * @return
     */
	public ResultBean findValidRechargeInfoByCodeForPay(String code, String userId);

	/**
	 * 充值码充值
	 * @param webUserMap
	 * @param rechangeCodeMap
	 * @param paySource
	 * @return
     * @throws Exception
     */
	public Integer rechargeCodePayOpreate(Map webUserMap, Map rechangeCodeMap, Integer paySource) throws Exception;
}
