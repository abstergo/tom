package com.hefan.oms.bean;

import java.io.Serializable;
import java.math.BigDecimal;

public class Present implements Serializable {

    private long id;

    private String presentName;

    private String icon;

    private String showpic;

    private int price;

    private int experience;

    private int rank;

    private int sort;

    private int iscontinue;

    private int iseffect;

    private int isexclusive;

    private String exclusiveids;

    private int typeid;

    private int isputaway;

    private int status;

    private int zf;

    private int type;

    private int redPacketId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPresentName() {
        return presentName == null ? "" : presentName.trim();
    }

    public void setPresentName(String presentName) {
        this.presentName = presentName == null ? "" : presentName.trim();
    }

    public String getIcon() {
        return icon == null ? "" : icon.trim();
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? "" : icon.trim();
    }

    public String getShowpic() {
        return showpic;
    }

    public void setShowpic(String showpic) {
        this.showpic = showpic == null ? "" : showpic.trim();
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Integer getExperience() {
        return experience;
    }

    public void setExperience(Integer experience) {
        this.experience = experience;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getIscontinue() {
        return iscontinue;
    }

    public void setIscontinue(Integer iscontinue) {
        this.iscontinue = iscontinue;
    }

    public Integer getIseffect() {
        return iseffect;
    }

    public void setIseffect(Integer iseffect) {
        this.iseffect = iseffect;
    }

    public Integer getIsexclusive() {
        return isexclusive;
    }

    public void setIsexclusive(Integer isexclusive) {
        this.isexclusive = isexclusive;
    }

    public String getExclusiveids() {
        return exclusiveids;
    }

    public void setExclusiveids(String exclusiveids) {
        this.exclusiveids = exclusiveids == null ? null : exclusiveids.trim();
    }

    public Integer getTypeid() {
        return typeid;
    }

    public void setTypeid(Integer typeid) {
        this.typeid = typeid;
    }

    public Integer getIsputaway() {
        return isputaway;
    }

    public void setIsputaway(Integer isputaway) {
        this.isputaway = isputaway;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getZf() {
        return zf;
    }

    public void setZf(Integer zf) {
        this.zf = zf;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getRedPacketId() {
        return redPacketId;
    }

    public void setRedPacketId(int redPacketId) {
        this.redPacketId = redPacketId;
    }
}