package com.hefan.oms.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by hbchen on 2016/10/12.
 */
@Entity(tableName = "order_redpacket_detail")
public class OrderRedPacketDetail extends BaseEntity {
    @Column("order_id")
    private long orderId;//红包订单主表的id
    @Column("fanpiao_num")
    private int fanpiaoNum;//抢到的饭票数
    @Column("user_id")
    private String userId;//抢红包的用户id
    @Column("live_uuid")
    private String liveUuid;//直播间id
    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public int getFanpiaoNum() {
        return fanpiaoNum;
    }

    public void setFanpiaoNum(int fanpiaoNum) {
        this.fanpiaoNum = fanpiaoNum;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }
}
