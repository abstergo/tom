package com.hefan.robot.configCenter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by nigle on 2016/12/7.
 */
@Component
public class RobotConfigCenter {

    @Value("#{publicConfig}")
    private Map<String, String> publicConfig;

    public Map<String, String> getPublicConfig() {
        return publicConfig;
    }

    public void setPublicConfig(Map<String, String> publicConfig) {
        this.publicConfig = publicConfig;
    }
}
