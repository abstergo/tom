package com.hefan.robot.service;

import com.hefan.robot.dao.RobotDao;
import com.hefan.robot.itf.RobotService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * Created by nigle on 2016/11/15.
 */
@Component("robotService")
public class RobotServiceImpl implements RobotService {

    @Resource
    RobotDao robotDao;

    /**
     * 获取全部机器人
     * new redis 方案
     */
    @Override
    public List<Map> getAllRobotList(String startTime) {
        return robotDao.getAllRobotInfo(startTime);
    }

}