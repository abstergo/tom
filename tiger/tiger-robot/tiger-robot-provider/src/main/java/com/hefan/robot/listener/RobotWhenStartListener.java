package com.hefan.robot.listener;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.ConsumeContext;
import com.aliyun.openservices.ons.api.Message;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.TopicRegistryDev;
import com.hefan.common.ons.TopicRegistryTest;
import com.hefan.common.ons.listener.GLLMessageListener;
import com.hefan.common.ons.service.ONSProducer;
import com.hefan.common.util.MapUtils;
import com.hefan.live.bean.RobotListenerVo;
import com.hefan.live.itf.LivingRedisOptService;
import com.hefan.live.itf.RoomEnterExitOptService;
import com.hefan.robot.common.util.RandomUtil;
import com.hefan.robot.configCenter.RobotConfigCenter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 开播时进入机器人
 * Created by nigle on 2016/12/7.
 */
@Component
public class RobotWhenStartListener implements GLLMessageListener {

    Logger logger = LoggerFactory.getLogger(RobotWhenStartListener.class);

    @Resource
    LivingRedisOptService livingRedisOptService;
    @Resource
    RoomEnterExitOptService roomEnterExitOptService;
    @Resource
    private RobotConfigCenter robotConfigCenter;
    @Resource
    ONSProducer onsProducer;

    @Override
    public TopicRegistry getTopicRegistry() {
        return TopicRegistry.HEFAN_ROBOT_WHEN_START ;
    }

    @Override
    public TopicRegistryDev getTopicRegistryDev() {
        return TopicRegistryDev.HEFAN_ROBOT_WHEN_START_DEV;
    }

    @Override
    public TopicRegistryTest getTopicRegistryTest() {
        return TopicRegistryTest.HEFAN_ROBOT_WHEN_START_TEST;
    }

    @Override
    public Action consume(Message message, ConsumeContext consumeContext) {
        String realTopic = message.getTag();
        logger.info("开播填充机器人--开始： receiver MessageListener topic: {}, realTopic : {}, msg_body: {}",
                message.getTopic(), realTopic, new String(message.getBody()));
        try {
            Map map = new ObjectMapper().readValue(message.getBody(), Map.class);
            if (null != map && !map.isEmpty()) {
                String voStr = (String) map.get("vo");
                RobotListenerVo vo = JSONObject.parseObject(voStr, RobotListenerVo.class);
                if (null == vo) {
                    logger.info("RobotListenerVo解析失败");
                    return Action.CommitMessage;
                }
                return addRobotWhenStart(vo.getLiveUuid(),vo.getChatRoomId(),vo.getAnchId());
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("HEFAN_ROBOT_WHEN_START_DELAY receiver MessageListener ERROR：" + e.getMessage());
            return Action.CommitMessage;
        } finally {
            logger.info("HEFAN_ROBOT_WHEN_START_DELAY receiver MessageListener END");
            return Action.CommitMessage;
        }
    }

    /**
     * 开播时机器人进入直播间
     * 发送MQ任务 HEFAN_ROBOT_ENTER_EXIT_DELAY
     */
    public Action addRobotWhenStart(String liveUuid, int chatRoomId, String anchId) {
        try {
            if (!livingRedisOptService.isExistsLivingInfo_Hash(anchId)){
                logger.info("直播已经结束，结束开播加机器人");
                return Action.CommitMessage;
            }
            Map<String,String> propty = robotConfigCenter.getPublicConfig();
            int robotCountMin = MapUtils.getIntValue(propty,"robot.start.count.min",0);
            int robotCountMax = MapUtils.getIntValue(propty,"robot.start.count.max",0);
            int robotCount = RandomUtil.getRandom(robotCountMin, robotCountMax + 1);
            long delayTimeMin = MapUtils.getIntValue(propty,"robot.start.delay.time.min",0);
            long delayTimeMax = MapUtils.getIntValue(propty,"robot.start.delay.time.max",0);
            long delayTime = RandomUtil.getRandom(delayTimeMin, delayTimeMax + 1);
            String onsEnv = MapUtils.getStrValue(propty,"ons.env","");
            int robotDelayMin = MapUtils.getIntValue(propty, "robot.delay.min", 30000);
            int robotDelayMax = MapUtils.getIntValue(propty, "robot.delay.max", 120000);
            logger.info("直播间{}初始化加入机器人{}个开始",anchId,robotCount);
            RobotListenerVo vo = new RobotListenerVo();
            vo.setAnchId(anchId);
            vo.setLiveUuid(liveUuid);
            vo.setChatRoomId(chatRoomId);
            vo.setInOrOut(1);
            int flag = RandomUtil.getRandom(1000, 5000);
            for (int i = 0; i < robotCount; i++) {
                com.hefan.common.ons.bean.Message message = new com.hefan.common.ons.bean.Message();
                vo.setInfo("机器人消费过滤主播ID：" + anchId + "正常机器人任务ID：" + flag + "--执行总次数：" + robotCount + "，本次第" + i + "个");
                message.put("vo", JSON.toJSONString(vo));
                message.setTopic(TopicRegistry.HEFAN_ROBOT_ENTER_EXIT_DELAY);
                message.setTag(onsEnv);
                onsProducer.sendDelayMsg(message, delayTime);
                logger.info("发送MQ：{}直播间初始化加入机器人，延时{}毫秒",anchId,delayTime);
                delayTime += RandomUtil.getRandom(robotDelayMin, robotDelayMax);
            }
            logger.info("直播间{}初始化加入机器人{}个结束",anchId,robotCount);
        } catch (Exception e) {
            logger.info("进入直播间初始化机器人失败");
            e.printStackTrace();
        }
        return Action.CommitMessage;
    }
}
