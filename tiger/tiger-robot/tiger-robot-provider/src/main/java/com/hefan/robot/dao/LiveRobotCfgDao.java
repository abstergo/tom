package com.hefan.robot.dao;

import com.cat.tiger.util.CollectionUtils;
import com.hefan.robot.bean.LiveRobotCfg;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nigle on 2016/12/5.
 * 后台机器人任务表操作
 */
@Repository
public class LiveRobotCfgDao {
    @Resource
    JdbcTemplate jdbcTemplate;

    /**
     * 取机器人任务列表
     * 0代表未完成任务
     * @return
     */
    public List<LiveRobotCfg> getRobotCfgList(){
        String sql = "select l.id,l.user_id,l.chat_room_id,l.live_uuid,l.robot_num,l.create_time,l.create_user,l.status,l.execute_count,l.type" +
                " from live_robot_cfg l where status = 0";
        List<LiveRobotCfg> list = jdbcTemplate.query(sql,new BeanPropertyRowMapper<LiveRobotCfg>(LiveRobotCfg.class));
        return CollectionUtils.isNotEmpty(list) ? list : new ArrayList<>();
    }

    /**
     * 更新机器人任务列表
     * @return
     */
    public int updteRobotCfg(long id,int status,int executeCount,String info) {
        String sql = "update live_robot_cfg lrc set lrc.execute_count = ? ,lrc.status = ? ,lrc.info = ? where lrc.id = ? ";
        return jdbcTemplate.update(sql,executeCount,status,info,id);
    }
}
