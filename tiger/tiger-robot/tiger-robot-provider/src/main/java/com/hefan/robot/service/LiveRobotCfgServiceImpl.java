package com.hefan.robot.service;

import com.hefan.robot.bean.LiveRobotCfg;
import com.hefan.robot.dao.LiveRobotCfgDao;
import com.hefan.robot.itf.LiveRobotCfgService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by nigle on 2016/12/5.
 */
@Component("liveRobotCfgService")
public class LiveRobotCfgServiceImpl implements LiveRobotCfgService {

    @Resource
    LiveRobotCfgDao liveRobotCfgDao;

    /**
     * 取机器人任务列表
     */
    @Override
    @Transactional(readOnly=true)
    public List<LiveRobotCfg> getRobotCfgList(){
        return liveRobotCfgDao.getRobotCfgList();
    }

    /**
     * 更新机器人任务列表
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int updteRobotCfg(long id,int status,int executeCount,String info){
        return liveRobotCfgDao.updteRobotCfg(id, status, executeCount, info);
    }
}
