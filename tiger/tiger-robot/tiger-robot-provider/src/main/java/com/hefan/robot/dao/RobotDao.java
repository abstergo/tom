package com.hefan.robot.dao;

import com.cat.tiger.util.CollectionUtils;
import com.cat.tiger.util.GlobalConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by nigle on 2016/11/15.
 */
@Repository
public class RobotDao {
    @Resource
    JdbcTemplate jdbcTemplate;

    public Logger logger = LoggerFactory.getLogger(RobotDao.class);


    /**
     * 获取全部机器人
     * 包含信息更健全
     * 2017-02-27
     */
    public List<Map> getAllRobotInfo(String startTime) {
        String sql = "SELECT wu.user_id userId,wu.head_img headImg,wu.user_level userLevel,wu.user_type userType,wu.nick_name nickName,wu.sex sex FROM web_user wu WHERE wu.user_type = ? and wu.create_time > ?";
        List list = jdbcTemplate.queryForList(sql, GlobalConstants.USER_TYPE_ROBOT,startTime);
        return CollectionUtils.isNotEmpty(list) ? list : new ArrayList<>();
    }

}