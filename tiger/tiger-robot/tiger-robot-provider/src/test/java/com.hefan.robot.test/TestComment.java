package com.hefan.robot.test;

import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;

/**
 * Created by wangchao on 2016/11/28.
 */
@ContextConfiguration(locations = "classpath*:spring/applicationContext.xml")
public class TestComment {

  @Resource
  public void testComment(){
  }
}
