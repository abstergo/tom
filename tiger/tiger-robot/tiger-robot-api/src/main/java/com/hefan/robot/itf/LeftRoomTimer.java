package com.hefan.robot.itf;

/**
 * 定时监测直播间新进观众情况，10分钟内无新观众进入，则减少机器人
 * Created by nigle on 2016/12/9.
 */
public interface LeftRoomTimer {

    void leftTimer();
}
