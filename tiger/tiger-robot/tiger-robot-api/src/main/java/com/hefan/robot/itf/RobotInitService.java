package com.hefan.robot.itf;

import com.cat.common.entity.ResultBean;

/**
 * Created by nigle on 2016/12/7.
 */
public interface RobotInitService {

     /**
      * 机器人入redis中List栈初始化任务
      */
     ResultBean initRobotToRedisList(String startTime);

     /**
      * redis中清除机器人信息缓存
      */
     ResultBean removeRobot(String userId);

     /**
      * 清理所有机器人信息（redis）
      * 除去直播间内
      */
     ResultBean removeRobot();
}
