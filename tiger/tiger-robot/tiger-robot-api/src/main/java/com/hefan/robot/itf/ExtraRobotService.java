package com.hefan.robot.itf;

/**
 * Created by nigle on 2016/12/7.
 */
public interface ExtraRobotService {
    void scanningTask();
    String test(String liveUuid,int chatRoomId);
    String index();
}
