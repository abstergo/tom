package com.hefan.robot.itf;

import com.hefan.robot.bean.LiveRobotCfg;

import java.util.List;

/**
 * Created by nigle on 2016/12/5.
 */
public interface LiveRobotCfgService {

    /**
     * 取机器人任务列表
     */
    List<LiveRobotCfg> getRobotCfgList();

    /**
     * 更新机器人任务列表
     * @param executeCount 执行次数
     * @param info 备注信息
     * @param status 任务执行状态
     */
    int updteRobotCfg(long id,int status,int executeCount,String info);
}
