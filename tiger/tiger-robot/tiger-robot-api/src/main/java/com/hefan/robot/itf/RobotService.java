package com.hefan.robot.itf;

import com.hefan.live.bean.LiveRobotVo;
import com.hefan.live.bean.LiveRoomPersonVo;

import java.util.List;
import java.util.Map;

/**
 * Created by nigle on 2016/11/15.
 */
public interface RobotService {
    /**
     * 获取全部机器人
     * new redis 方案
     */
    List<Map> getAllRobotList(String startTime);

}