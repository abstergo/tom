package com.hefan.activity.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;

import com.cat.tiger.util.CollectionUtils;
import com.hefan.activity.bean.AppTaskInfo;
import com.hefan.common.orm.dao.BaseDaoImpl;

@Repository
public class AppTaskInfoDao extends BaseDaoImpl<AppTaskInfo> {
	
	public List<AppTaskInfo> getAppTaskInfoList() {
		// TODO Auto-generated method stub
		String sql = "select * from app_task_info where delete_flag=0";
		return jdbcTemplate.query(sql,
				new BeanPropertyRowMapper<AppTaskInfo>(AppTaskInfo.class));
	}
	
	public Map<String,Object> getGiftUserNum(String userId, String toUserId, Date beginTime, Date endTime,int source){
		String sql = "select count(1) u_count,IFNULL(SUM(has),0) u_has from("
				+ " select to_user_id,IF(to_user_id=?,1,0) has from web_user_order where user_id=?"
				+ " and source=? and create_time BETWEEN ? and ?"
				+ " GROUP BY to_user_id) t";
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql,toUserId,userId,source,beginTime,endTime);
		return CollectionUtils.isEmptyList(list)?null:list.get(0);
	}
}
