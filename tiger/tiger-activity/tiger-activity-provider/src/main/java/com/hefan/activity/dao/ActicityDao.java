package com.hefan.activity.dao;

import com.cat.tiger.util.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 活动
 * 
 * @author kevin_zhang
 *
 */
@Repository
public class ActicityDao {

	@Resource
	JdbcTemplate jdbcTemplate;

	/**
	 * 获取推荐活动
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List queryHotActivity() {
		String sql = "SELECT img_path img, title title, id activityId,link url, DATE_FORMAT(start_date,'%Y-%m-%d %H:%i:%s') date "
				+ " FROM activity WHERE is_del=0 ORDER BY sort ASC LIMIT 5";
		List resutlList = jdbcTemplate.queryForList(sql);
		return CollectionUtils.isNotEmpty(resutlList) ? resutlList : new ArrayList<>();
	}

	/**
	 * 获取用户适应的活动规则
	 * 
	 * @param clubType:(俱乐部类型:1:网红
	 *            2:明星 3:片场)
	 * @param clubUserId:俱乐部对应的userId
	 * @return
	 */
	private String getUserActivityRule(int clubType, String clubUserId) {
		StringBuilder sqlBuilder = new StringBuilder();
		if (clubType > 0) {
			sqlBuilder.append(" and (a.club_display_rule=0 ");
			if (clubType == 1)
				sqlBuilder.append(" OR a.club_display_rule=1 OR a.club_display_rule=5 OR a.club_display_rule=6 ");
			if (clubType == 2)
				sqlBuilder.append(" OR a.club_display_rule=2 OR a.club_display_rule=4 OR a.club_display_rule=5 ");
			if (clubType == 3)
				sqlBuilder.append(" OR a.club_display_rule=3 OR a.club_display_rule=4 OR a.club_display_rule=6 ");
			if (StringUtils.isNotBlank(clubUserId))
				sqlBuilder.append(
						" OR ( a.id in( SELECT activity_id FROM activity_club_relation b WHERE b.user_id= ? ) ) ");
			sqlBuilder.append(" ) ");
		}
		return sqlBuilder.toString();
	}

	/**
	 * 获取活动数
	 * 
	 * @param clubType:(俱乐部类型:1:网红
	 *            2:明星 3:片场)
	 * @param clubUserId:俱乐部对应的userId
	 * @return
	 */
	public Long queryActivityCount(int clubType, String clubUserId) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT count(1) count FROM activity a WHERE a.is_del = 0 ");
		if (clubType > 0) {
			sqlBuilder.append(getUserActivityRule(clubType, clubUserId));
			return jdbcTemplate.queryForObject(sqlBuilder.toString(), new Object[] { clubUserId }, Long.class);
		}
		return jdbcTemplate.queryForObject(sqlBuilder.toString(), Long.class);
	}

	/**
	 * 获取活动列表
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @param clubType:(俱乐部类型:1:网红
	 *            2:明星 3:片场)
	 * @param clubUserId:俱乐部对应的userId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List queryActivityList(int pageNo, int pageSize, int clubType, String clubUserId) {
		List resutlList;
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder
				.append("SELECT a.id activityId, a.title title, DATE_FORMAT(a.start_date,'%Y-%m-%d %H:%i:%s') startDate, DATE_FORMAT(a.end_date,'%Y-%m-%d %H:%i:%s') endDate, "
						+ " DATE_FORMAT(a.create_date,'%Y-%m-%d %H:%i:%s') createDate, a.link activityLink, a.img_path imgPath FROM activity a "
						+ " WHERE a.is_del = 0 ");
		if (clubType > 0) {
			sqlBuilder.append(getUserActivityRule(clubType, clubUserId));
			sqlBuilder.append(" ORDER BY a.sort ASC limit ? , ? ");
			resutlList = jdbcTemplate.queryForList(sqlBuilder.toString(), clubUserId, (pageNo - 1) * pageSize,
					pageSize);
		} else {
			sqlBuilder.append(" ORDER BY a.sort ASC limit ? , ? ");
			resutlList = jdbcTemplate.queryForList(sqlBuilder.toString(), (pageNo - 1) * pageSize, pageSize);
		}
		return CollectionUtils.isNotEmpty(resutlList) ? resutlList : new ArrayList<>();
	}
}
