package com.hefan.activity.dao;

import com.hefan.activity.bean.HfActivityCfgAttr;
import com.hefan.common.orm.dao.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lxw on 2016/11/30.
 */
@Repository
public class HfActivityCfgAttrDao extends BaseDaoImpl<HfActivityCfgAttr> {

    /**
     * 获取活动属性列表
     * @param activityId
     * @return
     */
    public List<HfActivityCfgAttr> findHfActivityCfgAttrList(long activityId) {
        String sql = "select * from hf_activity_cfg_attr where activity_id=? and delete_flag=0";
        return this.query(sql,new Object[] {activityId});
    }

    /**
     * 获取活动属性
     * @param activityId
     * @param attrId
     * @return
     */
    public long findHfActivityCfgAttrAvlidNum(long activityId, long attrId) {
        String sql = "select attr_avlid_val from hf_activity_cfg_attr where activity_id=? and id=? and delete_flag=0";
        Long result = this.getJdbcTemplate().queryForObject(sql, new Object[] {activityId,attrId}, Long.class);
        if(result == null) {
            return 0L;
        }
        return result;
    }

    /**
     * 更新属性表有效值
     * @param modifyMode
     * @param activitId
     * @param attrId
     * @param avlidNum
     * @return
     */
    public int updateHfActivityCfgAttrAvlidNum(int modifyMode,long activitId,long attrId,long avlidNum) {
        String attrValidSql = "";
        if(modifyMode == 1) {
            attrValidSql = "attr_avlid_val+";
        }
        String sql = "update hf_activity_cfg_attr set attr_avlid_val="+attrValidSql+"? where  activity_id=? and id=? and delete_flag=0 and "+attrValidSql+"? >= 0";
        Object[] params = new Object[] {avlidNum, activitId,attrId,avlidNum};
        return getJdbcTemplate().update(sql, params);
    }

}
