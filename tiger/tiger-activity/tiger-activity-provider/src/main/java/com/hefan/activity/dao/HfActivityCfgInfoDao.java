package com.hefan.activity.dao;

import com.hefan.activity.bean.HfActivityCfgInfo;
import com.hefan.common.orm.dao.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lxw on 2016/11/17.
 */
@Repository
public class HfActivityCfgInfoDao extends BaseDaoImpl<HfActivityCfgInfo> {

    /**
     * 根据id 获取活动配置信息
     * @param id
     * @return
     */
    public HfActivityCfgInfo findActivityById(long id) {
        StringBuilder sqlBuilder = new StringBuilder("select id, activity_name,exec_clazz,check_clazz,");
        sqlBuilder.append("begin_date,end_date,give_num,create_time,update_time,update_time,delete_flag ")
                  .append(" from hf_activity_cfg_info where id=? and begin_date<=now() and end_date>=now() and delete_flag=0");
        List<Object> params = new ArrayList<Object>();
        params.add(id);
        return this.get(sqlBuilder.toString(), params);
    }
}
