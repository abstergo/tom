package com.hefan.activity.dao;

import org.springframework.stereotype.Repository;

import com.hefan.activity.bean.AppTaskRelation;
import com.hefan.common.orm.dao.BaseDaoImpl;

@Repository
public class AppTaskRelationDao extends BaseDaoImpl<AppTaskRelation> {

}
