package com.hefan.activity.service;

import com.cat.common.entity.Page;
import com.hefan.activity.dao.ActicityDao;
import com.hefan.activity.itf.ActivityService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 活动
 * 
 * @author kevin_zhang
 *
 */
@Component("activityService")
public class ActicityServiceImpl implements ActivityService {

	@Resource
	ActicityDao acticityDao;

	/**
	 * 获取推荐活动
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public List getHotActivity() {
		return acticityDao.queryHotActivity();
	}

	/**
	 * 获取更多活动
	 * 
	 * @param page
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Page getMoreActivityList(Page page) {
		Long count = acticityDao.queryActivityCount(-1, null);
		page.setTotalItems(count);
		page.setResult(acticityDao.queryActivityList(page.getPageNo(), page.getPageSize(), -1, null));
		return page;
	}

	/**
	 * 获取俱乐部活动
	 * 
	 * @param page
	 * @param clubType:(俱乐部类型:1:网红
	 *            2:明星 3:片场)
	 * @param clubUserId:俱乐部对应的userId
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Page getClubActivityList(Page page, int clubType, String clubUserId) {
		Long count = acticityDao.queryActivityCount(clubType, clubUserId);
		page.setTotalItems(count);
		page.setResult(acticityDao.queryActivityList(page.getPageNo(), page.getPageSize(), clubType, clubUserId));
		return page;
	}
}
