package com.hefan.activity.dao;

import com.hefan.activity.bean.HfActivityAttend;
import com.hefan.common.orm.dao.BaseDaoImpl;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lxw on 2016/11/17.
 */
@Repository
public class HfActivityAttendDao extends BaseDaoImpl<HfActivityAttend> {

    /**
     * 根据活动id和用户userId获取参加活动次数
     * @param activityId
     * @param userId
     * @return
     */
    public int getActivityAttendCount(long activityId, String userId) {
        String sql = "select count(1) as rowNum from hf_activity_attend where activity_id=? and user_id=?";
        return getJdbcTemplate().queryForObject(sql, new Object[] {activityId,userId}, Integer.class);
    }

    /**
     * 保存活动参与信息
     * @param hfActivityAttend
     * @return
     */
    public int saveActivityAttendInfo(HfActivityAttend hfActivityAttend) {
        return saveBackRowNum(hfActivityAttend);
    }

    /**
     * 根据活动id获取最佳参加获得的信息
     * @param activityId
     * @param rowNum 最多查询记录数
     * @return
     */
    public List<HfActivityAttend> findActivityAttendHis(long activityId, int rowNum) {
        StringBuilder sql = new StringBuilder("select id,user_id,activity_id,create_time,give_num,ext_data,ext_num,ext_id,delete_flag,activity_attr_type,activity_attr_id ")
                .append(" from hf_activity_attend ")
                .append("where activity_id=? and delete_flag=0 and activity_attr_type>0 AND create_time<=NOW() order by create_time desc limit 0,?");
        List<HfActivityAttend> list = this.query(sql.toString(),new Object[] {activityId,rowNum});
        if(list == null) {
            list = new ArrayList<HfActivityAttend>();
        }
        return list;
    }
}
