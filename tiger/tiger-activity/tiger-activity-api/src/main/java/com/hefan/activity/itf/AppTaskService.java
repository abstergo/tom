package com.hefan.activity.itf;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.hefan.activity.bean.AppTaskInfo;
import com.hefan.activity.bean.AppTaskRelation;

public interface AppTaskService {
	
	/**
	 * 获取任务列表
	 * @Title: getAppTaskInfoList   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @return      
	 * @return: List<AppTaskInfo>
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月15日 下午3:42:13
	 */
	List<AppTaskInfo> getAppTaskInfoList();
	
	/**
	 * 根据type获取任务信息
	 * @Title: getAppTaskInfoListByType   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param type
	 * @return      
	 * @return: AppTaskInfo
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月17日 上午10:23:05
	 */
	AppTaskInfo getAppTaskInfoListByType(int type);
	
	/**
	 * 获取用户任务进度
	 * @Title: getAppTaskRelationByUserId   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @return      
	 * @return: AppTaskRelation
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月15日 下午3:56:04
	 */
	AppTaskRelation getAppTaskRelationByUserId(String userId);
	
	/**
	 * 新增任务关系表
	 * @Title: saveOrUpAppTaskRelation   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param appTaskRe
	 * @return      
	 * @return: AppTaskRelation
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月17日 上午10:18:18
	 */
	AppTaskRelation saveOrUpAppTaskRelation(AppTaskRelation appTaskRe);
	
	/**
	 * 获取给多少个主播送了礼物
	 * @Title: getGiftUserNum   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @param toUserId
	 * @param beginTime
	 * @param endTime
	 * @return      
	 * @return: Map<String,Object>
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月17日 下午4:51:59
	 */
	Map<String,Object> getGiftUserNum(String userId,String toUserId,Date beginTime,Date endTime,int source);
	
	/**
	 * 完成任务
	 * @Title: updateAppTaskStatus   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param appTaskRe
	 * @param appTaskInfo
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年11月17日 下午8:53:40
	 */
	int updateAppTaskStatus(AppTaskRelation appTaskRe,AppTaskInfo appTaskInfo);

}
