package com.hefan.activity.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by lxw on 2016/11/17.
 */
@Entity(tableName="hf_activity_attend")
public class HfActivityAttend extends BaseEntity {

    @Column("user_id")
    private String userId;
    @Column("activity_id")
    private long activityId;
    @Column("give_num")
    private int giveNum;
    @Column("ext_data")
    private String extData;
    @Column("ext_num")
    private String extNum;
    @Column("ext_id")
    private String extId;
    @Column("activity_attr_type")
    private int activityAttrType;
    @Column("activity_attr_id")
    private long activityAttrId;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getActivityId() {
        return activityId;
    }

    public void setActivityId(long activityId) {
        this.activityId = activityId;
    }

    public int getGiveNum() {
        return giveNum;
    }

    public void setGiveNum(int giveNum) {
        this.giveNum = giveNum;
    }

    public String getExtData() {
        return extData;
    }

    public void setExtData(String extData) {
        this.extData = extData;
    }

    public String getExtNum() {
        return extNum;
    }

    public void setExtNum(String extNum) {
        this.extNum = extNum;
    }

    public String getExtId() {
        return extId;
    }

    public void setExtId(String extId) {
        this.extId = extId;
    }

    public int getActivityAttrType() {
        return activityAttrType;
    }

    public void setActivityAttrType(int activityAttrType) {
        this.activityAttrType = activityAttrType;
    }

    public long getActivityAttrId() {
        return activityAttrId;
    }

    public void setActivityAttrId(long activityAttrId) {
        this.activityAttrId = activityAttrId;
    }
}
