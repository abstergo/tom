package com.hefan.activity.itf;

import com.hefan.activity.bean.HfActivityAttend;
import com.hefan.activity.bean.HfActivityCfgAttr;
import com.hefan.activity.bean.HfActivityCfgInfo;

import java.util.List;

/**
 * Created by lxw on 2016/11/17.
 */
public interface HfActivityService {

    /**
     * 根据活动Id获取活动配置
     * @param activityId
     * @return
     */
    public HfActivityCfgInfo findHfActivityCfgInfo(long activityId);

    /**
     * 根据活动id和用户userId查询活动参加次数
     * @param activityId
     * @param userId
     * @return
     */
    public int findActivityAttendNum(long activityId, String userId);

    /**
     * 保存参加活动记录
     * @param hfActivityAttend
     * @return
     */
    public int saveActivityAttend(HfActivityAttend hfActivityAttend);


    /**
     * 获取最近参加活动信息
     * @param activityId
     * @param rowNum
     * @return
     */
    public List findActivityAttendHis(long activityId, int rowNum);

    /**
     * 获取活动属性列表，只包含有效值存在的列表
     * @param activityId
     * @return
     */
    public List<HfActivityCfgAttr> findAvlidActivitCfgAttrList(long activityId);

    /**
     * 获取活动属性信息
     * @param activityId
     * @return
     */
    public List<HfActivityCfgAttr> findActivityAttrList(long activityId);

    /**
     * 获取活动属性可用数量
     * @param activityId
     * @param attrId
     * @return
     */
    public Long getActivitAttrIsUseNum(long activityId,long attrId);

    /**
     * 记录活动属性的有效值
     * @param activityId
     * @param attrId
     * @param avlidNum
     * @return
     */
    public int incrActivityCfgAttrAvlidNum(long activityId, long attrId, long avlidNum);
}
