package com.hefan.activity.itf;

import com.cat.common.entity.Page;

import java.util.List;

/**
 * 活动
 * 
 * @author kevin_zhang
 *
 */
public interface ActivityService {

	/**
	 * 获取推荐活动
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	List getHotActivity();

	/**
	 * 获取更多活动
	 * 
	 * @param page
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	Page getMoreActivityList(Page page);

	/**
	 * 获取俱乐部活动
	 * 
	 * @param page
	 * @param clubType:(俱乐部类型:1:网红
	 *            2:明星 3:片场)
	 * @param clubUserId:俱乐部对应的userId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	Page getClubActivityList(Page page, int clubType, String clubUserId);
}
