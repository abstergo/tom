package com.hefan.activity.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

@Entity(tableName = "app_task_relation")
public class AppTaskRelation extends BaseEntity{
	
	@Column("user_id")
    private  String userId; 
	@Column("tasks_json")
	private String tasksJson;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTasksJson() {
		return tasksJson;
	}
	public void setTasksJson(String tasksJson) {
		this.tasksJson = tasksJson;
	}

}
