package com.hefan.activity.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.domain.BaseEntity;

import java.util.Date;

/**
 * Created by lxw on 2016/11/17.
 */
public class HfActivityCfgInfo extends BaseEntity {

    @Column("activity_name")
    private String activityName;

    @Column("exec_clazz")
    private String  execClazz;

    @Column("begin_date")
    private Date beginDate;

    @Column("end_date")
    private Date endDate;

    @Column("give_num")
    private int giveNum;

    @Column("check_clazz")
    private String checkClazz;

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getExecClazz() {
        return execClazz;
    }

    public void setExecClazz(String execClazz) {
        this.execClazz = execClazz;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getGiveNum() {
        return giveNum;
    }

    public void setGiveNum(int giveNum) {
        this.giveNum = giveNum;
    }

    public String getCheckClazz() {
        return checkClazz;
    }

    public void setCheckClazz(String checkClazz) {
        this.checkClazz = checkClazz;
    }
}
