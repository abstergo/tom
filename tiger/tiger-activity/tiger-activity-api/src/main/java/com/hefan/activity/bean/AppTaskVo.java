package com.hefan.activity.bean;

import java.io.Serializable;

public class AppTaskVo implements Serializable {
	private int taskType;
	//任务
	private String taskName;
	//任务说明
	private String taskExplain;
	//任务图标
	private String taskIcon;
	//完成能获得的经验
	private int expValue;
	//完成能获得的饭票
	private int ticketValue;
	//状态  1-未领取  2-未完成  4-已领取
	private int taskStatus;
	//进度
	private int setbacks;
	//总刻度 （任务完成需要的数量）
	private int scaleValue;
	//拓展字段
	private String taskExpand;
	public int getTaskType() {
		return taskType;
	}
	public void setTaskType(int taskType) {
		this.taskType = taskType;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskExplain() {
		return taskExplain;
	}
	public void setTaskExplain(String taskExplain) {
		this.taskExplain = taskExplain;
	}
	public String getTaskIcon() {
		return taskIcon;
	}
	public void setTaskIcon(String taskIcon) {
		this.taskIcon = taskIcon;
	}
	public int getExpValue() {
		return expValue;
	}
	public void setExpValue(int expValue) {
		this.expValue = expValue;
	}
	public int getTicketValue() {
		return ticketValue;
	}
	public void setTicketValue(int ticketValue) {
		this.ticketValue = ticketValue;
	}
	public int getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(int taskStatus) {
		this.taskStatus = taskStatus;
	}
	public int getSetbacks() {
		return setbacks;
	}
	public void setSetbacks(int setbacks) {
		this.setbacks = setbacks;
	}
	public int getScaleValue() {
		return scaleValue;
	}
	public void setScaleValue(int scaleValue) {
		this.scaleValue = scaleValue;
	}
	public String getTaskExpand() {
		return taskExpand;
	}
	public void setTaskExpand(String taskExpand) {
		this.taskExpand = taskExpand;
	}
}
