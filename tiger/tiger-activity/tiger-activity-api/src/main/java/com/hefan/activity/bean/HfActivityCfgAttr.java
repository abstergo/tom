package com.hefan.activity.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by lxw on 2016/11/30.
 */
@Entity(tableName = "hf_activity_cfg_attr")
public class HfActivityCfgAttr extends BaseEntity {

    @Column("activity_id")
    private long activityId;

    @Column("attr_type")
    private int attrType;

    @Column("attr_name")
    private String attrName;

    @Column("attr_val")
    private long attrVal;

    @Column("attr_max_val")
    private long attrMaxVal;

    @Column("attr_avlid_val")
    private long attrAvlidVal;

    public long getActivityId() {
        return activityId;
    }

    public void setActivityId(long activityId) {
        this.activityId = activityId;
    }

    public int getAttrType() {
        return attrType;
    }

    public void setAttrType(int attrType) {
        this.attrType = attrType;
    }

    public String getAttrName() {
        return attrName;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public long getAttrVal() {
        return attrVal;
    }

    public void setAttrVal(long attrVal) {
        this.attrVal = attrVal;
    }

    public long getAttrMaxVal() {
        return attrMaxVal;
    }

    public void setAttrMaxVal(long attrMaxVal) {
        this.attrMaxVal = attrMaxVal;
    }

    public long getAttrAvlidVal() {
        return attrAvlidVal;
    }

    public void setAttrAvlidVal(long attrAvlidVal) {
        this.attrAvlidVal = attrAvlidVal;
    }
}
