package com.hefan.activity.bean;

import java.util.Date;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

@Entity(tableName = "app_task_info")
public class AppTaskInfo extends BaseEntity{

	@Column("task_type")
    private  Integer taskType;
	@Column("task_name")
    private  String taskName;
	@Column("task_explain")
	private String taskExplain;
	@Column("task_icon")
	private String taskIcon;
	@Column("exp_value")
	private Integer expValue;
	@Column("ticket_value")
	private Integer ticketValue;
	@Column("scale_value")
	private Integer scaleValue;
	@Column("begin_time")
	private Date beginTime;
	@Column("end_time")
	private Date endTime;
	
	public Integer getTaskType() {
		return taskType;
	}
	public void setTaskType(Integer taskType) {
		this.taskType = taskType;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	public String getTaskExplain() {
		return taskExplain;
	}
	public void setTaskExplain(String taskExplain) {
		this.taskExplain = taskExplain;
	}
	public String getTaskIcon() {
		return taskIcon;
	}
	public void setTaskIcon(String taskIcon) {
		this.taskIcon = taskIcon;
	}
	public Integer getExpValue() {
		return expValue;
	}
	public void setExpValue(Integer expValue) {
		this.expValue = expValue;
	}
	public Integer getTicketValue() {
		return ticketValue;
	}
	public void setTicketValue(Integer ticketValue) {
		this.ticketValue = ticketValue;
	}
	public Integer getScaleValue() {
		return scaleValue;
	}
	public void setScaleValue(Integer scaleValue) {
		this.scaleValue = scaleValue;
	}
	public Date getBeginTime() {
		return beginTime;
	}
	public void setBeginTime(Date beginTime) {
		this.beginTime = beginTime;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
}
