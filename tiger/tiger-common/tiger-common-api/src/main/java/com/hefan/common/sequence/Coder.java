package com.hefan.common.sequence;

/**
 * @author: ninglijun
 * @description <p>
 * 定义生成序列的方式
 * </p>
 */
public interface Coder {

    /**
     * 序列名称,需要唯一
     *
     * @return
     */
    String sequence();

    /**
     * 序列产生的逻辑
     *
     * @param ownerCode
     * @param sequenceValue
     * @return
     */
    String encode(String ownerCode, long sequenceValue);

}
