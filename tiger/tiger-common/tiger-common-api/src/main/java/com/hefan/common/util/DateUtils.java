package com.hefan.common.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by IntelliJ IDEA. User: liufei Date: 11-10-28 Time: 下午7:30 To change
 * this template use File | Settings | File Templates.
 */
public class DateUtils {
    public static long timemillis_minute = 1000 * 60;
    public static long timemillis_hour = timemillis_minute * 60;
    public static long timemillis_day = timemillis_hour * 24;

    public static DateFormat parser_day = new SimpleDateFormat("yyyy-MM-dd");
    public static DateFormat parser_day_num = new SimpleDateFormat("yyyyMMdd");
    public static DateFormat parser_GMT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    /**
     * 得到day后的日期
     *
     * @param strDate
     * @param day
     * @return 字符串格式
     */
    public static String addDay(String strDate, int day) {
        if (strDate == null) {
            return null;
        }
        Date date = str2Date(strDate, "yyyy-MM-dd");
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, day);
        return date2SimpleStr(calendar.getTime());

    }

    public static boolean afterNow(Date curDate) {
        Date date = new Date();
        long curTimes = curDate.getTime();
        long now = date.getTime();
        return curTimes - now > 0;
    }

    /**
     * 得到当前短日期
     *
     * @return
     */
    public static String currentDate() {
        return currentDate("yyyy-MM-dd");
    }

    public static String currentDate(String format) {
        GregorianCalendar calenda = new GregorianCalendar();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(calenda.getTime());
    }

    /**
     * 得到当前时间,带上时分秒的当前时间
     *
     * @return
     */
    public static Date currentTime() {
        GregorianCalendar calenda = new GregorianCalendar();
        return calenda.getTime();
    }

    /**
     * 返回string类型，格式为yyyy-MM-dd
     *
     * @param date
     * @return
     */
    public static String date2SimpleStr(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        if (date == null) {
            return "";
        } else {
            return sdf.format(date);
        }
    }

    /**
     * java.util.Date转换为String
     *
     * @param date
     * @return
     */
    public static String date2Str(Date date) {
        return date2Str(date, "yyyy-MM-dd HH:mm:ss");
    }

    public static String date2Str(Date date, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        if (date == null) {
            return "";
        } else {
            return sdf.format(date);
        }
    }

    /**
     * 计算2个日期之间的天数
     *
     * @param start
     * @param end
     * @return
     */
    public static long getDaysBetween(Date start, Date end) {
        long d = (end.getTime() - start.getTime()) / (1000 * 60 * 60 * 24) + 1;
        return d;
    }

    public static long getHoursBetween(Date start, Date end) {
        long d = (end.getTime() - start.getTime()) / (1000 * 60 * 60) + 1;
        return d;
    }

    /**
     *
     * @param year
     *            int 年份
     * @param month
     *            int 月份
     *
     * @return int 某年某月的最后一天
     */
    public static int getLastDayOfMonth(int year, int month) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        // 某年某月的最后一天
        return cal.getActualMaximum(Calendar.DATE);
    }

    /**
     * 获取指定月的前一月（年）或后一月（年）
     *
     * @param dateStr
     * @param addYear
     * @param addMonth
     * @param addDate
     * @return
     * @throws ParseException
     * @throws Exception
     */
    public static String getLastMonth(String dateStr, int addYear, int addMonth, int addDate) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Date sourceDate = sdf.parse(dateStr);
        Calendar cal = Calendar.getInstance();
        cal.setTime(sourceDate);
        cal.add(Calendar.YEAR, addYear);
        cal.add(Calendar.MONTH, addMonth);
        cal.add(Calendar.DATE, addDate);

        SimpleDateFormat returnSdf = new SimpleDateFormat("yyyy-MM");
        String date = returnSdf.format(cal.getTime());
        return date;
    }

    /**
     * 当前月份
     *
     * @return
     */
    public static int getMonth() {
        Calendar cal = Calendar.getInstance();
        int month = cal.get(Calendar.MONTH) + 1;
        return month;
    }

    public static Date getPreviousNSecond(Date date, int n) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int second = calendar.get(Calendar.SECOND);
        calendar.set(Calendar.SECOND, second - n);
        return calendar.getTime();
    }

    /**
     * 获取周日历
     *
     * @param year
     * @param month
     * @return
     */
    public static List<String> getWeekCalender(int year, int month) {
        List<String> dlist = new ArrayList<String>();

        Calendar cn = Calendar.getInstance();
        cn.set(year, (month - 1), 1); // 设置年、月、日

        // 按星期显示, 打印当月1号前面的空格数,页面显示用
        for (int j = 1; j < (cn.get(Calendar.DAY_OF_WEEK)); j++) {
            dlist.add("&nbsp;");
        }
        // 当月所有日期
        int day = 1;
        for (int i = day; i <= day; i++) {
            dlist.add(String.valueOf(i));

            cn.add(Calendar.DAY_OF_WEEK, 1); // 增加一天
            day = cn.get(Calendar.DAY_OF_MONTH); // 得到当天是几号
        }

        return dlist;
    }

    /**
     * 当前年份
     *
     * @return
     */
    public static int getYear() {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        return year;
    }

    /**
     * String转换为java.util.Date
     *
     * @param date
     * @param format
     *            "yyyy-MM-dd"
     * @return
     */
    public static Date str2Date(String date, String format) {
        DateFormat parser = new SimpleDateFormat(format);
        try {
            return parser.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static  Timestamp getCurrentTimeStamp(){
        return new Timestamp(new Date().getTime());
    }

    public static String getHms(long l) {
        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT+00:00"));
        return formatter.format(l);
    }

    public static Integer getYearMonth(){
        SimpleDateFormat dayFormat = new SimpleDateFormat("yyyyMM");
        Calendar calendar = Calendar.getInstance();
        Integer year_months = Integer.valueOf(dayFormat.format(calendar.getTime()));
        return year_months;
    }

    /**
     * 取n分钟之前的时间
     * @param n
     * @return
     */
    public   static  Date  getNminutesAgoTime (int n) {
       long time = n* 60 * 1000;
        Date now = new Date();
        Date before = new Date(now.getTime() - time);
        return  before;
    }
}
