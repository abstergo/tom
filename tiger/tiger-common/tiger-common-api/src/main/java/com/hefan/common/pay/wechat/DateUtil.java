package com.hefan.common.pay.wechat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	/**
	 * 获取时间 分钟差的时间字符串
	 * @param cDateTime
	 * @param days
	 * @param formart
	 * @return
	 */
	public static String getDateIntervalMinutes(Date cDateTime, int days, String formart) {
		SimpleDateFormat simpleDateFormate = new SimpleDateFormat(formart);
		Calendar c = Calendar.getInstance();
		c.setTime(cDateTime);
		c.add(Calendar.MINUTE, days);
		return simpleDateFormate.format(c.getTime());
	}
	
	/**
	 * 获取微信支付时间戳
	 * @return
	 */
	public static String getTimeStamp() {
		return String.valueOf(System.currentTimeMillis() / 1000);
	}
	
}
