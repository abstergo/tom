package com.hefan.common.point.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by lxw on 2016/11/28.
 */
@Entity(tableName = "hf_channel_point_info")
public class HfChannelPointInfo extends BaseEntity {

    @Column("user_id")
    private String userId;

    @Column("channel_no")
    private String channelNo;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getChannelNo() {
        return channelNo;
    }

    public void setChannelNo(String channelNo) {
        this.channelNo = channelNo;
    }
}
