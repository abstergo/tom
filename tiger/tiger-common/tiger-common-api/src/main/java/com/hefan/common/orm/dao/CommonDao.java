package com.hefan.common.orm.dao;

import com.cat.common.entity.Page;

import java.util.List;
import java.util.Map;

/**
 * Created by lxw on 2016/9/26.
 */
public interface CommonDao {

    /**
     * 执行更新sql
     * @param sql
     * @param params
     * @return
     */
    public int update(String sql, Object[] params);

    /**
     * 执行更新sql
     * @param sql
     * @param params
     * @return
     */
    public int update(String sql, List<Object> params);

    /**
     * 根据id获取数据对象
     * @param id
     * @param tableName
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T get(long id, String tableName, Class<T> clazz);
    /**
     * 根据SQL查询相应对象
     *
     * @param sql
     * @param params
     * @return
     */
    public<T> T get(String sql, List<Object> params, Class<T> clazz);

    public<T> List<T> query(String sql, Object[] params, Class<T> clazz);

    public<T> Page<T> findPage(Page<T> page, String sql, Object[] params, Class<T> clazz);

    public List<Map<String, Object>> queryMap(String sql);

    public List<Map<String, Object>> queryMap(String sql, Object... params);

    public Page<Map<String, Object>> findPageMap(Page<Map<String, Object>> page, String sql, Object... params);

    /**
     * 查询记录集条数
     * @param countSql
     * @param params
     * @return
     */
    public int queryCount(String countSql, Object... params);
}
