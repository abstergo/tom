package com.hefan.common.orm.annotation;

import com.hefan.common.orm.dao.BaseDaoImpl;

import java.lang.annotation.*;

/**
 * 设置模型类对应的数据库表的名称
 * <p/>
 * 在 {@link BaseDaoImpl#getTableName()} 中使用时,
 * 如果注解的值不为空,则优先选择注解的值;
 * 如果注解的值为空,则将类名称转化为下划线分割的字符返回作为表名称.
 *
 * @author ninglijun
 * @description:
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Entity {
    /**
     * the table name to bind
     *
     * @return
     */
    String tableName() default "";
}
