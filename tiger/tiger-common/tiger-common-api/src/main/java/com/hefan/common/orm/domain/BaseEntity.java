package com.hefan.common.orm.domain;

import com.cat.tiger.util.GlobalConstants;
import com.hefan.common.orm.annotation.Column;

import java.io.Serializable;
import java.util.Date;

/**
 * @author ninglijun
 * @description
 */
public class BaseEntity implements Serializable {
    /**
     * 主键
     */
    protected long id;

    /**
     * 是否删除，0是正常，1是删除
     * <p/>
     * 设置可以使用
     *
     */
    @Column("delete_flag")
    protected int deleteFlag;


    /**
     * 数据记录创建时间
     * <p/>
     * 写代码的时候不需要设置.通过数据库自动更新
     * <p/>
     * 创建表时将其默认字段设置为 <code>DEFAULT CURRENT_TIMESTAMP</code>
     */
    protected Date createTime;

    /**
     * 数据记录修改时间
     * <p/>
     * 写代码的时候不需要设置.通过数据库自动更新
     * <p/>
     * 创建表时将其默认字段设置为 <code>DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP</code>
     */
    protected Date updateTime;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * 设置数据状态为删除.
     * <p/>
     * 注:需要保存到数据库才会永久有效
     */
    public void delete() {
        setDeleteFlag(GlobalConstants.DELETE_FLAG_YES);
    }

    /**
     * 是否删除
     *
     * @return
     */
    public boolean isDeleted() {
        return getDeleteFlag() == GlobalConstants.DELETE_FLAG_YES;
    }

    @Override
    public String toString() {
        return "BaseEntity{" +
                "id=" + id +
                ", deleteFlag=" + deleteFlag +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}
