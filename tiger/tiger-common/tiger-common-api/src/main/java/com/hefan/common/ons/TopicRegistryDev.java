package com.hefan.common.ons;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ninglijun
 * @description <p>
 * 定义所有的Topic,同时定义Topic对应的Consumer和Producer
 * </p>
 */
public enum TopicRegistryDev {
	HEFAN_LIVE_ENTER_DEV("HEFAN_LIVE_ENTER_DEV","PID_HEFAN_LIVE_ENTER_DEV","CID_HEFAN_LIVE_ENTER_DEV"),
	HEFAN_LIVE_USER_EXPERIENCE_DEV("HEFAN_LIVE_USER_EXPERIENCE_DEV","PID_HEFAN_LIVE_USER_EXPERIENCE_DEV","CID_HEFAN_LIVE_USER_EXPERIENCE_DEV"),
    HEFANTV_OMS_DEV("HEFAN_OMS_DEV", "PID_HEFAN_OMS_DEV", "CID_HEFAN_OMS_DEV"),
    HEFANTV_OMS_IM_DEV("HEFAN_OMS_IM_DEV", "PID_HEFAN_OMS_IM_DEV", "CID_HEFAN_OMS_IM_DEV"),
    HEFANTV_OMS_LOG_DEV("HEFAN_OMS_LOG_DEV", "PID_HEFAN_OMS_LOG_DEV", "CID_HEFAN_OMS_LOG_DEV"),
    HEFANTV_RED_DEV("HEFAN_RED_DEV", "PID_HEFAN_RED_DEV", "CID_HEFAN_RED_DEV"),
    HEFANTV_PUSH_DEV("HEFAN_PUSH_DEV", "PID_HEFAN_PUSH_DEV", "CID_HEFAN_PUSH_DEV"),
    HEFANTV_IMCC_DEV("HEFAN_IMCC_DEV", "PID_HEFAN_IMCC_DEV", "CID_HEFAN_IMCC_DEV"),
    HEFANTV_REDQ_DEV("HEFAN_REDQ_DEV", "PID_HEFAN_REDQ_DEV", "CID_HEFAN_REDQ_DEV"),
    HEFAN_LIVE_START_DEV("HEFAN_LIVE_START_DEV", "PID_HEFAN_LIVE_START_DEV", "CID_HEFAN_LIVE_START_DEV"),
    HEFAN_ROBOT_AUTO_FILL_DEV("HEFAN_ROBOT_AUTO_FILL_DEV","PID_HEFAN_ROBOT_AUTO_FILL_DEV","CID_HEFAN_ROBOT_AUTO_FILL_DEV"),
    HEFAN_ORDER_DEV("HEFAN_ORDER_DEV","PID_HEFAN_ORDER_DEV","CID_HEFAN_ORDER_DEV"),
    HEFAN_MONI_DATA_DEV("HEFAN_MONI_DATA_DEV","PID_HEFAN_MONI_DATA_DEV","CID_HEFAN_MONI_DATA_DEV"),
    HEFAN_ROBOT_WHEN_START_DEV("HEFAN_ROBOT_WHEN_START_DEV", "PID_HEFAN_ROBOT_WHEN_START_DEV", "CID_HEFAN_ROBOT_WHEN_START_DEV"),
    HEFAN_ROBOT_WHEN_LIVING_DEV("HEFAN_ROBOT_WHEN_LIVING_DEV", "PID_HEFAN_ROBOT_WHEN_LIVING_DEV", "CID_HEFAN_ROBOT_WHEN_LIVING_DEV"),
    HEFAN_ROBOT_ACTION_LIGHT_DELAY_DEV("HEFAN_ROBOT_ACTION_LIGHT_DELAY_DEV", "PID_HEFAN_ROBOT_ACTION_LIGHT_DELAY_DEV", "CID_HEFAN_ROBOT_ACTION_LIGHT_DELAY_DEV"),
    HEFAN_ROBOT_ACTION_WATCH_DELAY_DEV("HEFAN_ROBOT_ACTION_WATCH_DELAY_DEV", "PID_HEFAN_ROBOT_ACTION_WATCH_DELAY_DEV", "CID_HEFAN_ROBOT_ACTION_WATCH_DELAY_DEV"),
    HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY_DEV("HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY_DEV","PID_HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY_DEV","CID_HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY_DEV"),
    HEFAN_ROBOT_ENTER_EXIT_DELAY_DEV("HEFAN_ROBOT_ENTER_EXIT_DELAY_DEV","PID_HEFAN_ROBOT_ENTER_EXIT_DELAY_DEV","CID_HEFAN_ROBOT_ENTER_EXIT_DELAY_DEV");

    TopicRegistryDev(String topic, String producerId, String consumerId) {
        this.topic = topic;
        this.producerId = producerId;
        this.consumerId = consumerId;
    }

    private String topic;
    private String producerId;
    private String consumerId;

    public String getTopic() {
        return topic;
    }

    public String getProducerId() {
        return producerId;
    }

    public String getConsumerId() {
        return consumerId;
    }

    private static final HashMap<String, Set<TopicRegistryDev>> PRODUCER_REL_TOPICS = new HashMap<String, Set<TopicRegistryDev>>();
    private static final HashMap<String, Set<TopicRegistryDev>> CONSUMER_REL_TOPICS = new HashMap<String, Set<TopicRegistryDev>>();

    static {
        for (TopicRegistryDev registry : values()) {
            Set<TopicRegistryDev> pTopics = PRODUCER_REL_TOPICS.get(registry.producerId);
            Set<TopicRegistryDev> cTopics = CONSUMER_REL_TOPICS.get(registry.consumerId);
            if (pTopics == null) {
                pTopics = new HashSet<TopicRegistryDev>();
                PRODUCER_REL_TOPICS.put(registry.producerId, pTopics);
            }
            pTopics.add(registry);

            //topic maybe no consumer
            if (StringUtils.isBlank(registry.consumerId)) {
                continue;
            }
            if (cTopics == null) {
                cTopics = new HashSet<TopicRegistryDev>();
                CONSUMER_REL_TOPICS.put(registry.consumerId, cTopics);
            }
            cTopics.add(registry);
        }
    }

    public static Set<String> allProducerIds() {
        return PRODUCER_REL_TOPICS.keySet();
    }

    public static Set<String> allConsumerIds() {
        return CONSUMER_REL_TOPICS.keySet();
    }

    public static Set<TopicRegistryDev> getTopicsByProducerId(String producerId) {
        if (PRODUCER_REL_TOPICS.containsKey(producerId)) {
            return PRODUCER_REL_TOPICS.get(producerId);
        }
        return new HashSet<TopicRegistryDev>();
    }

    public static Set<TopicRegistryDev> getTopicsByConsumerId(String producerId) {
        if (CONSUMER_REL_TOPICS.containsKey(producerId)) {
            return CONSUMER_REL_TOPICS.get(producerId);
        }
        return new HashSet<TopicRegistryDev>();
    }
}
