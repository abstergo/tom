package com.hefan.common.exception;

/**
 * Sku下架异常
 *
 * @author diguage
 * @since 2016/2/3.
 */
public class SkuOffShelfException extends RootRuntimeException {
    public SkuOffShelfException(String msg) {
        super(msg);
    }

    public SkuOffShelfException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public SkuOffShelfException(Long id, String name) {
        this("商品(" + id + "-" + name + ")已经下架。请选购其他商品。");
    }
}
