package com.hefan.common.sequence;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

@Entity
public class CodeSequence extends BaseEntity {

    @Column
    private String sequence;
    @Column
    private long val;

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public long getVal() {
        return val;
    }

    public void setVal(long val) {
        this.val = val;
    }

    @Override
    public String toString() {
        return "CodeSequence{" +
                "sequence='" + sequence + '\'' +
                ", val=" + val +
                "} " + super.toString();
    }
}
