package com.hefan.common.point.itf;

import com.hefan.common.point.bean.HfChannelPointInfo;

/**
 * Created by lxw on 2016/11/28.
 */
public interface ChannelPointService {

    /**
     * 保存埋点记录信息
     * @param hfChannelPointInfo
     * @return
     */
    public int initChannelPointInfo(HfChannelPointInfo hfChannelPointInfo);
}
