package com.hefan.common.exception;


/**
 * 库存不足异常
 *
 * @author diguage
 * @since 2016/2/3.
 */
public class InventoryNotEnoughException extends RootRuntimeException {
    public InventoryNotEnoughException(String msg) {
        super(msg);
    }

    public InventoryNotEnoughException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public InventoryNotEnoughException(Long id, String name) {
        this("商品(" + id + "-" + name + ") 库存不足。请选购其他商品。");
    }
}
