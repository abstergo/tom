package com.hefan.common.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
/**
 * Created with IntelliJ IDEA. User: dongjingwei Date: 14-6-4 Time: 下午3:09 To
 * change this template use File | Settings | File Templates.
 */
public class CustomizedPropertyPlaceholderConfigurer extends
        PropertyPlaceholderConfigurer {

    private static Map<String, Object> ctxPropertiesMap = new HashMap<String, Object>();

    public static Object getContextProperty(final String name) {
        return ctxPropertiesMap.get(name);
    }

    @Override
    protected void processProperties(
            final ConfigurableListableBeanFactory beanFactoryToProcess,
            final Properties props) throws BeansException {
        super.processProperties(beanFactoryToProcess, props);
//        ctxPropertiesMap = new HashMap<String, Object>();
        for (Object key : props.keySet()) {
            String keyStr = key.toString();
            String value = props.getProperty(keyStr);
            ctxPropertiesMap.put(keyStr, value);
        }
    }
}