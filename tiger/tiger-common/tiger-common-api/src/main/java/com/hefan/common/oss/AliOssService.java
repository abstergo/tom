package com.hefan.common.oss;

import com.hefan.common.oss.dao.OssUploadCfgVo;

/**
 * Created by kevin_zhang on 01/03/2017.
 */
public interface AliOssService {

    /**
     * 获取上传图片所需的签名等认证信息
     *
     * @param expireTime
     * @return
     */
    OssUploadCfgVo getOssUploadCfgInfo(long expireTime);
}
