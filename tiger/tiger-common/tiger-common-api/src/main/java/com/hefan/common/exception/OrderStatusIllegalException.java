package com.hefan.common.exception;

/**
 * 订单状态非法
 *
 * @author diguage
 * @since 2016/2/29.
 */
public class OrderStatusIllegalException extends RootRuntimeException {

    public OrderStatusIllegalException(String msg) {
        super(msg);
    }

    public OrderStatusIllegalException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
