package com.hefan.common.ons;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ninglijun
 * @description <p>
 * 定义所有的Topic,同时定义Topic对应的Consumer和Producer
 * </p>
 */
public enum TopicRegistryTest {
	HEFAN_LIVE_ENTER_TEST("HEFAN_LIVE_ENTER_TEST","PID_HEFAN_LIVE_ENTER_TEST","CID_HEFAN_LIVE_ENTER_TEST"),
	HEFAN_LIVE_USER_EXPERIENCE_TEST("HEFAN_LIVE_USER_EXPERIENCE_TEST","PID_HEFAN_LIVE_USER_EXPERIENCE_TEST","CID_HEFAN_LIVE_USER_EXPERIENCE_TEST"),
    HEFANTV_OMS_TEST("HEFAN_OMS_TEST", "PID_HEFAN_OMS_TEST", "CID_HEFAN_OMS_TEST"),
    HEFANTV_OMS_IM_TEST("HEFAN_OMS_IM_TEST", "PID_HEFAN_OMS_IM_TEST", "CID_HEFAN_OMS_IM_TEST"),
    HEFANTV_OMS_LOG_TEST("HEFAN_OMS_LOG_TEST", "PID_HEFAN_OMS_LOG_TEST", "CID_HEFAN_OMS_LOG_TEST"),
    HEFANTV_RED_TEST("HEFAN_RED_TEST", "PID_HEFAN_RED_TEST", "CID_HEFAN_RED_TEST"),
    HEFANTV_PUSH_TEST("HEFAN_PUSH_TEST", "PID_HEFAN_PUSH_TEST", "CID_HEFAN_PUSH_TEST"),
    HEFANTV_IMCC_TEST("HEFAN_IMCC_TEST", "PID_HEFAN_IMCC_TEST", "CID_HEFAN_IMCC_TEST"),
    HEFANTV_REDQ_TEST("HEFAN_REDQ_TEST", "PID_HEFAN_REDQ_TEST", "CID_HEFAN_REDQ_TEST"),
    HEFAN_LIVE_START_TEST("HEFAN_LIVE_START_TEST", "PID_HEFAN_LIVE_START_TEST", "CID_HEFAN_LIVE_START_TEST"),
    HEFAN_ROBOT_AUTO_FILL_TEST("HEFAN_ROBOT_AUTO_FILL_TEST","PID_HEFAN_ROBOT_AUTO_FILL_TEST","CID_HEFAN_ROBOT_AUTO_FILL_TEST"),
    HEFAN_ORDER_TEST("HEFAN_ORDER_TEST","PID_HEFAN_ORDER_TEST","CID_HEFAN_ORDER_TEST"),
	HEFAN_MONI_DATA_TEST("HEFAN_MONI_DATA_TEST","PID_HEFAN_MONI_DATA_TEST","CID_HEFAN_MONI_DATA_TEST"),
    HEFAN_ROBOT_WHEN_START_TEST("HEFAN_ROBOT_WHEN_START_TEST", "PID_HEFAN_ROBOT_WHEN_START_TEST", "CID_HEFAN_ROBOT_WHEN_START_TEST"),
    HEFAN_ROBOT_WHEN_LIVING_TEST("HEFAN_ROBOT_WHEN_LIVING_TEST", "PID_HEFAN_ROBOT_WHEN_LIVING_TEST", "CID_HEFAN_ROBOT_WHEN_LIVING_TEST"),
    HEFAN_ROBOT_ACTION_LIGHT_DELAY_TEST("HEFAN_ROBOT_ACTION_LIGHT_DELAY_TEST", "PID_HEFAN_ROBOT_ACTION_LIGHT_DELAY_TEST", "CID_HEFAN_ROBOT_ACTION_LIGHT_DELAY_TEST"),
    HEFAN_ROBOT_ACTION_WATCH_DELAY_TEST("HEFAN_ROBOT_ACTION_WATCH_DELAY_TEST", "PID_HEFAN_ROBOT_ACTION_WATCH_DELAY_TEST", "CID_HEFAN_ROBOT_ACTION_WATCH_DELAY_TEST"),
    HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY_TEST("HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY_TEST","PID_HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY_TEST","CID_HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY_TEST"),
    HEFAN_ROBOT_ENTER_EXIT_DELAY_TEST("HEFAN_ROBOT_ENTER_EXIT_DELAY_TEST","PID_HEFAN_ROBOT_ENTER_EXIT_DELAY_TEST","CID_HEFAN_ROBOT_ENTER_EXIT_DELAY_TEST");

    TopicRegistryTest(String topic, String producerId, String consumerId) {
        this.topic = topic;
        this.producerId = producerId;
        this.consumerId = consumerId;
    }

    private String topic;
    private String producerId;
    private String consumerId;

    public String getTopic() {
        return topic;
    }

    public String getProducerId() {
        return producerId;
    }

    public String getConsumerId() {
        return consumerId;
    }

    private static final HashMap<String, Set<TopicRegistryTest>> PRODUCER_REL_TOPICS = new HashMap<String, Set<TopicRegistryTest>>();
    private static final HashMap<String, Set<TopicRegistryTest>> CONSUMER_REL_TOPICS = new HashMap<String, Set<TopicRegistryTest>>();

    static {
        for (TopicRegistryTest registry : values()) {
            Set<TopicRegistryTest> pTopics = PRODUCER_REL_TOPICS.get(registry.producerId);
            Set<TopicRegistryTest> cTopics = CONSUMER_REL_TOPICS.get(registry.consumerId);
            if (pTopics == null) {
                pTopics = new HashSet<TopicRegistryTest>();
                PRODUCER_REL_TOPICS.put(registry.producerId, pTopics);
            }
            pTopics.add(registry);

            //topic maybe no consumer
            if (StringUtils.isBlank(registry.consumerId)) {
                continue;
            }
            if (cTopics == null) {
                cTopics = new HashSet<TopicRegistryTest>();
                CONSUMER_REL_TOPICS.put(registry.consumerId, cTopics);
            }
            cTopics.add(registry);
        }
    }

    public static Set<String> allProducerIds() {
        return PRODUCER_REL_TOPICS.keySet();
    }

    public static Set<String> allConsumerIds() {
        return CONSUMER_REL_TOPICS.keySet();
    }

    public static Set<TopicRegistryTest> getTopicsByProducerId(String producerId) {
        if (PRODUCER_REL_TOPICS.containsKey(producerId)) {
            return PRODUCER_REL_TOPICS.get(producerId);
        }
        return new HashSet<TopicRegistryTest>();
    }

    public static Set<TopicRegistryTest> getTopicsByConsumerId(String producerId) {
        if (CONSUMER_REL_TOPICS.containsKey(producerId)) {
            return CONSUMER_REL_TOPICS.get(producerId);
        }
        return new HashSet<TopicRegistryTest>();
    }
}
