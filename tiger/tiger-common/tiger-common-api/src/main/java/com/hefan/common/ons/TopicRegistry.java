package com.hefan.common.ons;

import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ninglijun
 * @description <p>
 * 定义所有的Topic,同时定义Topic对应的Consumer和Producer
 * </p>
 */
public enum TopicRegistry {
	HEFAN_LIVE_ENTER("HEFAN_LIVE_ENTER","PID_HEFAN_LIVE_ENTER","CID_HEFAN_LIVE_ENTER"),
	HEFAN_LIVE_USER_EXPERIENCE("HEFAN_LIVE_USER_EXPERIENCE","PID_HEFAN_LIVE_USER_EXPERIENCE","CID_HEFAN_LIVE_USER_EXPERIENCE"),
    HEFANTV_TEST2("HEFAN_TEST", "PID_HEFAN_TEST", "CID_HEFAN_TEST"),
    HEFANTV_OMS("HEFAN_OMS", "PID_HEFAN_OMS", "CID_HEFAN_OMS"),
    HEFANTV_OMS_IM("HEFAN_OMS_IM", "PID_HEFAN_OMS_IM", "CID_HEFAN_OMS_IM"),
    HEFANTV_OMS_LOG("HEFAN_OMS_LOG", "PID_HEFAN_OMS_LOG", "CID_HEFAN_OMS_LOG"),
    HEFANTV_RED("HEFAN_RED", "PID_HEFAN_RED", "CID_HEFAN_RED"),
    HEFANTV_PUSH("HEFAN_PUSH", "PID_HEFAN_PUSH", "CID_HEFAN_PUSH"),
    HEFANTV_IMCC("HEFAN_IMCC", "PID_HEFAN_IMCC", "CID_HEFAN_IMCC"),
    HEFANTV_REDQ("HEFAN_REDQ", "PID_HEFAN_REDQ", "CID_HEFAN_REDQ"),
    HEFAN_LIVE_START("HEFAN_LIVE_START", "PID_HEFAN_LIVE_START", "CID_HEFAN_LIVE_START"),
    HEFAN_ROBOT_AUTO_FILL("HEFAN_ROBOT_AUTO_FILL","PID_HEFAN_ROBOT_AUTO_FILL","CID_HEFAN_ROBOT_AUTO_FILL"),
    HEFAN_ORDER("HEFAN_ORDER","PID_HEFAN_ORDER","CID_HEFAN_ORDER"),
	HEFAN_MONI_DATA("HEFAN_MONI_DATA","PID_HEFAN_MONI_DATA","CID_HEFAN_MONI_DATA"),
    HEFAN_ROBOT_WHEN_START("HEFAN_ROBOT_WHEN_START", "PID_HEFAN_ROBOT_WHEN_START", "CID_HEFAN_ROBOT_WHEN_START"),
    HEFAN_ROBOT_WHEN_LIVING("HEFAN_ROBOT_WHEN_LIVING", "PID_HEFAN_ROBOT_WHEN_LIVING", "CID_HEFAN_ROBOT_WHEN_LIVING"),
    HEFAN_ROBOT_ACTION_LIGHT_DELAY("HEFAN_ROBOT_ACTION_LIGHT_DELAY", "PID_HEFAN_ROBOT_ACTION_LIGHT_DELAY", "CID_HEFAN_ROBOT_ACTION_LIGHT_DELAY"),
    HEFAN_ROBOT_ACTION_WATCH_DELAY("HEFAN_ROBOT_ACTION_WATCH_DELAY", "PID_HEFAN_ROBOT_ACTION_WATCH_DELAY", "CID_HEFAN_ROBOT_ACTION_WATCH_DELAY"),
    HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY("HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY","PID_HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY","CID_HEFAN_ROBOT_ACTION_RAND_LIGHT_DELAY"),
    HEFAN_ROBOT_ENTER_EXIT_DELAY("HEFAN_ROBOT_ENTER_EXIT_DELAY","PID_HEFAN_ROBOT_ENTER_EXIT_DELAY","CID_HEFAN_ROBOT_ENTER_EXIT_DELAY");

    TopicRegistry(String topic, String producerId, String consumerId) {
        this.topic = topic;
        this.producerId = producerId;
        this.consumerId = consumerId;
    }

    private String topic;
    private String producerId;
    private String consumerId;

    public String getTopic() {
        return topic;
    }

    public String getProducerId() {
        return producerId;
    }

    public String getConsumerId() {
        return consumerId;
    }

    private static final HashMap<String, Set<TopicRegistry>> PRODUCER_REL_TOPICS = new HashMap<String, Set<TopicRegistry>>();
    private static final HashMap<String, Set<TopicRegistry>> CONSUMER_REL_TOPICS = new HashMap<String, Set<TopicRegistry>>();

    static {
        for (TopicRegistry registry : values()) {
            Set<TopicRegistry> pTopics = PRODUCER_REL_TOPICS.get(registry.producerId);
            Set<TopicRegistry> cTopics = CONSUMER_REL_TOPICS.get(registry.consumerId);
            if (pTopics == null) {
                pTopics = new HashSet<TopicRegistry>();
                PRODUCER_REL_TOPICS.put(registry.producerId, pTopics);
            }
            pTopics.add(registry);

            //topic maybe no consumer
            if (StringUtils.isBlank(registry.consumerId)) {
                continue;
            }
            if (cTopics == null) {
                cTopics = new HashSet<TopicRegistry>();
                CONSUMER_REL_TOPICS.put(registry.consumerId, cTopics);
            }
            cTopics.add(registry);
        }
    }

    public static Set<String> allProducerIds() {
        return PRODUCER_REL_TOPICS.keySet();
    }

    public static Set<String> allConsumerIds() {
        return CONSUMER_REL_TOPICS.keySet();
    }

    public static Set<TopicRegistry> getTopicsByProducerId(String producerId) {
        if (PRODUCER_REL_TOPICS.containsKey(producerId)) {
            return PRODUCER_REL_TOPICS.get(producerId);
        }
        return new HashSet<TopicRegistry>();
    }

    public static Set<TopicRegistry> getTopicsByConsumerId(String producerId) {
        if (CONSUMER_REL_TOPICS.containsKey(producerId)) {
            return CONSUMER_REL_TOPICS.get(producerId);
        }
        return new HashSet<TopicRegistry>();
    }
}
