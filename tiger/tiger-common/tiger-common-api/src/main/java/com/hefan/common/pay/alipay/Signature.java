package com.hefan.common.pay.alipay;

import com.hefan.common.util.PayPropertiesUtils;

import java.util.Map;

/**
 * 支付宝支付签名处理
 * Created by lxw on 2016/10/13.
 */
public class Signature {

    /**
     * 生成签名结果
     * @param sPara 要签名的数组
     * @return 签名结果字符串
     */
    public static String buildRequestMysign(Map<String, String> sPara, String signType, String privateKey) {
        String prestr = AlipayCore.createLinkString(sPara); //把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
        //System.out.println("-------prestr:"+prestr);
        String mysign = "";
        if("RSA".equals(signType)){
            mysign = RSA.sign(prestr, privateKey, PayPropertiesUtils.getString("alipay.inputCharset"));
        } else if("MD5".equals(signType)) {
            mysign = MD5.sign(prestr,privateKey, PayPropertiesUtils.getString("alipay.inputCharset"));
        }
        return mysign;
    }

    public static void main(String[] args) {
        String RSA_PRIVATE_KEY = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJusnY2S8e8y3wcr\n" +
                "/FDXVjFQjOZVGH28jSfeG1OAyH4SKTXpglufVygYewCuHFiC2NI75KIvD3/sp5DI\n" +
                "lgD7R5JY0U0CbBmrBqkT5gesDQxUFoYZOKR5h03UAUN7VCmAe+1RsQsmt/xdHy2v\n" +
                "3K3SFmdH9Do726AVzrUjWL9SYHFFAgMBAAECgYAU9JmI0z0KC/kFyCAA6dvKa6Nr\n" +
                "5gyT8Gu38CgRh4Z1ohA2F6bamopq9VCpeMaMC6EQO8u9IUSe3cZ4sOewXiL2FItp\n" +
                "4kZXrg1W3apGsx6bby0C3nfin0QZtU4GwTi6p8fnPVl5b4ZMkaWZXCDdpio3w9bS\n" +
                "2DKZjP5Ae4WiA33RCQJBAMjsmH26Ni49QkV1N7ZcmDnx6p/bXYqLqZ1BrduObA8D\n" +
                "aAkbLlcWy6AuyQ3eyu5/P6ZeaD9mlC6NMs9VIrZOP28CQQDGWLVIJ+/Hn+Dxz1f1\n" +
                "EivaSifhSUfjAaYldgzadmhJ359OQJV0di61VNNRFP3JxJl/YnAbSnRwdd+ZiewW\n" +
                "XACLAkA15zNp14EdmpWi6LWPmPvF397gEAviVWGlrK8lzemzhWhtuPsPpi/uw15w\n" +
                "+CpdHYpH6d/x0mVCr+LHuMFdQ1TFAkBsEpesGj6XObD6cKyPVvhX94HlSeWGdjaO\n" +
                "QNVeD5hhcBxAKgaFL4Phv7dofZSO5LVyaDOHWzk4sbFE2pat1DFjAkEAo1TonGNv\n" +
                "Fj+hWniVv4U+0j8TSmxjE9Hti+4SQ7AhGZs2SzzIRHFe4aU8HikxRsyTgeSduDRb\n" +
                "/7naA9TUREzq3Q==";
        String str = "app_id=2016092201948315&biz_content={\"body\":\"饭票\",\"goods_type\":\"0\",\"out_trade_no\":\"699b74abb98944afbc1b9ee5022f74ba\",\"product_code\":\"QUICK_WAP_PAY\",\"seller_id\":\"2088421922935636\",\"subject\":\"饭票\",\"timeout_express\":\"1d\",\"total_amount\":\"0.01\"}&charset=utf-8&format=JSON&method=alipay.trade.wap.pay&notify_url=http://test.api.hefantv.com/payNotify/alipayWapNotify&return_url=https://baidu.com&sign_type=RSA&timestamp=2016-11-25 19:31:38&version=1.0";
        String mysign = RSA.sign(str,RSA_PRIVATE_KEY, "utf-8");
        System.out.println(mysign);

        String public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCbrJ2NkvHvMt8HK/xQ11YxUIzm\n" +
                "VRh9vI0n3htTgMh+Eik16YJbn1coGHsArhxYgtjSO+SiLw9/7KeQyJYA+0eSWNFN\n" +
                "AmwZqwapE+YHrA0MVBaGGTikeYdN1AFDe1QpgHvtUbELJrf8XR8tr9yt0hZnR/Q6\n" +
                "O9ugFc61I1i/UmBxRQIDAQAB";
        boolean isSing = RSA.verify(str,mysign,public_key,"utf-8");
        System.out.println("isSing:"+String.valueOf(isSing));
    }
}
