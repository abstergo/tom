package com.hefan.common.exception;

/**
 * 订单商品为空
 *
 * @author diguage
 * @since 2016/2/29.
 */
public class OrderItemEmptyException extends RootRuntimeException {
    public OrderItemEmptyException(String msg) {
        super(msg);
    }

    public OrderItemEmptyException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
