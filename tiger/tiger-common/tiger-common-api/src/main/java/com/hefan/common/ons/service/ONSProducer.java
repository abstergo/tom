package com.hefan.common.ons.service;

import com.hefan.common.ons.bean.Message;

/**
 * @author ninglijun
 * @description
 */
public interface ONSProducer {
    boolean sendMsg(Message message);
    boolean sendDelayMsg(Message message,long delayTime);
}
