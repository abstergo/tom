package com.hefan.common.orm.dao;

import com.cat.common.entity.Page;
import com.hefan.common.orm.domain.BaseEntity;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ninglijun
 * @author diguage
 */
public interface BaseDao<T extends BaseEntity> {

    /**
     * 如果ID为0, 则创建记录;
     * <p/>
     * 如果ID大于0, 则是更新数据记录.
     *
     * @param t Bean对象
     * @return
     */
    T save(T t);

    /**
     * 保存对象 返回影响行数
     * @param t Bean对象
     * @return
     */
    int saveBackRowNum(T t);

    /**
     * 根据ID查询记录
     *
     * @param id 主键值
     * @return
     */
    T get(long id);

    /**
     * 根据ID批量查询数据
     *
     * @param ids ID Set
     * @return <code>Map<Long, T></code> Key为数据主键, value为对应的数据记录
     */
    Map<Long, T> find(Set<Long> ids);

    /**
     * 根据SQL查询相应对象
     *
     * @param sql
     * @param params
     * @return
     */
    T get(String sql, List<Object> params);

    /**
     * 逻辑删除
     *
     * @param id 数据主键
     * @return
     */
    boolean delete(long id);

    /**
     * 批量删除
     *
     * @param ids ID
     * @return 返回执行结果
     */
    boolean delete(Set<Long> ids);

    /**
     * 物理删除
     *
     * @param id 数据主键
     * @return
     */
    boolean realDelete(long id);

    /**
     * 批量物理删除
     *
     * @param ids 数据主键 Set
     * @return 执行结果
     */
    boolean realDelete(Set<Long> ids);

    List<Map<String, Object>> queryMap(String sql, Object... params);

    List<T> query(String sql, Object... params);

    Page<T> findPage(Page<T> page, String sql, Object... params);

    Page<Map<String, Object>> findPageMap(Page<Map<String, Object>> page, String sql, Object... params);
}
