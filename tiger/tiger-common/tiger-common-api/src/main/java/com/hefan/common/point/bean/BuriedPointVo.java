package com.hefan.common.point.bean;

import java.io.Serializable;

/**
 * Created by lxw on 2016/11/28.
 */
@SuppressWarnings("serial")
public class BuriedPointVo implements Serializable{
    private String channelNo;//渠道号，渠道来源:app下载渠道）
    private String deviceType;//设备类型 ios|android|pc
    private String deviceModel;//设备型号如iphone5、mi2
    private String uid;//用户的登录后唯一标示
    private String appVer;//app版本（1.0.1）
    private String appCode;//app版本号（16）
    private String devImei;//设备imei，苹果无法获取，采用系统策略生成。
    private String sysVersion;//安卓或者苹果的系统版本
    private String netType;//1:1g，2:2g，3:3g，4:4g,5:wifi,6:其
    private String cIp;//用户ip
    private String loc;//用户经纬度，用逗号分割

    public String getChannelNo() {
        return channelNo;
    }

    public void setChannelNo(String channelNo) {
        this.channelNo = channelNo;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAppVer() {
        return appVer;
    }

    public void setAppVer(String appVer) {
        this.appVer = appVer;
    }

    public String getDevImei() {
        return devImei;
    }

    public void setDevImei(String devImei) {
        this.devImei = devImei;
    }

    public String getSysVersion() {
        return sysVersion;
    }

    public void setSysVersion(String sysVersion) {
        this.sysVersion = sysVersion;
    }

    public String getNetType() {
        return netType;
    }

    public void setNetType(String netType) {
        this.netType = netType;
    }

    public String getcIp() {
        return cIp;
    }

    public void setcIp(String cIp) {
        this.cIp = cIp;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}
}
