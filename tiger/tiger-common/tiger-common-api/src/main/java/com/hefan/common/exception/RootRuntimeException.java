package com.hefan.common.exception;

import org.springframework.core.NestedRuntimeException;

/**
 * 根异常, 项目中所有异常都继承至这个异常
 *
 * @author diguage
 * @since 2016/2/3.
 */
public class RootRuntimeException extends NestedRuntimeException {
    public RootRuntimeException(String msg) {
        super(msg);
    }

    public RootRuntimeException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
