package com.hefan.common.sequence;

/**
 * @author ninglijun@guolele.com
 * @description <p>
 * 产生序列的方式
 * </p>
 */
public enum CoderType {
    /**
     * Common
     */
    COMMON,
    /**
     * 交易编码
     */
    TRANSACTION,
    /**
     * 微商订单编码
     */
    WDORDER,
    /**
     * 微商用户登录用户名编码
     */
    WUSERNAME
}