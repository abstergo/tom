package com.hefan.common.exception;


/**
 * Sku 不存在异常
 *
 * @author diguage
 * @since 2016/2/3.
 */
public class SkuNotExistException extends RootRuntimeException {

    public SkuNotExistException(String msg) {
        super(msg);
    }

    public SkuNotExistException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public SkuNotExistException(long id, String name) {
        this("商品(" + id + "-" + name + ") 不存在或者已经删除。");
    }
}
