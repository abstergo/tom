package com.hefan.common.orm.annotation;

import com.hefan.common.orm.dao.BaseDaoImpl;

import java.lang.annotation.*;

/**
 * 在 create 或者 update 时,根据这个值确定对应的数据库表中的字段名称
 * <p/>
 * 在 {@link BaseDaoImpl} 中使用时,
 * 如果没有设置其值,则自动读取添加该注解的属性名称并转化为下划线分割的小写形式作为其字段名称.
 * <p/>
 * 注意: 需要insert 的字段才加这个注解，其他的如果可以在数据库中自动生成最好自动生成
 *
 * @author ninglijun
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Column {
    String value() default "";
}
