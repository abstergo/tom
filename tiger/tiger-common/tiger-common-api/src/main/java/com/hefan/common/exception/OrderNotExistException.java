package com.hefan.common.exception;

/**
 * 订单不存在
 * @author  diguage
 * @since 2016/2/29.
 */
public class OrderNotExistException extends RootRuntimeException {
    public OrderNotExistException(String msg) {
        super(msg);
    }

    public OrderNotExistException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
