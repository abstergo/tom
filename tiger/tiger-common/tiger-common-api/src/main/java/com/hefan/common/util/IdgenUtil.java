package com.hefan.common.util;

import java.util.Random;
import java.util.UUID;

/**
 * Created by nigle on 2016/9/29.
 */
public class IdgenUtil {

    public static Integer getUserId(){
        StringBuilder str=new StringBuilder();//定义变长字符串
        Random random=new Random();
        //随机生成数字，并添加到字符串
        for(int i=0;i<8;i++){
            str.append(random.nextInt(10));
        }
        //将字符串转换为数字并输出
        int num=Integer.parseInt(str.toString());
        return num;
    }


    /**
     * 生成uuid
     * @return
     */
    public static String getUuid() {
        String uuid = UUID.randomUUID().toString();
        uuid = uuid.replaceAll("-", "");
        return uuid;
    }
}
