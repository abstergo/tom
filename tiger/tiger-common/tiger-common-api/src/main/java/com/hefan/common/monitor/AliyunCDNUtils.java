package com.hefan.common.monitor;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.cdn.model.v20141111.ForbidLiveStreamRequest;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.FormatType;
import com.aliyuncs.http.HttpResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.common.util.DynamicProperties;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by lxw on 2016/10/30.
 */
public class AliyunCDNUtils {

    private IAcsClient client = null;

    public AliyunCDNUtils() {
        if (null == client) {
            try {
                init();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void init() throws ClientException {
        // 初始化IAcsClient
        IClientProfile profile = DefaultProfile.getProfile(DynamicProperties.getString("aliyun.cdn.regionId"),
                DynamicProperties.getString("aliyun.cdn.accesskeyId"),DynamicProperties.getString("aliyun.cdn.secret"));
        client = new DefaultAcsClient(profile);
    }

    /**
     * 直播断流请求
     * @param userId
     * @param liveUuid
     * @return
     */
    public ResultBean closeLiveStreamRequest (String userId, String liveUuid) {
        ResultBean res = new ResultBean(ResultCode.SUCCESS,null);
        //初始化请求
        ForbidLiveStreamRequest forbidLiveStreamRequest = new ForbidLiveStreamRequest();
        forbidLiveStreamRequest.setAcceptFormat(FormatType.JSON);
        forbidLiveStreamRequest.setDomainName(DynamicProperties.getString("aliyun.cdn.DomainName"));
        forbidLiveStreamRequest.setAppName(DynamicProperties.getString("aliyun.cdn.setAppName"));
        forbidLiveStreamRequest.setStreamName(userId+"_"+liveUuid);
        forbidLiveStreamRequest.setLiveStreamType("publisher");
        //forbidLiveStreamRequest.setResumeTime("2015-12-01T17:37:00Z");
        try {
            HttpResponse httpResponse = client.doAction(forbidLiveStreamRequest);
            System.out.println(httpResponse.getUrl());
            String resStr = new String(httpResponse.getContent());
            if(StringUtils.isNotBlank(resStr)) {
                Map resMap = JSON.parseObject(resStr, HashMap.class);
                if(CollectionUtils.isEmpty(resMap) || !resMap.containsKey("RequestId")
                        || StringUtils.isBlank(String.valueOf(resMap.get("RequestId")))
                        || resMap.containsKey("Code")) {
                    return  new ResultBean(ResultCode.LiveCloseReqError,resMap.get("Message"));
                } else  {
                    res.setData(resMap.get("RequestId"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return  new ResultBean(ResultCode.LiveCloseReqError,null);
        }
        return res;
    }

}
