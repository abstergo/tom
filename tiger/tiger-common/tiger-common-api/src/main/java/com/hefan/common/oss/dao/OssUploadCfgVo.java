package com.hefan.common.oss.dao;

import java.io.Serializable;

/**
 * Created by kevin_zhang on 01/03/2017.
 */
@SuppressWarnings("serial")
public class OssUploadCfgVo implements Serializable {
    private String accessid;
    private String dir;
    private String host;
    private String policy;
    private String signature;
    private long expireTime;
    private long expireEndTime;

    public String getAccessid() {
        return accessid;
    }

    public void setAccessid(String accessid) {
        this.accessid = accessid;
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(long expireTime) {
        this.expireTime = expireTime;
    }

    public long getExpireEndTime() {
        return expireEndTime;
    }

    public void setExpireEndTime(long expireEndTime) {
        this.expireEndTime = expireEndTime;
    }
}
