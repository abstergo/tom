package com.hefan.common.exception;

/**
 * 数据非法
 *
 * @author diguage
 * @since 2016/2/25.
 */
public class DataIllegalException extends RootRuntimeException {
    public DataIllegalException(String msg) {
        super(msg);
    }

    public DataIllegalException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
