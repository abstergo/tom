package com.hefan.common.util;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;

import java.io.Serializable;

public class DynamicProperties implements Serializable {

    private static final long serialVersionUID = 4697358361020431226L;

    private static final String DEFAULT_SPLIT_CHAR = ",";

    private static PropertiesConfiguration applicationConfig = null;

    static {
        try {
            PropertiesConfiguration.setDefaultListDelimiter('+');
            applicationConfig = new PropertiesConfiguration("application.properties");
            applicationConfig.setReloadingStrategy(new FileChangedReloadingStrategy());
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    public static int getInt(String key) {
        return applicationConfig.getInt(key);
    }

    public static long getLong(String key) {
        return applicationConfig.getLong(key);
    }

    public static String getString(String key) {
        return applicationConfig.getString(key);
    }

    public static boolean getBoolean(String key) {
        return applicationConfig.getBoolean(key);
    }

    public static String[] getStringArr(String key) {
        return applicationConfig.getString(key).split(DEFAULT_SPLIT_CHAR);
    }

}
