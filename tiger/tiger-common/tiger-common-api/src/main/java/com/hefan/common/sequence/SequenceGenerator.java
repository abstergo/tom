package com.hefan.common.sequence;

/**
 * @author ninglijun
 * @description <p>
 * 定义Coder,并根据Coder的sequence和encode生成唯一序列
 * </p>
 * @see Coder
 */
public interface SequenceGenerator {
    /**
     * Coder需要在cherry-common-provider module中定义,并在CoderType enum中声明
     * CodeType enum
     *
     * @param coderType
     * @return 根据CoderType调用响应Coder的sequence策略生成下一个唯一编码
     */
    String nextVal(CoderType coderType);
}
