package com.hefan.common.util;

import java.util.Map;

/**
 * Map处理
 * 
 * @author kevin_zhang
 *
 */
public class MapUtils {

	@SuppressWarnings("rawtypes")
	public static int getIntValue(Map map, String key, int def) {
		try {
			if (map.containsKey(key))
				return Integer.parseInt(String.valueOf(map.get(key)));
			else
				return def;
		} catch (Exception e) {
			return def;
		}
	}

	@SuppressWarnings("rawtypes")
	public static String getStrValue(Map map, String key, String def) {
		try {
			if (map.containsKey(key))
				return String.valueOf(map.get(key));
			else
				return def;
		} catch (Exception e) {
			return def;
		}
	}

	@SuppressWarnings("rawtypes")
	public static long getLongValue(Map map, String key, long def) {
		try {
			if (map.containsKey(key))
				return Long.parseLong(String.valueOf(map.get(key)));
			else
				return def;
		} catch (Exception e) {
			return def;
		}
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isEmpty(Map map) {
		if (null == map || map.isEmpty())
			return true;
		else
			return false;
	}

	@SuppressWarnings("rawtypes")
	public static double getDoubleValue(Map map, String key, long def) {
		try {
			if (map.containsKey(key))
				return Double.parseDouble(String.valueOf(map.get(key)));
			else
				return def;
		} catch (Exception e) {
			return def;
		}
	}
}
