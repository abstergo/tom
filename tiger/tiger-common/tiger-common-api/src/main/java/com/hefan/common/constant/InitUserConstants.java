package com.hefan.common.constant;

/**
 * Created by lxw on 2016/9/27.
 */
public class InitUserConstants {

    /**
     * 默认头像
     */
    //public static final String DEFAULT_HEAD_IMG = "http://hefantv.b2social.cn/defaultHead.png";
    /**
     * 默认用户昵称
     */
    public static final String DEFAULT_NICK_NAME = "异次元";

    /**
     * 默认关注官方账号
     */
    public static final String OFFICE_USER_ID = "100000001";

}
