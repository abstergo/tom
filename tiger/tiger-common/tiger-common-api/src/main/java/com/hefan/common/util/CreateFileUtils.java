package com.hefan.common.util;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

public class CreateFileUtils {
	public static void createrFile(String fileName, String contentJson, String Globalpath) throws Exception {
		FileOutputStream out = new FileOutputStream(Globalpath + fileName);
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"));
		writer.write(contentJson);
		writer.close();
	}
}
