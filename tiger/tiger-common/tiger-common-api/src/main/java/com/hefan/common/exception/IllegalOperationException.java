package com.hefan.common.exception;

/**
 * 非法操作异常
 *
 * @author diguage
 * @since 2016/2/29.
 */
public class IllegalOperationException extends RootRuntimeException {
    public IllegalOperationException(String msg) {
        super(msg);
    }

    public IllegalOperationException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
