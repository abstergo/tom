package com.hefan.common.point.dao;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.common.point.bean.HfChannelPointInfo;
import org.springframework.stereotype.Repository;

/**
 * Created by lxw on 2016/11/28.
 */
@Repository
public class HfChannelPointInfoDao extends BaseDaoImpl<HfChannelPointInfo> {

    /**
     * 保存渠道埋点记录
     * @param hfChannelPointInfo
     * @return
     */
    public int saveChannelPointInfo(HfChannelPointInfo hfChannelPointInfo) {
        return this.saveBackRowNum(hfChannelPointInfo);
    }
}
