package com.hefan.common.ons.impl;

import com.alibaba.dubbo.rpc.protocol.rest.support.ContentType;
import com.hefan.common.ons.service.DeployCheckService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Component("deployCheckService")
@Path("check")
@Produces({ContentType.APPLICATION_JSON_UTF_8, ContentType.TEXT_XML_UTF_8})
public class DeployCheckServiceImpl implements DeployCheckService {

    Logger logger = LoggerFactory.getLogger(DeployCheckServiceImpl.class);

    @GET
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
    @Override
    public String index() {
        return "success";
    }

}
