package com.hefan.common.sequence.service;

import com.alibaba.dubbo.rpc.protocol.rest.support.ContentType;
import com.hefan.common.sequence.CoderType;
import com.hefan.common.sequence.SequenceGenerator;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.ws.rs.*;

/**
 * @author ninglijun
 * @description
 */
@Component("httpSequenceGenerator")
@Path("sequence")
@Consumes({ContentType.APPLICATION_JSON_UTF_8})
@Produces({ContentType.APPLICATION_JSON_UTF_8})
public class SequenceGeneratorRestImpl implements SequenceGenerator {

    @Resource
    SequenceGenerator sequenceGenerator;

    @GET
    @Path("/get/{code_type}")
    @Override
    public String nextVal(@PathParam("code_type") CoderType coderType) {
        return sequenceGenerator.nextVal(coderType);
    }
}
