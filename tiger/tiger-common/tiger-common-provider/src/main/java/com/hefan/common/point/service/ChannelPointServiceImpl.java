package com.hefan.common.point.service;

import com.hefan.common.point.bean.HfChannelPointInfo;
import com.hefan.common.point.dao.HfChannelPointInfoDao;
import com.hefan.common.point.itf.ChannelPointService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * Created by lxw on 2016/11/28.
 */
@Component("channelPointService")
public class ChannelPointServiceImpl implements ChannelPointService {

    @Resource
    private HfChannelPointInfoDao hfChannelPointInfoDao;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public int initChannelPointInfo(HfChannelPointInfo hfChannelPointInfo) {
        return hfChannelPointInfoDao.saveChannelPointInfo(hfChannelPointInfo);
    }
}
