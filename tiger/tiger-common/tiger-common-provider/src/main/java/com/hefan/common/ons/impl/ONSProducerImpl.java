package com.hefan.common.ons.impl;

import com.aliyun.openservices.ons.api.Producer;
import com.hefan.common.ons.ProducerFactory;
import com.cat.tiger.util.DateUtils;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.TopicRegistryDev;
import com.hefan.common.ons.TopicRegistryTest;
import com.hefan.common.ons.bean.Message;
import com.hefan.common.ons.service.ONSProducer;
import com.hefan.common.util.DynamicProperties;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author: ninglijun
 * @date: 15/5/22
 * @time: 下午2:25
 * @description:
 */
@Component("onsProducer")
public class ONSProducerImpl implements ONSProducer {

    private static final Logger logger = LoggerFactory.getLogger(ONSProducerImpl.class);

    @Resource
    ProducerFactory producerFactory;

    @Override
    public boolean sendMsg(Message message) {
        String onsEnv = DynamicProperties.getString("ons.env");

//        /**
//         * 如果是本地测试环境,这里篡改Topic为GLL_TEST,把所有的msg全都发送到GLL_TEST这个topic上,然后由GLL_TEST的Listener去决定由具体哪个Listener执行相关逻辑
//         */
        String topicName = message.getTopic().getTopic();
        if(StringUtils.equals(onsEnv,"local")) {
            message.setTag(message.getTopic().getTopic());
            message.setTopic(TopicRegistry.HEFANTV_TEST2);
            logger.info("topicName==local==" + topicName);
            Producer producer = producerFactory.getByTopic(topicName);
            try {
                com.aliyun.openservices.ons.api.Message msg = message.translateToOnsMessage(TopicRegistry.HEFANTV_TEST2.getProducerId());
                producer.send(msg);
                logger.info(DateUtils.currentDate(DateUtils.YYYY_MM_DD_HH_MM_SS) + "local receive msg,msg map body:{}, msg jsonbody:{}" + message.getMsgBody(), message.getJsonBody());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }else if(StringUtils.equals(onsEnv,"dev")) {
            topicName += "_DEV";
            String pid = "PID_"+topicName;
            logger.info("topicName==dev==" + topicName);
            Producer producer = producerFactory.getByTopic(topicName);
            try {
                com.aliyun.openservices.ons.api.Message msg = message.translateToOnsMessage(topicName);
                producer.send(msg);
                logger.info(DateUtils.currentDate(DateUtils.YYYY_MM_DD_HH_MM_SS) + "dev receive msg,msg map body:{}, msg jsonbody:{}" + message.getMsgBody(), message.getJsonBody());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }else if(StringUtils.equals(onsEnv,"test")) {
            topicName += "_TEST";
            String pid = "PID_"+topicName;
            logger.info("topicName==test==" + topicName);
            Producer producer = producerFactory.getByTopic(topicName);
            try {
                com.aliyun.openservices.ons.api.Message msg = message.translateToOnsMessage(topicName);
                producer.send(msg);
                logger.info(DateUtils.currentDate(DateUtils.YYYY_MM_DD_HH_MM_SS) + "test receive msg,msg map body:{}, msg jsonbody:{}" + message.getMsgBody(), message.getJsonBody());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }else{
            logger.info("topicName==online==" + topicName);
            Producer producer = producerFactory.getByTopic(topicName);
            try {
                com.aliyun.openservices.ons.api.Message msg = message.translateToOnsMessage(topicName);
                producer.send(msg);
                logger.info(DateUtils.currentDate(DateUtils.YYYY_MM_DD_HH_MM_SS) + "online receive msg,msg map body:{}, msg jsonbody:{}" + message.getMsgBody(), message.getJsonBody());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean sendDelayMsg(Message message,long delayTime) {
        String onsEnv = DynamicProperties.getString("ons.env");

//        /**
//         * 如果是本地测试环境,这里篡改Topic为GLL_TEST,把所有的msg全都发送到GLL_TEST这个topic上,然后由GLL_TEST的Listener去决定由具体哪个Listener执行相关逻辑
//         */
        String topicName = message.getTopic().getTopic();
        if(StringUtils.equals(onsEnv,"local")) {
            message.setTag(message.getTopic().getTopic());
            message.setTopic(TopicRegistry.HEFANTV_TEST2);
            logger.info("topicName==local==" + topicName);
            Producer producer = producerFactory.getByTopic(topicName);
            try {
                com.aliyun.openservices.ons.api.Message msg = message.translateToOnsMessage(TopicRegistry.HEFANTV_TEST2.getProducerId());
                msg.setStartDeliverTime(System.currentTimeMillis() + delayTime);
                producer.send(msg);
                logger.info( "time{},local receive msg,msg map body:{}, msg jsonbody:{}" ,DateUtils.currentDate(DateUtils.YYYY_MM_DD_HH_MM_SS), message.getMsgBody(), message.getJsonBody());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }else if(StringUtils.equals(onsEnv,"dev")) {
            topicName += "_DEV";
            logger.info("topicName==dev==" + topicName);
            Producer producer = producerFactory.getByTopic(topicName);
            try {
                com.aliyun.openservices.ons.api.Message msg = message.translateToOnsMessage(topicName);
                msg.setStartDeliverTime(System.currentTimeMillis() + delayTime);
                producer.send(msg);
                logger.info("time{},dev receive msg,msg map body:{}, msg jsonbody:{}" ,DateUtils.currentDate(DateUtils.YYYY_MM_DD_HH_MM_SS), message.getMsgBody(), message.getJsonBody());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }else if(StringUtils.equals(onsEnv,"test")) {
            topicName += "_TEST";
            logger.info("topicName==test==" + topicName);
            Producer producer = producerFactory.getByTopic(topicName);
            try {
                com.aliyun.openservices.ons.api.Message msg = message.translateToOnsMessage(topicName);
                msg.setStartDeliverTime(System.currentTimeMillis() + delayTime);
                producer.send(msg);
                logger.info("time:{},test receive msg,msg map body:{}, msg jsonbody:{}" ,DateUtils.currentDate(DateUtils.YYYY_MM_DD_HH_MM_SS) ,message.getMsgBody(), message.getJsonBody());
            } catch (Exception e) {
                logger.error("测试ONS失败,{}",e);
                e.printStackTrace();
                return false;
            }
        }else{
            logger.info("topicName==online==" + topicName);
            Producer producer = producerFactory.getByTopic(topicName);
            try {
                com.aliyun.openservices.ons.api.Message msg = message.translateToOnsMessage(topicName);
                msg.setStartDeliverTime(System.currentTimeMillis() + delayTime);
                producer.send(msg);
                logger.info("time {},online receive msg,msg map body:{}, msg jsonbody:{}" ,DateUtils.currentDate(DateUtils.YYYY_MM_DD_HH_MM_SS), message.getMsgBody(), message.getJsonBody());
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
}
