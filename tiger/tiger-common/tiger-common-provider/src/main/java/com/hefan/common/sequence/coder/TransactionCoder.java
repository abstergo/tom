package com.hefan.common.sequence.coder;

import com.cat.common.util.Base36;
import com.cat.tiger.util.StrUtils;
import com.hefan.common.sequence.Coder;
import org.springframework.stereotype.Component;

/**
 * @author ninglijun
 * @description
 */
@Component
public class TransactionCoder implements Coder {
    private static final String GLL_TRANSACTION_CODE = "GLL_TRANSACTION_CODE";

    @Override
    public String sequence() {
        return GLL_TRANSACTION_CODE;
    }

    @Override
    public String encode(String ownerCode, long sequenceValue) {
        String base36 = Base36.encode(sequenceValue);
        return "GTX" + StrUtils.fixLength(base36, 7, '0', true);
    }
}
