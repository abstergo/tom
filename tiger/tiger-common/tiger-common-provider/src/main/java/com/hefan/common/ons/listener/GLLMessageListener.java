package com.hefan.common.ons.listener;

import com.aliyun.openservices.ons.api.MessageListener;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.TopicRegistryDev;
import com.hefan.common.ons.TopicRegistryTest;

/**
 * @author: ninglijun
 * @date: 15/5/26
 * @time: 上午9:19
 * @description:
 */
public interface GLLMessageListener extends MessageListener {
    TopicRegistry getTopicRegistry();
    TopicRegistryTest getTopicRegistryTest();
    TopicRegistryDev getTopicRegistryDev();
}
