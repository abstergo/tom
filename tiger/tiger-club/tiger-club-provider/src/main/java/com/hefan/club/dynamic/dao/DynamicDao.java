package com.hefan.club.dynamic.dao;

import com.cat.common.entity.Page;
import com.google.common.collect.Lists;
import com.hefan.club.dynamic.bean.Message;
import com.hefan.club.dynamic.bean.MessageConstant;
import com.hefan.club.dynamic.bean.PraiseLog;
import com.hefan.common.exception.DataIllegalException;
import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.common.orm.domain.BaseEntity;
import com.hefan.live.bean.LiveDynamicVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("ALL")
@Repository
public class DynamicDao extends BaseDaoImpl<BaseEntity> {

  @Resource
  JdbcTemplate jdbcTemplate;

  /**
   * 删除动态
   *
   * @param userId
   * @param messageId
   * @return
   */
  public void deleteMessage(String userId, String messageId) {
    StringBuilder sql = new StringBuilder();
    sql.append(" UPDATE");
    sql.append(" message ");
    sql.append(" SET is_del=1 where id=? and user_id=?");
    int result = jdbcTemplate.update(sql.toString(), messageId, userId);
    if (result <= 0) {
      throw new DataIllegalException(String.format("用户userId=%s删除动态messageId=%s失败", userId, messageId));
    }
  }

  /**
   * 关注的人的俱乐部
   *
   * @param
   * @return
   */
  @Deprecated
  public Page<Message> listClubIndex(Page po, String userId) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" mes.message_info messageInfo,mes.user_id userId, mes.transcode transcode,mes.path_trans path,IFNULL(mes.length,'') length,mes.is_top topStatus,IFNULL(mes.back_img,'') backImg,");
    sql.append(" comments_count commentCount,present_count presentCount,times time,");
    sql.append(" praise_count praiseCount,ewaizan praiseCount,mes.id messageId, mes.message_type messageType,mes.message_time messageTime,mes.from_type fromType ");
    sql.append(" FROM message mes");
    sql.append(" WHERE");
    sql.append("  mes.user_id IN (" + userId + ") AND mes.transcode=1 AND  mes.is_del!=1 order by message_time DESC");
    String countSql = "SELECT count(1) AS c FROM (" + sql + ") t";
    logger.info(countSql);
    int count = jdbcTemplate.queryForObject(countSql, Integer.class);
    po.setTotalItems(count);
    if (count == 0) {
      return po;
    }

    String pageSql = sql.toString();
    if (StringUtils.isNotBlank(po.getOrderBy()) && StringUtils.isNotBlank(po.getOrder())) {
      pageSql += " ORDER BY " + po.getOrderBy() + " " + po.getOrder() + " ";
    }
    pageSql += " LIMIT " + po.getOffset() + "," + po.getPageSize();

    logger.info(pageSql);
    List<Message> result = jdbcTemplate.query(pageSql, new BeanPropertyRowMapper(Message.class));
    po.setResult(result);
    return po;

  }

  /**
   * 主播俱乐部
   *
   * @param po
   * @return
   */
  public Page<Message> listMessage(Page po, String authorId, String userId) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" mes.message_info messageInfo,mes.user_id userId, mes.transcode transcode,mes.path_trans path,IFNULL(mes.length,'') length,mes.is_top topStatus,IFNULL(mes.back_img,'') backImg,");
    sql.append(" comments_count commentCount,present_count presentCount,0 watchStatus,times time,mes.is_del isDel,");
    sql.append(" praise_count praiseCount ,ewaizan,mes.id messageId, mes.message_type messageType,mes.message_time messageTime, mes.from_type fromType");
    sql.append(" FROM message mes");
    StringBuilder where = new StringBuilder();
    where.append(" WHERE mes.is_del!=1  ");
    if (!userId.equals(authorId)) {
      where.append(" and transcode=1");
    }
    where.append(" and mes.user_id='" + authorId + "' ORDER BY is_top DESC,message_time DESC");
    sql.append(where.toString());
    String countSql = "SELECT count(1) AS c FROM message mes " + where.toString();
    int count = jdbcTemplate.queryForObject(countSql, Integer.class);
    po.setTotalItems(count);
    if (count == 0) {
      return po;
    }

    String pageSql = sql.toString();
    if (StringUtils.isNotBlank(po.getOrderBy()) && StringUtils.isNotBlank(po.getOrder())) {
      pageSql += " ORDER BY " + po.getOrderBy() + " " + po.getOrder() + " ";
    }
    pageSql += " LIMIT " + po.getOffset() + "," + po.getPageSize();

    logger.info(pageSql);
    List<Message> result = jdbcTemplate.query(pageSql, new BeanPropertyRowMapper(Message.class));
    po.setResult(result);
    return po;
  }

  /**
   * 主播俱乐部
   *
   * @param po
   * @return
   */
  public List<Message> listMessageIds(Page po, String userId) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT id messageId FROM message WHERE is_del=0 ");
    sql.append(" and user_id=? ORDER BY is_top DESC,message_time DESC limit ?,? ");
    return jdbcTemplate.query(sql.toString(), new Object[] { userId, po.getOffset(), po.getPageSize() }, new BeanPropertyRowMapper(Message.class));
  }

  /**
   * 置顶
   *
   * @param userId
   * @param messageId
   * @return
   */
  public int top(String userId, String messageId) {
    StringBuilder sql = new StringBuilder();
    sql.append("UPDATE");
    sql.append(" message ");
    sql.append(" SET is_top=1 where user_id=? AND id=?");
    return jdbcTemplate.update(sql.toString(), userId, messageId);
  }

  /**
   * 去下置顶
   *
   * @param userId
   * @param messageId
   * @return
   */
  public int unTop(String userId, String messageId) {
    StringBuilder sql = new StringBuilder();
    sql.append(" UPDATE");
    sql.append(" message ");
    sql.append(" SET is_top=0 where user_id= ? AND id=? ");
    return jdbcTemplate.update(sql.toString(), userId, messageId);
  }

  /**
   * 查询置顶
   *
   * @param userId
   * @return
   */
  public List<Map<String, Object>> findTop(String userId) {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT ");
    sql.append(" id ");
    sql.append(" FROM message where user_id=? AND is_top=1");
    return jdbcTemplate.queryForList(sql.toString(), userId);

  }

 /* *//**
   * 动态相册
   *
   * @param messageId
   * @return
   *//*
  public List<DynamicAlbum> photoListForMessage(String messageId) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" id,msg_id msgId,YEAR(create_time) yea,MONTH(create_time) mon,message_info messageInfo,path ");
    sql.append(" FROM message_album where is_del=0");
    sql.append(" AND msg_id IN (" + messageId + ")");
    return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper(DynamicAlbum.class));
  }*/

  /**
   * 视频
   *
   * @param po
   * @param isSync
   * @param authorId
   * @return
   */
  public Page<Message> videoList(Page po, String isSync, String authorId, String userId) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" mes.message_info messageInfo, mes.user_id userId, mes.message_time messageTime, mes.path_trans pictureUrls, mes.length,mes.transcode transcode,");
    sql.append(" mes.comments_count commentCount, mes.from_type fromType, praise_count+ewaizan praiseCount, present_count presentCount, mes.id messageId, mes.message_type messageType,");
    sql.append(" back_img backImg FROM message mes WHERE  mes.user_id='" + authorId + "' and mes.message_type='3' and mes.is_del=0");
    if (StringUtils.isNotBlank(isSync)) {
      sql.append(" AND is_sync=1");
    }
    if (!userId.equals(authorId)) {
      sql.append(" and transcode=1");
    }
    sql.append(" order by message_time DESC ");
    String countSql = "SELECT count(1) AS c FROM (" + sql + ") t";
    logger.info(countSql);
    int count = jdbcTemplate.queryForObject(countSql, Integer.class);
    po.setTotalItems(count);
    if (count == 0) {
      return po;
    }

    String pageSql = sql.toString();
    if (StringUtils.isNotBlank(po.getOrderBy()) && StringUtils.isNotBlank(po.getOrder())) {
      pageSql += " ORDER BY " + po.getOrderBy() + " " + po.getOrder() + " ";
    }
    pageSql += " LIMIT " + po.getOffset() + "," + po.getPageSize();

    logger.info(pageSql);
    List<Message> result = jdbcTemplate.query(pageSql, new BeanPropertyRowMapper(Message.class));
    po.setResult(result);
    return po;
  }

  /**
   * 插入动态
   * 新增插入is_square字段
   * 修改时间2017/03/10
   *
   * @param message
   * @return
   */
  public int saveObj(Message message) {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("user_id", message.getUserId());
    parameters.put("message_type", message.getMessageType());
    parameters.put("path", message.getPath());
    parameters.put("message_info", message.getMessageInfo());
    //parameters.put("message_time", message.getMessageTime());
    parameters.put("length", message.getLength());
    parameters.put("transcode", message.getTranscode());
    parameters.put("job_id", message.getJobId());
    parameters.put("back_img", message.getBackImg());
    parameters.put("is_sync", message.getIsSync());
    parameters.put("from_type", message.getFromType());
    parameters.put("is_square", message.getIsSqure());
    String[] columns = parameters.keySet().toArray(new String[] {});
    SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate.getDataSource()).usingColumns(columns).withTableName("message").usingGeneratedKeyColumns("id");
    Number id = simpleJdbcInsert.executeAndReturnKey(parameters);
    return id.intValue();
  }

  /**
   * 加动态附加数（type：1：:评论数2：礼物数3：赞数）
   *
   * @param messageId
   * @param type
   * @return
   */
  public int addMessageCount(String messageId, String type) throws Exception {
    StringBuilder sql = new StringBuilder();
    sql.append(" UPDATE");
    sql.append(" message");
    sql.append(" SET");
    if (Integer.valueOf(type) == MessageConstant.COMMENTSCOUT) {
      sql.append(" comments_count=comments_count+1");
    }
    if (Integer.valueOf(type) == MessageConstant.PRESENTCOUNT) {
      sql.append(" present_count=present_count+1");
    }
    if (Integer.valueOf(type) == MessageConstant.PRAISECOUNT) {
      sql.append(" praise_count=praise_count+1");
    }
    sql.append(" WHERE id=?");
    return jdbcTemplate.update(sql.toString(), messageId);

  }

  /**
   * 加动态附加数（type：1：:评论数2：礼物数3：赞数）
   *
   * @param messageId
   * @param type
   * @return
   */
  public int reMessageCount(String messageId, String type) {
    StringBuilder sql = new StringBuilder();
    sql.append(" UPDATE");
    sql.append(" message");
    sql.append(" SET");
    if (Integer.valueOf(type) == MessageConstant.COMMENTSCOUT) {
      sql.append(" comments_count=comments_count-1");
    }
    if (Integer.valueOf(type) == MessageConstant.PRESENTCOUNT) {
      sql.append(" present_count=present_count-1");
    }
    if (Integer.valueOf(type) == MessageConstant.PRAISECOUNT) {
      sql.append(" praise_count=praise_count-1");
    }
    sql.append(" WHERE id=? ");
    return jdbcTemplate.update(sql.toString(), messageId);

  }

  /**
   * 更新transcode字段
   *
   * @param jobId
   * @param pathTrans
   * @return
   */
  public int updateTranscode(String jobId, String pathTrans) {
    StringBuilder sql = new StringBuilder();
    sql.append(" UPDATE ");
    sql.append(" message ");
    sql.append(" SET transcode=1 , path_trans=? where job_id=? ");
    Object[] paramArr = new Object[] { pathTrans, jobId };
    int[] paramtypeArr = new int[] { java.sql.Types.VARCHAR, java.sql.Types.VARCHAR };
    return jdbcTemplate.update(sql.toString(), paramArr, paramtypeArr);
  }

  public int insertPraiseLog(PraiseLog log) {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("user_id", log.getUserId());
    parameters.put("message_id", log.getMessageId());
    parameters.put("content", log.getContent());
    parameters.put("time", new Date());
    String[] columns = parameters.keySet().toArray(new String[] {});
    SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplate.getDataSource()).usingColumns(columns).withTableName("praise_log").usingGeneratedKeyColumns("id");
    Number id = simpleJdbcInsert.executeAndReturnKey(parameters);
    return id.intValue();
  }

  public Message getMessageById(String messageId) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" mes.message_info messageInfo,mes.user_id userId,mes.path_trans path,mes.length,mes.is_top topStatus,mes.back_img backImg,mes.transcode,");
    sql.append(" comments_count commentCount,present_count presentCount,is_del isDel,");
    sql.append(" praise_count praiseCount,ewaizan,mes.id messageId, mes.message_type messageType,mes.message_time messageTime,mes.times time,from_type fromType");
    sql.append(" FROM message mes ");
    sql.append(" WHERE");
    sql.append(" mes.id= ? and mes.is_del=0");
    List<Message> message = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper(Message.class), messageId);
    return CollectionUtils.isEmpty(message) ? null : message.get(0);
  }

  /**
   * 直播分享动态
   *
   * @param vo
   * @return
   */
  public int shareLive(LiveDynamicVo vo, int isSquare) {
    String sql = "insert into message(user_id,message_type,message_info,length,transcode,back_img,is_sync,from_type,path_trans,times,is_square) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
    Object[] o = new Object[] { vo.getUserId(), vo.getMessageType(), vo.getMessageInfo(), vo.getLength(), vo.getTranscode(), vo.getBackImg(), vo.getIsSync(), vo.getFromType(), vo.getPathTrans(),
        vo.getTimes(), isSquare };
    return jdbcTemplate.update(sql, o);
  }

  /**
   * 查看直播是否被分享过动态 false:未分享过
   *
   * @return
   */
  public boolean isShareToDynamic(String userId, String liveUuid) {
    String sql = "SELECT count(1) from message m where m.user_id = ? and INSTR(m.path_trans,?)>0";
    long count = jdbcTemplate.queryForObject(sql, new Object[] { userId, liveUuid }, Long.class);
    return count != 0;
  }

  public int checkPraise(String userId, String messageId) {
    return jdbcTemplate.queryForObject("SELECT COUNT(1) count from praise_log pl where user_id=? and message_id=?", new Object[] { userId, messageId }, Integer.class);

  }

  public List<Map<String, Object>> getMessageResByIds(List<Integer> ids) {
    StringBuffer querySql = new StringBuffer(200);
    querySql.append("select a.id,a.user_id,a.message_info,a.message_type,a.is_del,a.message_time from message a ");
    if (!CollectionUtils.isEmpty(ids)) {
      querySql.append(" where a.id in (");
      for (int i = 0; i < ids.size(); i++) {
        int id = ids.get(i) == null ? 0 : ids.get(i);
        querySql.append(id);
        if (i < ids.size() - 1) {
          querySql.append(",");
        }
      }
      querySql.append(")");
    }
    return jdbcTemplate.queryForList(querySql.toString());
  }

  public Map getMessageInfoById(long messageId) {
    List<Map<String, Object>> list = jdbcTemplate.queryForList("select * from message where id=? LIMIT 1", messageId);
    return !CollectionUtils.isEmpty(list) ? list.get(0) : null;
  }

  /**
   * 获取minMsgId之后广场页动态数据
   * 广场下一页数据
   * minMsgId==0 则返回首页数据
   */
  @Deprecated
  public Page<Message> listSquareMessage(Page po, int minMsgId) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" mes.message_info messageInfo,mes.user_id userId, mes.transcode transcode,mes.path_trans path,IFNULL(mes.length,'') length,mes.is_top topStatus,IFNULL(mes.back_img,'') backImg,");
    sql.append(" mes.comments_count commentCount,mes.present_count presentCount,0 watchStatus,times time,");
    sql.append(" praise_count praiseCount , ewaizan ,mes.id messageId, mes.message_type messageType,mes.message_time messageTime, mes.from_type fromType");
    sql.append(" FROM message mes");
    sql.append(" WHERE 1=1");
    if (minMsgId > 0) {
      sql.append(" AND mes.id < ?");
    }
    sql.append(" AND mes.is_square = 1 AND mes.transcode=1 AND mes.is_del =0  ORDER BY mes.message_time DESC");
    String countSql = "SELECT count(1) AS c FROM (" + sql + ") t";
    logger.info(countSql);
    int count = jdbcTemplate.queryForObject(countSql, Integer.class);
    po.setTotalItems(count);
    if (count == 0) {
      return po;
    }
    sql.append(" LIMIT ?");
    logger.info(sql.toString());
    List<Message> list = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper(Message.class), minMsgId, po.getPageSize());
    po.setResult(list);
    return po;
  }

  /**
   * 获取minMsgId之后广场页动态数据
   * 广场下一页数据
   * minMsgId==0 则返回首页数据
   */
  @Deprecated
  public List<Message> getSquareMsgBeforMsgIdByUserId(String userId, String minMsgId) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" mes.message_info messageInfo,mes.user_id userId, mes.transcode transcode,mes.path_trans path,IFNULL(mes.length,'') length,mes.is_top topStatus,IFNULL(mes.back_img,'') backImg,");
    sql.append(" mes.comments_count commentCount,mes.present_count presentCount,0 watchStatus,times time,");
    sql.append(" praise_count praiseCount, ewaizan ,mes.id messageId, mes.message_type messageType,mes.message_time messageTime, mes.from_type fromType");
    sql.append(" FROM message mes");
    sql.append(" WHERE mes.user_id = ? AND mes.id > ?");
    sql.append(" AND mes.is_square = 1 AND mes.transcode = 1 AND mes.is_del <> 1  ORDER BY mes.message_time DESC");
    logger.info(sql.toString());
    List<Message> list = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper(Message.class), userId, minMsgId);
    return CollectionUtils.isEmpty(list) ? Lists.newArrayList() : list;
  }

  /**
   * 获取广场count条初始化数据
   */
  public List<Message> getSquareMessage(int count) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" mes.id messageId");
    sql.append(" FROM message mes");
    sql.append(" WHERE");
    sql.append(" mes.is_square = 1 AND mes.transcode=1 AND mes.is_del =0  ORDER BY mes.message_time DESC LIMIT ?");
    logger.info(sql.toString());
    List<Message> list = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper(Message.class),count);
    return CollectionUtils.isEmpty(list) ? Lists.newArrayList() : list;
  }

  /**
   * 关注的人的俱乐部
   */
  public String getWatcherNewMessageId(String userId) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" mes.id messageId");
    sql.append(" FROM message mes");
    sql.append(" WHERE");
    sql.append("  mes.user_id IN (" + userId + ") AND mes.transcode=1 AND  mes.is_del!=1 order by message_time DESC limit 1");
    logger.info(sql.toString());
    String result = jdbcTemplate.queryForObject(sql.toString(), String.class);
    return result;

  }

    /**
     * 获取最新新增的广场动态id
     *
     * @param optDateStr
     * @return
     */
    public List<Map<String,Object>> getNewSquareMsgId(String optDateStr) {
        StringBuilder sql = new StringBuilder();
        sql.append("select id messageId,message_time time from message where is_square=1 and is_del=0 and message_time >= ? and transcode=1 order by message_time desc");
        logger.info(sql.toString());
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql.toString(),optDateStr);
        return CollectionUtils.isEmpty(list) ? Lists.newArrayList() : list;
    }

    /**
     * 获取最新删除的广场动态id
     *
     * @param optDateStr
     * @return
     */
    public List<Map<String,Object>> getDeletedSquareMsgId(String optDateStr) {
        StringBuilder sql = new StringBuilder();
        sql.append("select id messageId,update_time time from message where is_square=1 and is_del=1 and update_time >= ? order by update_time desc");
        logger.info(sql.toString());
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql.toString(),optDateStr);
        return CollectionUtils.isEmpty(list) ? Lists.newArrayList() : list;
    }

    /**
     * 获取最新评论数、点赞数变化的广场动态id
     *
     * @param optDateStr
     * @return
     */
    public List<Map<String,Object>> getNewChangeSquareMsgId(String optDateStr) {
        StringBuilder sql = new StringBuilder();
        sql.append("select id messageId,update_time time from message where is_square=1 and is_del=0 and update_time >= ? order by update_time desc");
        logger.info(sql.toString());
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql.toString(),optDateStr);
        return CollectionUtils.isEmpty(list) ? Lists.newArrayList() : list;
    }

    public List<Map<String,Object>> getMessageOfPraise(String messageId) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT l.user_id userId from praise_log l where l.message_id = ? ");
        List<Map<String,Object>> list = jdbcTemplate.queryForList(sql.toString(),messageId);
        return CollectionUtils.isEmpty(list) ? Lists.newArrayList() : list;
    }
  /**
   * 从数据库获取当前用户，今天共发布动态条数
   *
   * @param userId
   * @return int
   * @throws Exception
   * @description （用一句话描述该方法的适条件、执行流程、适用方法、注意事 - 可选）
   * @author wangchaochao
   * @cre2017-03-14316:466:46
   */
  public int getMessageTotalByUserId(String userId, String startTime, String endTime) throws Exception {
    Integer count = jdbcTemplate.queryForObject("select count(1) from message where user_id=? and message_time BETWEEN ? and ? ", new Object[] { userId, startTime, endTime }, Integer.class);
    return count == null ? 0 : count;
  }

  /**
   * 获取时间段内被后台删除/修改的动态信息
   * @param beginTime
   * @param endTime
   * @return
     */
  public List<Message> getWebOperateMessage(String beginTime, String endTime) {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" mes.message_info messageInfo,mes.user_id userId,mes.path_trans path,mes.length,mes.is_top topStatus,mes.back_img backImg,");
    sql.append(" comments_count commentCount,present_count presentCount,");
    sql.append(" praise_count praiseCount ,ewaizan,mes.id messageId, mes.message_type messageType,");
    sql.append(" mes.message_time messageTime,mes.times time,from_type fromType,is_del as isDel ");
    sql.append(" FROM message mes where ");
    sql.append("update_time >=? and  update_time<?  AND operate_source=2 ");
    return jdbcTemplate.query(sql.toString(), new Object[] {beginTime,endTime}, new BeanPropertyRowMapper<Message>(Message.class));
  }

  /**
   * 通过jobId 查询动态
   *
   * @param jobId
   * @return a
   * @throws a
   * @description （用一句话描述该方法的适用条件执行流程、适用方法、注意事项- 可选）
   * @author wangchao
   * @create 2017-03-22 16:04
   */
  public Message getMessageInfoByJobId(String jobId) {
    List<Message> list = jdbcTemplate.query("select id messageId from message where job_id=?", new Object[] { jobId }, new BeanPropertyRowMapper(Message.class));
    return CollectionUtils.isEmpty(list) ? null : list.get(0);
  }

  /**
   * 获取所有俱乐部动态的ID
   */
  public List<Message> getAllMessage() {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT");
    sql.append(" mes.id messageId");
    sql.append(" FROM message mes");
    sql.append(" WHERE");
    sql.append(" mes.transcode=1 AND mes.is_del =0 ");
    logger.info(sql.toString());
    List<Message> list = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper(Message.class));
    return CollectionUtils.isEmpty(list) ? Lists.newArrayList() : list;
  }
}
