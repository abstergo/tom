package com.hefan.club.dynamic.service;

import com.hefan.club.dynamic.bean.DynamicAlbum;
import com.hefan.club.dynamic.dao.DynamicAlbumDao;
import com.hefan.club.dynamic.itf.DynamicAlbumService;
import com.hefan.club.dynamic.itf.cache.RedisDynamicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component("dynamicAlbumService")
public class DynamicAlbumServiceImpl implements DynamicAlbumService {

	@Autowired
	private DynamicAlbumDao dynamicAlbumDao;

	@Resource
	private RedisDynamicService redisDynamicService;

	@Override
	public int deleteAlbumByParams(DynamicAlbum record) throws Exception {
		//判断相册是否属于用户
		if (Integer.valueOf(dynamicAlbumDao.checkPhoto(record).get("count").toString()) == 0) {
			return 4;
		}

		redisDynamicService.delAlbumCache(record.getMsgId());
		dynamicAlbumDao.deleteAlbumByParams(record);

		return 1;
	}

	@Override
	public int checkLast(String messageId) {
		if (Integer.valueOf(dynamicAlbumDao.checkLast(messageId).get("count").toString()) > 0) {
			return 1;
		} else {
			return 0;
		}
	}

}
