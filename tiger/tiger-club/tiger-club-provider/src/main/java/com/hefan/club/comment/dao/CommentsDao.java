package com.hefan.club.comment.dao;

import com.cat.common.entity.Page;
import com.google.common.collect.Lists;
import com.hefan.club.comment.bean.Comments;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Repository
public class CommentsDao {
  @Resource
  private JdbcTemplate jdbcTemplate;

  public Comments getCommentsByIdForCheck(long commentsId) {
    String sql = "select * from comments where id=? and comments_infostatus=0";
    List<Comments> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<Comments>(Comments.class), commentsId);
    return CollectionUtils.isEmpty(list) ? null : list.get(0);
  }

  public Page<Map<String, Object>> getComment(int messageId, String userId, int isPresent, Page<Map<String, Object>> page){
    StringBuffer querySql = new StringBuffer(200);
    querySql.append("select a.id,a.user_id,a.comments_info,a.present_id ,a.comments_time,a.muser_id,a.parent_comments_id,a.message_id,a.comments_infostatus from comments a ");
    List<Object> params = Lists.newArrayList();
    StringBuffer whereSql = new StringBuffer(200);
    whereSql.append(" where ifnull(a.comments_infostatus,0)<>1 ");
    if (messageId > 0) {
      whereSql.append(" and a.message_id=? ");
      params.add(messageId);
    }
    if (StringUtils.isNotBlank(userId)) {
      whereSql.append(" and if(IFNULL(a.parent_comments_id,0)<=0,a.muser_id,a.parent_user_id)=?");
      params.add(userId);
    }
    if (isPresent == 1) {
      whereSql.append(" and IFNULL(a.present_id,0)<=0");
    } else if (isPresent == 2) {
      whereSql.append(" and IFNULL(a.present_id,0)>0");
    }
    querySql.append(whereSql);
    querySql.append(" ORDER BY a.comments_time desc limit ?,?");
    String countSql = "select IFNULL(COUNT(1),0) count from comments a " + whereSql.toString();
    page.setTotalItems(jdbcTemplate.queryForObject(countSql, params.toArray(), Long.class));
    params.add(page.getOffset());
    params.add(page.getPageSize());
    page.setResult(jdbcTemplate.queryForList(querySql.toString(), params.toArray()));
    return page;
  }

  public List<Map<String, Object>> getParentCommentsByIds(List<Integer> ids) {
    StringBuffer querySql = new StringBuffer(200);
    querySql.append("select a.id,a.user_id,a.comments_info,a.comments_infostatus,a.present_id,a.comments_time,a.message_id,a.parent_comments_id").append(" from comments a  ");
    if (!CollectionUtils.isEmpty(ids)) {
      querySql.append(" where a.id in (");
      for (int i = 0; i < ids.size(); i++) {
        int id = ids.get(i) == null ? 0 : ids.get(i);
        querySql.append(id);
        if (i < ids.size() - 1) {
          querySql.append(",");
        }
      }
      querySql.append(")");
    }
    return jdbcTemplate.queryForList(querySql.toString());
  }

  public int saveCommentsInfo(Comments com) throws Exception {
    String insertSql = "insert into comments(user_id,muser_id,message_id,parent_comments_id,parent_user_id,comments_info,comments_infostatus,comments_time,present_id) VALUES(?,?,?,?,?,?,?,?,?)";
    //参数数组
    Object[] paramArr = new Object[] { com.getUserId(), com.getMuserId(), com.getMessageId(), com.getParentCommentsId(), com.getParentUserId(), com.getCommentsInfo(), com.getCommentsInfostatus(),
        com.getCommentsTime(), com.getPresentId() };
    //参数类型数组
    return jdbcTemplate.update(insertSql, paramArr);
  }

  public int deleteCommentsById(long commentsId) throws Exception {
    String sql = "update comments set comments_infostatus=1 where id=?";
    return jdbcTemplate.update(sql, commentsId);
  }

  public Map<String, Object> getFristGiftCommentByUserId(String userId) throws Exception {
    String sql = "select comments_time as time,comments_info as content, present_id  from comments a where IF(IFNULL(a.parent_comments_id,0)>0,a.parent_user_id,a.muser_id)=? and IFNULL(present_id,0)>0 ORDER BY comments_time desc LIMIT 1";
    List<Map<String, Object>> list = jdbcTemplate.queryForList(sql, userId);
    if (!CollectionUtils.isEmpty(list)) {
      return list.get(0);
    } else {
      return null;
    }
  }

  public Map<String, Object> getFristCommentByUserId(String userId) throws Exception {
    String sql = "select comments_time as time,comments_info as content from comments a  where IF(IFNULL(a.parent_comments_id,0)>0,a.parent_user_id,a.muser_id)=? and IFNULL(present_id,0)<=0  ORDER BY comments_time desc LIMIT 1";
    List<Map<String, Object>> list = jdbcTemplate.queryForList(sql, userId);
    if (!CollectionUtils.isEmpty(list)) {
      return list.get(0);
    } else {
      return null;
    }
  }

  public int getCommentsSubSet(long commentsId) throws Exception {
    String sql = "select count(1) from comments where parent_comments_id=? and comments_infostatus=0";
    Integer subSetCount = this.jdbcTemplate.queryForObject(sql, Integer.class, commentsId);
    return subSetCount == null ? 0 : subSetCount;
  }
}
