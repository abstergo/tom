package com.hefan.club.dynamic.task;

import com.hefan.club.dynamic.itf.ClubDynamicTimerService;
import com.hefan.schedule.model.ScheduleExecuteRecord;
import com.hefan.schedule.model.ScheduleServer;
import com.hefan.schedule.service.IScheduleExecuteRecordService;
import com.hefan.schedule.service.IScheduleTaskDeal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 后台变更动态定时处理任务
 */
@Service("webDynamicHandlerTask")
public class WebDynamicHandlerTask implements IScheduleTaskDeal {

    private static Logger logger = LoggerFactory.getLogger(SquareTipsTask.class);

    @Resource
    private IScheduleExecuteRecordService scheduleExecuteRecordService;

    @Resource
    private ClubDynamicTimerService clubDynamicTimerService;

    @Override
    public boolean execute(ScheduleServer scheduleServer) {
        ScheduleExecuteRecord scheduleExecuteRecord = new ScheduleExecuteRecord();
        scheduleExecuteRecord.setExecuteTime(new Date());
        long startTime = System.currentTimeMillis();
        boolean rst = true;
        try {
            clubDynamicTimerService.webOperateMessageTimerInit();
        } catch (Exception e) {
            rst = false;
            logger.error("后台变更俱乐部动态，定时处理异常，异常原因", e);
        }
        long endTime = System.currentTimeMillis();
        scheduleExecuteRecord.setConsumingTime((endTime - startTime));
        scheduleExecuteRecord.setNextExecuteTime(scheduleServer.getNextRunStartTime());
        scheduleExecuteRecord.setTaskType(scheduleServer.getBaseTaskType());
        scheduleExecuteRecord.setStatus(rst ? (short) 1 : (short) 2);
        scheduleExecuteRecord.setExecuteResult("后台变更俱乐部动态定时处理");
        scheduleExecuteRecord.setHostName(scheduleServer.getHostName());
        scheduleExecuteRecord.setIp(scheduleServer.getIp());
        scheduleExecuteRecordService.addScheduleExecuteRecord(scheduleExecuteRecord);
        return true;
    }
}
