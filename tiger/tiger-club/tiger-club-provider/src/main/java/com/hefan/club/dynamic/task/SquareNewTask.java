package com.hefan.club.dynamic.task;

import com.hefan.club.dynamic.itf.SquareTimerService;
import com.hefan.schedule.model.ScheduleExecuteRecord;
import com.hefan.schedule.model.ScheduleServer;
import com.hefan.schedule.service.IScheduleExecuteRecordService;
import com.hefan.schedule.service.IScheduleTaskDeal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 获取最新新增的广场动态id 定时任务
 * Created by kevin_zhang on 13/03/2017.
 */
@Service("squareNewTask")
public class SquareNewTask implements IScheduleTaskDeal {

    private static Logger logger = LoggerFactory.getLogger(SquareTipsTask.class);

    @Resource
    private IScheduleExecuteRecordService scheduleExecuteRecordService;

    @Resource
    private SquareTimerService squareTimerService;

    /**
     * 获取最新新增的广场动态id
     *
     * @param scheduleServer
     * @return
     */
    @Override
    public boolean execute(ScheduleServer scheduleServer) {
        ScheduleExecuteRecord scheduleExecuteRecord = new ScheduleExecuteRecord();
        scheduleExecuteRecord.setExecuteTime(new Date());
        long startTime = System.currentTimeMillis();
        boolean rst = true;
        try {
            squareTimerService.getNewSquareMsgId();
        } catch (Exception e) {
            rst = false;
            logger.error("获取最新新增的广场动态id 定时任务 异常", e);
        }
        long endTime = System.currentTimeMillis();
        scheduleExecuteRecord.setConsumingTime((endTime - startTime));
        scheduleExecuteRecord.setNextExecuteTime(scheduleServer.getNextRunStartTime());
        scheduleExecuteRecord.setTaskType(scheduleServer.getBaseTaskType());
        scheduleExecuteRecord.setStatus(rst ? (short) 1 : (short) 2);
        scheduleExecuteRecord.setExecuteResult("获取最新新增的广场动态id 定时任务");
        scheduleExecuteRecord.setHostName(scheduleServer.getHostName());
        scheduleExecuteRecord.setIp(scheduleServer.getIp());
        scheduleExecuteRecordService.addScheduleExecuteRecord(scheduleExecuteRecord);
        return true;
    }
}
