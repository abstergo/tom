package com.hefan.club.util;

import com.alibaba.fastjson.JSONObject;
import com.hefan.club.configCenter.ClubConfigCenter;
import com.hefan.common.constant.SystemConstant;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Title.<br>
 * Description.视频转码工具类
 * <p>
 * Copyright: Copyright (c) 2016年9月26日 下午2:09:37
 * <p>
 * Company: 北京传奇创世信息技术有限公司
 * <p>
 * 
 * @author zhaomingxing
 * @version 1.0
 */
@Component("videoTranscodeUtil")
public class VideoTranscodeUtil {

	@Resource
	private  ClubConfigCenter clubConfigCenter;

	private  final Logger log = LoggerFactory.getLogger(VideoTranscodeUtil.class);

/**
	 * 水印模板ID
	 */
	//private static final String WATERMARKTEMPLATEID = DynamicProperties.getString("videoTranscode.WATERMARKTEMPLATEID");

	/**
	 * 转码模板ID
	 */
	//private static final String TRANSCODETEMPLATEID = DynamicProperties.getString("videoTranscode.TRANSCODETEMPLATEID");

	/**
	 * 转码url
	 */
	//public static final String TranscodeURL = DynamicProperties.getString("videoTranscode.TranscodeURL");

	/**
	 * 水印所在的OSS对应bucket下的存放路径
	 */
	//public static final String WATERMARK = DynamicProperties.getString("videoTranscode.WATERMARK");
	
	/**
	 * 水印所在的OSS对应bucket
	 */
	//public static final String WATERMARKBUCKET = DynamicProperties.getString("videoTranscode.WATERMARKBUCKET");


	/**
	 * 功能：
	 * 
	 * @param bucket
	 * @param videoUrl
	 *        视频url
	 * @param waterMarkImgUrl
	 *        水印
	 * @return 2016年9月26日下午3:16:07 zhaomingxing
	 */
	public  String transcode(String bucket, String videoUrl) {
		try {
			Map<String,String > config=clubConfigCenter.getPublicConfig();
			log.info("转码--Start");
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("waterMarkTemplateId", config.get("videoTranscode.WATERMARKTEMPLATEID"));
			map.put("transcodeTemplateId",config.get("videoTranscode.TRANSCODETEMPLATEID"));
			Map<String, Object> video = new HashMap<String, Object>();
			video.put("bucket", bucket);
			video.put("object",	videoUrl);
			Map<String, Object> waterMark = new HashMap<String, Object>();
			waterMark.put("bucket", config.get("videoTranscode.WATERMARKBUCKET"));
			waterMark.put("object", URLEncoder.encode(config.get("videoTranscode.WATERMARK"), SystemConstant.UTF8));
			map.put("video", video);
			map.put("waterMark", waterMark);
			log.info(JSONObject.toJSONString(map));
			String res = base(JSONObject.toJSONString(map));
			log.info("转码--res=" + res);
			log.info("转码--End");
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("转码异常", e);
			return null;
		}
	}

	private  String base(String data) throws Exception  {
		String responseText = null;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		Map<String,String> config=clubConfigCenter.getPublicConfig();
		HttpPost httpPost = new HttpPost(config.get("videoTranscode.TranscodeURL"));
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("data", data));
		httpPost.setEntity(new UrlEncodedFormEntity(nvps, SystemConstant.UTF8));
		// 执行请求
		HttpResponse response = httpClient.execute(httpPost);
		responseText = EntityUtils.toString(response.getEntity(), SystemConstant.UTF8);
		httpClient.close();
		return responseText;
	}
}
