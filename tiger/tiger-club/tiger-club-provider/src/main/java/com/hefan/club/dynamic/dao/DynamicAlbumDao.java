package com.hefan.club.dynamic.dao;

import com.alibaba.fastjson.JSONObject;
import com.cat.common.entity.Page;
import com.hefan.club.dynamic.bean.DynamicAlbum;
import com.hefan.common.exception.DataIllegalException;
import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.common.orm.domain.BaseEntity;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class DynamicAlbumDao extends BaseDaoImpl<BaseEntity> {

	@Resource
	JdbcTemplate jdbcTemplate;

	public void deleteAlbumByParams(DynamicAlbum record) throws Exception{
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE");
		sql.append(" message_album");
		sql.append(" SET ");
		sql.append(" is_del= 1");
		sql.append(" WHERE msg_id= ? ");
		if (!StringUtils.isBlank(record.getPicId())) {
			sql.append(" AND id in (" + record.getPicId() + ")");
		}
		int result =jdbcTemplate.update(sql.toString(),record.getMsgId());

		if (result <= 0) {
			throw new DataIllegalException(String.format("删除相册msgId=%s失败，事务回滚", record.getMsgId()));
		}
		jdbcTemplate.update(sql.toString(),record.getMsgId());
	}

	public Map checkPhoto(DynamicAlbum record) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" count(1) count");
		sql.append(" from  ");
		sql.append(" message_album");
		sql.append(" WHERE user_id= ? and msg_id= ? ");
		return jdbcTemplate.queryForMap(sql.toString(), record.getUserId(), record.getMsgId());
	}

	/**
	 * 保存
	 * 
	 * @param dynamicAlbum
	 * @return
	 */
	public int saveObj(DynamicAlbum dynamicAlbum) throws Exception{
		String sql = " insert into  message_album (user_id,msg_id,path,message_info,create_time,create_user,is_del,is_sync) values(?,?,?,?,?,?,?,?) ";
		int result= jdbcTemplate.update(sql,  new Object[] { dynamicAlbum.getUserId(), dynamicAlbum.getMsgId(), dynamicAlbum.getPath(),
				dynamicAlbum.getMessageInfo(), new Date(), dynamicAlbum.getCreateUser(), 0, dynamicAlbum.getIsSync() });
		if (result <= 0) {
			throw new DataIllegalException(String.format("保存相册内容%s失败，事务回滚", JSONObject.toJSONString(dynamicAlbum)));
		}
		return result;
	}

	public Map checkLast(String messageId) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" count(1) count ");
		sql.append(" FROM ");
		sql.append(" message_album ");
		sql.append(" where msg_id=? and is_del=0");
		return jdbcTemplate.queryForMap(sql.toString(),messageId);
	}

	/**
	 * 动态相册
	 *
	 * @param messageId
	 * @return
	 */
	public List<DynamicAlbum> photoListForMessage(String messageId) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT");
		sql.append(" id,msg_id msgId,YEAR(create_time) yea,MONTH(create_time) mon,message_info messageInfo,path ");
		sql.append(" FROM message_album where is_del=0");
		sql.append(" AND msg_id =?");
		return jdbcTemplate.query(sql.toString(), new Object[] { messageId }, new BeanPropertyRowMapper(DynamicAlbum.class));
	}

	/**
	 * 相册
	 *
	 * @param po
	 * @param userId
	 * @param isSync
	 * @param messageId
	 * @return
	 */
	public Page<Map<String, Object>> photoList(Page po, String userId, String isSync, String messageId) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT");
		sql.append(" id,msg_id messageId,YEAR(create_time) yea,MONTH(create_time) mon,message_info words,path pictureUrls");
		sql.append(" FROM message_album where is_del=0");
		if (StringUtils.isNotBlank(userId)) {
			sql.append(" AND user_id='" + userId + "'");
		}
		if (StringUtils.isNotBlank(isSync)) {
			sql.append(" AND is_sync=" + isSync + "");
		}
		if (StringUtils.isNotBlank(messageId)) {
			sql.append(" AND msg_id IN (" + messageId + ")");
		}
		po.setOrderBy("create_time");
		po.setOrder(po.DESC);
		return this.findPageMap(po, sql.toString());
	}
}
