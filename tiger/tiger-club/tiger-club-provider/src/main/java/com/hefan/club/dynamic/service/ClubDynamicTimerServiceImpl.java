package com.hefan.club.dynamic.service;

import com.cat.tiger.service.JedisService;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.club.comment.meta.RedisKeyConstant;
import com.hefan.club.dynamic.bean.Message;
import com.hefan.club.dynamic.dao.DynamicDao;
import com.hefan.club.dynamic.itf.ClubDynamicTimerService;
import com.hefan.club.dynamic.itf.cache.RedisDynamicService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/3/20.
 */
@Component("clubDynamicTimerService")
public class ClubDynamicTimerServiceImpl implements ClubDynamicTimerService {
    private Logger logger = LoggerFactory.getLogger(ClubDynamicTimerServiceImpl.class);
    @Autowired
    private DynamicDao dynamicDao;
    @Autowired
    private JedisService jedisService;
    @Autowired
    private RedisDynamicService redisDynamicService;

    @Autowired
    public void webOperateMessageTimerInit() throws Exception {
        logger.info("webOperateMessageTimerInit---start");
        String beginTime =  this.getQueryBeginTimeFromCach();
        String endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        if(StringUtils.isBlank(beginTime)) {
            this.changeQueryBeginTimerToCach(endTime);
            logger.info("由缓存中未获取到查询的开始时间，放入当前时间作为下次执行开始时间。");
        } else {
            List<Message> mssageList = queryWebOperateMessage(beginTime, endTime);
            logger.info("后台变更的记录数：{}", mssageList.size());
            if (!CollectionUtils.isEmptyList(mssageList)) {
                for (Message message : mssageList) {
                    if (message.getIsDel() == 0) { //修改|新增操作
                        try {
                            redisDynamicService.syncMessage(message);
                        } catch (Exception e) {
                            logger.error("变更|新增动态处理失败:messageId:{},失败原因:{}", message.getMessageId(), e.getMessage());
                            e.printStackTrace();
                        }
                    } else { //修改操作
                        try {
                            redisDynamicService.delMessageCache(message);
                        } catch (Exception e) {
                            logger.error("删除动态处理失败:messageId:{},失败原因:{}", message.getMessageId(), e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                logger.info("{}—{}该时间段内容无后台操作的动态信息", beginTime, endTime);
            }
        }
        logger.info("webOperateMessageTimerInit---end");
    }


    @Override
    @Transactional(readOnly =true)
    public List<Message> queryWebOperateMessage(String beginTime, String endTime) {
        return dynamicDao.getWebOperateMessage(beginTime,endTime);
    }

    @Override
    public String getQueryBeginTimeFromCach() {
        try {
            return jedisService.getStr(RedisKeyConstant.CLUB_DYNAMIC_WEB_TIMER_START);
        } catch (Exception e) {
            logger.info("由缓存获取后台操作动态查询开始时间异常---原因:"+e.getMessage());
        }
        return null;
    }

    @Override
    public void changeQueryBeginTimerToCach(String endTime) {
        try {
            jedisService.setStr(RedisKeyConstant.CLUB_DYNAMIC_WEB_TIMER_START,endTime);
        } catch (Exception e) {
            logger.info("向缓存保存后台操作动态查询开始时间异常---原因:"+e.getMessage());
        }
    }
}
