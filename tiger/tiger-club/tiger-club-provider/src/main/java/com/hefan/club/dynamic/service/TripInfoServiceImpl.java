package com.hefan.club.dynamic.service;

import com.cat.common.entity.Page;
import com.hefan.club.dynamic.bean.TripInfo;
import com.hefan.club.dynamic.dao.TripInfoDao;
import com.hefan.club.dynamic.itf.TripInfoService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component("tripInfoService")
public class TripInfoServiceImpl implements TripInfoService {
	
	@Resource
	TripInfoDao tripInfoDao;

	@Override
	public List findTripsTypeInfoList() {
		return this.tripInfoDao.findTripsTypeInfoList();
	}

	@Override
	public int findTripsTypeCountById(String type) {
		return this.tripInfoDao.findTripsTypeCountById(type);
	}

	@Override
	public int findTripsCountByTime(String userId, String time, String timeFormat, long tripId) {
		return this.tripInfoDao.findTripsCountByTime(userId, time, timeFormat, tripId);
	}

	@Override
	public int saveTripInfo(TripInfo tripInfo) {
		return this.tripInfoDao.saveTripInfo(tripInfo);
	}

	@Override
	public int updateTripInfo(TripInfo tripInfo) {
		return this.tripInfoDao.updateTripInfo(tripInfo);
	}

	@Override
	public int deleteTripInfo(long id) {
		return this.tripInfoDao.deleteTripInfo(id);
	}

	@Override
	public Map findTripsInfoLately(String userId, String isOpen) {
		Map map = tripInfoDao.findTripsInfoLately(userId,isOpen,0);
		if(map == null || map.isEmpty()){
			map = tripInfoDao.findTripsInfoLately(userId,isOpen,1);
		}
		return map;
	}

	@Override
	public List findTripsForInit(String userId, String isOpen, int mpageSize, String time) {
		return this.tripInfoDao.findTripsForInit(userId, isOpen, mpageSize, time);
	}

	@Override
	public List findTripsByTimeDirection(String userId, String isOpen, String tripTime, int mpageSize, String direction) {
		return this.tripInfoDao.findTripsByTimeDirection(userId, isOpen, tripTime, mpageSize, direction);
	}

	@Override
	public Page findTripsPage(String userId, String isOpen,Page page) {
		return this.tripInfoDao.findTripsPage(userId, isOpen, page);
	}

	@Override
	public List findTripsStarChart(String userId, String isOpen, String time) {
		List list = this.tripInfoDao.findTripsStarChart(userId, isOpen, time);
		if(list !=null && list.size()>0){
			int position = 0;
			for(int i=0;i<list.size();i++){
				Map<String,Object> map = (Map<String,Object>)list.get(i);
				if(map.containsKey("isLately")){
					if(map.get("isLately").toString().equals("1")){
						position = i;
						break;
					}
				}
			}
			for(int j=0;j<list.size();j++){
				Map<String,Object> map = (Map<String,Object>)list.get(j);
				if(map.containsKey("isLately")){
						map.put("isLately", position);
				}
			}
		}
		return list;
	}
}
