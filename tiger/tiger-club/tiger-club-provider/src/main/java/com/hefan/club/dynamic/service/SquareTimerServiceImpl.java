package com.hefan.club.dynamic.service;

import com.alibaba.fastjson.JSON;
import com.cat.tiger.service.JedisService;
import com.hefan.club.comment.meta.RedisKeyConstant;
import com.hefan.club.dynamic.dao.DynamicDao;
import com.hefan.club.dynamic.itf.SquareService;
import com.hefan.club.dynamic.itf.SquareTimerService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.*;

/**
 * Created by kevin_zhang on 10/03/2017.
 */
@Component("squareTimerService")
public class SquareTimerServiceImpl implements SquareTimerService {
    public Logger logger = LoggerFactory.getLogger(SquareTimerServiceImpl.class);

    @Resource
    DynamicDao dynamicDao;

    @Resource
    SquareService squareService;

    @Resource
    private JedisService jedisService;

    //获取最新新增的广场动态id定时操作时间戳
    private static String GETNEWSQUAREMSGID_KEY = "getnewsquaremsgid_key";
    //获取最新删除的广场动态id定时操作时间戳
    private static String GETDELETEDSQUAREMSGID_KEY = "getdeletedsquaremsgid_key";
    //获取最新评论数、点赞数变化的广场动态id定时操作时间戳
    private static String GETNEWCHANGESQUAREMSGID_KEY = "getnewchangesquaremsgid_key";

    /**
     * 获取最新新增的广场动态id
     */
    @Override
    public void getNewSquareMsgId() {
        try {
            String optDateStr = jedisService.getStr(GETNEWSQUAREMSGID_KEY);
            logger.info("获取最新新增的广场动态id 时间：{}",optDateStr);
            if (StringUtils.isBlank(optDateStr)) {
                optDateStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            }
            List<Map<String,Object>> result = new ArrayList<>();
            try {
                result = dynamicDao.getNewSquareMsgId(optDateStr);
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error("获取最新新增的广场动态id 获取数据出错", ex.getMessage());
            }

            if (null != result && result.size() > 0) {
                logger.info("获取最新新增的广场动态id 新增数据：{}",JSON.toJSONString(result));
                squareService.addSquareRedisForMessage(result);
                //自增 - 更新OSS信号量
                jedisService.incr(RedisKeyConstant.SQUARE_LATEST_KEY);
                optDateStr = result.get(0).get("time").toString();
                logger.info("生成缓存中最新新增的广场动态id 时间：{}",optDateStr);
            }else {
                logger.info("获取最新新增的广场动态id 无新增数据");
            }
            //更新下次操作时间
            jedisService.setexStr(GETNEWSQUAREMSGID_KEY, optDateStr, 24 * 60 * 60);
            logger.info("获取最新新增的广场动态id 定时任务执行成功");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("获取最新新增的广场动态id 定时任务执行失败", ex.getMessage());
        }
    }

    /**
     * 获取最新删除的广场动态id
     */
    @Override
    public void getDeletedSquareMsgId() {
        try {
            String optDateStr = jedisService.getStr(GETDELETEDSQUAREMSGID_KEY);
            logger.info("获取缓存中最新删除的广场动态id 时间：{}",optDateStr);
            if (StringUtils.isBlank(optDateStr)) {
                optDateStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                logger.info("凌晨时间：{}", optDateStr);
            }
            List<Map<String,Object>> result = new ArrayList<>();
            try {
                result = dynamicDao.getDeletedSquareMsgId(optDateStr);
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error("获取最新删除的广场动态id 获取数据出错", ex.getMessage());
            }

            if (null != result && result.size() > 0) {
                logger.info("获取最新删除的广场动态id 删除数据：{}",JSON.toJSONString(result));
                squareService.delSquareRedisForMessage(result);
                //自增 - 更新OSS信号量
                jedisService.incr(RedisKeyConstant.SQUARE_LATEST_KEY);
                optDateStr = result.get(0).get("time").toString();
                logger.info("生成缓存中最新删除的广场动态id 时间：{}",optDateStr);
            }else {
                logger.info("获取最新删除的广场动态id 无删除数据");
            }
            //更新下次操作时间
            jedisService.setexStr(GETDELETEDSQUAREMSGID_KEY, optDateStr, 24 * 60 * 60);
            logger.info("获取最新删除的广场动态id 定时任务执行成功");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("获取最新删除的广场动态id 定时任务执行失败", ex.getMessage());
        }
    }

    /**
     * 获取最新评论数、点赞数变化的广场动态id

    @Override
    public void getNewChangeSquareMsgId() {
        try {
            String optDateStr = jedisService.getStr(GETNEWCHANGESQUAREMSGID_KEY);
            logger.info("获取最新评论数、点赞数变化的广场动态id 时间：{}",optDateStr);
            if (StringUtils.isBlank(optDateStr)) {
                optDateStr = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                logger.info("凌晨时间：{}", optDateStr);
            }
            List<Map<String,Object>> result = new ArrayList<>();
            try {
                result = dynamicDao.getNewChangeSquareMsgId(optDateStr);
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error("获取最新评论数、点赞数变化的广场动态id 获取数据出错", ex.getMessage());
            }
            if (null != result && result.size() > 0) {
                logger.info("获取最新评论数、点赞数变化的广场动态id 更新数据：{}",JSON.toJSONString(result));
                squareService.updateSquareRedisForMessage(result);
                optDateStr = result.get(0).get("time").toString();
                logger.info("生成缓存中最新评论数、点赞数变化的广场动态id 时间：{}",optDateStr);
            }else {
                logger.info("获取最新评论数、点赞数变化的广场动态id 无更新数据");
            }
            //更新下次操作时间
            jedisService.setexStr(GETNEWCHANGESQUAREMSGID_KEY, optDateStr, 24 * 60 * 60);
            logger.info("获取最新评论数、点赞数变化的广场动态id 定时任务执行成功");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("获取最新评论数、点赞数变化的广场动态id 定时任务执行失败", ex.getMessage());
        }
    }*/
}
