package com.hefan.club.dynamic.task;

import com.hefan.club.dynamic.itf.SquareService;
import com.hefan.schedule.model.ScheduleExecuteRecord;
import com.hefan.schedule.model.ScheduleServer;
import com.hefan.schedule.service.IScheduleExecuteRecordService;
import com.hefan.schedule.service.IScheduleTaskDeal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by nigle on 2017/3/13.
 * 广场小红点刷新
 */
@Service("squareRedDotTask")
public class SquareRedDotTask implements IScheduleTaskDeal {

    private static Logger logger = LoggerFactory.getLogger(SquareRedDotTask.class);

    @Resource
    private IScheduleExecuteRecordService scheduleExecuteRecordService;
    @Resource
    private SquareService squareService;

    /*
     * 刷新广场小红点静态文件 * / 30 * * * * ?
     * 每隔30秒执行一次
     * @param scheduleServer
     * @author zhaoning
     * @version 1.0.0
     * @since 2017/03/13 08:25:16
    */
    @Override
    public boolean execute(ScheduleServer scheduleServer) {
        ScheduleExecuteRecord scheduleExecuteRecord = new ScheduleExecuteRecord();
        scheduleExecuteRecord.setExecuteTime(new Date());
        long startTime = System.currentTimeMillis();
        boolean rst = true;
        try {
            squareService.squareLatest();
        } catch (Exception e) {
            rst=false;
            logger.error("刷新广场小红点任务异常", e);
        }
        long endTime = System.currentTimeMillis();
        scheduleExecuteRecord.setConsumingTime((endTime - startTime));
        scheduleExecuteRecord.setNextExecuteTime(scheduleServer.getNextRunStartTime());
        scheduleExecuteRecord.setTaskType(scheduleServer.getBaseTaskType());
        scheduleExecuteRecord.setStatus(rst ? (short) 1 : (short) 2);
        scheduleExecuteRecord.setExecuteResult("刷新广场小红点任务");
        scheduleExecuteRecord.setHostName(scheduleServer.getHostName());
        scheduleExecuteRecord.setIp(scheduleServer.getIp());
        scheduleExecuteRecordService.addScheduleExecuteRecord(scheduleExecuteRecord);
        return true;
    }
}
