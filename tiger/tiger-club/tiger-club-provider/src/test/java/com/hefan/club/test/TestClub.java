package com.hefan.club.test;

import com.alibaba.fastjson.JSONObject;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.tiger.service.JedisService;
import com.hefan.club.comment.meta.RedisKeyConstant;
import com.hefan.club.dynamic.bean.DynamicAlbum;
import com.hefan.club.dynamic.bean.Message;
import com.hefan.club.dynamic.itf.DynamicAlbumService;
import com.hefan.club.dynamic.itf.DynamicService;
import com.hefan.club.dynamic.itf.cache.RedisDynamicService;
import com.hefan.club.util.StringUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wangchao
 * @title: cfcauser
 * @package com.hefan.club.test
 * @copyright: Copyright (c) 2017
 * @date 2017-03-21 10:01
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/applicationContext.xml")
public class TestClub {
  

  @Resource
  private DynamicService dynamicService;

  @Resource
  protected DynamicAlbumService dynamicAlbumService;

  @Resource
  private RedisDynamicService redisDynamicService;

  @Resource
  private JedisService jedisService;

  /**
   * 首页
   */

  @Test
  public void index() throws Exception {
    Page page = new Page();
    page.setPageNo(1);
    page.setPageSize(20);
    String userId = "1100339";
    Page<Message> messages = dynamicService.listMessage(page, userId,"1100339");
    System.out.println(JSONObject.toJSON(messages.getResult()));
    //Assert.assertTrue(messages == null);

  }

  /**
   * 首页
   */

  @Test
  public void index2() throws Exception {
    Page page = new Page();
    page.setPageNo(2);
    page.setPageSize(20);
    Page<Message> messages = dynamicService.listClubIndex(page, "1015");
    System.out.println(JSONObject.toJSON(messages.getResult()));
    //Assert.assertTrue(messages == null);

  }

  /**
   * 发布
   */

  @Test
  public void insert() throws Exception {
    Message message = new Message();
    //{"NickName":"LD明星Shirly","messageType":"1","isSync":"0","userId":"1082","fromType":"2","messageInfo":"Hhhhhh"}
    message.setNickName("8220847");
    message.setMessageType("1");
    message.setIsSync(0);
    message.setUserId("1011");
    message.setFromType("2");
    message.setMessageInfo("222222222222233333333333");
    dynamicService.insertMessage(message);

  }

  /**
   * 删除
   */

  @Test
  public void del() throws Exception {
    Message message = new Message();
    //{"NickName":"LD明星Shirly","messageType":"1","isSync":"0","userId":"1082","fromType":"2","messageInfo":"Hhhhhh"}
    message.setUserId("8220847");
    message.setMessageType("1");
    message.setMessageId(242);
    dynamicService.deleteMessage(message);

  }

  /**
   * message
   */

  @Test
  public void message() {
    Message message = redisDynamicService.getMessageInfo("229");
    Assert.assertNotNull(message);
    if (message.getMessageType().equals("3")) {
    }
    System.out.println(message);
  }

  /**
   * 删除相册
   */

  @Test
  public void dela() throws Exception {
    DynamicAlbum dynamicAlbum = new DynamicAlbum();
    dynamicAlbum.setMsgId(Integer.valueOf(229));
    dynamicAlbum.setPicId("163");
    dynamicAlbum.setUserId("8220896");
    dynamicAlbumService.deleteAlbumByParams(dynamicAlbum);
  }

  /**
   * message
   */
  @Test
  public void ablum() throws Exception {
    List<Object> list = jedisService.zrevrange(String.format(RedisKeyConstant.FEED_ALBUM, 166), 0, 1);
    System.out.println(list.get(0));
  }

  /**
   * 主播listmessage
   */

  @Test
  public void messageList() {
    Page page = new Page();
    page.setPageNo(1);
    page.setPageSize(10);
    try {
      Page<Message> list = dynamicService.listMessage(page, "8220864", "8220864");
      System.out.println(JSONObject.toJSON(list.getResult()));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * 置顶
   */

  @Test
  public void top() {
    try {
      ResultBean resultBean = dynamicService.top("8221096", "293");
      System.out.println(JSONObject.toJSON(resultBean));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  @Test
  public void admire() throws Exception {
   /* int id=dynamicService.admire("8221096","305");
    System.out.println(id);*/
    String st="http://img1.hefantv.com/20170327/386ead57c4f14b6aaf382025f032ceae.jpg,http://img1.hefantv.com/20170327/63570f8aa3f94ad3ac893a1869fa98ec.jpg,http://img1.hefantv.com/20170327/aa0a8ee7125f4ef1a9235c45d7014d3e.jpg";

    List<String> paths = StringUtil.splitToList(",", st);
    System.out.println(paths);
    //Assert.assertTrue(messages == null);

    dynamicService.getCacheMessageInfo("313", "8220864");
  }

}

