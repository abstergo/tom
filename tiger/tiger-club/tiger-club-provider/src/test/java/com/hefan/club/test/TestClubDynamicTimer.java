package com.hefan.club.test;

import com.hefan.club.dynamic.itf.ClubDynamicTimerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/3/22.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/applicationContext.xml")
public class TestClubDynamicTimer {

    @Resource
    private ClubDynamicTimerService clubDynamicTimerService;

    @Test
    public void ClubDynamicTimerTaskTest() {
        try {
            clubDynamicTimerService.webOperateMessageTimerInit();
            System.out.println("后台变更俱乐部动态，定时处理完成");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("后台变更俱乐部动态，定时处理异常，异常原因");
        }
    }
}
