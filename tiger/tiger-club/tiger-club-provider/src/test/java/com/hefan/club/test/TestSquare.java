/**
 * @title: TestAssetOutBiz.java
 * @package com.jianlc.asset.biz
 * @description: 简理财
 * @copyright: Copyright (c) 2016
 * @company:北京简便快乐信息技术有限公司
 * @author ZLH
 * @date 2016年9月20日 下午8:46:59
 */
package com.hefan.club.test;

import com.cat.tiger.service.JedisService;
import com.hefan.club.comment.meta.RedisKeyConstant;
import com.hefan.club.dynamic.bean.Message;
import com.hefan.club.dynamic.dao.DynamicDao;
import com.hefan.club.dynamic.itf.DynamicService;
import com.hefan.club.dynamic.itf.SquareService;
import com.hefan.club.dynamic.itf.SquareTimerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:spring/applicationContext.xml")
public class TestSquare {

  @Resource
  private JdbcTemplate jdbcTemplate;
  @Resource
  private JedisService jedisService;
  @Resource
  private SquareService squareService;
  @Resource
  private DynamicDao dynamicDao;
  @Resource
  private DynamicService dynamicService;
  @Resource
  private SquareTimerService squareTimerService;

//  @Resource
//  private SquareUserCheckService squareUserCheckService;

  final String SQUARE_SORTED_KEY = "square_sorted";
  final String SQUARE_HASHSET_KEY = "square_hashset";

  @Test
  public void testTimer() {
    try {
      String key = String.format(RedisKeyConstant.PRAISE_SORTED_KEY, String.valueOf(2655));
      System.out.println(key);
      Double b = jedisService.zscore(key, "1022");
      System.out.println(b);
      System.out.println(b>0);
      b = jedisService.zscore(key,1022);
      System.out.println(b);
//      String userId="1100059";
//      int msgId=2625;
//      boolean flag=dynamicService.followRedDot(userId,msgId);
//      System.out.println(flag);
//      squareTimerService.getDeletedSquareMsgId();
//
//      squareTimerService.getNewChangeSquareMsgId();

//      squareTimerService.getNewSquareMsgId();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testInint(){
    try {
      System.out.println(jedisService.zcard(RedisKeyConstant.SQUARE_SORTED_KEY));
      squareService.initSquareList(1000);
      System.out.println(jedisService.zcard(RedisKeyConstant.SQUARE_SORTED_KEY));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void squareLatest(){
    squareService.squareLatest();
  }
  @Test
  public void testGetDataByMsgId() {
    try {
//      boolean f=squareUserCheckService.checkSquareUserId("111");
//      System.out.print(f);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testSql(){
    try {
      List<Message> list = dynamicDao.getSquareMsgBeforMsgIdByUserId("1278","235");
      System.out.println(list.size());
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  @Test
  public void testSorted() {
    try {
      jedisService.del(SQUARE_SORTED_KEY);
      System.out.println(jedisService.zadd(SQUARE_SORTED_KEY, 111, 111));
      System.out.println(jedisService.zadd(SQUARE_SORTED_KEY, 222, 222));
      System.out.println(jedisService.zadd(SQUARE_SORTED_KEY, 333, 333));
      System.out.println(jedisService.zadd(SQUARE_SORTED_KEY, 444, 444));
      System.out.println(jedisService.zadd(SQUARE_SORTED_KEY, 555, "555"));
      System.out.println(jedisService.zadd(SQUARE_SORTED_KEY, 666, 666));
      System.out.println(jedisService.zadd(SQUARE_SORTED_KEY, 777, 777));
      System.out.println(jedisService.zadd(SQUARE_SORTED_KEY, 888, 888));
      System.out.println(jedisService.zadd(SQUARE_SORTED_KEY, 999, 999));
//      System.out.println("reset::"+jedisService.zadd(SQUARE_SORTED_KEY, 555, 555));
//      System.out.println("reset::"+jedisService.zadd(SQUARE_SORTED_KEY, 666, 555));
//      System.out.println("zrem::"+jedisService.zrem(SQUARE_SORTED_KEY,555));
      //count
      System.out.println(jedisService.zscore(SQUARE_SORTED_KEY,"555"));
      System.out.println(jedisService.zcard(SQUARE_SORTED_KEY));
      //rem
//      System.out.println(jedisService.zrevrange(SQUARE_SORTED_KEY, 0, -1));
//      System.out.println(jedisService.zremrangeByRank(SQUARE_SORTED_KEY,0,2));
//      System.out.println(jedisService.zrevrange(SQUARE_SORTED_KEY, 0, -1));
      //index
//      System.out.println(jedisService.zrank(SQUARE_SORTED_KEY, 168));
//      System.out.println(jedisService.zrank(SQUARE_SORTED_KEY, 333));
//      System.out.println("revrank:"+jedisService.zrevrank(SQUARE_SORTED_KEY, 444));
      //list
//      System.out.println(jedisService.zrevrange(SQUARE_SORTED_KEY, 0, 3));
//      System.out.println(jedisService.zrevrange(SQUARE_SORTED_KEY, 6, 6+20));
//      int index = jedisService.zrevrank(SQUARE_SORTED_KEY,333).intValue();
//      System.out.println("index:" + index);
//      System.out.println(jedisService.zrevrange(SQUARE_SORTED_KEY, index + 1, index + 3));
//      List list = jedisService.zrevrange(SQUARE_SORTED_KEY, 7, 7+20);
//      for (Object o : list) {
//        System.out.println(o.toString());
//        String s = String.valueOf(o);
//        System.out.println(s);
//      }

      jedisService.del(SQUARE_SORTED_KEY);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /*@Test
  public void testHashset() {
    try {
      Message mes = squareService.getDataByMsgId("168");
      //set
      System.out.println(jedisService.hset(SQUARE_HASHSET_KEY, String.valueOf(mes.getMessageId()), JSON.toJSONString(mes)));
      //count
      System.out.println(jedisService.hlen(SQUARE_HASHSET_KEY));
      //get
      String str = jedisService.hgetStr(SQUARE_HASHSET_KEY,"168");
      System.out.println(str);
      Message mes_ = JSONObject.parseObject(str,Message.class);
      //reset
      System.out.println(jedisService.hset(SQUARE_HASHSET_KEY, String.valueOf(mes.getMessageId()), JSON.toJSONString(mes)));
      str = jedisService.hgetStr(SQUARE_HASHSET_KEY,"168");
      System.out.println(str);
      //del
      System.out.println(jedisService.hdel(SQUARE_HASHSET_KEY,"168"));
      System.out.println(jedisService.hlen(SQUARE_HASHSET_KEY));
      jedisService.del(SQUARE_SORTED_KEY);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }*/
}
