package com.hefan.club.dynamic.itf;

import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.hefan.club.comment.bean.MessageResVo;
import com.hefan.club.dynamic.bean.Message;
import com.hefan.live.bean.LiveDynamicVo;
import com.hefan.user.bean.WebUser;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

public interface DynamicService {

  /**
   * 删除动态
   *
   * @return
   */
  ResultBean deleteMessage(Message mes) throws Exception;

  /**
   * 查询关注人的微博
   *
   * @param po
   * @return
   */
  Page<Message> listClubIndex(Page po, String userId);

  /**
   * 查询主播所有微博
   *
   * @param po
   * @return
   */
  Page<Message> listMessage(Page po, String userId, String authorId) throws Exception;

  /**
   * 置顶
   *
   * @return
   */
  ResultBean top(String userId, String messageId);

  /**
   * 取消置顶
   *
   * @return
   */
  int unTop(String userId, String messageId) throws Exception;

  /**
   * 查询图片
   *
   * @param po
   * @param isSync
   * @param userId
   * @return
   */
  Page<Map<String, Object>> photoList(Page po, String isSync, String messageId, String userId);

  /**
   * 查询所有视频
   *
   * @param po
   * @param isSync
   * @param authorId
   * @param userId
   * @return
   */
  Page<Message> videoList(Page<Map<String, Object>> po, String isSync, String authorId, String userId) throws ParseException;

  /**
   * 加动态附加数（type：1：:评论数2：礼物数3：赞数），同时更新缓存数据
   *
   * @param messageId
   * @param type
   * @return
   */

  void addMessageCount(String messageId, String userId, String type) throws Exception;

  /**
   * 粉丝排行榜
   *
   * @param userId
   * @return
   */
  public List listFans(String userId);

  /**
   * 赞
   *
   * @param userId
   * @param messageId
   * @return
   */
  public int admire(String userId, String messageId) throws Exception;

  /**
   * 添加动态
   *
   * @param message
   * @return
   */
  @SuppressWarnings("rawtypes")
  ResultBean insertMessage(Message message) throws Exception;

  /**
   * 视频回调
   *
   * @param jobId
   * @param pathTrans
   * @return
   */
  void videoCallback(String jobId, String pathTrans) throws Exception;

  /**
   * 用户首部信息
   *
   * @param userId
   * @return
   */
  WebUser getDynaUser(String userId);

  /**
   * 消息Id 获取正文详情
   *
   * @param messageId
   * @return
   */
  Message getCacheMessageInfo(String messageId, String userId);

  List recommendMesStarClubFor(String userId);

  /**
   * 减礼物数、评论数,赞数,同时刷新缓存
   *
   * @param messageId
   * @param type
   * @return
   */
  void reMessageCount(String messageId, String userId, String type);

  /**
   * 明星自动分享动态
   *
   * @param liveDynamicVo
   * @return
   */
  int shareLive(LiveDynamicVo liveDynamicVo);

  /**
   * 查看直播是否被分享过动态 false:未分享过
   *
   * @return
   */
  boolean isShareToDynamic(String userId, String liveUuid);

  /**
   * 根据动态id获取动态信息
   *
   * @throws @date: 2016年9月27日 下午3:58:10
   * @Title: getMessageResByIds
   * @Description: TODO(这里用一句话描述这个方法的作用)
   * @param: @param
   * ids
   * @param: @return
   * @return: Map
   * @author: LiTeng
   */
  Map<Integer, MessageResVo> getMessageResByIds(List<Integer> ids);

  /**
   * 获取动态评论/礼物/点赞数
   *
   * @param messageId
   * @return
   * @throws @date: 2016年9月30日 上午11:27:32
   * @Title: getMessageInfoById
   * @Description: TODO(这里用一句话描述这个方法的作用)
   * @return: Map
   * @author: LiTeng
   */
  Map getMessageInfoById(long messageId);

  /**
   * 获取关注小红点
   */
  boolean followRedDot(String userId, int messageId);

  /**
   * 查询普通用户今日已发送条数
   *
   * @param userId
   * @return boolean
   * @throws a
   * @description （用一句话描述该方法的适用条件执行流程、适用方法、注意事项- 可选）
   * @author wangchao
   * @create 2017-03-23 13:53
   */
  boolean isSendMessage(String userId) throws Exception;
}
