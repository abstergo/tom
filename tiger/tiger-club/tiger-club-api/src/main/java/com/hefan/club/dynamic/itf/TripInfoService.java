package com.hefan.club.dynamic.itf;

import java.util.List;
import java.util.Map;

import com.cat.common.entity.Page;
import com.hefan.club.dynamic.bean.TripInfo;

public interface TripInfoService {
	
	/**
	 * 获取行程类型列表
	 * @Title: findTripsTypeInfoList   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @return      
	 * @return: List
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月15日 下午4:34:05
	 */
	List findTripsTypeInfoList();
	
	/**
	 * 判断行程类型是否存在
	 * @Title: findTripsTypeCountById   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param type
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月15日 下午4:37:49
	 */
	int findTripsTypeCountById(String type);
	
	/**
	 * 获取用户某一天行程数量
	 * @Title: findTripsCountByTime   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @param time
	 * @param timeFormat
	 * @param tripId
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月15日 下午4:45:44
	 */
	int findTripsCountByTime(String userId,String time,String timeFormat,long tripId);
	
	/**
	 * 添加个人行程
	 * @Title: saveTripInfo   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param tripInfo
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月15日 下午5:09:57
	 */
	int saveTripInfo(TripInfo tripInfo);
	
	/**
	 * 修改个人行程
	 * @Title: updateTripInfo   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param tripInfo
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月15日 下午5:14:21
	 */
	int updateTripInfo(TripInfo tripInfo);
	
	/**
	 * 删除个人行程
	 * @Title: deleteTripInfo   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param id
	 * @return      
	 * @return: long
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月15日 下午5:17:14
	 */
	int deleteTripInfo(long id);
	
	/**
	 * 获取最近一条记录（未来第一条，如果没有，就去过去） 
	 * @Title: findTripsInfoLately   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @param isOpen
	 * @return      
	 * @return: Map
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月17日 上午10:34:35
	 */
	Map findTripsInfoLately(String userId,String isOpen);
	
	/**
	 * 行程揭秘首次加载
	 * @Title: findTripsForInit   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @param isOpen
	 * @param mpageSize
	 * @param time
	 * @return      
	 * @return: List
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月17日 上午10:35:21
	 */
	List findTripsForInit(String userId,String isOpen,int mpageSize,String time);
	
	/**
	 * 根据时间查询行程揭秘
	 * @Title: findTripsByTimeDirection   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param userId
	 * @param: @param isOpen 是否查询公开  1-查看所有  2-只查看公开
	 * @param: @return      
	 * @return: List
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月22日 下午2:51:27
	 */
	List findTripsByTimeDirection(String userId,String isOpen,String tripTime,int mpageSize,String direction);
	
	/**
	 * 分页获取个人行程列表
	 * @Title: findTripsPage   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @param isOpen
	 * @return      
	 * @return: Page
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月17日 上午10:54:50
	 */
	Page findTripsPage(String userId,String isOpen,Page page);
	
	/**
	 * 获取个人行程数据（星空图第一版用，之后作废）
	 * @Title: findTripsStarChart   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @param isOpen
	 * @param time
	 * @return      
	 * @return: List
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月26日 下午2:35:37
	 */
	List findTripsStarChart(String userId, String isOpen,String time);

}
