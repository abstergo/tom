package com.hefan.club.dynamic.itf;

import com.hefan.club.dynamic.bean.Message;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/3/20.
 */
public interface ClubDynamicTimerService {

    /**
     * 后台动态变更处理定时方法
     * @throws Exception
     */
    public void webOperateMessageTimerInit() throws Exception;

    /**
     * 获取时间段内后台变更动态记录
     * @param beginTime
     * @param endTime
     * @return
     */
    public List<Message> queryWebOperateMessage(String beginTime, String endTime);


    /**
     * 获取后台操作动态的查询开始时间
     * @return
     */
    public String getQueryBeginTimeFromCach();

    /**
     * 保存后台操作动态查询开始时间，即本次结束时间
     * @param endTime
     * @return
     * @throws Exception
     */
    public void changeQueryBeginTimerToCach(String endTime);
}
