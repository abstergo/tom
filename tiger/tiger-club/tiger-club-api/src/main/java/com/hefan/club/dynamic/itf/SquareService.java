package com.hefan.club.dynamic.itf;

import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.hefan.club.dynamic.bean.Message;

import java.util.List;
import java.util.Map;

/**
 * Created by nigle on 2017/3/4.
 */
public interface SquareService {

    /**
     * 获取广场下页信息
     */
    Page<Message> nextPageSquare(String minMessageId, String userId, Page po);

    /**
     * 刷新广场页数据
     */
    Page<Message> flushSquare(String maxMessageId, String userId, Page po);

    /**
     * 获取单条数据

    Message getDataByMsgId(String messageId);
     */
    /**
     * 缓存动态信息和排序ID
     */
    void setMessageToRdis(String messageId);

    /**
     * 删除动态信息和排序ID
     */
    void delMessageToRdis(String messageId);

    /**
     * 缓存动态信息

    void setDataToRedis(String messageId);
     */
    /**
     * 缓存排序动态ID
     */
    void setSortedToRedis(String messageId);

    /**
     * 缓存用户点赞动态信息
     */
    Long setPraiseToRedis(String userId, String messageId);

    /**
     * 初始化广场1000条动态
     */
    ResultBean initSquareList(int count);

    /**
     * 刷新广场小红点
     */
    void squareLatest();

    /**
     * 动态变更时更新缓存
     * @param editMessageIdList 信息变化

    @Deprecated
    void updateSquareRedisForMessage(List<Map<String,Object>> editMessageIdList);
     */
    /**
     * 动态变更时更新缓存
     * @param addMessageIdList 新增操作
     */
    void addSquareRedisForMessage(List<Map<String,Object>> addMessageIdList);
    /**
     * 动态变更时更新缓存
     * @param delMessageIdList 删除操作
     */
    void delSquareRedisForMessage(List<Map<String,Object>> delMessageIdList);

    /**
     * 初始化点赞数据
     * @param messageId
     */
    void initPraiseForMessage(String messageId);

    /**
     * 初始化动态的点赞数据(用于检查动态点赞状态)
     * @return
     */
    ResultBean initPraiseForMessageCache();
}
