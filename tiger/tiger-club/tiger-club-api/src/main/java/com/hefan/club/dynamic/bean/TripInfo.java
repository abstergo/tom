package com.hefan.club.dynamic.bean;

import java.io.Serializable;
import java.util.Date;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;

@Entity(tableName = "trip_info")
public class TripInfo implements Serializable{
	/**
     * 评论ID
     * @author: LiTeng
     */
    private Long id;
    
    @Column("user_id")
    private String userId;

    @Column("img_url")
    private String imgUrl;

    private String title;

    private String content;

    private String type;

    private Date time;

    @Column("activity_id")
    private Integer activityId;

    private String link;

    @Column("is_open")
    private Integer isOpen;

    private Date opentime;

    private String address;
    
    @Column("trip_city")
    private String tripCity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getImgUrl() {
		return imgUrl==null?"":imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = (imgUrl==null?"":imgUrl);
	}

	public String getTitle() {
		return title==null?"":title;
	}

	public void setTitle(String title) {
		this.title = (title==null?"":title);
	}

	public String getContent() {
		return content==null?"":content;
	}

	public void setContent(String content) {
		this.content = (content==null?"":content);
	}

	public String getType() {
		return type==null?"":type;
	}

	public void setType(String type) {
		this.type = (type==null?"":type);
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getActivityId() {
		return activityId==null?0:activityId;
	}

	public void setActivityId(Integer activityId) {
		this.activityId = (activityId==null?0:activityId);
	}

	public String getLink() {
		return link==null?"":link;
	}

	public void setLink(String link) {
		this.link = (link==null?"":link);
	}

	public Integer getIsOpen() {
		return isOpen;
	}

	public void setIsOpen(Integer isOpen) {
		this.isOpen = isOpen;
	}

	public Date getOpentime() {
		return opentime;
	}

	public void setOpentime(Date opentime) {
		this.opentime = opentime;
	}

	public String getAddress() {
		return address==null?"":address;
	}

	public void setAddress(String address) {
		this.address = (address==null?"":address);
	}

	public String getTripCity() {
		return tripCity==null?"":tripCity;
	}

	public void setTripCity(String tripCity) {
		this.tripCity = (tripCity==null?"":tripCity);
	}
    
}
