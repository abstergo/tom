package com.hefan.club.comment.meta;

/**
 * 俱乐部缓存keys
 */
public class RedisKeyConstant {

  /**
   * 广场列表排序的KEY
   * 类型：SortedSet
   * member：messageId
   * score：messageId
   */
  public final static String SQUARE_SORTED_KEY = "square_sorted";

  /**
   * 广场信息缓存的KEY
   * 类型：Hashs
   * field：messageID
   * value：动态信息拼装数据
   */
  public final static String SQUARE_HASHSET_KEY = "square_hashset";

  /**
   * 广场信息缓存的KEY
   * 类型：Hashs
   * field：messageID
   * value：动态信息拼装数据
   */
  public final static String SQUARE_HASHSET_KEY2 = "square_hashset_%s";

  /**
   * 广场页红点是否刷新标记
   * 类型：incr
   */
  public final static String SQUARE_LATEST_KEY = "square_latest";

  /**
   * 动态点赞用户列表
   * 类型：SortedSet
   * member：userId
   * score：time
   */
  public final static String PRAISE_SORTED_KEY = "parise_sorted_%s";
  /**
   * 每个用户每天发表条数 日期和userID
   */
  public final static String USER_FEEDS_COUNT = "user_feed_%s_count_%s";

  /**
   * 用户动态列表,用于自己和别人查看俱乐部动态信息
   */
  public final static String USER_FEEDS = "user_feed_%s";

  /**
   * 用户动态列表,按照ID降序排列，我的关注列表数据从此处获取
   */
  public final static String FEEDS = "feeds_%s";

  /**
   * 用户关注的动态列表 value:id,score:id
   */
  public final static String FOLLOW_FEEDS = "follow_feeds_%s";

  /**
   * 用户转码中动态
   * 类型：SortedSet
   * member：id
   * score -1*id
   */
  public final static String TRANSCODING_LIST = "transcoding_list_%s";

  public final static int THREE_MONTH_EXPIRE_TIME = 90 * 24 * 60;

  public final static int DAY_EXPIRE_TIME = 90 * 24 * 60;

  /**
   * web端处理动态变更查询开始时间
   */
  public final static String CLUB_DYNAMIC_WEB_TIMER_START = "clubDynamicWebTimerStart";
  /**
   * 动态点赞数
   * key：feedid
   * value:数值

  public final static String FEED_PRAISE_COUNT = "feed_parise_%s";
   */

  /**
   * 动态评论数
   * key：feedid
   * value:数值

  public final static String FEED_COMMENT_COUNT= "feed_comment_%s";
   */
  /**
   * 相册
   * 类型：Hashs
   * field：messageID
   * value：相册
   */
  public final static String FEED_ALBUM= "album_%s";

  /**
   * 动态排序 置顶基础分 900000000
   *
   */
  public final static int BASE_DATA= 900000000;

}
