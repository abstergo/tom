package com.hefan.club.dynamic.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 最后修改时间 2017/03/10
 * 修改人:葛玉琦
 * 内容:新增属性isSquare是否将动态发送至广场
 */
public class Message implements Serializable {
    @JSONField(
            serialize = false
    )
    private long id;

    private String userId;
    private String messageType;
    private int messageId;
    private String time;
    private String topStatus;
    private String watchStatus;
    private String pictureUrls;
    private String bucket;
    private int isSqure = 0;
    private int operateSource;

    private String path;
    private String messageInfo;
    private Date messageTime = new Date();
    private String messageTimes = "";
    private String length = "";
    private Integer ewaizan;
    private Integer times;
    private Boolean transcode = true;
    private String jobId;
    private String NickName;
    private String headImg;
    private String userType;
    private String livetype;
    private Integer isSync;
    private String pathTrans;
    /**
     * 发动态过来的图片路径全
     */
    private String pictureId;
    private Integer isDel;
    private int commentCount;
    private int praiseCount;
    private int presentCount;
    private String backImg = "";
    private String fromType = "0";
    private int praiseType;
    private int userLevel;
    private List articleContent;

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }

    public String getPictureUrls() {
        return pictureUrls;
    }

    public void setPictureUrls(String pictureUrls) {
        this.pictureUrls = pictureUrls;
    }

    public String getWatchStatus() {
        return watchStatus;
    }

    public void setWatchStatus(String watchStatus) {
        this.watchStatus = watchStatus;
    }

    public String getTopStatus() {
        return topStatus;
    }

    public void setTopStatus(String topStatus) {
        this.topStatus = topStatus;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public String getLivetype() {
        return livetype;
    }

    public void setLivetype(String livetype) {
        this.livetype = livetype;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getNickName() {
        return NickName;
    }

    public void setNickName(String nickName) {
        NickName = nickName;
    }

    public List getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(List articleContent) {
        this.articleContent = articleContent;
    }

    public String getFromType() {
        return fromType;
    }

    public void setFromType(String fromType) {
        this.fromType = fromType;
    }

    public int getPraiseType() {
        return praiseType;
    }

    public void setPraiseType(int praiseType) {
        this.praiseType = praiseType;
    }

    public String getBackImg() {
        return backImg;
    }

    public void setBackImg(String backImg) {
        this.backImg = backImg;
    }

    public int getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public int getPraiseCount() {
        return praiseCount;
    }

    public void setPraiseCount(int praiseCount) {
        this.praiseCount = praiseCount;
    }

    public int getPresentCount() {
        return presentCount;
    }

    public void setPresentCount(int presentCount) {
        this.presentCount = presentCount;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType == null ? null : messageType.trim();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    public String getMessageInfo() {
        return messageInfo;
    }

    public void setMessageInfo(String messageInfo) {
        this.messageInfo = messageInfo == null ? null : messageInfo.trim();
    }

    public Date getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(Date messageTime) {
        this.messageTime = messageTime;
    }

    public String getMessageTimes() {
        return messageTimes;
    }

    public void setMessageTimes(String messageTimes) {
        this.messageTimes = messageTimes;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length == null ? null : length.trim();
    }

    public Integer getEwaizan() {
        return ewaizan;
    }

    public void setEwaizan(Integer ewaizan) {
        this.ewaizan = ewaizan;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public Boolean getTranscode() {
        return transcode;
    }

    public void setTranscode(Boolean transcode) {
        this.transcode = transcode;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId == null ? null : jobId.trim();
    }

    public Integer getIsDel() {
        return isDel;
    }

    public void setIsDel(Integer isDel) {
        this.isDel = isDel;
    }

    public Integer getIsSync() {
        return isSync;
    }

    public void setIsSync(Integer isSync) {
        this.isSync = isSync;
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }

    public String getPathTrans() {
        return pathTrans;
    }

    public void setPathTrans(String pathTrans) {
        this.pathTrans = pathTrans;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public int getIsSqure() {
        return isSqure;
    }

    public void setIsSqure(int isSqure) {
        this.isSqure = isSqure;
    }

    public int getOperateSource() {
        return operateSource;
    }

    public void setOperateSource(int operateSource) {
        this.operateSource = operateSource;
    }
}
