package com.hefan.club.comment.bean;

import java.io.Serializable;
import java.util.Date;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;

@Entity(tableName = "comments")
public class Comments implements Serializable {
    /**
     * 评论ID
     * @author: LiTeng
     */
    private Long id;

    /**
     * 用户ID
     * @author: LiTeng
     */
    @Column("user_id")
    private  String userId; 
    
	/**
     * 消息ID
     * @author: LiTeng
     */
    @Column("message_id")
    private Integer messageId;
    
    /**
     * 动态userId
     * @author: LiTeng
     */
    @Column("muser_id")
    private String muserId;
    
    /**
     * 父消息id
     * @author: LiTeng
     */
    @Column("parent_comments_id")
    private Integer parentCommentsId;
    
    /**
     * 父消息userId
     * @author: LiTeng
     */
    @Column("parent_user_id")
    private String parentUserId;
    
    /**
     * 消息内容
     * @author: LiTeng
     */
    @Column("comments_info")
    private String commentsInfo;
    
    /**
     * 消息状态（0：正常，1：删除）
     * @author: LiTeng
     */
    @Column("comments_infostatus")
    private String commentsInfostatus;
    
    /**
     * 消息时间
     * @author: LiTeng
     */
    @Column("comments_time")
    private Date commentsTime;
    
    /**
     * 礼物id
     * @author: LiTeng
     */
    @Column("present_id")
    private Integer presentId;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserId() {
		return userId == null?"":userId;
	}
	public void setUserId(String userId) {
		this.userId = (userId==null?"":userId);
	}
	public Integer getMessageId() {
		return messageId == null?0:messageId;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = (messageId== null?0:messageId);
	}
	public String getMuserId() {
		return muserId==null?"":muserId;
	}
	public void setMuserId(String muserId) {
		this.muserId = (muserId==null?"":muserId);
	}
	public Integer getParentCommentsId() {
		return parentCommentsId==null?0:parentCommentsId;
	}
	public void setParentCommentsId(Integer parentCommentsId) {
		this.parentCommentsId = (parentCommentsId==null?0:parentCommentsId);
	}
	public String getParentUserId() {
		return parentUserId==null?"":parentUserId;
	}
	public void setParentUserId(String parentUserId) {
		this.parentUserId = (parentUserId==null?"":parentUserId);
	}
	public String getCommentsInfo() {
		return commentsInfo==null?"":commentsInfo;
	}
	public void setCommentsInfo(String commentsInfo) {
		this.commentsInfo = (commentsInfo==null?"":commentsInfo);
	}
	public String getCommentsInfostatus() {
		return commentsInfostatus==null?"0":commentsInfostatus;
	}
	public void setCommentsInfostatus(String commentsInfostatus) {
		this.commentsInfostatus = (commentsInfostatus==null?"0":commentsInfostatus);
	}
	public Date getCommentsTime() {
		return commentsTime;
	}
	public void setCommentsTime(Date commentsTime) {
		this.commentsTime = commentsTime;
	}
	public Integer getPresentId() {
		return presentId==null?0:presentId;
	}
	public void setPresentId(Integer presentId) {
		this.presentId = (presentId==null?0:presentId);
	}
}
