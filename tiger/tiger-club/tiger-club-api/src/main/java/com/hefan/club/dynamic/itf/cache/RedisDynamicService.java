package com.hefan.club.dynamic.itf.cache;

import com.cat.common.entity.Page;
import com.hefan.club.dynamic.bean.Message;

import java.util.List;

/**
 * 动态缓存操作
 *
 * @author wangchao
 * @title: wolf-api
 * @package com.hefan.club.cache
 * @copyright: Copyright (c) 2017
 * @date 2017-03-14 12:10
 */
public interface RedisDynamicService {
  /**
   * 缓存用户点赞数据
   *
   * @param userId
   * @param messageId
   * @return
   * @throws
   * @description （用一句话描述该方法的适用条件执行流程、适用方法、注意事项- 可选）
   * @author wangchao
   * @create 2017-03-14 12:20
   */
  void setPraiseToRedis(String userId, String messageId);

  /**
   * 动态发表成功后，缓存明细和更新个人缓存列表
   *
   * @param message
   * @return a
   * @throws a
   * @description （用一句话描述该方法的适用条件执行流程、适用方法、注意事项- 可选）
   * @author wangchao
   * @create 2017-03-14 12:24
   */
  void syncMessage(Message message);

  /**
   * 查询用户今天已发布动态数,先从缓存获取，缓存丢失从数据库获取
   *
   * @param userId
   * @return 返回已发条数
   * @throws Exception
   * @description （用一句话描述该方法的用条件、执行流程、适用方法、注意项 - 可选）
   * @author wangchao
   * @create 2017-03-14 15:42
   */
  int getMessageTotalByUserId(String userId) throws Exception;

  /**
   * 从缓存删除用户动态，用户动态列表和详细内容
   *
   *
   * @param Message
   * @return a
   * @throws a
   * @description （用一句话描述该方法的适用条件执行流程、适用方法、注意事项- 可选）
   * @author wangchao
   * @create 2017-03-15 14:28
   */
  void delMessageCache(Message mes) throws Exception;

  /**
   * 从缓存中获取动态详情 :正文，点赞数，评论数
   * @param 动态id
   */
  Message getMessageInfo(String id);

  /**
   * 获取动态相册数据
   * @param id
   * @return
   */
  List<Object> getDynamicAlbumCache(long id);

  /**
   * 获取feeds缓存列表
   *
   * @param anchorId 主播id
   * @param page 页码
   * @param key 缓存key
   * @param userId 用户ID
   * @return
   * @throws
   * @description （用一句话描述该方法的适用条件执行流程、适用方法、注意事项- 可选）
   * @author wangchao
   * @create 2017-03-21 11:03
   */
  List<Object> getFeedsFromCache(String anchorId, Page page, String key, String userId);

  /**
   * 删除相册
   * @param msgId
   */
  void delAlbumCache(Integer msgId) throws Exception;

  /**
   * 获取主播转码中动态
   * @param authorId
   */
  List<Object> getTranscodingList(String authorId) throws Exception;

  /**
   * 更新用户feed缓存score
   * @param userId 用户ID
   * @param msgId messgeId
   * @param score 正置顶，否，取消置顶
   */
  void updateUserFeedsScore(String userId, int msgId, double score) throws Exception;

  /**
   * 转码成功后，更新用户cache
   *
   * @param message
   * @return a
   * @throws Exception
   * @description （用一句话描述该方法的适用条件执行流程、适用方法、注意事项- 可选）
   * @author wangchao
   * @create 2017-03-22 16:06
   */
  void removeTranscodingToFeeds(Message message) throws Exception;

  /**
   * 增加用户每日发送数
   * @param userId
   */
  long incrMessageCount(String userId) throws Exception;
}
