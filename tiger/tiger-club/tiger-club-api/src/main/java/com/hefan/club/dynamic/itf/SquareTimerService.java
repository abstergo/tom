package com.hefan.club.dynamic.itf;

/**
 * Created by kevin_zhang on 10/03/2017.
 */
public interface SquareTimerService {

    /**
     * 获取最新新增的广场动态id
     */
    void getNewSquareMsgId();

    /**
     * 获取最新删除的广场动态id
     */
    void getDeletedSquareMsgId();

    /**
     * 获取最新评论数、点赞数变化的广场动态id

    void getNewChangeSquareMsgId();*/
}
