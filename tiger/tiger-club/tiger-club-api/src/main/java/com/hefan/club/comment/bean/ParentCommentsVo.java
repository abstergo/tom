package com.hefan.club.comment.bean;

import java.io.Serializable;

public class ParentCommentsVo implements Serializable {
	//被回复id
	public int parentCommentsId;
	//被回复者的userId
	public String parentUserId;
	//被回复者的头像
	public String parentHeadImg;
	//被回复者的昵称
	public String parentNickName;
	//被回复者的类型
	public int parentUserType; 
	//被回复者性别
	public int parentSex;
	//被回复者等级
	public String parentLevel;
	//被回复的内容
	public String parentCommentsInfo;
	//被回复的状态 0-被删除  1-未删除
	public int parentStatus=1;
	//被回复的评论时间
	public String parentCommentsTime;
	//被回复的礼物id(为0不是礼物评论)
	public int parentPresentId;
	public String parentPresentName;
	public String parentPresentIcon;
}
