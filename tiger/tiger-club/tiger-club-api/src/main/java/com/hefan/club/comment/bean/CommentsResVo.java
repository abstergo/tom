package com.hefan.club.comment.bean;

import java.io.Serializable;

/**
 * “所有评论”返回结构
 * @ClassName:  CommentsRes   
 * @Description:TODO(这里用一句话描述这个类的作用)   
 * @author: LiTeng  
 * @date:   2016年8月29日 下午5:15:31   
 *
 */
public class CommentsResVo implements Serializable {
	//回复人的userId
	public String resUserId;
	//回复人的头像
	public String resHeadImg;
	//回复人的昵称
	public String resNickName;
	//回复人性别
	public int resSex;
	//回复人等级
	public String resLevel; 
	//回复人的类型
	public int resUserType;
	//回复的id
	public int resCommentsId;
	//回复的内容
	public String resCommentsInfo;
	//回复的状态  0-被删除  1-未删除
	public int resStatus=1;
	//回复的时间
	public String resCommentsTime;
	//礼物id(为0不是礼物评论)
	public int presentId;
	public String resPresentName;
	public String resPresentIcon;
	//被回复信息
	public int parentCommentsId;
	public ParentCommentsVo parentComment;
	//动态信息
	public int messageId;
	//主播昵称
	public String ancNickName;
	public MessageResVo messageRes;
	
	
}
