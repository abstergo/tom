package com.hefan.club.comment.bean;

import java.io.Serializable;

public class MessageResVo implements Serializable {
	//动态id
	public int messageId;
	//动态创建人的userId
	public String mesUserId;
	//动态创建人的头像
	public String mesHeadImg;
	//动态创建人的昵称
	public String mesNickName;
	//动态创建人的类型
	public int mesUserType; 
	//动态创建人性别
	public int mesSex;
	//动态创建人等级
	public String mesLevel;
	//动态内容
	public String messageInfo;
	//动态状态 0-被删除  1-未删除
	public int mesStatus=1;
	//动态发布时间
	public String messageTime;
}
