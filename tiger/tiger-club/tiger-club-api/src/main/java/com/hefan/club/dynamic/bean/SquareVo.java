package com.hefan.club.dynamic.bean;

import java.io.Serializable;

/**
 * Created by nigle on 2017/3/9.
 */
public class SquareVo implements Serializable {
    String userId;
    int pageNo;
    String minMsgId;
    String maxMsgId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public String getMinMsgId() {
        return minMsgId;
    }

    public void setMinMsgId(String minMsgId) {
        this.minMsgId = minMsgId;
    }

    public String getMaxMsgId() {
        return maxMsgId;
    }

    public void setMaxMsgId(String maxMsgId) {
        this.maxMsgId = maxMsgId;
    }
}
