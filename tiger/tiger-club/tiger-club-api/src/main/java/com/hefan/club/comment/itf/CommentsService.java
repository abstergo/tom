package com.hefan.club.comment.itf;

import com.cat.common.entity.Page;
import com.hefan.club.comment.bean.Comments;
import com.hefan.club.comment.bean.ParentCommentsVo;

import java.util.List;
import java.util.Map;


public interface CommentsService {

	/**
	 * 根据id获取评论/回复信息
	 * 
	 * @Title: getCommentsByIdForCheck
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param
	 *             commentsId
	 * @param: @return
	 * @return: Comments
	 * @author: LiTeng
	 * @throws @date:
	 *             2016年9月28日 下午4:44:50
	 */
	Comments getCommentsByIdForCheck(long commentsId);

	/**
	 * 获取评论信息
	 * 
	 * @Title: getComments
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param
	 *             messageId
	 * @param: @param
	 *             userId
	 * @param: @param
	 *             isPresent 1-评论 2-礼物 其他-全部
	 * @param: @param
	 *             page
	 * @param: @return
	 * @return: Page
	 * @author: LiTeng
	 * @throws @date:
	 *             2016年9月27日 下午3:32:59
	 */
	Page getComments(int messageId, String userId, int isPresent, Page<Map<String, Object>> page);

	/**
	 * 根据父id获取评论/回复的父消息
	 *
	 * @Title: getParentCommentsByIds
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param
	 *             ids
	 * @param: @return
	 * @return: Map
	 * @author: LiTeng
	 * @throws @date:
	 *             2016年9月27日 下午3:34:48
	 */
	Map<Integer, ParentCommentsVo> getParentCommentsByIds(List<Integer> ids);


	/**
	 * 新增评论/回复/礼物评论
	 * 
	 * @Title: saveCommentsInfo
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param
	 *             com
	 * @param: @return
	 * @return: int
	 * @author: LiTeng
	 * @throws @date:
	 *             2016年9月28日 下午3:58:06
	 */
	int saveCommentsInfo(Comments com);

	/**
	 * 删除品论/回复
	 * 
	 * @Title: deleteCommentsById
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param
	 *             commentsId
	 * @param: @return
	 * @return: int
	 * @author: LiTeng
	 * @throws @date:
	 *             2016年9月28日 下午4:43:07
	 */
	int deleteCommentsById(long commentsId, int messageId, boolean isPresent,String userId);

	/**
	 * 获取第一条数据
	 * 
	 * @Title: getCommentsFristByUserId
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param userId
	 * @param isPresent
	 *            是否礼物评论 1-是， 其他为不是
	 * @return
	 * @return: Map
	 * @author: LiTeng
	 * @throws @date:
	 *             2016年10月9日 下午3:08:47
	 */
	Map getCommentsFristByUserId(String userId, int isPresent);

}
