package com.hefan.club.dynamic.bean;

import com.hefan.common.orm.annotation.Entity;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "message_album")
public class DynamicAlbum implements Serializable {

	private long id;
	private String userId;
	private int msgId;
	private String path;

	private String messageInfo;

	private Date createTime = new Date();

	private String createUser="";

	private int isDel;

  private Integer isSync;
  private String yea;
  private String mon;
  private String words;
  private String picId;

	public String getYea() {
		return yea;
	}

	public void setYea(String yea) {
		this.yea = yea;
	}

	public String getMon() {
		return mon;
	}

	public void setMon(String mon) {
		this.mon = mon;
	}

	public String getWords() {
		return words;
	}

	public void setWords(String words) {
		this.words = words;
	}

	public String getPicId() {
		return picId;
	}

	public void setPicId(String picId) {
		this.picId = picId;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setMsgId(int msgId) {
		this.msgId = msgId;
	}

	public void setIsDel(int isDel) {
		this.isDel = isDel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId == null ? null : userId.trim();
	}

	public Integer getMsgId() {
		return msgId;
	}

	public void setMsgId(Integer msgId) {
		this.msgId = msgId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path == null ? null : path.trim();
	}

	public String getMessageInfo() {
		return messageInfo;
	}

	public void setMessageInfo(String messageInfo) {
		this.messageInfo = messageInfo == null ? null : messageInfo.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser == null ? null : createUser.trim();
	}

	public Integer getIsDel() {
		return isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	public Integer getIsSync() {
		return isSync;
	}

	public void setIsSync(Integer isSync) {
		this.isSync = isSync;
	}

}
