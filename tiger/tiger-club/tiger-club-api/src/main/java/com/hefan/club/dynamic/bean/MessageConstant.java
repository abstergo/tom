package com.hefan.club.dynamic.bean;

public class MessageConstant {
	// 普通文字
	public static int WORDS = 1;
	//相册
	public static int PICTURE = 2;
	//视频
	public static int VIDEOS = 3;
	public static int ISSYNC = 1;
	public static int NOSYNC = 0;

	public static int COMMENTSCOUT = 1;
	public static int PRESENTCOUNT = 2;
	public static int PRAISECOUNT = 3;
}
