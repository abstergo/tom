package com.hefan.club.dynamic.itf;

import com.hefan.club.dynamic.bean.DynamicAlbum;

public interface DynamicAlbumService {
	/**
	 * 删除相册
	 * @param record
	 * @return
	 */
	int deleteAlbumByParams(DynamicAlbum record) throws Exception;
	
	
	/**
	 * 删除相册
	 * @param record
	 * @return
	 */
	int checkLast(String messageId);
}
