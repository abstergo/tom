#!/bin/sh
DATE=`date "+%Y%m%d%H%M"`
job=$1
work=tiger
work1=tiger-main
dir_work="/hefan/gkd_app/build.$work"
project="/hefan/gkd_app/build.$work/$work"
target="$project/$work1/target"
deploy_target="/hefan/gkd_app/build.$work"
backup="backup"
logfile="/hefan/gkd_app/build.$work/dubbo-tiger-provider.log"

update() {
    cd $project
    git checkout master
    git pull
}

build() {
    cd $project
    mvn clean deploy -Pbuild
    cd $project/$work1
    mvn clean package -DskipTests -Pproduct -U
    WAR=`ls $target|grep .tar.gz`
    echo $WAR
    cp target/$WAR $deploy_target/$backup/$WAR.$DATE
    rm -rf $deploy_target/ROOT
    mkdir $deploy_target/ROOT
    tar zxvf target/$WAR -C $deploy_target/ROOT/
    mv $deploy_target/ROOT/**/* $deploy_target/ROOT/
}

backup() {
    cd $dir_work
    tar cfz ROOT.$DATE.tar.gz ROOT
    mv ROOT.$DATE.tar.gz $backup/
}

restart() {
    cd $dir_work/ROOT/bin
    ./restart.sh
}

case "$job" in
    build)
        build
    ;;
    update)
        update
    ;;
    backup)
        backup
    ;;
    restart)
        restart
    ;;
    *)
        backup
        update
        build
        restart
esac