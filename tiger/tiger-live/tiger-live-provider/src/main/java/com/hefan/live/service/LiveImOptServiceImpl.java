package com.hefan.live.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ImCustomMsgEnum;
import com.cat.common.meta.ResultCode;
import com.cat.common.util.RandomUtil;
import com.cat.tiger.util.GlobalConstants;
import com.google.common.collect.Maps;
import com.hefan.common.ons.TopicRegistry;
import com.hefan.common.ons.bean.Message;
import com.hefan.common.ons.service.ONSProducer;
import com.hefan.common.util.DynamicProperties;
import com.hefan.live.bean.LiveLog;
import com.hefan.live.bean.LiveRoomPersonVo;
import com.hefan.live.itf.LiveImOptService;
import com.hefan.notify.itf.ImChatroomService;
import com.hefan.notify.itf.ImMsgService;
import com.hefan.oms.bean.ImCustomMsgVo;
import com.hefan.user.bean.WebUser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 直播间IM自定义消息发送处理
 *
 * @author kevin_zhang
 *
 */
@Component("liveImOptService")
public class LiveImOptServiceImpl implements LiveImOptService {
	@Resource
	ImChatroomService imChatroomService;
	@Resource
	ImMsgService imMsgService;
	@Resource
	ONSProducer onsProducer;

	public Logger logger = LoggerFactory.getLogger(LiveImOptServiceImpl.class);
	/**
	 * 参数常量(可选) 操作完成后是否需要发广播，true或false，默认true
	 */
	private static final String SHUTUP_TIME = "43200";// 禁言时间
	/**
	 * IM禁言接口参数，needNotify
	 */
	private static final String NEEDNOTIFY_FALSE = "false";
	/**
	 * IM禁言接口参数，notifyExt(可选) 通知广播事件中的扩展字段，长度限制2048字符
	 */
	private static final String NOTIFYEXT_EMPTY = "";

	/**
	 * 发私信
	 */
	private String from;// 发送者accid，用户帐号，最大32字符，必须保证一个APP内唯一
	private String ope;// 0：点对点个人消息，1：群消息，其他返回414
	private String to;// ope==0是表示accid即用户id，ope==1表示tid即群id
	private String type;// 0 表示文本消息,1 表示图片，、2 表示语音，3 表示视频，4 表示地理位置信息，6 表示文件，100
						// 自定义消息类型
	private String body;// 请参考下方消息示例说明中对应消息的body字段，最大长度5000字符，为一个json串
	private String option;// 发消息时特殊指定的行为选项,Json格式，可用于指定消息的漫游，存云端历史，发送方多端同步，推送，消息抄送等特殊行为;option中字段不填时表示默认值
	private String pushcount;// ios推送内容，不超过150字符，option选项中允许推送（push=true），此字段可以指定推送内容
	private String payload;// ios 推送对应的payload,必须是JSON,不能超过2k字符
	private String ext_sms;// 开发者扩展字段，长度限制1024字符
	private String forcepushlist;// 发送群消息时的强推（@操作）用户列表，格式为JSONArray，如["accid1","accid2"]。若forcepushall为true，则forcepushlist为除发送者外的所有有效群成员
	private String forcepushcontent;// 发送群消息时，针对强推（@操作）列表forcepushlist中的用户，强制推送的内容
	private String forcepushall;// 发送群消息时，强推（@操作）列表是否为群里除发送者外的所有有效成员，true或false，默认为false
	/**
	 * 发IM
	 */
	private long roomid;// (必传) 聊天室id
	private String fromAccid;// (必传) 消息发出者的账号accid
	private int msgType;// (必传) 消息类型： 0: 表示文本消息，1: 表示图片，2: 表示语音，3: 表示视频，4:
						// 表示地理位置信息，6: 表示文件，10: 表示Tips消息，100: 自定义消息类型
	private int resendFlag;// (可选) 重发消息标记，0：非重发消息，1：重发消息，如重发消息会按照msgid检查去重逻辑
	private String attach;// (可选) 消息内容，格式同消息格式示例中的body字段,长度限制2048字符
	private String ext;// (可选) 消息扩展字段，内容可自定义，请使用JSON格式，长度限制4096
	/**
	 * 禁言操作
	 */
	private String operator; // (必传) 操作者accid,必须是管理员或创建者
	private String target; // (必传) 被禁言的目标账号accid

	/**
	 * 禁言IM消息处理
	 * @param optUser 操作者
	 * @param user 被操作者
	 * @param chatRoomId
	 * @param liveUuid
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public boolean liveShutUpIm(WebUser optUser, WebUser user, int chatRoomId, String liveUuid, String superAdmin) {
		roomid = chatRoomId;
		fromAccid = optUser.getUserId();
		ResultBean resultBean = null;
		int code = 0;
		// 禁言
		resultBean = imChatroomService.temporaryMute(roomid, superAdmin, user.getUserId(), SHUTUP_TIME,
				NEEDNOTIFY_FALSE, NOTIFYEXT_EMPTY);
		code = resultBean.getCode();
		if (code == 1000 || user.getUserType() == GlobalConstants.USER_TYPE_ROBOT) {
			// 广播
			msgType = 100;// 自定义消息
			resendFlag = 0;// 非重发消息
			ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
			imMsgVo.setType(ImCustomMsgEnum.LiveShutupMsg.getType());
			imMsgVo.setContent(user.getNickName() + ImCustomMsgEnum.LiveShutupMsg.getMsg());
			imMsgVo.setLiveUuid(liveUuid);
			imMsgVo.setUserId(user.getUserId());
			attach = JSONObject.toJSONString(imMsgVo);

			Map imItem = new HashMap<>();
			imItem.put("userId", user.getUserId());
			imItem.put("headImg", user.getHeadImg());
			imItem.put("userType", user.getUserType());
			imItem.put("userLevel", user.getUserLevel());
			imItem.put("nickName", user.getNickName());
			ext = JSONObject.toJSONString(imItem);
			// IM
			imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
			from = GlobalConstants.HEFAN_OFFICIAL_ID;
			ope = "0";// 点对点消息
			to = user.getUserId();
			type = "100";// 自定义消息
			imMsgVo.setContent("您" + ImCustomMsgEnum.LiveShutupMsg.getMsg());
			body = JSONObject.toJSONString(imMsgVo);
			// 私信
			Map roamMap = new HashMap();
			roamMap.put("persistent", false);
			roamMap.put("history", false);
			roamMap.put("badge", false);
			imMsgService.msgsendMsg(from, ope, to, type, body, JSONObject.toJSONString(roamMap), "", "", "", "", "", "");
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 踢出IM消息处理
	 * @param optUser 操作者
	 * @param user 被操作者
	 * @param chatRoomId
	 * @param liveUuid
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void liveOutIm(WebUser optUser, WebUser user, int chatRoomId, String liveUuid) {
		roomid = chatRoomId;
		fromAccid = optUser.getUserId();
		// 广播
		msgType = 100;// 自定义消息
		resendFlag = 0;// 非重发消息
		ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
		imMsgVo.setType(ImCustomMsgEnum.LiveOuterMsg.getType());
		imMsgVo.setContent(user.getNickName() + ImCustomMsgEnum.LiveOuterMsg.getMsg());
		imMsgVo.setUserId(user.getUserId());
		imMsgVo.setLiveUuid(liveUuid);
		attach = JSONObject.toJSONString(imMsgVo);

		Map imItem = new HashMap<>();
		imItem.put("userId", user.getUserId());
		imItem.put("headImg", user.getHeadImg());
		imItem.put("userType", user.getUserType());
		imItem.put("userLevel", user.getUserLevel());
		imItem.put("nickName", user.getNickName());
		ext = JSONObject.toJSONString(imItem);
		// IM
		ResultBean r2 = imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
		if (r2.getCode() != ResultCode.SUCCESS.get_code()) {
			logger.info(user.getUserId() + "被踢出IM广播发送失败");
		}
		// 更新用户列表 走离开直播间队列任务
		Map paramMap = Maps.newHashMap();
		paramMap.put("liveUuid", liveUuid);
		paramMap.put("chatRoomId", chatRoomId);
		paramMap.put("authoruserId", optUser.getUserId());
		paramMap.put("userId", user.getUserId());
		try {
			logger.info("踢出直播间队列消息发送－－Start");
			// 更新观看用户列表,向云信端发送群自定义消息
			Message message = new Message();
			message.put("leftLiveRoomVo", JSON.toJSONString(paramMap));
			message.setTopic(TopicRegistry.HEFAN_LIVE_ENTER);
			String onsEnv = DynamicProperties.getString("ons.env");
			message.setTag(onsEnv);
			onsProducer.sendMsg(message);
			logger.info("踢出直播间队列消息发送－－End");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("踢出直播间队列队列消息发送失败", e);
		}
		from = GlobalConstants.HEFAN_OFFICIAL_ID;
		ope = "0";// 点对点消息
		to = user.getUserId();
		type = "100";// 自定义消息
		imMsgVo.setContent("您" + ImCustomMsgEnum.LiveOuterMsg.getMsg());
		body = JSONObject.toJSONString(imMsgVo);
		// 私信
		Map roamMap = new HashMap();
		roamMap.put("persistent", false);
		roamMap.put("history", false);
		roamMap.put("badge", false);
		ResultBean r3 = imMsgService.msgsendMsg(from, ope, to, type, body, JSONObject.toJSONString(roamMap), "", "", "", "", "", "");
		if (r3.getCode() != ResultCode.SUCCESS.get_code()) {
			logger.info(user.getUserId() + "被踢出私信发送失败");
		}
	}

	/**
	 * 设置和取消管理员
	 * @param optUser 操作者
	 * @param user 被操作者
	 * @param action 动作：SET 设置管理员 UNSET 取消管理医院
	 * @param chatRoomId
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ResultBean liveOptAdmin(WebUser optUser, WebUser user, int chatRoomId, String liveUuid, String action) {
		roomid = chatRoomId;
		fromAccid = optUser.getUserId();
		msgType = 100;
		resendFlag = 0;
		ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
		if (action.equals("SET")) {
			imMsgVo.setType(ImCustomMsgEnum.LiveSetAdmin.getType());
			imMsgVo.setContent(user.getNickName() + ImCustomMsgEnum.LiveSetAdmin.getMsg());
		} else {
			imMsgVo.setType(ImCustomMsgEnum.LiveUnSetAdmin.getType());
			imMsgVo.setContent(user.getNickName() + ImCustomMsgEnum.LiveUnSetAdmin.getMsg());
		}
		imMsgVo.setUserId(user.getUserId());
		imMsgVo.setLiveUuid(liveUuid);
		attach = JSONObject.toJSONString(imMsgVo);
		Map imItem = new HashMap<>();
		imItem.put("userId", user.getUserId());
		imItem.put("headImg", user.getHeadImg());
		imItem.put("userType", user.getUserType());
		imItem.put("userLevel", user.getUserLevel());
		imItem.put("nickName", user.getNickName());
		ext = JSONObject.toJSONString(imItem);
		return imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
	}

	/**
	 * 关播发IM关播
	 * @param user
	 * @param chatRoomId
	 * @param liveUuid
	 * @param watchNum
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ResultBean liveEndIm(WebUser user, int chatRoomId, String liveUuid, String watchNum, String liveLength,
			String heFanNum) {
		ResultBean resultBean = new ResultBean();
		roomid = chatRoomId;
		fromAccid = user.getUserId();
		msgType = 100;
		resendFlag = 0;
		ImCustomMsgVo imCustomMsgVo = new ImCustomMsgVo();
		imCustomMsgVo.setType(ImCustomMsgEnum.LiveEndMsg.getType());
		imCustomMsgVo.setContent(ImCustomMsgEnum.LiveEndMsg.getMsg());
		imCustomMsgVo.setWatchNum(watchNum);
		imCustomMsgVo.setLiveLength(liveLength);
		imCustomMsgVo.setHeFanNum(heFanNum);
		imCustomMsgVo.setLiveUuid(liveUuid);
		attach = JSONObject.toJSONString(imCustomMsgVo);

		Map imItem = new HashMap<>();
		imItem.put("userId", user.getUserId());
		imItem.put("headImg", user.getHeadImg());
		imItem.put("userType", user.getUserType());
		imItem.put("userLevel", user.getUserLevel());
		imItem.put("nickName", user.getNickName());
		ext = JSONObject.toJSONString(imItem);
		if (chatRoomId == 0 || StringUtils.isBlank(fromAccid) || StringUtils.isBlank(attach)
				|| StringUtils.isBlank(ext)) {
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return resultBean;
		}
		resultBean = imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
		return resultBean;
	}

	/**
	 * 更新直播间用户列表
	 * @param chatRoomId
	 * @param liveUuid
	 * @param authoruserId
	 * @param userListStr:("membersList":"")
	 */
	@SuppressWarnings({ "rawtypes" })
	@Override
	public ResultBean updateUserListIm(int chatRoomId, String liveUuid, String authoruserId, String userListStr) {
		try {
			logger.info("更新直播间用户列表IM消息发送－－start");
			roomid = chatRoomId;// (必传) 聊天室id
			fromAccid = authoruserId;// (必传) 消息发出者的账号id
			msgType = 100;// (必传)自定义消息
			resendFlag = 0;// (可选) 重发消息标记，0：非重发消息，1：重发消息

			ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
			imMsgVo.setType(ImCustomMsgEnum.IM_MSG_205.getType());
			imMsgVo.setLiveUuid(liveUuid);
			attach = JSONObject.toJSONString(imMsgVo);

			// {"membersList":"[],[]"}
			ext = userListStr;
			logger.info("更新直播间用户列表IM消息发送:" + ext);

			return imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("更新直播间用户列表IM消息发送失败：" + e.getMessage().toString());
			return new ResultBean(ResultCode.UNSUCCESS.get_code(), ResultCode.UNSUCCESS.getMsg());
		} finally {
			logger.info("更新直播间用户列表IM消息发送－－end");
		}
	}

	/**
	 * 高等级用户进入直播间
	 * @param chatRoomId
	 * @param liveUuid
	 * @param authoruserId
	 * @param userInfoStr
	 */
	@SuppressWarnings({ "rawtypes" })
	@Override
	public ResultBean highLevelUserIm(int chatRoomId, String liveUuid, String authoruserId, String userInfoStr) {
		try {
			logger.info("高等级用户进入直播间IM消息发送－－start");
			roomid = chatRoomId;// (必传) 聊天室id
			fromAccid = authoruserId;// (必传) 消息发出者的账号id
			msgType = 100;// (必传)自定义消息
			resendFlag = 0;// (可选) 重发消息标记，0：非重发消息，1：重发消息

			// 广播
			ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
			imMsgVo.setType(ImCustomMsgEnum.IM_MSG_105.getType());
			imMsgVo.setLiveUuid(liveUuid);
			attach = JSONObject.toJSONString(imMsgVo);

			ext = userInfoStr;
			logger.info("高等级用户进入直播间IM消息发送:" + ext);

			return imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("高等级用户进入直播间IM消息发送失败：" + e.getMessage().toString());
			return new ResultBean(ResultCode.UNSUCCESS.get_code(), ResultCode.UNSUCCESS.getMsg());
		} finally {
			logger.info("高等级用户进入直播间IM消息发送－－end");
		}
	}

	/**
	 * 直播间人数更新IM通知
	 * @param chatRoomId
	 * @param liveUuid
	 * @param authoruserId
	 */
	@SuppressWarnings({ "rawtypes" })
	@Override
	public ResultBean updateOnlineUserNumIm(int chatRoomId, String liveUuid, String authoruserId, long watchNum) {
		try {
			logger.info("直播间人数更新IM消息发送－－start");
			roomid = chatRoomId;// (必传) 聊天室id
			fromAccid = authoruserId;// (必传) 消息发出者的账号id
			msgType = 100;// (必传)自定义消息
			resendFlag = 0;// (可选) 重发消息标记，0：非重发消息，1：重发消息

			ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
			imMsgVo.setType(ImCustomMsgEnum.LiveOnLineNum.getType());
			imMsgVo.setWatchNum(String.valueOf(watchNum));
			imMsgVo.setLiveUuid(liveUuid);
			attach = JSONObject.toJSONString(imMsgVo);
			logger.info("直播间人数更新IM消息发送:" + attach);

			return imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("直播间人数更新IM消息发送" + e.getMessage().toString());
			return new ResultBean(ResultCode.UNSUCCESS.get_code(), ResultCode.UNSUCCESS.getMsg());
		} finally {
			logger.info("直播间人数更新IM消息发送－－end");
		}
	}

	/**
	 * 请离出直播间，用于主播开播前操作
	 * @param userId
	 * @param liveUuid
	 * @param chatRoomId
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ResultBean liveStartClear(String userId, String liveUuid, int chatRoomId) {
		ResultBean resultBean = new ResultBean();
		if (StringUtils.isBlank(userId) || StringUtils.isBlank(liveUuid) || chatRoomId < 0) {
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return resultBean;
		}
		roomid = chatRoomId;
		fromAccid = GlobalConstants.HEFAN_OFFICIAL_ID;
		msgType = 100;// 自定义消息
		resendFlag = 0;// 非重发消息
		ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
		imMsgVo.setType(ImCustomMsgEnum.IM_MSG_207.getType());
		imMsgVo.setUserId(userId);
		imMsgVo.setLiveUuid(liveUuid);
		imMsgVo.setContent(ImCustomMsgEnum.IM_MSG_207.getMsg());
		attach = JSONObject.toJSONString(imMsgVo);

		Map imItem = new HashMap<>();
		imItem.put("userId", userId);
		imItem.put("chatRoomId", chatRoomId);
		ext = JSONObject.toJSONString(imItem);
		resultBean = imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
		return resultBean;
	}

	/**
	 * 点亮IM消息 泡泡+文字
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ResultBean lightRoom(int chatRoomId, String liveUuid, LiveRoomPersonVo liveRoomPersonVo) {
		roomid = chatRoomId;
		fromAccid = GlobalConstants.HEFAN_OFFICIAL_ID;
		// 广播
		msgType = 100;// 自定义消息
		resendFlag = 0;// 非重发消息
		ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
		imMsgVo.setType(ImCustomMsgEnum.LIGHT_ROOM.getType());
		imMsgVo.setIndexOfClickScreen(RandomUtil.getRandom(0, 10));
		imMsgVo.setLiveUuid(liveUuid);
		attach = JSONObject.toJSONString(imMsgVo);
		ext = JSONObject.toJSONString(liveRoomPersonVo);
		// IM
		return imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
	}

	/**
	 * 泡泡消息 只有泡泡
	 */
	@Override
	public ResultBean lightRoomOnlyPop(int chatRoomId, String liveUuid) {
		roomid = chatRoomId;
		fromAccid = GlobalConstants.HEFAN_OFFICIAL_ID;
		// 广播
		msgType = 100;// 自定义消息
		resendFlag = 0;// 非重发消息
		ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
		imMsgVo.setType(ImCustomMsgEnum.LIGHT_ROOM_PAOPAO.getType());
		imMsgVo.setIndexOfClickScreen(RandomUtil.getRandom(0, 10));
		imMsgVo.setLiveUuid(liveUuid);
		imMsgVo.setNickName("机器人随机点亮");
		attach = JSONObject.toJSONString(imMsgVo);
		LiveRoomPersonVo liveRoomPersonVo = new LiveRoomPersonVo();
		liveRoomPersonVo.setNickName("robot light");
		ext = JSONObject.toJSONString(liveRoomPersonVo);
		// IM
		return imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
	}

	/**
	 * 关注IM消息
	 */
	@Override
	public ResultBean watchAnchor(int chatRoomId, String liveUuid, LiveRoomPersonVo liveRoomPersonVo) {
		roomid = chatRoomId;
		fromAccid = GlobalConstants.HEFAN_OFFICIAL_ID;
		// 广播
		msgType = 100;// 自定义消息
		resendFlag = 0;// 非重发消息
		ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
		imMsgVo.setType(ImCustomMsgEnum.WATCH_ANTHER.getType());
		imMsgVo.setUserId(liveRoomPersonVo.getUserId());
		imMsgVo.setNickName(liveRoomPersonVo.getNickName());
		imMsgVo.setLiveUuid(liveUuid);
		attach = JSONObject.toJSONString(imMsgVo);
		ext = JSONObject.toJSONString(liveRoomPersonVo);
		// IM
		return imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
	}

	/**
	 * 踢出登录
	 */
	@Override
	public ResultBean userLoginOut(WebUser user) {
		from = GlobalConstants.HEFAN_OFFICIAL_ID;
		ope = "0";// 点对点消息
		to = user.getUserId();
		type = "100";// 自定义消息
		ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
		imMsgVo.setType(ImCustomMsgEnum.IM_MSG_208.getType());
		imMsgVo.setContent(user.getNickName() + ImCustomMsgEnum.IM_MSG_208.getMsg());
		imMsgVo.setUserId(user.getUserId());
		body = JSONObject.toJSONString(imMsgVo);
		Map roamMap = new HashMap();
		roamMap.put("persistent", false);
		roamMap.put("history", false);
		roamMap.put("badge", false);
        ResultBean resultBean = imMsgService.msgsendMsg(from,ope,to,type,body,JSONObject.toJSONString(roamMap),"","","","","","");
        return resultBean;
	}

	/**
	 * 禁播发IM
	 * @param user
	 * @param chatRoomId
	 * @param liveUuid
	 * @param watchNum
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public ResultBean liveBand(WebUser user, int chatRoomId, String liveUuid, String watchNum, String liveLength,
			String heFanNum,String promptCopy) {
		ResultBean resultBean = new ResultBean();
		roomid = chatRoomId;
		fromAccid = user.getUserId();
		msgType = 100;
		resendFlag = 0;
		ImCustomMsgVo imCustomMsgVo = new ImCustomMsgVo();
		imCustomMsgVo.setType(ImCustomMsgEnum.Live_Ban.getType());
		imCustomMsgVo.setContent(StringUtils.isBlank(promptCopy)?ImCustomMsgEnum.Live_Ban.getMsg():promptCopy);
		imCustomMsgVo.setWatchNum(watchNum);
		imCustomMsgVo.setLiveLength(liveLength);
		imCustomMsgVo.setHeFanNum(heFanNum);
		imCustomMsgVo.setLiveUuid(liveUuid);
		attach = JSONObject.toJSONString(imCustomMsgVo);

		Map imItem = new HashMap<>();
		imItem.put("userId", user.getUserId());
		imItem.put("headImg", user.getHeadImg());
		imItem.put("userType", user.getUserType());
		imItem.put("userLevel", user.getUserLevel());
		imItem.put("nickName", user.getNickName());
		ext = JSONObject.toJSONString(imItem);
		if (chatRoomId == 0 || StringUtils.isBlank(fromAccid) || StringUtils.isBlank(attach)
				|| StringUtils.isBlank(ext)) {
			resultBean.setCode(ResultCode.UNSUCCESS.get_code());
			resultBean.setMsg(ResultCode.UNSUCCESS.getMsg());
			return resultBean;
		}
		return imChatroomService.chatroomSendMsg(roomid, fromAccid, msgType, resendFlag, attach, ext);
	}

	@Override
	@Async
	public ResultBean liveEndImAsync(WebUser user, int chatRoomId, String liveUuid, String watchNum, String liveLength, String heFanNum) {
		return this.liveEndIm(user,chatRoomId,liveUuid,watchNum,liveLength,heFanNum);
	}


}
