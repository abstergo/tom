package com.hefan.live.dao;

import com.cat.tiger.util.CollectionUtils;
import com.hefan.live.bean.WarnLog;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Repository
public class LiveWarnDao {
	
	@Resource
    JdbcTemplate jdbcTemplate;
	
	public int saveWarnLog(WarnLog warnLog) {
		// TODO Auto-generated method stub
		String insertSql = "insert into warn_log(user_id,create_time,remark,chat_room_id,live_uuid,opt_user_id,warn_content)"
				+"VALUES(?,now(),?,?,?,?,?)";
		//参数数组
	   	Object[]  paramArr =new Object[] {
	   			warnLog.getUserId(),
	   			warnLog.getRemark(),
	   			warnLog.getChatRoomId(),
	   			warnLog.getLiveUuid(),
	   			warnLog.getOptUserId(),
				warnLog.getWarnContent()
	   			 }; 
	   	 //参数类型数组
	   	 int[] paramtypeArr = new int[]{
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.INTEGER,
				java.sql.Types.VARCHAR,
			    java.sql.Types.VARCHAR,
				 java.sql.Types.VARCHAR
			   }; 
	   	 return jdbcTemplate.update(insertSql,paramArr,paramtypeArr);
	}
	
	public int updateLiveHighStatus(String userId,int risk) {
		// TODO Auto-generated method stub
		String sql = "update live_room set high_status=? where user_id=?";
		return jdbcTemplate.update(sql,risk==1?2:0,userId);
	}
	
	public int saveLiveHighStatusLog(String userId,String liveUuid,int risk,String optUserId) {
		// TODO Auto-generated method stub
		String insertSql = "insert into live_high_status_log(user_id,live_uuid,risk_type,delete_flag,create_time,update_time,opt_user_id)"
				+"VALUES(?,?,?,0,now(),now(),?)";
		//参数数组
	   	Object[]  paramArr =new Object[] {
	   			userId,
	   			liveUuid,
	   			risk,
	   			optUserId
	   			 }; 
	   	 //参数类型数组
	   	 int[] paramtypeArr = new int[]{
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.INTEGER,
				java.sql.Types.VARCHAR
			   }; 
	   	 return jdbcTemplate.update(insertSql,paramArr,paramtypeArr);
	}
	
	public List getPunishTypeList(){
		String sql = "select id as typeId,category,type_name as typeName,description," +
				" IFNULL(freeze_days,0) freezeDays,IFNULL(stop_days,0) stopDays," +
				" IFNULL(ded_commision,0) dedCommision,IFNULL(ded_money,0) dedMoney" +
				" from punish_type where delete_flag=0 ORDER BY create_time ";
		return jdbcTemplate.queryForList(sql);
	}


	public Map getPunishTypeByIdAndPunishTypeFromTb(long typeId,int punishTypeFromTb){
		String tableName = " punish_type ";
		if(punishTypeFromTb==1){
			tableName = " web_user_punish_type ";
		}
		String sql ="select * from "+tableName+" where delete_flag=0 and id=?";
		List list = jdbcTemplate.queryForList(sql,typeId);
		return CollectionUtils.isEmpty(list)?null:(Map)list.get(0);
	}

	public int dealPunishDetalL(String userId, String optUserId,Map map, String punishReason,
							   String illegalPic,String liveUuid,int punishTypeFromTb, int punishDataType,int pid){
		
		String insertSql = "insert into punish_detail("
				+ "user_id,category,type_id,type_name,freeze_days,stop_days,ded_money,ded_commision,"
				+ "punish_reason,create_time,create_user,illegal_pic,from_type,live_uuid,punish_type_from_tb,punish_data_type,pid)"
				+ "VALUES(?,?,?,?,?,?,?,?,?,now(),?,?,1,?,?,?,?)";
		//参数数组
		Object[]  paramArr = null;
		if(punishTypeFromTb==0){
			paramArr =new Object[] {
					userId,
					Integer.parseInt(map.get("category").toString()),
					Long.parseLong(map.get("id").toString()),
					map.get("type_name").toString(),
					Integer.parseInt(map.get("freeze_days").toString()),
					Integer.parseInt(map.get("stop_days").toString()),
					map.get("ded_money").toString(),
					map.get("ded_commision").toString(),
					punishReason,
					optUserId,
					illegalPic,
					liveUuid,
					0,
					0,
					pid
			};
		}else if(punishTypeFromTb==1){
				  paramArr =new Object[] {
					userId,
					Integer.parseInt(map.get("category").toString()),
					Long.parseLong(map.get("id").toString()),
					map.get("type_name").toString(),
					Integer.parseInt(map.get("freeze_days").toString()),
					Integer.parseInt(map.get("stop_days").toString()),
					map.get("ded_money").toString(),
					map.get("ded_commision").toString(),
					punishReason,
					optUserId,
					illegalPic,
					liveUuid,
					1,
					punishDataType,
					pid
			};
		}


		//参数类型数组
		int[] paramtypeArr = new int[]{
				java.sql.Types.VARCHAR,
				java.sql.Types.INTEGER,
				java.sql.Types.BIGINT,
				java.sql.Types.VARCHAR,
				java.sql.Types.INTEGER,
				java.sql.Types.INTEGER,
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.INTEGER,
				java.sql.Types.INTEGER,
				java.sql.Types.INTEGER
		};
		return jdbcTemplate.update(insertSql,paramArr,paramtypeArr);
	}

	//获取用户上级id【pid】
	public int getUserPid(String userId){
		String sql = "select wi.pid from web_user_identity wi where wi.user_id=? ";
		List<Integer> list = jdbcTemplate.queryForList(sql,Integer.class,userId);
		return CollectionUtils.isNotEmpty(list)?list.get(0):0;
	}

	//冻结
	public int freezeUser(String userId){
		String sql = "update web_user set state=1 where user_id=?";
		return jdbcTemplate.update(sql,userId);
	}
	//禁播
	public int stopLiveRoom(String userId,int stopDays){
		String sql = "update live_room set stuff=1,stop_days=stop_days+? where user_id=?";
		return jdbcTemplate.update(sql,stopDays,userId);
	}
	
	public Map getTaskPunish(String userId){
		String sql = "select * from task_punish where user_id=?";
		List list = this.jdbcTemplate.queryForList(sql,userId);
		return CollectionUtils.isEmpty(list)?null:(Map)list.get(0);
	}
	
	public int insertTaskPunish(String userId,int freezeDays,int stopDays){
		String insertSql = "insert into task_punish("
				+ "user_id,freeze_days,stop_days,account_unfreeze_time,live_open_time)"
				+ "VALUES(?,?,?,DATE_ADD(now(),INTERVAL ? DAY),DATE_ADD(now(),INTERVAL ? DAY))";
		//参数数组
	   	Object[]  paramArr =new Object[] {
	   			userId,
	   			freezeDays,
	   			stopDays,
	   			freezeDays,
	   			stopDays
	   			 }; 
	   	 //参数类型数组
	   	 int[] paramtypeArr = new int[]{
				java.sql.Types.VARCHAR,
				java.sql.Types.INTEGER,
				java.sql.Types.INTEGER,
				java.sql.Types.INTEGER,
				java.sql.Types.INTEGER
			   }; 
	   	 return jdbcTemplate.update(insertSql,paramArr,paramtypeArr);
	}
	
	public int updateTaskPunish(String userId,int freezeDays,int stopDays){
		String insertSql = "update task_punish set freeze_days=freeze_days+?,stop_days=stop_days+?"
				+ ",account_unfreeze_time=DATE_ADD(account_unfreeze_time,INTERVAL ? DAY)"
				+ ",live_open_time=DATE_ADD(live_open_time,INTERVAL ? DAY)"
				+ " where user_id=?";
		//参数数组
	   	Object[]  paramArr =new Object[] {
	   			freezeDays,
	   			stopDays,
	   			freezeDays,
	   			stopDays,
	   			userId
	   			 }; 
	   	 //参数类型数组
	   	 int[] paramtypeArr = new int[]{
				java.sql.Types.INTEGER,
				java.sql.Types.INTEGER,
				java.sql.Types.INTEGER,
				java.sql.Types.INTEGER,
				java.sql.Types.VARCHAR
			   }; 
	   	 return jdbcTemplate.update(insertSql,paramArr,paramtypeArr);
	}

}