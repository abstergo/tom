package com.hefan.live.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * Created by kevin_zhang on 06/01/2017.
 */
@Repository
public class LiveConfigCenter {

    /**
     * 配置中心
     */
    @Value("#{imSendCheckConfig}")
    private Map<String, String> imSendCheckConfig;

    public Map<String, String> getImSendCheckConfig() {
        return imSendCheckConfig;
    }

    public void setImSendCheckConfig(Map<String, String> imSendCheckConfig) {
        this.imSendCheckConfig = imSendCheckConfig;
    }
}
