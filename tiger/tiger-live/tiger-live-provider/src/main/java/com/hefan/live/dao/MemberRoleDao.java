package com.hefan.live.dao;

import com.cat.tiger.util.CollectionUtils;
import com.hefan.common.orm.dao.CommonDaoImpl;
import com.hefan.live.bean.LiveLog;
import com.hefan.live.bean.MemberRole;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nigle on 2016/10/8.
 */
@Repository
public class MemberRoleDao extends CommonDaoImpl{

    @Resource
    JdbcTemplate jdbcTemplate;

	private static String TABLE_NAME = "member_role";
    /**
     * 是否直播间的管理员
     * @param targetAccid
     * @param chatRoomId
     * @return
     */
    public boolean findIsAdminForAnchorByUserId(String targetAccid,int chatRoomId){
        String sql = "select mr.id,mr.chat_room_id,mr.operator_accid,mr.target_accid,mr.create_time,mr.create_user " +
				" from member_role mr where mr.target_accid = ? and mr.chat_room_id = ?";
        List<MemberRole> list = jdbcTemplate.query(sql,new BeanPropertyRowMapper<MemberRole>(MemberRole.class),new Object[]{targetAccid,chatRoomId});
        return CollectionUtils.isNotEmpty(list) ? true : false;
    }

	/**
	 * 获取直播间管理员列表
	 * @param chatRoomId
	 * @return
     */
	public List<MemberRole> findRoomAdminList(int chatRoomId) {
		String sql = "select mr.id,mr.chat_room_id,mr.operator_accid,mr.target_accid,mr.create_time,mr.create_user" +
				" from member_role mr where mr.chat_room_id = ?";
		List<MemberRole> list = jdbcTemplate.query(sql,new BeanPropertyRowMapper<MemberRole>(MemberRole.class),chatRoomId);
		return CollectionUtils.isNotEmpty(list) ? list : null;
	}

	public int save(MemberRole m) {
		String sql = "insert into member_role(chat_room_id,operator_accid,target_accid,create_user) " +
				     "select ?,?,?,? from dual where not exists( select * from member_role where " +
				     "chat_room_id = ? and operator_accid = ? and target_accid = ?)";
		Object[] o = new Object[]{m.getChatRoomId(),m.getOperatorAccid(),m.getTargetAccid(),m.getCreateUser(),m.getChatRoomId(),m.getOperatorAccid(),m.getTargetAccid()};
		return jdbcTemplate.update(sql,o);
		}

	public int del(int chatRoomId,String target_accid){
		String sql = "delete from member_role where chat_room_id = ? and target_accid = ?";
		Object[] o = new Object[]{chatRoomId,target_accid};
		return jdbcTemplate.update(sql,o);
	}

	public long getAdminCount(int chatRoomId) {
		String sql = "select count(id) from member_role mr where mr.chat_room_id = ?";
		long count = jdbcTemplate.queryForObject(sql,Long.class,chatRoomId);
		return count;
	}
}
