package com.hefan.live.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.service.JedisService;
import com.hefan.common.monitor.AliyunCDNUtils;
import com.hefan.live.bean.LiveLog;
import com.hefan.live.dao.LiveMonitorDao;
import com.hefan.live.itf.LiveLogService;
import com.hefan.live.itf.LiveMonitorService;

/**
 * 直播间监控处理
 * Created by lxw on 2016/10/30.
 */
@Path("/live")
@Component("liveMonitorService")
public class LiveMonitorServiceImpl implements LiveMonitorService {

    private Logger logger = LoggerFactory.getLogger(LiveMonitorServiceImpl.class);

    @Resource
    private LiveLogService liveLogService;
    @Resource
    JedisService jedisService;
    
    @Resource
    LiveMonitorDao liveMonitorDao;

    @Override
    public ResultBean closeLiving(String userId, String chartRoomId, String liveUuid,String promptCopy) {
        ResultBean res = new ResultBean(ResultCode.SUCCESS,null);
        AliyunCDNUtils aliyunCDNUtils = new AliyunCDNUtils();
        //请求aliyun断流
        res = aliyunCDNUtils.closeLiveStreamRequest(userId,liveUuid);
        if(res.getCode() == ResultCode.SUCCESS.get_code()) { //断流成功
            //调用直播结束处理接口
        	 res = liveLogService.liveExcEnd(liveUuid,3,promptCopy);
        } else {
            return res;
        }
        return res;
    }
    @GET
    @Path("/getCurrentlyOnLiveList")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
	@Override
	public ResultBean getCurrentlyOnLiveList() {
		// TODO Auto-generated method stub
    	ResultBean res = new ResultBean(ResultCode.SUCCESS,null);
		try {
			List<LiveLog> liveList = this.liveMonitorDao.getCurrentlyOnLiveList();
			if(liveList !=null && liveList.size()>0){
				for(int i =0;i<liveList.size();i++){
					LiveLog liveLog = liveList.get(i);
					if(liveLog !=null && liveLog.getId()>0 && StringUtils.isNotBlank(liveLog.getLiveUuid())){
						//判断缓存内是否存在
						if(!jedisService.isExist("jh_"+liveLog.getLiveUuid())){
							Map<String,Object> map = new HashMap<String,Object>();
							map.put("imgNo", 1);
							map.put("imgErrNum", 0);
							jedisService.setexStr("jh_"+liveLog.getLiveUuid(), JSON.toJSONString(map), 60*60);
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("(鉴黄)在播列表加入缓存失败:"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			res.setMsg(ResultCode.UNSUCCESS.getMsg());
			return res;
		}
		return res;
	}

	@Override
	public List getHighRiskLiveList(String userId, String nickName, String startTime, String endTime) {
		// TODO Auto-generated method stub
		return this.liveMonitorDao.getHighRiskLiveList(userId, nickName, startTime, endTime);
	}
    
}
