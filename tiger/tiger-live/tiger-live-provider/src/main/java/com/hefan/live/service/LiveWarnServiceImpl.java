package com.hefan.live.service;

import com.hefan.live.bean.WarnLog;
import com.hefan.live.dao.LiveWarnDao;
import com.hefan.live.itf.LiveStaticService;
import com.hefan.live.itf.LiveWarnService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Component("liveWarnService")
public class LiveWarnServiceImpl implements LiveWarnService {

	private Logger logger = LoggerFactory.getLogger(LiveWarnServiceImpl.class);


	@Resource
	LiveWarnDao liveWarnDao;

	@Resource
	LiveStaticService liveStaticService;

	@Override
	public int saveWarnLog(WarnLog warnLog) {
		// TODO Auto-generated method stub
		return this.liveWarnDao.saveWarnLog(warnLog);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int updateLiveHighStatus(String userId,String liveUuid,int risk,String optUserId) {
		// TODO Auto-generated method stub
		int rs = this.liveWarnDao.updateLiveHighStatus(userId,risk);
		if(rs >0){
			this.liveWarnDao.saveLiveHighStatusLog(userId, liveUuid, risk, optUserId);
		}
		return rs;
	}

	@Override
	public List getPunishTypeList() {
		// TODO Auto-generated method stub
		return this.liveWarnDao.getPunishTypeList();
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int dealPunishDetal(String userId, String optUserId,Map map, String punishReason,
							   String illegalPic,String liveUuid,int punishTypeFromTb, int punishDataType) {
		// TODO Auto-generated method stub
		//添加处罚记录
		//获取用户上级id【pid】
		int pid = liveWarnDao.getUserPid(userId);
		int ur = this.liveWarnDao.dealPunishDetalL(userId,optUserId,map,punishReason,illegalPic,liveUuid,punishTypeFromTb,punishDataType,pid);
		if(ur <=0){
			return 0;
		}
		//冻结天数
		int freezeDays = Integer.parseInt(map.get("freeze_days").toString());
		if(freezeDays>0){
			this.liveWarnDao.freezeUser(userId);
			//运营后台踢出
			try {
				liveStaticService.loginOutForBack(userId);
			} catch (Exception e) {
				logger.info("运营后台踢出消息发送失败");
				e.printStackTrace();
			}
		}
		//停播天数
		int stopDays = Integer.parseInt(map.get("stop_days").toString());
		if(stopDays>0){
			this.liveWarnDao.stopLiveRoom(userId, stopDays);
		}
		if(freezeDays>0 || stopDays>0){
			//增加任务
			int tr =0;
			Map tpMap = this.liveWarnDao.getTaskPunish(userId);
			if(tpMap == null || tpMap.isEmpty()){
				//新增
				tr = this.liveWarnDao.insertTaskPunish(userId, freezeDays, stopDays);
			}else{
				//修改
				tr = this.liveWarnDao.updateTaskPunish(userId, freezeDays, stopDays);
			}
			if(tr<=0){
				//任务出错，回滚
				throw new RuntimeException("处罚定时任务添加失败");
			}
		}
		return ur;
	}


	public Map getPunishTypeById(long typeId,int punishTypeFromTb){
		return this.liveWarnDao.getPunishTypeByIdAndPunishTypeFromTb(typeId,punishTypeFromTb);
	}
	
}
