package com.hefan.live.dao;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hefan.common.orm.dao.CommonDaoImpl;
import com.hefan.common.util.DynamicProperties;

/**
 * 直播预告
 * @author sagagyq
 *
 */
@Repository
public class LiveNoticeDao extends CommonDaoImpl{
	private static String TABLE_NAME = "live_notice";

    @Resource
    JdbcTemplate jdbcTemplate;
    
    public Map<String,Object> getNewLiveNotice(){
    	StringBuilder sql = new StringBuilder();
    	sql.append(" SELECT ");
    	sql.append(" ln.pic pic,");
    	sql.append(" '"+DynamicProperties.getString("liveNotice.url")+"' url");
    	sql.append(" FROM "+TABLE_NAME+" ln");
    	sql.append(" WHERE ln.open_time>NOW() ");
    	sql.append(" AND ln.state = 1 ");
    	sql.append(" AND ln.is_del = 0 ");
    	sql.append(" ORDER BY ln.open_time ");
    	sql.append(" LIMIT 0,1");
    	List<Map<String,Object>> list= jdbcTemplate.queryForList(sql.toString());
    	if(list!=null && list.size()>0){
    		return list.get(0);
    	}
    	return null;
    }
    
}
