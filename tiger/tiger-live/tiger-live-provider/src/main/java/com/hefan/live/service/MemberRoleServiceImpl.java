package com.hefan.live.service;

import com.cat.tiger.util.DateUtils;
import com.hefan.live.bean.LiveReturnVo;
import com.hefan.live.bean.MemberRole;
import com.hefan.live.dao.MemberRoleDao;
import com.hefan.live.itf.MemberRoleService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.WebUserService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nigle on 2016/10/8.
 */
@Component("memberRoleService")
public class MemberRoleServiceImpl implements MemberRoleService {

    @Resource
    MemberRoleDao memberRoleDao;
	@Resource
	WebUserService webUserService;

	/**
	 * 添加直播间管理员
	 * @param memberRole
	 * @return
     */
	@Override
	public int save(MemberRole memberRole) {
		return memberRoleDao.save(memberRole);
	}

	/**
	 * 删除直播间管理员
	 * @param chatRoomId
	 * @param userId
     * @return
     */
	@Override
	public int del(int chatRoomId, String userId) {
		return memberRoleDao.del(chatRoomId,userId);
	}

	@Override
    @Transactional(readOnly=true)
    public boolean findIsAdminForAnchorByUserId(String optUserId, int chatRoomId) {
        return memberRoleDao.findIsAdminForAnchorByUserId(optUserId,chatRoomId);
    }

	/**
	 * 获取直播间管理员列表
	 * @param chatRoomId
	 * @return
     */
	@Override
	@Transactional(readOnly=true)
	public List<MemberRole> findRoomAdminList(int chatRoomId) {
		return memberRoleDao.findRoomAdminList(chatRoomId);
	}

	/**
	 * 获取直播间管理员信息列表
	 * @param listMem
	 * @return
	 */
	@Override
	@Transactional(readOnly=true)
	public List<LiveReturnVo> findAdminInfo(List<MemberRole> listMem) {

		List<LiveReturnVo> list = new ArrayList<LiveReturnVo>();
		for (MemberRole mem : listMem ) {
			WebUser user = webUserService.findMyUserInfoFromCach(mem.getTargetAccid());
			//如果不存在或者用户被删除，则不显示 add by 王超 20170106
			if (user != null) {
				LiveReturnVo vo = new LiveReturnVo(DateUtils.date2SimpleStr(user.getBirthday()), user.getHeadImg(), String.valueOf(user.getUserLevel()), user.getWatcherCount(), user.getNickName(),
						user.getSex(), user.getPersonSign(), user.getFansCount(), user.getUserId(), user.getAuthInfo(), user.getGpsLocation(), user.getPayCount().intValue(), user.getUserType());
				list.add(vo);
			}
		}
		return list;
	}

	@Override
	public long getAdminCount(int chatRoomId) {

		return memberRoleDao.getAdminCount(chatRoomId);
	}
}
