package com.hefan.live.service;

import java.util.List;
import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.cat.tiger.service.JedisService;
import com.hefan.live.bean.LivingRoomInfoVo;
import com.hefan.live.itf.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.live.bean.LiveLog;
import com.hefan.live.bean.LiveRoom;

/**
 * 直播间用户人数定时推送任务
 * 
 * @author kevin_zhang
 *
 */
@Path("/live")
@Component("livingRoomMonitorService")
public class LivingRoomMonitorServiceImpl implements LivingRoomMonitorService {
	Logger logger = LoggerFactory.getLogger(LivingRoomMonitorServiceImpl.class);

	@Resource
    JedisService jedisService;
	@Resource
	LiveRoomService liveRoomService;
	@Resource
	LiveLogService liveLogService;
	@Resource
	LiveImOptService liveImOptService;
	@Resource
	LivingRedisOptService livingRedisOptService;
	@Resource
	RoomEnterExitOptService roomEnterExitOptService;

	/**
	 * 直播间用户端定时监听
	 */
	@SuppressWarnings("rawtypes")
	@GET
	@Path("/livingRoomMonitor")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@Override
	public void livingRoomMonitorOpreate() {
		logger.info("直播间用户端监听任务－－start");
		try {
			List<LivingRoomInfoVo> livingRoomList = roomEnterExitOptService.getAllLivingRoomInfoList();
			if (null != livingRoomList && livingRoomList.size() > 0) {
				for (LivingRoomInfoVo liveRoom : livingRoomList) {
					try {
						if (StringUtils.isBlank(liveRoom.getLiveUuid())) {
							logger.info("直播间用户端监听任务－－直播间uuid获取失败");
						} else {
							logger.info("直播间用户端监听任务－－直播间：主播user_id=" + liveRoom.getUserId());
							long watchNum = roomEnterExitOptService.getLiveRoomPeoleCount(liveRoom.getUserId());
							try {
								String addNUm = livingRedisOptService.getAddNumForRoom(liveRoom.getUserId());
								if (null != addNUm && Long.valueOf(addNUm) >= 0) {
									watchNum += Long.valueOf(addNUm);
									logger.info("{}加入了{}额外人数", liveRoom.getUserId(), addNUm);
								}
							} catch (NumberFormatException e) {
								e.printStackTrace();
							}
							if (watchNum >= 20) {
								ResultBean resultBean = liveImOptService.updateOnlineUserNumIm(liveRoom.getChatRoomId(),
										liveRoom.getLiveUuid(), liveRoom.getUserId(), watchNum);
								if (resultBean.getCode() == ResultCode.SUCCESS.get_code()) {
									logger.info("直播间用户端监听任务－－在线人数更新IM发送成功");
								}
							} else {
								logger.info("直播间用户端监听任务－－在线人数不超过20人");
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						logger.info("直播间用户端监听任务－－直播间用户端监听任务执行失败：" + e);
					}
				}
			} else {
				logger.info("直播间用户端监听任务－－执行失败：暂无在线直播数据");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("直播间用户端监听任务－－执行失败：" + e);
		} finally {
			logger.info("直播间用户端监听任务－－end");
		}
	}
}