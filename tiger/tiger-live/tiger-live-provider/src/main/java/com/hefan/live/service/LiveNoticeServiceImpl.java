package com.hefan.live.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.hefan.common.util.MapUtils;
import com.hefan.live.dao.LiveNoticeDao;
import com.hefan.live.itf.LiveNoticeService;

@Component("liveNoticeService")
public class LiveNoticeServiceImpl implements LiveNoticeService{
	
	@Resource
	private LiveNoticeDao liveNoticeDao;
	
	@Override
	public Map<String, Object> liveNotice() {return liveNoticeDao.getNewLiveNotice();}

	@Override
	public List<Map<String, Object>> livingList(List<String> ids) {
		StringBuilder sql =getBaseSql();
		sql.append(" AND wu.user_id IN( ");
		for(int i=0; i<ids.size();i++){
			sql.append(ids.get(i));
			if(i!=ids.size()-1){
				sql.append(",");
			}
		}
		sql.append(" )");
		sql.append(" ORDER BY ll.start_time DESC");
		System.out.println(sql.toString());
		return liveNoticeDao.queryMap(sql.toString());
	}
	
	/***
	 * 查询正在直播的原始sql
	 * @return
	 */
	private StringBuilder getBaseSql(){
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT ");
		sql.append(" wu.user_id id, ");
		sql.append(" wu.nick_name name, ");
		sql.append(" lr.type type, ");
		sql.append(" lr.location location, ");
		sql.append(" lr.chat_room_id chatRoomId, ");
		sql.append(" lr.live_img liveImg, ");
		sql.append(" lr.live_name personSign, ");
		sql.append(" wu.head_img headImg, ");
		sql.append(" ll.pull_url liveUrl, ");
		sql.append(" ll.live_uuid liveUuid, ");
		sql.append(" ll.display_graph displayGraph ");
		sql.append(" FROM ");
		sql.append(" live_room lr, ");
		sql.append(" web_user wu , ");
		sql.append(" live_log ll ");
		sql.append(" WHERE ");
		sql.append(" lr.user_id = wu.user_id ");
		sql.append(" AND ll.user_id = lr.user_id ");
		sql.append(" AND lr.status=1 ");
		sql.append(" AND lr.stuff=0 ");
		sql.append(" AND ll.end_time IS NULL ");
		return sql;
	}

}
