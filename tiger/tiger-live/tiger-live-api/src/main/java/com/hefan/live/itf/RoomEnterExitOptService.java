package com.hefan.live.itf;

import com.cat.common.entity.ResultBean;
import com.hefan.live.bean.LiveRoomPersonVo;
import com.hefan.live.bean.LivingRoomInfoVo;

import java.util.List;

/**
 * 用户进出直播间操作
 */
public interface RoomEnterExitOptService {

    /**
     * 检查用户能否进入直播间
     */
    List enterLiveRoomCheck(String userId);

    /**
     * 用户进入直播间
     *
     * @return
     */
    ResultBean enterRoom(LiveRoomPersonVo user, String liveUuid, int chatRoomId, String authId, String deviceToken);

    /**
     * 用户进入直播间队列消息发送
     */
    void sendEnterRoomMsgQueue(String anchId, int chatRoomId, String liveUuid, String userId, int userType,
                               int userLevel, String nickName, String headImg);

    /**
     * 用户离开直播间
     */
    ResultBean exitRoom(String userId, int chatRoomId, String liveUuid, String authId);

    /**
     * 用户离开直播间队列消息发送
     */
    void sendExitRoomMsgQueue(String authoruserId, int chatRoomId, String liveUuid, String userId);

    /**
     * 获取直播间实际观看人数
     */
    long getLiveRoomPeoleCount(String anchId);

    /**
     * 获取用户列表分页信息
     */
    List getOnlineUserlist(String authId, int pageNum, int pageSize);

    /**
     * 获取所有可显示的正在直播的直播间列表信息
     */
    List<LivingRoomInfoVo> getAllLivingRoomInfoList();

    /**
     * 获取所有正在直播的VIP直播间列表信息
     */
    List<LivingRoomInfoVo> getALLVIPLivingRoomInfoList();

    /**
     * 获取所有正在直播的直播间列表信息
     */
    List<LivingRoomInfoVo> getALLMonitorLivingRoomInfoList();

    /**
     * 获取指定直播间信息
     */
    LivingRoomInfoVo getLivingRoomInfoByAuthId(String anchId);

    /**
     * 设置指定直播间信息
     */
    ResultBean setLivingRoomInfo(LivingRoomInfoVo livingRoomInfoVo);

    /**
     * 发送更新直播间用户列表消息
     */
    ResultBean sendUpdateUserListIM(int chatRoomId, String liveUuid, String authoruserId);

    /**
     * 开关播清除数据
     */
    void endLiveClear(String anchId);
}
