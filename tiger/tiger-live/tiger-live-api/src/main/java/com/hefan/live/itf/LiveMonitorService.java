package com.hefan.live.itf;

import java.util.List;

import com.cat.common.entity.ResultBean;

/**
 * Created by lxw on 2016/10/30.
 */
public interface LiveMonitorService {

    /**
     * 直播断流处理
     * @param userId
     * @param chartRoomId
     * @param liveUuid
     * @return
     */
    ResultBean closeLiving(String userId, String chartRoomId, String liveUuid,String promptCopy);

    /**
     * 鉴黄-取在直播列表加入缓存
     * @Title: getCurrentlyOnLiveList   
     * @Description: TODO(这里用一句话描述这个方法的作用)   
     * @return      
     * @return: ResultBean
     * @author: LiTeng      
     * @throws 
     * @date:   2016年11月21日 下午4:03:18
     */
    public ResultBean getCurrentlyOnLiveList();

    /**
     *  获取高危视频库
     * @Title: getHighRiskLiveList   
     * @Description: TODO(这里用一句话描述这个方法的作用)   
     * @param userId
     * @param nickName
     * @param startTime
     * @param endTime      
     * @return: void
     * @author: LiTeng      
     * @throws 
     * @date:   2016年11月24日 下午2:41:01
     */
    public List getHighRiskLiveList(String userId,String nickName,String startTime,String endTime);
}
