package com.hefan.live.bean;

import java.io.Serializable;

/**
 * Created by nigle on 2016/10/10.
 */
public class LiveReturnVo implements Serializable{
    String birthday;//生日
    String headImg;//头像
    String level;//等级
    long watcherCount;//关注
    String nickName;//昵称
    int sex;//性别
    String personSign;//个性签名
    long fansCount;//粉丝数
    String userId;
    String authInfo;//认证信息
    String gpsLocation;//实时定位地址
    long payCount;//贡献数

    int isWatched;//是否关注 0-未关注 非0-已关注
    int isAdmin;//是否管理员 0-不是 非0-是
    int userType;//主播类型 0:普通 1:网红2:明星3:片场
    long ticketFact;//盒饭数
    int isShutup;//是否被禁言 0-不是 非0-是 int型",
    int selfIsAdmin;//查看人是否是管理员（直播间主播本人不算管理员） 0-不是 非0-是
    int relation;//0关注1互粉2未关注

    /**
     * 管理员列表信息
     */
    public LiveReturnVo(String birthday, String headImg, String userLevel, long watcherCount, String nickName, int sex, String personSign, long fansCount, String userId, String authInfo, String gpsLocation, long payCount, int userType) {
        this.birthday = birthday;
        this.headImg = headImg;
        this.level = userLevel;
        this.watcherCount = watcherCount;
        this.nickName = nickName;
        this.sex = sex;
        this.personSign = personSign;
        this.fansCount = fansCount;
        this.userId = userId;
        this.authInfo = authInfo;
        this.gpsLocation = gpsLocation;
        this.payCount = payCount;
        this.userType = userType;
    }
    /**
     * 直播间查看用户信息
     */
    public LiveReturnVo(String birthday, String headImg, String userLevel, long watcherCount, String nickName, int sex, String personSign, long fansCount, String userId, String authInfo, String gpsLocation, long payCount, int isWatched, int isAdmin, int userType, long ticketFact, int isShutup, int selfIsAdmin) {
        this.birthday = birthday;
        this.headImg = headImg;
        this.level = userLevel;
        this.watcherCount = watcherCount;
        this.nickName = nickName;
        this.sex = sex;
        this.personSign = personSign;
        this.fansCount = fansCount;
        this.userId = userId;
        this.authInfo = authInfo;
        this.gpsLocation = gpsLocation;
        this.payCount = payCount;
        this.isWatched = isWatched;
        this.isAdmin = isAdmin;
        this.userType = userType;
        this.ticketFact = ticketFact;
        this.isShutup = isShutup;
        this.selfIsAdmin = selfIsAdmin;
    }

    public LiveReturnVo() {
    }

    public int getRelation() {
        return relation;
    }

    public void setRelation(int relation) {
        this.relation = relation;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public long getPayCount() {
        return payCount;
    }

    public void setPayCount(long payCount) {
        this.payCount = payCount;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getAuthInfo() {
        return authInfo;
    }

    public void setAuthInfo(String authInfo) {
        this.authInfo = authInfo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getFansCount() {
        return fansCount;
    }

    public void setFansCount(long fansCount) {
        this.fansCount = fansCount;
    }

    public String getpersonSign() {
        return personSign;
    }

    public void setpersonSign(String personSign) {
        this.personSign = personSign;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public long getWatcherCount() {
        return watcherCount;
    }

    public void setWatcherCount(long watcherCount) {
        this.watcherCount = watcherCount;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPersonSign() {
        return personSign;
    }

    public void setPersonSign(String personSign) {
        this.personSign = personSign;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getIsWatched() {
        return isWatched;
    }

    public void setIsWatched(int isWatched) {
        this.isWatched = isWatched;
    }

    public int getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(int isAdmin) {
        this.isAdmin = isAdmin;
    }

    public long getTicketFact() {
        return ticketFact;
    }

    public void setTicketFact(long ticketFact) {
        this.ticketFact = ticketFact;
    }

    public int getIsShutup() {
        return isShutup;
    }

    public void setIsShutup(int isShutup) {
        this.isShutup = isShutup;
    }

    public int getSelfIsAdmin() {
        return selfIsAdmin;
    }

    public void setSelfIsAdmin(int selfIsAdmin) {
        this.selfIsAdmin = selfIsAdmin;
    }
}
