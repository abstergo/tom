package com.hefan.live.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by nigle on 2016/9/26.
 */
public class LiveRoom implements Serializable {

	private long id;

	private String userId;

	private int chatRoomId;// 聊天室ID

	private String liveName;// 直播房间描述

	private String authInfo;// 认证信息

	private String peopleCount;

	private int type;// web_user 的 user_type

	private String personSign;// 个性签名/直播间描述--废弃

	private long ticketHistory;// 历史总计饭票数

	private long ticketFact;// 实际饭票数--废弃

	private String liveImg;// 直播间封面图片地址

	private String location;// 定位地址

	private int status;// 直播状态0:正常 1:正在直播

	private int stuff;// 禁止状态0:正常1:禁止

	private String liveDomain;// 推流地址域名

	private String pullDomain;// 拉流地址域名

	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;// 最近开播时间

	private String nickName;// 不实例化到数据库

	private String liveUuid;// 最新一次直播 uuid

	public String pullM3u8;// 不实例化到数据库

	public String reM3u8;// 不实例化到数据库

	public String getLiveUuid() {
		return liveUuid;
	}

	public void setLiveUuid(String liveUuid) {
		this.liveUuid = liveUuid;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPullM3u8() {
		return pullM3u8;
	}

	public void setPullM3u8(String pullM3u8) {
		this.pullM3u8 = pullM3u8;
	}

	public String getReM3u8() {
		return reM3u8;
	}

	public void setReM3u8(String reM3u8) {
		this.reM3u8 = reM3u8;
	}

	public String getNickName() {
		return nickName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getChatRoomId() {
		return chatRoomId;
	}

	public void setChatRoomId(int chatRoomId) {
		this.chatRoomId = chatRoomId;
	}

	public String getLiveName() {
		return liveName;
	}

	public void setLiveName(String liveName) {
		this.liveName = liveName;
	}

	public String getAuthInfo() {
		return authInfo;
	}

	public void setAuthInfo(String authInfo) {
		this.authInfo = authInfo;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getPersonSign() {
		return personSign;
	}

	public void setPersonSign(String personSign) {
		this.personSign = personSign;
	}

	public long getTicketHistory() {
		return ticketHistory;
	}

	public void setTicketHistory(long ticketHistory) {
		this.ticketHistory = ticketHistory;
	}

	public String getLiveImg() {
		return liveImg;
	}

	public void setLiveImg(String liveImg) {
		this.liveImg = liveImg;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getStuff() {
		return stuff;
	}

	public void setStuff(int stuff) {
		this.stuff = stuff;
	}

	public String getLiveDomain() {
		return liveDomain;
	}

	public void setLiveDomain(String liveDomain) {
		this.liveDomain = liveDomain;
	}

	public String getPullDomain() {
		return pullDomain;
	}

	public void setPullDomain(String pullDomain) {
		this.pullDomain = pullDomain;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public long getTicketFact() {
		return ticketFact;
	}

	public void setTicketFact(long ticketFact) {
		this.ticketFact = ticketFact;
	}
	public String getPeopleCount() {
		return peopleCount;
	}

	public void setPeopleCount(String peopleCount) {
		this.peopleCount = peopleCount;
	}

}
