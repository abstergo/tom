package com.hefan.live.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by nigle on 2016/9/26.
 */
public class LiveRoomVo implements Serializable {

	private String userId="";
	private String nickName="";
	private int chatRoomId;
	private String liveUuid="";
	private int userType;
	private String livingPic="";
	private int userHStatus;
	private String pullUrl="";
	private List picList;
	private int isVipLive;//VIP直播 0:不是  1:是
	private int isShow;//前端是否显示 0：不显示  1:显示
	private int rotate;
	private int doubleSrc;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date startTime;// 最近开播时间
	private long peopleCount=0;// 观看人数


	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}


	public String getPullUrl() {
		return pullUrl;
	}

	public void setPullUrl(String pullUrl) {
		this.pullUrl = pullUrl;
	}

	public List getPicList() {
		return picList;
	}

	public void setPicList(List picList) {
		this.picList = picList;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public int getChatRoomId() {
		return chatRoomId;
	}

	public void setChatRoomId(int chatRoomId) {
		this.chatRoomId = chatRoomId;
	}

	public String getLiveUuid() {
		return liveUuid;
	}

	public void setLiveUuid(String liveUuid) {
		this.liveUuid = liveUuid;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getLivingPic() {
		return livingPic;
	}

	public void setLivingPic(String livingPic) {
		this.livingPic = livingPic;
	}

	public int getUserHStatus() {
		return userHStatus;
	}

	public void setUserHStatus(int userHStatus) {
		this.userHStatus = userHStatus;
	}

	public int getIsVipLive() {
		return isVipLive;
	}

	public void setIsVipLive(int isVipLive) {
		this.isVipLive = isVipLive;
	}

	public int getIsShow() {
		return isShow;
	}

	public void setIsShow(int isShow) {
		this.isShow = isShow;
	}

	public int getRotate() {
		return rotate;
	}

	public void setRotate(int rotate) {
		this.rotate = rotate;
	}

	public int getDoubleSrc() {
		return doubleSrc;
	}

	public void setDoubleSrc(int doubleSrc) {
		this.doubleSrc = doubleSrc;
	}

	public long getPeopleCount() {
		return peopleCount;
	}

	public void setPeopleCount(long peopleCount) {
		this.peopleCount = peopleCount;
	}
}
