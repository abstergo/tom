package com.hefan.live.common;

/**
 * Created by nigle on 2016/10/22.
 */
public interface Constants {

    //直播间机器人数redieskey：robot_+liveUuid
    String ROBOT_KEY = "robot_";

    //直播间初始加入的机器人数值
    int ROBOT_COUNT_START = 3;
    int ROBOT_COUNT_END = 6;

    //机器人库存警戒值
    long ROBOT_WARNING_NUM = 100;

    //直播间离开一个真实用户移除的机器人数量
    int ROBOT_REMOVE_COUNT_START = 2;
    int ROBOT_REMOVE_COUNT_END = 3;

    //直播间进入一个真实用户增加的机器人数量
    int ROBOT_ADD_COUNT = 3;

    //机器人数最大占比18~25+1~3X+5X=观众人数
    int ROBOT_MAX_X = 4;
    int ROBOT_MIN_X = 2;

    //后台添加或减少额外机器人的速度，N/min
    int ROBOT_BACK_COUNT_MIN = 100;
}
