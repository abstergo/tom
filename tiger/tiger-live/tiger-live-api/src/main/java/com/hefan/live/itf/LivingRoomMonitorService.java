package com.hefan.live.itf;

/**
 * 直播间用户端定时监听
 * 
 * @author kevin_zhang
 *
 */
public interface LivingRoomMonitorService {

	/**
	 * 定时监听直播间
	 */
	public void livingRoomMonitorOpreate();
}
