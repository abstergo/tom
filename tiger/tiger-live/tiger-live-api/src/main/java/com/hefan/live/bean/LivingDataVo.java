package com.hefan.live.bean;

import java.io.Serializable;
/**
 * 直播列表
 * @author sagagyq
 *
 */
@SuppressWarnings("serial")
public class LivingDataVo implements Serializable  {
	private String headImg;
	private String liveUuid;
	private String name;
	private int chatRoomId;
	private String liveUrl;
	private String id;
	private int type;
	private String liveImg;
	private String displayGraph;
	private String personSign;
	private String location;
	private int peopleCount;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getPeopleCount() {
		return peopleCount;
	}
	public void setPeopleCount(int peopleCount) {
		this.peopleCount = peopleCount;
	}
	public String getHeadImg() {
		return headImg;
	}
	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}
	public String getLiveUuid() {
		return liveUuid;
	}
	public void setLiveUuid(String liveUuid) {
		this.liveUuid = liveUuid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getChatRoomId() {
		return chatRoomId;
	}
	public void setChatRoomId(int chatRoomId) {
		this.chatRoomId = chatRoomId;
	}
	public String getLiveUrl() {
		return liveUrl;
	}
	public void setLiveUrl(String liveUrl) {
		this.liveUrl = liveUrl;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getLiveImg() {
		return liveImg;
	}
	public void setLiveImg(String liveImg) {
		this.liveImg = liveImg;
	}
	public String getDisplayGraph() {
		return displayGraph;
	}
	public void setDisplayGraph(String displayGraph) {
		this.displayGraph = displayGraph;
	}
	public String getPersonSign() {
		return personSign;
	}
	public void setPersonSign(String personSign) {
		this.personSign = personSign;
	}
}
