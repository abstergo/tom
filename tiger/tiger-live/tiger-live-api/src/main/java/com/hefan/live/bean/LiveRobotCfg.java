package com.hefan.live.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by nigle on 2016/10/27.
 */
public class LiveRobotCfg implements Serializable{
    private long id;
    private String userId;
    private int chatRoomId;
    private String liveUuid;
    private int robotNum;//本次增加机器数量',
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date createTime;// '添加时间
    private String create_user;//'操作人',
    private int status;//0 新增未处理 1 已处理完成
    private int executeCount;//根据机器人添加速度得到的执行次数，每分钟一次，第一次执行给最大值，之后依次递减
    private int type;//0增加1减少
    private String info;//备注

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getExecuteCount() {
        return executeCount;
    }

    public void setExecuteCount(int executeCount) {
        this.executeCount = executeCount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getChatRoomId() {
        return chatRoomId;
    }

    public void setChatRoomId(int chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public int getRobotNum() {
        return robotNum;
    }

    public void setRobotNum(int robotNum) {
        this.robotNum = robotNum;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreate_user() {
        return create_user;
    }

    public void setCreate_user(String create_user) {
        this.create_user = create_user;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
