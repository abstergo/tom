package com.hefan.live.itf;

/**
 * 直播间心跳监听任务
 * 
 * @author kevin_zhang
 *
 */
public interface LivingHeartBeatMonitorService {

	/**
	 * 定时监听直播间
	 */
	public void livingHeartBeatMonitorOpreate();

}
