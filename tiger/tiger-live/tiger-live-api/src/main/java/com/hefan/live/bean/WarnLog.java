package com.hefan.live.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "warn_log")
public class WarnLog implements Serializable {
	
	/**   
	 * @Description: serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 * @author: LiTeng
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

    /**
     * 用户ID
     * @author: LiTeng
     */
    @Column("user_id")
    private  String userId; 
    
    @Column("create_time")
    private Date createTime;
    
    @Column("remark")
    private String remark;
    
    @Column("chat_room_id")
    private Integer chatRoomId;
    
    @Column("live_uuid")
    private String liveUuid;
    
    @Column("opt_user_id")
    private String optUserId;

	@Column("warn_content")
	private String warnContent;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId == null?"":userId;
	}
	
	public void setUserId(String userId) {
		this.userId = (userId==null?"":userId);
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getRemark() {
		return remark == null?"":remark;
	}

	public void setRemark(String remark) {
		this.remark = (remark==null?"":remark);
	}

	public Integer getChatRoomId() {
		return chatRoomId == null?0:chatRoomId;
	}

	public void setChatRoomId(Integer chatRoomId) {
		this.chatRoomId = (chatRoomId== null?0:chatRoomId);
	}

	public String getLiveUuid() {
		return liveUuid ==null?"":liveUuid;
	}

	public void setLiveUuid(String liveUuid) {
		this.liveUuid = (liveUuid==null?"":liveUuid);
	}

	public String getOptUserId() {
		return optUserId ==null?"":optUserId;
	}

	public void setOptUserId(String optUserId) {
		this.optUserId = (optUserId==null?"":optUserId);
	}

	public String getWarnContent() {
		return warnContent;
	}

	public void setWarnContent(String warnContent) {
		this.warnContent = warnContent;
	}
}
