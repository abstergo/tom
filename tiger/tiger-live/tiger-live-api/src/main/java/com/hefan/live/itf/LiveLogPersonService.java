package com.hefan.live.itf;

import java.util.List;
import java.util.Map;

/**
 * 直播间用户处理
 * @author kevin_zhang
 */
public interface LiveLogPersonService {

	/**
	 * 从redis获取直播间观看人数
	 * @description 从liveUUid 直播间随机获取 num人，且不包含userId
	 * @param authId 主播id
	 * @param num
	 * @param userId
	 * @return
	 * @throws
	 * @author wangchao
	 * @create 2016/11/30 11:07
	 */
  List<Map<String,String>> getIdsFromRedisForLive(String authId, int num, String userId);
}
