package com.hefan.live.itf;

import java.util.List;
import java.util.Map;

public interface LiveNoticeService {
	/**
	 * 直播预告
	 * @return
	 */
	public Map<String,Object> liveNotice();
	
	/**
	 * 我关注的直播
	 * @param ids
	 * @return
	 */
	public List<Map<String, Object>> livingList(List<String> ids);
}
