package com.hefan.live.itf;

import com.cat.common.entity.ResultBean;
import com.hefan.live.bean.LiveRoomPersonVo;
import com.hefan.user.bean.WebUser;

/**
 * 直播间IM自定义消息发送处理
 */
public interface LiveImOptService {

	/**
	 * 禁言IM消息处理
	 * @param optUser 操作者
	 * @param user 被操作者
	 */
	boolean liveShutUpIm(WebUser optUser, WebUser user, int chatRoomId, String liveUuid, String superAdmin);

	/**
	 * 踢出IM消息处理
	 * @param optUser 操作者
	 * @param user 被操作者
	 */
	void liveOutIm(WebUser optUser, WebUser user, int chatRoomId, String liveUuid);

	/**
	 * 设置和取消管理员
	 * @param optUser 操作者
	 * @param user 被操作者
	 * @param action 动作：SET 设置管理员 UNSET 取消管理医院
	 */
	@SuppressWarnings("rawtypes")
	ResultBean liveOptAdmin(WebUser optUser, WebUser user, int chatRoomId, String liveUuid, String action);

	/**
	 * 更新直播间用户列表
	 * @param userListStr:("membersList":"")
	 */
	@SuppressWarnings("rawtypes")
	ResultBean updateUserListIm(int chatRoomId, String liveUuid, String authoruserId, String userListStr);

	/**
	 * 高等级用户进入直播间
	 */
	@SuppressWarnings("rawtypes")
	ResultBean highLevelUserIm(int chatRoomId, String liveUuid, String authoruserId, String userInfoStr);

	/**
	 * 关播时发送IM
	 */
	@SuppressWarnings("rawtypes")
	ResultBean liveEndIm(WebUser user, int chatRoomId, String liveUuid, String watchNum, String liveLength,
			String heFanNum);

	/**
	 * 直播间人数更新IM通知
	 */
	@SuppressWarnings("rawtypes")
	ResultBean updateOnlineUserNumIm(int chatRoomId, String liveUuid, String authoruserId, long watchNum);

	/**
	 * 请离出直播间，用于开播前对主播处理
	 */
	@SuppressWarnings("rawtypes")
	ResultBean liveStartClear(String userId, String liveUuid, int chatRoomId);

	/**
	 * 点亮IM消息 泡泡+文字
	 */
	@SuppressWarnings("rawtypes")
	ResultBean lightRoom(int chatRoomId, String liveUuid, LiveRoomPersonVo liveRoomPersonVo);

	/**
	 * 泡泡消息 只有泡泡
	 */
	ResultBean lightRoomOnlyPop(int chatRoomId, String liveUuid );

	/**
	 * 关注IM消息
	 */
	ResultBean watchAnchor(int chatRoomId, String liveUuid, LiveRoomPersonVo liveRoomPersonVo);

	/**
	 * 踢出登录
	 */
	@SuppressWarnings("rawtypes")
	ResultBean userLoginOut(WebUser user);

	/**
	 * 禁播发IM
	 */
	@SuppressWarnings("rawtypes")
	ResultBean liveBand(WebUser user, int chatRoomId, String liveUuid, String watchNum, String liveLength,
						String heFanNum,String promptCopy);

	/**
	 * 异步发送关闭直播间im
	 * @param user
	 * @param chatRoomId
	 * @param liveUuid
	 * @param watchNum
	 * @param liveLength
	 * @param heFanNum
	 * @return
	 */
	ResultBean liveEndImAsync(WebUser user, int chatRoomId, String liveUuid, String watchNum, String liveLength,
							  String heFanNum);
}
