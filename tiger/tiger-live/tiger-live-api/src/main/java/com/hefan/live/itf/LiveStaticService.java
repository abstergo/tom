package com.hefan.live.itf;

import com.cat.common.entity.ResultBean;
import com.cat.common.entity.ResultPojo;

/**
 * Created by nigle on 2016/10/17.
 */
public interface LiveStaticService {

    /**
     * 运营后台禁播
     * 非正常关播
     * map:{"liveUuid":"","type":"admin"}
     * 来源：“admin”表示后台关闭，“heartbeat”表示心跳失联关闭
     * @return {"result":1，"msg":"成功"}
     */
    ResultPojo liveExcepEnd(String liveUuid,String  promptCopy)throws Exception;

    /**
     * 运营后台踢出
     * @param userId
     * @return
     * @throws Exception
     */
    ResultPojo loginOutForBack(String userId)throws Exception;

    /**
     * 给直播间添加额外人数
     * @param userId
     * @return
     */
    ResultPojo addCountForLiving( String userId, long number);

    void test( String a, String b);
}
