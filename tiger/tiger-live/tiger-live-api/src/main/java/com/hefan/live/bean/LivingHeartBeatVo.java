package com.hefan.live.bean;

import java.io.Serializable;

/**
 * 心跳
 * 
 * @author kevin_zhang
 *
 */
@SuppressWarnings("serial")
public class LivingHeartBeatVo implements Serializable {
	/**
	 * 直播间聊天室id
	 */
	private int chatRoomId;
	/**
	 * 直播UUID
	 */
	private String liveUuid;
	/**
	 * 主播ID
	 */
	private String authoruserId;
	/**
	 * 心跳更新时间
	 */
	private long updateTime;

	public int getChatRoomId() {
		return chatRoomId;
	}

	public void setChatRoomId(int chatRoomId) {
		this.chatRoomId = chatRoomId;
	}

	public String getLiveUuid() {
		return liveUuid;
	}

	public void setLiveUuid(String liveUuid) {
		this.liveUuid = liveUuid;
	}

	public String getAuthoruserId() {
		return authoruserId;
	}

	public void setAuthoruserId(String authoruserId) {
		this.authoruserId = authoruserId;
	}

	public long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}
}
