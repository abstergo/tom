package com.hefan.live.itf;

import com.cat.common.entity.ResultBean;
import com.hefan.live.bean.LiveLog;
import com.hefan.live.bean.LiveRoom;
import com.hefan.live.bean.LivingRoomInfoVo;

import java.util.List;

/**
 * User: zhaoning
 * Date: 16/9/26
 * Time: 08:32
 */
public interface LiveLogService {

    LiveLog getLiveLogByUuid(String liveUuid);

    /**
     * 直播初始化数据
     * @param liveRoom
     * @param liveLog
     * @return
     */
    void liveStartOperate(LiveRoom liveRoom, LiveLog liveLog )throws Exception;

    /**
     * 下单后更新直播盒饭收入（包含虚拟）
     * ticket_count
     * false_ticket_count
     * @param liveLog
     * @return
     */
    int updateLiveLogForRebalance(LiveLog liveLog);

    /**
     * 直播关闭时处理操作
     * @param liveLog
     * @return
     */
    LiveLog liveEndOperate(LiveLog liveLog);

    /**
     * 用户进入直播间更新观看人数，直播结束时取
     *
     * @param liveUuid
     * @param chatRoomId
     * @param addNum
     * @return
     */
    int changeWatchNum(String liveUuid, int chatRoomId, int addNum);

    /**
     * 根据时间和roomId获取正在直播的记录
     * @Title: getLiveLogByLiveTime
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @param liveTime
     * @return
     * @return: LiveLog
     * @author: LiTeng
     * @throws
     * @date:   2016年10月11日 下午7:16:30
     */
    LiveLog getLiveLogByLiveTime(String roomId,String liveTime);

    /**
     * 查询正在直播的liveLog
     * @return
     */
    LiveLog getLivingLiveLog(String userId);

    /**
     * 非正常关播
     * map:{"liveUuid":"","type":1}
     * 来源：1表示后台关闭，2表示心跳失联关闭
     * @return {"result":1，"msg":"成功"}
     */
    ResultBean liveExcEnd(String liveUuid,int from,String promptCopy);

    /**
     * 关播处理主线程
     * @param liveLog
     * @returnpr
     */
    void liveEndMain(LiveLog liveLog) throws Exception;

    /**
     * 关播辅线程，发送队列消息
     * 关播处理：清理房间观众、机器人、未领红包
     * @param liveLog
     */
    void liveEndAux(LiveLog liveLog);

    /**
     * 监控端修改直播封面或直播描述
     * @param liveName 直播描述
     * @param liveImg 直播封面
     * @param liveUuid 直播uuid
     * @param anthId 主播id
     */
    ResultBean updateLiveInfoForMonitor(String liveName, String liveImg, String liveUuid, String anthId);


  /**
     * 更新VIP直播间状态
     * @param livingRoomInfoVo
     * @return
     */
    ResultBean updateVIPLiveStatus(LivingRoomInfoVo livingRoomInfoVo);

  /**
   * 查询在start和end区间关播所有主播间
   * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
   * @param start
   * @param end
   * @return
   * @throws
   * @author wangchao
   * @create 2016/12/28 15:13
   */
  List<LiveLog> getLiveLogByEndTime(String start, String end) throws Exception;
}