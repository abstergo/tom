package com.hefan.live.bean;

import com.hefan.user.bean.WebUser;

import java.io.Serializable;

/**
 * Created by nigle on 2016/12/7.
 */
public class RobotListenerVo implements Serializable {

    private String liveUuid;
    private int chatRoomId;
    private String anchId;
    private int inOrOut;//1,进入；0，离开
    private int from;//来源渠道，0，正常，1，后台任务添加 ---------------废弃20170112
    private LiveRoomPersonVo liveRoomPersonVo;//机器人点亮时发Im消息所用参数：userId、headImg、userType、userLevel、nickName
    private String info;//备注机器人任务信息

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public int getChatRoomId() {
        return chatRoomId;
    }

    public void setChatRoomId(int chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getAnchId() {
        return anchId;
    }

    public void setAnchId(String anchId) {
        this.anchId = anchId;
    }

    public int getInOrOut() {
        return inOrOut;
    }

    public void setInOrOut(int inOrOut) {
        this.inOrOut = inOrOut;
    }

    public LiveRoomPersonVo getLiveRoomPersonVo() {
        return liveRoomPersonVo;
    }

    public void setLiveRoomPersonVo(LiveRoomPersonVo liveRoomPersonVo) {
        this.liveRoomPersonVo = liveRoomPersonVo;
    }

}
