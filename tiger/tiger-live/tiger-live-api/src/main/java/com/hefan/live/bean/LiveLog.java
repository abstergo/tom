package com.hefan.live.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by nigle on 2016/9/26.
 */
public class LiveLog implements Serializable {

    private long id;

    private String userId;

    private int chatRoomId;//聊天室ID
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    private String liveUuid;//一次直播的uuid

    private long ticketCount;//本次直播实际总盒饭数（真盒饭）

    private long falseTicketCount;//本次直播实际总盒饭数（虚拟盒饭）

    private long watchNum;//观看人数

    private int handleStatus;//0 未做统计处理 1 统计完成

    private long liveLength;//直播时长(单位秒)

    private long validLiveLength;//有效直播时长(单位秒)

    private String location;//定位地址

    private String liveUrl;//直播推流实际地址

    private String displayGraph;//直播中直播间显示图形标识

    private int abnormal_end;//1：表示异常关闭补全的endtime，否则是0

    private String pullUrl;//拉流地址

    private String liveName;//本次直播的房间描述

    private int optUser;//异常关播的操作者，1表示后台关闭，2表示心跳失联关闭

    private String liveImg;//直播封面

    private int isVipLive = 0;//VIP直播 0:不是  1:是

    private int isShow = 1;//前端是否显示 0：不显示  1:显示

    private int isLandscape = 0;//是否是横屏直播 0：竖屏  1:横屏

    public String getLiveImg() {
        return liveImg;
    }

    public void setLiveImg(String liveImg) {
        this.liveImg = liveImg;
    }

    public String getPullUrl() {
        return pullUrl;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getChatRoomId() {
        return chatRoomId;
    }

    public void setChatRoomId(int chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public long getTicketCount() {
        return ticketCount;
    }

    public void setTicketCount(long ticketCount) {
        this.ticketCount = ticketCount;
    }

    public long getFalseTicketCount() {
        return falseTicketCount;
    }

    public void setFalseTicketCount(long falseTicketCount) {
        this.falseTicketCount = falseTicketCount;
    }

    public long getWatchNum() {
        return watchNum;
    }

    public void setWatchNum(long watchNum) {
        this.watchNum = watchNum;
    }

    public int getHandleStatus() {
        return handleStatus;
    }

    public void setHandleStatus(int handleStatus) {
        this.handleStatus = handleStatus;
    }

    public long getLiveLength() {
        return liveLength;
    }

    public void setLiveLength(long liveLength) {
        this.liveLength = liveLength;
    }

    public long getValidLiveLength() {
        return validLiveLength;
    }

    public void setValidLiveLength(long validLiveLength) {
        this.validLiveLength = validLiveLength;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public String getDisplayGraph() {
        return displayGraph;
    }

    public void setDisplayGraph(String displayGraph) {
        this.displayGraph = displayGraph;
    }

    public int getAbnormal_end() {
        return abnormal_end;
    }

    public void setAbnormal_end(int abnormal_end) {
        this.abnormal_end = abnormal_end;
    }

    public void setPullUrl(String pullUrl) {
        this.pullUrl = pullUrl;
    }

    public String getLiveName() {
        return liveName;
    }

    public void setLiveName(String liveName) {
        this.liveName = liveName;
    }

    public int getOptUser() {
        return optUser;
    }

    public void setOptUser(int optUser) {
        this.optUser = optUser;
    }

    public int getIsVipLive() {
        return isVipLive;
    }

    public void setIsVipLive(int isVipLive) {
        this.isVipLive = isVipLive;
    }

    public int getIsShow() {
        return isShow;
    }

    public void setIsShow(int isShow) {
        this.isShow = isShow;
    }

    public int getIsLandscape() {
        return isLandscape;
    }

    public void setIsLandscape(int isLandscape) {
        this.isLandscape = isLandscape;
    }
}
