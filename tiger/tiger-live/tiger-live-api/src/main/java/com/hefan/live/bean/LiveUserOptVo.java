package com.hefan.live.bean;

import java.io.Serializable;

/**
 * Created by nigle on 2016/10/8.
 */
@SuppressWarnings("serial")
public class LiveUserOptVo implements Serializable{

    String userId;
    String optUserId;
    String liveUuid;
    int chatRoomId;
    String token; //直播token

    int type;//0关播1开播2进入3离开

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOptUserId() {
        return optUserId;
    }

    public void setOptUserId(String optUserId) {
        this.optUserId = optUserId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public int getChatRoomId() {
        return chatRoomId;
    }

    public void setChatRoomId(int chatRoomId) {
        this.chatRoomId = chatRoomId;
    }
}
