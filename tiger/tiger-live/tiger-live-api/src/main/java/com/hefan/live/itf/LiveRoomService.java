package com.hefan.live.itf;

import java.util.List;
import java.util.Map;

import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.hefan.live.bean.LiveRoom;
import com.hefan.live.bean.LiveRoomVo;
import com.hefan.live.bean.LivingRoomInfoVo;

/**
 * Created by nigle on 2016/9/28.
 */
public interface LiveRoomService {

	LiveRoom getLiveRoom(String userId);

	/**
	 * 下单后更新直播盒饭收入 ticket_history
	 * 
	 * @param liveRoom
	 * @return
	 */
	int updateLiveRoomForRebalance(LiveRoom liveRoom);

	/**
	 * 随机推荐
	 * 
	 * @return
	 */
	List<LivingRoomInfoVo> recommendLive()throws Exception ;

	/**
	 * 查询正在直播的liveRoom集合
	 * 
	 * @return
	 */
	List<LiveRoom> getLivingRoomList();

	LivingRoomInfoVo findLiveRoomById(String liveId);

	LiveRoom getLiveRoomByChatRoomId(int chatRoomId);

	List<LiveRoomVo> getLivingList(Page po);

	LiveRoomVo findLiveRoomByIdFormonitor(String userId, String uuid);

	ResultBean listHostIndex();

	List getshareWordsForBefore();
	List getshareWordsForAfter();

}
