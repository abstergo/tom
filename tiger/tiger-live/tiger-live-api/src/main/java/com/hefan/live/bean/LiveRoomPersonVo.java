package com.hefan.live.bean;

import java.io.Serializable;

/**
 * 直播间用户信息
 * 
 * @author kevin_zhang
 *
 */
@SuppressWarnings("serial")
public class LiveRoomPersonVo implements Serializable {

	private String userId;
	private int userType;
	private String headImg;
	private int userLevel;
	private String nickName;

	public static int getSortScore(int userType, int userLevel) {
		switch (userType) {
		case 0:// 0:用户（同等级真实用户与假用户，真是用户优先排在假用户之前）
			return 1001 + userLevel;
		case 1:// 1:网红
			return 2000 + userLevel;
		case 2:// 2:明星
			return 4000 + userLevel;
		case 3:// 3:片场
			return 3000 + userLevel;
		case 4:// 4 内部员工
		case 5:// 5:经纪人
		case 6:// 6:虚拟币用户
		case 7:// 7:机器人
			return 1000 + userLevel;
		default:
			return 1000 + userLevel;
		}
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public int getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(int userLevel) {
		this.userLevel = userLevel;
	}
}
