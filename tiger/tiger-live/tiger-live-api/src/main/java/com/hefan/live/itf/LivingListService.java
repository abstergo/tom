package com.hefan.live.itf;

import java.util.List;

/**
 * 直播列表处理
 * 
 * @author kevin_zhang
 *
 */
public interface LivingListService {

	/**
	 * 获取所有正在直播的直播间人数
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List getLivingRoomPeopleCount();

	/**
	 * 获取热门页正在直播的直播列表
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List getHotLivingList();

	/**
	 * 获取最新正在直播的直播列表
	 * 
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List getLatestLivingList();
}
