package com.hefan.live.bean;

import java.io.Serializable;

/**
 * Created by nigle on 2016/10/24.
 */
public class LiveRobotVo implements Serializable{
    String userId;
    String headImg;
    int userLevel;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }
}
