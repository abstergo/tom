package com.hefan.live.bean;

import java.io.Serializable;

/**
 * Created by nigle on 2016/10/18.
 */
public class LiveDynamicVo implements Serializable {

    String userId;
    String messageType;//3表示视频
    String messageInfo;//动态文字描述
    String length;//视频长度mm:ss
    Boolean transcode;//true（是否转码）
    Integer isSync;//1表示同步到视频
    String pathTrans;//转码后视频地址
    String backImg;//视频封面图
    String fromType;//来源0:回放
    long times;//观看次数

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageInfo() {
        return messageInfo;
    }

    public void setMessageInfo(String messageInfo) {
        this.messageInfo = messageInfo;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public Boolean getTranscode() {
        return transcode;
    }

    public void setTranscode(Boolean transcode) {
        this.transcode = transcode;
    }

    public Integer getIsSync() {
        return isSync;
    }

    public void setIsSync(Integer isSync) {
        this.isSync = isSync;
    }

    public String getPathTrans() {
        return pathTrans;
    }

    public void setPathTrans(String pathTrans) {
        this.pathTrans = pathTrans;
    }

    public String getBackImg() {
        return backImg;
    }

    public void setBackImg(String backImg) {
        this.backImg = backImg;
    }

    public String getFromType() {
        return fromType;
    }

    public void setFromType(String fromType) {
        this.fromType = fromType;
    }

    public long getTimes() {
        return times;
    }

    public void setTimes(long times) {
        this.times = times;
    }
}
