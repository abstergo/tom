package com.hefan.live.itf;

import com.hefan.live.bean.*;
import java.util.List;
import java.util.Map;

/**
 * 用户进出直播间操作
 * 
 * @author kevin_zhang
 *
 */
public interface LivingRedisOptService {
	/**
	 * 增加指定主播的直播信息
	 */
	long addLivingInfo_Hash(String authID, String livingInfo);

	/**
	 * 删除指定主播的直播信息
	 */
	long delLivingInfo_Hash(String authID);

	/**
	 * 查询指定主播是否正在直播中
	 */
	boolean isExistsLivingInfo_Hash(String authID);

	/**
	 * 查询指定主播的直播信息
	 */
	String getLivingInfo_Hash(String authID);

	/**
	 * 查询所有正在直播的直播信息
	 */
	Map<String, String> getAllLivingInfo_Hash();

	/**
	 * 增加用户进入直播间的Check信息
	 */
	long addUserEnterLivingInfo_Hash(String userId, String valueCheck);

	/**
	 * 删除用户进入直播间的Check信息
	 */
	long delUserEnterLivingInfo_Hash(String userId);

	/**
	 * 查询用户是否有进入直播间的Check信息
	 */
	boolean isExistsUserEnterLivingInfo_Hash(String userId);

	/**
	 * 查询用户进入直播间的Check信息
	 */
	String getUserEnterLivingInfo_Hash(String userId);

	/**
	 * 向直播间添加进入的用户信息
	 */
	long addLivingUserInfo_Hash(String anchId, String userId, String livingPersonStr);

	/**
	 * 删除直播间中指定的用户信息
	 */
	long delLivingUserInfo_Hash(String anchId, String userId);

	/**
	 * 删除直播间中所有的用户信息
	 */
	long delAllLivingUserInfo_Hash(String anchId);

	/**
	 * 获取指定直播间的用户信息
	 */
	String getLivingUserInfo_Hash(String anchId, String userId);

	/**
	 * 向直播间添加进入的用户信息(排序)
	 */
	long addLivingUserInfo_SortedSet(String anchId, double sortScore, String userId);

	/**
	 * 查询用户是否在直播间
	 */
	Long isInRoom(String anchId, String userId);

	/**
	 * 删除直播间中指定的用户信息(排序)
	 */
	long delLivingUserInfo_SortedSet(String anchId, String userId);

	/**
	 * 删除直播间中指定的用户信息(排序)
	 */
	long delAllLivingUserInfo_SortedSet(String anchId);

	/**
	 * 获取指定主播直播间观看人数(排序)
	 */
	long getLivingUserCount_SortedSet(String anchId);

	/**
	 * 分页获取指定主播直播间用户列表(排序)
	 */
	List getLivingUserList_SortedSet(String anchId, int pageNum, int pageSize);

	/**
	 * 获取直播间全部观众的ID
	 */
	List getAllUserIdInRoom(String anchId);

	/**
	 * 向指定主播的直播间添加机器人
	 */
	long addLivingRobot(String anchId, String userId);

	/**
	 * 移除一个直播间机器人（后归还全局）
	 */
	String removeLivingRobot(String anchId);

	/**
	 * 取指定主播直播间所有机器人ID
	 */
	List getLivingRoomRobotList(String anchId);

	/**
	 * 取指定主播直播间机器人个数
	 */
	long getLivingRobotCount(String anchId);

	/**
	 * 取redis中所有机器人的数量
	 */
	long getLivingRobotCount();

	/**
	 * 向全局机器人列表中添加机器人
	 */
	long addWholeRobot(String userId, String robotStr);

	/**
	 * 查询缓存中机器人信息数量
	 */
	long getWholeRobotCount();

	/**
	 * 查询缓存中机器人信息数量
	 */
	long getWholeRobotIdCount();

	/**
	 * 删除机器人信息的IDlist和infoHash
	 */
	void clearRobot();

	/**
	 * 取全局机器人中的一个ID
	 */
	String getRobotIdInAll();

	/**
	 * 归还全局机器人中的一个ID
	 */
	long returnRobotIdToAll(String userId);

	/**
	 * 关播时批量归还机器人ID
	 * @param anchId
	 * @param listRobotIds
     */
	void returnRobotIdsToAll(String anchId, List listRobotIds);

	/**
	 * 获取机器人在缓存中的信息
	 * @param userId
	 * @return json for robotinfo
	 */
	String getRobotInfoInAll(String userId);

	/**
	 * 设置机器人所在位置（主播ID）
	 * @param userId
	 * @return anchId
	 */
	long setRobotLocation(String userId, String anchId);

	/**
	 * 获取机器人所在位置（主播ID）
	 * @param userId
	 * @return anchId
	 */
	String getRobotLocation(String userId);

	/**
	 * 移除所有直播间机器人
	 */
	long removeAllRoomRobot(String anchId);

	/**
	 * 移除所有全局机器人
	 */
	long removeAllWholeRobot_List();

	/**
	 * 初始化直播间第一页用户列表信息
	 */
	String initLivingHomePageUserListInfo(String liveUuid, String valueStr);

	/**
	 * 增加直播间第一页用户列表信息
	 */
	void addLivingHomePageUserListInfo(String liveUuid, String valueStr);

	/**
	 * 查询直播间第一页用户列表信息
	 */
	String getLivingHomePageUserListInfo(String liveUuid);

	/**
	 * 初始化直播间心跳
	 */
	String initLivingHeartBeatInfo(String liveUuid, String valueStr);

	/**
	 * 增加直播间心跳
	 */
	void addLivingHeartBeatInfo(String liveUuid, String valueStr);

	/**
	 * 查询直播间心跳信息
	 */
	String getLivingHeartBeatInfo(String liveUuid);

	/**
	 * 初始化直播间观看人次
	 */
	void initLivingWatchNum(String liveUuid, int watchNum);

	/**
	 * 增加直播间观看人次
	 */
	long addLivingWatchNum(String liveUuid, int watchNum);

	/**
	 * 获取直播间观看人次
	 */
	String getLivingWatchNum(String liveUuid);

	/**
	 * 存直播间踢出信息
	 */
	String setLivingForbiddenEnter(String liveUuid,  RoomOuter roomOuter);

	/**
	 * 获取直播间踢出信息
	 */
	String getLivingForbiddenEnter(String liveUuid, String userId);

	/**
	 * 存储live_log live_room数据到redis
     */
	boolean addLiveLogData(LiveRoom liveRoom, LiveLog liveLog, LivingRoomInfoVo lriVo);

	/**
	 * 取liveLog缓存数据
     */
	LiveLog getLiveLogData(String userId);

	/**
	 * 取liveRoom缓存数据
	 */
	LiveRoom getLiveRoomData(String userId);

	/**
	 * 取liveInfo缓存数据
	 */
	LivingRoomInfoVo getLivingRoomInfoVoData(String userId);

	/**
	 * 清除liveLog、liveRoom、liveInfo缓存数据
	 */
	void delLiveCacheData(String userId);

	/**
	 * 清除开播token
	 */
	long delLiveToken(String userId);

	/**
	 * 设置直播token
	 */
	String setLiveToken(String userId, String token);

	/**
	 * 获取直播token
	 */
	String getLiveToken(String userId);

	/**
	 * 设置禁言用户
	 */
	String addShutUpUser(RoomShutup roomShutup, String liveUuid, String userId);

	/**
	 * 获取禁言用户
	 */
	String getShutUpUser(String liveUuid, String userId);

	/**
	 * 用户是否被禁言
	 */
	boolean isShutUpUser(String liveUuid, String userId);

	/**
	 * 设置新进入直播间时间
	 */
	String addLastJoinTime(String anchId);

	/**
	 * 获取新进入直播间时间
	 */
	String getLastJoinTime(String anchId);

  /**
   * 获取本次直播间收入
   */
  long getTicketCountByUuid(String liveUuid);

  /**
   * 清除本次直播收入缓存--设置两小时缓存失效时间
   */
  void delTicketCountByUuid(String liveUuid);

	/**
	 * 给主播直播间加额外人数
	 */
	String setAddNumForRoom(String userId, String number);

	/**
	 * 获取主播直播间的额外人数
	 */
	String getAddNumForRoom(String userId);

	/**
	 * 获取主播直播间所有真实人数
     */
	long getLivingUserCountReal(String anchId);

	/**
	 * 清除主播直播间所有真实人数的缓存
	 */
	void delLivingUserCountReal(String anchId);

	/**
	 * 增加主播直播间所有真实人数
	 */
	long addLivingUserCountReal(String anchId, int num);

	/**
	 * 减少主播直播间所有真实人数
	 */
	long decLivingUserCountReal(String anchId, int num);

	/**
	 * IM发送策略检查
	 *
	 * @param authId：主播ID
	 * @param IMMsgType(0：小礼物 1：盒饭数 2：点亮 3：关注 4：弹幕 5：特效)
	 * @return
	 */
	boolean IMSendCheck(String authId, int IMMsgType);

	/**
	 * 全部机器人缓存中删除机器人信息
	 * @param userId
	 * @return
	 */
	long removeRobotInAll(String userId);

	/**
	 * 直播间机器人缓存中删除机器人信息
	 * @param userId
	 * @return
	 */
	long removeRobotInRoom(String userId, String anchId);

	/**
	 * 直播间踢机器人
	 */
	 void outRobot(String anchId,String userId);
}
