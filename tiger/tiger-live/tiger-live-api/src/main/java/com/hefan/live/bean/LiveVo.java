package com.hefan.live.bean;

import java.io.Serializable;

/**
 * Created by nigle on 2016/10/8.
 */
public class LiveVo implements Serializable{

    String userId;
    String liveName;
    String displayGraph;
    String liveImg;
    String location;
    String liveUuid;
    int chatRoomId;
    String token;
    int isLandscape = 0;//0：竖屏 1:横屏

    public int getChatRoomId() {
        return chatRoomId;
    }

    public void setChatRoomId(int chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLiveName() {
        return liveName;
    }

    public void setLiveName(String liveName) {
        this.liveName = liveName;
    }

    public String getDisplayGraph() {
        return displayGraph;
    }

    public void setDisplayGraph(String displayGraph) {
        this.displayGraph = displayGraph;
    }

    public String getLiveImg() {
        return liveImg;
    }

    public void setLiveImg(String liveImg) {
        this.liveImg = liveImg;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIsLandscape() {
        return isLandscape;
    }

    public void setIsLandscape(int isLandscape) {
        this.isLandscape = isLandscape;
    }
}
