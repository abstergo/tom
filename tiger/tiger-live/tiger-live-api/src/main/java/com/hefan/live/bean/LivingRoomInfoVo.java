package com.hefan.live.bean;

import java.io.Serializable;
import com.alibaba.fastjson.annotation.JSONField;

/**
 * 直播中的直播间信息
 * 
 * @author kevin_zhang
 *
 */
@SuppressWarnings({ "serial", "rawtypes" })
public class LivingRoomInfoVo implements Serializable, Comparable, Cloneable {

	private String userId = "";// 主播ID
	private String id = "";// 主播ID
	private int type=0; // 主播类型
	private String name = "";// 主播名字
	private String headImg = "";// 头像地址
	private String liveImg = "";// 直播封面图片地址
	private String personSign = "";// 直播间简述
	private String displayGraph = "";
	private String location = "";// 定位地址
	private int chatRoomId=0;
	private String liveUuid = "";
	private String liveUrl = "";
	private String pullUrls = "";// 拉流地址，以英文逗号隔开
	private String pushUrls = "";// 拉流地址，以英文逗号隔开
	private String hefanTotal = "";// 盒饭数
	private long peopleCount=0;// 观看人数
	private long startLiveTime=0;// 开播时间
	private int isVipLive = 0;//VIP直播 0:不是  1:是
	private int isShow = 1;//前端是否显示 0：不显示  1:显示
	private int rotate = 0;
	private int doubleSrc = 0;
	private int isLandscape = 0;//是否是横屏直播 0：竖屏  1:横屏

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	private int status;

	public void setPeopleCount(long peopleCount) {
		this.peopleCount = peopleCount;
	}

	public long getWatchNum() {
		return watchNum;
	}

	public void setWatchNum(long watchNum) {
		this.watchNum = watchNum;
	}

	private long watchNum;

	public String getReUrl() {
		return reUrl;
	}

	public void setReUrl(String reUrl) {
		this.reUrl = reUrl;
	}

	private String reUrl;

	@JSONField(serialize = false)
	private long sortScore;// 排序值

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public String getLiveImg() {
		return liveImg;
	}

	public void setLiveImg(String liveImg) {
		this.liveImg = liveImg;
	}

	public String getPersonSign() {
		return personSign;
	}

	public void setPersonSign(String personSign) {
		this.personSign = personSign;
	}

	public String getDisplayGraph() {
		return displayGraph;
	}

	public void setDisplayGraph(String displayGraph) {
		this.displayGraph = displayGraph;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getChatRoomId() {
		return chatRoomId;
	}

	public void setChatRoomId(int chatRoomId) {
		this.chatRoomId = chatRoomId;
	}

	public String getLiveUuid() {
		return liveUuid;
	}

	public void setLiveUuid(String liveUuid) {
		this.liveUuid = liveUuid;
	}

	public String getLiveUrl() {
		return liveUrl;
	}

	public void setLiveUrl(String liveUrl) {
		this.liveUrl = liveUrl;
	}

	public String getPullUrls() {
		return pullUrls;
	}

	public void setPullUrls(String pullUrls) {
		this.pullUrls = pullUrls;
	}

	public String getPushUrls() {
		return pushUrls;
	}

	public void setPushUrls(String pushUrls) {
		this.pushUrls = pushUrls;
	}
	
	public String getHefanTotal() {
		return hefanTotal;
	}

	public void setHefanTotal(String hefanTotal) {
		this.hefanTotal = hefanTotal;
	}

	public Long getPeopleCount() {
		return peopleCount;
	}

	public void setPeopleCount(Long peopleCount) {
		this.peopleCount = peopleCount;
	}

	public long getStartLiveTime() {
		return startLiveTime;
	}

	public void setStartLiveTime(long startLiveTime) {
		this.startLiveTime = startLiveTime;
	}

	public long getSortScore() {
		return sortScore;
	}

	public void setSortScore(long sortScore) {
		this.sortScore = sortScore;
	}

	public int getIsVipLive() {
		return isVipLive;
	}

	public void setIsVipLive(int isVipLive) {
		this.isVipLive = isVipLive;
	}

	public int getIsShow() {
		return isShow;
	}

	public void setIsShow(int isShow) {
		this.isShow = isShow;
	}

	public int getRotate() {
		return rotate;
	}

	public void setRotate(int rotate) {
		this.rotate = rotate;
	}

	public int getDoubleSrc() {
		return doubleSrc;
	}

	public void setDoubleSrc(int doubleSrc) {
		this.doubleSrc = doubleSrc;
	}

	public int getIsLandscape() {
		return isLandscape;
	}

	public void setIsLandscape(int isLandscape) {
		this.isLandscape = isLandscape;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		LivingRoomInfoVo o = null;
		try {
			o = (LivingRoomInfoVo) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return o;
	}

	@Override
	public int compareTo(Object o) {
		LivingRoomInfoVo s = (LivingRoomInfoVo) o;
		return (int) (s.sortScore - this.sortScore);
	}
}
