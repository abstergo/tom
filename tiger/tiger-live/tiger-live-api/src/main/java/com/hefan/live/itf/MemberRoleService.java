package com.hefan.live.itf;

import com.cat.common.entity.Page;
import com.hefan.live.bean.LiveReturnVo;
import com.hefan.live.bean.MemberRole;

import java.util.List;

/**
 * Created by nigle on 2016/10/8.
 */
public interface MemberRoleService {

	/**
	 * 添加直播间管理员
	 * @param memberRole
	 * @return
     */
	int save(MemberRole memberRole);

	/**
	 * 删除直播间管理员
	 * @param chatRoomId
	 * @param userId
	 * @return
	 */
	int del(int chatRoomId, String userId);

    /**
     * 查询是否直播间管理员
     * @param optUserId
     * @param chatRoomId
     * @return
     */
    boolean findIsAdminForAnchorByUserId(String optUserId, int chatRoomId);


	/**
	 * 获取直播间管理员列表
	 * @param chatRoomId
	 * @return
     */
	List<MemberRole> findRoomAdminList(int chatRoomId);

	/**
	 * 获取直播间管理员信息列表
	 * @param listMem
	 * @return
     */
	List<LiveReturnVo> findAdminInfo(List<MemberRole> listMem);

	/**
	 * 获取直播间管理员数量
	 * @param chatRoomId
	 * @return
	 */
	long getAdminCount(int chatRoomId);

}
