package com.hefan.live.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;


/**
 * 房间禁言列表
 * Created by nigle on 2016/10/6.
 */
@Entity(tableName = "room_shutup")
public class RoomShutup extends BaseEntity {

    @Column("user_id")
    private String userId;
    @Column("opt_user_id")
    private String optUserId;
    @Column("chat_room_id")
    private int chatRoomId;
    @Column("live_uuid")
    private String liveUuid;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOptUserId() {
        return optUserId;
    }

    public void setOptUserId(String optUserId) {
        this.optUserId = optUserId;
    }

    public void setChatRoomId(int chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public int getChatRoomId() {
        return chatRoomId;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }
}
