package com.hefan.live.itf;

import com.hefan.live.bean.WarnLog;

import java.util.List;
import java.util.Map;

public interface LiveWarnService {
	
	/**
	 * 保存警告日志表
	 * @Title: saveWarnLog   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param warnLog
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月29日 下午1:57:37
	 */
	int saveWarnLog(WarnLog warnLog);
	
	/**
	 * 改为高危直播
	 * @Title: updateLiveHighStatus   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月29日 下午2:01:29
	 */
	int updateLiveHighStatus(String userId,String liveUuid,int risk,String optUserId);
	
	/**
	 * 获取违规配置信息
	 * @Title: getPunishTypeList   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @return      
	 * @return: List
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月29日 下午3:23:34
	 */
	public List getPunishTypeList();
	
	/**
	 * 提交违规处理
	 * @Title: dealPunishDetal   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @param optUserId
	 * @param map (punish_type)
	 * @param punishReason
	 * @param illegalPic
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月29日 下午5:11:23
	 */
	public int dealPunishDetal(String userId,String optUserId,Map map,String punishReason,String illegalPic,String liveUuid,int punishTypeFromTb, int punishDataType);

	/**
	 * 根据id获取违规配置信息
	 * @Title: getPunishTypeById   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param typeId
	 * @return      
	 * @return: Map
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月29日 下午5:19:42
	 */
	public Map getPunishTypeById(long typeId,int punishTypeFromTb);
}
