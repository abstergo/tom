package com.hefan.live.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by nigle on 2016/10/8.
 */
public class MemberRole implements Serializable {

    private int id;
    private int chatRoomId;
    private String operatorAccid;//操作者账号accid-必须是房间的创建者
    private String targetAccid;//被操作者账号accid ,对应web_user表user_id
    private String createUser;//创建人：后台系统--创建人
    private Date createTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChatRoomId() {
        return chatRoomId;
    }

    public void setChatRoomId(int chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getOperatorAccid() {
        return operatorAccid;
    }

    public void setOperatorAccid(String operatorAccid) {
        this.operatorAccid = operatorAccid;
    }

    public String getTargetAccid() {
        return targetAccid;
    }

    public void setTargetAccid(String targetAccid) {
        this.targetAccid = targetAccid;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
