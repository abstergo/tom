package com.hefan.notify.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cat.common.constant.AliSmsConstants;
import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class AliSendSmsUtil {
	public static Logger logger = LoggerFactory.getLogger(AliSendSmsUtil.class);

	/**
	 * @param SmsFreeSignName
	 *            短信签名 登录:SagaConstant.bigFishSmsTemplateCodeLogin
	 *            其他是身份验证:SagaConstant.bigFishSmsTemplateSign,
	 * @param SmsParamString
	 *            {"code":"1234","product":"传单"}
	 * @param phone
	 *            手机号
	 * @param smsTemplateCode
	 *            短信模板 登录:SagaConstant.bigFishSmsFreeSignNameLogin
	 *            其他是身份验证:SagaConstant.bigFishSmsFreeSignNameSign
	 * @return "success" or sub_code:错误代码 sub_code:isv.MOBILE_NUMBER_ILLEGAL
	 *         -手机号码格式错误 sub_code:isv.BUSINESS_LIMIT_CONTROL -验证码请求频繁,请稍后再试
	 *         备注:同一手机同一模板签名的短信次数限制:1分钟1条1小时7条1天50条
	 * @author ning
	 * @date 2016-1-22
	 */
	public static String loginSign(String SmsFreeSignName, Map<String, String> SmsParamString, String phone,
			String smsTemplateCode) {
		TaobaoClient client = new DefaultTaobaoClient(AliSmsConstants.bigFishUrl, AliSmsConstants.bigFishAppKey,
				AliSmsConstants.bigFishSecret);
		AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
		req.setSmsType("normal");
		req.setSmsFreeSignName(SmsFreeSignName);
		req.setSmsParamString(JSON.toJSONString(SmsParamString));
		req.setRecNum(phone);
		req.setSmsTemplateCode(smsTemplateCode);
		AlibabaAliqinFcSmsNumSendResponse rsp;
		try {
			rsp = client.execute(req);
			JSONObject o = JSONObject.parseObject(rsp.getBody());
			logger.info("阿里短信发送请求返回：" + o);

			JSONObject o1 = JSONObject.parseObject(o.getString("alibaba_aliqin_fc_sms_num_send_response"));
			if (null != o1) {
				JSONObject o2 = JSONObject.parseObject(o1.getString("result"));
				String result = o2.getString("success");
				logger.info("阿里短信发送成功返回结果：" + result);
				if (!StringUtils.isBlank(result) && result.equals("true"))
					return "success";
				else
					return "error";
			}

			JSONObject oo1 = JSONObject.parseObject(o.getString("error_response"));
			if (null != oo1) {
				logger.info("阿里短信发送失败返回结果：" + oo1);
				String sub_code = oo1.getString("sub_code");

				printLog(sub_code);
				return sub_code;
			}
		} catch (ApiException e) {
			e.printStackTrace();
		}
		return "error";
	}

	private static void printLog(String subCode) {
		if (subCode.equals("isv.OUT_OF_SERVICE"))
			logger.info("返回SubCode=" + subCode + " 业务停机：登陆www.alidayu.com充值");
		else if (subCode.equals("isv.PRODUCT_UNSUBSCRIBE"))
			logger.info("返回SubCode=" + subCode + " 产品服务未开通：登陆www.alidayu.com开通相应的产品服务");
		else if (subCode.equals("isv.ACCOUNT_NOT_EXISTS"))
			logger.info("返回SubCode=" + subCode + " 账户信息不存在：登陆www.alidayu.com完成入驻");
		else if (subCode.equals("isv.ACCOUNT_ABNORMAL"))
			logger.info("返回SubCode=" + subCode + " 账户信息异常：联系技术支持");
		else if (subCode.equals("isv.SMS_TEMPLATE_ILLEGAL"))
			logger.info("返回SubCode=" + subCode + " 模板不合法：登陆www.alidayu.com查询审核通过短信模板使用");
		else if (subCode.equals("isv.SMS_SIGNATURE_ILLEGAL"))
			logger.info("返回SubCode=" + subCode + " 签名不合法：登陆www.alidayu.com查询审核通过的签名使用");
		else if (subCode.equals("isv.MOBILE_NUMBER_ILLEGAL"))
			logger.info("返回SubCode=" + subCode + " 手机号码格式错误：使用合法的手机号码");
		else if (subCode.equals("isv.MOBILE_COUNT_OVER_LIMIT"))
			logger.info("返回SubCode=" + subCode + " 手机号码数量超过限制：批量发送，手机号码以英文逗号分隔，不超过200个号码");
		else if (subCode.equals("isv.TEMPLATE_MISSING_PARAMETERS"))
			logger.info("返回SubCode=" + subCode + " 短信模板变量缺少参数：确认短信模板中变量个数，变量名，检查传参是否遗漏");
		else if (subCode.equals("isv.INVALID_PARAMETERS"))
			logger.info("返回SubCode=" + subCode + " 参数异常：检查参数是否合法");
		else if (subCode.equals("isv.BUSINESS_LIMIT_CONTROL"))
			logger.info("返回SubCode=" + subCode
					+ " 触发业务流控限制：短信验证码，使用同一个签名，对同一个手机号码发送短信验证码，允许每分钟1条，累计每小时7条。 短信通知，使用同一签名、同一模板，对同一手机号发送短信通知，允许每天50条（自然日）。");
		else if (subCode.equals("isv.INVALID_JSON_PARAM"))
			logger.info(
					"返回SubCode=" + subCode + " JSON参数不合法：JSON参数接受字符串值。例如{\"code\":\"123456\"}，不接收{\"code\":123456}");
		else if (subCode.equals("isp.SYSTEM_ERROR"))
			logger.info("返回SubCode=" + subCode);
		else if (subCode.equals("isv.BLACK_KEY_CONTROL_LIMIT"))
			logger.info("返回SubCode=" + subCode + " 模板变量中存在黑名单关键字。如：阿里大鱼：黑名单关键字禁止在模板变量中使用，若业务确实需要使用，建议将关键字放到模板中，进行审核。");
		else if (subCode.equals("isv.PARAM_NOT_SUPPORT_URL"))
			logger.info("返回SubCode=" + subCode + " 不支持url为变量：域名和ip请固化到模板申请中");
		else if (subCode.equals("isv.PARAM_LENGTH_LIMIT"))
			logger.info("返回SubCode=" + subCode + " 变量长度受限：变量长度受限 请尽量固化变量中固定部分");
		else if (subCode.equals("isv.AMOUNT_NOT_ENOUGH"))
			logger.info("返回SubCode=" + subCode + " 余额不足：因余额不足未能发送成功，请登录管理中心充值后重新发送");
		else
			logger.info("返回SubCode=" + subCode);
	}
}
