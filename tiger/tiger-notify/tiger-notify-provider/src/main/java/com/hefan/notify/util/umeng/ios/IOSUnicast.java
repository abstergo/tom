package com.hefan.notify.util.umeng.ios;

import com.hefan.notify.util.umeng.IOSNotification;

public class IOSUnicast extends IOSNotification {
	public IOSUnicast(String appkey,String appMasterSecret,boolean much) throws Exception{
			setAppMasterSecret(appMasterSecret);
			setPredefinedKeyValue("appkey", appkey);
			if(!much){
				this.setPredefinedKeyValue("type", "unicast");	
			}else{
				this.setPredefinedKeyValue("type", "listcast");
			}
	}
	
	public void setDeviceToken(String token) throws Exception {
    	setPredefinedKeyValue("device_tokens", token);
    }
}
