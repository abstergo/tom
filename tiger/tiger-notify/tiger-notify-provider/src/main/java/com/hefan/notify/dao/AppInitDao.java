package com.hefan.notify.dao;

import com.hefan.notify.bean.AppInitInfo;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.common.orm.dao.BaseDaoImpl;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * User: criss Date: 16/9/21 Time: 15:20
 */
@Repository
public class AppInitDao extends BaseDaoImpl<AppInitInfo> {

	@Resource
	JdbcTemplate jdbcTemplate;

	public AppInitInfo getAppInitInfo(int type) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" type,app_version,update_link ");
		sql.append(" from app_init_info ");
		sql.append(" WHERE type=" + type + " and  delete_flag=0 ORDER BY create_time DESC LIMIT 1");
		List<AppInitInfo> list = jdbcTemplate.query(sql.toString(),
				new BeanPropertyRowMapper<AppInitInfo>(AppInitInfo.class), type);
		return CollectionUtils.isNotEmpty(list) ? list.get(0) : null;
	}
}
