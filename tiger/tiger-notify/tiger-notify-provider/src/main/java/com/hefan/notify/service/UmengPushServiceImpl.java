package com.hefan.notify.service;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.hefan.common.util.PayPropertiesUtils;
import com.hefan.notify.bean.PubParams;
import com.hefan.notify.itf.UmengPushService;
import com.hefan.notify.util.umeng.AndroidNotification;
import com.hefan.notify.util.umeng.PushClient;
import com.hefan.notify.util.umeng.android.AndroidBroadcast;
import com.hefan.notify.util.umeng.android.AndroidCustomizedcast;
import com.hefan.notify.util.umeng.android.AndroidFilecast;
import com.hefan.notify.util.umeng.android.AndroidGroupcast;
import com.hefan.notify.util.umeng.android.AndroidUnicast;
import com.hefan.notify.util.umeng.ios.IOSBroadcast;
import com.hefan.notify.util.umeng.ios.IOSCustomizedcast;
import com.hefan.notify.util.umeng.ios.IOSFilecast;
import com.hefan.notify.util.umeng.ios.IOSGroupcast;
import com.hefan.notify.util.umeng.ios.IOSUnicast;

@Component("umengPushService")
public class UmengPushServiceImpl implements UmengPushService {

	Logger logger = LoggerFactory.getLogger(UmengPushServiceImpl.class);

	private PushClient client = new PushClient();
	private boolean ProductionMode = PayPropertiesUtils.getBoolean("umeng.productionMode");//环境变量   true-生产环境   false-测试环境
	private String uemngAndAppKey = PayPropertiesUtils.getString("umeng.and.appKey");
	private String uemngAndAppMasterSecret = PayPropertiesUtils.getString("umeng.and.appMasterSecret");
	private String uemngIosAppKey = PayPropertiesUtils.getString("umeng.ios.appKey");
	private String uemngIosAppMasterSecret = PayPropertiesUtils.getString("umeng.ios.appMasterSecret");
	/**
	 * android广播
	 * @Title: sendAndroidBroadcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param params
	 * @param: @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月25日 下午12:01:24
	 */
	@Override
	public boolean sendAndroidBroadcast(PubParams params){
		// TODO Auto-generated method stub
		try {
			AndroidBroadcast broadcast = new AndroidBroadcast(uemngAndAppKey,uemngAndAppMasterSecret);
			broadcast.setTicker(params.getTicker());
			broadcast.setTitle(params.getTitle());
			broadcast.setText(params.getText());
			broadcast.goAppAfterOpen();
			broadcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
			// TODO Set 'production_mode' to 'false' if it's a test device. 
			// For how to register a test device, please see the developer doc.
			if(ProductionMode){
				broadcast.setProductionMode();
			}else{
				broadcast.setTestMode();
			}
			// Set customized fields
			broadcast.setExtraField("after_open_type", params.getAfterOpenType()+"");
			if(params.getUpType() > 0){
				broadcast.setExtraField("up_type", params.getUpType()+"");
				broadcast.setExtraField("up_value", params.getUpValue());
			}
			if(StringUtils.isNotBlank(params.getStartTime()))
				broadcast.setPredefinedKeyValue("start_time",params.getStartTime());
			return client.send(broadcast);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	/**
	 * android单播
	 * @Title: sendAndroidUnicast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param params
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月25日 下午2:34:54
	 */
	@Override
	public boolean sendAndroidUnicast(PubParams params,boolean much){
		// TODO Auto-generated method stub
		try {
			AndroidUnicast unicast = new AndroidUnicast(uemngAndAppKey,uemngAndAppMasterSecret,much);
			// TODO Set your device token
			unicast.setDeviceToken(params.getDeviceTokens());
			unicast.setTicker(params.getTicker());
			unicast.setTitle(params.getTitle());
			unicast.setText(params.getText());
			unicast.goAppAfterOpen();
			unicast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
			// TODO Set 'production_mode' to 'false' if it's a test device. 
			// For how to register a test device, please see the developer doc.
			if(ProductionMode){
				unicast.setProductionMode();
			}else{
				unicast.setTestMode();
			}
			// Set customized fields
			unicast.setExtraField("after_open_type", params.getAfterOpenType()+"");
			if(params.getUpType() > 0){
				unicast.setExtraField("up_type", params.getUpType()+"");
				unicast.setExtraField("up_value", params.getUpValue());
			}
			if(StringUtils.isNotBlank(params.getStartTime()))
				unicast.setPredefinedKeyValue("start_time",params.getStartTime());
			return client.send(unicast);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	/**
	 * android组播
	 * @Title: sendAndroidGroupcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param params
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月25日 下午2:45:14
	 */
	@Override
	public boolean sendAndroidGroupcast(PubParams params){
		// TODO Auto-generated method stub
		try {
			AndroidGroupcast groupcast = new AndroidGroupcast(uemngAndAppKey,uemngAndAppMasterSecret);
			JSONObject filterJson = new JSONObject();
			JSONObject whereJson = new JSONObject();
			JSONArray tagArray = new JSONArray();
			for (String tag : params.getTagList()) {
				JSONObject tagParam = new JSONObject();
				tagParam.put("tag", tag);
				tagArray.put(tagParam);
			}
			whereJson.put("and", tagArray);
			filterJson.put("where", whereJson);
			System.out.println(filterJson.toString());
			
			groupcast.setFilter(filterJson);
			groupcast.setTicker(params.getTicker());
			groupcast.setTitle(params.getTitle());
			groupcast.setText(params.getText());
			groupcast.goAppAfterOpen();
			groupcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
			// TODO Set 'production_mode' to 'false' if it's a test device. 
			// For how to register a test device, please see the developer doc.
			if(ProductionMode){
				groupcast.setProductionMode();
			}else{
				groupcast.setTestMode();
			}
			// Set customized fields
			groupcast.setExtraField("after_open_type", params.getAfterOpenType()+"");
			if(params.getUpType() > 0){
				groupcast.setExtraField("up_type", params.getUpType()+"");
				groupcast.setExtraField("up_value", params.getUpValue());
			}
			if(StringUtils.isNotBlank(params.getStartTime()))
				groupcast.setPredefinedKeyValue("start_time",params.getStartTime());
			return client.send(groupcast);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	
	/**
	 * android文件播
	 * @Title: sendAndroidFilecast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param params
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月25日 下午2:45:14
	 */
	@Override
	public boolean sendAndroidFilecast(PubParams params){
		// TODO Auto-generated method stub
		try {
			AndroidFilecast filecast = new AndroidFilecast(uemngAndAppKey,uemngAndAppMasterSecret);
			filecast.setFileId(params.getFileId());
			filecast.setTicker(params.getTicker());
			filecast.setTitle(params.getTitle());
			filecast.setText(params.getText());
			filecast.goAppAfterOpen();
			filecast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
			// TODO Set 'production_mode' to 'false' if it's a test device.
			// For how to register a test device, please see the developer doc.
			if(ProductionMode){
				filecast.setProductionMode();
			}else{
				filecast.setTestMode();
			}
			// Set customized fields
			filecast.setExtraField("after_open_type", params.getAfterOpenType()+"");
			if(params.getUpType() > 0){
				filecast.setExtraField("up_type", params.getUpType()+"");
				filecast.setExtraField("up_value", params.getUpValue());
			}
			if(StringUtils.isNotBlank(params.getStartTime()))
				filecast.setPredefinedKeyValue("start_time",params.getStartTime());
			return client.send(filecast);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	@Override
	public boolean sendAndroidCustomizedcast(PubParams params) {
		// TODO Auto-generated method stub
		try {
			logger.info("umeng push sendAndroidCustomizedcast appkey:"+uemngAndAppKey);
			logger.info("umeng push sendAndroidCustomizedcast params:"+ JSON.toJSONString(params));
			AndroidCustomizedcast customizedcast = new AndroidCustomizedcast(uemngAndAppKey,uemngAndAppMasterSecret);
			if(StringUtils.isBlank(params.getFileId())){
				customizedcast.setAlias(params.getDeviceTokens(), "HEFANTV");
			}else{
				customizedcast.setFileId(params.getFileId(), "HEFANTV");
			}
			customizedcast.setTicker(params.getTicker());
			customizedcast.setTitle(params.getTitle());
			customizedcast.setText(params.getText());
			customizedcast.goAppAfterOpen();
			customizedcast.setDisplayType(AndroidNotification.DisplayType.NOTIFICATION);
			// TODO Set 'production_mode' to 'false' if it's a test device.
			// For how to register a test device, please see the developer doc.
			if(ProductionMode){
				customizedcast.setProductionMode();
			}else{
				customizedcast.setTestMode();
			}
			// Set customized fields
			customizedcast.setExtraField("after_open_type", params.getAfterOpenType()+"");
			if(params.getUpType() > 0){
				customizedcast.setExtraField("up_type", params.getUpType()+"");
				customizedcast.setExtraField("up_value", params.getUpValue());
			}
			if(StringUtils.isNotBlank(params.getStartTime()))
				customizedcast.setPredefinedKeyValue("start_time",params.getStartTime());
			return client.send(customizedcast);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	/**
	 * ios广播
	 * @Title: sendIOSBroadcast
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param params
	 * @param: @return
	 * @param: @throws Exception
	 * @return: boolean
	 * @author: LiTeng
	 * @throws
	 * @date:   2016年8月25日 下午1:15:54
	 */
	@Override
	public boolean sendIOSBroadcast(PubParams params){
		// TODO Auto-generated method stub
		try {
			IOSBroadcast broadcast = new IOSBroadcast(uemngIosAppKey,uemngIosAppMasterSecret);
			broadcast.setAlert(params.getText());
			broadcast.setBadge(0);
			broadcast.setSound("default");
			// TODO set 'production_mode' to 'true' if your app is under production mode
			if(ProductionMode){
				broadcast.setProductionMode();
			}else{
				broadcast.setTestMode();
			}
			// Set customized fields
			broadcast.setCustomizedField("after_open_type", params.getAfterOpenType()+"");
			if(params.getUpType() > 0){
				broadcast.setCustomizedField("up_type", params.getUpType()+"");
				broadcast.setCustomizedField("up_value", params.getUpValue());
			}
			if(StringUtils.isNotBlank(params.getStartTime()))
				broadcast.setPredefinedKeyValue("start_time",params.getStartTime());
			// Set customized fields
			return client.send(broadcast);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	/**
	 * ios单播
	 * @Title: sendIOSUnicast
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param params
	 * @param: @throws Exception
	 * @return: boolean
	 * @author: LiTeng
	 * @throws
	 * @date:   2016年8月25日 下午2:31:03
	 */
	@Override
	public boolean sendIOSUnicast(PubParams params,boolean much){
		// TODO Auto-generated method stub
		try {
			IOSUnicast unicast = new IOSUnicast(uemngIosAppKey,uemngIosAppMasterSecret,much);
			// TODO Set your device token
			unicast.setDeviceToken(params.getDeviceTokens());
			unicast.setAlert(params.getText());
			unicast.setBadge(0);
			unicast.setSound("default");
			// TODO set 'production_mode' to 'true' if your app is under production mode
			if(ProductionMode){
				unicast.setProductionMode();
			}else{
				unicast.setTestMode();
			}
			// Set customized fields
			unicast.setCustomizedField("after_open_type", params.getAfterOpenType()+"");
			if(params.getUpType() > 0){
				unicast.setCustomizedField("up_type", params.getUpType()+"");
				unicast.setCustomizedField("up_value", params.getUpValue());
			}
			if(StringUtils.isNotBlank(params.getStartTime()))
				unicast.setPredefinedKeyValue("start_time",params.getStartTime());
			return client.send(unicast);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	/**
	 * IOS组播
	 * @Title: sendIOSGroupcast
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param params
	 * @param: @return
	 * @param: @throws Exception
	 * @return: boolean
	 * @author: LiTeng
	 * @throws
	 * @date:   2016年8月25日 下午2:48:12
	 */
	@Override
	public boolean sendIOSGroupcast(PubParams params){
		// TODO Auto-generated method stub
		try {
			IOSGroupcast groupcast = new IOSGroupcast(uemngIosAppKey,uemngIosAppMasterSecret);
			JSONObject filterJson = new JSONObject();
			JSONObject whereJson = new JSONObject();
			JSONArray tagArray = new JSONArray();
			for (String tag : params.getTagList()) {
				JSONObject tagParam = new JSONObject();
				tagParam.put("tag", tag);
				tagArray.put(tagParam);
			}
			whereJson.put("and", tagArray);
			filterJson.put("where", whereJson);
			System.out.println(filterJson.toString());

			// Set filter condition into rootJson
			groupcast.setAlert(params.getText());
			groupcast.setBadge(0);
			groupcast.setSound("default");
			// TODO set 'production_mode' to 'true' if your app is under production mode
			if(ProductionMode){
				groupcast.setProductionMode();
			}else{
				groupcast.setTestMode();
			}
			// Set customized fields
			groupcast.setCustomizedField("after_open_type", params.getAfterOpenType()+"");
			if(params.getUpType() > 0){
				groupcast.setCustomizedField("up_type", params.getUpType()+"");
				groupcast.setCustomizedField("up_value", params.getUpValue());
			}
			if(StringUtils.isNotBlank(params.getStartTime()))
				groupcast.setPredefinedKeyValue("start_time",params.getStartTime());
			return client.send(groupcast);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	/**
	 * IOS文件播
	 * @Title: sendIOSFilecast
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param: @param params
	 * @param: @return
	 * @param: @throws Exception
	 * @return: boolean
	 * @author: LiTeng
	 * @throws
	 * @date:   2016年8月25日 下午2:48:12
	 */
	@Override
	public boolean sendIOSFilecast(PubParams params){
		// TODO Auto-generated method stub
		try {
			IOSFilecast filecast = new IOSFilecast(uemngIosAppKey,uemngIosAppMasterSecret);
			// TODO Set your device token
			filecast.setFileId(params.getFileId());
			filecast.setAlert(params.getText());
			filecast.setBadge(0);
			filecast.setSound("default");
			// TODO set 'production_mode' to 'true' if your app is under production mode
			if(ProductionMode){
				filecast.setProductionMode();
			}else{
				filecast.setTestMode();
			}
			// Set customized fields
			filecast.setCustomizedField("after_open_type", params.getAfterOpenType()+"");
			if(params.getUpType() > 0){
				filecast.setCustomizedField("up_type", params.getUpType()+"");
				filecast.setCustomizedField("up_value", params.getUpValue());
			}
			if(StringUtils.isNotBlank(params.getStartTime()))
				filecast.setPredefinedKeyValue("start_time",params.getStartTime());
			return client.send(filecast);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	@Override
	public boolean sendIOSCustomizedcast(PubParams params) {
		// TODO Auto-generated method stub
		try {
			IOSCustomizedcast customizedcast = new IOSCustomizedcast(uemngIosAppKey,uemngIosAppMasterSecret);
			// TODO Set your device token
			if(StringUtils.isBlank(params.getFileId())){
				customizedcast.setAlias(params.getDeviceTokens(), "HEFANTV");
			}else{
				customizedcast.setFileId(params.getFileId(), "HEFANTV");
			}
			customizedcast.setAlert(params.getText());
			customizedcast.setBadge(0);
			customizedcast.setSound("default");
			// TODO set 'production_mode' to 'true' if your app is under production mode
			if(ProductionMode){
				customizedcast.setProductionMode();
			}else{
				customizedcast.setTestMode();
			}
			// Set customized fields
			customizedcast.setCustomizedField("after_open_type", params.getAfterOpenType()+"");
			if(params.getUpType() > 0){
				customizedcast.setCustomizedField("up_type", params.getUpType()+"");
				customizedcast.setCustomizedField("up_value", params.getUpValue());
			}
			if(StringUtils.isNotBlank(params.getStartTime()))
				customizedcast.setPredefinedKeyValue("start_time",params.getStartTime());
			return client.send(customizedcast);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	@Override
	public String upLoadTokenFile(String contents,int modelType){
		// TODO Auto-generated method stub
		if(modelType == 1){
			//and
			try {
				return client.uploadContents(uemngAndAppKey,uemngAndAppMasterSecret, contents);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return "";
			}
		}else if(modelType == 2){
			//IOS
			try {
				return client.uploadContents(uemngIosAppKey,uemngIosAppMasterSecret, contents);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				return "";
			}
		}else{
			return "";
		}
	}

	@Override
	public boolean sendUnicast(PubParams params) {
		// TODO Auto-generated method stub
		boolean au = this.sendAndroidUnicast(params, false);
		boolean iu = this.sendIOSUnicast(params, false);
		if(!au && !iu){
			return false;
		}
		return true;
	}
	@Override
	public boolean sendListcast(PubParams params) {
		// TODO Auto-generated method stub
		boolean al = this.sendAndroidUnicast(params, true);
		boolean il = this.sendIOSUnicast(params, true);
		if(!al && !il){
			return false;
		}
		return true;
	}
	@Override
	public boolean sendBroadcast(PubParams params) {
		// TODO Auto-generated method stub
		boolean ab = this.sendAndroidBroadcast(params);
		boolean ib = this.sendIOSBroadcast(params);
		if(!ab && !ib){
			return false;
		}
		return true;
	}

	@Override
	public boolean sendCustomizedcast(PubParams params) {
		// TODO Auto-generated method stub
		boolean ac = this.sendAndroidCustomizedcast(params);
		boolean ic = this.sendIOSCustomizedcast(params);
		if(!ac && !ic){
			return false;
		}
		return true;
	}

	public static void main(String[] args) {
		UmengPushServiceImpl um = new UmengPushServiceImpl();
		PubParams params = new PubParams();
		params.setFileId("PF079411476441496551");
		params.setTicker("测试Ticker");
		params.setTitle("测试Title");
		params.setText("测试Text");
		try {
			um.sendAndroidFilecast(params);
//			String fileId = um.upLoadTokenFile("AoLYd72HP67aToKUUEuI0oCxWxvpQfN31JXEm81IuVtF\nAqaCrniPXri1C3CYLCtkXY0PUApeBXPEy5STcNlCtvTt\n", 1);
//			System.out.println(fileId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
