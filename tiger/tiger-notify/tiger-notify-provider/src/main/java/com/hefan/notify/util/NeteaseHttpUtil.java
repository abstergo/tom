package com.hefan.notify.util;

import com.cat.common.util.CheckSumBuilder;
import com.cat.common.util.YunXinUtil;
import com.google.common.collect.Maps;
import com.hefan.common.util.PayPropertiesUtils;
import org.apache.http.NameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class NeteaseHttpUtil {

  private static Logger logger = LoggerFactory.getLogger(NeteaseHttpUtil.class);
  private static final String appKey = PayPropertiesUtils.getString("yunxin.appKey");
  private static final String appSecret = PayPropertiesUtils.getString("yunxin.appSecret");

  public static String sendHttpApiUtil(String url,List<NameValuePair> nvps) throws Exception{
    Fetch fetch = new Fetch();
    String result ="";
    Map<String,String> map = Maps.newConcurrentMap();
    try{
      String nonce =  YunXinUtil.Nonce();
      String curTime = String.valueOf((new Date()).getTime() / 1000L);
      String checkSum = CheckSumBuilder.getCheckSum(appSecret, nonce ,curTime);//参考 计算CheckSum的java代码
      String contentType = YunXinUtil.ContentType;
      logger.info("appKey="+appKey);
      logger.info("appSecret="+appSecret);
      logger.info("nonce="+nonce);
      logger.info("checkSum="+checkSum);
      logger.info("contentType="+contentType);

      map.put("AppKey", appKey);
      map.put("Nonce", nonce);
      map.put("CurTime", curTime);
      map.put("CheckSum", checkSum);
      map.put("Content-Type", contentType);

      result = fetch.post(url, map,nvps);
      if(fetch.getStatusCode() == 200){
        logger.info("result==="+result);
        return  result;
      }
    }catch (Exception e){
      e.printStackTrace();
    }
    return result;
  }
}
