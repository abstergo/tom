package com.hefan.notify.dao;

import com.hefan.common.orm.dao.CommonDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017/2/13.
 */
@Repository
public class AppPresentInfoDao extends CommonDaoImpl {

    /**
     * 根据版本类型获取最小的版本号
     * @param type
     * @return
     */
    public int getAppPresentVersion(int type) {
        String sql = "select max(present_version) as presentVersion from app_present_info where delete_flag=0 and type=?";
        Integer value = this.getJdbcTemplate().queryForObject(sql, new Object[]{type}, Integer.class);
        if(value == null) {
            return 0;
        }
        return value.intValue();
    }

    /**
     * 新增新的版本号
     * @param type
     * @param pVersion
     * @return
     */
    public int saveAppPresentInfo(int type, int pVersion) {
        String sql = "insert into app_present_info (type,present_version,create_time) values(?,?,now())";
        return this.getJdbcTemplate().update(sql,new Object[]{type,pVersion});
    }
}
