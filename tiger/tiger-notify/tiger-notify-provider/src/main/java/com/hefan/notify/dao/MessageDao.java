package com.hefan.notify.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cat.tiger.util.CollectionUtils;

/**
 * 消息
 * 
 * @author kevin_zhang
 *
 */
@Repository
public class MessageDao {

	@Resource
	JdbcTemplate jdbcTemplate;

	/**
	 * 获取消息查询规则
	 * 
	 * @return
	 */
	private String getSysRule() {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" WHERE a.isDel=0 AND unix_timestamp(now()) > unix_timestamp(a.msg_send_time) ");
		sqlBuilder.append(" AND a.msg_type= ? ");
		sqlBuilder.append(" AND (a.msg_receive_type=0 ");
		sqlBuilder.append(" OR a.msg_receive_type= ? ");
		sqlBuilder.append(
				" OR (a.msg_receive_type=3 AND a.id in(SELECT msg_id FROM sys_message_relation b WHERE b.user_id=? ) ) ");
		sqlBuilder.append(" ) ");

		return sqlBuilder.toString();
	}

	/**
	 * 获取消息页基础数据（系统消息，活动消息）
	 * 
	 * @param msgType:(0:系统消息
	 *            1:活动消息)
	 * @param userType:(0:普通用户
	 *            1:网红 2:明星／片场)
	 * @param userId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Map queryMsgPageInfo(int msgType, int userType, String userId) {
		List<Map<String, Object>> resutlList;
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT a.title content,DATE_FORMAT(a.time,'%Y-%m-%d %H:%i:%s') time FROM sys_message a ");
		sqlBuilder.append(getSysRule());
		sqlBuilder.append(" ORDER BY a.msg_send_time DESC ");
		sqlBuilder.append(" limit 1 ");

		resutlList = jdbcTemplate.queryForList(sqlBuilder.toString(), msgType, userType, userId);
		return CollectionUtils.isNotEmpty(resutlList) ? resutlList.get(0) : null;
	}

	/**
	 * 获取消息数
	 * 
	 * @param msgType:(0:系统消息
	 *            1:活动消息)
	 * @param userType:(0:普通用户
	 *            1:网红 2:明星／片场)
	 * @param userId
	 * @return
	 */
	public Long queryMsgCount(int msgType, int userType, String userId) {
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append("SELECT count(1) count FROM sys_message a ");
		sqlBuilder.append(getSysRule());

		return jdbcTemplate.queryForObject(sqlBuilder.toString(), new Object[] { msgType, userType, userId },
				Long.class);
	}

	/**
	 * 获取消息列表
	 * 
	 * @param msgType:(0:系统消息
	 *            1:活动消息)
	 * @param userType:(0:普通用户
	 *            1:网红 2:明星／片场)
	 * @param userId
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public List queryMsgList(int msgType, int userType, String userId, int pageNo, int pageSize) {
		List resutlList;
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder
				.append("SELECT id msgId, a.title title, a.simple_content simpleContent, a.cover_image coverImage, a.content content, "
						+ " DATE_FORMAT(a.msg_send_time,'%Y-%m-%d %H:%i:%s') time, a.is_skip isSkip FROM sys_message a ");
		sqlBuilder.append(getSysRule());
		sqlBuilder.append(" ORDER BY a.msg_send_time DESC ");
		sqlBuilder.append(" limit ? , ? ");

		resutlList = jdbcTemplate.queryForList(sqlBuilder.toString(), msgType, userType, userId,
				(pageNo - 1) * pageSize, pageSize);
		return CollectionUtils.isNotEmpty(resutlList) ? resutlList : new ArrayList<>();
	}

	/**
	 * 获取消息详情
	 * 
	 * @param msgId
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Map getMsgDetail(String msgId) {
		List<Map<String, Object>> resutlList;
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder
				.append("SELECT id msgId, a.title title, a.simple_content simpleContent, a.cover_image coverImage, a.content content, "
						+ "DATE_FORMAT(a.msg_send_time,'%Y-%m-%d %H:%i:%s') time, a.is_skip isSkip FROM sys_message a");
		sqlBuilder.append(" WHERE isDel=0 AND id= ? ");

		resutlList = jdbcTemplate.queryForList(sqlBuilder.toString(), msgId);
		return CollectionUtils.isNotEmpty(resutlList) ? resutlList.get(0) : null;
	}
}