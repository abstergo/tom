package com.hefan.notify.util;

import com.cat.common.constant.AliSmsConstants;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.common.meta.SmsCodeTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kevin_zhang on 08/12/2016.
 */
public class SmsSendUtil {

	public static Logger logger = LoggerFactory.getLogger(SmsSendUtil.class);

	// 登录发送验证码
	public static final String loginSmsTemplate = "【盒饭LIVE】验证码%s，您正在登录%s，若非本人操作，请勿泄露。";

	// 身份认证发送验证码短信配置
	public static final String identitySmsTemplate = "【盒饭LIVE】验证码%s，您正在进行%s身份验证，打死不要告诉别人哦！";

	// 变更手机号短信配置信息
	public static final String changeMobileSmsTemplate = "【盒饭LIVE】验证码%s，您正在尝试变更%s重要信息，请妥善保管账户信息。";

	// 修改密码
	public static final String modifyPassworSmsTemplate = "【盒饭LIVE】验证码%s，您正在尝试修改%s登录密码，请妥善保管账户信息。";

	/**
	 * 获取发送短信内容
	 *
	 * @param smsCodeTypeEnum
	 * @param codeStr
	 * @return
	 */
	public static String getSmsContent(SmsCodeTypeEnum smsCodeTypeEnum, String codeStr) {
		if (smsCodeTypeEnum == SmsCodeTypeEnum.LOGIN_SMS_CODE_TYPE) {
			// 登陆手机验证码类型
			return String.format(SmsSendUtil.loginSmsTemplate,
					new Object[] { codeStr, AliSmsConstants.bigFishSmsFreeSignNameLogin });
		} else if (smsCodeTypeEnum == SmsCodeTypeEnum.IDENTITY_SMS_CODE_TYPE) {
			// 身份验证验证码类型
			return String.format(SmsSendUtil.identitySmsTemplate,
					new Object[] { codeStr, AliSmsConstants.bigFishSmsFreeSignIdentity });
		} else if (smsCodeTypeEnum == SmsCodeTypeEnum.CHANGE_SMS_CODE_TYPE) {
			// 变更|绑定手机号验证码类型
			return String.format(SmsSendUtil.changeMobileSmsTemplate,
					new Object[] { codeStr, AliSmsConstants.bigFishSmsFreeSignNameChangeMobile });
		} else if (smsCodeTypeEnum == SmsCodeTypeEnum.STAR_MODIFY_PASSWOR_SMS_CODE_TYPE) {
			// 明星后台修改密码发短信
			return String.format(SmsSendUtil.modifyPassworSmsTemplate,
					new Object[] { codeStr, AliSmsConstants.bigFishSmsFreeSignStarModifyPasswor });
		}
		return "";
	}

	/**
	 * 短信发送
	 *
	 * @param smsCodeTypeEnum
	 * @param codeStr
	 * @param phone
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ResultBean sendSms(SmsCodeTypeEnum smsCodeTypeEnum, String codeStr, String phone) {
		ResultBean resultBean = new ResultBean(ResultCode.SUCCESS, codeStr);
		resultBean = sendSmsByAli(smsCodeTypeEnum, codeStr, phone);
		if (null != resultBean && resultBean.getCode() != ResultCode.SUCCESS.get_code()) {
			resultBean = sendSmsByZucp(smsCodeTypeEnum, codeStr, phone);
		}
		return resultBean;
	}

	/**
	 * 短信发送_Ali
	 *
	 * @param smsCodeTypeEnum
	 * @param codeStr
	 * @param phone
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ResultBean sendSmsByAli(SmsCodeTypeEnum smsCodeTypeEnum, String codeStr, String phone) {
		try {
			Map<String, String> smsParamString = new HashMap();
			smsParamString.put("code", codeStr);
			smsParamString.put("product", AliSmsConstants.productName);
			// 调用ALI短信发送
			String flag = AliSendSmsUtil.loginSign(smsCodeTypeEnum.getSmsFreeSignName(), smsParamString, phone,
					smsCodeTypeEnum.getSmsTemplateCode());
			if ("success".equals(flag)) {
				return new ResultBean(ResultCode.SUCCESS, codeStr);
			} else {
				return new ResultBean(ResultCode.SendSmsCodeFial, "短信发送结果：" + flag);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("短信发送失败");
			return new ResultBean(ResultCode.UNSUCCESS, null);
		}
	}

	/**
	 * 短信发送_Zucp
	 *
	 * @param smsCodeTypeEnum
	 * @param codeStr
	 * @param phone
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ResultBean sendSmsByZucp(SmsCodeTypeEnum smsCodeTypeEnum, String codeStr, String phone) {
		try {
			String result = ZucpSendSmsUtil.mt(phone, getSmsContent(smsCodeTypeEnum, codeStr), "", "",
					smsCodeTypeEnum.getSmsTemplateCode());
			if (null != result && result.equals(smsCodeTypeEnum.getSmsTemplateCode())) {
				return new ResultBean(ResultCode.SUCCESS, codeStr);
			} else {
				return new ResultBean(ResultCode.SendSmsCodeFial, "短信发送结果：" + result);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("短信发送失败");
			return new ResultBean(ResultCode.UNSUCCESS, null);
		}
	}
}
