package com.hefan.notify.dao;

import java.util.ArrayList;
import java.util.List;
import com.cat.tiger.util.CollectionUtils;
import com.hefan.common.orm.dao.CommonDaoImpl;
import com.hefan.notify.bean.SettingVo;

/**
 * 系统配置
 * 
 * @author kevin_zhang
 *
 */
public class SettingDao extends CommonDaoImpl {

	public List<SettingVo> querySettingList() {
		List<SettingVo> resutlList = query(
				"SELECT id, config_key configKey, confilg_value confilgValue, delete_flag deleteFlag, "
						+ " DATE_FORMAT(create_time,'%Y-%m-%d %H:%i:%s') createTime, DATE_FORMAT(update_time,'%Y-%m-%d %H:%i:%s') updateTime FROM setting  WHERE delete_flag=0",
				new Object[] {}, SettingVo.class);
		return CollectionUtils.isNotEmpty(resutlList) ? resutlList : new ArrayList<>();
	}
}
