package com.hefan.notify.service;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.common.util.YunXinUtil;
import com.hefan.notify.itf.ImUserService;
import com.hefan.notify.util.NeteaseHttpUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("imUserService")
public class ImUserServiceImpl implements ImUserService {
	
	private Logger logger = LoggerFactory.getLogger(ImUserServiceImpl.class);

	@Override
	public ResultBean createUser(String accid,String name,String props,String icon,String token) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.createUser;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(StringUtils.isBlank(accid)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("云信ID不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("accid", accid));
			if(StringUtils.isNotBlank(name)){
				nvps.add(new BasicNameValuePair("name", name));
			}
			if(StringUtils.isNotBlank(props)){
				nvps.add(new BasicNameValuePair("props", props));
			}
			if(StringUtils.isNotBlank(icon)){
				nvps.add(new BasicNameValuePair("icon", icon));
			}
			if(StringUtils.isNotBlank(token)){
				nvps.add(new BasicNameValuePair("token", token));
			}
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				//获取token
				String info = JSON.parseObject(result).getString("info");
				String tokenRes = JSON.parseObject(info).getString("token");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(tokenRes);
			}else{
				if(code !=null && code.equals("414") && JSON.parseObject(result).getString("desc").equals("already register")){
					res.setCode(ResultCode.ImAlreadyRegistered.get_code());
					res.setMsg(JSON.parseObject(result).getString("desc"));
				}else{
					res.setCode(ResultCode.UNSUCCESS.get_code());
					res.setMsg(JSON.parseObject(result).getString("desc"));	
				}
				
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("云信账号创建失败：云信id"+accid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
		
	}

	@Override
	public ResultBean updateUser(String accid,String props,String token) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.updateUser;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(StringUtils.isBlank(accid)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("云信ID不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("accid", accid));
			if(StringUtils.isNotBlank(props)){
				nvps.add(new BasicNameValuePair("props", props));
			}
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				res.setCode(ResultCode.SUCCESS.get_code());
			}else{
				logger.info("云信账号更新失败：云信id"+accid+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("云信账号更新失败：云信id"+accid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean refreshToken(String accid) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.refreshToken;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(StringUtils.isBlank(accid)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("云信ID不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("accid", accid));
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				//获取token
				String info = JSON.parseObject(result).getString("info");
				String tokenRes = JSON.parseObject(info).getString("token");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(tokenRes);
			}else{
				logger.info("云信账号更新token失败：云信id"+accid+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("云信账号更新token失败：云信id"+accid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean blockUser(String accid,String needkick) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.blockUser;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(StringUtils.isBlank(accid)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("云信ID不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("accid", accid));
			if(StringUtils.isNotBlank(needkick)){
				nvps.add(new BasicNameValuePair("needkick", needkick));
			}
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				res.setCode(ResultCode.SUCCESS.get_code());
			}else{
				logger.info("云信账号封禁失败：云信id"+accid+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("云信账号封禁失败：云信id"+accid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean unblockUser(String accid) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.unblockUser;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(StringUtils.isBlank(accid)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("云信ID不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("accid", accid));
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				res.setCode(ResultCode.SUCCESS.get_code());
			}else{
				logger.info("云信账号解禁失败：云信id"+accid+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("云信账号解禁失败：云信id"+accid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}
	
	public static void main(String[] args) {
		ImUserServiceImpl se = new ImUserServiceImpl();
		ResultBean res = se.createUser("11", "11", "", "", "");
		System.out.println(JSON.toJSONString(res));
	}

}
