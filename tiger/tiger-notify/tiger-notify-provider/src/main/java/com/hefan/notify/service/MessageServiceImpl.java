package com.hefan.notify.service;

import java.util.*;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSON;
import com.cat.tiger.service.JedisService;
import com.hefan.notify.bean.SysMsgVo;
import com.hefan.notify.constant.RedisKeyConstant;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cat.common.entity.Page;
import com.hefan.notify.dao.MessageDao;
import com.hefan.notify.itf.MessageService;

/**
 * 消息
 *
 * @author kevin_zhang
 */
@Component("messageService")
public class MessageServiceImpl implements MessageService {
    public static Logger logger = LoggerFactory.getLogger(MessageServiceImpl.class);

    @Resource
    MessageDao messageDao;

    @Resource
    JedisService jedisService;

    /**
     * 获取未读消息数
     *
     * @param userId
     * @return
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public SysMsgVo getMsgCount(String userId) {
        SysMsgVo vo = null;
        try {
            String infoRedis = jedisService.getStr(RedisKeyConstant.MSG_CENTER_KEY + userId);
            if (StringUtils.isNotBlank(infoRedis)) {
                logger.info("获取用户中心未读消息数操作 获取缓存消息数信息：" + infoRedis);
                vo = JSON.parseObject(infoRedis, SysMsgVo.class);
            }
            if (null == vo) {
                logger.info("获取用户中心未读消息数操作 未获取缓存消息数信息！");
                vo = new SysMsgVo();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("获取用户中心未读消息数操作 获取关注数异常");
            vo = new SysMsgVo();
        } finally {
            return vo;
        }
    }

    /**
     * 获取消息主页信息(系统消息，活动消息)
     *
     * @param msgType:(0:系统消息  1:活动消息)
     * @param userId
     * @param userType:(0:普通用户 1:网红 2:明星／片场)
     * @return
     */
    @SuppressWarnings({"rawtypes"})
    @Override
    public Map getMsgPageInfo(int msgType, String userId, int userType) {
        return messageDao.queryMsgPageInfo(msgType, userType, userId);
    }

    /**
     * 清空消息数
     *
     * @param userId
     * @param clearMsgType 0：清空我的消息  1：清空新增好友消息数
     * @return
     */
    @Override
    public void cleanMsgCount(String userId, int clearMsgType) {
        SysMsgVo vo = null;
        try {
            String infoRedis = jedisService.getStr(RedisKeyConstant.MSG_CENTER_KEY + userId);
            if (StringUtils.isNotBlank(infoRedis)) {
                logger.info("清空消息数操作 获取缓存消息数信息：" + infoRedis);
                vo = JSON.parseObject(infoRedis, SysMsgVo.class);
            }
            if (null == vo) {
                vo = new SysMsgVo();
            }
            if (clearMsgType == 0) {
                vo.setActivemessagecount(0);
                vo.setSysmessagecount(0);
                vo.setCommetmessagecount(0);
                vo.setPresentmessagecount(0);
            } else if (clearMsgType == 1) {
                vo.setNewFansCount(0);
                jedisService.del("fans_add_count_" + userId);
            }
            vo.setTotalMsgCount();
            vo.setTotalSysMsgCount();
            logger.info("清空消息数操作 清空操作后的消息数信息：" + JSON.toJSONString(vo));

            jedisService.setStr(RedisKeyConstant.MSG_CENTER_KEY + userId, JSON.toJSONString(vo));
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("清空消息数操作 清空消息数操作异常");
        }
    }

    /**
     * 获取系统消息列表
     *
     * @param page
     * @param userId
     * @param userType:(0:普通用户 1:网红 2:明星／片场)
     * @return
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public Page getSysMsgList(Page page, String userId, int userType) {
        Long count = messageDao.queryMsgCount(0, userType, userId);
        page.setTotalItems(count);
        page.setResult(messageDao.queryMsgList(0, userType, userId, page.getPageNo(), page.getPageSize()));
        return page;
    }

    /**
     * 获取活动消息列表
     *
     * @param page
     * @param userId
     * @param userType:(0:普通用户 1:网红 2:明星／片场)
     * @return
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    @Override
    public Page getActivityMsgList(Page page, String userId, int userType) {
        Long count = messageDao.queryMsgCount(1, userType, userId);
        page.setTotalItems(count);
        page.setResult(messageDao.queryMsgList(1, userType, userId, page.getPageNo(), page.getPageSize()));
        return page;
    }

    /**
     * 获取消息详情
     *
     * @param msgId
     * @return
     */
    @SuppressWarnings("rawtypes")
    @Override
    public Map getMsgDetail(String msgId) {
        return messageDao.getMsgDetail(msgId);
    }

    /**
     * 增加指定消息数
     *
     * @param updateTypeValus:(1:系统消息 2:活动消息 3:评论消息数 4:赞数 5:礼物数) （不在支持4:赞数）
     * @param userId
     * @return
     */
    @Override
    public int addMsgCout(int updateTypeValus, String userId) {
        SysMsgVo vo = getMsgCount(userId);
        if (updateTypeValus == 1)
            vo.setSysmessagecount(vo.getSysmessagecount() + 1);
        if (updateTypeValus == 2)
            vo.setActivemessagecount(vo.getActivemessagecount() + 1);
        if (updateTypeValus == 3)
            vo.setCommetmessagecount(vo.getCommetmessagecount() + 1);
        if (updateTypeValus == 5)
            vo.setPresentmessagecount(vo.getPresentmessagecount() + 1);
        vo.setTotalMsgCount();
        int result = 1;
        try {
            jedisService.setStr(RedisKeyConstant.MSG_CENTER_KEY + userId, JSON.toJSONString(vo));
            result = 1;
            logger.info("增加指定消息数操作 增加指定消息数操作成功");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("增加指定消息数操作 增加指定消息数操作异常");
            result = 0;
        } finally {
            return result;
        }
    }

    /**
     * 清空所有用户消息缓存（慎重操作）
     */
    @Override
    public void cleanAllMsgCache() {
        int result = 1;
        try {
            Set<String> setList = jedisService.keys(RedisKeyConstant.MSG_CENTER_KEY + "*");
            if (null != setList && setList.size() > 0) {
                Iterator<String> it = setList.iterator();
                while (it.hasNext()) {
                    jedisService.del(it.next());
                    result++;
                }
            }
            logger.info("清空所有用户消息缓存操作 一共清空 " + result + " 记录");
            logger.info("清空所有用户消息缓存操作 清空所有用户消息缓存操作成功");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("清空所有用户消息缓存操作 清空所有用户消息缓存操作异常");
        }
    }
}