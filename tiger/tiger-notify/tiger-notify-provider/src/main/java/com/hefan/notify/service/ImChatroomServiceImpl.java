package com.hefan.notify.service;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.notify.itf.ImChatroomService;
import com.hefan.notify.util.NeteaseHttpUtil;
import com.cat.common.util.YunXinUtil;

@Component("imChatroomService")
public class ImChatroomServiceImpl implements ImChatroomService{
	
	private Logger logger = LoggerFactory.getLogger(ImChatroomServiceImpl.class);

	@Override
	public ResultBean createChatroom(String creator,String name,
			String announcement,String broadcasturl,
			String ext) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.createChatroom;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(StringUtils.isBlank(creator)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("聊天室属主的账号不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("creator", creator));
			if(StringUtils.isBlank(name)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("聊天室名称不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("name", name));
			if(StringUtils.isNotBlank(announcement)){
				nvps.add(new BasicNameValuePair("announcement", announcement));
			}
			if(StringUtils.isNotBlank(broadcasturl)){
				nvps.add(new BasicNameValuePair("broadcasturl", broadcasturl));
			}
			if(StringUtils.isNotBlank(ext)){
				nvps.add(new BasicNameValuePair("ext", ext));
			}
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				String chatroom =  JSON.parseObject(result).getString("chatroom");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(chatroom);
			}else{
				logger.info("云信账号创建聊天室失败：云信id"+creator+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("云信账号创建聊天室失败：云信id"+creator+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean getChatroom(long roomid,String needOnlineUserCount) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.getChatroom;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(roomid<=0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("聊天室id不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("roomid", String.valueOf(roomid)));
			if(StringUtils.isNotBlank(needOnlineUserCount)){
				nvps.add(new BasicNameValuePair("needOnlineUserCount", needOnlineUserCount));
			}
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				String chatroom =  JSON.parseObject(result).getString("chatroom");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(chatroom);
			}else{
				logger.info("查询聊天室信息失败：聊天室"+roomid+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("查询聊天室信息失败：聊天室"+roomid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean updateChatroom(long roomid,String name,
			String announcement,String broadcasturl,
			String ext,String needNotify,
			String notifyExt) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.updateChatroom;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(roomid<=0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("聊天室id不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("roomid", String.valueOf(roomid)));
			if(StringUtils.isNotBlank(name)){
				nvps.add(new BasicNameValuePair("name", name));
			}
			if(StringUtils.isNotBlank(announcement)){
				nvps.add(new BasicNameValuePair("announcement", announcement));
			}
			if(StringUtils.isNotBlank(broadcasturl)){
				nvps.add(new BasicNameValuePair("broadcasturl", broadcasturl));
			}
			if(StringUtils.isNotBlank(ext)){
				nvps.add(new BasicNameValuePair("ext", ext));
			}
			if(StringUtils.isNotBlank(needNotify)){
				nvps.add(new BasicNameValuePair("needNotify", needNotify));
			}
			if(StringUtils.isNotBlank(notifyExt)){
				nvps.add(new BasicNameValuePair("notifyExt", notifyExt));
			}
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				String chatroom =  JSON.parseObject(result).getString("chatroom");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(chatroom);
			}else{
				logger.info("更新聊天室信息失败：聊天室"+roomid+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("更新聊天室信息失败：聊天室"+roomid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean toggleCloseStat(long roomid,String operator,
			String valid) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.toggleCloseStat;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(roomid<=0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("聊天室id不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("roomid", String.valueOf(roomid)));
			if(StringUtils.isBlank(operator)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("操作者账号不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("operator", operator));
			if(StringUtils.isBlank(valid)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("开关标示不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("valid", valid));
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				String desc =  JSON.parseObject(result).getString("desc");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(desc);
			}else{
				logger.info("开/关聊天室信息失败：聊天室"+roomid+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("开/关聊天室信息失败：聊天室"+roomid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean setMemberRole(long roomid,String operator,
			String target,String opt,String optvalue,String notifyExt) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.setMemberRole;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(roomid<=0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("聊天室id不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("roomid", String.valueOf(roomid)));
			if(StringUtils.isBlank(operator)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("操作者账号不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("operator", operator));
			if(StringUtils.isBlank(target)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("被操作者账号不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("target", target));
			if(StringUtils.isBlank(opt)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("操作不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("opt", opt));
			if(StringUtils.isBlank(optvalue)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("设置/取消不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("optvalue", optvalue));
			if(StringUtils.isNotBlank(notifyExt)){
				nvps.add(new BasicNameValuePair("notifyExt", notifyExt));
			}
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				String desc =  JSON.parseObject(result).getString("desc");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(desc);
			}else{
				logger.info("设置聊天室角色失败：聊天室"+roomid+"目标账号"+target+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("设置聊天室角色失败：聊天室"+roomid+"目标账号"+target+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean chatroomSendMsg(long roomid,String fromAccid,int msgType,
			int resendFlag,String attach,String ext) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.chatroomSendMsg;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(roomid<=0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("聊天室id不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("roomid", String.valueOf(roomid)));
			nvps.add(new BasicNameValuePair("msgId", UUID.randomUUID().toString()));
			if(StringUtils.isBlank(fromAccid)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("消息发出者的账号不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("fromAccid", fromAccid));
			if(msgType<0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("消息类型不合法");
				return res;
			}
			nvps.add(new BasicNameValuePair("msgType", String.valueOf(msgType)));
			if(resendFlag>=0){
				nvps.add(new BasicNameValuePair("resendFlag", String.valueOf(resendFlag)));
			}
			if(StringUtils.isNotBlank(attach)){
				nvps.add(new BasicNameValuePair("attach", attach));
			}
			if(StringUtils.isNotBlank(ext)){
				nvps.add(new BasicNameValuePair("ext", ext));
			}
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				String desc =  JSON.parseObject(result).getString("desc");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(desc);
			}else{
				logger.info("聊天室消息发送失败：聊天室"+roomid+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("聊天室消息发送失败：聊天室"+roomid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean temporaryMute(long roomid,String operator,
			String target,String muteDuration,String needNotify,String notifyExt) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.temporaryMute;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(roomid<=0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("聊天室id不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("roomid", String.valueOf(roomid)));
			if(StringUtils.isBlank(operator)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("操作者账号不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("operator", operator));
			if(StringUtils.isBlank(target)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("被禁言的目标账号不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("target", target));
			if(StringUtils.isBlank(muteDuration) || Integer.parseInt(muteDuration)<0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("禁言时间不合法");
				return res;
			}
			nvps.add(new BasicNameValuePair("muteDuration", String.valueOf(muteDuration)));
			if(StringUtils.isNotBlank(needNotify)){
				nvps.add(new BasicNameValuePair("needNotify", needNotify));
			}
			if(StringUtils.isNotBlank(notifyExt)){
				nvps.add(new BasicNameValuePair("notifyExt", notifyExt));
			}
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				String desc =  JSON.parseObject(result).getString("desc");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(desc);
			}else{
				logger.info("聊天室禁言失败：聊天室"+roomid+"目标账号"+target+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("聊天室禁言失败：聊天室"+roomid+"目标账号"+target+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean addRobot(long roomid,JSONArray accids,
			String roleExt,String notyfyExt) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.addRobot;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(roomid<=0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("聊天室id不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("roomid", String.valueOf(roomid)));
			if(accids ==null || accids.size()<0 || accids.size()>100){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("机器人账号不合法，一次添加上线100个");
				return res;
			}
			nvps.add(new BasicNameValuePair("accids", URLEncoder.encode(accids.toString(), "utf-8")));
			if(StringUtils.isNotBlank(roleExt)){
				nvps.add(new BasicNameValuePair("roleExt", roleExt));
			}
			if(StringUtils.isNotBlank(notyfyExt)){
				nvps.add(new BasicNameValuePair("notyfyExt", notyfyExt));
			}
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				String desc =  JSON.parseObject(result).getString("desc");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(desc);
			}else{
				logger.info("聊天室添加机器人失败：聊天室"+roomid+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("聊天室添加机器人失败：聊天室"+roomid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

	@Override
	public ResultBean removeRobot(long roomid,JSONArray accids) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.removeRobot;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(roomid<=0){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("聊天室id不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("roomid", String.valueOf(roomid)));
			if(accids ==null || accids.size()<0 || accids.size()>100){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("机器人账号不合法，一次删除上线100个");
				return res;
			}
			nvps.add(new BasicNameValuePair("accids", URLEncoder.encode(accids.toString(), "utf-8")));
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				String desc =  JSON.parseObject(result).getString("desc");
				res.setCode(ResultCode.SUCCESS.get_code());
				res.setData(desc);
			}else{
				logger.info("聊天室删除机器人失败：聊天室"+roomid+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("聊天室删除机器人失败：聊天室"+roomid+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}


	public static void main(String[] args) {
		ImChatroomServiceImpl service = new ImChatroomServiceImpl();
		ResultBean res = service.createChatroom("7191567", "7191567", "", "", "");
		System.out.println(JSON.toJSONString(res));
	}
}
