package com.hefan.notify.constant;

/**
 * Created by kevin_zhang on 17/03/2017.
 */
public class RedisKeyConstant {

    /**
     * 系统消息数
     * key：msg_center_'userId'
     * value：{"sysmessagecount”:2,”activemessagecount”:5,“commetmessagecount”:4,”presentmessagecount”:7}
     */
    public final static String MSG_CENTER_KEY = "msg_center_";
}
