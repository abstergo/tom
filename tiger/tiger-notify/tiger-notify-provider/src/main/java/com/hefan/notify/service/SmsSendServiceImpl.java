package com.hefan.notify.service;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.common.meta.SmsCodeTypeEnum;
import com.cat.common.util.RandomUtil;
import com.cat.tiger.service.JedisService;
import com.cat.tiger.util.GlobalConstants;
import com.hefan.common.util.DynamicProperties;
import com.hefan.common.util.MapUtils;
import com.hefan.notify.util.SmsSendUtil;
import com.hefan.notify.config.SmsConfigCenter;
import com.hefan.notify.itf.SmsSendService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.ws.rs.*;
import java.util.Map;

/**
 * Created by lxw on 2016/9/28.
 */
@Path("/sms")
@Component("smsSendService")
public class SmsSendServiceImpl implements SmsSendService {
    private Logger logger = LoggerFactory.getLogger(SmsSendServiceImpl.class);

    @Resource
    private SmsConfigCenter smsConfigCenter;
    @Resource
    JedisService jedisService;

    @SuppressWarnings({"rawtypes", "unchecked"})
    @Override
    public ResultBean aliSendSmsCode(int smsType, String moblie) {
        try {
            if (DynamicProperties.getBoolean("test.open")) {
                return new ResultBean(ResultCode.SUCCESS, "测试环境不发送，直接成功！");
            }
            return sendMsg(moblie, smsType);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResultBean(ResultCode.UnknownException, null);
        }
    }

    @SuppressWarnings("rawtypes")
    @Override
    public ResultBean webReqAliSendSmsCode(String moblie, int smsType) {
        return sendMsg(moblie, smsType);
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    private ResultBean sendMsg(String moblie, int smsType) {
        try {
            SmsCodeTypeEnum smsEnumByType = SmsCodeTypeEnum.getEnumByType(smsType);
            if (StringUtils.isBlank(moblie) || smsEnumByType == null) {
                return new ResultBean(ResultCode.SmsCodeParamError, null);
            }
            String code = String.valueOf(RandomUtil.getPhoneSignCode());

            Map<String, String> map = smsConfigCenter.getPublicConfig();
            int smsTypeConfig = MapUtils.getIntValue(map, "smsType", -1);
            // 0：双短信通道  1：阿里大鱼  2:zucp
            if (smsTypeConfig == 1) {
                logger.info("阿里大鱼短信发送");
                return SmsSendUtil.sendSmsByAli(smsEnumByType, code, moblie);
            } else if (smsTypeConfig == 2) {
                logger.info("漫道大鱼短信发送");
                return SmsSendUtil.sendSmsByZucp(smsEnumByType, code, moblie);
            } else {
                logger.info("双通道短信发送");
                String result = jedisService.getStr("Sms_" + moblie);
                if (StringUtils.isBlank(result)) {
                    logger.info("阿里大鱼短信发送初次尝试");
                    jedisService.setexStr("Sms_" + moblie, "1", 24 * 60 * 60);// 1day
                    return SmsSendUtil.sendSmsByAli(smsEnumByType, code, moblie);
                } else if (result.equals("1")) {
                    jedisService.setStr("Sms_" + moblie, "2");
                    logger.info("漫道大鱼短信发送尝试");
                    return SmsSendUtil.sendSmsByZucp(smsEnumByType, code, moblie);
                } else if (result.equals("2")) {
                    jedisService.setStr("Sms_" + moblie, "1");
                    logger.info("阿里大鱼短信发送尝试");
                    return SmsSendUtil.sendSmsByAli(smsEnumByType, code, moblie);
                }
                return SmsSendUtil.sendSms(smsEnumByType, code, moblie);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return new ResultBean(ResultCode.UnknownException, null);
        }
    }
}
