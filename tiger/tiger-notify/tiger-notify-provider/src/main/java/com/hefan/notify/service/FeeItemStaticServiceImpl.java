package com.hefan.notify.service;

import com.alibaba.fastjson.JSON;
import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.entity.ResultPojo;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.service.JedisService;
import com.cat.tiger.util.GlobalConstants;
import com.google.common.collect.Maps;
import com.hefan.common.oss.OssImageService;
import com.hefan.common.util.DynamicProperties;
import com.hefan.live.bean.LivingRoomInfoVo;
import com.hefan.live.itf.LivingListService;
import com.hefan.live.itf.LivingRedisOptService;
import com.hefan.live.itf.RoomEnterExitOptService;
import com.hefan.notify.dao.AppPresentInfoDao;
import com.hefan.notify.dao.ItemStaticDao;
import com.hefan.notify.itf.ItemsStaticService;
import com.hefan.user.bean.WebUser;
import com.hefan.user.itf.UserAuthService;
import com.hefan.user.itf.WebUserService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.ByteArrayInputStream;
import java.util.*;

/**
 * User: criss Date: 16/9/20 Time: 08:34
 */
@Path("/static")
@Component("feeItemsStaticService")
public class FeeItemStaticServiceImpl implements ItemsStaticService {

	public static Logger logger = LoggerFactory.getLogger(FeeItemStaticServiceImpl.class);
	@Resource
	private ItemStaticDao feeItemStaticDao;

	@Resource
	private AppPresentInfoDao appPresentInfoDao;

	@Resource
	OssImageService ossImageService;
	
	@Resource
    UserAuthService userAuthService;
	
	@Resource
	JedisService jedisService;

	private static String PATH = DynamicProperties.getString("oss_static_path");
	private static String FEEITEMNAMEPATH = PATH + "FeeItem.js";
	private static String ADVERTISENAMEPATH = PATH + "Advertise.js";

	private static String DYNAMICPRESENTPATH = PATH + "DynamicPresent.js";
	private static String LIVEPRESENTPATH = PATH + "LivePresent.js";
	private static String VERSIONPATH = PATH + "Version.js";
	private static String LIVE_VERSION_PATH = PATH + "liveVersion.js";
	private static String DYNAMIC_VERSION_PATH = PATH + "dynameicVersion.js";

	private static int ANDRIODTYPE = 0;
	private static int IOSTYPE = 1;

	private static int barrageType = 2;
	private static int letterPrice = 1;

	private static String ANDRIODPATH = PATH + "AppInfoForandriod.js";
	private static String IOSPATH = PATH + "AppInfoForIos.js";
	private static String IOSMAJIABAOPATH = PATH + "AppInfoForIos_%d.js";

	private static String RANKINGLISTPATH = PATH + "RankingList.js";
	private static String LISTHOTINDEXPATH = PATH + "listHotIndex.js";
	private static String PEOPLECOUNTPATH = PATH + "PeopleCount.js";
	private static String BANNERPATH = PATH + "banner.js";
	private static String LISTNEWLIVEPATH = PATH + "listNewLive.js";
	private static String FANSCONTRIBUTIONLISTPATH = PATH + "fansContributionList_";
	private static String JOINLIVEROOMWORDPATH = PATH + "joinLiveRoomWord.js";

	// 自定义表情资源文件
	private static String CUSTOMEXPRESSIONPATH = PATH + "CustomExpression.js";

	// 有异议
	private static String BIGVERSIONPATH = PATH + "BigVersion.js";

	private static String LIVENOTICEPATH = PATH + "liveNotice.js";
	private static String LIVEBOOKINGLISTPATH = PATH + "liveBookingList.json";

	// 敏感词
	private static String SENSITIVEWORD = PATH + "SensitiveWord.txt";

	// 广场最新动态ID（小红点）
	private static String SQUARERED = PATH + "squareLatest.js";
	
	@Resource
	LivingListService livingListService;
	@Resource
	RoomEnterExitOptService roomEnterExitOptService;
	@Resource
	WebUserService webUserService;
	@Resource
	LivingRedisOptService livingRedisOptService;

	@Override
	@GET
	@Path("/feeItem")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int FeeItemStaticService() throws Exception {
		// TODO Auto-generated method stub
		ResultBean resultBean = new ResultBean();

		try {

			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setData(feeItemStaticDao.findFee());
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), FEEITEMNAMEPATH);
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("收费项查询出错");
			e.printStackTrace();
			return 400;
		}
	}

	//
	@GET
	@Path("/advertise")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public int AdvertiseService() throws Exception {
		// TODO Auto-generated method stub
		ResultBean resultBean = new ResultBean();
		try {
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			resultBean.setData(feeItemStaticDao.advertiseFee());
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), ADVERTISENAMEPATH);
			logger.info("广告文件SUCCESS");
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("广告查询出错");
			e.printStackTrace();
			return 400;
		}
	}

	@GET
	@Path("/present")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int present() throws Exception {
		// TODO Auto-generated method stub

		ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
		try {
			/*// 动态礼物
			resultBean.setData(feeItemStaticDao.getDynamicPresent());
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), DYNAMICPRESENTPATH);*/
			// 礼物版本
			int version = appPresentInfoDao.getAppPresentVersion(-1);
			Map versionMap = new HashMap();
			versionMap.put("Version",version);
			resultBean.setData(versionMap);
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), VERSIONPATH);
			/*// 大礼物特效
			resultBean.setData(feeItemStaticDao.getBigVersion());
			buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), BIGVERSIONPATH);*/

			logger.info("礼物，版本文件SUCCESS");
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("礼物查询出错");
			e.printStackTrace();
			return 400;
		}
	}

	@GET
	@Path("/presentResourceCreater")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int presentResourceCreater (@QueryParam("status") int status) {
		ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
		Map versionMap = new HashMap();
		try {
			// 礼物版本
			int version = appPresentInfoDao.getAppPresentVersion(status);
			resultBean.setData(feeItemStaticDao.getPresentByStatus(status));
			logger.info("presentResourceCreater------version:"+version);
			if(status == 0) {//直播间礼物
				// 直播间礼物列表
				ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
				ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), LIVEPRESENTPATH);
				logger.info("直播间礼物列表文件路径------"+LIVEPRESENTPATH);
				//直播间礼物版本
				versionMap.put("Version",version);
				resultBean.setData(versionMap);
				buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
				ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), LIVE_VERSION_PATH);
				logger.info("直播间版本文件路径------"+LIVE_VERSION_PATH);
			} else if(status == 1) { //俱乐部礼物
				// 俱乐部礼物列表
				ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
				ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), DYNAMICPRESENTPATH);
				logger.info("俱乐部礼物列表文件路径------"+DYNAMICPRESENTPATH);
				//直播间礼物版本
				versionMap.put("Version",version);
				resultBean.setData(versionMap);
				buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
				ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), DYNAMIC_VERSION_PATH);
				logger.info("俱乐部间版本文件路径------"+DYNAMIC_VERSION_PATH);
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("礼物查询出错");
			e.printStackTrace();
			return 400;
		}
		return 200;
	}

	@GET
	@Path("/updatePresent")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int updatePresent(@QueryParam("status") int status) throws Exception {
		logger.info("updatePresent-----status:"+status);
		ResultBean resultBean = new ResultBean();
		try {
			
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			/*feeItemStaticDao.updatePresentVersion(
					Integer.valueOf(feeItemStaticDao.getPresentVersion().get("Version").toString()) + 1);*/
			appPresentInfoDao.saveAppPresentInfo(-1,appPresentInfoDao.getAppPresentVersion(-1)+1);
			appPresentInfoDao.saveAppPresentInfo(status,appPresentInfoDao.getAppPresentVersion(status)+1);
			//兼容老版本的全局礼物版本号
			present();
			//新版本根据礼物分类生成 礼物版本和礼物列表资源文件
			presentResourceCreater(status);
			logger.info("updatePresent-----success");
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("礼物查询出错");
			e.printStackTrace();
			return 400;
		}
	}

	@GET
	@Path("/rankingList")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int rankingList() throws Exception {
		ResultBean resultBean = new ResultBean();
		try {
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			Page page = new Page();
			page.setPageNo(1);
			page.setPageSize(5);
			Page pageData=webUserService.getAnchorRankingList(page,"",0);
			if(pageData ==null || CollectionUtils.isEmpty(pageData.getResult())){
				logger.error("主播排行榜查询空");
				return 400;
			}
			/*resultBean.setData(pageData.getResult());
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), RANKINGLISTPATH);*/
			logger.info("主播排行榜文件SUCCESS");
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			logger.error("主播排行榜查询出错");
			e.printStackTrace();
			return 400;
		}
	}

	@GET
	@Path("/appVersion")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int appVersion() throws Exception {
		// TODO Auto-generated method stub
		ResultBean resultBean = new ResultBean();
		Map map = Maps.newHashMap();
		try {
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			map = feeItemStaticDao.getAppInitInfo(ANDRIODTYPE);
			resultBean.setData(map);

			ByteArrayInputStream buff = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buff, buff.available(), ANDRIODPATH);

			map = feeItemStaticDao.getAppInitInfo(IOSTYPE);
			resultBean.setData(map);
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), IOSPATH);
			logger.info("app版本文件SUCCESS");
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.error("appVersion出错");

		}
		return 400;
	}

	@GET
	@Path("/appMajiaBaoVersion")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int appMajiaBaoVersion(@QueryParam("type") int type) throws Exception {
		ResultBean resultBean = new ResultBean();
		Map map = Maps.newHashMap();
		try {
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			map = feeItemStaticDao.getAppInitInfo(type);
			resultBean.setData(map);
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), String.format(IOSMAJIABAOPATH,type));
			logger.info("appIOS马甲包版本文件SUCCESS");
			return 200;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			logger.error("appVersion出错");

		}
		return 400;
	}

	/**
	 * 获取所有正在直播的直播间人数
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GET
	@Path("/peopleCount")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int peopleCount() {
		try {
			logger.info("直播人数列表文件生成 -Start");
			ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
			List peloleCountMap = new ArrayList<>();
			List<LivingRoomInfoVo> livingRoomList = roomEnterExitOptService.getAllLivingRoomInfoList();
			if (null != livingRoomList && livingRoomList.size() > 0) {
				for (LivingRoomInfoVo livingRoomInfoVo : livingRoomList) {
					Map item = new HashMap<>();
					long watchNum = roomEnterExitOptService.getLiveRoomPeoleCount(livingRoomInfoVo.getUserId());
					try {
						String addNUm = livingRedisOptService.getAddNumForRoom(livingRoomInfoVo.getUserId());
						if (null != addNUm && Long.valueOf(addNUm) >= 0) {
							watchNum += Long.valueOf(addNUm);
							logger.info("{}加入了{}额外人数", livingRoomInfoVo.getUserId(), addNUm);
						}
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
					item.put("peopleCount", watchNum);
					item.put("chatRoomId", livingRoomInfoVo.getChatRoomId());
					peloleCountMap.add(item);
				}
			}
			logger.info("直播人数列表文件生成：" + peloleCountMap);

			resultBean.setData(peloleCountMap);
			// resultBean.setData(livingListService.getLivingRoomPeopleCount());
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), PEOPLECOUNTPATH);
			logger.info("直播人数列表文件生成 -成功");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("直播人数列表文件生成 -错误");
			return 400;
		}
		logger.info("直播人数列表文件生成 -End");
		return 200;
	}
	
	/**
	 * 生成直播列表文件数据（最新直播＋发现）
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public int getLivingList() {
		logger.info("直播列表文件生成 -Start");
		try {
			List<LivingRoomInfoVo> livingRoomList = roomEnterExitOptService.getAllLivingRoomInfoList();
			if (null != livingRoomList && livingRoomList.size() > 0) {
				/**
				 * 热门直播列表排序规则：明星=片场>网红，在线观看人数倒序
				 * 
				 * 发现最新直播排序规则：时间倒序
				 */
				List<LivingRoomInfoVo> hotList = new ArrayList<LivingRoomInfoVo>();
				List<LivingRoomInfoVo> latestList = new ArrayList<LivingRoomInfoVo>();
				for (int i = 0; i < livingRoomList.size(); i++) {
					LivingRoomInfoVo item = livingRoomList.get(i);
					if (item.getIsVipLive() == 1 && item.getIsShow() == 0)
						continue;
					item.setPeopleCount(roomEnterExitOptService.getLiveRoomPeoleCount(item.getUserId()));
					LivingRoomInfoVo item02 = (LivingRoomInfoVo) item.clone();

					// 1:网红 2:明星 3:片场
					if (item.getType() == 2 || item.getType() == 3) {
						item.setSortScore(200000000 + item.getPeopleCount());
					} else {
						item.setSortScore(100000000 + item.getPeopleCount());
					}
					hotList.add(item);

					item02.setSortScore(item02.getStartLiveTime());
					latestList.add(item02);
				}

				logger.info("直播列表文件生成 -开始排序");
				Collections.sort(hotList);
				creatHotLivingListFile(hotList);

				Collections.sort(latestList);
				creatLatestLivingListFile(latestList);
			} else {
				logger.info("直播列表文件生成 -暂无直播列表信息");
				creatHotLivingListFile(new ArrayList<LivingRoomInfoVo>());
				creatLatestLivingListFile(new ArrayList<LivingRoomInfoVo>());
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("直播列表文件生成 -错误");
			return 400;
		}
		logger.info("直播列表文件生成 -End");
		return 200;
	}
	
	/**
	 * 生成热门直播列表文件
	 * 
	 * @param list
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void creatHotLivingListFile(List<LivingRoomInfoVo> list) {
		try {
			ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
			resultBean.setData(list);
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), LISTHOTINDEXPATH);
			logger.info("直播列表文件生成 -热门直播列表文件生成成功");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("直播列表文件生成 -热门直播列表文件生成错误");
		}
	}

	/**
	 * 生成最新直播列表文件
	 * 
	 * @param list
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void creatLatestLivingListFile(List<LivingRoomInfoVo> list) {
		try {
			ResultBean resultBean = new ResultBean(ResultCode.SUCCESS);
			resultBean.setData(list);
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), LISTNEWLIVEPATH);
			logger.info("直播列表文件生成 -最新直播列表文件生成成功");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("直播列表文件生成 -最新直播列表文件生成错误");
		}
	}
	
	/**
	 * 获取热门页正在直播的直播列表
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GET
	@Path("/listHotIndex")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int listHostIndex() {
		try {
			ResultBean resultBean = new ResultBean();
			resultBean.setCode(ResultCode.SUCCESS.get_code());

			resultBean.setData(livingListService.getHotLivingList());
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), LISTHOTINDEXPATH);
			logger.info("热门文件SUCCESS:" + JSON.toJSONString(resultBean));
			logger.info("success=写入文件静态化~");
			return 200;
		} catch (Exception e) {
			logger.error("failed=写入文件发生错误!");
			e.printStackTrace();
			return 400;
		}
	}

	/**
	 * 获取最新正在直播的直播列表
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@GET
	@Path("/listNewLive")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int listNewLive() {
		try {
			ResultBean resultBean = new ResultBean();
			resultBean.setCode(ResultCode.SUCCESS.get_code());

			resultBean.setData(livingListService.getLatestLivingList());
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), LISTNEWLIVEPATH);
			logger.info("listNewLive文件SUCCESS");
			return 200;
		} catch (Exception e) {
			logger.error("failed=写入文件发生错误!");
			e.printStackTrace();
			return 400;
		}
	}
	
	@GET
	@Path("/banner")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int banner() {
		try {
			// TODO Auto-generated method stub
			ResultBean resultBean = new ResultBean();
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			List list = feeItemStaticDao.banner();
			resultBean.setData(list);
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), BANNERPATH);
			logger.info("banner文件SUCCESS");
			return 200;
		} catch (Exception e) {
			logger.error("failed=写入文件发生错误!");
			e.printStackTrace();
			return 400;
		}
	}

	/**
	 * 主播粉丝贡献榜
	 * @return
     */
	@GET
	@Path("/fansContributionList")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Override
	public int fansContributionList() {
		try {
			ResultBean resultBean = new ResultBean();
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			Map<String, Object> resultMap = Maps.newHashMap();
			List<String> ids = feeItemStaticDao.getAllAnchorUserId();
			for (String id : ids) {
				logger.info("粉丝贡献榜信息查询{收入总盒饭数,粉丝贡献详情}");
				//主播基本信息
				Long hefanNum = 0l;
				WebUser user = webUserService.findMyUserInfo(id);
				if(user!=null){
					hefanNum = user.getHefanTotal();
				}
				resultMap.put("hefanNum", hefanNum);
				//查询该主播的贡献信息
				List<Map<String, Object>> list = feeItemStaticDao.fansContributionList(id);
				for(Map one:list){
					WebUser webUser = webUserService.findUserInfoFromCache((String)one.get("userId"));
					one.put("authName", userAuthService.findAnchorInfo(webUser)); 
				}
				resultMap.put("dataValue", list);
				resultBean.setData(resultMap);
				String path = FANSCONTRIBUTIONLISTPATH + id + ".json";
				ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
				ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), path);
			}
			logger.info("fansContributionList文件SUCCESS");
			return 200;
		} catch (Exception e) {
			logger.error("failed=写入文件发生错误!");
			e.printStackTrace();
			return 400;
		}
	}


	/**
	 * 非主播粉丝贡献榜
	 * @return
     */
	@GET
	@Path("/fansContributionListForUser")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Override
	public int fansContributionListForUser() {
		try {
			ResultBean resultBean = new ResultBean();
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			Map<String, Object> resultMap = Maps.newHashMap();
			logger.info("生成普通用户的粉丝贡献榜");
			List<String> ids = feeItemStaticDao.getAllExcludeAnchorUserId();
			for (String id : ids) {
				logger.info("普通用户的粉丝贡献榜信息查询{收入总盒饭数,粉丝贡献详情}");
				//用户基本信息
				Long hefanNum = 0l;
				WebUser user = webUserService.findMyUserInfo(id);
				if(user!=null){
					hefanNum = user.getHefanTotal();
				}
				resultMap.put("hefanNum", hefanNum);
				//查询该用户的贡献信息
				List<Map<String, Object>> list = feeItemStaticDao.fansContributionList(id);
				for(Map one:list){
					WebUser webUser = webUserService.findUserInfoFromCache((String)one.get("userId"));
					one.put("authName", userAuthService.findAnchorInfo(webUser));
				}
				resultMap.put("dataValue", list);
				resultBean.setData(resultMap);
				String path = FANSCONTRIBUTIONLISTPATH + id + ".json";
				ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
				ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), path);
			}
			logger.info("fansContributionListforUser文件SUCCESS");
			return 200;
		} catch (Exception e) {
			logger.error("failed=写入文件发生错误!");
			e.printStackTrace();
			return 400;
		}
	}

	@GET
	@Path("/joinLiveRoomWord")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Override
	public int joinLiveRoomWord() {
		try {
			ResultBean resultBean = new ResultBean();
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			List list = feeItemStaticDao.joinLiveRoomWord();
			resultBean.setData(list);
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), JOINLIVEROOMWORDPATH);
			logger.info("joinLiveRoomWord文件SUCCESS");
			return 200;
		} catch (Exception e) {
			logger.error("failed=写入文件发生错误!");
			e.printStackTrace();
			return 400;
		}
	}

	@GET
	@Path("/dealCustomExpression")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Override
	public int dealCustomExpression() {
		// TODO Auto-generated method stub
		try {
			ResultBean resultBean = new ResultBean();
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			List list = feeItemStaticDao.getCustomExpressionList();
			resultBean.setData(list);
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), CUSTOMEXPRESSIONPATH);
			logger.info("自定义表情文件生成成功!");
			resultBean.setData(feeItemStaticDao.getPresentVersion());
			buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), VERSIONPATH);
			logger.info("版本文件生成成功");
			return 200;
		} catch (Exception e) {
			logger.error("failed=自定义表情写入文件发生错误!");
			e.printStackTrace();
			return 400;
		}
	}



	@GET
	@Path("/liveNotice")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Override
	public ResultPojo liveNotice() {
		ResultPojo resultPojo = new ResultPojo();
		try {
			ResultBean resultBean = new ResultBean();
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			Map map = feeItemStaticDao.liveNotice();
			resultBean.setData(map);
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), LIVENOTICEPATH);
			logger.info("直播预告文件写入成功");
			resultPojo.setCode(ResultCode.SUCCESS.get_code());
			resultPojo.setMsg("成功");

		} catch (Exception e) {
			resultPojo.setCode(ResultCode.UNSUCCESS.get_code());
			resultPojo.setMsg("DB事务回滚，请重试");
			logger.error("直播预告文件生成错误!!");
			e.printStackTrace();
		}
		return resultPojo;
	}




	@GET
	@Path("/liveSubscribe")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Override
	public ResultPojo liveSubscribe() {
		ResultPojo resultPojo = new ResultPojo();
		try {
			ResultBean resultBean = new ResultBean();
			resultBean.setCode(ResultCode.SUCCESS.get_code());
			List list = feeItemStaticDao.liveBookingList();
			resultBean.setData(list);
			// System.out.println("liveSubscribe================="+JSON.toJSONString(resultBean));
			ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(resultBean).getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), LIVEBOOKINGLISTPATH);
			logger.info("直播预告文件写入成功");
			resultPojo.setCode(ResultCode.SUCCESS.get_code());
			resultPojo.setMsg("成功");

			return resultPojo;
		} catch (Exception e) {
			resultPojo.setCode(ResultCode.UNSUCCESS.get_code());
			resultPojo.setMsg("DB事务回滚，请重试");
			logger.error("直播预告文件生成错误!!");
			e.printStackTrace();
		}
		return resultPojo;
	}

	/**
	 * 敏感词处理
	 */
	@GET
	@Path("/initSensitiveword")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Override
	public int initSensitiveword() {
		// TODO Auto-generated method stub
		try {
			List<String> list = feeItemStaticDao.getSensitivewordList();
			StringBuffer contentBuffer = new StringBuffer();
			for (String content : list) {
				if (StringUtils.isBlank(content)) {
					continue;
				}
				contentBuffer.append(content);
				contentBuffer.append("\n"); // don't forget the return character
			}
			ByteArrayInputStream buf = new ByteArrayInputStream(contentBuffer.toString().getBytes("utf-8"));
			ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), SENSITIVEWORD);
			logger.info("敏感词文件写入成功");
			return 200;
		} catch (Exception e) {
			logger.error("敏感词文件生成错误!!");
			e.printStackTrace();
			return 400;
		}
	}

	/**
	 * 刷新广场小红点
	 */
	@GET
	@Path("/squareLatest")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	@Override
    public void squareLatest(){
        String latestId = "";
        try {
            List list = jedisService.zrevrange("square_sorted",0,0);
			if (list.size() == 0){
				logger.info("获取广场最新动态ID失败", latestId);
				return;
			}else {
				ResultBean rb = new ResultBean(ResultCode.SUCCESS);
				Map map = Maps.newHashMap();
				map.put("msgId", Integer.valueOf(latestId));
				rb.setData(map);
				ByteArrayInputStream buf = new ByteArrayInputStream(JSON.toJSONString(rb).getBytes("utf-8"));
				ossImageService.updataFile(GlobalConstants.PIC_OSS_BUCKET, buf, buf.available(), SQUARERED);
				logger.info("刷新广场最新动态ID：{}", latestId);
			}
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("获取广场最新动态ID缓存失败");
        }

    }
}
