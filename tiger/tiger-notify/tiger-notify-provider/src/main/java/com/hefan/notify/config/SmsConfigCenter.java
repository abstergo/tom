package com.hefan.notify.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import java.util.Map;

/**
 * 
 * @author kevin_zhang
 *
 */
@Repository
public class SmsConfigCenter {
	/**
	 * 配置中心
	 */
	@Value("#{publicConfig}")
	private Map<String, String> publicConfig;

	public Map<String, String> getPublicConfig() {
		return publicConfig;
	}

	public void setPublicConfig(Map<String, String> publicConfig) {
		this.publicConfig = publicConfig;
	}
}
