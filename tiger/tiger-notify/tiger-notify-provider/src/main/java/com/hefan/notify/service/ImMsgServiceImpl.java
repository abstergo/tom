package com.hefan.notify.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ImCustomMsgEnum;
import com.cat.common.meta.ResultCode;
import com.cat.common.util.YunXinUtil;
import com.cat.tiger.util.GlobalConstants;
import com.hefan.notify.itf.ImMsgService;
import com.hefan.notify.util.NeteaseHttpUtil;
import com.hefan.oms.bean.ImCustomMsgVo;

@Component("imMsgService")
public class ImMsgServiceImpl implements ImMsgService {
	
	private Logger logger = LoggerFactory.getLogger(ImMsgServiceImpl.class);

	@Override
	public ResultBean msgsendMsg(String from,String ope,String to,String type,String body,String option,
			String pushcount,String payload,String ext,String forcepushlist,String forcepushcontent,String forcepushall) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.msgSendMsg;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			if(StringUtils.isBlank(from)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("发送者帐号不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("from", from));
			if(StringUtils.isBlank(ope)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("发送类型不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("ope", ope));
			if(StringUtils.isBlank(to)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("接受方不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("to", to));
			if(StringUtils.isBlank(type)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("消息类型不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("type", type));
			if(StringUtils.isBlank(body)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("消息内容不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("body", body));
			logger.info("私信内容："+body);
			if(StringUtils.isNotBlank(option))
			nvps.add(new BasicNameValuePair("option", option));
			if(StringUtils.isNotBlank(pushcount))
			nvps.add(new BasicNameValuePair("pushcount", pushcount));
			if(StringUtils.isNotBlank(payload))
			nvps.add(new BasicNameValuePair("payload", payload));
			if(StringUtils.isNotBlank(ext))
			nvps.add(new BasicNameValuePair("ext", ext));
			if(StringUtils.isNotBlank(forcepushlist))
			nvps.add(new BasicNameValuePair("forcepushlist", forcepushlist));
			if(StringUtils.isNotBlank(forcepushcontent))
			nvps.add(new BasicNameValuePair("forcepushcontent", forcepushcontent));
			if(StringUtils.isNotBlank(forcepushall))
			nvps.add(new BasicNameValuePair("forcepushall", forcepushall));
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				res.setCode(ResultCode.SUCCESS.get_code());
			}else{
				logger.info("云信私聊消息发送失败：发送人"+from+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("云信私聊消息发送失败：发送人"+from+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}
	
	public static void main(String[] args) {
		ImMsgServiceImpl service = new ImMsgServiceImpl();
		ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
        imMsgVo.setType(ImCustomMsgEnum.IM_MSG_208.getType());
        imMsgVo.setContent(ImCustomMsgEnum.IM_MSG_208.getMsg());
		Map roamMap = new HashMap();
		roamMap.put("persistent", false);
		roamMap.put("history", false);
		roamMap.put("badge", false);
		ResultBean res = service.msgsendMsg("100000001", "0", "1100553", "100", JSONObject.toJSONString(imMsgVo), JSONObject.toJSONString(roamMap), "", "", "", "", "", "");
//		ResultBean res = service.msgsendMsg("100000001", "0", "1100553", "0", "{\"msg\":\"haha22\"}", JSONObject.toJSONString(roamMap), "", "", "", "", "", "");
		System.out.println(JSON.toJSONString(res));
	}

	@Override
	public ResultBean logoutSendMsg(String userId) {
		// TODO Auto-generated method stub
		ResultBean res = new ResultBean();
		try {
			String url = YunXinUtil.msgSendMsg;
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair("from", GlobalConstants.HEFAN_OFFICIAL_ID));
			nvps.add(new BasicNameValuePair("ope", "0"));
			if(StringUtils.isBlank(userId)){
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg("接受方不能为空");
				return res;
			}
			nvps.add(new BasicNameValuePair("to", userId));
			nvps.add(new BasicNameValuePair("type", "100"));
			ImCustomMsgVo imMsgVo = new ImCustomMsgVo();
            imMsgVo.setType(ImCustomMsgEnum.IM_MSG_208.getType());
            imMsgVo.setContent(ImCustomMsgEnum.IM_MSG_208.getMsg());
			nvps.add(new BasicNameValuePair("body", JSONObject.toJSONString(imMsgVo)));
			Map roamMap = new HashMap();
			roamMap.put("persistent", false);
			roamMap.put("history", false);
			roamMap.put("badge", false);
			nvps.add(new BasicNameValuePair("option", JSONObject.toJSONString(roamMap)));
			String result = NeteaseHttpUtil.sendHttpApiUtil(url, nvps);
			String code = JSON.parseObject(result).getString("code");
			if(code !=null && code.equals("200")){
				res.setCode(ResultCode.SUCCESS.get_code());
			}else{
				logger.info("请登出私信发送失败：接收人"+userId+",错误信息"+result);
				res.setCode(ResultCode.UNSUCCESS.get_code());
				res.setMsg(JSON.parseObject(result).getString("desc"));
			}
			return res;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("请登出私信发送失败：接收人"+userId+",错误信息"+e.getMessage());
			res.setCode(ResultCode.UNSUCCESS.get_code());
			return res;
		}
	}

}
