package com.hefan.notify.bean;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;

@SuppressWarnings("serial")
public class ListHotIndexVo implements Serializable {

	private String id;
	@JSONField(serialize = false)
	private String userId;
	private String headImg;
	private String name;
	private int type;
	private String liveImg;
	private String location;
	private String liveUrl;
	private String personSign;
	private int chatRoomId;
	private String liveUuid;
	private String displayGraph;
	private int peopleCount;
	private String pullUrls;
	private String hefanTotal;

	public String getLiveUrl() {
		return liveUrl;
	}

	public void setLiveUrl(String liveUrl) {
		this.liveUrl = liveUrl;
	}

	public String getHefanTotal() {
		return hefanTotal;
	}

	public void setHefanTotal(String hefanTotal) {
		this.hefanTotal = hefanTotal;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPullUrls() {
		return pullUrls;
	}

	public void setPullUrls(String pullUrls) {
		this.pullUrls = pullUrls;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getLiveImg() {
		return liveImg;
	}

	public void setLiveImg(String liveImg) {
		this.liveImg = liveImg;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPersonSign() {
		return personSign;
	}

	public void setPersonSign(String personSign) {
		this.personSign = personSign;
	}

	public int getChatRoomId() {
		return chatRoomId;
	}

	public void setChatRoomId(int chatRoomId) {
		this.chatRoomId = chatRoomId;
	}

	public String getLiveUuid() {
		return liveUuid;
	}

	public void setLiveUuid(String liveUuid) {
		this.liveUuid = liveUuid;
	}

	public String getDisplayGraph() {
		return displayGraph;
	}

	public void setDisplayGraph(String displayGraph) {
		this.displayGraph = displayGraph;
	}

	public int getPeopleCount() {
		return peopleCount;
	}

	public void setPeopleCount(int peopleCount) {
		this.peopleCount = peopleCount;
	}
}
