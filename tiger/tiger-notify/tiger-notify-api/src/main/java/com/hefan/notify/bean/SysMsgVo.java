package com.hefan.notify.bean;

import java.io.Serializable;

/**
 * Created by kevin_zhang on 17/03/2017.
 */
public class SysMsgVo implements Serializable {
    int sysmessagecount = 0;
    int activemessagecount = 0;
    int commetmessagecount = 0;
    int presentmessagecount = 0;
    int newFansCount = 0;
    int totalSysMsgCount = 0;
    int totalMsgCount = 0;

    public int getSysmessagecount() {
        return sysmessagecount;
    }

    public void setSysmessagecount(int sysmessagecount) {
        this.sysmessagecount = sysmessagecount;
    }

    public int getActivemessagecount() {
        return activemessagecount;
    }

    public void setActivemessagecount(int activemessagecount) {
        this.activemessagecount = activemessagecount;
    }

    public int getCommetmessagecount() {
        return commetmessagecount;
    }

    public void setCommetmessagecount(int commetmessagecount) {
        this.commetmessagecount = commetmessagecount;
    }

    public int getPresentmessagecount() {
        return presentmessagecount;
    }

    public void setPresentmessagecount(int presentmessagecount) {
        this.presentmessagecount = presentmessagecount;
    }

    public int getNewFansCount() {
        return newFansCount;
    }

    public void setNewFansCount(int newFansCount) {
        this.newFansCount = newFansCount;
    }

    public int getTotalSysMsgCount() {
        return totalSysMsgCount;
    }

    public void setTotalSysMsgCount() {
        this.totalSysMsgCount = sysmessagecount + activemessagecount + commetmessagecount + presentmessagecount;
    }

    public int getTotalMsgCount() {
        return totalMsgCount;
    }

    public void setTotalMsgCount() {
        this.totalMsgCount = sysmessagecount + activemessagecount + commetmessagecount + presentmessagecount + newFansCount;
    }
}
