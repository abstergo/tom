package com.hefan.notify.bean;

import java.io.Serializable;
import java.util.List;


public class PubParams implements Serializable{

	// 必填 通知栏提示文字
	private String ticker;
	// 必填 通知标题
	private String title;
	// 必填 通知文字描述 
	private String text;
	/**
	 * 必填  点击"通知"的后续行为，默认为打开app。
	 * 1."go_app": 打开应用
	 * 2."go_url": 跳转到URL
	 * 3."go_activity": 打开特定的activity
	 * 4."go_custom": 用户自定义内容。
	 */
	private int afterOpenType=1;
	//
	/**拓展字段 up_type  类型   1-打开直播间 2-热门推荐更新
	 * 当afterOpenType为3，4时必填
	 * afterOpenType=3，
	 * afterOpenType=4，备用
	 */
	private int upType=0;
	/**拓展字段  up_value  值
	 * afterOpenType=2，url值  必填
	 * afterOpenType=3，userId  可选
	 * afterOpenType=4，备用
	 */
	private String upValue;
	//定时发送 格式: "YYYY-MM-DD HH:mm:ss"。 只对任务生效
	private String startTime;
	//组播，taglist
	private List<String> tagList;
	//单播/列波（不超过500个）
	private String deviceTokens;
	//文件播
	private String fileId;
	
	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getAfterOpenType() {
		return afterOpenType;
	}

	public void setAfterOpenType(int afterOpenType) {
		this.afterOpenType = afterOpenType;
	}

	public int getUpType() {
		return upType;
	}

	public void setUpType(int upType) {
		this.upType = upType;
	}

	public String getUpValue() {
		return upValue;
	}

	public void setUpValue(String upValue) {
		this.upValue = upValue;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public List<String> getTagList() {
		return tagList;
	}

	public void setTagList(List<String> tagList) {
		this.tagList = tagList;
	}

	public String getDeviceTokens() {
		return deviceTokens;
	}

	public void setDeviceTokens(String deviceTokens) {
		this.deviceTokens = deviceTokens;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

}
