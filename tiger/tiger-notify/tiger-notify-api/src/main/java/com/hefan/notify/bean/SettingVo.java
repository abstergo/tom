package com.hefan.notify.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统配置
 * 
 * @author kevin_zhang
 *
 */
@SuppressWarnings("serial")
public class SettingVo implements Serializable {
	private long id;
	private String configKey;
	private String confilgValue;
	private int deleteFlag;
	private Date createTime;
	private Date updateTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfilgValue() {
		return confilgValue;
	}

	public void setConfilgValue(String confilgValue) {
		this.confilgValue = confilgValue;
	}

	public int getDeleteFlag() {
		return deleteFlag;
	}

	public void setDeleteFlag(int deleteFlag) {
		this.deleteFlag = deleteFlag;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}
