package com.hefan.notify.itf;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.cat.common.entity.ResultBean;

@Path("/imUser")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public interface ImUserService {
	
	/**
	 * 创建云信id
	 * @Title: createUser   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param accid (必传) 云信ID，最大长度32字符，必须保证一个APP内唯一（只允许字母、数字、半角下划线_、@、半角点以及半角-组成，不区分大小写，会统一小写处理，请注意以此接口返回结果中的accid为准）。
	 * @param name  (可选) 云信ID昵称，最大长度64字符，用来PUSH推送时显示的昵称
	 * @param props (可选) json属性，第三方可选填，最大长度1024字符
	 * @param icon  (可选) 云信ID头像URL，第三方可选填，最大长度1024
	 * @param token (可选) 云信ID可以指定登录token值，最大长度128字符，并更新，如果未指定，会自动生成token，并在创建成功后返回
	 * @param: @return      
	 * @return: ResultBean  code同ResultCode里成功的code，data为token
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午2:41:47
	 */
	@GET
	@Path("/createUser")
	public ResultBean createUser(@QueryParam("accid") String accid,
			@QueryParam("name") String name,@QueryParam("props") String props,
			@QueryParam("icon") String icon,@QueryParam("token") String token);

	/**
	 * 云信ID更新
	 * @Title: updateUser   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param accid (必传) 云信ID，最大长度32字符，必须保证一个APP内唯一
	 * @param props (可选) json属性，第三方可选填，最大长度1024字符
	 * @param token (可选) 云信ID可以指定登录token值，最大长度128字符
	 * @param: @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午2:49:53
	 */
	@GET
	@Path("/updateUser")
	public ResultBean updateUser(@QueryParam("accid") String accid,
			@QueryParam("props") String props,@QueryParam("token") String token);
	
	/**
	 * 更新并获取新token
	 * @Title: refreshToken   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param accid (必传) 云信ID，最大长度32字符，必须保证一个APP内唯一
	 * @param: @return      
	 * @return: ResultBean code同ResultCode里成功的code，data为token
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午2:54:03
	 */
	@GET
	@Path("/refreshToken")
	public ResultBean refreshToken(@QueryParam("accid") String accid);
	
	/**
	 * 封禁云信ID
	 * @Title: blockUser   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param accid    (必传) 云信ID，最大长度32字符，必须保证一个APP内唯一
	 * @param needkick (可选) 是否踢掉被禁用户，true或false，默认false
	 * @param: @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午3:02:09
	 */
	@GET
	@Path("/blockUser")
	public ResultBean blockUser(@QueryParam("accid") String accid,@QueryParam("needkick") String needkick);
	
	/**
	 * 解禁云信ID
	 * @Title: unblockUser   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param accid (必传) 云信ID，最大长度32字符，必须保证一个APP内唯一
	 * @param: @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午3:06:02
	 */
	@GET
	@Path("/unblockUser")
	public ResultBean unblockUser(@QueryParam("accid") String accid);
}
