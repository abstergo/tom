package com.hefan.notify.itf;


import com.cat.common.entity.ResultPojo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

public interface ItemsStaticService {

	/**
	 * 弹幕私信静态文件
	 */
	public int FeeItemStaticService() throws Exception;

	/**
	 * 弹幕私信静态文件
	 */
	public int AdvertiseService() throws Exception;

	/**
	 * 礼物
	 */
	public int present() throws Exception;

	/**
	 * 根据礼物分类生成礼物版本和礼物列表静态资源文件
	 * @param status
	 * @return
	 * @throws Exception
     */
	public int presentResourceCreater (int status) throws Exception;

	public int updatePresent(int status) throws Exception;

	/**
	 * 主播排行榜
	 */
	public int rankingList() throws Exception;

	/**
	 * APP版本
	 */
	public int appVersion() throws Exception;


	/**
	 * ios马甲包升级版本
	 * @return
	 * @throws Exception
     */
	public int appMajiaBaoVersion(int type) throws Exception;


	/**
	 * 获取所有正在直播的直播间人数
	 * 
	 * @return
	 */
	public int peopleCount();
	
	/**
	 * 生成直播列表文件数据（最新直播＋发现）
	 * 
	 * @return
	 */
	@GET
	@Path("/getLivingList")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public int getLivingList();

	/**
	 * 获取热门页正在直播的直播列表
	 * 
	 * @return
	 */
	public int listHostIndex();

	/**
	 * 获取最新正在直播的直播列表
	 * 
	 * @return
	 */
	public int listNewLive();

	/**
	 * 热门页banner图
	 * @return
	 */
	public int banner();
	
	/**
	 * 主播粉丝贡献榜
	 * @return
	 */
	public int fansContributionList();

	/**
	 * 其他用户粉丝贡献榜
	 * @return
     */
	public int fansContributionListForUser();
	
	/**
	 * 进入直播间提示词
	 * @return
	 */
	public int joinLiveRoomWord();
	
	/**
	 * 自定义表情
	 * @Title: dealCustomExpression   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月26日 下午6:40:32
	 */
	public int dealCustomExpression();
	
	/** 
	 * 直播预告
	 * @return
	 */
	public ResultPojo liveNotice();
	
	/**
	 * 直播预约
	 */
	public ResultPojo liveSubscribe();
	
	/**
	 * 敏感词文件生成
	 * @Title: initSensitiveword   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月29日 下午11:05:33
	 */
	public int initSensitiveword();

	/**
	 * 刷新广场小红点
	 */
	void squareLatest();
}
