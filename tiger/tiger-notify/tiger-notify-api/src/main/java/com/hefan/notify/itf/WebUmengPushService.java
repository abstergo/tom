package com.hefan.notify.itf;

import com.cat.common.entity.ResultBean;
import com.hefan.notify.bean.WebPushParam;

public interface WebUmengPushService {
	
	/**
	 * web端推送调用
	 * @Title: webPushByParams   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param webPushParam
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月20日 下午3:56:07
	 */
	public ResultBean webPushByParams(WebPushParam webPushParam);

}
