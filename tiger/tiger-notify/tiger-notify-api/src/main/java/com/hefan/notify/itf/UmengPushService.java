package com.hefan.notify.itf;

import com.hefan.notify.bean.PubParams;

public interface UmengPushService {
	
	/**
	 * android广播
	 * @Title: sendAndroidBroadcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param params
	 * @param: @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月25日 下午12:01:24
	 */
	public boolean sendAndroidBroadcast(PubParams params);
	
	/**
	 * android单播
	 * @Title: sendAndroidUnicast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param params
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月25日 下午2:34:54
	 */
	public boolean sendAndroidUnicast(PubParams params,boolean much);
	
	/**
	 * android组播
	 * @Title: sendAndroidGroupcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param params
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月25日 下午2:45:14
	 */
	public boolean sendAndroidGroupcast(PubParams params);
	
	/**
	 * android文件播
	 * @Title: sendAndroidFilecast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param params
	 * @return
	 * @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月14日 下午2:17:20
	 */
	public boolean sendAndroidFilecast(PubParams params);
	
	/**
	 * android自定义播
	 * @Title: sendAndroidCustomizedcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param params
	 * @return      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月20日 下午6:36:56
	 */
	public boolean sendAndroidCustomizedcast(PubParams params);
	
	/**
	 * ios广播
	 * @Title: sendIOSBroadcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param params
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月25日 下午1:15:54
	 */
	public boolean sendIOSBroadcast(PubParams params) throws Exception;
	
	/**
	 * ios单播
	 * @Title: sendIOSUnicast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param params
	 * @param: @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月25日 下午2:31:03
	 */
	public boolean sendIOSUnicast(PubParams params,boolean much);
	
	/**
	 * IOS组播
	 * @Title: sendIOSGroupcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param: @param params
	 * @param: @return
	 * @param: @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年8月25日 下午2:48:12
	 */
	public boolean sendIOSGroupcast(PubParams params);
	
	/**
	 * IOS文件播
	 * @Title: sendIOSFilecast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param params
	 * @return
	 * @throws Exception      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月14日 下午2:17:20
	 */
	public boolean sendIOSFilecast(PubParams params);
	
	/**
	 * IOS自定义播
	 * @Title: sendIOSCustomizedcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param params
	 * @return      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月20日 下午6:37:47
	 */
	public boolean sendIOSCustomizedcast(PubParams params);
	
	/**
	 * 上传文件返回文件id
	 * @Title: upLoadTokenFile   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param contents
	 * @param modelType  类型  1-and  2-ios
	 * @return
	 * @throws Exception      
	 * @return: String
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月14日 下午2:35:46
	 */
	public String upLoadTokenFile(String contents,int modelType);
	
	/**
	 * 单播（内调AND和IOS）
	 * @Title: sendUnicast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param params
	 * @return      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月20日 下午4:33:51
	 */
	public boolean sendUnicast(PubParams params);
	/**
	 * 列播 （内调AND和IOS）
	 * @Title: sendListcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param params
	 * @return      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月20日 下午4:34:25
	 */
	public boolean sendListcast(PubParams params);
	/**
	 * 广播 （内调AND和IOS）
	 * @Title: sendBroadcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param params
	 * @return      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月20日 下午4:34:32
	 */
	public boolean sendBroadcast(PubParams params);
	
	/**
	 * 自定义播（别名播）
	 * @Title: sendCustomizedcast   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param params
	 * @return      
	 * @return: boolean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月20日 下午6:56:43
	 */
	public boolean sendCustomizedcast(PubParams params);

}
