package com.hefan.notify.bean;


import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;


@Entity(tableName = "app_init_info")
public class AppInitInfo extends BaseEntity {

    @Column("type")
    int type;
    @Column("app_version")
    String appVersion;
    @Column("update_link")
    String updateLink;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getUpdateLink() {
        return updateLink;
    }

    public void setUpdateLink(String updateLink) {
        this.updateLink = updateLink;
    }
}