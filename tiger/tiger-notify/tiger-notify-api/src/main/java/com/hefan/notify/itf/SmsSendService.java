package com.hefan.notify.itf;

import com.cat.common.entity.ResultBean;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * 短信发送 Created by lxw on 2016/9/28.
 */
@Path("/sms")
public interface SmsSendService {

	/**
	 * 发送短信验证码
	 * 
	 * @param smsType
	 *            发送短信类型
	 * @param moblie
	 *            接收手机
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@GET
	@Path("/serverSendSms")
	@Produces({ MediaType.APPLICATION_JSON })
	ResultBean aliSendSmsCode(@QueryParam("smsType") int smsType, @QueryParam("moblie") String moblie);

	/**
	 * 后台请求发送短信
	 * 
	 * @param moblie
	 * @param smsType
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	@GET
	@Path("/sendSms")
	@Produces({ MediaType.APPLICATION_JSON })
	ResultBean webReqAliSendSmsCode(@QueryParam("moblie") String moblie, @QueryParam("smsType") int smsType);

}
