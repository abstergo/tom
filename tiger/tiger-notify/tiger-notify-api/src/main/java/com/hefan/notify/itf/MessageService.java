package com.hefan.notify.itf;

import com.cat.common.entity.Page;
import com.hefan.notify.bean.SysMsgVo;

import java.util.Map;

/**
 * 消息
 *
 * @author kevin_zhang
 */
public interface MessageService {

    /**
     * 获取未读消息数
     *
     * @param userId
     * @return
     */
    @SuppressWarnings("rawtypes")
    SysMsgVo getMsgCount(String userId);

    /**
     * 清空消息数
     *
     * @param userId
     * @param clearMsgType 0：清空我的消息  1：清空新增好友消息数
     * @return
     */
    void cleanMsgCount(String userId, int clearMsgType);

    /**
     * 获取消息主页信息(系统消息，活动消息)
     *
     * @param msgType:(0:系统消息  1:活动消息)
     * @param userId
     * @param userType:(0:普通用户 1:网红 2:明星／片场)
     * @return
     */
    @SuppressWarnings({"rawtypes"})
    Map getMsgPageInfo(int msgType, String userId, int userType);

    /**
     * 获取系统消息列表
     *
     * @param page
     * @param userId
     * @param userType:(0:普通用户 1:网红 2:明星／片场)
     * @return
     */
    @SuppressWarnings("rawtypes")
    Page getSysMsgList(Page page, String userId, int userType);

    /**
     * 获取活动消息列表
     *
     * @param page
     * @param userId
     * @param userType:(0:普通用户 1:网红 2:明星／片场)
     * @return
     */
    @SuppressWarnings("rawtypes")
    Page getActivityMsgList(Page page, String userId, int userType);

    /**
     * 获取消息详情
     *
     * @param msgId
     * @return
     */
    @SuppressWarnings("rawtypes")
    Map getMsgDetail(String msgId);

    /**
     * 增加指定消息数
     *
     * @param updateTypeValus:(1:系统消息 2:活动消息 3:评论消息数 4:赞数 5:礼物数)
     * @param userId
     * @return
     */
    int addMsgCout(int updateTypeValus, String userId);

    /**
     * 清空所有用户消息缓存（慎重操作）
     */
    void cleanAllMsgCache();
}