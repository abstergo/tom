package com.hefan.notify.itf;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.cat.common.entity.ResultBean;

@Path("/imMsg")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public interface ImMsgService {
	
	/**
	 * 发送普通消息
	 * @Title: msgSendMsgsendMsg   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param from
	 *            发送者accid，用户帐号，最大32字符，必须保证一个APP内唯一
	 * @param ope
	 *            0：点对点个人消息，1：群消息，其他返回414
	 * @param to
	 *            ope==0是表示accid即用户id，ope==1表示tid即群id
	 * @param type
	 *            0 表示文本消息,1 表示图片，、2 表示语音，3 表示视频，4 表示地理位置信息，6 表示文件，100 自定义消息类型
	 * @param body
	 *            请参考下方消息示例说明中对应消息的body字段，最大长度5000字符，为一个json串
	 * @param option
	 * 			     发消息时特殊指定的行为选项,Json格式，可用于指定消息的漫游，存云端历史，发送方多端同步，推送，消息抄送等特殊行为;option中字段不填时表示默认值
	 *            option示例:
	 * 
	 *            {"push":false,"roam":true,"history":false,"sendersync":true,"route":false,"badge":false,"needPushNick":true}
	 * 
	 *            字段说明： 1. roam: 该消息是否需要漫游，默认true（需要app开通漫游消息功能）； 2. history:
	 *            该消息是否存云端历史，默认true； 3. sendersync: 该消息是否需要发送方多端同步，默认true； 4.
	 *            push: 该消息是否需要APNS推送或安卓系统通知栏推送，默认true； 5. route:
	 *            该消息是否需要抄送第三方；默认true (需要app开通消息抄送功能); 6.
	 *            badge:该消息是否需要计入到未读计数中，默认true; 7. needPushNick:
	 *            推送文案是否需要带上昵称，不设置该参数时默认true;
	 * @param pushcount ios推送内容，不超过150字符，option选项中允许推送（push=true），此字段可以指定推送内容
	 * @param payload  ios 推送对应的payload,必须是JSON,不能超过2k字符
	 * @param ext  开发者扩展字段，长度限制1024字符
	 * @param forcepushlist 发送群消息时的强推（@操作）用户列表，格式为JSONArray，如["accid1","accid2"]。若forcepushall为true，则forcepushlist为除发送者外的所有有效群成员
	 * @param forcepushcontent	发送群消息时，针对强推（@操作）列表forcepushlist中的用户，强制推送的内容
	 * @param forcepushall 发送群消息时，强推（@操作）列表是否为群里除发送者外的所有有效成员，true或false，默认为false
	 * @param: @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午4:10:12
	 */
	@GET
	@Path("/msgsendMsg")
	public ResultBean msgsendMsg(@QueryParam("from") String from,@QueryParam("ope") String ope,
			@QueryParam("to") String to,@QueryParam("type") String type,
			@QueryParam("body") String body,@QueryParam("option") String option,
			@QueryParam("pushcount") String pushcount,@QueryParam("payload") String payload,
			@QueryParam("ext") String ext,@QueryParam("forcepushlist") String forcepushlist,
			@QueryParam("forcepushcontent") String forcepushcontent,@QueryParam("forcepushall") String forcepushall);
	
	/**
	 * 208私信-退出登录
	 * @Title: logoutSendMsg   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月25日 下午4:17:04
	 */
	@GET
	@Path("/logoutSendMsg")
	public ResultBean logoutSendMsg(@QueryParam("userId") String userId);

}
