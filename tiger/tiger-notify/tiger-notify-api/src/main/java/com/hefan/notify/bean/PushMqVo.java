package com.hefan.notify.bean;

import com.hefan.live.bean.LivingRoomInfoVo;

import java.io.Serializable;
import java.util.Map;

public class PushMqVo implements Serializable{
	
	/**   
	 * @Description: serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 * @author: LiTeng
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 1-开播提醒
	 */
	public int type;
	/**
	 * 用户id
	 */
	public String userId;
	/**
	 * 昵称
	 */
	public String nickName;

	/**
	 * 直播信息
	 */
	public LivingRoomInfoVo livingRoomInfoVo;

	public LivingRoomInfoVo getLivingRoomInfoVo() {
		return livingRoomInfoVo;
	}

	public void setLivingRoomInfoVo(LivingRoomInfoVo livingRoomInfoVo) {
		this.livingRoomInfoVo = livingRoomInfoVo;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
}
