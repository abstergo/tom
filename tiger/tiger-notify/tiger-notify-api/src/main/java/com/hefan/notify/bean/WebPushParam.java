package com.hefan.notify.bean;

import java.io.Serializable;
import java.util.List;

public class WebPushParam implements Serializable{
	/**   
	 * @Description: serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 * @author: LiTeng
	 */
	private static final long serialVersionUID = 1L;
	//0:所有用户  1:所有主播（网红）  2:所有明星（明星/片场）  3:个人（指定的多个人） 4:所有普通用户
	private Integer userType;
	//当类型为3个人时，用户userId数组
	private List<String> userId;
	//标题
	private String title;
	//内容
	private String content;
	public Integer getUserType() {
		return userType;
	}
	public void setUserType(Integer userType) {
		this.userType = userType;
	}
	public List<String> getUserId() {
		return userId;
	}
	public void setUserId(List<String> userId) {
		this.userId = userId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

}
