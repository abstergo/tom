package com.hefan.notify.itf;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.alibaba.fastjson.JSONArray;
import com.cat.common.entity.ResultBean;

@Path("/imChatroom")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public interface ImChatroomService {
	
	/**
	 * 创建聊天室
	 * @Title: createChatroom   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param creator (必传) 聊天室属主的账号accid
	 * @param name (必传) 聊天室名称，长度限制128个字符
	 * @param announcement (可选) 公告，长度限制4096个字符
	 * @param broadcasturl (可选) 直播地址，长度限制1024个字符
	 * @param ext (可选) 扩展字段，最长4096字符
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午4:44:01
	 */
	@GET
	@Path("/createChatroom")
	public ResultBean createChatroom(@QueryParam("creator") String creator,@QueryParam("name") String name,
			@QueryParam("announcement") String announcement,@QueryParam("broadcasturl") String broadcasturl,
			@QueryParam("ext") String ext);
	
	/**
	 * 查询聊天室信息
	 * @Title: getChatroom   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param roomid (必传) 聊天室id
	 * @param needOnlineUserCount (可选) 是否需要返回在线人数，true或false，默认false
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午5:01:04
	 */
	@GET
	@Path("/getChatroom")
	public ResultBean getChatroom(@QueryParam("roomid") long roomid,@QueryParam("needOnlineUserCount") String needOnlineUserCount);
	
	/**
	 * 更新聊天室信息
	 * @Title: updateChatroom   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param roomid (必传) 聊天室id
	 * @param name (可选) 聊天室名称，长度限制128个字符
	 * @param announcement (可选) 公告，长度限制4096个字符
	 * @param broadcasturl (可选) 直播地址，长度限制1024个字符 
	 * @param ext (可选) 扩展字段，长度限制4096个字符
	 * @param needNotify (可选) true或false,是否需要发送更新通知事件，默认true 
	 * @param notifyExt (可选) 通知事件扩展字段，长度限制2048 
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午5:10:02
	 */
	@GET
	@Path("/updateChatroom")
	public ResultBean updateChatroom(@QueryParam("roomid") long roomid,@QueryParam("name") String name,
			@QueryParam("announcement") String announcement,@QueryParam("broadcasturl") String broadcasturl,
			@QueryParam("ext") String ext,@QueryParam("needNotify") String needNotify,
			@QueryParam("notifyExt") String notifyExt);
	
	/**
	 * 修改聊天室开/关闭状态
	 * @Title: toggleCloseStat   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param roomid (必传) 聊天室id
	 * @param operator (必传) 操作者账号，必须是创建者才可以操作 
	 * @param valid (必传) true或false，false:关闭聊天室；true:打开聊天室
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午5:11:24
	 */
	@GET
	@Path("/toggleCloseStat")
	public ResultBean toggleCloseStat(@QueryParam("roomid") long roomid,
			@QueryParam("operator") String operator,@QueryParam("valid") String valid);
	
	/**
	 * 设置聊天室内用户角色
	 * @Title: setMemberRole   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param roomid (必传) 聊天室id
	 * @param operator (必传) 操作者账号accid 
	 * @param target (必传) 被操作者账号accid
	 * @param opt (必传) 操作：1: 设置为管理员，operator必须是创建者，2:设置普通等级用户，operator必须是创建者或管理员，-1:设为黑名单用户，operator必须是创建者或管理员，-2:设为禁言用户，operator必须是创建者或管理员
	 * @param optvalue (必传) true或false，true:设置；false:取消设置
	 * @param notifyExt (可选) 通知扩展字段，长度限制2048，请使用json格式
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午5:12:43
	 */
	@GET
	@Path("/setMemberRole")
	public ResultBean setMemberRole(@QueryParam("roomid") long roomid,@QueryParam("operator") String operator,
			@QueryParam("target") String target,@QueryParam("opt") String opt,
			@QueryParam("optvalue") String optvalue,@QueryParam("notifyExt")String notifyExt);
	
	/**
	 * 往聊天室内发消息
	 * @Title: chatroomSendMsg   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param roomid (必传) 聊天室id
	 * @param msgId (必传) 客户端消息id，使用uuid等随机串，msgId相同的消息会被客户端去重
	 * @param fromAccid (必传) 消息发出者的账号accid
	 * @param msgType (必传) 消息类型： 0: 表示文本消息，1: 表示图片，2: 表示语音，3: 表示视频，4: 表示地理位置信息，6: 表示文件，10: 表示Tips消息，100: 自定义消息类型 
	 * @param resendFlag (可选) 重发消息标记，0：非重发消息，1：重发消息，如重发消息会按照msgid检查去重逻辑
	 * @param attach (可选) 消息内容，格式同消息格式示例中的body字段,长度限制2048字符
	 * @param ext (可选) 消息扩展字段，内容可自定义，请使用JSON格式，长度限制4096 
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午5:16:11
	 */
	@GET
	@Path("/chatroomSendMsg")
	public ResultBean chatroomSendMsg(@QueryParam("roomid") long roomid,
			@QueryParam("fromAccid")String fromAccid,@QueryParam("msgType") int msgType,
			@QueryParam("resendFlag") int resendFlag,@QueryParam("attach") String attach,
			@QueryParam("ext") String ext);

	/**
	 * 将聊天室内成员设置为临时禁言
	 * @Title: temporaryMute   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param roomid (必传) 聊天室id
	 * @param operator (必传) 操作者accid,必须是管理员或创建者 
	 * @param target (必传) 被禁言的目标账号accid 
	 * @param muteDuration (必传) 0:解除禁言;>0设置禁言的秒数，不能超过2592000秒(30天)
	 * @param needNotify (可选) 操作完成后是否需要发广播，true或false，默认true 
	 * @param notifyExt (可选) 通知广播事件中的扩展字段，长度限制2048字符 
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午5:17:43
	 */
	@GET
	@Path("/temporaryMute")
	public ResultBean temporaryMute(@QueryParam("roomid") long roomid,@QueryParam("operator") String operator,
			@QueryParam("target") String target,@QueryParam("muteDuration") String muteDuration,
			@QueryParam("needNotify") String needNotify,@QueryParam("notifyExt") String notifyExt);
	
	/**
	 * 往聊天室内添加机器人
	 * @Title: addRobot   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param roomid (必传) 聊天室id
	 * @param accids (必传) 机器人账号accid列表，必须是有效账号，账号数量上限100个
	 * @param roleExt (可选) 机器人信息扩展字段，请使用json格式，长度4096字符 
	 * @param notyfyExt (可选) 机器人进入聊天室通知的扩展字段，请使用json格式，长度2048字符 
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午5:18:40
	 */
	@GET
	@Path("/addRobot")
	public ResultBean addRobot(@QueryParam("roomid") long roomid,@QueryParam("accids") JSONArray accids,
			@QueryParam("roleExt") String roleExt,@QueryParam("notyfyExt") String notyfyExt);
	
	/**
	 * 从聊天室内删除机器人
	 * @Title: removeRobot   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param roomid (必传) 聊天室id
	 * @param accids (必传) 机器人账号accid列表，必须是有效账号，账号数量上限100个
	 * @return      
	 * @return: ResultBean
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年9月29日 下午5:19:40
	 */
	@GET
	@Path("/removeRobot")
	public ResultBean removeRobot(@QueryParam("roomid") long roomid,@QueryParam("accids") JSONArray accids);
}
