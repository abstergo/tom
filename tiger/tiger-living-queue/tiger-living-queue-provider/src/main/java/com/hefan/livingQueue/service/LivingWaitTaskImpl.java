package com.hefan.livingQueue.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.cat.tiger.service.JedisService;
import com.cat.tiger.util.GlobalConstants;
import com.hefan.common.util.DynamicProperties;
import com.hefan.live.bean.LiveRoom;
import com.hefan.live.itf.LiveRoomService;
import com.hefan.live.itf.LivingRedisOptService;
import com.hefan.livingQueue.itf.LivingWaitTask;
import com.hefan.livingQueue.itf.PoolOptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by kevin_zhang on 11/28/16.
 */
@Path("/livingWait")
@Component("liveWaitTask")
public class LivingWaitTaskImpl implements LivingWaitTask {
    public Logger logger = LoggerFactory.getLogger(LivingWaitTaskImpl.class);

    @Resource
    LiveRoomService liveRoomService;
    @Resource
    JedisService jedisService;
    @Resource
    PoolOptService poolOptService;
    @Reference
    LivingRedisOptService livingRedisOptService;

    @GET
    @Path("/waitTask")
    @Override
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void waitTask() {
        //直播中直播间信息
        List<LiveRoom> livingList = liveRoomService.getLivingRoomList();
        for (LiveRoom liveRoom : livingList) {
            long watchNum = 0l;
            try {
                watchNum = Long.parseLong(livingRedisOptService.getLivingWatchNum(liveRoom.getLiveUuid()));
            } catch (Exception e) {
                logger.info("ID为" + liveRoom.getUserId() + "的主播直播间，队列池任务获取直播间实际人数失败");
                e.printStackTrace();
            }

            long waitQueuelimitMinNum = DynamicProperties.getLong("living_wait_queue_min_person_num_limit");
            long waitQueuelimitMidNum = DynamicProperties.getLong("living_wait_queue_mid_person_num_limit");
            long waitQueuelimitMaxNum = DynamicProperties.getLong("living_wait_queue_max_person_num_limit");

            int enterQueuelimitMinNum = DynamicProperties.getInt("living_enter_queue_min_person_num_limit");
            int enterQueuelimitMidNum = DynamicProperties.getInt("living_enter_queue_mid_person_num_limit");
            int enterQueuelimitMaxNum = DynamicProperties.getInt("living_enter_queue_max_person_num_limit");


            String listKey = GlobalConstants.LIVING_WAITING_QUEUE_KEY + liveRoom.getLiveUuid();
            String hashKey = GlobalConstants.LIVING_PREPARE_QUEUE_KEY + liveRoom.getLiveUuid();
            if (watchNum > waitQueuelimitMinNum && watchNum < waitQueuelimitMidNum) {
                logger.info("5s内允许进入" + enterQueuelimitMinNum + "个用户");
                poolOptService.waitToEnter(listKey, hashKey, enterQueuelimitMinNum);
            } else if (watchNum >= waitQueuelimitMidNum && watchNum < waitQueuelimitMaxNum) {
                logger.info("5s内允许进入" + enterQueuelimitMidNum + "个用户");
                poolOptService.waitToEnter(listKey, hashKey, enterQueuelimitMidNum);
            } else if (watchNum >= waitQueuelimitMaxNum) {
                logger.info("5s内允许进入" + enterQueuelimitMaxNum + "个用户");
                poolOptService.waitToEnter(listKey, hashKey, enterQueuelimitMaxNum);
            } else {
                continue;
            }
        }
    }
}
