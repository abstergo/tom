package com.hefan.livingQueue.itf;

import com.cat.common.entity.ResultBean;
import com.hefan.livingQueue.bean.LivingVo;

/**
 * Created by kevin_zhang on 11/28/16.
 */
public interface PoolOptService {

    /**
     * 排队结束，进入准入池
     *
     * @param listKey 排队池元素
     * @param hashKey 准入池元素
     * @param count   每次迁移个数
     */
    void waitToEnter(String listKey, String hashKey, int count);

    /**
     * 用户进入直播间排队处理
     */
    ResultBean enterQueueOperate(LivingVo item);
}
