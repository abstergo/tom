package com.hefan.livingQueue.bean;

import java.io.Serializable;

/**
 * Created by kevin_zhang on 11/28/16.
 */
public class LivingVo implements Serializable {
    private int chatRoomId;// 进入直播间对应聊天室id
    private String liveUuid;// 直播间UUID
    private String authoruserId;// 主播id
    private String userId;// 进入直播间用户id
    private String nickName;// 进入直播间用户昵称d
    private int userType;// 进入直播间用户类型，
    private String headImg;// 进入直播间用户头像
    private int userLevel;// 进入直播间用户等级

    public int getChatRoomId() {
        return chatRoomId;
    }

    public void setChatRoomId(int chatRoomId) {
        this.chatRoomId = chatRoomId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid;
    }

    public String getAuthoruserId() {
        return authoruserId;
    }

    public void setAuthoruserId(String authoruserId) {
        this.authoruserId = authoruserId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public String getHeadImg() {
        return headImg;
    }

    public void setHeadImg(String headImg) {
        this.headImg = headImg;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }
}
