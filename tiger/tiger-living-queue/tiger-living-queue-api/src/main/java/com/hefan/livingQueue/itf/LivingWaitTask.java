package com.hefan.livingQueue.itf;

/**
 * Created by kevin_zhang on 11/28/16.
 */
public interface LivingWaitTask {
    void waitTask();
}
