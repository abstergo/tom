package com.hefan.user.dao;

import com.cat.tiger.util.CollectionUtils;
import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.user.bean.HfIosPackageCfg;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/27.
 */
@Repository
public class HfIosMajiaBaoCfgDao extends BaseDaoImpl<HfIosPackageCfg> {

    /**
     * 获取ios配置开关
     * @param bundleId
     * @return
     */
    public int getIosPackageCfgIsUse(String bundleId) {
        String sql = "select is_use as isUse from hf_ios_package_cfg where bundle_id=? and delete_flag=0 limit 1";
        List<Map<String,Object>> resultList = queryMap(sql,new Object[] {bundleId});
        int isUse = 2;
        if(!CollectionUtils.isEmptyList(resultList)) {
            Map<String,Object> resultMap = resultList.get(0);
            isUse = Integer.valueOf(String.valueOf(resultMap.get("isUse")));
        }
        return isUse;
    }
}
