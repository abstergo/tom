package com.hefan.user.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.user.bean.LiveNoticeReserve;

@Repository
public class LiveNoticeReserveDao  extends BaseDaoImpl<LiveNoticeReserve> {
	@Resource
    private JdbcTemplate jdbcTemplate;
	
	/**
	 * 保存
	 * @param liveNoticeReserve
	 * @return
	 */
	public int saveObj(LiveNoticeReserve liveNoticeReserve){
		String sql = "insert IGNORE into live_notice_reserve (live_notice_id,user_id,is_notice)"+
			 	"values (?,?,?)";
		 //参数数组
		 Object[]  paramArr =new Object[] {
				 liveNoticeReserve.getLiveNoticeId(),
				 liveNoticeReserve.getUserId(),
				 liveNoticeReserve.getIsNotice()
				 }; 
		 //参数类型数组
		 int[] paramtypeArr = new int[]{
				 						java.sql.Types.INTEGER,
				 						java.sql.Types.VARCHAR,
				 						java.sql.Types.INTEGER,
		 							   };
		 return jdbcTemplate.update(sql,paramArr,paramtypeArr);
	}
	
	/**
	 * 查看是否预约
	 * @param liveNoticeReserve
	 * @return
	 */
	public int checkNotice(LiveNoticeReserve liveNoticeReserve){
		String countSql = "select count(1) as rowNum FROM live_notice_reserve where live_notice_id = ? AND user_id = ? and delete_flag = 0";
		return getJdbcTemplate().queryForObject(countSql, new Object[] {liveNoticeReserve.getLiveNoticeId(), liveNoticeReserve.getUserId()}, Integer.class);
	}
	
	/**
	 * 单条更新提醒状态
	 * @param liveNoticeReserve
	 * @return
	 */
	public int updateNoticeStatusOne(Integer id){
		String updateSql = " update live_notice_reserve set is_notice = 1 where id = ?";
		return getJdbcTemplate().update(updateSql, id);
	}
	
	/**
	 * 批量更新提醒状态
	 * @param liveNoticeReserveList
	 * @return
	 */
	public int updateNoticeStatusBatch(List<LiveNoticeReserve> liveNoticeReserveList){
		List paramArr = new ArrayList();
		String sql = " UPDATE live_notice_reserve set is_notice = 1 WHERE id IN( ";
		for(LiveNoticeReserve liveNoticeReserve:liveNoticeReserveList){
			sql = sql +"?,";
			paramArr.add(liveNoticeReserve.getId());
		}
		sql = sql.substring(0,sql.length()-1)+" )";
		return getJdbcTemplate().update(sql, paramArr.toArray());		
	}
	
	/***
	 * 根据直播预约id查询所有没有被通知的已经预约了的预约信息
	 */
	public List<LiveNoticeReserve> listLiveNoticeReserveByLiveNoticeId(Integer liveNoticeId){
		String sql = "select * from live_notice_reserve where live_notice_id = ? and is_notice = 0 and delete_flag = 0 ";
		return this.query(sql, liveNoticeId);
	}
	
}
