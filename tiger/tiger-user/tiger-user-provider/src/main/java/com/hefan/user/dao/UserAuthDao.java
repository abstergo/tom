package com.hefan.user.dao;

import com.hefan.common.orm.dao.CommonDaoImpl;
import org.jboss.resteasy.core.ExceptionAdapter;
import org.springframework.stereotype.Repository;

/**
 * Created by lxw on 2016/10/31.
 */
@Repository
public class UserAuthDao extends CommonDaoImpl{


    /**
     * 网红获取身份认证称号
     * @param userId
     * @return
     */
    public String findRedUserAuth(String userId) {
        try {
           StringBuilder sqlBuilder = new StringBuilder("SELECT a.name FROM identity_activity a JOIN identity_activity_user b ON a.id=b.identity_activity_id ");
            sqlBuilder.append("WHERE a.state = 1 AND a.start_time<=NOW() AND a.end_time>=NOW() AND b.user_id=? ORDER BY a.create_time DESC LIMIT 1");
            return getJdbcTemplate().queryForObject(sqlBuilder.toString(),new Object[] {userId},String.class);
        } catch (Exception e) {
        }
        return "";
    }

    /**
     * 获取明星的身份认证称号
     * @param authId
     * @return
     */
    public String findStarUserAuth(int authId) {
        try {
            String sql = "SELECT name FROM identity_official WHERE isDel=0 AND id=? LIMIT 1";
            return getJdbcTemplate().queryForObject(sql,new Object[] {authId},String.class);
        } catch (Exception e) {
        }
        return "";
    }
}
