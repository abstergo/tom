package com.hefan.user.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import com.hefan.user.bean.FeedBack;
import com.hefan.user.bean.FindBug;
import com.hefan.user.bean.HfFindBugRecord;
import com.hefan.user.dao.FeedBackDao;
import com.hefan.user.dao.FindBugDao;
import com.hefan.user.dao.HfFindBugRecordDao;
import com.hefan.user.itf.FeedBackService;

@Component("feedBackService")
public class FeedBackServiceImpl implements FeedBackService{
	
	@Resource
	private FeedBackDao feedBackDao;
	
	@Resource
	private FindBugDao findBugDao;
	
	@Resource
	private HfFindBugRecordDao hfFindBugRecordDao;
	
	@Override
	public int submit(FeedBack feedBack) {
		return feedBackDao.saveFeedBack(feedBack);
	}

	@Override
	public void findBug(FindBug findBug) {
		 findBugDao.save(findBug);
	}

	@Override
	public void visitRecord(HfFindBugRecord visitRecord) {
		hfFindBugRecordDao.save(visitRecord);
	}

}
