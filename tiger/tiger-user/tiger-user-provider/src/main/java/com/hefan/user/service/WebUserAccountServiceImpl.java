package com.hefan.user.service;

import com.hefan.user.bean.WebUserAccount;
import com.hefan.user.dao.WebUserAccountDao;
import com.hefan.user.itf.WebUserAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by nigle on 2017/3/24.
 */
@Component("webUserAccountService")
public class WebUserAccountServiceImpl implements WebUserAccountService {

    @Resource
    WebUserAccountDao webUserAccountDao;

    private Logger logger = LoggerFactory.getLogger(WebUserAccountServiceImpl.class);

    /**
     * 查询普通用户的收入余额
     * @param userId
     * @return
     */
    @Override
    public long getBaseUserIncome(String userId) {
        WebUserAccount webUserAccount = webUserAccountDao.getWebUserAccount(userId);
        if (null != webUserAccount) {
            return webUserAccount.getIncome();
        }
        return 0;
    }

    /**
     * 更新或新增普通用户账户余额
     * @param userId
     * @param income
     * @return
     */
    public int updateOrInsertForBaseUserIncome(String userId, long income) {
        return webUserAccountDao.updateOrInsertForBaseUserIncome(userId, income);
    }
}