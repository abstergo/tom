package com.hefan.user.dao;

import com.hefan.common.orm.dao.CommonDaoImpl;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * 用户订单
 *
 * @author wangchao
 * @create 2016/11/9 14:44
 */
@Repository
public class WebUserOrderDao extends CommonDaoImpl {

  @Resource
  private JdbcTemplate jdbcTemplate;

  /**
   * 通过用户订单查询总经验
   *
   * @param userId
   * @return
   * @throws
   * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
   * @author wangchao
   * @create 2016/11/9 15:00
   */
  public Long countExperienceByOrder(String userId) {
    String countSql = "SELECT sum(p.experience) FROM web_user_order wuo LEFT JOIN present p  on wuo.present_id=p.id where wuo.user_id=?";
    return jdbcTemplate.queryForObject(countSql, new Object[] { userId }, Long.class);
  }
}
