package com.hefan.user.service;

import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.service.JedisService;
import com.hefan.user.bean.WatchRelationCacheDao;
import com.hefan.user.bean.WebUser;
import com.hefan.user.config.UserConfigCenter;
import com.hefan.user.constant.RedisKeysConstant;
import com.hefan.user.dao.WatchRelationDao;
import com.hefan.user.itf.WatchCacheService;
import com.hefan.user.itf.WatchService;
import com.hefan.user.itf.WebUserService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 关注相关操作
 * Created by kevin_zhang on 13/03/2017.
 */
@Component("watchCacheService")
public class WatchCacheServiceImpl implements WatchCacheService {
  public static Logger logger = LoggerFactory.getLogger(WatchCacheServiceImpl.class);

  @Resource
  JedisService jedisService;

  @Resource
  WatchService watchService;

  @Resource
  WatchRelationDao watchRelationDao;

  @Resource
  WebUserService webUserService;

  @Resource
  UserConfigCenter userConfigCenter;

  /**
   * 关注
   *
   * @param userId     关注者
   * @param mainUserId 被关注者
   * @return
   */
  @Override
  public ResultBean fork(String userId, String mainUserId) {
    ResultBean result = new ResultBean(ResultCode.SUCCESS);
    try {
      long watchedMaxLimit = 2000;
      try{
        Map<String, String> map = userConfigCenter.getPublicConfig();
        watchedMaxLimit = StringUtils.isBlank(map.get("watchedMaxLimit")) ? 2000 : Long.parseLong(map.get("watchedMaxLimit"));
      } catch (Exception ex) {
        ex.printStackTrace();
        logger.error("关注操作 获取最大关注数异常");
      }

      logger.info("关注操作 最大关注数：" + watchedMaxLimit);
      if (watchNum(userId) >= watchedMaxLimit) {
        /**
         * 已达关注上限
         */
        result.setCode(ResultCode.WatchedMaxLimit.get_code());
        result.setMsg(ResultCode.WatchedMaxLimit.getMsg());
      }
      logger.info("关注操作 mainUserId：" + mainUserId + " userId：" + userId);
      if (RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_0") >= 0) {
        /**
         * 已关注对方
         */
        logger.info("关注操作 当前关系：已关注对方");
        result.setCode(ResultCode.UserWatched.get_code());
        result.setMsg(ResultCode.UserWatched.getMsg());
      } else {
        if (RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_1") >= 0) {
          /**
           * 已互粉
           */
          logger.info("关注操作 已互粉");
          result.setCode(ResultCode.UserWatched.get_code());
          result.setMsg(ResultCode.UserWatched.getMsg());
        } else {
          /**
           * 未关注
           */
          logger.info("关注操作 当前关系：未关注对方");
          double score = RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_0");
          if (score >= 0) {
            /**
             * 对方已关注自己
             */
            logger.info("关注操作 当前关系：对方已关注自己 score=" + BigDecimal.valueOf(score));
            jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_0"); //移除自己对对方的关注（防止关系偏差）
            jedisService.zadd(RedisKeysConstant.WATCH_LIST_KEY + userId, System.currentTimeMillis(), mainUserId + "_1"); //自己互粉
            jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_0"); //移除对方对自己的关注
            jedisService.zadd(RedisKeysConstant.WATCH_LIST_KEY + mainUserId, (score > 0) ? score : System.currentTimeMillis(), userId + "_1"); //对方互粉，不改变排序

            //粉丝数修改
            jedisService.incr(RedisKeysConstant.FANS_ADD_COUNT_KEY + mainUserId);
          } else {
            /**
             * 对方未关注自己
             */
            logger.info("关注操作 当前关系：对方未关注自己");
            jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_1"); //移除自己对对方的互粉（防止关系偏差）
            jedisService.zadd(RedisKeysConstant.WATCH_LIST_KEY + userId, System.currentTimeMillis(), mainUserId + "_0"); //关注对方
            jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_0"); //移除对方对自己的关注（防止关系偏差）
            jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_1"); //移除对方对自己的互粉（防止关系偏差）

            //粉丝数修改
            jedisService.incr(RedisKeysConstant.FANS_ADD_COUNT_KEY + mainUserId);
          }
        }
      }
      logger.info("关注操作 关注成功");
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("关注操作 关注异常");
      result = new ResultBean(ResultCode.UnknownException);
    } finally {
      return result;
    }
  }

  /**
   * 关注（DB）
   *
   * @param userId     关注者
   * @param mainUserId 被关注者
   * @param liveRemind
   */
  @Override
  public int forkDB(String userId, String mainUserId, Integer liveRemind) {
    /**
     * 维护双方的关注/粉丝关系DB
     * 1，关注对方
     * 2，成为对方粉丝
     */
    return watchService.fork(userId, mainUserId, liveRemind);
  }

  /**
   * 取消关注
   *
   * @param userId     关注者
   * @param mainUserId 被关注者
   * @return
   */
  @Override
  public ResultBean unfork(String userId, String mainUserId) {
    ResultBean result = new ResultBean(ResultCode.SUCCESS);
    try {
      logger.info("取消关注操作 mainUserId：" + mainUserId + " userId：" + userId);
      if (RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_0") >= 0) {
        /**
         * 已关注对方
         */
        logger.info("取消关注操作 当前关系：已关注对方");
        jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_0"); //移除自己对对方的关注
        jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_1"); //移除自己对对方的互粉（防止关系偏差）
        jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_0"); //移除对方对自己的关注（防止关系偏差）
        jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_1"); //移除对方对自己的互粉（防止关系偏差）
      } else {
        if (RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_1") >= 0) {
          /**
           * 已互粉对方
           */
          logger.info("取消关注操作 当前关系：已互粉对方");
          jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_0"); //移除自己对对方的关注（防止关系偏差）
          jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_1"); //移除自己对对方的互粉
          double score = RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_1");
          logger.info("取消关注操作 对方存储的关系排序值： score=" + BigDecimal.valueOf(score));
          jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_1"); //移除对方对自己的互粉
          jedisService.zadd(RedisKeysConstant.WATCH_LIST_KEY + mainUserId, (score > 0) ? score : System.currentTimeMillis(), userId + "_0"); //对方关注自己，不改变排序
        } else {
          /**
           * 已取消关注
           */
          logger.info("取消关注操作 当前关系：已取消关注");
          result.setCode(ResultCode.CancelWatched.get_code());
          result.setMsg(ResultCode.CancelWatched.getMsg());
        }
      }
      logger.info("取消关注操作 取消关注成功");
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("取消关注操作 取消关注异常");
      result = new ResultBean(ResultCode.UnknownException);
    } finally {
      return result;
    }
  }

  /**
   * 取消关注（DB）
   *
   * @param userId     关注者
   * @param mainUserId 被关注者
   */
  @Override
  public int unforkDB(String userId, String mainUserId) {
    /**
     * 维护双方的关注/粉丝关系DB
     * 1，取消关注对方
     * 2，取消成为对方粉丝
     */
    return watchService.unfork(userId, mainUserId);
  }

  /**
   * 关注总数
   *
   * @param userId
   * @return
   */
  @Override
  public long watchNum(String userId) {
    long forkTotalNum = 0;
    try {
      logger.info("获取关注数操作 userId：" + userId);
      long forkNum = jedisService.zcard(RedisKeysConstant.WATCH_LIST_KEY + userId); //自己关注数
      forkTotalNum = forkNum > 0 ? forkNum : 0;

      logger.info("获取关注数操作 获取自己关注数：" + forkNum);
      logger.info("获取关注数操作 获取关注数：" + forkTotalNum);
      logger.info("获取关注数操作 获取关注数成功");
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("获取关注数操作 获取关注数异常");
    } finally {
      return forkTotalNum;
    }
  }

  /**
   * 获取指定用户间的关注关系
   *
   * @param userId     关注者
   * @param mainUserId 被关注者
   * @return 0：已关注  1：互粉  2：未关注
   */
  @Override
  public int getWatchRelationByUserId(String userId, String mainUserId) {
    int relation = 0;
    try {
      logger.info("获取关注数操作 mainUserId：" + mainUserId + " userId：" + userId);
      if (RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_0") >= 0)
        relation = 0;
      else if (RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + userId, mainUserId + "_1") >= 0)
        relation = 1;
      else
        relation = 2;
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("获取关注数操作 获取关注数异常");
    } finally {
      return relation;
    }
  }

  /**
   * 获取用户的关注关系列表
   */
  @Override
  public List<WatchRelationCacheDao> getAllWatchList(String userId) {
    List<WatchRelationCacheDao> result = new ArrayList<>();
    try {
      logger.info("获取关注数操作 userId：" + userId);
      List<Object> lst = jedisService.zrevrange(RedisKeysConstant.WATCH_LIST_KEY + userId, 0, -1);
      for (Object item : lst) {
        try {
          String releationStr = item.toString();
          logger.info("获取关注数操作 关系：" + releationStr);
          String[] releationStrArr = releationStr.split("_");
          if (null != releationStrArr && releationStrArr.length >= 2) {
            String _userId = releationStrArr[0];
            int _watchRelation = Integer.parseInt(releationStrArr[1]);
            result.add(new WatchRelationCacheDao(_userId, _watchRelation));
          } else {
            logger.info("获取关注数操作 关系异常");
          }
        } catch (Exception ex) {
          ex.printStackTrace();
          logger.error("获取关注数操作 关系出错");
        }
      }
      logger.info("获取关注数操作 获取关注数成功");
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("获取关注关系列表操作 获取关注关系列表异常");
    } finally {
      return result;
    }
  }

  /**
   * 获取用户关注的用户id列表
   */
  @Override
  public List<String> getAllWatchUserIdList(String userId) {
    List<String> result = new ArrayList<>();
    try {
      Profiler profiler=new Profiler("获取用户关注的用户id列表开始------------------");
      logger.info("获取用户关注的用户id列表操作 userId：" + userId);
      profiler.start("获取用户关注的用户,从缓存获取------------------");
      List<Object> lst = jedisService.zrevrange(RedisKeysConstant.WATCH_LIST_KEY + userId, 0, -1);
      if (null == lst || lst.size() <= 0) {
        profiler.start("获取用户关注的用户,从DB获取------------------");
        logger.info("获取用户关注的用户id列表操作 userId：" + userId + " 关注的缓存信息未找到");
        recoverWatchRelationCacheByUserId(userId);
        logger.info("获取用户关注的用户id列表操作 userId：" + userId + " 恢复该用户的关注信息");
        lst = jedisService.zrevrange(RedisKeysConstant.WATCH_LIST_KEY + userId, 0, -1);
      }
      profiler.start("获取用户关注的用户,处理逻辑------------------");
      logger.info("获取用户关注的用户id={}列表操作size={}：" ,userId,lst.size());
      for (Object item : lst) {
        try {
          String releationStr = item.toString();
          //logger.info("获取用户关注的用户id列表操作 关系：" + releationStr);
          String[] releationStrArr = releationStr.split("_");
          if (null != releationStrArr && releationStrArr.length >= 2) {
            String _userId = releationStrArr[0];
            result.add(_userId);
          } else {
            logger.info("获取用户关注的用户id列表操作 关系异常");
          }
        } catch (Exception ex) {
          ex.printStackTrace();
          logger.error("获取用户关注的用户id列表操作 关系出错");
        }
      }
      profiler.stop().print();
      logger.info("获取用户关注的用户id列表操作 获取关注数成功");
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("获取用户关注的用户id列表操作 获取关注关系列表异常");
    } finally {
      return result;
    }
  }

  /**
   * 分页获取关注列表用户信息
   *
   * @param pageNo
   * @param pageSize
   * @param userId
   * @param latestScore
   * @return
   */
  @Override
  public ResultBean getWatchListByPage(int pageNo, int pageSize, String userId, BigDecimal latestScore) {
    ResultBean result = new ResultBean(ResultCode.SUCCESS);
    try {
      List<Map<String, Object>> resultList = new ArrayList<>();
      List<Object> lst = new ArrayList<>();
      logger.info("分页获取关注列表用户信息操作 pageNo：" + pageNo + " pageSize：" + pageSize + " latestScore" + latestScore);
      if (latestScore.compareTo(BigDecimal.valueOf(0)) == 1) {
        lst = jedisService.zrevrange(RedisKeysConstant.WATCH_LIST_KEY + userId, 0, -1);
        if (null != lst && lst.size() > 0) {
          for (Object item : lst) {
            try {
              String releationStr = item.toString();
              logger.info("分页获取关注列表用户信息操作 关系：" + releationStr);
              BigDecimal score = BigDecimal.valueOf(RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + userId, releationStr));
              logger.info("分页获取关注列表用户信息操作 当前关注用户排序分值：" + score.toString());
              if (score.compareTo(BigDecimal.valueOf(0)) == 1 && latestScore.compareTo(score) == 1) {
                String[] releationStrArr = releationStr.split("_");
                if (null != releationStrArr && releationStrArr.length >= 2) {
                  String _userId = releationStrArr[0];
                  int _watchRelation = Integer.parseInt(releationStrArr[1]);
                  /**
                   * 获取用户信息
                   */
                  long startTime = System.currentTimeMillis();
                  logger.info("分页获取关注列表用户信息操作 获取用户信息用时 -Start：" + startTime);
                  WebUser webUser = webUserService.findUserInfoFromCache(_userId);
                  if (null == webUser) {
                    logger.info("分页获取关注列表用户信息操作 获取用户信息失败：UserId = " + _userId + "用户信息不存在");
                    jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + userId, releationStr);//清空对该用户的关注信息
                  } else {
                    Map<String, Object> itemMap = new HashMap<>();
                    itemMap.put("userLevel", webUser.getUserLevel());
                    itemMap.put("headImg", webUser.getHeadImg());
                    itemMap.put("personalSignature", webUser.getPersonSign());
                    itemMap.put("sex", webUser.getSex());
                    itemMap.put("nickName", webUser.getNickName());
                    itemMap.put("userId", webUser.getUserId());
                    itemMap.put("userType", webUser.getUserType());
                    itemMap.put("watchType", _watchRelation);//0：关注 1：互粉
                    itemMap.put("uniqueId", score.toString()); //用户唯一标示，即缓存中的排序分值
                    //itemMap.put("relationId,);//关注表的记录ID
                    //itemMap.put("liveRemind,);//对关注的用户是否开启开播提醒，默认开启
                    resultList.add(itemMap);
                    logger.info("分页获取关注列表用户信息操作 获取用户信息用时 -End：" + (System.currentTimeMillis() - startTime));
                  }
                } else {
                  logger.info("分页获取关注列表用户信息操作 关系异常");
                }
              } else {
                logger.info("分页获取关注列表用户信息操作 当前关注用户排序分值不符合当前页数据：" + score.toString());
              }
              if (resultList.size() == pageSize)
                break;
            } catch (Exception ex) {
              ex.printStackTrace();
              logger.error("分页获取关注列表用户信息操作 关系出错");
            }
          }
        }
      } else {
        /**
         * 兼容旧版本：2017-04-08之前版本
         */
        lst = jedisService.zrevrange(RedisKeysConstant.WATCH_LIST_KEY + userId, (pageNo - 1) * pageSize, pageNo * pageSize - 1);
        if (null != lst && lst.size() > 0) {
          for (Object item : lst) {
            try {
              String releationStr = item.toString();
              logger.info("分页获取关注列表用户信息操作 关系：" + releationStr);

              BigDecimal score = BigDecimal.valueOf(RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + userId, releationStr));
              logger.info("分页获取关注列表用户信息操作 当前关注用户排序分值：" + score.toString());
              String[] releationStrArr = releationStr.split("_");
              if (null != releationStrArr && releationStrArr.length >= 2) {
                String _userId = releationStrArr[0];
                int _watchRelation = Integer.parseInt(releationStrArr[1]);
                /**
                 *获取用户信息
                 */
                long startTime = System.currentTimeMillis();
                logger.info("分页获取关注列表用户信息操作 获取用户信息用时 -Start：" + startTime);
                WebUser webUser = webUserService.findUserInfoFromCache(_userId);
                if (null == webUser) {
                  logger.info("分页获取关注列表用户信息操作 获取用户信息失败：UserId = " + _userId + "用户信息不存在");
                  jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + userId, releationStr);//清空对该用户的关注信息
                } else {
                  Map<String, Object> itemMap = new HashMap<>();
                  itemMap.put("userLevel", webUser.getUserLevel());
                  itemMap.put("headImg", webUser.getHeadImg());
                  itemMap.put("personalSignature", webUser.getPersonSign());
                  itemMap.put("sex", webUser.getSex());
                  itemMap.put("nickName", webUser.getNickName());
                  itemMap.put("userId", webUser.getUserId());
                  itemMap.put("userType", webUser.getUserType());
                  itemMap.put("watchType", _watchRelation);//0：关注 1：互粉
                  itemMap.put("uniqueId", score.toString()); //用户唯一标示，即缓存中的排序分值
                  //itemMap.put("relationId,);//关注表的记录ID
                  //itemMap.put("liveRemind,);//对关注的用户是否开启开播提醒，默认开启
                  resultList.add(itemMap);
                  logger.info("分页获取关注列表用户信息操作 获取用户信息用时 -End：" + (System.currentTimeMillis() - startTime));
                }
              } else {
                logger.info("分页获取关注列表用户信息操作 关系异常");
              }
            } catch (Exception ex) {
              ex.printStackTrace();
              logger.error("分页获取关注列表用户信息操作 关系出错");
            }
          }
        }
      }
      Page page = new Page();
      page.setPageNo(pageNo);
      page.setPageSize(resultList.size());
      page.setResult(resultList);
      result.setData(page);
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("分页获取关注列表用户信息操作 操作异常");
    } finally {
      return result;
    }
  }

  /**
   * 恢复指定用户的关注关系缓存
   */
  @Override
  public void recoverWatchRelationCacheByUserId(String userId) {
    logger.info("恢复指定用户关注缓存操作 Start");
    if (StringUtils.isNotBlank(userId)) {
      try {
        //获取用户DB中的关注列表
        List<String> watchList = watchRelationDao.listAllWatchId(userId);
        jedisService.del(RedisKeysConstant.WATCH_LIST_KEY + userId);
        if (null != watchList && watchList.size() > 0) {
          for (String mainUserId : watchList) {
            logger.info("恢复指定用户关注缓存操作 mainUserId：" + mainUserId + " userId：" + userId);
            if (RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_0") >= 0) {
              /**
               * 对方已关注自己
               */
              logger.info("恢复指定用户关注缓存操作 当前关系：对方已关注自己");
              jedisService.zrem(RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_0"); //移除对方对自己的关注
              jedisService.zadd(RedisKeysConstant.WATCH_LIST_KEY + mainUserId, System.currentTimeMillis(), userId + "_1"); //对方互粉
              jedisService.zadd(RedisKeysConstant.WATCH_LIST_KEY + userId, System.currentTimeMillis(), mainUserId + "_1"); //自己互粉
            } else {
              if (RedisHelper.zscore(jedisService, RedisKeysConstant.WATCH_LIST_KEY + mainUserId, userId + "_1") >= 0) {
                /**
                 * 对方已互粉自己
                 */
                logger.info("恢复指定用户关注缓存操作 当前关系：对方已互粉自己");
                jedisService.zadd(RedisKeysConstant.WATCH_LIST_KEY + userId, System.currentTimeMillis(), mainUserId + "_1"); //自己互粉
              } else {
                /**
                 * 对方未关注自己
                 */
                logger.info("恢复指定用户关注缓存操作 当前关系：对方未关注自己");
                jedisService.zadd(RedisKeysConstant.WATCH_LIST_KEY + userId, System.currentTimeMillis(), mainUserId + "_0"); //关注对方
              }
            }
          }
        }
      } catch (Exception ex) {
        ex.printStackTrace();
        logger.error("恢复指定用户关注缓存操作 恢复异常");
      }
    }
    logger.info("恢复指定用户关注缓存操作 End");
  }

  /**
   * 初始化所有用户的关注关系缓存（谨慎操作）
   */
  @Override
  public void initWatchRelationCache(int userRecordId) {
    logger.info("初始化所有用户的关注关系缓存操作 Start");
    int cleanCount = 0;
    try {
      //获取所有发生关注关系的用户列表DB
      List<String> hasWatchList = watchRelationDao.getAllUserIdsForHasWatchRelation();
      if (null != hasWatchList && hasWatchList.size() > 0) {
        for (String userId : hasWatchList) {
          recoverWatchRelationCacheByUserId(userId);
          cleanCount++;
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("恢复指定用户关注缓存操作 恢复异常");
    }
    logger.info("初始化所有用户的关注关系缓存操作 一共恢复 " + cleanCount + "条数据");
    logger.info("初始化所有用户的关注关系缓存操作 End");
  }

  /**
   * 清空，一次性操作
   */
  @Override
  public void clearWatchRelationCache() {
    logger.info("清空，一次性操作操作 Start");
    int cleanCount = 0;
    try {
      //获取所有发生关注关系的用户列表DB
      List<String> hasWatchList = watchRelationDao.getAllUserIdsForHasWatchRelation();
      if (null != hasWatchList && hasWatchList.size() > 0) {
        for (String userId : hasWatchList) {
          if (StringUtils.isNotBlank(userId)) {
            logger.info("清空，一次性操作操作 userId：" + userId);
            jedisService.del(RedisKeysConstant.WATCH_LIST_KEY + userId);
            cleanCount++;
          } else {
            logger.info("清空，一次性操作操作 userId异常：" + userId);
          }
        }
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("清空，一次性操作操作 恢复异常");
    }
    logger.info("清空，一次性操作操作 一共清空 " + cleanCount + "条数据");
    logger.info("清空，一次性操作操作 End");
  }

  /**
   * 获取用户当天关注数
   *
   * @param userId
   * @return
   */
  @Override
  public long getWatchNumForDay(String userId) {
    String key = RedisKeysConstant.WATCH_NUM_LIMIT_BY_DAY_KEY + userId + "_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
    logger.info("获取用户当天关注数操作 key=" + key);
    long watchNum = 0;
    try {
      String watchNumForDayStr = jedisService.getStr(key);
      logger.info("获取用户当天关注数操作 watchNumForDayStr=" + watchNumForDayStr);
      watchNum = Long.parseLong(watchNumForDayStr);
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("获取用户当天关注数操作 恢复异常");
    } finally {
      return watchNum;
    }
  }

  /**
   * 设置用户当天关注数
   *
   * @param userId
   * @return
   */
  @Override
  public long setWatchNumForDay(String userId) {
    String key = RedisKeysConstant.WATCH_NUM_LIMIT_BY_DAY_KEY + userId + "_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
    logger.info("设置用户当天关注数操作 key=" + key);
    long watchNum = 0;
    try {
      watchNum = jedisService.incr(key);
      logger.info("设置用户当天关注数操作 watchNumr=" + watchNum);
    } catch (Exception ex) {
      ex.printStackTrace();
      logger.error("设置用户当天关注数操作 恢复异常");
    } finally {
      return watchNum;
    }
  }

  /**
   * 刷新粉丝列表
   *
   * @param mainUserId
   * @return a
   * @throws a
   * @description （用一句话描述该方法的用条件、执行流程、适用方法、注意项 - 可选）
   * @author wangchao
   * @create 2017-03-23 15:51
   */
  @Override
  public void refreshFanCacheList(String userId) throws Exception {
    String key = String.format(RedisKeysConstant.USER_FANS, userId);
    jedisService.del(key);
    Page<Map<String, Object>> page = new Page();
    page.setPageNo(1);
    page.setOrderBy("time");
    page.setOrder("DESC");
    page.setPageSize(40);
    page = watchService.listFansInfo(page, userId);
    if (page != null) {
      List<Map<String, Object>> list = page.getResult();
      for (Map<String, Object> map : list) {
        String id = String.valueOf(map.get("id"));
        jedisService.zadd(key, Double.parseDouble(id), map);
      }
      jedisService.expire(key, RedisKeysConstant.ONE_DAY_EXPIRE);
    }
  }

}
