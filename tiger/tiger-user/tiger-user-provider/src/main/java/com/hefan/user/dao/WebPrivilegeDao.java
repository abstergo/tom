package com.hefan.user.dao;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.user.bean.WebUserPrivilege;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by hbchen on 2016/10/17.
 */
@Repository
public class WebPrivilegeDao extends BaseDaoImpl<WebUserPrivilege> {

    public List<WebUserPrivilege> queryWebUserPrivilegeList() {
        String sql = "SELECT * FROM hf_privilege_cfg  ORDER BY sort asc";
        return this.query(sql);
    }
}
