package com.hefan.user.service;

import com.hefan.user.dao.HfIosMajiaBaoCfgDao;
import com.hefan.user.itf.HfIosPackageCfgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Administrator on 2017/3/27.
 */
@Service("hfIosPackageCfgService")
public class HfIosPackageCfgServiceImpl implements HfIosPackageCfgService {

    @Autowired
    private HfIosMajiaBaoCfgDao hfIosMajiaBaoCfgDao;

    @Override
    @Transactional(readOnly = true)
    public int getIosPackageCfgIsUse(String bundleId) throws Exception {
        return hfIosMajiaBaoCfgDao.getIosPackageCfgIsUse(bundleId);
    }
}
