package com.hefan.user.dao;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.common.orm.domain.BaseEntity;
import com.hefan.user.bean.WebUserLevel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lxw on 2016/10/10.
 */
@Repository
public class WebUserLevelDao extends BaseDaoImpl<WebUserLevel> {

    /**
     * 获取用户等级规则配置
     * @return
     */
    public List<WebUserLevel> queryWebUserLevelList() {
        String sql = "SELECT * FROM web_user_level WHERE delete_flag =0 ORDER BY level_val";
        return this.query(sql);
    }
}
