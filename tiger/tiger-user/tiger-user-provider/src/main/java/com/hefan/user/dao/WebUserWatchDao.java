package com.hefan.user.dao;

import com.google.common.collect.Lists;
import com.hefan.common.exception.DataIllegalException;
import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.user.bean.WebUserWatch;
import com.hefan.user.bean.WebUserWatchDetail;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by hbchen on 2016/10/20.
 */
@Repository
public class WebUserWatchDao extends BaseDaoImpl<WebUserWatch> {

    public long queryWebUserWatchTime(String userId,Integer yearMonth) {
        String sql = "SELECT watch_total_time FROM web_user_watchday  where user_id = ? and watch_day=? limit 1";
        return getJdbcTemplate().queryForObject(sql, new Object[] {userId,yearMonth}, Long.class);
    }

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public int initWatchDay(WebUserWatch record) throws Exception{
        String sql = "insert into web_user_watchday (user_id,watch_day,watch_total_time) " +
                "  values (?,?,?)";

        int result = getJdbcTemplate().update(sql, record.getUserId(),record.getWatch_day(),record.getWatch_total_time());
        if(result<=0){
            throw new DataIllegalException(String.format("保存用户%s某天%s时长%s错误", record.getUserId(), record.getWatch_day(), record.getWatch_total_time()));
        }
        return result;
    }

    public int queryWatchDayCount(WebUserWatch record){
        String sql = " select count(1) from web_user_watchday where user_id = ? and watch_day=? ";
        return getJdbcTemplate().queryForObject(sql, new Object[] {record.getUserId(),record.getWatch_day()}, int.class);
    }

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public int updateWebUserWatch(WebUserWatch webUserWatch)  {
        String sql = " update web_user_watchday set ";
            sql += " watch_total_time  = watch_total_time + ? ";
        sql += " where user_id= ? and watch_day = ? ";

        int result = getJdbcTemplate().update(sql,
                    new Object[]{webUserWatch.getWatch_total_time(),webUserWatch.getUserId(),webUserWatch.getWatch_day()});
        if(result<=0){
            throw new DataIllegalException(String.format("更新用户%s某天%s时长%s错误", webUserWatch.getUserId(), webUserWatch.getWatch_day(), webUserWatch.getWatch_total_time()));
        }
        return result;
    }

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public void saveWatchDetail(WebUserWatchDetail detail) {
        String sql = "insert into web_user_watch_detail (user_id,watch_day,total_time) values (?,?,?)";
        int result = getJdbcTemplate().update(sql, detail.getUserId(), detail.getWatchDay(), detail.getTotalTime());
        if (result <= 0) {
            throw new DataIllegalException("保存用户观看时长异常");
        }
    }

    public List<Map<String,Object>> getWatchTimeByDay(String time) {
        String sql = "select sum(total_time) totalTime,user_id userId FROM web_user_watch_detail WHERE watch_day=? GROUP by user_id";
        List<Map<String, Object>> result = getJdbcTemplate().queryForList(sql, time);
        return CollectionUtils.isEmpty(result) ? Lists.newArrayList() : result;
    }
}
