package com.hefan.user.service;

import com.cat.common.meta.DrdsSequenceEnum;
import com.hefan.common.util.DynamicProperties;
import com.hefan.user.dao.UserSequenceDao;
import com.hefan.user.itf.UserSequenceService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by lxw on 2016/9/27.
 */
@Component("userSequenceService")
public class UserSequenceServiceImpl implements UserSequenceService {

    @Resource
    private UserSequenceDao userSequenceDao;

    @Override
    public long getNextSeqVal(int seqType) {
        if(!DynamicProperties.getBoolean("isUseDRDSseq")) { //自检sequece
            return this.getNextSeqVal(seqType, 1L);
        } else  {
            //由drds 获取唯一sqeuce
            String sequenceName = DrdsSequenceEnum.getSequenceNameByType(seqType);
            if(StringUtils.isBlank(sequenceName)) {
                return 0L;
            } else {
                return userSequenceDao.getSequenceFromDrdsBySequenceName(sequenceName);
            }
        }

    }

    @Override
    public synchronized long getNextSeqVal(int seqType, long seqNum) {
        return userSequenceDao.getNextSeqVal(seqType,seqNum);
    }

}
