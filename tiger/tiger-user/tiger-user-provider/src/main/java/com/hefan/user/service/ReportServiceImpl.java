package com.hefan.user.service;

import com.hefan.user.bean.Denounce;
import com.hefan.user.bean.Report;
import com.hefan.user.dao.DenounceDao;
import com.hefan.user.dao.ReportDao;
import com.hefan.user.itf.ReportService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by hbchen on 2016/9/28.
 */
@Component("reportService")
public class ReportServiceImpl implements ReportService {

    @Resource
    DenounceDao denounceDao;
    @Resource
    ReportDao reportDao;

    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public Long report(Report report){
        Denounce denounce = denounceDao.noHandlerDenounce(report.getReportedUserId());
        if (denounce == null) {
            denounce = new Denounce();
            denounce.setDenouncedUserId(report.getReportedUserId());
            denounceDao.addNewDenounce(denounce);
        }
        report.setDenounceId(0L);
        if (denounce.getId() != null) {
            report.setDenounceId(denounce.getId());
        }

        report.setDateTime(new Timestamp(new Date().getTime()));

        return reportDao.insertReport(report);

    }
}
