package com.hefan.user.service;

import javax.annotation.Resource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.user.itf.LiveBookingPushService;
import com.hefan.user.itf.LiveBookingService;

/**
 * 直播预约推送定时任务
 * @author sagagyq
 *
 */
@Path("/liveBooking")
@Component("liveBookingPushService")
public class LiveBookingPushServiceImpl implements LiveBookingPushService{
	
	@Resource
	LiveBookingService liveBookingService;
	
	Logger logger = LoggerFactory.getLogger(LiveBookingPushServiceImpl.class);
	
	/**
	 * 检测推送预约直播定时任务
	 */
	@GET
    @Path("/LiveBookingCheckPush")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
	@Override
	public ResultBean LiveBookingCheckPush() {
		try {
			return liveBookingService.LiveBookingCheckPush();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
			return new ResultBean(ResultCode.UnknownException.get_code(),e.getMessage());
		}
	}

}
