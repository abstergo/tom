package com.hefan.user.dao;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.user.bean.FindBug;

@Repository
public class FindBugDao extends BaseDaoImpl<FindBug>{
		@Resource
	    private JdbcTemplate jdbcTemplate;

	    public JdbcTemplate getJdbcTemplate() {
	        return jdbcTemplate;
	    }
	    
	    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
	    public long insertNewRecord(FindBug record) {
	        record = super.save(record);
	        return record.getId();
	    }
}
