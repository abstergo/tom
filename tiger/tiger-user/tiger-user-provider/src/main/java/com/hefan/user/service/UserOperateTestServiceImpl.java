package com.hefan.user.service;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.common.meta.WebUserCachTypeEnum;
import com.hefan.common.util.MapUtils;
import com.hefan.user.config.UserConfigCenter;
import com.hefan.user.itf.UserOperateTestService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/6.
 */
@Service("userOperateTestService")
public class UserOperateTestServiceImpl implements UserOperateTestService {

    @Resource
    private UserConfigCenter userConfigCenter;
    @Override
    public ResultBean userCachZkOperate() {
        long expZK = MapUtils.getLongValue(userConfigCenter.getPublicConfig(), WebUserCachTypeEnum.Exp_type.getUpBorderZooKey(),0l);
        long hefanTotalZK = MapUtils.getLongValue(userConfigCenter.getPublicConfig(),WebUserCachTypeEnum.HefanTotal_type.getUpBorderZooKey(),0l);
        Map resMap = new HashMap();
        resMap.put("expZK",expZK);
        resMap.put("hefanTotalZK",hefanTotalZK);
        return new ResultBean(ResultCode.SUCCESS,resMap);
    }
}
