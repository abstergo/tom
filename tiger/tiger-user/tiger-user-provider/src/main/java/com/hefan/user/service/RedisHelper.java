package com.hefan.user.service;

import com.cat.tiger.service.JedisService;

/**
 * Redis操作异常返回处理
 * Created by kevin_zhang on 18/03/2017.
 */
public class RedisHelper {

    public static Double zscore(JedisService jedisService, String key, Object member) throws Exception {
        Double result = jedisService.zscore(key, member);
        if (null == result)
            result = new Double(-1d);
        return result;
    }

    public static Long zadd(JedisService jedisService, String key, double score, Object member) throws Exception {
        Long result = jedisService.zadd(key, score, member);
        if (null == result)
            result = new Long(-1L);
        return result;
    }


    public static Long zrem(JedisService jedisService, String key, Object member) throws Exception {
        Long result = jedisService.zrem(key, member);
        if (null == result)
            result = new Long(-1L);
        return result;
    }

    public static Double zincrby2(JedisService jedisService, String key,long score, String member) throws Exception {
        Double result = jedisService.zincrby2(key,score,member);
        if (null == result)
            result = new Double(-1d);
        return result;
    }

    public static Long del(JedisService jedisService, String key) throws Exception {
        Long result = jedisService.del(key);
        if (null == result)
            result = new Long(-1L);
        return result;
    }
}
