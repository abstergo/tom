package com.hefan.user.dao;

import com.hefan.user.bean.Report;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by hbchen on 2016/9/28.
 */
@Repository
public class ReportDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    private static String TABLE_NAME = "report";

    /**
     * 增加新的举报记录
     * @param record
     * @return
     */
    @Transactional(propagation= Propagation.REQUIRED, rollbackFor=Exception.class)
    public long insertReport(Report record){

        Map<String,Object> parameters =  new HashMap<String,Object>();
        parameters.put("user_id",record.getUserId());
        parameters.put("denounce_id",record.getDenounceId());
        parameters.put("reason_type",record.getReasonType());
        parameters.put("reason",record.getReason()==null?"":record.getReason());
        parameters.put("date_time",record.getDateTime());
        parameters.put("status",record.getStatus()==null?0:record.getStatus());
        parameters.put("reported_user_id",record.getReportedUserId());
        parameters.put("report_type",record.getReportType());
        parameters.put("contend_id",record.getContendId()==null?0:record.getContendId());
        parameters.put("live_uuid",record.getLiveUuid()==null?"":record.getLiveUuid());
        String[] columns = parameters.keySet().toArray(new String[]{});
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(getJdbcTemplate().getDataSource())
                .usingColumns(columns)
                .withTableName(TABLE_NAME)
                .usingGeneratedKeyColumns("id");
        Number id = simpleJdbcInsert.executeAndReturnKey(parameters);

        return id.longValue();
    }

}
