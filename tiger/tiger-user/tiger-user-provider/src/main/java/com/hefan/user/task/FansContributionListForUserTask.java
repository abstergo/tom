package com.hefan.user.task;

import com.hefan.notify.itf.ItemsStaticService;
import com.hefan.schedule.model.ScheduleExecuteRecord;
import com.hefan.schedule.model.ScheduleServer;
import com.hefan.schedule.service.IScheduleExecuteRecordService;
import com.hefan.schedule.service.IScheduleTaskDeal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 普通用户粉丝贡献榜
 * Created by sagagyq on 2017/3/30.
 */
@Component("fansContributionListForUserTask")
public class FansContributionListForUserTask implements IScheduleTaskDeal {
    private static Logger logger = LoggerFactory.getLogger(FansContributionListForUserTask.class);

    @Resource
    private IScheduleExecuteRecordService scheduleExecuteRecordService;

    @Resource
    private ItemsStaticService itemsStaticService;

    @Override
    public boolean execute(ScheduleServer scheduleServer) {
        ScheduleExecuteRecord scheduleExecuteRecord = new ScheduleExecuteRecord();
        scheduleExecuteRecord.setExecuteTime(new Date());
        long startTime = System.currentTimeMillis();
        boolean rst = true;

        try {
            itemsStaticService.fansContributionListForUser();
        } catch (Exception e) {
            logger.error("普通用户生成粉丝贡献榜异常", e);
            rst = false;
        }
        long endTime = System.currentTimeMillis();
        scheduleExecuteRecord.setConsumingTime((endTime - startTime));
        scheduleExecuteRecord.setNextExecuteTime(scheduleServer.getNextRunStartTime());
        scheduleExecuteRecord.setTaskType(scheduleServer.getBaseTaskType());
        scheduleExecuteRecord.setStatus(rst ? (short) 1 : (short) 2);
        scheduleExecuteRecord.setExecuteResult("普通用户生成粉丝贡献榜成功");
        scheduleExecuteRecord.setHostName(scheduleServer.getHostName());
        scheduleExecuteRecord.setIp(scheduleServer.getIp());
        scheduleExecuteRecordService.addScheduleExecuteRecord(scheduleExecuteRecord);
        return true;
    }
}