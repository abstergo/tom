package com.hefan.user.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.cat.tiger.util.GlobalConstants;
import com.google.common.collect.Maps;
import com.hefan.common.util.DynamicProperties;
import com.hefan.notify.bean.WebPushParam;
import com.hefan.notify.itf.WebUmengPushService;
import com.hefan.user.bean.AdvanceNoticeUser;
import com.hefan.user.bean.LiveNoticeReserve;
import com.hefan.user.bean.WebUser;
import com.hefan.user.dao.AdvanceNoticeUserDao;
import com.hefan.user.dao.LiveNoticeReserveDao;
import com.hefan.user.dao.WebUserDao;
import com.hefan.user.itf.LiveBookingService;
import com.hefan.user.itf.WebUserService;

@Component("liveBookingService")
public class LiveBookingServiceImpl implements LiveBookingService{
	
	@Resource
	private LiveNoticeReserveDao liveNoticeReserveDao;
	
	@Resource
	private AdvanceNoticeUserDao advanceNoticeUserDao;
	
	@Resource
	private WebUserService webUserService;
	
	@Resource
	private WebUmengPushService webUmengPushService;
	
	private Logger logger = LoggerFactory.getLogger(LiveBookingServiceImpl.class);
	
	
	@Override
	public ResultBean saveLiveNoticeReserve(LiveNoticeReserve liveNoticeReserve) {
		
		AdvanceNoticeUser advanceNoticeUser = advanceNoticeUserDao.findAdvanceNoticeUserById(liveNoticeReserve.getLiveNoticeId());
		if(advanceNoticeUser ==null ){
			return new ResultBean(ResultCode.ParamException.get_code(),"预约直播不存在");
		}
		if(advanceNoticeUser.getStartTime() ==null ){
			return new ResultBean(ResultCode.ParamException.get_code(),"预约直播开播时间不存在");
		}
		long minute = getMinute(new Date(),advanceNoticeUser.getStartTime());
		logger.info("距离开播时间还有"+minute+" 分钟");
		//当前时间处于在播时间之前时存预约提醒  处于开播时间之后为异常操作
		if(minute>0){
			if(minute<GlobalConstants.LIVE_BOOK_TIME){
				logger.info("距离开播时间还有"+minute+"分钟 发推送任务给userId为"+liveNoticeReserve.getUserId()+" 的用户");
				List<String> userIdList = new ArrayList<String>();
				userIdList.add(liveNoticeReserve.getUserId());
				
				WebUser webUser= webUserService.findUserInfoFromCache(advanceNoticeUser.getUserId());
				if(webUser==null){
					return new ResultBean(ResultCode.UNSUCCESS.get_code(),"预约的主播不存在");
				}else if( webUser.getState() == 1){
					return new ResultBean(ResultCode.UNSUCCESS.get_code(),"预约的主播被冻结");
				}
				else if( webUser.getSuperiorState() == 1){
					return new ResultBean(ResultCode.UNSUCCESS.get_code(),"预约的主播家族|经济公司已被冻结");
				}
				else if( webUser.getContractState() == 1){
					return new ResultBean(ResultCode.UNSUCCESS.get_code(),"主播已被解约");
				}
				ResultBean res = pushLiveBooking(userIdList);
				if(res.getCode()==ResultCode.SUCCESS.get_code()){
					liveNoticeReserve.setIsNotice(1);
				}else{
					logger.error("预约直播id{}"+advanceNoticeUser.getId()+"推送返回码resultCode{}"+res.getCode()+"推送接口返回消息msg{}"+res.getMsg());
					System.out.println("预约直播id{}"+advanceNoticeUser.getId()+"推送返回码resultCode{}"+res.getCode()+"推送接口返回消息msg{}"+res.getMsg());
				}
			}
			if(liveNoticeReserveDao.saveObj(liveNoticeReserve)==1){
				advanceNoticeUserDao.updatePreCount(liveNoticeReserve.getLiveNoticeId());
			}
			return new ResultBean(ResultCode.SUCCESS.get_code(),ResultCode.SUCCESS.getMsg());
		}else{
			return new ResultBean(ResultCode.UNSUCCESS.get_code(),"预约的直播正在直播中或已经结束");
		}
		
	}

	@Override
	public ResultBean liveBookingStatus(List<LiveNoticeReserve> liveNoticeReserveList) {
		ResultBean resultBean = new ResultBean(ResultCode.SUCCESS.get_code(),ResultCode.SUCCESS.getMsg());
		List<Map<String,Object>> resultList = new ArrayList<Map<String,Object>>();
		for(LiveNoticeReserve liveNoticeReserve:liveNoticeReserveList){
			AdvanceNoticeUser advanceNoticeUser = advanceNoticeUserDao.findAdvanceNoticeUserById(liveNoticeReserve.getLiveNoticeId());
			Map<String,Object> dataMap = Maps.newHashMap();
			dataMap.put("liveNoticeId",liveNoticeReserve.getLiveNoticeId());
			if(advanceNoticeUser ==null ){
				dataMap.put("status", GlobalConstants.LIVE_BOOK_STATUS_NOTEXISTBOOK);
				resultList.add(dataMap);
				continue;
			}
			if(advanceNoticeUser.getStartTime() ==null ){
				dataMap.put("status", GlobalConstants.LIVE_BOOK_STATUS_STARTIMRNOTEXISTENDBOOK);
				resultList.add(dataMap);
				continue;
			}
			long minute = getMinute(new Date(),advanceNoticeUser.getStartTime());
			if(minute<0){
				dataMap.put("status", GlobalConstants.LIVE_BOOK_STATUS_ENDBOOK);
			}else{
				if(liveNoticeReserveDao.checkNotice(liveNoticeReserve) ==GlobalConstants.LIVE_BOOK_STATUS_ISBOOK )
					dataMap.put("status",GlobalConstants.LIVE_BOOK_STATUS_ISBOOK);
				else if(liveNoticeReserveDao.checkNotice(liveNoticeReserve) ==GlobalConstants.LIVE_BOOK_STATUS_NOBOOK )
					dataMap.put("status",GlobalConstants.LIVE_BOOK_STATUS_NOBOOK);
			}
			resultList.add(dataMap);
		}
		resultBean.setData(resultList);
		return resultBean;
	}
	
	private long getMinute(Date startTime,Date endTime){
		return (endTime.getTime() - startTime.getTime()) / (1000 * 60  ) ;
	}
	
	
	@Override
	public ResultBean LiveBookingCheckPush() {
		ResultBean ResultBean = new ResultBean(ResultCode.SUCCESS.get_code(),ResultCode.SUCCESS.getMsg());
		List<AdvanceNoticeUser> advanceNoticeUserList = advanceNoticeUserDao.listAdvanceNoticeUserByStartTime();
		List<LiveNoticeReserve>  liveNoticeReserveList = new ArrayList<LiveNoticeReserve>();
		for(AdvanceNoticeUser advanceNoticeUser:advanceNoticeUserList){
			liveNoticeReserveList = liveNoticeReserveDao.listLiveNoticeReserveByLiveNoticeId(advanceNoticeUser.getId());
			if(liveNoticeReserveList != null && liveNoticeReserveList.size()>0){
				List<String> userIdList = new ArrayList<String>();
				for(LiveNoticeReserve liveNoticeReserve:liveNoticeReserveList){
					userIdList.add(liveNoticeReserve.getUserId());
				}
				//调用推送
				//等待返回批量更新通知状态
				WebUser webUser= webUserService.findUserInfoFromCache(advanceNoticeUser.getUserId());
				if(webUser==null){
					logger.error("预约直播id{}"+advanceNoticeUser.getId()+"主播信息不存在或被删除||主播id{}"+advanceNoticeUser.getUserId());
				}else if( webUser.getState() == 1){
					logger.error("预约直播id{}"+advanceNoticeUser.getId()+"主播被冻结||主播id{}"+advanceNoticeUser.getUserId());
				}
				else if( webUser.getSuperiorState() == 1){
					logger.error("预约直播id{}"+advanceNoticeUser.getId()+"主播家族|经济公司已被冻结||主播id{}"+advanceNoticeUser.getUserId());
				}
				else if( webUser.getContractState() == 1){
					logger.error("预约直播id{}"+advanceNoticeUser.getId()+"主播已被解约||主播id{}"+advanceNoticeUser.getUserId());
				}else{
					ResultBean res = pushLiveBooking(userIdList);
					if(res.getCode()==ResultCode.SUCCESS.get_code()){
						liveNoticeReserveDao.updateNoticeStatusBatch(liveNoticeReserveList);
					}else{
						logger.error("预约直播id{}"+advanceNoticeUser.getId()+"推送返回码resultCode{}"+res.getCode()+"推送接口返回消息msg{}"+res.getMsg());
					}
				}
			}
		}
		return ResultBean;
	}
	
	/**
	 * 推送直播预约信息
	 * @return
	 */
	private ResultBean pushLiveBooking(List<String> userIdList){
		WebPushParam webPushParam = new WebPushParam();
		webPushParam.setUserType(3);
		webPushParam.setTitle(DynamicProperties.getString("liveBook.title"));
		webPushParam.setContent(DynamicProperties.getString("liveBook.content"));
		webPushParam.setUserId(userIdList);
		return webUmengPushService.webPushByParams(webPushParam);
	}
}
