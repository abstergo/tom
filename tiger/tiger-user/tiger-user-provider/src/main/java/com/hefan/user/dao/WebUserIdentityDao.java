package com.hefan.user.dao;

import com.hefan.common.orm.dao.CommonDaoImpl;
import com.hefan.user.bean.WebUserIdentity;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lxw on 2016/10/8.
 */
@Repository
public class WebUserIdentityDao extends CommonDaoImpl {

    private static String TABLE_NAME = "web_user_identity";

    /**
     * 新增用户主播认证信息
     * @param webUserIdentity
     * @return
     */
    public int saveUserIdentity(WebUserIdentity webUserIdentity) {
        Map<String,Object> parameters =  new HashMap<String,Object>();
        parameters.put("user_id",webUserIdentity.getUserId());
        parameters.put("contact_phone",webUserIdentity.getContactPhone());
        parameters.put("real_name",webUserIdentity.getRealName());
        parameters.put("person_id",webUserIdentity.getPersonId());
        parameters.put("life_phone1",webUserIdentity.getLifePhone1());
        parameters.put("life_phone2",webUserIdentity.getLifePhone2());
        parameters.put("life_phone3",webUserIdentity.getLifePhone3());
        parameters.put("history",webUserIdentity.getHistory());
        parameters.put("plat_form",webUserIdentity.getPlatForm());
        parameters.put("live_id",webUserIdentity.getLiveId());
        String[] columns = parameters.keySet().toArray(new String[]{});
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(getJdbcTemplate().getDataSource())
                .usingColumns(columns)
                .withTableName(TABLE_NAME);
        return  simpleJdbcInsert.execute(parameters);
    }

    /**
     * 更新用户的申请主播认证信息
     * @param webUserIdentity
     * @return
     */
    public int updateUserIndentity(WebUserIdentity webUserIdentity) {
        StringBuilder sql = new StringBuilder();
        sql.append("update web_user_identity set contact_phone=?,real_name=?,person_id=?,")
                .append("life_phone1=?,life_phone2=?,life_phone3=?,history=?,plat_form=?,live_id=? ")
                .append(" where user_id=?");
        List<Object> params = new ArrayList<Object>();
        params.add(webUserIdentity.getContactPhone());
        params.add(webUserIdentity.getRealName());
        params.add(webUserIdentity.getPersonId());
        params.add(webUserIdentity.getLifePhone1());
        params.add(webUserIdentity.getLifePhone2());
        params.add(webUserIdentity.getLifePhone3());
        params.add(webUserIdentity.getHistory());
        params.add(webUserIdentity.getPlatForm());
        params.add(webUserIdentity.getLiveId());
        params.add(webUserIdentity.getUserId());
        return this.update(sql.toString(),params);
    }

    public WebUserIdentity getUserIndentityInfo(String userId) {
        String sql = "select * from web_user_identity where user_id=?";
        List<Object> params = new ArrayList<Object>();
        params.add(userId);
        return this.get(sql,params,WebUserIdentity.class);
    }

    /**
     * 修改个人简介
     * @param userId
     * @param bgimg
     * @param profiles
     * @return
     */
    public int modifyUserPersionalProfile(String userId,String bgimg,String profiles) {
        String sql = "update web_user_identity set bgimg=?,profiles=? where user_id=?";
        return this.update(sql,new Object[] {bgimg,profiles,userId});
    }

}