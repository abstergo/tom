package com.hefan.user.dao;

import com.hefan.common.orm.dao.CommonDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * Created by lxw on 2016/10/8.
 */
@Repository
public class SysBankInfoDao extends CommonDaoImpl {

    private static String TABLE_NAME = "sys_bank_info";
}
