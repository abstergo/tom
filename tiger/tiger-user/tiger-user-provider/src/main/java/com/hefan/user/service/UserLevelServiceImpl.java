package com.hefan.user.service;

import com.alibaba.fastjson.JSON;
import com.cat.common.constant.RedisKeyConstant;
import com.cat.tiger.service.JedisService;
import com.hefan.user.bean.WebUserLevel;
import com.hefan.user.bean.WebUserNetworkStirLevel;
import com.hefan.user.dao.WebUserDao;
import com.hefan.user.dao.WebUserLevelDao;
import com.hefan.user.dao.WebUserNetworkStirLevelDao;
import com.hefan.user.dao.WebUserOrderDao;
import com.hefan.user.itf.UserLevelService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by lxw on 2016/10/10.
 */
@Component("userLevelService")
public class UserLevelServiceImpl implements UserLevelService {

    private Logger logger = LoggerFactory.getLogger(UserLevelServiceImpl.class);

    @Resource
    private WebUserLevelDao webUserLevelDao;

    @Resource
    private JedisService jedisService;

    @Resource
    private WebUserOrderDao webUserOrderDao;

    @Resource
    private WebUserNetworkStirLevelDao webUserNetworkStirLevelDao;

    @Resource
    private WebUserDao webUserDao;

    @Override
    @Transactional(readOnly = true)
    public List<WebUserLevel> getUserLevelInfo() {
        //由缓存中获取
        List resList = getUserLevelCfgFromRdis();
        if(resList == null || resList.isEmpty()) {
            //由数据库中获取
            resList = getUserLevelCfgFromDb();
            if(resList != null && !resList.isEmpty()) {
                //保存到redis中
                this.setUserLevelInRedis(JSON.toJSONString(resList));
            }
        }
        return resList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<WebUserLevel> getUserLevelCfgFromDb() {
      return webUserLevelDao.queryWebUserLevelList();
    }

    @Override
    public List<WebUserLevel> getUserLevelCfgFromRdis() {
        try {
            String key = RedisKeyConstant.USER_LEVEL_CFG_KEY;
            String jsonRes = jedisService.getStr(key);
            if(StringUtils.isBlank(jsonRes)) {
                return null;
            }
            return JSON.parseArray(jsonRes, WebUserLevel.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @Override
    public int setUserLevelInRedis(String jsonStr) {
        try {
            jedisService.setStr(RedisKeyConstant.USER_LEVEL_CFG_KEY, jsonStr);
            return 1;
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return 0;
    }

    /**
     * 获取网红等级配置信息
     * @return
     */
    public List<WebUserNetworkStirLevel> getNetworkStirLevelInfo() {
        //由缓存中获取
        List resList = getNetworkStirLevelFromRdis();
        if(resList == null || resList.isEmpty()) {
            //由数据库中获取
            resList = getNetworkStirLevelCfgFromDb();
            if(resList != null && !resList.isEmpty()) {
                //保存到redis中
                this.setNetworkStirLevelInRedis(JSON.toJSONString(resList));
            }
        }
        return resList;
    }

    /**
     * 从DB中获取网红用户等级规则配置
     * @return
     */
    public List<WebUserNetworkStirLevel> getNetworkStirLevelCfgFromDb() {
       return  webUserNetworkStirLevelDao.queryWebUserNetworkStirLevelList();
    }

    /**
     * 获取网红用户等级规则配置从Redis中
     * @return
     */
    public List<WebUserNetworkStirLevel> getNetworkStirLevelFromRdis() {
        try {
            String key = RedisKeyConstant.USER_RED_LEVEL_CFG_KEY;
            String jsonRes = jedisService.getStr(key);
            if(StringUtils.isBlank(jsonRes)) {
                return null;
            }
            return JSON.parseArray(jsonRes, WebUserNetworkStirLevel.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    /**
     * 将网红等级规则保存到redis中
     * @param jsonStr
     * @return   1 成功  0 失败
     */
    public int setNetworkStirLevelInRedis(String jsonStr) {
        try {
            jedisService.setStr(RedisKeyConstant.USER_RED_LEVEL_CFG_KEY, jsonStr);
            return 1;
        }catch (Exception e) {
            logger.error(e.getMessage());
        }
        return 0;
    }


    @Override
    @Transactional(readOnly = true)
    public WebUserLevel userLevelCalc(long exp) {
        try {
            List<WebUserLevel> cfgList = getUserLevelInfo();
            if(cfgList == null || cfgList.isEmpty()) {
                return null;
            } else  {
                for(int i=0; i<cfgList.size(); i++) {
                    WebUserLevel webUserLevel = cfgList.get(i);
                    if(i == cfgList.size()-1) {
                        if(webUserLevel.getLevelMin()<=exp) {
                            return webUserLevel;
                        }
                    } else {
                        if(webUserLevel.getLevelMin()<=exp && exp<=webUserLevel.getLevelMax()) {
                            return webUserLevel;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }



    @Override
    @Transactional(readOnly = true)
    public WebUserNetworkStirLevel networkStirLevelCalc(long hefanTotal) {
        try {
            List<WebUserNetworkStirLevel> cfgList = getNetworkStirLevelInfo();
            if(cfgList == null || cfgList.isEmpty()) {
                return null;
            } else  {
                for(int i=0; i<cfgList.size(); i++) {
                    WebUserNetworkStirLevel WebUserNetworkStirLevel = cfgList.get(i);
                    if(i == cfgList.size()-1) {
                        if(WebUserNetworkStirLevel.getLevelMin()<=hefanTotal) {
                            return WebUserNetworkStirLevel;
                        }
                    } else {
                        if(WebUserNetworkStirLevel.getLevelMin()<=hefanTotal && hefanTotal<=WebUserNetworkStirLevel.getLevelMax()) {
                            return WebUserNetworkStirLevel;
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    @Override
    public String getNetworkStirLevelAuth(int sex, int userLeve) {
        List<WebUserNetworkStirLevel> cfgList = getNetworkStirLevelInfo();
        if(cfgList == null || cfgList.isEmpty()) {
            return "";
        } else  {
            for (WebUserNetworkStirLevel obj : cfgList) {
                if(obj.getLevelVal() == userLeve) {
                    return sex == 1 ? obj.getBoyTitle():obj.getGirlTitle();
                }
            }
        }
        return "";
    }

}
