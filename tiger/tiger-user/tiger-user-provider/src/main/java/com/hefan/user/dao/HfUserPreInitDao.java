package com.hefan.user.dao;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.user.bean.HfUserPreInit;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/1/4.
 */
@Repository
public class HfUserPreInitDao extends BaseDaoImpl<HfUserPreInit> {

    /**
     * 根据userId获取已初始化好的信息
     * @param userId
     * @return
     */
    public HfUserPreInit queryUserPreInitInfo(String userId) {
        String sql = "SELECT user_id,im_token FROM hf_user_pre_init where user_id=? limit 1";
        List<Object> params = new ArrayList<Object>();
        params.add(userId);
        return this.get(sql, params);
    }
}
