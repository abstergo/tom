package com.hefan.user.dao;

import com.hefan.user.bean.FriendsRecommend;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by sagagyq on 2017/3/23.
 */
@Repository
public class FriendsRecommendDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    /**
     * 查询所有热门推荐
     * @return
     */
    public List<FriendsRecommend> selectAllFriendsRecommend(){
        String sql = "SELECT f.user_id userId,f.user_type userType,f.update_time updateTime,f.sort,f.is_del isDel FROM friends_recommend f  ORDER BY f.sort,f.update_time DESC";
        List<FriendsRecommend>  list = jdbcTemplate.query(sql,new BeanPropertyRowMapper<FriendsRecommend>(FriendsRecommend.class));
        return list;
    }

    /**
     * 查询更新时间大于lastUpdateTime的热门推荐
     * @return
     */
    public  List<FriendsRecommend> selectNewFriendsRecommend(Date lastUpdateTime){
        String sql = "SELECT f.user_id userId,f.user_type userType,f.update_time updateTime,f.sort,f.is_del isDel FROM friends_recommend f WHERE f.update_time>? ORDER BY f.update_time DESC ";
        List<FriendsRecommend>  list = jdbcTemplate.query(sql,new BeanPropertyRowMapper<FriendsRecommend>(FriendsRecommend.class),lastUpdateTime);
        return list;
    }

    /**
     * 查询最新的更新时间
     * @return
     */
    public long selectLastUpdateTime(){
        String sql = "SELECT f.update_time updateTime FROM friends_recommend f  ORDER BY f.update_time DESC ";
        List<FriendsRecommend> list =  jdbcTemplate.query(sql,new BeanPropertyRowMapper<FriendsRecommend>(FriendsRecommend.class));
        FriendsRecommend friendsRecommend = CollectionUtils.isEmpty(list)?list.get(0):null;
        if(friendsRecommend!=null && friendsRecommend.getUpdateTime()!=null){
            return friendsRecommend.getUpdateTime().getTime();
        }
        return new Date(0L).getTime();
    }

}
