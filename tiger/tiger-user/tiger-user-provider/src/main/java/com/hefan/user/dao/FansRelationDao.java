package com.hefan.user.dao;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class FansRelationDao {
	
	
	 	@Resource
	    JdbcTemplate jdbcTemplate;
	 	
	 	/**
	 	 * 保存
	 	 * @param userId
	 	 * @param mainUserId
	 	 * @return
	 	 */
	    public int saveObj(String userId,String mainUserId) {
	    	
	    	String sql = "insert IGNORE into fans_relation (user_id,main_user_id)"+
	    				 	"values (?,?)";
	    	 //参数数组
	    	 Object[]  paramArr =new Object[] {
	    			 userId,
	    			 mainUserId
	    			 }; 
	    	 //参数类型数组
	    	 int[] paramtypeArr = new int[]{
	    			 						java.sql.Types.VARCHAR,
	    			 						java.sql.Types.VARCHAR,
	    	 							   };
			int row = 0;
	    	try {
				row = jdbcTemplate.update(sql,paramArr,paramtypeArr);
			} catch (Exception e) {
			}

	    	 return row;
	    }
	    
	    /**
	     * delete删除
	     * @param userId
	     * @param mainUserId
	     * @return
	     */
		public int delObj(String userId, String mainUserId) {
			String sql = "delete from fans_relation where user_id = ? and main_user_id = ? ";
			//参数数组
			 Object[]  paramArr =new Object[] {
	    			 userId,
	    			 mainUserId
	    			 }; 
			//参数类型数组
			 int[] paramtypeArr = new int[]{
						java.sql.Types.VARCHAR,
						java.sql.Types.VARCHAR
					   };
			
			 return jdbcTemplate.update(sql, paramArr, paramtypeArr);
		}

	/**
	 * 根据用户获取用户的粉丝数
	 * @param userId
	 * @return
     */
	public long findFansCount(String userId) {
		String countSql = "select count(1) as rowNum from fans_relation where main_user_id=?";
		return jdbcTemplate.queryForObject(countSql, new Object[] {userId}, Long.class);
	}


}
