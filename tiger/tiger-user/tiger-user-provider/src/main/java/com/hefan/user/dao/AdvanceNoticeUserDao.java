package com.hefan.user.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.cat.tiger.util.GlobalConstants;
import com.hefan.common.orm.dao.CommonDaoImpl;
import com.hefan.user.bean.AdvanceNoticeUser;
import com.hefan.user.bean.WebUser;

/**
 * 预约列表相关的dao
 * @author sagagyq
 *
 */
@Repository
public class AdvanceNoticeUserDao extends CommonDaoImpl{

	@Resource
    JdbcTemplate jdbcTemplate;
	
	public AdvanceNoticeUser findAdvanceNoticeUserById(Integer id){
		String sql = "select id ,name,user_id userId,img_path imgPath , start_time startTime,state,pre_count preCount from advance_notice_user where id = ? and delete_flag = 0 ";
		List<Object> params = new ArrayList<Object>();
        params.add(id);
        return this.get(sql,params,AdvanceNoticeUser.class);
	}
	
	
	/**
	 * 更新预定人数
	 * @return
	 */
	public int updatePreCount(Integer id){
		String sql = " update advance_notice_user set pre_count = pre_count+1 where id = ? ";
		return this.update(sql,new Object[]{id});
	}
	
	/**
	 * 查询出需要推送的预约直播信息  查询为每5分钟查询一次 查询出与推送时间相差5分钟以内的
	 * 
	 * 5:30开始发推送信息
	 * 
	 * 在5:25-5:35时检测发推送避免遗漏
	 * 
	 */
	public List<AdvanceNoticeUser> listAdvanceNoticeUserByStartTime(){
		//计算开始和结束推送时间
		Object[] objArr = new Object[]{GlobalConstants.LIVE_BOOK_TIME+GlobalConstants.LIVE_BOOK_TASK_TIME,GlobalConstants.LIVE_BOOK_TIME-GlobalConstants.LIVE_BOOK_TASK_TIME};
		String sql = " select id ,name,user_id userId,img_path imgPath , start_time startTime,state,pre_count preCount from advance_notice_user  WHERE NOW() BETWEEN SUBDATE(start_time,interval ? minute) AND  SUBDATE(start_time,interval ? minute); ";
		return	this.query(sql,objArr, AdvanceNoticeUser.class);
	}

}
