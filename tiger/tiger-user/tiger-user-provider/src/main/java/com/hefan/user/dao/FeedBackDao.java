package com.hefan.user.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import com.hefan.common.orm.dao.CommonDaoImpl;
import com.hefan.user.bean.FeedBack;

@Repository
public class FeedBackDao extends CommonDaoImpl{
	private static String TABLE_NAME = "feedback";
	
	
	/**
     * 保存新增
     * @param webUser
     * @return
     */
    public int saveFeedBack(FeedBack feedBack) {
        Map<String,Object> parameters =  new HashMap<String,Object>();
        parameters.put("user_id",feedBack.getUserId());
        parameters.put("content",feedBack.getContent());
        parameters.put("contact",feedBack.getContact());
        parameters.put("photo",feedBack.getPhoto());
        parameters.put("time",new Date());
        String[] columns = parameters.keySet().toArray(new String[]{});
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(getJdbcTemplate().getDataSource())
                .usingColumns(columns)
                .withTableName(TABLE_NAME)
                .usingGeneratedKeyColumns("id");
        return simpleJdbcInsert.execute(parameters);
    }

}
