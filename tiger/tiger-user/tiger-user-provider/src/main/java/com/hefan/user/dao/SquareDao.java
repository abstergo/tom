package com.hefan.user.dao;

import com.google.common.collect.Lists;
import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.common.orm.domain.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by kevin_zhang on 10/03/2017.
 */
@Repository
public class SquareDao extends BaseDaoImpl<BaseEntity> {
    public Logger logger = LoggerFactory.getLogger(SquareDao.class);

    @Resource
    JdbcTemplate jdbcTemplate;

    /**
     * 检查用户是否能发动态到广场
     *
     * @param userId
     * @return
     */
    public List<String> checkSquareUserId(String userId) {
        StringBuilder sql = new StringBuilder();
        sql.append("select user_id from square where is_del=0 and type=0 and user_id = ?");
        logger.info(sql.toString());
        List<String> list = jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper(String.class), userId);
        return CollectionUtils.isEmpty(list) ? Lists.newArrayList() : list;
    }
}
