package com.hefan.user.service;

import com.hefan.user.bean.WebUser;
import com.hefan.user.bean.WebUserLevel;
import com.hefan.user.bean.WebUserPrivilege;
import com.hefan.user.dao.WebPrivilegeDao;
import com.hefan.user.itf.UserLevelService;
import com.hefan.user.itf.WebUserPrivilegeService;
import com.hefan.user.itf.WebUserService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hbchen on 2016/10/17.
 */
@Component("webUserPrivilegeService")
public class WebUserPrivilegeServiceImpl implements WebUserPrivilegeService {

    @Resource
    WebUserService webUserService;
    @Resource
    UserLevelService userLevelService;
    @Resource
    WebPrivilegeDao webPrivilegeDao;

    @Override
    public Map getUserLevelInfo(String userId) {

        Map map = new HashMap<>();
        WebUser webUser = webUserService.getWebUserInfoByUserId(userId);
        if (webUser==null){
            map.put("headImg","0");
            map.put("nickName","0");
            map.put("sex","0");
            map.put("userType","0");
            map.put("userId",userId);
            map.put("exp","0");
            map.put("level", 0);
            map.put("nextLevel", 1);
            map.put("minExp", 0);
            map.put("maxExp", 0);

        }else {

            map.put("headImg", webUser.getHeadImg());
            map.put("nickName", webUser.getNickName());
            map.put("sex", webUser.getSex());
            map.put("userType", webUser.getUserType());
            map.put("userId", webUser.getUserId());
            map.put("exp", webUser.getExp());

            WebUserLevel webUserLevel = userLevelService.userLevelCalc(webUser.getExp());
            if (webUserLevel == null) {
                map.put("level", 0);
                map.put("nextLevel", 1);
                map.put("minExp", 0);
                map.put("maxExp", 0);
            } else {
                map.put("level", webUserLevel.getLevelVal());
                map.put("nextLevel", webUserLevel.getLevelVal() + 1);
                map.put("minExp", webUserLevel.getLevelMin());
                map.put("maxExp", webUserLevel.getLevelMax());
            }

        }
        List<WebUserPrivilege> list = webPrivilegeDao.queryWebUserPrivilegeList();
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                WebUserPrivilege webUserPrivilege = list.get(i);
                if (webUserPrivilege != null) {
                    webUserPrivilege.setIsLight("0");
                    String privilegeQfc = webUserPrivilege.getPrivilegeQfc();
                    if (StringUtils.isBlank(privilegeQfc)) {
                        if (webUser.getUserLevel() >= webUserPrivilege.getQfcLevel()) {
                            webUserPrivilege.setIsLight("1");
                        }
                    }
                }
            }
            map.put("privilegeList", list);
        }else {
            map.put("privilegeList", new ArrayList<>());
        }
        return map;
    }
}
