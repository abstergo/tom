package com.hefan.user.service;

import com.cat.common.entity.ResultBean;
import com.cat.common.meta.ResultCode;
import com.hefan.user.bean.WebUser;
import com.hefan.user.bean.WebUserIdentity;
import com.hefan.user.dao.SysBankInfoDao;
import com.hefan.user.dao.WebUserIdentityDao;
import com.hefan.user.itf.UserCacheService;
import com.hefan.user.itf.UserIdentityService;
import com.hefan.user.itf.WebUserService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by lxw on 2016/10/8.
 */
@Component("userIdentityService")
public class UserIdentityServiceImpl implements UserIdentityService {

    @Resource
    private WebUserIdentityDao webUserIdentityDao;
    @Resource
    private SysBankInfoDao sysBankInfoDao;

    @Resource
    private WebUserService webUserService;

    @Resource
    private UserCacheService userCacheService;

    @Override
    @Transactional(readOnly = true)
    public int getUserIndentityCount(String userId) {
        String sql = "select count(1) from web_user_identity where user_id=?";
        return webUserIdentityDao.queryCount(sql,userId);
    }

    @Override
    @Transactional(readOnly = true)
    public WebUserIdentity getUserIndentityInfo(String userId) {
        return webUserIdentityDao.getUserIndentityInfo(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public WebUserIdentity findUserIndentityFromCache(String userId) {
        WebUserIdentity webUserIdentity = userCacheService.findUserIdentityFromRedis(userId);
        if(webUserIdentity == null) {
            webUserIdentity = webUserIdentityDao.getUserIndentityInfo(userId);
            userCacheService.setUserIdentityToRedis(webUserIdentity);
        }
        return webUserIdentity;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ResultBean saveUserIndentity(WebUserIdentity webUserIdentity) {
        WebUser webUser = webUserService.getWebUserInfoByUserId(webUserIdentity.getUserId());
        if(webUser == null) {
            return new ResultBean(ResultCode.LoginUserIsNotExist,null);
        }
        //检查认证信息是否存在
        if(webUser.getIdentityStatus() == 3 && getUserIndentityCount(webUserIdentity.getUserId()) > 0) {
            //保存认证信息
            int rowNum = webUserIdentityDao.updateUserIndentity(webUserIdentity);
            if(rowNum > 0) {
                //更新web_user 表认证状态
                webUserService.updateWebUserIdentityStatus(webUserIdentity.getUserId(),1);
            }
        } else if(webUser.getIdentityStatus() > 0 && webUser.getIdentityStatus() < 3)   {
            return new ResultBean(ResultCode.ParamsIdentityRepeat,null);
        } else {
            //保存认证信息
            int rowNum = webUserIdentityDao.saveUserIdentity(webUserIdentity);
            if(rowNum > 0) {
                //更新web_user 表认证状态
                webUserService.updateWebUserIdentityStatus(webUserIdentity.getUserId(),1);
            }
        }
        return new ResultBean(ResultCode.SUCCESS,null);
    }

    @Override
    @Transactional(readOnly = true)
    public ResultBean findSysBankList() {
        String sql = "select * from sys_bank_info";
        List<Map<String,Object>> resList = sysBankInfoDao.queryMap(sql);
        if(resList == null || resList.isEmpty()) {
            resList = new ArrayList<Map<String,Object>>();
        }
        return new ResultBean(ResultCode.SUCCESS,resList);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public ResultBean modifyUserPersionalProfile(Map paramsMap) {
        String userId = String.valueOf(paramsMap.get("userId"));
        String nickName = String.valueOf(paramsMap.get("nickName"));
        String bgimg = String.valueOf(paramsMap.get("bgimg"));
        String profiles = String.valueOf(paramsMap.get("profiles"));
        //修改认证信息表中简介和背景图
        int updateRow = webUserIdentityDao.modifyUserPersionalProfile(userId,bgimg,profiles);
        WebUser webUser = new WebUser();
        webUser.setUserId(userId);
        webUser.setNickName(nickName);
        //修改用户的昵称
        ResultBean res = webUserService.updateWebUserBaseInfo(webUser);
        return res;
    }

}
