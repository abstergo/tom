package com.hefan.user.service;

import com.cat.tiger.service.JedisService;
import com.hefan.user.constant.RedisKeysConstant;
import com.hefan.user.itf.FansCacheService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 粉丝相关操作
 * Created by kevin_zhang on 14/03/2017.
 */
@Component("fansCacheService")
public class FansCacheServiceImpl implements FansCacheService {
    public static Logger logger = LoggerFactory.getLogger(FansCacheServiceImpl.class);

    @Resource
    JedisService jedisService;

    /**
     * 新增粉丝数
     *
     * @param userId
     * @return
     */
    @Override
    public int newFansNum(String userId) {
        int newFansNum = 0;
        try {
            String newFansNumStr = jedisService.getStr(RedisKeysConstant.FANS_ADD_COUNT_KEY + userId);
            logger.info("获取新增粉丝数操作 获取新增粉丝数：" + newFansNumStr);
            if (StringUtils.isNotBlank(newFansNumStr)) {
                newFansNum = Integer.parseInt(newFansNumStr);
            }
            newFansNum = newFansNum >= 0 ? newFansNum : 0;
            logger.info("获取新增粉丝数操作 获取新增粉丝数成功");
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("获取新增粉丝数操作 获取新增粉丝数异常");
        } finally {
            return newFansNum;
        }
    }
}
