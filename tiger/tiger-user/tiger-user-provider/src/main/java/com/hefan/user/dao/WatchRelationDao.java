package com.hefan.user.dao;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.common.orm.domain.BaseEntity;
import com.hefan.user.bean.RecommendForStar;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Repository
public class WatchRelationDao extends BaseDaoImpl<BaseEntity>{
	
	@Resource
    JdbcTemplate jdbcTemplate;
    
	/**
	 * 保存
	 * @param userId
	 * @param mainUserId
	 * @return
	 */
    public int saveObj(String userId,String mainUserId,Integer liveRemind) {
    	String sql = "insert IGNORE into watch_relation (user_id,main_user_id,live_remind) "+
    				 	"values (?,?,?)";
    	//参数数组
	   	 Object[]  paramArr =new Object[] {userId,mainUserId,liveRemind};
    	 //参数类型数组
    	 int[] paramtypeArr = new int[]{java.sql.Types.VARCHAR,java.sql.Types.VARCHAR,java.sql.Types.INTEGER};
		int row = 0;
		try {
			row = jdbcTemplate.update(sql,paramArr,paramtypeArr);;
		} catch (Exception e) {
		}
    	 return row;
    }
	
    /**
     * 获取该用户关注的所有主播的userid列表
     * @param userId
     * @return
     */
	public List<String> listAllWatchId(String userId) {
		String sql = " select w.main_user_id from watch_relation w where w.user_id = ? ";
		 Object[]  paramArr =new Object[] {userId};
		 int[] paramtypeArr = new int[]{java.sql.Types.VARCHAR};
		 return jdbcTemplate.queryForList(sql, paramArr, paramtypeArr,String.class);
	}

	 /**
     * delete删除
     * @param userId
     * @param mainUserId
     * @return
     */
	public int delObj(String userId, String mainUserId) {
		String sql = "delete from watch_relation where user_id = ? and main_user_id = ? ";
		//参数数组
		 Object[]  paramArr =new Object[] {userId,mainUserId}; 
		//参数类型数组
		 int[] paramtypeArr = new int[]{java.sql.Types.VARCHAR,java.sql.Types.VARCHAR};
		 return jdbcTemplate.update(sql, paramArr, paramtypeArr);
	}

	/**
	 * 获取用户的关注数
	 * @param userId
	 * @return
     */
	public long findWatchedCount(String userId) {
		String countSql = "select count(1) as rowNum from watch_relation where user_id=?";
		return getJdbcTemplate().queryForObject(countSql, new Object[] {userId}, Long.class);
	}

	/**
	 * 是否已进行关注
	 * @param userId  查看人
	 * @param beViUserId 被查看人
     * @return
     */
	public int isWatchedCheck(String userId, String beViUserId) {
		String countSql = "select count(1) as rowNum from watch_relation t where t.user_id=? and t.main_user_id=?";
		return getJdbcTemplate().queryForObject(countSql, new Object[] {userId, beViUserId}, Integer.class);
	}
	
	/**
	 * 修改是否开播提醒
	 * @Title: updateWatchRelationForLiveRemind   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param relationId
	 * @param LiveRemind  0-不提醒  1-提醒
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月12日 下午3:48:35
	 */
	public int updateWatchRelationForLiveRemind(Long relationId, int LiveRemind) {
		// TODO Auto-generated method stub
		String sql = "update watch_relation set live_remind=? where id=?";
		return getJdbcTemplate().update(sql, LiveRemind,relationId);
	}


	public List<RecommendForStar> recommendMesStarClubFor(String userIds) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT");
		sql.append(" wu.nick_name name,wu.head_img headImg,wu.user_id id,wu.person_sign personSign,wu.user_type type ");
		sql.append("  FROM web_user wu WHERE user_id in(" + userIds + ")");
		return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper(RecommendForStar.class));
	}

	public Map getrelationIds(String userId) {
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT wui.relation_ids ids FROM web_user_identity wui WHERE wui.user_id = ? ");
		return jdbcTemplate.queryForMap(sql.toString(), userId);
	}

	/**
	 * 获取所有发生关注关系的用户ids
	 * @return
	 */
	public List<String> getAllUserIdsForHasWatchRelation() {
		String sql = "select DISTINCT(user_id) userId from watch_relation";
		return jdbcTemplate.queryForList(sql,String.class);
	}
}
