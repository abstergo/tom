package com.hefan.user.dao;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hefan.user.bean.LoginToken;

@Repository
public class LoginTokenDao {

	@Resource
    JdbcTemplate jdbcTemplate;
	
	/**
	 * 根据device token 删除loginToken
	 * @Title: deleteLoginToken   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param deviceToken
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月12日 下午2:06:53
	 */
	public int deleteLoginToken(String deviceToken){
		String sql = "delete from login_token where device_token=?";
		return jdbcTemplate.update(sql, deviceToken);
	}
	
	/**
	 * 保存loginToken
	 * @Title: saveLoginToken   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param loginToken
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月12日 下午2:22:04
	 */
	public int saveLoginToken(LoginToken loginToken){
		String sql = "insert into login_token (user_id,device_token,login_time)"+
			 	" values( ?,?,? )";
		//参数数组
		Object[]  paramArr =new Object[] {
				loginToken.getUserId(),
				loginToken.getDeviceToken(),
				new Date()}; 
		//参数类型数组
		int[] paramtypeArr = new int[]{
				java.sql.Types.VARCHAR,
				java.sql.Types.VARCHAR,
				java.sql.Types.TIMESTAMP}; 
		return jdbcTemplate.update(sql,paramArr,paramtypeArr);
	}
	
	public List<String> getLiveRemindUserId(String userId) {
		String sql = " select a.user_id from watch_relation a LEFT JOIN web_user b on a.user_id=b.user_id"
				+" where a.main_user_id=? AND a.live_remind=1 AND b.is_del=0 AND (b.state=0 AND b.superior_state=0)";
		return this.jdbcTemplate.queryForList(sql, String.class,userId);
	}
	
	public List<String> getUserByUserType(String[] userTypes) {
		StringBuilder querySql = new StringBuilder("");
		querySql.append(" select a.user_id from web_user a where a.is_del=0 AND (a.state=0 AND a.superior_state=0)");
		if(userTypes !=null && userTypes.length>0){
			querySql.append(" and a.user_type in (");
			for (int i=0;i<userTypes.length;i++) {
				String userType = userTypes[i]==null?"":userTypes[i];
				querySql.append(userType);
				if(i<userTypes.length-1){
					querySql.append(",");
				}
			}
			querySql.append(")");
		}
		return this.jdbcTemplate.queryForList(querySql.toString(), String.class);
	}
}
