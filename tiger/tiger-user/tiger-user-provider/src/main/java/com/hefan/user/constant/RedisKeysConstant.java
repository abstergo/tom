package com.hefan.user.constant;

/**
 * Created by kevin_zhang on 14/03/2017.
 */
public class RedisKeysConstant {
    /**
     * 我关注的列表
     * key：watch_list_userId
     * score：时间戳
     * member：
     *      userId_0 关注
     *      userId_1 互粉
     */
    public static String WATCH_LIST_KEY = "watch_list_";

    /**
     * 新增粉丝数
     * key：fans_add_count_userId
     * value：新增粉丝数
     */
    public static String FANS_ADD_COUNT_KEY = "fans_add_count_";

    /**
     * 用户每日关注数限制
     * key：watch_num_by_day_userId_YYYYMMMDD
     * value：关注数
     */
    public static String WATCH_NUM_LIMIT_BY_DAY_KEY = "watch_num_limit_by_day_";

    /**
     * 用户粉丝列表
     */
    public static final String USER_FANS = "user_fans_%s";

    /**
     * 用户粉丝列表
     */
    public static final int ONE_DAY_EXPIRE = 24 * 60 * 60;

}
