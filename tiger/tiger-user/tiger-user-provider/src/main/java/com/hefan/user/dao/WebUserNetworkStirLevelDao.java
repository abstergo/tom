package com.hefan.user.dao;

import com.hefan.common.orm.dao.BaseDaoImpl;
import com.hefan.user.bean.WebUserLevel;
import com.hefan.user.bean.WebUserNetworkStirLevel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by lxw on 2016/11/22.
 */
@Repository
public class WebUserNetworkStirLevelDao extends BaseDaoImpl<WebUserNetworkStirLevel> {

    /**
     * 获取网红用户等级规则配置
     * @return
     */
    public List<WebUserNetworkStirLevel> queryWebUserNetworkStirLevelList() {
        String sql = "SELECT id,level_min,level_max,level_val,boy_title,girl_title,max_vedio_num,max_pic_num  FROM web_user_network_stir_level WHERE delete_flag =0 ORDER BY level_val";
        return this.query(sql);
    }
}
