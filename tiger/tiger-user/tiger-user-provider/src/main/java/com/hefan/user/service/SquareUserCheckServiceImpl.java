package com.hefan.user.service;

import com.hefan.user.dao.SquareDao;
import com.hefan.user.itf.SquareUserCheckService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by kevin_zhang on 10/03/2017.
 */
@Component("squareUserCheckService")
public class SquareUserCheckServiceImpl implements SquareUserCheckService {
    public Logger logger = LoggerFactory.getLogger(SquareUserCheckServiceImpl.class);

    @Resource
    SquareDao squareDao;

    /**
     * 检查用户是否能发动态到广场
     *
     * @param userId
     * @return
     */
    @Override
    public boolean checkSquareUserId(String userId) {
        try {
            List<String> result = squareDao.checkSquareUserId(userId);
            return !CollectionUtils.isEmpty(result);
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("查询用户userId={}动态是否上广场异常", userId, ex);
        }
        return false;
    }
}
