package com.hefan.user.dao;

import com.cat.tiger.util.CollectionUtils;
import com.hefan.user.bean.WebUserAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by nigle on 2017/3/24.
 */
@Repository
public class WebUserAccountDao {
    public Logger logger = LoggerFactory.getLogger(WebUserAccountDao.class);

    @Resource
    JdbcTemplate jdbcTemplate;

    /**
     * 记录普通用户收入
     * 有则累加无则更新
     * @param userId
     * @param income
     * @return
     */
    public int updateOrInsertForBaseUserIncome(String userId,long income){
        String sql = " INSERT INTO web_user_account (user_id,income) VALUES (?,?) ON DUPLICATE KEY UPDATE income = income + ?";
        Object[] o = new Object[]{userId, income, income};
        return jdbcTemplate.update(sql, o);
    }

    /**
     * 查询普通用户收入
     * @return
     */
    public WebUserAccount getWebUserAccount(String userId) {
        String sql = "SELECT w.user_id ,w.income,w.create_time,w.update_time FROM web_user_account w WHERE w.user_id = ?";
        List<WebUserAccount> list = jdbcTemplate.query(sql,new BeanPropertyRowMapper<WebUserAccount>(WebUserAccount.class),userId);
        return CollectionUtils.isNotEmpty(list)?list.get(0):null;
    }
}