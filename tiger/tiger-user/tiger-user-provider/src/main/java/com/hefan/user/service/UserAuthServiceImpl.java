package com.hefan.user.service;

import com.cat.common.meta.UserTypeEnum;
import com.hefan.user.bean.WebUser;
import com.hefan.user.dao.UserAuthDao;
import com.hefan.user.itf.UserAuthService;
import com.hefan.user.itf.UserLevelService;
import org.slf4j.profiler.Profiler;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Created by lxw on 2016/10/31.
 */
@Component("userAuthService")
public class UserAuthServiceImpl implements UserAuthService {
    @Resource
    private UserLevelService userLevelService;
    @Resource
    private UserAuthDao userAuthDao;

    @Override
    public String findAnchorInfo(WebUser webUser) {
        String authName = null;
        if(UserTypeEnum.isRedAnchor(webUser.getUserType())) {  //网红
            authName = userAuthDao.findRedUserAuth(webUser.getUserId());
            if(authName == "") {
                //网红等级称号
                authName = userLevelService.getNetworkStirLevelAuth(webUser.getSex(), webUser.getUserLevel());
            }
        } else if(UserTypeEnum.isStarAnchor(webUser.getUserType())) { //明星
            if(webUser.getAuthId() != 0) {
                authName = userAuthDao.findStarUserAuth(webUser.getAuthId());
            }
        }
        if(authName == null) {
            authName = "";
        }
        return authName;
    }
}
