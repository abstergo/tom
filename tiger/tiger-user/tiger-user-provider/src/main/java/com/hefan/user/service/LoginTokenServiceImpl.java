package com.hefan.user.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.hefan.user.bean.LoginToken;
import com.hefan.user.dao.LoginTokenDao;
import com.hefan.user.itf.LoginTokenService;

@Component("loginTokenService")
public class LoginTokenServiceImpl implements LoginTokenService{
	
	@Resource
	LoginTokenDao loginTokenDao;

	@Override
	@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
	public int saveLoginToken(LoginToken loginToken) {
		// TODO Auto-generated method stub
		//删除原有的device token 信息
		this.loginTokenDao.deleteLoginToken(loginToken.getDeviceToken());
		//保存lonin token
		return this.loginTokenDao.saveLoginToken(loginToken);
	}

	@Override
	public List<String> getLiveRemindUserId(String userId) {
		// TODO Auto-generated method stub
		return this.loginTokenDao.getLiveRemindUserId(userId);
	}

	@Override
	public List<String> getUserByUserType(String[] userTypes) {
		// TODO Auto-generated method stub
		return this.loginTokenDao.getUserByUserType(userTypes);
	}

}
