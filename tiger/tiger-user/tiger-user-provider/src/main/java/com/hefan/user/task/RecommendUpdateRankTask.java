package com.hefan.user.task;

import com.hefan.schedule.model.ScheduleExecuteRecord;
import com.hefan.schedule.model.ScheduleServer;
import com.hefan.schedule.service.IScheduleExecuteRecordService;
import com.hefan.schedule.service.IScheduleTaskDeal;
import com.hefan.user.itf.WatchRecommendCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by sagagyq on 2017/3/27.
 */
@Service("recommendUpdateRankTask")
public class RecommendUpdateRankTask implements IScheduleTaskDeal {
    private static Logger logger = LoggerFactory.getLogger(RecommendUpdateRankTask.class);

    @Resource
    private IScheduleExecuteRecordService scheduleExecuteRecordService;

    @Resource
    private WatchRecommendCacheService watchRecommendCacheService;

    @Override
    public boolean execute(ScheduleServer scheduleServer) {
        ScheduleExecuteRecord scheduleExecuteRecord = new ScheduleExecuteRecord();
        scheduleExecuteRecord.setExecuteTime(new Date());
        long startTime = System.currentTimeMillis();
        boolean rst = true;

        try {
            watchRecommendCacheService.updateLastHotRecommend();
        } catch (Exception e) {
            logger.error("热门推荐定时更新排行信息执行异常", e);
            rst = false;
        }
        long endTime = System.currentTimeMillis();
        scheduleExecuteRecord.setConsumingTime((endTime - startTime));
        scheduleExecuteRecord.setNextExecuteTime(scheduleServer.getNextRunStartTime());
        scheduleExecuteRecord.setTaskType(scheduleServer.getBaseTaskType());
        scheduleExecuteRecord.setStatus(rst ? (short) 1 : (short) 2);
        scheduleExecuteRecord.setExecuteResult("热门推荐定时更新排行信息执行成功");
        scheduleExecuteRecord.setHostName(scheduleServer.getHostName());
        scheduleExecuteRecord.setIp(scheduleServer.getIp());
        scheduleExecuteRecordService.addScheduleExecuteRecord(scheduleExecuteRecord);
        return true;
    }
}
