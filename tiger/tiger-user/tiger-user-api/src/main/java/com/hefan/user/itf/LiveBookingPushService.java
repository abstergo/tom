package com.hefan.user.itf;

import com.cat.common.entity.ResultBean;

/**
 * 直播预约推送定时任务
 * @author sagagyq
 *
 */
public interface LiveBookingPushService {
	/**
	 * 定时检测推送任务
	 * @return
	 */
	ResultBean LiveBookingCheckPush();
}
