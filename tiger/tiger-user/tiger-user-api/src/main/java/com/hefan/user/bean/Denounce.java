package com.hefan.user.bean;

import java.io.Serializable;
import java.util.Date;

/*
 * 举报处理流水表
 */
public class Denounce implements Serializable{

    private Long id;

    private String denouncedUserId;

    private Date manageTime;

    private Integer punishTypeId;

    private String manageReson;

    private Integer status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDenouncedUserId() {
        return denouncedUserId;
    }

    public void setDenouncedUserId(String denouncedUserId) {
        this.denouncedUserId = denouncedUserId == null ? null : denouncedUserId.trim();
    }

    public Date getManageTime() {
        return manageTime;
    }

    public void setManageTime(Date manageTime) {
        this.manageTime = manageTime;
    }

    public Integer getPunishTypeId() {
        return punishTypeId;
    }

    public void setPunishTypeId(Integer punishTypeId) {
        this.punishTypeId = punishTypeId;
    }

    public String getManageReson() {
        return manageReson;
    }

    public void setManageReson(String manageReson) {
        this.manageReson = manageReson == null ? null : manageReson.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}