package com.hefan.user.itf;

/**
 * Created by nigle on 2017/3/24.
 */
public interface WebUserAccountService {

    /**
     * 查询普通用户的收入余额
     * @param userId
     * @return
     */
    long getBaseUserIncome(String userId);

    /**
     * 更新或新增普通用户账户余额
     * @param userId
     * @param income
     * @return
     */
    int updateOrInsertForBaseUserIncome(String userId, long income);
}