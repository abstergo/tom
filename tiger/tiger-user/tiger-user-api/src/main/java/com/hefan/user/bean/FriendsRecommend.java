package com.hefan.user.bean;

import java.util.Date;

/**
 * 热门推荐数据库对应实体
 * Created by sagagyq on 2017/3/23.
 */
public class FriendsRecommend {


    private String userId;

    private Date updateTime;

    private int sort;
    private int userType;
    private int isDel;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public int getIsDel() {
        return isDel;
    }


    public void setIsDel(int isDel) {
        this.isDel = isDel;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }
}
