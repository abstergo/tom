package com.hefan.user.itf;

/**
 * Created by lxw on 2016/9/27.
 */
public interface UserSequenceService {

    /**
     * 获取最新sequenceVal
     * @param seqType 0 普通用户自增序列
     * @return
     */
    public long getNextSeqVal(int seqType);

    /**
     * 获取一组自增序列值的最大值
     * @param seqType
     * @param seqNum
     * @return
     */
    public long getNextSeqVal(int seqType, long seqNum);
}
