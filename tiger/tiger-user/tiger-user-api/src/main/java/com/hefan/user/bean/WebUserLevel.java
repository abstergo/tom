package com.hefan.user.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by lxw on 2016/10/10.
 */
public class WebUserLevel extends BaseEntity {

    /**
     * 等级最小值
     */
    @Column("level_min")
    private long levelMin;

    /**
     * 等级最大值
     */
    @Column("level_max")
    private long levelMax;

    /**
     * 等级
     */
    @Column("level_val")
    private int levelVal;

    /**
     * 特权
     */
    private String prvilege;


    public long getLevelMin() {
        return levelMin;
    }

    public void setLevelMin(long levelMin) {
        this.levelMin = levelMin;
    }

    public long getLevelMax() {
        return levelMax;
    }

    public void setLevelMax(long levelMax) {
        this.levelMax = levelMax;
    }

    public int getLevelVal() {
        return levelVal;
    }

    public void setLevelVal(int levelVal) {
        this.levelVal = levelVal;
    }

    public String getPrvilege() {
        return prvilege;
    }

    public void setPrvilege(String prvilege) {
        this.prvilege = prvilege;
    }
}
