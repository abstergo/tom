package com.hefan.user.itf;

import java.util.List;

import com.cat.common.entity.ResultBean;
import com.hefan.user.bean.LiveNoticeReserve;

public interface LiveBookingService {
	
	/**
	 * 保存用户预约信息
	 * @param liveNoticeReserve
	 * @return
	 */
	public ResultBean saveLiveNoticeReserve(LiveNoticeReserve liveNoticeReserve);
	
	/**
	 * 获取预约状态
	 * @param liveNoticeReserve
	 * @return
	 */
	public ResultBean liveBookingStatus(List<LiveNoticeReserve> liveNoticeReserveList);
	
	/**
	 * 检测推送预约直播定时任务
	 * @return
	 */
	public ResultBean LiveBookingCheckPush();
}
