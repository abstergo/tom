package com.hefan.user.itf;

import com.cat.common.entity.Page;
import com.cat.common.meta.WebUserCachTypeEnum;
import com.hefan.user.bean.WebUser;
import com.hefan.user.bean.WebUserIdentity;

import java.util.List;
import java.util.Map;

/**
 * Created by lxw on 2016/10/24.
 */
public interface UserCacheService {

    /**
     * 由redis中获取userInfo
     * @param userId
     * @return
     */
    public WebUser findUserInfoFromRedis(String userId);

    /**
     * 将userInfo保存到redis中
     * @param webUser
     */
    public void setUserInfoToRedis(WebUser webUser);

    /**
     * 由redis中获取WebUserIdentity
     * @param userId
     * @return
     */
    public WebUserIdentity findUserIdentityFromRedis(String userId);

    /**
     * 将webUserIdentity保存到redis中
     * @param webUserIdentity
     */
    public void setUserIdentityToRedis(WebUserIdentity webUserIdentity);

    /**
     * 累计增加用户缓存数据值
     * @param userId
     * @param webUserCachTypeEnum
     * @param incrVal
     * @return
     */
    public Long  incrUserCach(String userId, WebUserCachTypeEnum webUserCachTypeEnum , long incrVal);

    /**
     * 获取用户累计缓存的值（只获取不进行初始化）
     * @param userId
     * @param webUserCachTypeEnum
     * @return
     */
    public Long getIncrUserCach(String userId, WebUserCachTypeEnum webUserCachTypeEnum);
    /**
     * 获取缓存数据
     * @param userId
     * @param webUserCachTypeEnum
     * @return
     */
    public Long getUserCach(String userId,WebUserCachTypeEnum webUserCachTypeEnum);

    /**
     * 初始化用户累计缓存数据
     * @param webUser
     */
    public void initAllUserCach(WebUser webUser);

    /**
     * 获取用户信息获取累计缓存数据
     * @param webUser
     */
    public void getAllUserCach(WebUser webUser);

    /**
     * 删除用户的缓存信息
     * @param userId
     */
    public long delUserBaseInfoCach(String userId);

    /**
     * 主播排行榜
     * //TODO (这里用一句话描述这个方法的作用 - 必填)
     * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
     * @param type 0:积分/总榜，1盒饭 2月榜
     * @param page
     * @return
     * @throws
     * @author wangchao
     * @create 2016/12/7 9:57
     */
    Page<Map<String, Object>> getAnchorRankingList(int type, Page<Map<String, Object>> page) throws Exception;

    void cacheAnchorRanking(int type, Map temp) throws Exception;

    /**
     * 扣费（（直播间/俱乐部）礼物，红包）成功后更新主播排行榜，总榜，月榜
     *
     *
     * @param fromId
     * @param userId   主播id
     * @param hefanNum 增长的盒饭数
     * @return
     * @throws
     * @author wangchao
     * @create 2017/2/7 11:51
     */
    @Deprecated
    void updateRankByRebalance(String fromId, String userId, int hefanNum) throws Exception;

    /**
     * 获取进入俱乐部弹窗状态
     * @param userId
     * @return
     */
    Integer getToClubIndex(String userId);

    /**
     * 设置进入俱乐部弹窗状态
     * @param userId
     */
    void setToClubIndex(String userId);

    /**
     * 首次进入俱乐部推荐
     * @return
     */
    List recommendAnchorFirstCache(String userId);
}
