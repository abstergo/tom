package com.hefan.user.itf;

import com.cat.common.entity.ResultBean;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by Administrator on 2017/1/6.
 */
@Path("/userTt")
public interface UserOperateTestService {

    @GET
    @Path("/userCachZkOperate")
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultBean userCachZkOperate();
}
