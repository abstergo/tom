package com.hefan.user.itf;

import com.hefan.user.bean.WebUserLevel;
import com.hefan.user.bean.WebUserNetworkStirLevel;

import java.util.List;

/**
 * 用户等级操作类
 * Created by lxw on 2016/10/10.
 */
public interface UserLevelService {

    /**
     * 获取用户等级
     * @return
     */
    public List<WebUserLevel> getUserLevelInfo();

    /**
     * 获取用户等级规则配置从DB中
     * @return
     */
    public List<WebUserLevel> getUserLevelCfgFromDb();

    /**
     * 获取用户等级规则配置从Redis中
     * @return
     */
    public List<WebUserLevel> getUserLevelCfgFromRdis();

    /**
     * 将规则保存到redis中
     * @param jsonStr
     * @return   1 成功  0 失败
     */
    public int setUserLevelInRedis(String jsonStr);


    /**
     * 获取网红等级配置信息
     * @return
     */
    public List<WebUserNetworkStirLevel> getNetworkStirLevelInfo();

    /**
     * 获取用户等级规则配置从DB中
     * @return
     */
    public List<WebUserNetworkStirLevel> getNetworkStirLevelCfgFromDb();

    /**
     * 获取用户等级规则配置从Redis中
     * @return
     */
    public List<WebUserNetworkStirLevel> getNetworkStirLevelFromRdis();

    /**
     * 将规则保存到redis中
     * @param jsonStr
     * @return   1 成功  0 失败
     */
    public int setNetworkStirLevelInRedis(String jsonStr);

    /**
     * 根据经验获取等级
     * @param exp
     * @return  null 未处理成功  非空表示返回等级配置对象
     */
    public WebUserLevel userLevelCalc(long exp);



    /**
     * 根据网红的历史累计盒饭获取等级信息
     * @param hefanTotal
     * @return
     */
    public WebUserNetworkStirLevel networkStirLevelCalc(long hefanTotal);

    /**
     * 获取网红等级获取称号
     * @param sex
     * @param userLeve
     * @return
     */
    public String getNetworkStirLevelAuth(int sex, int userLeve);
}
