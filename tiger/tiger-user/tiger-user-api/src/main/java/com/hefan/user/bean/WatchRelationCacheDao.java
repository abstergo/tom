package com.hefan.user.bean;

import java.io.Serializable;

/**
 * Created by kevin_zhang on 14/03/2017.
 */
public class WatchRelationCacheDao implements Serializable {
    String userId;
    /**
     * 0：关注
     * 1：互粉
     * 与 RedisKeysConstant.WATCH_LIST_KEY 保持一致
     */
    int watchRelation;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getWatchRelation() {
        return watchRelation;
    }

    public void setWatchRelation(int watchRelation) {
        this.watchRelation = watchRelation;
    }

    public WatchRelationCacheDao() {
    }

    public WatchRelationCacheDao(String userId, int watchRelation) {
        this.userId = userId;
        this.watchRelation = watchRelation;
    }
}