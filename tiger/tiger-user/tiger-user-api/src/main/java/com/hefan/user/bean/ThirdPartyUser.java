package com.hefan.user.bean;

import java.io.Serializable;

/**
 * 用于第三方通讯
 *
 * @author wangchao
 * @classname com.hefan.user.bean
 * @description
 * @date 2016/11/2 16:40
 */
public class ThirdPartyUser implements Serializable {
  /**
   * 等级
   */
  private Integer userLevel;

  /**
   * 用户类型
   */
  private Integer userType;

  /**
   * 用户ID
   */
  private String userId;
  /**
   * 用户昵称
   */
  private String nickName;
  /**
   * 用户头像
   */
  private String headImg;

  /**
   *
   * @return
   */
  private String liveUuid;

  public Integer getUserLevel() {
    return userLevel;
  }

  public void setUserLevel(Integer userLevel) {
    this.userLevel = userLevel;
  }

  public Integer getUserType() {
    return userType;
  }

  public void setUserType(Integer userType) {
    this.userType = userType;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getNickName() {
    return nickName;
  }

  public void setNickName(String nickName) {
    this.nickName = nickName;
  }

  public String getHeadImg() {
    return headImg;
  }

  public void setHeadImg(String headImg) {
    this.headImg = headImg;
  }

  public String getLiveUuid() {
    return liveUuid;
  }

  public void setLiveUuid(String liveUuid) {
    this.liveUuid = liveUuid;
  }

  @Override
  public String toString() {
    return "ThirdPartyUser{" + "userLevel=" + userLevel + ", userType=" + userType + ", userId='" + userId + '\'' + ", nickName='" + nickName + '\'' + ", headImg='" + headImg + '\'' + ", liveUuid='"
        + liveUuid + '\'' + '}';
  }
}
