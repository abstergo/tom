package com.hefan.user.bean;

import java.io.Serializable;

public class RecommendForStar implements Serializable {

	private String id;
	private String personSign;
	private String name;
	private int type;
	private int isFollow=0;//0关注1互粉2未关注
	private String headImg;
	private int watchedCount;
	private int fansCount;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPersonSign() {
		return personSign;
	}

	public void setPersonSign(String personSign) {
		this.personSign = personSign;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getHeadImg() {
		return headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg;
	}

	public int getWatchedCount() {
		return watchedCount;
	}

	public void setWatchedCount(int watchedCount) {
		this.watchedCount = watchedCount;
	}

	public int getFansCount() {
		return fansCount;
	}

	public void setFansCount(int fansCount) {
		this.fansCount = fansCount;
	}

	public int getIsFollow() {
		return isFollow;
	}

	public void setIsFollow(int isFollow) {
		this.isFollow = isFollow;
	}
}
