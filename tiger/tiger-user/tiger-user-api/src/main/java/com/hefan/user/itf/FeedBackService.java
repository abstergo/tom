package com.hefan.user.itf;

import com.hefan.user.bean.FeedBack;
import com.hefan.user.bean.FindBug;
import com.hefan.user.bean.HfFindBugRecord;

public interface FeedBackService {

	int submit(FeedBack feedBack);
	void findBug(FindBug findBug);
	void visitRecord(HfFindBugRecord visitRecord);
}
