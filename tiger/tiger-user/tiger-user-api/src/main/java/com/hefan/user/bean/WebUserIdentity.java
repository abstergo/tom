package com.hefan.user.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户主播认证信息
 */
public class WebUserIdentity implements Serializable {
    private Long id;

    private String userId;

    private String realName;

    private String personId;

    private Integer history;

    private String infoFile;

    private String platForm;

    private String liveId;

    private String bankId;

    private String personImg;

    private String personImgByHand;

    private String personImgHeads;

    private String personImgTails;

    private Date signStartTime;

    private Date signEndTime;

    private String bank;

    private String bankName;

    private String levelId;

    private Integer ruleDay;

    private Integer ruleTime;

    private Integer validTime;

    private Integer groupId;

    private Integer superiorInto;

    private Integer platformInto;

    private Integer basicSalary;

    private Integer platformIntoRatio;

    private Integer groupIntoRatio;

    private Integer personalIntoRatio;

    private Integer pid;

    private String bgimg;

    private String profiles;

    private String relationIds;

    private Integer operationId;

    private Integer firstShow;

    private String contactPhone;

    private Date applyTime;

    private String agentName;

    private String agentPhone;

    private String agentQq;

    private String lifePhone1;
    private String lifePhone2;
    private String lifePhone3;


    /**
     * 验证码  非实例化属性
     */
    private String msCode;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId == null ? "0" : userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getRealName() {
        return realName == null ? "" : realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? "" : realName.trim();
    }

    public String getPersonId() {
        return personId == null ? "" : personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId == null ? "" : personId.trim();
    }

    public Integer getHistory() {
        return history;
    }

    public void setHistory(Integer history) {
        this.history = history;
    }

    public String getInfoFile() {
        return infoFile == null ? "" : infoFile;
    }

    public void setInfoFile(String infoFile) {
        this.infoFile = infoFile == null ? "" : infoFile.trim();
    }

    public String getPlatForm() {
        return platForm == null ? "" : platForm;
    }

    public void setPlatForm(String platForm) {
        this.platForm = platForm == null ? "" : platForm.trim();
    }

    public String getLiveId() {
        return liveId == null ? "" : liveId;
    }

    public void setLiveId(String liveId) {
        this.liveId = liveId == null ? "" : liveId.trim();
    }

    public String getBankId() {
        return bankId == null ? "" : bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId == null ? "" : bankId.trim();
    }

    public String getPersonImg() {
        return personImg == null ? "" : personImg;
    }

    public void setPersonImg(String personImg) {
        this.personImg = personImg == null ? "" : personImg.trim();
    }

    public String getPersonImgByHand() {
        return personImgByHand == null ? "" : personImgByHand;
    }

    public void setPersonImgByHand(String personImgByHand) {
        this.personImgByHand = personImgByHand == null ? "" : personImgByHand.trim();
    }

    public String getPersonImgHeads() {
        return personImgHeads == null ? "" : personImgHeads;
    }

    public void setPersonImgHeads(String personImgHeads) {
        this.personImgHeads = personImgHeads == null ? "" : personImgHeads.trim();
    }

    public String getPersonImgTails() {
        return personImgTails == null ? "" : personImgTails;
    }

    public void setPersonImgTails(String personImgTails) {
        this.personImgTails = personImgTails == null ? "" : personImgTails.trim();
    }

    public Date getSignStartTime() {
        return signStartTime;
    }

    public void setSignStartTime(Date signStartTime) {
        this.signStartTime = signStartTime;
    }

    public Date getSignEndTime() {
        return signEndTime;
    }

    public void setSignEndTime(Date signEndTime) {
        this.signEndTime = signEndTime;
    }

    public String getBank() {
        return bank == null ? "" : bank;
    }

    public void setBank(String bank) {
        this.bank = bank == null ? "" : bank.trim();
    }

    public String getBankName() {
        return bankName == null ? "" : bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName == null ? "" : bankName.trim();
    }

    public String getLevelId() {
        return levelId == null ? "" : levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId == null ? "" : levelId.trim();
    }

    public Integer getRuleDay() {
        return ruleDay;
    }

    public void setRuleDay(Integer ruleDay) {
        this.ruleDay = ruleDay;
    }

    public Integer getRuleTime() {
        return ruleTime;
    }

    public void setRuleTime(Integer ruleTime) {
        this.ruleTime = ruleTime;
    }

    public Integer getValidTime() {
        return validTime;
    }

    public void setValidTime(Integer validTime) {
        this.validTime = validTime;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Integer getSuperiorInto() {
        return superiorInto;
    }

    public void setSuperiorInto(Integer superiorInto) {
        this.superiorInto = superiorInto;
    }

    public Integer getPlatformInto() {
        return platformInto;
    }

    public void setPlatformInto(Integer platformInto) {
        this.platformInto = platformInto;
    }

    public Integer getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(Integer basicSalary) {
        this.basicSalary = basicSalary;
    }

    public Integer getPlatformIntoRatio() {
        return platformIntoRatio;
    }

    public void setPlatformIntoRatio(Integer platformIntoRatio) {
        this.platformIntoRatio = platformIntoRatio;
    }

    public Integer getGroupIntoRatio() {
        return groupIntoRatio;
    }

    public void setGroupIntoRatio(Integer groupIntoRatio) {
        this.groupIntoRatio = groupIntoRatio;
    }

    public Integer getPersonalIntoRatio() {
        return personalIntoRatio;
    }

    public void setPersonalIntoRatio(Integer personalIntoRatio) {
        this.personalIntoRatio = personalIntoRatio;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getBgimg() {
        return bgimg  == null ? "" : bgimg;
    }

    public void setBgimg(String bgimg) {
        this.bgimg = bgimg == null ? "" : bgimg.trim();
    }

    public String getProfiles() {
        return profiles == null ? "" : profiles;
    }

    public void setProfiles(String profiles) {
        this.profiles = profiles == null ? "" : profiles.trim();
    }

    public String getRelationIds() {
        return relationIds == null ? "" : relationIds;
    }

    public void setRelationIds(String relationIds) {
        this.relationIds = relationIds == null ? "" : relationIds.trim();
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public Integer getFirstShow() {
        return firstShow;
    }

    public void setFirstShow(Integer firstShow) {
        this.firstShow = firstShow;
    }

    public String getContactPhone() {
        return contactPhone == null ? "" : contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone == null ? "" : contactPhone.trim();
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public String getAgentName() {
        return agentName == null ? "" : agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? "" : agentName.trim();
    }

    public String getAgentPhone() {
        return agentPhone == null ? "" : agentPhone;
    }

    public void setAgentPhone(String agentPhone) {
        this.agentPhone = agentPhone == null ? "" : agentPhone.trim();
    }

    public String getAgentQq() {
        return agentQq == null ? "" : agentQq;
    }

    public void setAgentQq(String agentQq) {
        this.agentQq = agentQq == null ? "" : agentQq.trim();
    }

    public String getMsCode() {
        return msCode == null ? "" : msCode;
    }

    public void setMsCode(String msCode) {
        this.msCode = msCode;
    }

    public String getLifePhone1() {
        return lifePhone1;
    }

    public void setLifePhone1(String lifePhone1) {
        this.lifePhone1 = lifePhone1;
    }

    public String getLifePhone2() {
        return lifePhone2;
    }

    public void setLifePhone2(String lifePhone2) {
        this.lifePhone2 = lifePhone2;
    }

    public String getLifePhone3() {
        return lifePhone3;
    }

    public void setLifePhone3(String lifePhone3) {
        this.lifePhone3 = lifePhone3;
    }
}