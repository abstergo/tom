package com.hefan.user.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by hbchen on 2016/10/17.
 */
@Entity(tableName = "hf_privilege_cfg")
public class WebUserPrivilege extends BaseEntity {

    @Column("privilege_name")
    private String privilegeName;
    @Column("privilege_describe")
    private String privilegeDescribe;
    @Column("privilege_img")
    private String privilegeImg;
    @Column("privilege_type")
    private int privilegeType;
    @Column("privilege_qfc")
    private String privilegeQfc;
    @Column("qfc_level")
    private int qfcLevel;
    @Column("sort")
    private int sort;
    @Column("rgbValue")
    private String rgb_value;

    private String isLight;

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName;
    }

    public String getPrivilegeDescribe() {
        return privilegeDescribe;
    }

    public void setPrivilegeDescribe(String privilegeDescribe) {
        this.privilegeDescribe = privilegeDescribe;
    }

    public String getPrivilegeImg() {
        return privilegeImg;
    }

    public void setPrivilegeImg(String privilegeImg) {
        this.privilegeImg = privilegeImg;
    }

    public String getPrivilegeQfc() {
        return privilegeQfc;
    }

    public void setPrivilegeQfc(String privilegeQfc) {
        this.privilegeQfc = privilegeQfc;
    }

    public int getPrivilegeType() {
        return privilegeType;
    }

    public void setPrivilegeType(int privilegeType) {
        this.privilegeType = privilegeType;
    }

    public int getQfcLevel() {
        return qfcLevel;
    }

    public void setQfcLevel(int qfcLevel) {
        this.qfcLevel = qfcLevel;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getRgb_value() {
        return rgb_value;
    }

    public void setRgb_value(String rgb_value) {
        this.rgb_value = rgb_value;
    }

    public String getIsLight() {
        return isLight;
    }

    public void setIsLight(String isLight) {
        this.isLight = isLight;
    }
}
