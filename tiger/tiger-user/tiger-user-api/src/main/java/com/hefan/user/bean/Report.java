package com.hefan.user.bean;

import java.io.Serializable;
import java.util.Date;

public class Report implements Serializable {

    private Long id;
    
    private Long denounceId;
    
    private String userId;//用户id

    private Integer reasonType;//举报类型

    private String reason;//原因文字描述

    private Date dateTime;

    private Integer status;//状态0：未处理；1：举报成功；2：举报失败

    private String reportedUserId;//被举报人id

    private Integer reportType;//1 动态举报 2 评论举报 3 举报主播 4 举报观看直播用户

    private Integer contendId;//report_type 1、2  为动态和评论id  3、4 为0

    private String liveUuid;//report_type 3、4 为直播uuid

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public Long getDenounceId() {
		return denounceId;
	}

	public void setDenounceId(Long denounceId) {
		this.denounceId = denounceId;
	}

	public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Integer getReasonType() {
        return reasonType;
    }

    public void setReasonType(Integer reasonType) {
        this.reasonType = reasonType;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getReportedUserId() {
        return reportedUserId;
    }

    public void setReportedUserId(String reportedUserId) {
        this.reportedUserId = reportedUserId == null ? null : reportedUserId.trim();
    }

    public Integer getReportType() {
        return reportType;
    }

    public void setReportType(Integer reportType) {
        this.reportType = reportType;
    }

    public Integer getContendId() {
        return contendId;
    }

    public void setContendId(Integer contendId) {
        this.contendId = contendId;
    }

    public String getLiveUuid() {
        return liveUuid;
    }

    public void setLiveUuid(String liveUuid) {
        this.liveUuid = liveUuid == null ? null : liveUuid.trim();
    }
}