package com.hefan.user.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

@Entity(tableName="find_bug")
public class FindBug extends BaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column("content")
	private String content;
	@Column("user_id")
	private String userId;
	@Column("contact")
	private String contact;
	@Column("photo")
	private String photo;
	@Column("bug_position")
	private String bugPosition;
	@Column("source")
	private int source;
	@Column("phone_type")
	private String phoneType;
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getBugPosition() {
		return bugPosition;
	}
	public void setBugPosition(String bugPosition) {
		this.bugPosition = bugPosition;
	}
	public int getSource() {
		return source;
	}
	public void setSource(int source) {
		this.source = source;
	}
	public String getPhoneType() {
		return phoneType;
	}
	public void setPhoneType(String phoneType) {
		this.phoneType = phoneType;
	}
}
