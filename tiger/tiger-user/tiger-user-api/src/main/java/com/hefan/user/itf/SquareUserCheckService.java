package com.hefan.user.itf;

/**
 * Created by kevin_zhang on 10/03/2017.
 */
public interface SquareUserCheckService {

    /**
     * 检查用户是否能发动态到广场
     *
     * @param userId
     * @return
     */
    boolean checkSquareUserId(String userId);
}
