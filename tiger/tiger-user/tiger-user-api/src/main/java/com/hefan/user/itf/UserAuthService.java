package com.hefan.user.itf;

import com.hefan.user.bean.WebUser;

/**
 * Created by lxw on 2016/10/31.
 */
public interface UserAuthService {

    /**
     * 获取用户身份称号信息
     * @param webUser
     * @return
     */
    public String findAnchorInfo(WebUser webUser);
}
