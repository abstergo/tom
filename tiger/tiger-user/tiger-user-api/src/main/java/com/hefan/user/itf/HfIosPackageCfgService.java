package com.hefan.user.itf;

/**
 * Created by Administrator on 2017/3/27.
 */
public interface HfIosPackageCfgService {

    /**
     * ios获取马甲包是否显示开关
     * @param bundleId
     * @return
     * @throws Exception
     */
    public int getIosPackageCfgIsUse(String bundleId) throws Exception;
}
