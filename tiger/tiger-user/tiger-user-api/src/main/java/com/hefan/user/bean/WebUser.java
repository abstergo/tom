package com.hefan.user.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.Date;

public class WebUser implements Serializable {

	private Long id;

	private String userId;

	private String nickName;

	private Integer account;

	private String passwords;

	private Integer state;

	private Integer superiorState;

	private Integer contractState;

	private String mobile;

	private String openidWx;

	private String openidWb;

	private String openidQq;

	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date createTime;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date modifyTime;

	private String headImg;

	private Integer userType;

	private Integer sex;
	@JSONField(format = "yyyy-MM-dd")
	private Date birthday;

	private String infoFile;

	private Long watchTotalTime;

	public String getInfoFile() {
		return infoFile;
	}

	public void setInfoFile(String infoFile) {
		this.infoFile = infoFile;
	}

	private String qq;

	private String address;

	private String personSign;

	private String star;

	private String feeling;

	private String job;

	private String province;

	private String city;

	private Integer age;

	private String authInfo;

	private Integer authId;

	private Long payCount;
	/**
	 * 额外粉丝数
	 */
	private Long extraFans;

	/**
	 * 盒饭累计值（不减）
	 */
	private Long hefanTotal;

	private String backGround;

	private Long balance;

	private Long exp;

	private Long levelExp;

	private Integer userLevel;

	private String imToken;

	private Integer identityStatus;

	private Integer toClubStatus;

	private Integer chatRoomId;

	private String token;

	private String gpsLocation;

	private String description;

	private String createFromSrc;

	private Integer isDel;

	private String profiles;

	public String getProfiles() {
		return profiles;
	}

	public void setProfiles(String profiles) {
		this.profiles = profiles == null ? "" : profiles.trim();
	}

	/**
	 * 粉丝数 --非实例化字段--
	 */
	private long fansCount;
	/**
	 * 关注数 --非实例化字段--
	 */
	private long watcherCount;
	/**
	 * 是否被关注 --非实例化字段-- 【0 未关注 , 非0 已关注】
	 */
	private int isWatched;

	/**
	 * 登录返回系统时间 --非实例化字段--
	 */
	private long serverTime;

	/**
	 * 微信的unionId
	 */
	private String unionidWx;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId == null ? "0" : userId;
	}

	public void setUserId(String userId) {
		this.userId = userId == null ? "" : userId.trim();
	}

	public String getNickName() {
		return nickName == null ? "" : nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName == null ? "" : nickName.trim();
	}

	public Integer getAccount() {
		return account;
	}

	public void setAccount(Integer account) {
		this.account = account;
	}

	public String getPasswords() {
		return passwords == null ? "" : passwords;
	}

	public void setPasswords(String passwords) {
		this.passwords = passwords == null ? "" : passwords.trim();
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getSuperiorState() {
		return superiorState;
	}

	public void setSuperiorState(Integer superiorState) {
		this.superiorState = superiorState;
	}

	public Integer getContractState() {
		return contractState;
	}

	public void setContractState(Integer contractState) {
		this.contractState = contractState;
	}

	public String getMobile() {
		return mobile == null ? "" : mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile == null ? "" : mobile.trim();
	}

	public String getOpenidWx() {
		return openidWx == null ? "" : openidWx;
	}

	public void setOpenidWx(String openidWx) {
		this.openidWx = openidWx == null ? "" : openidWx.trim();
	}

	public String getOpenidWb() {
		return openidWb == null ? "" : openidWb;
	}

	public void setOpenidWb(String openidWb) {
		this.openidWb = openidWb == null ? "" : openidWb.trim();
	}

	public String getOpenidQq() {
		return openidQq == null ? "" : openidQq;
	}

	public void setOpenidQq(String openidQq) {
		this.openidQq = openidQq == null ? "" : openidQq.trim();
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getHeadImg() {
		return headImg == null ? "" : headImg;
	}

	public void setHeadImg(String headImg) {
		this.headImg = headImg == null ? "" : headImg.trim();
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getQq() {
		return qq == null ? "" : qq;
	}

	public void setQq(String qq) {
		this.qq = qq == null ? "" : qq.trim();
	}

	public String getAddress() {
		return address == null ? "" : address;
	}

	public void setAddress(String address) {
		this.address = address == null ? "" : address.trim();
	}

	public String getPersonSign() {
		return personSign;
	}

	public void setPersonSign(String personSign) {
		this.personSign = personSign;
	}

	public String getStar() {
		return star == null ? "" : star;
	}

	public void setStar(String star) {
		this.star = star == null ? "" : star.trim();
	}

	public String getFeeling() {
		return feeling == null ? "" : feeling;
	}

	public void setFeeling(String feeling) {
		this.feeling = feeling == null ? "" : feeling.trim();
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getProvince() {
		return province == null ? "" : province;
	}

	public void setProvince(String province) {
		this.province = province == null ? "" : province.trim();
	}

	public String getCity() {
		return city == null ? "" : city;
	}

	public void setCity(String city) {
		this.city = city == null ? "" : city.trim();
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getAuthInfo() {
		return authInfo == null ? "" : authInfo;
	}

	public void setAuthInfo(String authInfo) {
		this.authInfo = authInfo == null ? "" : authInfo.trim();
	}

	public Integer getAuthId() {
		return authId;
	}

	public void setAuthId(Integer authId) {
		this.authId = authId;
	}

	public Long getPayCount() {
		return payCount;
	}

	public void setPayCount(Long payCount) {
		this.payCount = payCount;
	}

	public String getBackGround() {
		return backGround == null ? "" : backGround;
	}

	public void setBackGround(String backGround) {
		this.backGround = backGround == null ? "" : backGround.trim();
	}

	public Long getBalance() {
		return balance;
	}

	public void setBalance(Long balance) {
		this.balance = balance;
	}

	public Long getExp() {
		return exp;
	}

	public void setExp(Long exp) {
		this.exp = exp;
	}

	public Long getLevelExp() {
		return levelExp;
	}

	public void setLevelExp(Long levelExp) {
		this.levelExp = levelExp;
	}

	public Integer getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(Integer userLevel) {
		this.userLevel = userLevel;
	}

	public String getImToken() {
		return imToken == null ? "" : imToken;
	}

	public void setImToken(String imToken) {
		this.imToken = imToken == null ? "" : imToken.trim();
	}

	public Integer getIdentityStatus() {
		return identityStatus;
	}

	public void setIdentityStatus(Integer identityStatus) {
		this.identityStatus = identityStatus;
	}

	public Integer getToClubStatus() {
		return toClubStatus;
	}

	public void setToClubStatus(Integer toClubStatus) {
		this.toClubStatus = toClubStatus;
	}

	public Integer getChatRoomId() {
		return chatRoomId;
	}

	public void setChatRoomId(Integer chatRoomId) {
		this.chatRoomId = chatRoomId;
	}

	public String getToken() {
		return token == null ? "" : token;
	}

	public void setToken(String token) {
		this.token = token == null ? "" : token.trim();
	}

	public String getGpsLocation() {
		return gpsLocation == null ? "" : gpsLocation;
	}

	public void setGpsLocation(String gpsLocation) {
		this.gpsLocation = gpsLocation == null ? "" : gpsLocation.trim();
	}

	public String getDescription() {
		return description == null ? "" : description;
	}

	public void setDescription(String description) {
		this.description = description == null ? "" : description.trim();
	}

	public String getCreateFromSrc() {
		return createFromSrc == null ? "" : createFromSrc;
	}

	public void setCreateFromSrc(String createFromSrc) {
		this.createFromSrc = createFromSrc == null ? "" : createFromSrc.trim();
	}

	public Integer getIsDel() {
		return isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel == null ? 0 : isDel;
	}

	public Long getHefanTotal() {
		return hefanTotal;
	}

	public void setHefanTotal(Long hefanTotal) {
		this.hefanTotal = hefanTotal == null ? 0 : hefanTotal;
	}

	public long getWatcherCount() {
		return watcherCount;
	}

	public void setWatcherCount(long watcherCount) {
		this.watcherCount = watcherCount;
	}

	public long getFansCount() {
		return fansCount;
	}

	public void setFansCount(long fansCount) {
		this.fansCount = fansCount;
	}

	public int getIsWatched() {
		return isWatched;
	}

	public void setIsWatched(int isWatched) {
		this.isWatched = isWatched;
	}

	public long getServerTime() {
		return serverTime;
	}

	public void setServerTime(long serverTime) {
		this.serverTime = serverTime;
	}

	public Long getExtraFans() {
		return extraFans;
	}

	public void setExtraFans(Long extraFans) {
		this.extraFans = extraFans;
	}

	public Long getWatchTotalTime() {
		return watchTotalTime;
	}

	public void setWatchTotalTime(Long watchTotalTime) {
		this.watchTotalTime = watchTotalTime;
	}

	public String getUnionidWx() {
		return unionidWx;
	}

	public void setUnionidWx(String unionidWx) {
		this.unionidWx =  unionidWx == null ? "" : unionidWx.trim();
	}
}