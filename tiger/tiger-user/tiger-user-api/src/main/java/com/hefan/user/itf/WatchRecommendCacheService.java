package com.hefan.user.itf;

import com.cat.common.entity.ResultBean;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * 推荐关注相关操作
 * Created by kevin_zhang on 17/03/2017.
 */

@Path("/watchRecommendCacheService")
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
public interface WatchRecommendCacheService {
    /**
     * 获取热门推荐数据
     * @return
     */
    ResultBean getHotRecommend();

    /**
     * 初始化热门推荐
     * @return
     */
    @GET
    @Path("/initHotRecommend")
    ResultBean initHotRecommend();

    /**
     * 定时更新最新的热门推荐[发推送给前端]
     * @return
     */
    @GET
    @Path("/updateLastHotRecommend")
    ResultBean updateLastHotRecommend();

    /**
     * 全量更新热门推荐数据
     *
     */
    @GET
    @Path("/allUpdateHotRecommend")
    ResultBean allUpdateHotRecommend();

    /**
     * 发热门推荐更新推送
     * @return
     */
    @GET
    @Path("/pushHotRecommend")
    public ResultBean pushHotRecommend();

}
