package com.hefan.user.itf;

import com.cat.common.entity.ResultBean;
import com.hefan.user.bean.WatchRelationCacheDao;

import java.math.BigDecimal;
import java.util.List;

/**
 * 关注相关操作
 * Created by kevin_zhang on 15/03/2017.
 */
public interface WatchCacheService {

    /**
     * 关注
     *
     * @param userId
     * @param mainUserId
     * @return
     */
    ResultBean fork(String userId, String mainUserId);

    /**
     * 关注（DB）
     *
     * @param userId
     * @param mainUserId
     * @param liveRemind
     */
    int forkDB(String userId, String mainUserId, Integer liveRemind);

    /**
     * 取消关注
     *
     * @param userId
     * @param mainUserId
     * @return
     */
    ResultBean unfork(String userId, String mainUserId);

    /**
     * 取消关注（DB）
     *
     * @param userId
     * @param mainUserId
     */
    int unforkDB(String userId, String mainUserId);

    /**
     * 关注总数
     *
     * @param userId
     * @return
     */
    long watchNum(String userId);

    /**
     * 获取指定用户间的关注关系
     *
     * @param userId
     * @param mainUserId
     * @return 0：已关注  1：互粉  2：未关注
     */
    int getWatchRelationByUserId(String userId, String mainUserId);

    /**
     * 获取用户的关注关系列表
     *
     * @return
     */
    List<WatchRelationCacheDao> getAllWatchList(String userId);

    /**
     * 获取用户关注的用户id列表
     */
    List<String> getAllWatchUserIdList(String userId);

    /**
     * 分页获取关注列表用户信息
     *
     * @param pageNo
     * @param pageSize
     * @param userId
     * @param latestScore
     * @return
     */
    ResultBean getWatchListByPage(int pageNo, int pageSize, String userId, BigDecimal latestScore);

    /**
     * 恢复指定用户的关注关系缓存
     *
     * @param userId
     */
    void recoverWatchRelationCacheByUserId(String userId);

    /**
     * 初始化所有用户的关注关系缓存（谨慎操作）
     *
     * @param userRecordId
     */
    void initWatchRelationCache(int userRecordId);

    /**
     * 清空，一次性操作
     */
    void clearWatchRelationCache();

    /**
     * 获取用户当天关注数
     *
     * @param userId
     * @return
     */
    long getWatchNumForDay(String userId);

    /**
     * 设置用户当天关注数
     *
     * @param userId
     * @return
     */
    long setWatchNumForDay(String userId);

    /**
     *  刷新粉丝列表
     *
     * @param mainUserId
     * @return a
     * @throws a
     * @description （用一句话描述该方法的用条件、执行流程、适用方法、注意项 - 可选）
     * @author wangchao
     * @create 2017-03-23 15:51
     */
  void refreshFanCacheList(String mainUserId) throws Exception;

}
