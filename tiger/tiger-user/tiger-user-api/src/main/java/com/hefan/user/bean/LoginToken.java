package com.hefan.user.bean;

import java.io.Serializable;
import java.util.Date;

import com.hefan.common.orm.annotation.Column;

public class LoginToken implements Serializable{
	
	/**   
	 * @Description: serialVersionUID : TODO(用一句话描述这个变量表示什么)
	 * @author: LiTeng
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	
	/**
     * 用户user_id
     */
    @Column("user_id")
    private String userId;
    
    /**
     * 设备串（umeng）
     */
    @Column("device_token")
    private String deviceToken;
    
    /**
     * 登陆时间
     */
    @Column("login_time")
    private Date loginTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}
    
}
