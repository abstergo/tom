package com.hefan.user.itf;

import com.cat.common.entity.ResultBean;
import com.hefan.user.bean.WebUserIdentity;

import java.util.Map;

/**
 * Created by lxw on 2016/10/8.
 */
public interface UserIdentityService {

    /**
     * 获取用户申请主播认证记录数
     * @param userId
     * @return
     */
    public int getUserIndentityCount(String userId);

    /**
     * 根据userId获取认证信息
     * @param userId
     * @return
     */
    public WebUserIdentity getUserIndentityInfo(String userId);

    /**
     * 根据userId从缓存中获取认证信息
     * @param userId
     * @return
     */
    public WebUserIdentity findUserIndentityFromCache(String userId);

    /**
     * 新增用户申请主播认证信息
     * @param webUserIdentity
     * @return
     */
    public ResultBean saveUserIndentity(WebUserIdentity webUserIdentity);

    /**
     * 获取认证填写的开户行列表信息
     * @return
     */
    public ResultBean findSysBankList();

    /**
     * 修改用户的个人简介信息
     * @param paramsMap
     * @return
     */
    public ResultBean modifyUserPersionalProfile(Map paramsMap);
}
