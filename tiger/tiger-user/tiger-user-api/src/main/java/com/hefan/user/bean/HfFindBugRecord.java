package com.hefan.user.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;
/**
 * 访问记录
 * @author sagagyq
 *
 */
@Entity(tableName="hf_find_bug_record")
public class HfFindBugRecord extends BaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 用户id
	 */
	@Column("user_id")
	private String userId;
	
	/**
	 * 
	 */
	@Column("from_source")
	private int fromSource;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getFromSource() {
		return fromSource;
	}
	public void setFromSource(int fromSource) {
		this.fromSource = fromSource;
	}
}
