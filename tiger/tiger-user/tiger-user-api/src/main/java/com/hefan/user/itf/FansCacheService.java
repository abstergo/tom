package com.hefan.user.itf;

/**
 * 粉丝相关操作
 * Created by kevin_zhang on 14/03/2017.
 */
public interface FansCacheService {

    /**
     * 新增粉丝数
     *
     * @param userId
     * @return
     */
    int newFansNum(String userId);
}
