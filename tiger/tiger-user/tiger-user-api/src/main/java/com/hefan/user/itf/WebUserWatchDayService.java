package com.hefan.user.itf;

import java.util.List;
import java.util.Map;

/**
 * Created by hbchen on 2016/10/20.
 */
public interface WebUserWatchDayService {

  /**
   * 长经验
   *
   * @param userId ,用户id
   * @param time   时长
   */
  public void updateUserExpByWatchTime(String userId, long time);

  /**
   * 根据日期查询，所有用户观看时长
   *
   * @param time
   * @return
   * @throws
   * @description
   * @author wangchao
   * @create 2016/12/28 14:13
   */
  List<Map<String, Object>> getWatchTimeByDay(String time);

  void updateUserWatchTime(String userId, long watchTime, String time) throws Exception;
}
