package com.hefan.user.bean;

import java.util.Date;

public class AdvanceNoticeUser  implements java.io.Serializable{
	
	/**
	 * 预约直播id
	 */
	private int id;
	/**
	 * 预约名称
	 */
	private String name;
	/**
	 * 预约主播id
	 */
	private String userId;
	/**
	 * 预约展示图片路径
	 */
	private String imgPath;
	
	/**
	 * 预约直播开始时间
	 */
	private Date startTime;
	/**
	 * 预约状态 1开始预定2正在直播3已结束
	 */
	private int state;
	/**
	 * 预定人数
	 */
	private int preCount;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public int getPreCount() {
		return preCount;
	}
	public void setPreCount(int preCount) {
		this.preCount = preCount;
	}
}
