package com.hefan.user.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by Administrator on 2017/1/4.
 */
@Entity(tableName ="hf_user_pre_init")
public class HfUserPreInit extends BaseEntity {

    @Column("user_id")
    private String userId;

    @Column("im_token")
    private String ImToken;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getImToken() {
        return ImToken;
    }

    public void setImToken(String imToken) {
        ImToken = imToken;
    }
}
