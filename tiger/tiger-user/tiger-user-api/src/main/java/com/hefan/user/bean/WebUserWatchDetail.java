package com.hefan.user.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by wangchao on 2016/12/28.
 */
@Entity(tableName = "web_user_watch_detail")
public class WebUserWatchDetail extends BaseEntity {
  @Column("user_Id")
  private String userId;
  @Column("total_time")
  private long totalTime;
  @Column("watch_day")
  private int watchDay;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public long getTotalTime() {
    return totalTime;
  }

  public void setTotalTime(long totalTime) {
    this.totalTime = totalTime;
  }

  public int getWatchDay() {
    return watchDay;
  }

  public void setWatchDay(int watchDay) {
    this.watchDay = watchDay;
  }

}