package com.hefan.user.itf;

import java.util.List;
import java.util.Map;

import com.cat.common.entity.Page;
import com.hefan.user.bean.RecommendForStar;

public interface WatchService {
//	List<String> listWatchInfo(String userId);
	
	int fork(String userId,String mainUserId,Integer liveRemind);

	int unfork(String userId, String mainUserId);

	@SuppressWarnings("rawtypes")
	Page listWatchAnchorInfo(Page page, String userId);

	/**
	 * 更新首次进入俱乐部的状态
	 *
	 * @param userId
	 * @param mainUserIds
	 * @return
	 */
	int updateFirstEnterClubStatus(String userId, String mainUserIds);

	/**
	 * 根据用户id获取关注数
	 * @param userId
	 * @return
     */
	public long findWatchedCount(String userId);

	/**
	 * 根据用户id获取真实粉丝数
	 * @param userId
     */
	public long findFansCount(String userId);

	/**
	 * 根据用户id获取全部粉丝数 [额外粉丝数+真实粉丝数]
	 * @param userId
	 * @return
	 */
	public long findAllFansCount(String userId);

    /**
     * 是否已进行关注
     *
     * @param userId     查看人
     * @param beViUserId 被查看人
     * @return
     */
    public int isWatchedCheck(String userId, String beViUserId);

    /**
     * 修改是否开播提醒
     *
     * @param relationId
     * @param LiveRemind 0-不提醒  1-提醒
     * @return
     * @throws
     * @Title: updateWatchRelationForLiveRemind
     * @Description: TODO(这里用一句话描述这个方法的作用)
     * @return: int
     * @author: LiTeng
     * @date: 2016年10月12日 下午3:48:35
     */
    public int updateWatchRelationForLiveRemind(Long relationId, int LiveRemind);

    /**
     * 粉丝列表
     *
     * @param page
     * @param userId
     * @return
     */
    public Page listFansInfo(Page page, String userId);


    /**
     * 俱乐部推荐
     *
     * @return
     */
    List<RecommendForStar> recommendMesStarClubFor(String userIds);

    /**
     * 关注的人
     *
     * @return
     */
    Map getrelationIds(String userId);

	/**
	 * 刷新关注数粉丝数缓存
	 * @param userId
	 */
	public void flushWatchRedis(String userId);
}
