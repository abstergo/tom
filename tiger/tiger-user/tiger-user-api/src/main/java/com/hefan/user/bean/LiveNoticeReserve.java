package com.hefan.user.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/***
 * 用户直播预约表
 * @author sagagyq
 *
 */
@Entity(tableName="live_notice_reserve")
public class LiveNoticeReserve extends BaseEntity{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 直播预约表id
	 */
	@Column("live_notice_id")
	private int liveNoticeId;
	/**
	 * 预约用户id
	 */
	@Column("user_id")
	private String userId;
	/**
	 * 是否通知 0未通知 1通知
	 */
	@Column("is_notice")
	private int isNotice=0;
	public int getLiveNoticeId() {
		return liveNoticeId;
	}
	public void setLiveNoticeId(int liveNoticeId) {
		this.liveNoticeId = liveNoticeId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public int getIsNotice() {
		return isNotice;
	}
	public void setIsNotice(int isNotice) {
		this.isNotice = isNotice;
	}
}
