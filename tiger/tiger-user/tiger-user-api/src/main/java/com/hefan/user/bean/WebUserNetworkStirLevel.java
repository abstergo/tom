package com.hefan.user.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by lxw on 2016/11/22.
 */
@Entity(tableName = "web_user_network_stir_level")
public class WebUserNetworkStirLevel extends BaseEntity {

    /**
     * 等级最小值
     */
    @Column("level_min")
    private long levelMin;

    /**
     * 等级最大值
     */
    @Column("level_max")
    private long levelMax;

    /**
     * 等级
     */
    @Column("level_val")
    private int levelVal;

    /**
     * 男等级称号
     */
    @Column("boy_title")
    private String boyTitle;

    /**
     * 女|无性别等级称号
     */
    @Column("girl_title")
    private String girlTitle;

    /**
     * 最大上传视频数量
     */
    @Column("max_vedio_num")
    private long maxVedioNum;

    /**
     * 最大上传图片数量
     */
    @Column("max_pic_num")
    private long maxPicNum;

    public long getLevelMin() {
        return levelMin;
    }

    public void setLevelMin(long levelMin) {
        this.levelMin = levelMin;
    }

    public long getLevelMax() {
        return levelMax;
    }

    public void setLevelMax(long levelMax) {
        this.levelMax = levelMax;
    }

    public int getLevelVal() {
        return levelVal;
    }

    public void setLevelVal(int levelVal) {
        this.levelVal = levelVal;
    }

    public String getBoyTitle() {
        return boyTitle;
    }

    public void setBoyTitle(String boyTitle) {
        this.boyTitle = boyTitle;
    }

    public String getGirlTitle() {
        return girlTitle;
    }

    public void setGirlTitle(String girlTitle) {
        this.girlTitle = girlTitle;
    }

    public long getMaxVedioNum() {
        return maxVedioNum;
    }

    public void setMaxVedioNum(long maxVedioNum) {
        this.maxVedioNum = maxVedioNum;
    }

    public long getMaxPicNum() {
        return maxPicNum;
    }

    public void setMaxPicNum(long maxPicNum) {
        this.maxPicNum = maxPicNum;
    }
}
