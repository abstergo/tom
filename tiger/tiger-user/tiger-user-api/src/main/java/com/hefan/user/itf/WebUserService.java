package com.hefan.user.itf;

import com.cat.common.entity.Page;
import com.cat.common.entity.ResultBean;
import com.hefan.user.bean.HfUserPreInit;
import com.hefan.user.bean.WebUser;

import java.util.List;

/**
 * Created by lxw on 2016/9/26.
 */
public interface WebUserService {

    /**
     * 根据查询参数判断是否存在用户
     * @param queryVal 查询字段内容
     * @param paramType  0 userId 1 微博   2 微信  3 qq  4 手机号
     * @param isDel  0 只查询正常  1 只查询删除  2 全部包含
     * @return
     */
    public int getWebUserCountByQueryVal(String queryVal, int paramType, int isDel);


    /**
     * 根据查询参数判断是否存在用户
     * @param userId 查询字段内容
     * @param isDel  0 只查询正常  1 只查询删除  2 全部包含
     * @return
     */
    public WebUser getWebUserInfoByIsDel(String userId,int isDel);

    /**
     * 根据userId获取用户基本信息
     * @param userId
     * @return
     */
    public WebUser getWebUserInfoByUserId(String userId);

    /**
     * 由缓存中获取用户基本信息
     * @param userId
     * @return
     */
    public WebUser findUserInfoFromCache(String userId);

    /**
     * 根据userId的set对象批量获取用户基本信息[未获取用户累计缓存数据]
     * @param userIdList
     * @return
     */
    public ResultBean getWebUserInfoByUserIdList(List userIdList);

    /**
     * 根据第三方信息获取用基本信息 [获取用户累计缓存数据]
     * @param thridType  1 微博   2 微信  3 qq  4 手机号
     * @param thridOpendId
     * @return
     */
    public WebUser getWebUserInfoByThridParty(int thridType, String thridOpendId);

    /**
     * 保存用户注册信息
     * @param webUser
     * @param thridType
     * @param account
     * @return
     */
    public WebUser saveRegisterUserInfo(WebUser webUser, int thridType, String account);

    /**
     * 绑定|解绑用户账户信息[微博|微信|QQ|手机号]（不进行手机号解绑）
     * @param userId
     * @param opFlag  1 绑定   2 解绑
     * @param thirdType 1 微博   2 微信  3 qq  4 手机号
     * @param openid
     * @param headImg
     * @param nickName
     * @param qq
     * @param unionidWx 微信的unionid
     * @return
     */
    public ResultBean userAccountSafeOprate(String userId, int opFlag, int thirdType, String openid, String headImg, String nickName, String qq, String unionidWx);

    /**
     * 根据用户id增加饭票数
     * @param userId
     * @param incrBalance
     * @return
     */
    public int incrWebUserBalance(String userId, long incrBalance);

    /**
     * 普通用户进行盒饭兑换饭票，增加饭票减少盒饭
     * @param userId
     * @param incrBalance
     * @param hefanNum
     * @return
     */
    public int incrWebUserBalanceAndHefan(String userId,long incrBalance, long hefanNum);

    /**
     * 根据用户userId更新用户盒饭的累计值
     * @param userId
     * @param hefanVal
     * @return
     */
    public int icrWebUserHefanTotal(String userId, long hefanVal);

    /**
     * 由缓存中获取盒饭total,并更新数据库
     * @param userId
     */
    public void incrWebUserHefanFromCach(String userId);

    /**
     * 根据用户userId更新用户基本信息
     * @param webUser
     * @return
     */
    public ResultBean updateWebUserBaseInfo(WebUser webUser);

    /**
     * 根据userId更新用户申请主播认证状态
     * @param userId
     * @param identityStatus
     * @return
     */
    public int updateWebUserIdentityStatus(String userId, int identityStatus);

    /**
     * 根据userId更新用户的定位
     * @param userId
     * @param gpsLocation
     * @return
     */
    public int updateWebUserLocation(String userId, String gpsLocation);

    /**
     * 根据userId更新用户直播信息
     * @param userId
     * @param liveUuid
     * @param token
     * @return
     */
    public int updateWebUserLiveInfo(String userId, String liveUuid, String token);

    /**
     * 获取用户我的信息
     * @param userId
     * @return
     */
    public WebUser findMyUserInfo(String userId);

    /**
     * 获取用我的信息由缓存中获取
     * @param userId
     * @return
     */
    public WebUser findMyUserInfoFromCach(String userId);

    /**
     * 查看其它用户的信息
     * @param userId  查看人userId
     * @param beViUserId 被查看人userId
     * @return
     */
    public WebUser findOtherUserInfo(String userId, String beViUserId);

    /**
     * 用户等级处理
     * @param userId 用户id
     * @param incrExp 用户经验
     * @return
     */
    public ResultBean userLevelOprate(String userId, long incrExp);
    
    /**
     * 随机取出27个没有关注的明星和片场信息
     * @param userId
     * @return
     */
    public List recommendAnchorFirst(String userId);
    
    /**
     * h5主播排行榜
     * @param page
     * @param userId
     * @param type 0:积分，1盒饭排行榜
     * @return
     */
    public Page getAnchorRankingList(Page page,String userId ,int type) throws Exception;

    /**
     * h5主播排行榜
     * @param page
     * @param type 0:总榜，2月榜
     * @return
     */
    public Page getAnchorRankingList2(Page page,int type) throws Exception;


    /**
     * 查询单个主播月榜收入，扣除内部用户和黑名单
     * @description
     * @param userId 主播id
     * @return
     * @throws 
     * @author wangchao
     * @create 2017/2/8 9:32
     */
    void getUserMonthRankingData(String userId) throws Exception;

    /**
     * 查询单个主播总榜收入，扣除内部用户和黑名单
     * @description
     * @param userId 主播id
     * @param type
     * @return
     * @throws
     * @author wangchao
     * @create 2017/2/8 9:32
     */
    void getUserRankingData(String userId, int type) throws Exception;

    /**
     * 推荐明星俱乐部
     * @param page
     * @param userId
     * @return
     */
    public Page recommendStarClub(Page page,String userId);
    
	/**
	 * 更新用户观看时长
	 * 
	 * @param userId
	 * @param watchTime
	 * @return
	 */
	public int updateUserWatchTime(String userId, long watchTime);
	
	
	public List getPunishDetail();

    /**
     * 分页获取用户列表
     * @param page
     * @return
     * @throws
     * @author wangchao
     * @create 2016/11/9 11:24
     */
    Page<WebUser> getWebListByPage(Page<WebUser> page);


    /**
     * 获取用户信息
     * @param
     * @return
     * @throws
     * @author wangchao
     * @create 2016/11/9 11:24
     */
     WebUser getDynaUser(String userId) ;


    /**
     * 更新用户的新微信openid信息
     * @param userId
     * @param wxOpenId
     * @return
     */
    public int modifyWebUserInfoNewWxOpenId(String userId,String wxOpenId);

    /**
     * 获取用户已初始化好的用户信息
     * @param userId
     * @return
     */
    public HfUserPreInit getUserPreInit(String userId);

    /**
     * 扣费（（直播间/俱乐部）礼物，红包）成功后更新主播排行榜，总榜，月榜，影响扣费性能 废弃
     * @description （用一句话描述该方法的适用条件、执行流程、适用方法、注意事项 - 可选）
     * @param fromId  送礼人id
     *@param authId 主播id
     * @param hefanNum 增长的盒饭数   @return
     * @throws
     * @author wangchao
     * @create 2017/2/7 11:51
     */
    @Deprecated
    void updateRankByRebalance(String fromId, String authId, int hefanNum) throws Exception;

    /**
     * 同步app发现主播排行榜静态文件
     * @description 只用于本同步文件
     * @return
     * @throws 
     * @author wangchao
     * @create 2017/2/7 18:37
     */
    void syncOSS();

    /**
     * mgmt 后台手动刷新排行榜
     * @description 仅用于后台手动刷新
     * @param
     * @return
     * @throws
     * @author wangchao
     * @create 2017/2/8 14:24
     */
    void refreshRanking() throws Exception;


    /**
     * 获取某一时间点之后的无效用户信息
     * @param updateTimeStr  “yyyy-MM-dd HH:mm:ss”
     * @return
     * @throws Exception
     */
    public List<WebUser> findInvalidUserInfoByUpdateTime(String updateTimeStr) throws Exception;


    /**
     * 更新用户的unionidWx
     * @param userId
     * @param unionidWx
     * @throws Exception
     */
    public void updateWebUserUnionidWx(String userId, String unionidWx) throws Exception;
}
