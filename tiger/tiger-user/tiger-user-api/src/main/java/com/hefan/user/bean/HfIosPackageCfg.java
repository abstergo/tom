package com.hefan.user.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by Administrator on 2017/3/27.
 */
public class HfIosPackageCfg extends BaseEntity {

    @Column("is_use")
    private int isUse;

    @Column("bundle_id")
    private String bundleId;

    public int getIsUse() {
        return isUse;
    }

    public void setIsUse(int isUse) {
        this.isUse = isUse;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }
}
