package com.hefan.user.bean;

import java.io.Serializable;

/**
 * 用户等级变化结果VO
 * Created by lxw on 2016/10/10.
 */
public class UserLevelChangeVo implements Serializable{

    private String userId;

    private long exp;

    private int userLevel;

    /**
     * 等级是否变化 0 未变化  1 等级提升
     */
    private int isChange;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getIsChange() {
        return isChange;
    }

    public void setIsChange(int isChange) {
        this.isChange = isChange;
    }

    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }
}
