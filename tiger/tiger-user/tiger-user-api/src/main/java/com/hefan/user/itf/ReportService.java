package com.hefan.user.itf;

import com.hefan.user.bean.Report;

/**
 * Created by hbchen on 2016/9/28.
 */
public interface ReportService {

    public Long report(Report report);
}
