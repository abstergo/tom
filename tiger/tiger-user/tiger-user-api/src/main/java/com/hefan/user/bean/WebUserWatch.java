package com.hefan.user.bean;

import com.hefan.common.orm.annotation.Column;
import com.hefan.common.orm.annotation.Entity;
import com.hefan.common.orm.domain.BaseEntity;

/**
 * Created by hbchen on 2016/10/20.
 */
@Entity(tableName = "web_user_watchday")
public class WebUserWatch extends BaseEntity {
    @Column("user_Id")
    private String userId;
    @Column("watch_total_time")
    private long watch_total_time;
    @Column("watch_day")
    private int watch_day;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getWatch_total_time() {
        return watch_total_time;
    }

    public void setWatch_total_time(long watch_total_time) {
        this.watch_total_time = watch_total_time;
    }

    public int getWatch_day() {
        return watch_day;
    }

    public void setWatch_day(int watch_day) {
        this.watch_day = watch_day;
    }
}
