package com.hefan.user.bean;
import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class WatchRelation implements java.io.Serializable{
    private long id;

    private String userId;

    private String mainUserId;
    
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date time;
    
    private int liveRemind;
    
    public WatchRelation(){
    	setTime(new Date());
    }

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getMainUserId() {
        return mainUserId;
    }

    public void setMainUserId(String mainUserId) {
        this.mainUserId = mainUserId == null ? null : mainUserId.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

	public int getLiveRemind() {
		return liveRemind;
	}

	public void setLiveRemind(int liveRemind) {
		this.liveRemind = liveRemind;
	}
    
    
}