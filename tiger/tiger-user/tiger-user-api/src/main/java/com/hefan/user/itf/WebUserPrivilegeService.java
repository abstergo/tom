package com.hefan.user.itf;

import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by hbchen on 2016/10/17.
 */
public interface WebUserPrivilegeService {

        public Map getUserLevelInfo(String userId);

}
