package com.hefan.user.bean;

import java.util.Date;

import com.alibaba.fastjson.annotation.JSONField;

public class FeedBack implements java.io.Serializable{
    private long id;

    private String content;

    private String userId;

    private String contact;

    private String photo;
    @JSONField(format="yyyy-MM-dd HH:mm:ss")
    private Date time;

    public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact == null ? null : contact.trim();
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo == null ? null : photo.trim();
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}