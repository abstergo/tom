package com.hefan.user.itf;

import java.util.List;

import com.hefan.user.bean.LoginToken;

public interface LoginTokenService {
	
	/**
	 * 保存登录获取的推送device token
	 * @Title: saveLoginToken   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param loginToken
	 * @return      
	 * @return: int
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月12日 下午1:59:05
	 */
	public int saveLoginToken(LoginToken loginToken);
	
	/**
	 * 查询开播提醒用户token
	 * @Title: getLiveRemindUserId   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userId
	 * @return      
	 * @return: List<String>
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月14日 下午3:59:45
	 */
	public List<String> getLiveRemindUserId(String userId);
	
	/**
	 * 按照类型查找用户token
	 * @Title: getUserByUserType   
	 * @Description: TODO(这里用一句话描述这个方法的作用)   
	 * @param userTypes
	 * @return      
	 * @return: List<String>
	 * @author: LiTeng      
	 * @throws 
	 * @date:   2016年10月20日 下午5:27:17
	 */
	public List<String> getUserByUserType(String [] userTypes);

}
